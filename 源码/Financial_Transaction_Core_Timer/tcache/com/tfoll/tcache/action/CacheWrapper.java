package com.tfoll.tcache.action;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class CacheWrapper extends HttpServletResponseWrapper {
	private CacheAndSOS cacheAndSOS = null;
	private CacheContent cc = null;
	private PrintWriter writer = null;

	public CacheWrapper(HttpServletResponse response) {
		super(response);
		this.cc = new CacheContent();
	}

	public CacheContent getContent() {
		try {
			flush();
		} catch (IOException ignore) {
		}
		cc.toByteArray();// Create the byte array
		return cc;
	}

	/**
	 * Get an output stream
	 */
	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		if (cacheAndSOS == null) {
			cacheAndSOS = new CacheAndSOS(cc.getOutputStream(), super.getOutputStream());
		}
		return cacheAndSOS;
	}

	/**
	 * Get a print writer
	 */
	@Override
	public PrintWriter getWriter() throws IOException {
		if (writer == null) {
			String encoding = getCharacterEncoding();
			if (encoding != null) {
				writer = new PrintWriter(new OutputStreamWriter(getOutputStream(), encoding));
			} else {
				writer = new PrintWriter(new OutputStreamWriter(getOutputStream()));
			}
		}

		return writer;
	}

	/**
	 * Flushes all streams.
	 */
	private void flush() throws IOException {
		if (cacheAndSOS != null) {
			cacheAndSOS.flush();
		}

		if (writer != null) {
			writer.flush();
		}
	}

	public void flushBuffer() throws IOException {
		super.flushBuffer();
		flush();
	}
}
