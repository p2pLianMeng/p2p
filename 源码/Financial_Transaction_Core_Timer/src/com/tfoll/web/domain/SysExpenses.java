package com.tfoll.web.domain;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 系统收入部分
 * 
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.FIELD })
public @interface SysExpenses {

}
