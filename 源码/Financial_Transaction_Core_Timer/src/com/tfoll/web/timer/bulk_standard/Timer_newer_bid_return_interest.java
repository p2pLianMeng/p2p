package com.tfoll.web.timer.bulk_standard;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.LenderBulkStandardNewerBidRecordM;
import com.tfoll.web.model.LenderBulkStandardNewerBidRecordWaitingM;
import com.tfoll.web.model.SysExpensesRecordsM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 散标新手表利息自动还款
 */
public class Timer_newer_bid_return_interest extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_newer_bid_return_interest.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_newer_bid_return_interest.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_newer_bid_return_interest.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_newer_bid_return_interest.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				logger.info("定时任务:" + Timer_newer_bid_return_interest.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_newer_bid_return_interest.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	/**
	 * <pre>
	 * 业务介绍
	 * 散标新手表N天后为用户收取利息
	 * 
	 * 处理的原理参见联富宝的处理规则
	 * 
	 * 途径  直接从lender_bulk_standard_newer_bid_record_waiting等待表里面查询出待收的散标新手表进行利息返还处理
	 * </pre>
	 */
	public static void doTask() throws Exception {
		String now_day = Model.Date.format(new Date());

		List<LenderBulkStandardNewerBidRecordWaitingM> lender_bulk_standard_newer_bid_record_waiting_list = LenderBulkStandardNewerBidRecordWaitingM.dao.find("SELECT id,newer_bid_record_id from lender_bulk_standard_newer_bid_record_waiting WHERE recieve_date=?", new Object[] { now_day });
		if (Utils.isHasData(lender_bulk_standard_newer_bid_record_waiting_list)) {
			for (LenderBulkStandardNewerBidRecordWaitingM lender_bulk_standard_newer_bid_record_waiting : lender_bulk_standard_newer_bid_record_waiting_list) {
				try {
					doSubTaskFor(lender_bulk_standard_newer_bid_record_waiting);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}

	}

	private static void doSubTaskFor(final LenderBulkStandardNewerBidRecordWaitingM lender_bulk_standard_newer_bid_record_waiting) {
		long newer_bid_record_id = lender_bulk_standard_newer_bid_record_waiting.getLong("newer_bid_record_id");
		final LenderBulkStandardNewerBidRecordM lender_bulk_standard_newer_bid_record = LenderBulkStandardNewerBidRecordM.dao.findFirst("SELECT id,user_id,recieve_interest,is_return_interest from  lender_bulk_standard_newer_bid_record WHERE id=?", new Object[] { newer_bid_record_id });
		if (lender_bulk_standard_newer_bid_record == null) {
			lender_bulk_standard_newer_bid_record_waiting.delete();
			return;
		}
		final int user_id = lender_bulk_standard_newer_bid_record.getInt("user_id");
		/**
		 * 利息在保存的时候已经计算好了
		 */
		final BigDecimal recieve_interest = lender_bulk_standard_newer_bid_record.getBigDecimal("recieve_interest");
		int is_return_interest = lender_bulk_standard_newer_bid_record.getInt("is_return_interest");
		if (is_return_interest == 1) {
			lender_bulk_standard_newer_bid_record_waiting.delete();
			return;
		}
		/**
		 * 利息转移
		 */
		Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				boolean is_ok_update_lender_bulk_standard_newer_bid_record = lender_bulk_standard_newer_bid_record.//
						set("is_return_interest", 1).//
						update();
				if (!is_ok_update_lender_bulk_standard_newer_bid_record) {
					return false;
				}
				boolean is_ok_delete_lender_bulk_standard_newer_bid_record_waiting = lender_bulk_standard_newer_bid_record_waiting.delete();
				if (!is_ok_delete_lender_bulk_standard_newer_bid_record_waiting) {
					return false;
				}
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
				boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used.add(recieve_interest)).update();
				if (!is_ok_update_user_now_money) {
					return false;
				}
				/**
				 * 添加财务记录
				 * 
				 * <pre>
				 * sys_expenses_records-yes
				 * sys_income_records-no
				 * user_money_change_records-yes
				 * user_transaction_records-yes
				 * </pre>
				 */
				boolean is_ok_add_sys_income_records = SysExpensesRecordsM.add_sys_expenses_records(user_id, UserMoneyChangeRecordsM.Type_201, recieve_interest, "支出散标新手标利息,新手标Id:" + lender_bulk_standard_newer_bid_record.getLong("id"));
				if (!is_ok_add_sys_income_records) {
					return false;
				}
				boolean is_ok_add_user_money_change_records = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_201, cny_can_used.add(cny_freeze), new BigDecimal("0"), recieve_interest, cny_can_used.add(cny_freeze).add(recieve_interest), "支出散标新手标利息#新手标Id:" + lender_bulk_standard_newer_bid_record.getLong("id"));
				if (!is_ok_add_user_money_change_records) {
					return false;
				}
				boolean is_ok_add_user_transaction_records_by_type = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id, UserTransactionRecordsM.Type_201, cny_can_used.add(cny_freeze), new BigDecimal("0"), recieve_interest, cny_can_used.add(cny_freeze).add(recieve_interest), "支出散标新手标利息");
				if (!is_ok_add_user_transaction_records_by_type) {
					return false;
				}
				return true;
			}
		});

	}

}