package com.tfoll.web.timer.bulk_standard;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.ProcedureCall;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderWaitingM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldLogM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardOrderM;
import com.tfoll.web.model.SysErrorInfoM;
import com.tfoll.web.model.SysErrorInfoWaitingForDealingM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model._____Activity___Standard_Order_Waiting_Of_BorrowerM;
import com.tfoll.web.model._____Activity___Standard_Order_Waiting_Of_LenderM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 凑集单失败与成功处理
 * 
 */
public class Timer_gather_money_order_waiting extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_gather_money_order_waiting.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_gather_money_order_waiting.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_gather_money_order_waiting.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_gather_money_order_waiting.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				logger.info("定时任务:" + Timer_gather_money_order_waiting.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_gather_money_order_waiting.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {
		/**
		 * 查询凑集单等待表
		 */
		List<BorrowerBulkStandardGatherMoneyOrderWaitingM> borrower_bulk_standard_gather_money_order_waiting_list = BorrowerBulkStandardGatherMoneyOrderWaitingM.dao.find("select id,user_id,gather_money_order_id,deadline from borrower_bulk_standard_gather_money_order_waiting");// 获取ID不锁表
		if (Utils.isHasData(borrower_bulk_standard_gather_money_order_waiting_list)) {
			for (BorrowerBulkStandardGatherMoneyOrderWaitingM borrower_bulk_standard_gather_money_order_waiting : borrower_bulk_standard_gather_money_order_waiting_list) {
				try {
					doSubTaskFor(borrower_bulk_standard_gather_money_order_waiting);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 处理凑集单等待数据
	 */
	public static void doSubTaskFor(final BorrowerBulkStandardGatherMoneyOrderWaitingM borrower_bulk_standard_gather_money_order_waiting) {
		/**
		 * 需要根据凑集单ID去查询是否凑集满
		 */
		long gather_money_order_id = borrower_bulk_standard_gather_money_order_waiting.getLong("gather_money_order_id");
		Date deadline = borrower_bulk_standard_gather_money_order_waiting.getDate("deadline");
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findById(gather_money_order_id);
		if (borrower_bulk_standard_gather_money_order == null) {
			try {
				borrower_bulk_standard_gather_money_order_waiting.delete();
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
			return;
		}

		/**
		 * 找到截止时间
		 */
		Date now = new Date();
		if (now.before(deadline)) {// 时间未到需要检查是否凑集满
			int remain_share = borrower_bulk_standard_gather_money_order.getInt("remain_share");
			if (remain_share == 0) {
				/**
				 * 如果remain_share == 0则进行凑集成功处理
				 */
				doSubTaskForSuccessHandling(borrower_bulk_standard_gather_money_order_waiting, borrower_bulk_standard_gather_money_order);
				return;
			} else {
				return;
			}

		} else {
			/**
			 * 如果之前在截至时间进行凑集成功处理失败了,但是刚刚到截至时间任然需要对这个单子进行后续的处理
			 */
			int gather_state = borrower_bulk_standard_gather_money_order.getInt("gather_state");
			/**
			 * gather_state=1 2 3
			 */
			if (gather_state == 1) {
				doSubTaskForFailureHandling(borrower_bulk_standard_gather_money_order_waiting);
				return;
			} else if (gather_state == 2) {
				logger.error("bug:在等待表里面等待记录所对应的凑集单还有gather_state == 2");
				throw new RuntimeException("bug:在等待表里面等待记录所对应的凑集单还有gather_state == 2");
			} else if (gather_state == 3) {
				/**
				 * 继续凑集成功后续处理-为什么会出现则一部的原因是凑集处理成功的时候刚刚在凑集单生成后的第七天
				 */
				doSubTaskForSuccessHandling(borrower_bulk_standard_gather_money_order_waiting, borrower_bulk_standard_gather_money_order);
				return;
			} else {
				logger.error("bug:在等待表里面等待记录所对应的凑集单gather_state != 1 2 3,gather_state=" + gather_state);
				throw new RuntimeException("bug:在等待表里面等待记录所对应的凑集单gather_state != 1 2 3,gather_state=" + gather_state);
			}

		}

	}

	/**
	 * 失败处理
	 * 
	 * @param borrower_bulk_standard_gather_money_order_waiting
	 */
	private static void doSubTaskForFailureHandling(final BorrowerBulkStandardGatherMoneyOrderWaitingM borrower_bulk_standard_gather_money_order_waiting) {
		/**
		 * <pre>
		 * call procedure_gather_money_order_waiting_failure_handling(?)
		 * 参数说明
		 * @param  1  gather_money_order_waiting_id                      临时表Id-需要删除掉
		 * </pre>
		 */
		final long gather_money_order_waiting_id = borrower_bulk_standard_gather_money_order_waiting.getLong("id");
		Map<Integer, Object> in = new HashMap<Integer, Object>();
		in.put(1, gather_money_order_waiting_id);
		Map<Integer, Integer> out = new HashMap<Integer, Integer>();
		out.put(2, Types.INTEGER);// 出参数位置和类型
		Map<Integer, Object> map = ProcedureCall.call("{call procedure_gather_money_order_waiting_failure_handling(?,?)}", in, out);
		logger.error("凑集单等待表ID:" + gather_money_order_waiting_id + "    " + "返回值:" + map.get(2));
		Integer is_ok = (Integer) map.get(2);
		if (!(is_ok != null && is_ok.intValue() == 5)) {
			logger.error("凑集单等待表ID:" + gather_money_order_waiting_id + "    " + "返回值:" + map.get(2) + " not=5,原因是调用的存储过程里面的SQL语句错误");
			return;
		}

		final long gather_money_order_id = borrower_bulk_standard_gather_money_order_waiting.getLong("gather_money_order_id");
		final int user_id = borrower_bulk_standard_gather_money_order_waiting.getInt("user_id");
		boolean is_ok_chang_user_loan_info = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT id,borrow_type from borrower_bulk_standard_gather_money_order WHERE id=?", new Object[] { gather_money_order_id });
				UserM user_of_borrower = UserM.getUserM(user_id);
				/**
				 * 设置借款人的借贷状态为0
				 */
				boolean is_ok_update_user_of_borrower = user_of_borrower.set("borrow_type", 0).update();
				if (!is_ok_update_user_of_borrower) {
					return false;
				}
				int borrow_type = borrower_bulk_standard_gather_money_order.getInt("borrow_type");
				if (borrow_type == 3) {
					/**
					 * 如果借贷类型是净值贷则需要把借款人的user_now_money表里面的关于净值贷的两个字段数据变为0
					 */
					UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money_for_select(user_id);
					boolean is_ok_update_user_now_money_of_borrower = user_now_money_of_borrower.set("net_amount", new BigDecimal(0)).set("total_net_cash", new BigDecimal(0)).update();
					if (!is_ok_update_user_now_money_of_borrower) {
						return false;
					}
				}
				return true;
			}
		});

		/**
		 * 需要添加失败通知信息
		 */
		if (!is_ok_chang_user_loan_info) {
			SysErrorInfoM sys_error_info = new SysErrorInfoM();
			sys_error_info.set("is_deal", 0).set("detail", "user_id为" + user_id + "的凑集单[id]:" + gather_money_order_waiting_id + "凑集失败,修改用户借贷状态或者净值贷信息记录失败,可能导致用户资金体现失败").save();
		}
		return;

	}

	/**
	 * 该方法既要做到数据执行下去又要做到数据恢复的功能
	 */
	private static void doSubTaskForSuccessHandling(final BorrowerBulkStandardGatherMoneyOrderWaitingM borrower_bulk_standard_gather_money_order_waiting, final BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order) {
		/**
		 * <pre>
		 * 凑集成功变化表
		 * gather_state=1 gather_success_subsequent_process=0-临时表的数据不能删除
		 * gather_state=2 gather_success_subsequent_process= {1表示完成资金交换处理,2完成还款计划处理,3完成持仓和持仓记录处理-临时表数据需要删除,默认0}
		 * </pre>
		 */
		final long gather_money_order_waiting_id = borrower_bulk_standard_gather_money_order_waiting.getLong("id");
		final int user_id = borrower_bulk_standard_gather_money_order_waiting.getInt("user_id");// 借款人用户ID
		final long gather_money_order_id = borrower_bulk_standard_gather_money_order_waiting.getLong("gather_money_order_id");
		/**
		 * 从数据库里面查询出凑集单信息进行初始化
		 */
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order_of_init = BorrowerBulkStandardGatherMoneyOrderM.dao.findById(gather_money_order_id);
		int gather_state_of_init = borrower_bulk_standard_gather_money_order_of_init.getInt("gather_state");
		int gather_success_subsequent_process_of_init = borrower_bulk_standard_gather_money_order_of_init.getInt("gather_success_subsequent_process");

		/**
		 * 凑集单等待功能模块只处理gather_state=1 or 3两种状态
		 */
		if (!(gather_state_of_init == 1 || gather_state_of_init == 3)) {
			logger.error("非法凑集单状态 不是1 3 而是" + gather_state_of_init);
			return;
		}
		/**
		 * 该属性表示是否从数据库里面重新加载凑集单的数据,是否在这之前进行了其他的业务操作
		 */
		boolean is_deal_other_business = false;
		/**
		 * <pre>
		 * ====================================================================================================================================
		 * 进行资金清算
		 * ====================================================================================================================================
		 * </pre>
		 */
		if (gather_state_of_init == 1) {
			if (gather_success_subsequent_process_of_init != 0) {
				logger.error("gather_state==1 and gather_success_subsequent_process=" + gather_success_subsequent_process_of_init);
				return;
			}

			/**
			 * <pre>
			 * call procedure_gather_money_order_waiting_failure_handling(?)
			 * 参数说明
			 * @param  1  gather_money_order_waiting_id                      临时表Id-需要保留,只能在所有的流程都执行成功的时候才能进行删除
			 * </pre>
			 */
			/**
			 * 资金没有完成资金交换处理
			 */
			Map<Integer, Object> in = new HashMap<Integer, Object>();
			in.put(1, gather_money_order_waiting_id);
			Map<Integer, Integer> out = new HashMap<Integer, Integer>();
			out.put(2, Types.INTEGER);// 出参数位置和类型
			/**
			 * 临时表的数据是不能删除的,需要保证里面的流程是可恢复的
			 */
			Map<Integer, Object> map = ProcedureCall.call("{call procedure_gather_money_order_waiting_success_handling(?,?)}", in, out);
			logger.info("凑集单等待表ID:" + gather_money_order_waiting_id + "    " + "返回值:" + map.get(2));
			Integer result = (Integer) map.get(2);
			if (!(result != null && (Integer) result == 5)) {// 正确还回值是5
				logger.error("凑集单等待表ID:" + gather_money_order_waiting_id + "    gather_money_order_id=" + gather_money_order_id + "返回值:" + map.get(2));
				return;
			} else {
				is_deal_other_business = true;
			}

		}

		/**
		 * 根据上面是否处理了决定是否重新加载数据,如果加载成功则把is_deal_other_business设置为false;
		 */
		if (is_deal_other_business) {
			borrower_bulk_standard_gather_money_order_of_init = BorrowerBulkStandardGatherMoneyOrderM.dao.findById(gather_money_order_id);
			gather_state_of_init = borrower_bulk_standard_gather_money_order_of_init.getInt("gather_state");
			gather_success_subsequent_process_of_init = borrower_bulk_standard_gather_money_order_of_init.getInt("gather_success_subsequent_process");
			is_deal_other_business = false;
		}
		if (gather_state_of_init == 2) {
			logger.error("凑集单等待表ID:" + gather_money_order_waiting_id + "    gather_money_order_id=" + gather_money_order_id + "gather_state=" + gather_state_of_init);
			return;
		}

		if (gather_state_of_init == 3) {
			/**
			 * <pre>
			 * ====================================================================================================================================
			 * 进行创建还款计划
			 * ====================================================================================================================================
			 * </pre>
			 */
			if (gather_success_subsequent_process_of_init == 1) {
				final BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order_of_money_exchange_success = borrower_bulk_standard_gather_money_order_of_init;
				boolean is_ok_create_repayment_plan = Db.tx(new IAtomic() {

					public boolean transactionProcessing() throws Exception {
						int borrower_user_id = borrower_bulk_standard_gather_money_order_of_money_exchange_success.getInt("user_id");
						/**
						 * 数量-年化利率-周期
						 */
						BigDecimal borrow_amount = borrower_bulk_standard_gather_money_order_of_money_exchange_success.getBigDecimal("borrow_all_money");
						BigDecimal annual_interest_rate = borrower_bulk_standard_gather_money_order_of_money_exchange_success.getBigDecimal("annulized_rate");
						int borrow_duration = borrower_bulk_standard_gather_money_order_of_money_exchange_success.getInt("borrow_duration");
						// ###创建还款计划
						boolean is_ok_create_repayment_plan = false;
						if (borrow_duration > 0) {// 一个月也表示还款的周期为1
							is_ok_create_repayment_plan = BorrowerBulkStandardRepaymentPlanM.create_repayment_plan(gather_money_order_id, borrower_user_id, borrow_amount, annual_interest_rate, borrow_duration);
						} else {
							int borrow_duration_day = borrower_bulk_standard_gather_money_order_of_money_exchange_success.getInt("borrow_duration_day");
							is_ok_create_repayment_plan = BorrowerBulkStandardRepaymentPlanM.create_repayment_plan_of_current_month(gather_money_order_id, borrower_user_id, borrow_amount, annual_interest_rate, borrow_duration_day);
						}
						if (!is_ok_create_repayment_plan) {
							return false;
						}
						// ###更新gather_success_subsequent_process状态
						boolean is_ok_update_gather_success_subsequent_process_of_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order_of_money_exchange_success.set("gather_success_subsequent_process", 2).set("payment_state", 2).update();
						if (!is_ok_update_gather_success_subsequent_process_of_borrower_bulk_standard_gather_money_order) {
							return false;
						}

						return true;
					}
				});
				if (!is_ok_create_repayment_plan) {
					logger.error("凑集单等待表ID:" + gather_money_order_waiting_id + "    gather_money_order_id=" + gather_money_order_id + "创建凑集计划失败");
					return;
				} else {
					is_deal_other_business = true;
				}

			}
			/**
			 * 根据上面是否处理了决定是否重新加载数据,如果加载成功则把is_deal_other_business设置为false;
			 */
			if (is_deal_other_business) {
				borrower_bulk_standard_gather_money_order_of_init = BorrowerBulkStandardGatherMoneyOrderM.dao.findById(gather_money_order_id);
				gather_state_of_init = borrower_bulk_standard_gather_money_order_of_init.getInt("gather_state");
				gather_success_subsequent_process_of_init = borrower_bulk_standard_gather_money_order_of_init.getInt("gather_success_subsequent_process");
				is_deal_other_business = false;
			}
			if (gather_success_subsequent_process_of_init == 2) {

				final BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order_of_complete_repayment_plan = borrower_bulk_standard_gather_money_order_of_init;
				/**
				 * 散标理财人订单ID
				 */
				final List<LenderBulkStandardOrderM> lender_bulk_standard_order_list = LenderBulkStandardOrderM.dao.find("SELECT id,gather_money_order_id,lend_user_id,borrow_user_id,invest_money,invest_share,bid_time,bid_time_long from lender_bulk_standard_order WHERE gather_money_order_id=?", new Object[] { gather_money_order_id });
				if (!Utils.isHasData(lender_bulk_standard_order_list)) {
					logger.error("lender_bulk_standard_order_list is null according to gather_money_order_id:" + gather_money_order_id);
					return;
				}
				/**
				 * 创建持仓和持仓记录
				 */
				boolean is_ok_create_right_hold = Db.tx(new IAtomic() {

					public boolean transactionProcessing() throws Exception {

						Map<String, LenderBulkStandardCreditorRightHoldM> lender_bulk_standard_creditor_right_hold_map = new HashMap<String, LenderBulkStandardCreditorRightHoldM>();

						for (LenderBulkStandardOrderM lender_bulk_standard_order : lender_bulk_standard_order_list) {
							// gather_money_order_id
							int lend_user_id = lender_bulk_standard_order.getInt("lend_user_id");

							int borrow_user_id = lender_bulk_standard_order.getInt("borrow_user_id");
							BigDecimal invest_money = lender_bulk_standard_order.getBigDecimal("invest_money");
							int invest_share = lender_bulk_standard_order.getInt("invest_share");
							/**
							 * <pre>
							 * 为每个理财用户建立债权持仓-注意这个是通过理财凑集而来的不是通过债权转让而来的<br/>
							 * 注意不能针对某个订单进行一次资金清算,因为所有订单处于一个事务里面，而我们是对所有的用户的资金信息进行加锁处理，如果一个人对一个标投了多次就会出现[数据未保存丢失现象]
							 * </pre>
							 */
							String key = gather_money_order_id + "_" + lend_user_id;

							LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold = lender_bulk_standard_creditor_right_hold_map.get(key);
							if (lender_bulk_standard_creditor_right_hold == null) {
								/**
								 * <pre>
								 * CREATE TABLE `lender_bulk_standard_creditor_right_hold` (
								 * `id` bigint(20) NOT NULL AUTO_INCREMENT,
								 * `user_id` int(11) DEFAULT NULL COMMENT '债权持有人id',
								 * `gather_money_order_id` bigint(20) DEFAULT NULL COMMENT '筹集单ID-该ID用来进行还款',
								 * `gather_money_order_user_id_borrower` int(11) DEFAULT NULL COMMENT 'gather_money_order_id对应的借款用户ID',
								 * ###
								 * `hold_money` decimal(10,0) DEFAULT '0' COMMENT '持有金额',
								 * `hold_share` int(11) DEFAULT NULL COMMENT '持有份额',
								 * `transfer_money` decimal(10,0) DEFAULT '0' COMMENT '转出金额-冻结',
								 * `transfer_share` int(11) DEFAULT NULL COMMENT '转出份额-冻结',
								 * ###
								 *  XX-`is_transfer_all` int(1) DEFAULT '0' COMMENT '该字段不再使用-删除掉',
								 * `add_time` timestamp NULL DEFAULT NULL COMMENT '开始持有债权时间',
								 * `add_time_long` bigint(20) DEFAULT NULL,
								 * ###
								 * `is_need_handle` int(1) DEFAULT '1' COMMENT '是否需要处理默认是1，当不再进行处理的时候-也就是借款人全部还款或系统为借款人因为严重逾期给理财人还款才会变为0:那么在因为严重逾期处理的时候需要把每个债权持有的is_need_handle都变为0，或者借款人提前还款【还款给理财人而不是系统】或者单独还款的时候,所有期都还完了的时候',
								 * PRIMARY KEY (`id`)
								 * ) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8 COMMENT='精英标债权持有表-动态标-里面的数据会变的-当债权份数为0或者';
								 * </pre>
								 */

								lender_bulk_standard_creditor_right_hold = new LenderBulkStandardCreditorRightHoldM();
								lender_bulk_standard_creditor_right_hold.//
										// id
										set("user_id", lend_user_id).// ###
										set("gather_money_order_id", gather_money_order_id).// ###
										set("gather_money_order_user_id_borrower", borrow_user_id).//
										// ###
										set("hold_money", invest_money).//
										set("hold_share", invest_share).//
										set("transfer_money", new BigDecimal("0")).//
										set("transfer_share", 0).//
										// set("is_transfer_all",
										// 0).//该字段不再使用-删除掉
										//
										set("add_time", new Date()).//
										set("add_time_long", (new Date()).getTime()).//
										//
										set("is_need_handle", 1);
								lender_bulk_standard_creditor_right_hold_map.put(key, lender_bulk_standard_creditor_right_hold);

							} else {

								BigDecimal hold_money = lender_bulk_standard_creditor_right_hold.getBigDecimal("hold_money");
								int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share");
								/**
								 * 存在则修改
								 */
								lender_bulk_standard_creditor_right_hold.//
										set("hold_money", hold_money.add(invest_money)).//
										set("hold_share", hold_share + invest_share);//

								lender_bulk_standard_creditor_right_hold_map.put(key, lender_bulk_standard_creditor_right_hold);
							}

						}

						/**
						 * 批量保存持有信息
						 */
						for (LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold : lender_bulk_standard_creditor_right_hold_map.values()) {
							boolean is_ok_add_lender_bulk_standard_creditor_right_hold = lender_bulk_standard_creditor_right_hold.save();
							if (!is_ok_add_lender_bulk_standard_creditor_right_hold) {
								return false;

							}
						}
						for (LenderBulkStandardOrderM lender_bulk_standard_order : lender_bulk_standard_order_list) {
							long order_id = lender_bulk_standard_order.getLong("id");
							int lend_user_id = lender_bulk_standard_order.getInt("lend_user_id");
							int borrow_user_id = lender_bulk_standard_order.getInt("borrow_user_id");
							BigDecimal invest_money = lender_bulk_standard_order.getBigDecimal("invest_money");
							int invest_share = lender_bulk_standard_order.getInt("invest_share");

							/**
							 * 持仓历史记录-可以从订单记录和债权转入的记录进行获取-如果把这两个揉在一起那么就会出现
							 */
							// 每一个订单则创建持有记录
							LenderBulkStandardCreditorRightHoldLogM lender_bulk_standard_creditor_right_hold_log = new LenderBulkStandardCreditorRightHoldLogM();
							/**
							 * <pre>
							 * CREATE TABLE `lender_bulk_standard_creditor_right_hold_log` (
							 *   `gather_money_order_id` bigint(20) DEFAULT NULL COMMENT '凑集单ID-该ID用来进行还款',
							 *   `gather_money_order_user_id_borrower` int(11) DEFAULT NULL COMMENT 'gather_money_order_id对应的借款用户ID',
							 *   //
							 *   `id` bigint(20) NOT NULL AUTO_INCREMENT,
							 *   `user_id` int(20) DEFAULT NULL COMMENT '持有者id',
							 *  ###
							 *   `creditor_right_way` int(11) DEFAULT '0' COMMENT '债权来源方式 0.购买精英标获得 1.转入债权,2转出债权',
							 *   `order_id` bigint(20) DEFAULT '0' COMMENT '理财人订单表-with凑集单表',
							 *   `creditor_right_hold_from_id` bigint(20) DEFAULT '0' COMMENT '债权来源id（本表的id）,如果是直接从散标理财得来的默认是0,通过债权转让则不是0',
							 *   `creditor_right_hold_from_user_id` int(11) DEFAULT '0' COMMENT '上一个债权持有用户ID-如果通过理财投标来的那个叫借贷用户的ID0，如果是债权转让那么就是上一个债权转让者的ID不为0',
							 *   ###
							 *   `hold_money` decimal(10,0) DEFAULT '0' COMMENT '持有金额',
							 *   `hold_share` int(11) DEFAULT NULL COMMENT '持有份额',
							 *   `hold_start_time` timestamp NULL DEFAULT NULL COMMENT '开始持有债权时间',
							 *   `hold_start_time_long` bigint(20) DEFAULT NULL,
							 *   ###
							 *   `transfer_money` decimal(10,0) DEFAULT '0' COMMENT '转入转出金额 正数是转入负数是转出.通过散标投标的不涉及转入转出，通过债权转让的涉及转入转入转出',
							 *   `transfer_share` int(11) DEFAULT NULL COMMENT '转入转出份额 正数是转入,负数是转出.通过散标投标的不涉及转入转出，通过债权转让的涉及转入+转出-',
							 * XX==`is_transfer_all` int(255) DEFAULT '0' COMMENT '这个字段不使用-以后会删掉的',
							 *   PRIMARY KEY (`id`)
							 * ) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8 COMMENT='精英标债权持有变化表';
							 * 
							 * 
							 * </pre>
							 */

							boolean is_ok_save_lender_bulk_standard_creditor_right_hold_log = lender_bulk_standard_creditor_right_hold_log.//
									/**
									 * 借款信息
									 */
									set("gather_money_order_id", gather_money_order_id).//
									set("gather_money_order_user_id_borrower", borrow_user_id).//
									// id
									set("user_id", lend_user_id).//
									/**
									 * 该持有是从那来的-两种方式
									 */
									set("creditor_right_way", 0).//
									set("order_id", order_id).//
									// 该字段不再使用-
									// set("gather_money_order_user_id_lender",
									// lend_user_id).
									set("creditor_right_hold_from_id", 0).//
									set("creditor_right_hold_from_user_id", 0).//
									/**
									 * 持有状态
									 */
									set("hold_money", invest_money).//
									set("hold_share", invest_share).//
									set("hold_start_time", new Date()).//
									set("hold_start_time_long", (new Date()).getTime()).//
									/**
									 * 转让状态
									 */
									//
									set("transfer_money", new BigDecimal("0")).//
									set("transfer_share", 0).//
									// 该字段不再使用 -set("is_transfer_all", 0).
									save();
							if (!is_ok_save_lender_bulk_standard_creditor_right_hold_log) {
								return false;
							}

						}
						boolean is_ok_update_gather_success_subsequent_process_of_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order_of_complete_repayment_plan.set("gather_success_subsequent_process", 3).update();
						if (!is_ok_update_gather_success_subsequent_process_of_borrower_bulk_standard_gather_money_order) {
							return false;
						}
						boolean is_ok_delete_gather_money_order_waiting = Db.update("DELETE FROM borrower_bulk_standard_gather_money_order_waiting where id=?", new Object[] { gather_money_order_waiting_id }) >= 1;// 正常的情况是还回的记录数是1
						if (!is_ok_delete_gather_money_order_waiting) {
							return false;
						}
						/**
						 * 更新还款状态
						 */
						boolean is_ok_update_gather_money_order = Db.update("UPDATE borrower_bulk_standard_gather_money_order set payment_state=2 WHERE id=? ", new Object[] { gather_money_order_id }) >= 1;// 正常的情况是还回的记录数是1
						if (!is_ok_update_gather_money_order) {
							return false;
						}
						return true;
					}
				});
				if (!is_ok_create_right_hold) {
					logger.error("凑集单等待表ID:" + gather_money_order_waiting_id + "    gather_money_order_id=" + gather_money_order_id + " 凑集成功创建持仓失败");
					return;

				} else {
					/**
					 * 把凑集单的ID写入活动1表的等待处理表，只进行推荐人添加奖励处理,
					 * 把理财单的ID添加到散标理财活动和推荐人活动等待处理
					 */
					boolean is_ok_add______activity___standard_order_waiting = Db.tx(new IAtomic() {

						public boolean transactionProcessing() throws Exception {
							// 添加凑集单ID
							_____Activity___Standard_Order_Waiting_Of_BorrowerM _____activity___standard_order_waiting_of_borrower = new _____Activity___Standard_Order_Waiting_Of_BorrowerM();
							boolean is_ok_add______activity___standard_order_waiting_of_borrower = _____activity___standard_order_waiting_of_borrower.set("user_id", user_id).set("gather_money_order_id", gather_money_order_id).set("add_time_long", System.currentTimeMillis()).save();

							if (!is_ok_add______activity___standard_order_waiting_of_borrower) {
								return false;
							}
							// 添加理财订单ID
							for (LenderBulkStandardOrderM lender_bulk_standard_order : lender_bulk_standard_order_list) {
								int lend_user_id = lender_bulk_standard_order.getInt("lend_user_id");
								long order_id = lender_bulk_standard_order.getLong("id");
								// gather_money_order_id
								_____Activity___Standard_Order_Waiting_Of_LenderM _____activity___standard_order_waiting_of_lender = new _____Activity___Standard_Order_Waiting_Of_LenderM();
								boolean is_ok_add______activity___standard_order_waiting_of_lender = _____activity___standard_order_waiting_of_lender.set("user_id", lend_user_id).set("order_id", order_id).set("add_time_long", System.currentTimeMillis()).save();

								if (!is_ok_add______activity___standard_order_waiting_of_lender) {
									return false;
								}

							}
							return true;
						}
					});
					/**
					 * 如果没有处理则需要把这些信息通过通知告诉系统,该相关的理财和借贷ID没有添加到等待表里面
					 */
					if (!is_ok_add______activity___standard_order_waiting) {
						SysErrorInfoWaitingForDealingM sys_error_info_waiting_for_dealing = new SysErrorInfoWaitingForDealingM();
						boolean is_ok_add_0sys_error_info_waiting_for_dealing = sys_error_info_waiting_for_dealing.set("detail", "凑集单ID为" + gather_money_order_id + "添加推荐活动和理财活动(1,2)进入等待处理表失败").set("add_time_long", System.currentTimeMillis()).save();
						if (!is_ok_add_0sys_error_info_waiting_for_dealing) {
							logger.error("凑集单ID为" + gather_money_order_id + "添加推荐活动和理财活动(1,2)进入等待处理表失败");
						}
					}
				}
			}
			if (gather_success_subsequent_process_of_init == 3) {
				logger.error("凑集单等待表ID:" + gather_money_order_waiting_id + "    gather_money_order_id=" + gather_money_order_id + " 凑集成功处理状态不可能为3");
				return;

			}

		}

	}

}