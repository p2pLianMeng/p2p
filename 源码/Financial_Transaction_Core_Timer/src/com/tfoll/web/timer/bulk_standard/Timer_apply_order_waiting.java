package com.tfoll.web.timer.bulk_standard;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderWaitingM;
import com.tfoll.web.util.Utils;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 申请单审核等待处理
 * 
 */
public class Timer_apply_order_waiting extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_apply_order_waiting.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_apply_order_waiting.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_apply_order_waiting.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_apply_order_waiting.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();

					}
				}
				logger.info("定时任务:" + Timer_apply_order_waiting.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_apply_order_waiting.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {
		/**
		 * 查询申请单等待表
		 */
		List<BorrowerBulkStandardApplyOrderWaitingM> borrower_bulk_standard_apply_order_waiting_list = BorrowerBulkStandardApplyOrderWaitingM.dao.find("select id,apply_order_id from borrower_bulk_standard_apply_order_waiting");
		if (Utils.isHasData(borrower_bulk_standard_apply_order_waiting_list)) {
			for (BorrowerBulkStandardApplyOrderWaitingM borrower_bulk_standard_apply_order_waiting : borrower_bulk_standard_apply_order_waiting_list) {
				try {
					doSubTaskFor(borrower_bulk_standard_apply_order_waiting);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}

	public static final long Three_Hour = Model.Hour * 3;

	/**
	 * 针对每一条等待处理的申请单进行处理
	 */
	private static void doSubTaskFor(final BorrowerBulkStandardApplyOrderWaitingM borrower_bulk_standard_apply_order_waiting) {
		long apply_order_id = borrower_bulk_standard_apply_order_waiting.getLong("apply_order_id");
		/**
		 * 首先到申请表查询这个申请单存在与否
		 */
		final BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findFirst("SELECT id,state,commit_time from borrower_bulk_standard_apply_order WHERE id=?", new Object[] { apply_order_id });
		if (borrower_bulk_standard_apply_order == null) {
			borrower_bulk_standard_apply_order_waiting.delete();
			return;
		}
		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (state != 2) {// state=2为等待审核
			borrower_bulk_standard_apply_order_waiting.delete();
			return;
		}
		Date commit_time = borrower_bulk_standard_apply_order.getDate("commit_time");
		/**
		 * 判断现在的时间和提交的时间是否相差3天?
		 */
		long now_long = System.currentTimeMillis();
		long commit_time_long = commit_time.getTime();
		if (!(commit_time_long + Three_Hour <= now_long)) {
			/**
			 * 不足三天则不进行处理
			 */
		} else {
			boolean is_ok = Db.tx(new IAtomic() {

				public boolean transactionProcessing() throws Exception {
					boolean is_ok_update_apply_order_state = borrower_bulk_standard_apply_order.set("state", 3).set("commit_time", new Date()).update();// 更新提交时间可以优先得到处理
					if (!is_ok_update_apply_order_state) {
						return false;
					}
					boolean is_ok_update_apply_order_waiting = borrower_bulk_standard_apply_order_waiting.delete();
					if (!is_ok_update_apply_order_waiting) {
						return false;
					}
					return true;
				}
			});
			if (!is_ok) {
				logger.error("borrower_bulk_standard_apply_order-id:" + apply_order_id + "三个小时自动变为[正在审核处理]状态失败");
			}
			return;

		}

	}

}