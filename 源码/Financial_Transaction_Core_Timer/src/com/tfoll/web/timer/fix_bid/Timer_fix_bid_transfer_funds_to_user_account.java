package com.tfoll.web.timer.fix_bid;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.model.FixBidTransferFundsToUserAccountM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 联富宝资金自动转移到系统指定的三个理财账户
 * 
 */
public class Timer_fix_bid_transfer_funds_to_user_account extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_fix_bid_transfer_funds_to_user_account.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_fix_bid_transfer_funds_to_user_account.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_fix_bid_transfer_funds_to_user_account.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_fix_bid_transfer_funds_to_user_account.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				logger.info("定时任务:" + Timer_fix_bid_transfer_funds_to_user_account.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_fix_bid_transfer_funds_to_user_account.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	/**
	 * 联富宝资金转移业务介绍
	 * 
	 * <pre>
	 * 业务处理的时间段为联富宝[预定支付截至时间]到[锁定开始时间十分钟]
	 * 
	 * 分成两个阶段进行处理:如果在支付截至时间满了80%,则把当时整个的80%转移到对应的理财账户里面, 然后锁定的时候把剩下的钱打给系统指定的理财的用户[0->1->2],否在在锁定的时候才把所有的钱打给系统指定的理财的用户[0-3]
	 * </pre>
	 * 
	 * <pre>
	 * 默认的  预约时间是周一早上十点,支付截至时间是星期三早上两点,开放结束的时间是周六早上十点
	 * </pre>
	 */
	public static void doTask() throws Exception {
		/**
		 * 获取一周开始时间和结束时间的毫秒数
		 */
		Date now = new Date();
		long first_day_of_week = get_first_day_of_week(now).getTime();
		long last_day_evening_of_week = get_last_day_evening_of_week(now).getTime();
		/**
		 * SQL意思是 支付截至时间是大于一周的周一，锁定的截至时间小于周末的最后一刻
		 */
		List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(//
				"SELECT id,bid_name,total_money,sold_money,closed_period,pay_end_time,invite_end_time,is_deduct_if_not_pay from fix_bid_system_order " + //
						"WHERE pay_end_time_long>=? and invite_end_time_long<? order by closed_period asc ", //
				new Object[] { first_day_of_week, last_day_evening_of_week });//
		if (Utils.isHasData(fix_bid_system_order_list)) {
			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				try {
					doSubTaskFor(fix_bid_system_order);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}

	}

	public static final BigDecimal $80 = new BigDecimal("0.8");

	private static void doSubTaskFor(FixBidSystemOrderM fix_bid_system_order) {
		long id = fix_bid_system_order.getLong("id");
		String bid_name = fix_bid_system_order.getString("bid_name");
		int total_money = fix_bid_system_order.getInt("total_money");
		final int sold_money = fix_bid_system_order.getInt("sold_money");
		int closed_period = fix_bid_system_order.getInt("closed_period");
		/**
		 * <code>
		 * is_deduct_if_not_pay 该字段很重要。说明只有系统已经对预定未支付的那部分的资金进行划扣后才能变为1,参见[针对联富包预定未支付处理 com.tfoll.web.timer.fix_bid.Timer_fix_bid_fails_pay_handle]这个类
		 * </code>
		 */
		int is_deduct_if_not_pay = fix_bid_system_order.getInt("is_deduct_if_not_pay");
		if (is_deduct_if_not_pay == 0) {
			return;// 等待针对预定未支付的部分进行资金划扣
		}
		int transfer_to_user_id = 0;
		switch (closed_period) {
		// 月
		case 3: {
			transfer_to_user_id = 1;
			break;
		}
		case 6: {
			transfer_to_user_id = 2;
			break;
		}
		case 12: {
			transfer_to_user_id = 3;
			break;
		}
		default:
			throw new RuntimeException("closed_period is not 3 6 12");
		}
		FixBidTransferFundsToUserAccountM fix_bid_transfer_funds_to_user_account_from_db = FixBidTransferFundsToUserAccountM.dao.findById(id);
		if (fix_bid_transfer_funds_to_user_account_from_db == null) {
			// 不存在则创建
			fix_bid_transfer_funds_to_user_account_from_db = new FixBidTransferFundsToUserAccountM();
			fix_bid_transfer_funds_to_user_account_from_db.//
					set("system_order_id", id).//
					set("bid_name", bid_name).//
					set("closed_period", closed_period).//
					// 中间的时间信息不需要了
					set("transfer_to_user_id", transfer_to_user_id).// ]
					set("transfer_state", 0).//
					set("add_time_long", new Date().getTime()).//
					save();
		}

		if (fix_bid_transfer_funds_to_user_account_from_db == null) {
			return;
		}

		final FixBidTransferFundsToUserAccountM fix_bid_transfer_funds_to_user_account = fix_bid_transfer_funds_to_user_account_from_db;
		int transfer_state = fix_bid_transfer_funds_to_user_account.getInt("transfer_state");
		Date now = new Date();
		// 支付截止时间
		Date pay_end_time = fix_bid_system_order.getDate("pay_end_time");
		// 投标结束时间
		Date invite_end_time = fix_bid_system_order.getDate("invite_end_time");

		/**
		 * <pre>
		 * 下面是针对上面的两个的时间点进行处理  转移状态[1-80%,2-left20%],3all
		 * </pre>
		 */
		/**
		 * 判断当前的时间是位于(支付截至时间-锁定开始时间)，还是[锁定开始时间...)
		 */
		final int user_id = transfer_to_user_id;// 系统指定的专用的联富宝转钱的用户的ID,该三个账户是不能提现的
		// 第一种情况:刚刚支付截至后
		// 在这期间只能存在01两个状态值
		if (pay_end_time.before(now) && now.before(invite_end_time)) {

			if (transfer_state == 0) {
				final BigDecimal $80_of_all = new BigDecimal(total_money + "").multiply($80);
				if ($80_of_all.compareTo(new BigDecimal(sold_money + "")) <= 0) {// 判断资金达到了80%?
					/**
					 * 整个的80%转过去
					 */
					Db.tx(new IAtomic() {

						public boolean transactionProcessing() throws Exception {

							boolean is_ok_update_fix_bid_transfer_funds_to_user_account = fix_bid_transfer_funds_to_user_account.set("money_80_before", $80_of_all).set("money_80_before_time", new Date()).set("transfer_state", 1).update();
							if (!is_ok_update_fix_bid_transfer_funds_to_user_account) {
								return false;
							}
							/**
							 * <pre>
							 * 锁表会造成系统性能很低的-所以放在修改[fix_bid_transfer_funds_to_user_account]记录之后
							 * </pre>
							 */
							UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
							if (user_now_money == null) {
								return false;
							}
							BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
							cny_can_used = cny_can_used.add($80_of_all);// 只是80%
							boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used).update();
							if (!is_ok_update_user_now_money) {
								return false;
							}
							return true;
						}
					});
					return;
				} else {
					// 资金不够则不进行处理
					return;
				}
			} else if (transfer_state == 1) {// 只允许0 1
				// 需要等待时间到达锁定时间transfer_state才能变成2
				return;
			} else {
				logger.error("在支付截止时间到开放加入截至时间内不允许出现:transfer_state =" + transfer_state);
				throw new RuntimeException("在支付截止时间到开放加入截至时间内不允许出现:transfer_state =" + transfer_state);
			}

		} else if (now.compareTo(invite_end_time) >= 0) {
			// 判断当前时间是不是锁定开始时间-如果是则把剩下的钱转到特定的账户里面
			if (transfer_state == 0) {// 0->3 or 1->2
				/**
				 * 钱全部转过去
				 */
				Db.tx(new IAtomic() {

					public boolean transactionProcessing() throws Exception {

						boolean is_ok_update_fix_bid_transfer_funds_to_user_account = fix_bid_transfer_funds_to_user_account.set("money_80_all", sold_money).set("money_80_all_time", new Date()).set("transfer_state", 3).update();
						if (!is_ok_update_fix_bid_transfer_funds_to_user_account) {
							return false;
						}
						UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
						if (user_now_money == null) {
							return false;
						}
						BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
						cny_can_used = cny_can_used.add(new BigDecimal(sold_money + ""));
						boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used).update();
						if (!is_ok_update_user_now_money) {
							return false;
						}
						return true;
					}
				});

				return;
			} else if (transfer_state == 1) {
				/**
				 * 把剩下的钱全部转过去
				 */
				Db.tx(new IAtomic() {

					public boolean transactionProcessing() throws Exception {
						BigDecimal money_80_before = fix_bid_transfer_funds_to_user_account.getBigDecimal("money_80_before");
						// 减去以前的80%
						BigDecimal $ = new BigDecimal(sold_money + "").subtract(money_80_before);
						boolean is_ok_update_fix_bid_transfer_funds_to_user_account = fix_bid_transfer_funds_to_user_account.set("money_80_after", sold_money).set("money_80_after_add", $).set("money_80_after_time", new Date()).set("transfer_state", 2).update();
						if (!is_ok_update_fix_bid_transfer_funds_to_user_account) {
							return false;
						}
						UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
						if (user_now_money == null) {
							return false;
						}
						// add $
						BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
						cny_can_used = cny_can_used.add($);
						boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used).update();
						if (!is_ok_update_user_now_money) {
							return false;
						}

						return true;
					}
				});

				return;
			} else if (transfer_state == 2) {
				// 处理OK
				return;
			} else if (transfer_state == 3) {
				// 处理OK
				return;
			}
		} else {
			logger.debug("非业务指定处理时间");
			return;
		}
	}

	/**
	 * 获得周一早上日期
	 */
	public static Date get_first_day_of_week(Date now) {
		if (now == null) {
			throw new NullPointerException("now is null");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();

	}

	/**
	 * 当周星期天最后一毫秒
	 */
	public static Date get_last_day_evening_of_week(Date now) {
		if (now == null) {
			throw new NullPointerException("now is null");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date monday = calendar.getTime();
		long monday_long = monday.getTime();
		return new Date(monday_long + Model.Day * 7 - 1);

	}
}