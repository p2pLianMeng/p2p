package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.math.BigDecimal;
import java.util.Date;

@TableBind(tableName = "lender_bulk_standard_creditor_right_hold_repayment_record", primaryKey = "id")
public class LenderBulkStandardCreditorRightHoldRepaymentRecordM extends Model<LenderBulkStandardCreditorRightHoldRepaymentRecordM> {
	public static final LenderBulkStandardCreditorRightHoldRepaymentRecordM dao = new LenderBulkStandardCreditorRightHoldRepaymentRecordM();

	/**
	 * 理财人收款信息
	 * 
	 * <pre>
	 * CREATE TABLE `lender_bulk_standard_creditor_right_hold_repayment_record` (
	 *   `id` bigint(20) NOT NULL AUTO_INCREMENT,
	 *   `lender_user_id` int(11) DEFAULT NULL COMMENT '理财人id',
	 *   `borrower_user_id` int(11) DEFAULT NULL COMMENT 'gather_money_order_id对应的借款用户ID',
	 *   `gather_money_order_id` bigint(20) DEFAULT NULL COMMENT '筹集单ID-该ID用来进行还款',
	 *   `borrow_type` int(2) DEFAULT NULL,
	 *   `borrower_nickname` varchar(10) DEFAULT NULL COMMENT '借款人昵称',
	 *   `repayment_plan_id` bigint(13) DEFAULT NULL,
	 *   `total_periods` int(11) DEFAULT NULL,
	 *   `current_period` int(11) DEFAULT NULL,
	 *   `creditor_right_hold_id` bigint(13) DEFAULT NULL,
	 *   `hold_money` decimal(10,4) DEFAULT '0.0000' COMMENT '持有金额',
	 *   `hold_share` int(11) DEFAULT NULL COMMENT '持有份额',
	 *   `principal` decimal(20,4) DEFAULT NULL COMMENT '收到的本金',
	 *   `interest` decimal(20,4) DEFAULT NULL COMMENT '收到的利息',
	 *   `principal_interest` decimal(20,4) DEFAULT '0.0000' COMMENT '收到的本息',
	 *   `punish_interest` decimal(20,4) DEFAULT NULL COMMENT '逾期罚息',
	 *   `actual_repayment` decimal(20,4) DEFAULT NULL COMMENT '实际还款金额',
	 *   `add_time` timestamp NULL DEFAULT NULL COMMENT '收款时间',
	 *   `add_time_long` bigint(13) DEFAULT NULL,
	 *   PRIMARY KEY (`id`)
	 * ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='精英标债权持有还款记录表';
	 * 
	 * 
	 *</pre>
	 */
	// 新添加了几个字段
	public static boolean add_lender_bulk_standard_creditor_right_hold_repayment_record(//
			int lender_user_id,//
			int borrower_user_id,//
			long gather_money_order_id,//
			int borrow_type,//
			String borrower_nickname,//

			long repayment_plan_id,//
			//
			int total_periods,//
			int current_period,//
			//
			long creditor_right_hold_id,//
			BigDecimal hold_money,//
			int hold_share,//
			//
			BigDecimal principal,//
			BigDecimal interest,//
			BigDecimal principal_interest,//
			BigDecimal punish_interest,//
			BigDecimal recieve_money,//
			Date add_time//

	) {
		LenderBulkStandardCreditorRightHoldRepaymentRecordM lender_bulk_standard_creditor_right_hold_repayment_record = new LenderBulkStandardCreditorRightHoldRepaymentRecordM();

		return lender_bulk_standard_creditor_right_hold_repayment_record.//
				set("lender_user_id", lender_user_id).//
				//
				set("borrower_user_id", borrower_user_id).//
				set("gather_money_order_id", gather_money_order_id).//
				set("borrow_type", borrow_type).//
				set("borrower_nickname", borrower_nickname).//
				//
				set("repayment_plan_id", repayment_plan_id).//
				set("total_periods", total_periods).//
				set("current_period", current_period).//
				//
				set("creditor_right_hold_id", creditor_right_hold_id).//
				set("hold_money", hold_money).// 当时使用的本金
				set("hold_share", hold_share).// 当时持有的份额
				//
				set("principal", principal).//
				set("interest", interest).//
				set("principal_interest", principal_interest).//
				set("punish_interest", punish_interest).//
				set("recieve_money", recieve_money).//
				//
				set("add_time", add_time).//
				set("add_time_long", add_time.getTime()).//
				save();

	}

	public static boolean add_lender_bulk_standard_creditor_right_hold_repayment_record_to_system(//
			int lender_user_id,//
			int borrower_user_id,//
			long gather_money_order_id,//
			int borrow_type,//
			String borrower_nickname,//

			long repayment_plan_id,// #0
			//
			int total_periods,//
			int current_period,// #0
			//
			long creditor_right_hold_id,//
			BigDecimal hold_money,//
			int hold_share,//
			//
			BigDecimal principal,//
			BigDecimal interest,//
			BigDecimal principal_interest,//
			BigDecimal punish_interest,//
			Date add_time//

	) {
		LenderBulkStandardCreditorRightHoldRepaymentRecordM lender_bulk_standard_creditor_right_hold_repayment_record = new LenderBulkStandardCreditorRightHoldRepaymentRecordM();

		return lender_bulk_standard_creditor_right_hold_repayment_record.//
				set("lender_user_id", lender_user_id).//
				//
				set("borrower_user_id", borrower_user_id).//
				set("gather_money_order_id", gather_money_order_id).//
				set("borrow_type", borrow_type).//
				set("borrower_nickname", borrower_nickname).//
				//
				set("repayment_plan_id", repayment_plan_id).//
				set("total_periods", total_periods).//
				set("current_period", current_period).//
				//
				set("creditor_right_hold_id", creditor_right_hold_id).//
				set("hold_money", hold_money).// 当时使用的本金
				set("hold_share", hold_share).// 当时持有的份额
				//
				set("principal", principal).//
				set("interest", interest).//
				set("principal_interest", principal_interest).//
				set("punish_interest", punish_interest).//
				set("add_time", add_time).//
				set("add_time_long", add_time.getTime()).//
				set("pay_by_borrow_or_system", 1).//
				save();

	}
}
