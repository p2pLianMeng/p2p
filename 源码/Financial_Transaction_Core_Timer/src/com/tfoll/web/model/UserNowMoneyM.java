package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_now_money", primaryKey = "user_id")
public class UserNowMoneyM extends Model<UserNowMoneyM> {
	public static UserNowMoneyM dao = new UserNowMoneyM();

	public static UserNowMoneyM get_user_current_money_for_update(int user_id) {
		return UserNowMoneyM.dao.findFirst("SELECT * from user_now_money WHERE user_id =? for UPDATE", new Object[] { user_id });

	}

	public static UserNowMoneyM get_user_current_money_for_select(int user_id) {
		return UserNowMoneyM.dao.findFirst("SELECT * from user_now_money WHERE user_id = ?", new Object[] { user_id });

	}
}
