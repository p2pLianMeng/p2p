package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@TableBind(tableName = "lender_bulk_standard_order_creditor_right_transfer_out", primaryKey = "id")
public class LenderBulkStandardOrderCreditorRightTransferOutM extends Model<LenderBulkStandardOrderCreditorRightTransferOutM> {
	public static LenderBulkStandardOrderCreditorRightTransferOutM dao = new LenderBulkStandardOrderCreditorRightTransferOutM();

	/**
	 * 
	 * 根据id加锁 获取 债权转出单的信息
	 * 
	 */
	private static LenderBulkStandardOrderCreditorRightTransferOutM get_lender_bulk_standard_order_creditor_right_transfer_out_by_id_for_update(Long id) {
		String sql = "select * from lender_bulk_standard_order_creditor_right_transfer_out where id = ? for update";
		LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out = LenderBulkStandardOrderCreditorRightTransferOutM.dao.findFirst(sql, new Object[] { id });
		return lender_bulk_standard_order_creditor_right_transfer_out;
	}

	/**
	 * 多个用户并发访问同一条 转出 单时，
	 * 
	 */
	public static class GetTransferOutOrderByIdTask implements Callable<LenderBulkStandardOrderCreditorRightTransferOutM> {
		private long order_id;

		public GetTransferOutOrderByIdTask(long orderId) {
			super();
			this.order_id = orderId;
		}

		public LenderBulkStandardOrderCreditorRightTransferOutM call() throws Exception {
			return LenderBulkStandardOrderCreditorRightTransferOutM.get_lender_bulk_standard_order_creditor_right_transfer_out_by_id_for_update(this.order_id);
		}
	}

	/**
	 * <pre>
	 * 1 该方法可能由于数据库锁等待超时，会返回为null
	 * 2 该方法只能在原子事务里面调用-否则会死锁
	 * </pre>
	 */
	public static LenderBulkStandardOrderCreditorRightTransferOutM get_lender_bulk_standard_order_creditor_right_transfer_out_short_wait(final long order_id) throws Exception {

		LenderBulkStandardOrderCreditorRightTransferOutM.GetTransferOutOrderByIdTask transfer_out_order_id_task = new LenderBulkStandardOrderCreditorRightTransferOutM.GetTransferOutOrderByIdTask(order_id);
		ExecutorService get_gather_order_task_service = Executors.newFixedThreadPool(1);
		// 对task对象进行各种set操作以初始化任务
		Future<LenderBulkStandardOrderCreditorRightTransferOutM> future = get_gather_order_task_service.submit(transfer_out_order_id_task);
		try {
			return future.get(3, TimeUnit.SECONDS);
		} catch (Exception e) {
			return null;
		} finally {
			if (future.isCancelled()) {
				future.cancel(true);
			}
			get_gather_order_task_service.shutdownNow();
		}
	}

}
