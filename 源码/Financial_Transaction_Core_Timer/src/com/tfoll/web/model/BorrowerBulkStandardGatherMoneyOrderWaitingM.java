package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@TableBind(tableName = "borrower_bulk_standard_gather_money_order_waiting", primaryKey = "id")
public class BorrowerBulkStandardGatherMoneyOrderWaitingM extends Model<BorrowerBulkStandardGatherMoneyOrderWaitingM> {
	public static BorrowerBulkStandardGatherMoneyOrderWaitingM dao = new BorrowerBulkStandardGatherMoneyOrderWaitingM();

	/**
	 * 
	 * 根据id加锁 获取 筹款单的信息
	 * 
	 */
	private static BorrowerBulkStandardGatherMoneyOrderWaitingM get_borrower_bulk_standard_gather_money_order_waiting_by_id_for_update(Long id) {
		String sql = "select * from borrower_bulk_standard_gather_money_order_waiting where id = ? for update";
		BorrowerBulkStandardGatherMoneyOrderWaitingM borrower_bulk_standard_gather_money_order_waiting = BorrowerBulkStandardGatherMoneyOrderWaitingM.dao.findFirst(sql, new Object[] { id });
		return borrower_bulk_standard_gather_money_order_waiting;
	}

	/**
	 * 多个用户并发访问同一条筹款单时，
	 * 
	 */
	public static class GetGatherOrderWaitingByIdTask implements Callable<BorrowerBulkStandardGatherMoneyOrderWaitingM> {
		private long order_waiting_id;

		public GetGatherOrderWaitingByIdTask(long order_waiting_id) {
			super();
			this.order_waiting_id = order_waiting_id;
		}

		public BorrowerBulkStandardGatherMoneyOrderWaitingM call() throws Exception {
			return BorrowerBulkStandardGatherMoneyOrderWaitingM.get_borrower_bulk_standard_gather_money_order_waiting_by_id_for_update(this.order_waiting_id);
		}
	}

	/**
	 * <pre>
	 * 1 该方法可能由于数据库锁等待超时，会返回为null
	 * 2 该方法只能在原子事务里面调用-否则会死锁
	 * </pre>
	 */
	public static BorrowerBulkStandardGatherMoneyOrderWaitingM get_borrower_bulk_standard_gather_money_order_waiting_short_wait(final long order_waiting_id) throws Exception {

		BorrowerBulkStandardGatherMoneyOrderWaitingM.GetGatherOrderWaitingByIdTask get_fix_bid_waiting_task = new BorrowerBulkStandardGatherMoneyOrderWaitingM.GetGatherOrderWaitingByIdTask(order_waiting_id);
		ExecutorService get_gather_order_waiting_task_service = Executors.newFixedThreadPool(1);
		// 对task对象进行各种set操作以初始化任务
		Future<BorrowerBulkStandardGatherMoneyOrderWaitingM> future = get_gather_order_waiting_task_service.submit(get_fix_bid_waiting_task);
		try {
			return future.get(3, TimeUnit.SECONDS);
		} catch (Exception e) {
			return null;
		} finally {
			if (future.isCancelled()) {
				future.cancel(true);
			}
			get_gather_order_waiting_task_service.shutdownNow();
		}

	}

}
