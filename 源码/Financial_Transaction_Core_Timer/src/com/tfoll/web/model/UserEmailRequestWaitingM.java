package com.tfoll.web.model;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.annotation.Function;

@TableBind(tableName = "user_email_request_waiting", primaryKey = "id")
public class UserEmailRequestWaitingM extends Model<UserEmailRequestWaitingM> {
	public static UserEmailRequestWaitingM dao = new UserEmailRequestWaitingM();

	/**
	 * 1用户注册发送验证码
	 */
	@Function(for_people = "所有人", function_description = "用户注册发送验证码", last_update_author = "czh")
	public static boolean add_user_email_request_of_user_register(final String nickname, final String email, final String code) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserEmailRequestLogM user_email_request_log = new UserEmailRequestLogM();
				boolean is_ok_add_user_email_request_log = user_email_request_log.set("user_id", 0).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("email", email).//
						set("nickname", nickname).set("content", code).//
						set("request_type", 1).//
						set("request_type_name", "用户注册发送验证码").//
						save();

				UserEmailRequestWaitingM user_email_request_waiting = new UserEmailRequestWaitingM();
				boolean is_ok_add_user_email_request_waiting = user_email_request_waiting.set("user_id", 0).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("email", email).//
						set("content", code).//
						set("nickname", nickname).set("request_type", 1).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_email_request_log && is_ok_add_user_email_request_waiting;
			}
		});

	}

	/**
	 * 2用户找回密码发送验证码
	 */
	@Function(for_people = "所有人", function_description = "用户找回密码发送验证码", last_update_author = "czh")
	public static boolean add_user_email_request_of_user_back_password(final String email, final String code) {
		UserM user = UserM.dao.findFirst("select * from user_info where email=?", new Object[] { email });
		final int user_id = user.getInt("id");
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserEmailRequestLogM user_email_request_log = new UserEmailRequestLogM();
				boolean is_ok_add_user_email_request_log = user_email_request_log.set("user_id", user_id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("email", email).//
						set("content", code).//
						set("request_type", 2).//
						set("request_type_name", "用户找回密码发送验证码").//
						save();

				UserEmailRequestWaitingM user_email_request_waiting = new UserEmailRequestWaitingM();
				boolean is_ok_add_user_email_request_waiting = user_email_request_waiting.set("user_id", user_id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("email", email).//
						set("content", code).//
						set("request_type", 2).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_email_request_log && is_ok_add_user_email_request_waiting;
			}
		});

	}

	/**
	 * 3用户找回密码发送验证码
	 */
	@Function(for_people = "所有人", function_description = "用户找回密码发送验证码", last_update_author = "czh")
	public static boolean add_user_bingding_email(final int user_id, final String email, final String code, final String nickname) {
		return Db.tx(new IAtomic() {
			public boolean transactionProcessing() throws Exception {
				UserEmailRequestLogM user_email_request_log = new UserEmailRequestLogM();
				boolean is_ok_add_user_email_request_log = user_email_request_log.set("user_id", user_id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("email", email).//
						set("nickname", nickname).set("content", code).//
						set("request_type", 6).//
						set("request_type_name", "用户找回密码发送验证码").//
						save();

				UserEmailRequestWaitingM user_email_request_waiting = new UserEmailRequestWaitingM();
				boolean is_ok_add_user_email_request_waiting = user_email_request_waiting.set("user_id", user_id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("email", email).//
						set("nickname", nickname).set("content", code).//
						set("request_type", 6).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_email_request_log && is_ok_add_user_email_request_waiting;
			}
		});

	}
}
