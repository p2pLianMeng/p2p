package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_creditor_right_hold", primaryKey = "id")
public class LenderBulkStandardCreditorRightHoldM extends Model<LenderBulkStandardCreditorRightHoldM> {
	public static LenderBulkStandardCreditorRightHoldM dao = new LenderBulkStandardCreditorRightHoldM();
}
