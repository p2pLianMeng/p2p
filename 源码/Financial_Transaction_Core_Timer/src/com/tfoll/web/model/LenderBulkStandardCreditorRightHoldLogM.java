package com.tfoll.web.model;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_creditor_right_hold_log", primaryKey = "id")
public class LenderBulkStandardCreditorRightHoldLogM extends Model<LenderBulkStandardCreditorRightHoldLogM> {
	public static LenderBulkStandardCreditorRightHoldLogM dao = new LenderBulkStandardCreditorRightHoldLogM();

	/**
	 * 根据凑集单ID把所有的is_need_handle更新为0，这其中包含了已经债权为0的 <code>is_need_handle</code>
	 * 让所有理财人的持有失效 不再系统的处理流程中进行业务流转
	 * 
	 * <pre>
	 * 是否需要处理默认是1，当不再进行处理的时候-也就是借款人全部还款或系统为借款人因为严重逾期给理财人还款才会变为0:那么在因为严重逾期处理的时候需要把每个债权持有的is_need_handle都变为0，或者借款人提前还款【还款给理财人而不是系统】或者单独还款的时候,所有期都还完了的时候
	 *</pre>
	 */
	public static boolean set_all_is_not_need_handle(long gather_money_order_id) {
		return Db.update("UPDATE lender_bulk_standard_creditor_right_hold set is_need_handle=0 WHERE gather_money_order_id=?", new Object[] { gather_money_order_id }) >= 0;
	}
}
