package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.web.domain.RepaymentPlan;
import com.tfoll.web.util.CalculationFormula;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@TableBind(tableName = "borrower_bulk_standard_repayment_plan", primaryKey = "id")
public class BorrowerBulkStandardRepaymentPlanM extends Model<BorrowerBulkStandardRepaymentPlanM> {
	public static BorrowerBulkStandardRepaymentPlanM dao = new BorrowerBulkStandardRepaymentPlanM();

	/**
	 * 保存还款计划
	 * 
	 * @param gather_money_order_id
	 *            凑集单ID
	 * @param borrower_user_id
	 *            借款人用户ID
	 * @param borrow_amount
	 *            借款的数量
	 * @param annual_interest_rate
	 *            年化利率
	 * @param total_period
	 *            总期数 throw exception
	 * @throws Exception
	 */
	public static boolean create_repayment_plan(long gather_money_order_id, int borrower_user_id, BigDecimal borrow_amount, BigDecimal annual_interest_rate, int total_period) throws Exception {
		List<RepaymentPlan> repayment_plan_list = CalculationFormula.get_repayment_plan(borrow_amount, annual_interest_rate, total_period);
		int current_period = 1;
		for (RepaymentPlan p : repayment_plan_list) {
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = new BorrowerBulkStandardRepaymentPlanM();

			boolean is_ok_save_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.//
					set("gather_money_order_id", gather_money_order_id).//
					set("borrower_user_id", borrower_user_id).//
					set("total_periods", total_period).//
					set("current_period", current_period++).//

					set("repay_start_time", p.getRepayment_start_time()).//
					set("repay_start_time_long", p.getRepayment_start_time().getTime()).//

					set("repay_end_time", p.getRepayment_end_time()).//
					set("repay_end_time_long", p.getRepayment_end_time().getTime()).//

					set("automatic_repayment_date", p.getAuto_repayment_date()).//
					set("automatic_repayment_date_long", (Model.Date.parse(p.getAuto_repayment_date())).getTime()).//

					set("repay_actual_time", null).//
					set("repay_actual_time_long", 0).//

					set("should_repayment_total", p.getMonth_pricipal_and_interest()).//
					set("should_repayment_principle", p.getMonth_pricipal()).//
					set("should_repayment_interest", p.getMonth_interest()).//

					set("normal_manage_fee", new BigDecimal(0)).//
					set("over_manage_fee", new BigDecimal(0)).//
					set("over_punish_interest", new BigDecimal(0)).//

					set("actual_repayment", new BigDecimal(0)).//
					set("is_repay", 0).//
					set("repayment_period", 0).save();
			if (!is_ok_save_borrower_bulk_standard_repayment_plan) {
				return false;
			}

		}
		return true;
	}

	/**
	 * 保存还款计划
	 * 
	 * @param gather_money_order_id
	 *            凑集单ID
	 * @param borrower_user_id
	 *            借款人用户ID
	 * @param borrow_amount
	 *            借款的数量
	 * @param annual_interest_rate
	 *            年化利率
	 * @param total_period
	 *            总期数 throw exception
	 * @throws Exception
	 */
	public static boolean create_repayment_plan_of_current_month(long gather_money_order_id, int borrower_user_id, BigDecimal borrow_amount, BigDecimal annual_interest_rate, int day) throws Exception {
		List<RepaymentPlan> repayment_plan_list = new ArrayList<RepaymentPlan>();
		{
			RepaymentPlan repayment_plan = new RepaymentPlan();

			/**
			 * 计算本金和利息
			 */
			repayment_plan.setMonth_pricipal(borrow_amount);
			/**
			 * 利息=本金*年化利率*天数/365
			 */
			BigDecimal interest = borrow_amount.multiply(annual_interest_rate).multiply(new BigDecimal(day + "")).divide(new BigDecimal("366"), 8, BigDecimal.ROUND_DOWN);
			repayment_plan.setMonth_interest(interest);
			repayment_plan.setMonth_pricipal_and_interest(borrow_amount.add(interest));

			repayment_plan.setRemain_principal(new BigDecimal(0));
			repayment_plan.setRepayment_start_time(new Date());
			Date end_time = new Date((new Date()).getTime() + day * Model.Day);
			repayment_plan.setRepayment_end_time(end_time);
			repayment_plan.setAuto_repayment_date(Model.Date.format(end_time));

			repayment_plan_list.add(repayment_plan);
		}
		/**
		 * 根据传入进来的参数构建。。。当月还款计划
		 */
		int current_period = 1;
		int total_period = 1;
		for (RepaymentPlan p : repayment_plan_list) {
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = new BorrowerBulkStandardRepaymentPlanM();

			boolean is_ok_save_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.//
					set("gather_money_order_id", gather_money_order_id).//
					set("borrower_user_id", borrower_user_id).//
					set("total_periods", total_period).//
					set("current_period", current_period++).//

					set("repay_start_time", p.getRepayment_start_time()).//
					set("repay_start_time_long", p.getRepayment_start_time().getTime()).//

					set("repay_end_time", p.getRepayment_end_time()).//
					set("repay_end_time_long", p.getRepayment_end_time().getTime()).//

					set("automatic_repayment_date", p.getAuto_repayment_date()).//
					set("automatic_repayment_date_long", (Model.Date.parse(p.getAuto_repayment_date())).getTime()).//

					set("repay_actual_time", null).//
					set("repay_actual_time_long", 0).//

					set("should_repayment_total", p.getMonth_pricipal_and_interest()).//
					set("should_repayment_principle", p.getMonth_pricipal()).//
					set("should_repayment_interest", p.getMonth_interest()).//

					set("normal_manage_fee", new BigDecimal(0)).//
					set("over_manage_fee", new BigDecimal(0)).//
					set("over_punish_interest", new BigDecimal(0)).//

					set("actual_repayment", new BigDecimal(0)).//
					set("is_repay", 0).//
					set("repayment_period", 0).save();
			if (!is_ok_save_borrower_bulk_standard_repayment_plan) {
				return false;
			}

		}
		return true;
	}
}
