package com.tfoll.web.model;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.trade.activerecord.transaction.IAtomic;

import java.io.Serializable;
import java.util.Date;

@TableBind(tableName = "sys_notification", primaryKey = "id")
public class SysNotificationM extends Model<SysNotificationM> implements Serializable {

	public static final String No_Title = "";
	public static final String No_Phone = "";
	public static final String No_Email = "";
	private static final long serialVersionUID = -4503054112919855075L;
	/**
	 * 邮件-短信-系统-把数据写入到系统通知系统里面
	 * 
	 * <pre>
	 * 系统显示的-只需要把这部分的消息让用户查询即可-默认是需要显示给用户看的
	 * 邮件发送的-只需要把这部分的消息同时写入到邮件发送引擎里面
	 * 短信发送的-只需要把这部分的消息同时写入到短信发送引擎里面
	 * </pre>
	 */

	public static SysNotificationM dao = new SysNotificationM();

	/**
	 * @param admin_id
	 *            0表示系统
	 * @param user_id
	 *            不存在为0的消息
	 * @param title
	 * @param content
	 * @param is_send_to_email
	 * @param email
	 * @param is_send_to_phone
	 * @param phone
	 * @return
	 */
	public static boolean send_sys_notification(//
			final int admin_id, final int user_id, final String title, final String content,// 基本信息
			final String phone, final boolean is_send_to_email, final int email_request_type, final String email// 发送邮件
	//
	) {//

		if (is_send_to_email) {
			/**
			 * 检查邮箱正确不
			 */
			if (!email.contains("@")) {
				return false;
			}

		}

		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				SysNotificationM sys_notification = new SysNotificationM();
				boolean is_ok_add_sys_notification = sys_notification.//
						set("admin_id", admin_id).//
						set("user_id", user_id).//
						set("title", title).//
						set("content", content).//
						set("system_status", 0).// 0用户未读
						//
						//
						set("email_request_type", email_request_type).//
						set("email", email.trim()).//
						set("email_send_state", is_send_to_email ? 0 : 1).//
						set("add_time", new Date()).save();

				if (!is_ok_add_sys_notification) {
					return false;
				}

				/**
				 * 邮箱引擎
				 */
				if (is_send_to_email) {
					UserEmailRequestWaitingM user_email_request_waiting = new UserEmailRequestWaitingM();
					UserM user = UserM.dao.findFirst("select * from user_info where id = ?", user_id);
					boolean is_ok_add_user_email_request_waiting = user_email_request_waiting.//
							set("user_id", user_id).//

							set("ip", "管理员").//
							set("nickname", user.getString("nickname")).set("email", email).//
							set("content", content).// 根据请求类型结合内容进行页面渲染
							set("request_type", email_request_type).//
							set("retry_times", 0).//
							save();
					if (!is_ok_add_user_email_request_waiting) {
						return false;// 只要出现为假则事务回退
					}

				}
				return true;
			}
		});

	}

	public static boolean sys_send_message(String phone, String content, int message_request_type) {
		/**
		 * 短信引擎
		 */
		UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
		boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.//
				set("user_id", 0).//
				set("ip", "管理员").//
				set("phone", phone).//
				set("content", content).// 根据请求类型结合内容进行页面渲染
				set("request_type", message_request_type).//
				set("retry_times", 0).//
				save();
		if (!is_ok_add_user_message_request_waiting) {
			return false;// 只要出现为假则事务回退
		}

		return true;
	}

}
