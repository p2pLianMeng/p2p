package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.math.BigDecimal;
import java.util.Date;

@TableBind(tableName = "borrower_bulk_standard_overdue_repayment_record", primaryKey = "id")
public class BorrowerBulkStandardOverdueRepaymentRecordM extends Model<BorrowerBulkStandardOverdueRepaymentRecordM> {

	public static final BorrowerBulkStandardOverdueRepaymentRecordM dao = new BorrowerBulkStandardOverdueRepaymentRecordM();

	/**
	 * 添加逾期还款资金流动记录
	 * 
	 * < pre>
	 * CREATE TABLE `borrower_bulk_standard_overdue_repayment_record` (
	 *   `id` bigint(13) NOT NULL AUTO_INCREMENT,
	 *   `gather_money_order_id` bigint(13) DEFAULT NULL COMMENT '凑集单ID',
	 *   `repayment_plan_id` bigint(13) DEFAULT NULL COMMENT '借款方ID',
	 *   `total_periods` int(11) DEFAULT NULL COMMENT '还款总期数',
	 *   `current_period` int(11) DEFAULT NULL COMMENT '当期期数',
	 *   `repay_actual_time` timestamp NULL DEFAULT NULL COMMENT '实际还款时间',
	 *   `repay_actual_time_long` bigint(20) DEFAULT NULL COMMENT '实际还款时间毫秒数',
	 *   `should_repayment_total` decimal(10,2) DEFAULT '0.00' COMMENT '应该要还的本金和利息',
	 *   `should_repayment_principle` decimal(10,2) DEFAULT '0.00' COMMENT '要还的本金',
	 *   `should_repayment_interest` decimal(10,2) DEFAULT '0.00' COMMENT '要还的利息（逾期后该数值每天都会改变，要写存储过程？）',
	 *   `normal_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '正常管理费',
	 *   `over_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '逾期管理费',
	 *   `over_punish_interest` decimal(10,2) DEFAULT '0.00' COMMENT '逾期罚息',
	 *   `actual_repayment` decimal(10,2) DEFAULT '0.00' COMMENT '实际还款金额',
	 *   PRIMARY KEY (`id`)
	 * ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='借款人的逾期还款记录-这部分的记录同时也记录在还款计划里面的';
	 * 
	 * 
	 * </pre>
	 */
	public static final boolean add_borrower_bulk_standard_overdue_repayment_record(//
			long gather_money_order_id,//
			long repayment_plan_id, //
			int total_periods, //
			int current_period,//
			BigDecimal should_repayment_total,//
			BigDecimal should_repayment_principle,//
			BigDecimal should_repayment_interest,//
			BigDecimal normal_manage_fee,//
			BigDecimal over_manage_fee,//
			BigDecimal over_punish_interest,//
			BigDecimal actual_repayment) {//
		BorrowerBulkStandardOverdueRepaymentRecordM borrower_bulk_standard_overdue_repayment_record = new BorrowerBulkStandardOverdueRepaymentRecordM();
		return borrower_bulk_standard_overdue_repayment_record.//
				set("gather_money_order_id", gather_money_order_id).//
				set("repayment_plan_id", repayment_plan_id).//

				set("total_periods", total_periods).//
				set("current_period", current_period).//

				set("repay_actual_time", new Date()).//
				set("repay_actual_time_long", System.currentTimeMillis()).//

				set("should_repayment_total", should_repayment_total).//

				set("should_repayment_principle", should_repayment_principle).//
				set("should_repayment_interest", should_repayment_interest).//
				set("normal_manage_fee", normal_manage_fee).//
				set("over_manage_fee", over_manage_fee).//
				set("over_punish_interest", over_punish_interest).//
				set("actual_repayment", actual_repayment).//

				save();

	}
}
