package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "_____activity___standard_order_waiting_of_lender", primaryKey = "id")
public class _____Activity___Standard_Order_Waiting_Of_LenderM extends Model<_____Activity___Standard_Order_Waiting_Of_LenderM> implements Serializable {

	private static final long serialVersionUID = -7124140993536632669L;
	public static _____Activity___Standard_Order_Waiting_Of_LenderM dao = new _____Activity___Standard_Order_Waiting_Of_LenderM();
}
