package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "fix_bid_transfer_funds_to_user_account", primaryKey = "system_order_id")
public class FixBidTransferFundsToUserAccountM extends Model<FixBidTransferFundsToUserAccountM> {
	public static FixBidTransferFundsToUserAccountM dao = new FixBidTransferFundsToUserAccountM();
}
