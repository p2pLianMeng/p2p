package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import org.apache.log4j.Logger;

@TableBind(tableName = "user_automatically_bid_user_info_change_notify", primaryKey = "id")
public class UserAutomaticallyBidUserInfoChangeNotifyM extends Model<UserAutomaticallyBidUserInfoChangeNotifyM> {
	public static UserAutomaticallyBidUserInfoChangeNotifyM dao = new UserAutomaticallyBidUserInfoChangeNotifyM();

	public static void add(int user_id) {
		try {
			UserAutomaticallyBidUserInfoChangeNotifyM user_automatically_bid_user_info_change_notify = new UserAutomaticallyBidUserInfoChangeNotifyM();
			user_automatically_bid_user_info_change_notify.set("user_id", user_id).set("add_time_long", System.currentTimeMillis()).save();
		} catch (Exception e) {
			Logger logger = Logger.getLogger(UserAutomaticallyBidUserInfoChangeNotifyM.class);
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
}
