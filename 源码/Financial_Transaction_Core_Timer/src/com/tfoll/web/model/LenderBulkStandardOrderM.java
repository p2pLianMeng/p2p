package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_order", primaryKey = "id")
public class LenderBulkStandardOrderM extends Model<LenderBulkStandardOrderM> {
	public static LenderBulkStandardOrderM dao = new LenderBulkStandardOrderM();
}
