package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "sys_error_info_waiting_for_dealing", primaryKey = "id")
public class SysErrorInfoWaitingForDealingM extends Model<SysErrorInfoWaitingForDealingM> {
	public static final SysErrorInfoWaitingForDealingM dao = new SysErrorInfoWaitingForDealingM();

}
