package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_info", primaryKey = "id")
public class UserM extends Model<UserM> {
	public static UserM dao = new UserM();

	public static UserM getUserM(int user_id) {
		return UserM.dao.findFirst("select * from user_info WHERE id=?  for UPDATE", new Object[] { user_id });
	}

	public static UserM getUserForSelect(int user_id) {
		return UserM.dao.findFirst("select * from user_info WHERE id=? ", new Object[] { user_id });
	}
}
