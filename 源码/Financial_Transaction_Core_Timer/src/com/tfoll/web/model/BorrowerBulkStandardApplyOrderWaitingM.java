package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_apply_order_waiting", primaryKey = "id")
public class BorrowerBulkStandardApplyOrderWaitingM extends Model<BorrowerBulkStandardApplyOrderWaitingM> {
	public static BorrowerBulkStandardApplyOrderWaitingM dao = new BorrowerBulkStandardApplyOrderWaitingM();
}
