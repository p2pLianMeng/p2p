package com.tfoll.web.action.system_manager;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserOnLineManagerLoginedAop;
import com.tfoll.web.util.Utils;

@ActionKey("/user_online_manager_login")
public class UserOnlineManagerLoginAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "日志登录认证", last_update_author = "向旋")
	public void index() {
		String password = getParameter("password");
		if (Utils.isNullOrEmptyString(password)) {
			renderText("密码为空");
			return;
		}
		if (!"8e7ab097-5a3d-4f05-bddc-671e4a6877a6".equals(password)) {
			renderText("密码错误");
			return;
		}
		setSessionAttribute(UserOnlineManagerLoginAction.class.getName(), 1);
		renderJsp("/user_online_manager/admin/admin_role_all_menu.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserOnLineManagerLoginedAop.class)
	@Function(for_people = "所有人", function_description = "日志登录认证", last_update_author = "向旋")
	public void quit() {
		getSession().invalidate();
		renderHttp("http://www.baidu.com");
		return;
	}
}