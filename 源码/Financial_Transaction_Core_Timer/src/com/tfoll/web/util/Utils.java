package com.tfoll.web.util;

import com.tfoll.trade.activerecord.model.Model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author czh 330937205 对HttpServletRequest参数进行处理
 * 
 */
public class Utils {

	public static double convertToDouble(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");
		}
		return Double.parseDouble(string);
	}

	public static double convertToDouble(String string, double defaultValue) {
		try {
			return Double.parseDouble(string);
		} catch (Exception e) {
			return defaultValue;
		}

	}

	public static float convertToFloat(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");
		}
		return Float.parseFloat(string);

	}

	public static float convertToFloat(String string, float defaultValue) {
		try {
			return Float.parseFloat(string);
		} catch (Exception e) {
			return defaultValue;
		}

	}

	/**
	 * 将字符串转换为数字-支持int-long-float-double
	 */
	public static int convertToInt(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");
		}
		return Integer.parseInt(string);
	}

	public static int convertToInt(String string, int defaultValue) {
		try {
			return Integer.parseInt(string);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public static long convertToLong(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");
		}
		return Long.parseLong(string);

	}

	public static long convertToLong(String string, long defaultValue) {
		try {
			return Long.parseLong(string);
		} catch (Exception e) {
			return defaultValue;
		}

	}

	/**
	 * 仅仅处理一个参数的HttpServletRequest请求参数
	 */
	public static String getParameter(HttpServletRequest request, String name) {
		if (request == null) {
			throw new NullPointerException("request is null");
		}
		if (name == null) {
			throw new NullPointerException("name is null");
		}
		try {

			String value = request.getParameter(name);
			if (value == null) {
				return null;
			}
			if (value != null) {
				value = value.trim();
			}
			return value;
		} catch (Exception e) {
			return null;
		}
	}

	public static String getParameter(HttpServletRequest request, String name, String defaultValue) {
		if (request == null) {
			throw new NullPointerException("request is null");
		}
		if (name == null) {
			throw new NullPointerException("name is null");
		}

		String value = request.getParameter(name);
		if (value != null) {
			return value.trim();
		} else {
			return defaultValue;
		}

	}

	public static int getParameterToInt(HttpServletRequest request, String name, int defaultValue) {
		if (request == null) {
			throw new NullPointerException("request is null");
		}
		if (name == null) {
			throw new NullPointerException("name is null");
		}

		String value = request.getParameter(name);
		if (value == null) {
			return defaultValue;
		}
		int intValue = defaultValue;
		try {
			intValue = Integer.parseInt(value.trim());
			return intValue;
		} catch (NumberFormatException e) {
			intValue = defaultValue;
			return intValue;
		}

	}

	/**
	 * 处理多个参数的HttpServletRequest请求参数
	 */
	public static String[] getParameterValues(HttpServletRequest request, String name) {
		if (request == null) {
			throw new NullPointerException("request is null");
		}
		if (name == null) {
			throw new NullPointerException("name is null");
		}
		return request.getParameterValues(name);
	}

	/**
	 * 是否为字符串
	 */
	public static boolean isDefaultValue(String value, String defaultValue) {
		if (value == null && defaultValue == null) {
			return true;
		}
		if (value != null && defaultValue != null && defaultValue.equals(value)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 判断是不是数字-支持int-long-float-double
	 */
	public static boolean isDigital(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");
		}
		try {
			Integer.parseInt(string);
			return true;
		} catch (Exception e) {
		}
		try {
			Long.parseLong(string);
			return true;
		} catch (Exception e) {
		}
		try {
			Double.parseDouble(string);
			return true;
		} catch (Exception e) {
		}
		try {
			Float.parseFloat(string);
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * 判断一个字符串是否为空
	 */
	public static boolean isEmpty(String string) {
		if (string == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 是否为字符串
	 */
	public static boolean isEmptyString(String string) {
		if (string != null && "".equals(string)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空且不为空字符串
	 */
	public static boolean isNotNullAndNotEmptyString(String string) {
		if (string != null && !"".equals(string.trim())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空但为空字符串
	 */
	public static boolean isNotNullAndEmptyString(String string) {
		if (string != null && "".equals(string)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 是否为空或为字符串
	 */
	public static boolean isNullOrEmptyString(String string) {
		if (string == null || (string != null && "".equals(string))) {
			return true;
		} else {
			return false;
		}
	}

	public static void StringToUpperCase(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");
		}
		System.out.println(string.toUpperCase());
	}

	/**
	 * 去除空格
	 */
	public static String trim(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");
		} else {
			return string.trim();
		}
	}

	/**
	 * 针对美红他们的查询集合的快速处理的办法
	 * 
	 * @param <T>
	 * @param collection
	 * @return
	 */
	public static <T> T CollectionToOne(Collection<T> collection) {
		Iterator<T> iterator = collection.iterator();
		T t = null;
		if (iterator.hasNext()) {
			t = iterator.next();
		}
		return t;

	}

	/**
	 * 如果错误会返回当前系统的时间-如要是用于需要记录系统默认时间的操作
	 * 
	 * @param request
	 * @param string
	 * @return
	 */
	public static Date getParameterToDate(HttpServletRequest request, String string) {
		String var = request.getParameter(string);
		Date date = new Date();
		try {
			date = Model.Date.parse(var);
		} catch (Exception e) {
		}
		return date;
	}

	public static Date getParameterToDateIfNull(HttpServletRequest request, String string) {
		String var = request.getParameter(string);
		Date date = null;
		try {
			date = Model.Date.parse(var);
		} catch (Exception e) {
		}
		return date;
	}

	/**
	 * 去除字符串两边的,
	 * 
	 * @return
	 */
	public static String trimQ(String string) {

		System.out.println(string);
		String temp = string;
		// 判断第一个或者最后一个字符是否为,
		if (temp == null) {
			return null;
		}
		if ("".equals(temp.trim())) {
			return "";
		}
		if (temp.startsWith(",")) {
			// 判断字符串的长度
			int len = temp.length();
			if (len == 1) {
				return "";
			} else {
				temp = temp.substring(1, len);
			}
		}

		if (temp.endsWith(",")) {
			// 判断字符串的长度
			int len = temp.length();
			if (len == 1) {
				return "";
			} else {
				temp = temp.substring(0, len - 1);
			}
		}
		return temp;

	}

	/**
	 * 判断一个集合存在且有数据
	 */
	public static boolean isHasData(List<?> list) {
		if (list != null && list.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 判断数组是否有效
	 */
	public static boolean isHasData(String[] array) {

		if (array == null || (array != null && array.length == 0)) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean isHasData(Map<String, Object> map) {
		if (map != null && map.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isHasDataOfBigDecimal(Map<String, BigDecimal> map) {
		if (map != null && map.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 判断传入的参数有几个不为空
	 */
	public static int countOfNotNull(String[] strings) {
		if (strings == null || (strings != null && strings.length == 0)) {
			throw new NullPointerException("strings is null");
		}
		int len = strings.length;
		int count = 0;
		for (int i = 0; i < len; i++) {
			if (strings[i] != null) {
				count++;
			}
		}
		return count;

	}

	/**
	 * 去掉[]
	 * 
	 * @param string
	 * @return
	 */
	public static String trim$(String string) {
		if (string == null) {
			return string;
		} else {
			// 判断是否含有[]
			if (string.indexOf("[") != -1 && string.indexOf("[") != -1) {
				int len = string.indexOf("[");
				String s = string.substring(0, len);
				return s;
			} else {
				return string;
			}
		}

	}

	public static boolean checkEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {

			flag = false;
		}

		return flag;
	}

	// 判断是否为 数值类型
	public static boolean isNum(String num) {
		try {
			Double.parseDouble(num);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	public static boolean isHasData(Set set) {
		if (set != null && set.size() != 0) {
			return true;
		} else {
			return false;
		}

	}
}
