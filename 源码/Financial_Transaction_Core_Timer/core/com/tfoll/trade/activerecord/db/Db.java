package com.tfoll.trade.activerecord.db;

import com.tfoll.trade.activerecord.ActiveRecordException;
import com.tfoll.trade.activerecord.DataSourceKit;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.activerecord.transaction.Transaction;
import com.tfoll.trade.config.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 注意该工具类的数据源实例从DbKit取得<br/>
 * Db+Record组合<br/>
 * 
 * <pre>
 * 事务操作
 * {@link #tx(IAtomic)}
 * </pre>
 */
@SuppressWarnings( { "unchecked" })
public class Db {
	public static Logger logger = Logger.getLogger(Db.class);
	private static Object[] o = new Object[0];

	// 查询
	public static List<Record> find(Connection conn, String sql, Object... params) throws SQLException {
		if (Constants.devMode) {
			logger.debug("sql:" + sql);
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		int length = params.length;
		for (int i = 0; i < length; i++) {
			ps.setObject(i + 1, params[i]);
		}
		ResultSet rs = ps.executeQuery();
		List<Record> result = RecordBuilder.build(rs);
		DataSourceKit.close(rs, ps);
		return result;
	}

	public static List<Record> find(String sql) {
		return find(sql, o);
	}

	public static List<Record> find(String sql, Object... params) {
		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();
			return find(conn, sql, params);
		} catch (Exception e) {
			logger.error(e);
			throw new ActiveRecordException(e);
		} finally {
			DataSourceKit.close(conn);
		}
	}

	public static Record findFirst(String sql) {
		List<Record> result = find(sql, o);
		return result.size() > 0 ? result.get(0) : null;
	}

	public static Record findFirst(String sql, Object... params) {
		List<Record> result = find(sql, params);
		return result.size() > 0 ? result.get(0) : null;
	}

	/**
	 * 查询--底层方法
	 */
	private static <T> List<T> query(Connection conn, String sql, Object... params) throws SQLException {
		if (Constants.devMode) {
			logger.debug("sql:" + sql);
		}
		List result = new ArrayList();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		int length = params.length;
		for (int i = 0; i < length; i++) {
			pstmt.setObject(i + 1, params[i]);
		}
		ResultSet rs = pstmt.executeQuery();
		int columnCount = rs.getMetaData().getColumnCount();
		if (columnCount > 1) {
			while (rs.next()) {
				Object[] temp = new Object[columnCount];
				for (int i = 0; i < columnCount; i++) {
					temp[i] = rs.getObject(i + 1);
				}
				result.add(temp);
			}
		} else if (columnCount == 1) {
			while (rs.next()) {
				result.add(rs.getObject(1));
			}
		}
		DataSourceKit.close(rs, pstmt);
		return result;
	}

	public static <T> List<T> query(String sql) {

		return query(sql, o);
	}

	public static <T> List<T> query(String sql, Object... params) {
		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();
			return query(conn, sql, params);
		} catch (Exception e) {
			logger.error(e);
			throw new ActiveRecordException(e);
		} finally {
			DataSourceKit.close(conn);
		}
	}

	public static java.math.BigDecimal queryBigDecimal(String sql) {
		return (java.math.BigDecimal) queryColumn(sql, o);
	}

	public static java.math.BigDecimal queryBigDecimal(String sql, Object... params) {
		return (java.math.BigDecimal) queryColumn(sql, params);
	}

	public static Boolean queryBoolean(String sql) {
		return (Boolean) queryColumn(sql, o);
	}

	public static Boolean queryBoolean(String sql, Object... params) {
		return (Boolean) queryColumn(sql, params);
	}

	public static byte[] queryBytes(String sql) {
		return (byte[]) queryColumn(sql, o);
	}

	public static byte[] queryBytes(String sql, Object... params) {
		return (byte[]) queryColumn(sql, params);
	}

	public static <T> T queryColumn(String sql) {
		return queryColumn(sql, o);
	}

	/**
	 * 只返回第一条记录-有且只有这条数据只有一条数据
	 */
	public static <T> T queryColumn(String sql, Object... params) {
		List<T> result = query(sql, params);
		if (result.size() > 0) {
			T temp = result.get(0);
			if (temp instanceof Object[]) {
				throw new ActiveRecordException("只能返回一个字段");
			}
			return temp;
		}
		return null;
	}

	public static java.sql.Date queryDate(String sql) {
		return (java.sql.Date) queryColumn(sql, o);
	}

	public static java.sql.Date queryDate(String sql, Object... params) {

		return (java.sql.Date) queryColumn(sql, params);
	}

	public static Double queryDouble(String sql) {
		return (Double) queryColumn(sql, o);
	}

	public static Double queryDouble(String sql, Object... params) {
		return (Double) queryColumn(sql, params);
	}

	public static <T> T queryFirst(String sql) {
		List<T> result = query(sql, o);
		return (result.size() > 0 ? result.get(0) : null);
	}

	public static <T> T queryFirst(String sql, Object... params) {
		List<T> result = query(sql, params);
		return (result.size() > 0 ? result.get(0) : null);
	}

	public static Float queryFloat(String sql) {
		return (Float) queryColumn(sql, o);
	}

	public static Float queryFloat(String sql, Object... params) {
		return (Float) queryColumn(sql, params);
	}

	public static Integer queryInt(String sql) {
		return (Integer) queryColumn(sql, o);
	}

	public static Integer queryInt(String sql, Object... params) {
		return (Integer) queryColumn(sql, params);
	}

	public static Long queryLong(String sql) {
		return (Long) queryColumn(sql, o);
	}

	public static Long queryLong(String sql, Object... params) {
		return (Long) queryColumn(sql, params);
	}

	public static Number queryNumber(String sql) {
		return (Number) queryColumn(sql, o);
	}

	public static Number queryNumber(String sql, Object... params) {
		return (Number) queryColumn(sql, params);
	}

	public static String queryString(String sql) {
		return (String) queryColumn(sql, o);
	}

	public static String queryString(String sql, Object... params) {
		return (String) queryColumn(sql, params);
	}

	public static java.sql.Time queryTime(String sql) {
		return (java.sql.Time) queryColumn(sql, o);
	}

	public static java.sql.Time queryTime(String sql, Object... params) {
		return (java.sql.Time) queryColumn(sql, params);
	}

	public static java.sql.Timestamp queryTimestamp(String sql) {
		return (java.sql.Timestamp) queryColumn(sql, o);
	}

	public static java.sql.Timestamp queryTimestamp(String sql, Object... params) {
		return (java.sql.Timestamp) queryColumn(sql, params);
	}

	/**
	 * 采用一个线程一个数据库连接的设计模式
	 */
	public static boolean tx(IAtomic atomic) {
		if (atomic == null || !(atomic instanceof IAtomic)) {
			throw new NullPointerException("请传入IAtomic实例对象");
		}
		if (Transaction.getConnection() != null) {
			throw new RuntimeException("不支持嵌套事务");
		}
		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();
			Transaction.set(true);
			DataSourceKit.setAutoCommit(conn, false);// 这里进行手动设置提交方式-代理的连接需要在其他的地方进行恢复

			boolean result = false;
			try {
				result = atomic.transactionProcessing();
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				logger.error(e);
			}
			if (result) {
				DataSourceKit.commit(conn);
				return true;
			} else {
				DataSourceKit.rollback(conn);
				return false;
			}
		} catch (Exception e) {
			logger.error(e);
			DataSourceKit.rollback(conn);
			return false;
		} finally {

			Transaction.set(false);
			DataSourceKit.setAutoCommit(conn, true);// 和设置的方向相反
			DataSourceKit.close(conn);

		}
	}

	// 更新:最基本的方法

	/**
	 * 整个框架最核心的数据库更新操作方法
	 */
	public static int update(Connection conn, String sql, Object... params) throws SQLException {
		if (Constants.devMode) {
			logger.debug("sql:" + sql);
		}
		PreparedStatement pstmt = conn.prepareStatement(sql);
		int length = params.length;
		for (int i = 0; i < length; i++) {
			pstmt.setObject(i + 1, params[i]);
		}
		int result = pstmt.executeUpdate();
		DataSourceKit.close(pstmt);
		return result;
	}

	// 更新:最基本的方法
	public static int update(String sql) {
		return update(sql, o);
	}

	// 更新:最基本的方法-可以执行删除操作
	public static int update(String sql, Object... params) {
		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();
			return update(conn, sql, params);
		} catch (Exception e) {
			if (Constants.devMode) {
				e.printStackTrace();
			}
			logger.error(e);

			throw new ActiveRecordException(e);
		} finally {
			DataSourceKit.close(conn);
		}
	}

}
