package com.tfoll.trade.activerecord.dialect;

import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.TableInfo;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class MysqlDialect extends Dialect {
	@Override
	public String buildSqlForTableInfo(String tableName) {
		return "SELECT * FROM " + tableName + " limit 0,1";
	}

	@Override
	public String deleteByIdForDb(String tableName, String primaryKey) {
		StringBuilder sql = new StringBuilder("DELETE FROM `");
		sql.append(tableName.trim());
		sql.append("` WHERE `").append(primaryKey.trim()).append("` = ?");
		return sql.toString();
	}

	@Override
	public String deleteByIdForModel(TableInfo tableInfo) {
		String primaryKey = tableInfo.getPrimaryKey();
		StringBuilder sql = new StringBuilder(60);
		sql.append("DELETE FROM `");
		sql.append(tableInfo.getTableName());
		sql.append("` WHERE `").append(primaryKey).append("` = ?");
		return sql.toString();
	}

	@Override
	public String findByIdForDb(String tableName, String primaryKey, String columns) {
		StringBuilder sql = new StringBuilder("SELECT ");
		if (columns.trim().equals("*")) {
			sql.append(columns);
		} else {
			String[] columnsArray = columns.split(",");
			for (int i = 0; i < columnsArray.length; i++) {
				if (i > 0)
					sql.append(", ");
				sql.append("`").append(columnsArray[i].trim()).append("`");
			}
		}
		sql.append(" FROM `");
		sql.append(tableName.trim());
		sql.append("` WHERE `").append(primaryKey).append("` = ?");
		return sql.toString();
	}

	@Override
	public String findByIdForModel(TableInfo tableInfo, String columns) {
		StringBuilder sql = new StringBuilder("SELECT ");
		if (columns.trim().equals("*")) {
			sql.append(columns);
		} else {
			String[] columnsArray = columns.split(",");
			for (int i = 0; i < columnsArray.length; i++) {
				if (i > 0) {
					sql.append(", ");
				}
				sql.append("`");
				sql.append(columnsArray[i].trim());
				sql.append("`");
			}
		}
		sql.append(" FROM `");
		sql.append(tableInfo.getTableName());
		sql.append("` WHERE `").append(tableInfo.getPrimaryKey()).append("` = ?");
		return sql.toString();
	}

	@Override
	public boolean isSupportAutoIncrementKey() {
		return true;
	}

	@Override
	public void saveForDb(String tableName, Record record, StringBuilder sql, List<Object> paras) {
		sql.append("INSERT INTO `");
		sql.append(tableName.trim()).append("`(");
		StringBuilder temp = new StringBuilder();
		temp.append(") VALUES(");

		for (Entry<String, Object> entry : record.getColumnMap().entrySet()) {
			if (paras.size() > 0) {
				sql.append(", ");
				temp.append(", ");
			}
			sql.append("`").append(entry.getKey()).append("`");
			temp.append("?");
			paras.add(entry.getValue());
		}
		sql.append(temp.toString()).append(")");
	}

	@Override
	public void saveForModel(TableInfo tableInfo, Map<String, Object> attributeMap, StringBuilder sql, List<Object> params) {
		sql.append("INSERT INTO `").append(tableInfo.getTableName()).append("`(");
		StringBuilder temp = new StringBuilder(") VALUES(");
		for (Entry<String, Object> entry : attributeMap.entrySet()) {
			String columnName = entry.getKey();
			if (tableInfo.containsColumnName(columnName)) {
				if (params.size() > 0) {
					sql.append(", ");
					temp.append(", ");
				}
				sql.append("`").append(columnName).append("`");
				temp.append("?");
				params.add(entry.getValue());
			}
		}
		sql.append(temp.toString()).append(")");
	}

	@Override
	public void updateForDb(String tableName, String primaryKey, Object id, Record record, StringBuilder sql, List<Object> params) {
		sql.append("UPDATE `").append(tableName.trim()).append("` SET ");
		for (Entry<String, Object> entry : record.getColumnMap().entrySet()) {
			String columnName = entry.getKey();
			if (!primaryKey.equalsIgnoreCase(columnName)) {
				if (params.size() > 0) {
					sql.append(", ");
				}
				sql.append("`").append(columnName).append("` = ? ");
				params.add(entry.getValue());
			}
		}
		sql.append(" WHERE `").append(primaryKey).append("` = ?");
		params.add(id);
	}

	@Override
	public void updateForModel(TableInfo tableInfo, Map<String, Object> attributeMap, Set<String> modifyFlag, String primaryKey, Object id, StringBuilder sql, List<Object> params) {
		sql.append("UPDATE `").append(tableInfo.getTableName()).append("` set ");
		for (Entry<String, Object> entry : attributeMap.entrySet()) {
			String columnName = entry.getKey();
			if (!primaryKey.equalsIgnoreCase(columnName) && modifyFlag.contains(columnName) && tableInfo.containsColumnName(columnName)) {
				if (params.size() > 0) {
					sql.append(", ");
				}
				sql.append("`").append(columnName).append("` = ? ");
				params.add(entry.getValue());
			}
		}
		sql.append(" WHERE `").append(primaryKey).append("` = ?");
		params.add(id);
	}

}
