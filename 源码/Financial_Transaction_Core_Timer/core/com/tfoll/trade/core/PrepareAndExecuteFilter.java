package com.tfoll.trade.core;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.trade.render.ActionRender;
import com.tfoll.trade.render.FileRender;
import com.tfoll.trade.render.HttpRender;
import com.tfoll.trade.render.Render;
import com.tfoll.trade.render.RootRender;
import com.tfoll.trade.render.TextRender;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * 核心类:天富网-贸易平台核心框架请求响应入口
 */

public final class PrepareAndExecuteFilter implements Filter {
	private static Logger logger = Logger.getLogger(PrepareAndExecuteFilter.class);

	/**
	 * 用户配置信息
	 */
	private UserConfig userConfig = null;

	public void destroy() {
		WebContent.stopPlugins(WebContent.pluginList);
	}

	// 加载配置文件
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("====================框架资源管理器初始化开始====================");
		// 接受配置参数
		String webContentUserConfigClass = filterConfig.getInitParameter("webContentUserConfigClass");
		String actionExtension = filterConfig.getInitParameter("actionExtension");
		String devMode = filterConfig.getInitParameter("devMode");

		if (actionExtension != null && !"".equals(actionExtension.trim())) {
			Constants.actionExtension = actionExtension;
			Constants.isUseActionExtension = true;
		}
		if (devMode != null && ("true".equalsIgnoreCase(devMode) || "false".equalsIgnoreCase(devMode))) {
			Constants.devMode = Boolean.parseBoolean(devMode);

		}

		if (webContentUserConfigClass == null) {
			throw new IllegalArgumentException("请在web.xml配置天富网内部框架所需的webContentUserConfigClass");
		}
		try {
			userConfig = (UserConfig) Class.forName(webContentUserConfigClass).newInstance();
			if (userConfig == null) {
				throw new IllegalArgumentException("您在web.xml配置天富网内部框所需的webContentUserConfigClass[" + webContentUserConfigClass + "]不能实例化");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		boolean initok = false;
		try {
			initok = WebContent.initWebContent(userConfig);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		if (!initok) {
			throw new IllegalArgumentException("框架初始化失败");
		}

		logger.info("====================框架资源管理器初始化完成====================");
	}

	private static final String Index_Url = "/";
	private static final String Index_Url_To_Action_Url = "/index/index.html";

	/**
	 * 接管动态请求，控制 action + interceptor + render 执行流程
	 */

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		/**
		 * <pre>
		 * 		 1.关于get出现乱码的问题处理的办法是约定:get提交的方式只是数字和字母
		 * 		 2.1.jsp页面必须这样设置：<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
		 * 		 2.2request.setCharacterEncoding("utf-8");
		 * 		 2.3response.setCharacterEncoding("utf-8");
		 * </pre>
		 */
		// 编码设置
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		// 编码设置
		request.setCharacterEncoding(Constants.characterEncoding);
		response.setCharacterEncoding(Constants.characterEncoding);
		// 不能使用这句话-否则造成session丢失response.setHeader("SET-COOKIE", "JSESSIONID=" +
		// request.getSession().getId() + ";HttpOnly");

		// 相对于系统根目录的访问的路径
		String key = request.getServletPath();
		if (Index_Url.equals(key)) {// /
			key = Index_Url_To_Action_Url;
		}
		// 访问参数
		// String queryString = request.getQueryString();
		if (Constants.isUseActionExtension) {
			int index = key.lastIndexOf(Constants.actionExtension);
			if (index != -1) {
				key = key.substring(0, index);
			}
		}
		Action action = ActionMapping.getAction(key);
		if (action == null) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		} else {

			// 缓存控制器
			Controller controller = action.getController();

			ActionContext.setActionContext(request, response);
			ActionReporter.doReport(controller, action);
			(new ActionExecutor(action, controller)).invoke();
			Render render = ActionContext.getRender();
			// 下面这些渲染器是通过测试的
			// 核心处理：Action的渲染
			if (render instanceof ActionRender) {
				String actionUrl = ((ActionRender) render).getActionUrl();
				if (actionUrl == null || "".equals(actionUrl)) {
					throw new IllegalArgumentException("将要跳转的Action的actionUrl为空");
				}
				render.render();
				return;

			}
			if (render instanceof TextRender) {
				render.render();
				return;
			}
			if (render instanceof FileRender) {
				render.render();
				return;
			}
			// 页面跳转和重定向

			if (render instanceof RootRender) {
				render.render();
				return;
			}

			if (render instanceof HttpRender) {
				render.render();
				return;
			}
			throw new RuntimeException("不支持的渲染方式");
		}

	}

}
