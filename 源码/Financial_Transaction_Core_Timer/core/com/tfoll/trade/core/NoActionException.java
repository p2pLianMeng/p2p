package com.tfoll.trade.core;

@SuppressWarnings("serial")
public class NoActionException extends RuntimeException {

	public NoActionException() {
		super();

	}

	public NoActionException(String arg0) {
		super(arg0);

	}

	public NoActionException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	public NoActionException(Throwable arg0) {
		super(arg0);

	}

}