<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<html xmlns="http://www.w3.org/1999/xhtml">

<body>

<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="loan_aa">       
        <div class="loan_yl">
           <div class="up"><span>借款信息预览 - 工薪贷</span>(适用工薪阶层)</div> 
           <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td>借款金额<span>￥${borrower_bulk_standard_apply_order.m.borrow_all_money }</span></td>
                 <td>借款期限<span>${borrower_bulk_standard_apply_order.m.borrow_duration }个月</span></td>
                 <td>年化利率<span>${borrower_bulk_standard_apply_order.m.annulized_rate }%</span></td>
                 <td>月还款额<span>￥${monthly_principal_interest }</span></td>
              </tr>
           </table>  
           <div class="up_2">个人信息</div>  
           <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td width="490"><p>姓名</p><span>${user.m.real_name}</span></td>
                 <td width="490"><p>手机号</p><span>${phone}</span></td>
              </tr>
              <tr>
                 <td width="490"><p>性别</p><span>${sex}</span></td>
                 <td width="490"><p>最高学历</p><span>${user_authenticate_persional_info.m.highest_education}</span></td>
                
              </tr>
              <tr>
                 <td width="490"><p>出生日期</p><span>${birthday}</span></td>
                  <td width="490"><p>居住地址</p><span>${user_authenticate_persional_info.m.address}</span></td>
              </tr>
              <tr>
                  <td width="490"><p>身份证</p><span>${user_identity }</span></td>
                  <td width="490"><p>居住地电话</p><span>${user_authenticate_persional_info.m.landline}</span></td>
              </tr>
              <tr>
                <td width="490"><p>籍贯</p><span>${user_authenticate_persional_info.m.native_place_province}:${user_authenticate_persional_info.m.native_place_city}</span></td>
                 <td width="490"><p>居住地邮编</p><span>${user_authenticate_persional_info.m.zip_code}</span></td>
              </tr>
              <tr>
               	 <td width="490"><p>户口所在地</p><span>${user_authenticate_persional_info.m.domicile_place_province}:${user_authenticate_persional_info.m.domicile_place_city}</span></td>
              </tr>
           </table> 
           <div class="up_2">家庭信息</div>   
           <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td class="td_1" width="490"><span>婚姻状况</span></td>
                 <td class="td_1" width="490"><span>直系亲属</span></td>
              </tr>
              <tr>
                 <td width="490"><p>婚姻状况</p><span>${user_authenticate_family_info.m.marriage }</span></td>
                 <td width="490"><p>姓名</p><span>${user_authenticate_family_info.m.lineal_relatives_name }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>有无子女</p><span>${user_authenticate_family_info.m.children }</span></td>
                 <td width="490"><p>手机号</p><span>${user_authenticate_family_info.m.lineal_relatives_phone }</span></td>
              </tr>
              <tr>
                 <td width="490"><p></p><span></span></td>
                 <td width="490"><p>关系</p><span>${user_authenticate_family_info.m.lineal_relatives_relation }</span></td>
              </tr>
              <tr>
                 <td class="td_1" width="490"><span>其他联系人</span></td>
                 <td class="td_1" width="490"><span>其他联系人2</span></td>
              </tr>
              <tr>
                 <td width="490"><p>姓名</p><span>${user_authenticate_family_info.m.other_contacts_name }</span></td>
                 <td width="490"><p>姓名</p><span>${user_authenticate_family_info.m.other_contacts_name_two }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>手机号</p><span>${user_authenticate_family_info.m.other_contacts_phone }</span></td>
                 <td width="490"><p>手机号</p><span>${user_authenticate_family_info.m.other_contacts_phone_two }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>关系</p><span>${user_authenticate_family_info.m.other_contacts_relation }</span></td>
                 <td width="490"><p>关系</p><span>${user_authenticate_family_info.m.other_contacts_relation_two }</span></td>
              </tr>
           </table> 
           <div class="up_2">工作信息</div>  
           <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td width="490"><p>职业状态</p><span>${user_authenticate_work_info.m.occupation_status }</span></td>
                 <td width="490"><p>单位名称</p><span>${user_authenticate_work_info.m.company_name }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>职位</p><span>${user_authenticate_work_info.m.position }</span></td>
                 <td width="490"><p>工作邮箱</p><span>${user_authenticate_work_info.m.work_email }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>工作城市</p><span>${user_authenticate_work_info.m.work_province }:${user_authenticate_work_info.m.work_city }</span></td>
                 <td width="490"><p>现单位工作年限</p><span>${user_authenticate_work_info.m.year_limit }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>公司性质</p><span>${user_authenticate_work_info.m.company_type }</span></td>
                 <td width="490"><p>公司地址</p><span>${user_authenticate_work_info.m.company_address }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>公司行业</p><span>${user_authenticate_work_info.m.company_trades }</span></td>
                 <td width="490"><p>公司规模</p><span>${user_authenticate_work_info.m.company_size }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>月收入</p><span>${user_authenticate_work_info.m.monthly_income }</span></td>
                 <td width="490"><p>公司电话</p><span>${user_authenticate_work_info.m.company_phone }</span></td>
              </tr>
           </table> 
           <div class="up_2">资产信息</div> 
           <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td width="490"><p>是否有房</p><span>${user_authenticate_assets_info.m.housing }</span></td>
                 <td width="490"><p>是否有房贷</p><span>${user_authenticate_assets_info.m.housing_mortgage }</span></td>
              </tr>
              <tr>
                 <td width="490"><p>是否有车</p><span>${user_authenticate_assets_info.m.car }</span></td>
                 <td width="490"><p>是否有车贷</p><span>${user_authenticate_assets_info.m.car_mortgage }</span></td>
              </tr>            
           </table> 
           <div class="up_2">上传资料</div> 
           <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td class="td_1" width="490"><span>必要上传资料</span></td>
                 <td class="td_1" width="490"><span>可选上传资料</span></td>
              </tr>
              <tr>
                 <td width="490"><p>身份认证</p><span>${user_authenticate_upload_info.m.id_authenticate_url!=null?"已上传":"未上传"}</span></td>
                 <td  width="490"><p>房产认证</p><span> ${user_authenticate_upload_info.m.housing_authenticate_url!=null?"已上传":"未上传"}</span></td>
              </tr>
              <tr>
                 <td width="490"><p>信用认证</p><span>${user_authenticate_upload_info.m.credit_authenticate_url!=null?"已上传":"未上传"}</span></td>
                 <td  width="490"><p>购车认证</p><span>${user_authenticate_upload_info.m.car_authenticate_url!=null?"已上传":"未上传"}</span></td>
              </tr>
              <tr>
                 <td width="490"><p>工作认证</p><span>${user_authenticate_upload_info.m.work_authenticate_url!=null?"已上传":"未上传"}</span></td>
                 <td  width="490"><p>学历认证</p><span>${user_authenticate_upload_info.m.education_authenticate_url!=null?"已上传":"未上传"}</span></td>
              </tr>
              <tr>
                 <td width="490"><p>收入认证</p><span>${user_authenticate_upload_info.m.income_authenticate_url!=null?"已上传":"未上传"}</span></td>
               	 <td  width="490"><p> </p><span></span></td>
              </tr>
           </table> 
           <div class="down"><a href="javascript:window.close()">关&nbsp;闭</a></div>
        </div>
        
     </div>
  </div>  

<%@ include file="/jsp/index/index_foot.jsp" %>
<script src="<%=__PUBLIC__%>/js/select.js"></script>
<script type="text/javascript">
	
</script>


</body>
</html>

