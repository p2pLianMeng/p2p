<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<html xmlns="http://www.w3.org/1999/xhtml">

<body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="loan_bb loan_bb_2 loan_bb_4">
        <div class="loan_aa_top">
           <div class="up">
              <a href="#">我要借款</a>&nbsp;<span>></span>
               <c:choose>
			   	<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
			   	<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
			   </c:choose>
              <span>></span>&nbsp;填写借款申请
           </div>
           <div class="center">
              <span style="background:#ed5050; color:#fff;">1</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">2</span>
              <s></s>
              <span>3</span>
              <s></s>
              <span>4</span>
              <s></s>
              <span>5</span>
           </div>
           <div class="down">
              <span style="color:#ed5050;">填写借款申请</span>
              <span style="margin-left:55px; color:#ed5050;">填写借款信息</span>
              <span style="margin-left:60px;">审核</span>
              <span style="margin-left:60px;">筹集借款</span>
              <span style="margin-left:55px;">获得借款</span>
           </div>
        </div>
        
        <div class="loan_bb_con">
			<%@ include file="/jsp/user/authenticate/authenticate_left.jsp"%>
          <div class="right">
             <div class="right_top">资产信息</div>
             <div class="right_up">温馨提示：我们将在您的必要认证资料上传齐全后为您提交审核。</div>
             <div class="con con_2"><span>房产信息</span></div>
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;是否有房：</label>
                <p class="label_p">
                   <input value="有" name="housing" type="radio"> 有&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="无" name="housing" checked="checked" type="radio"> 无&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
             </div>
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;有无房贷：</label>
                <p class="label_p">
                   <input value="有" name="housing_mortgage" type="radio"> 有&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="无" name="housing_mortgage" checked="checked" type="radio"> 无
                </p>
             </div>  
             <div class="con con_2"><span>车产信息</span></div>
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;是否有车：</label>
                <p class="label_p">
                   <input value="有" name="car" type="radio" > 有&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="无" name="car" checked="checked" type="radio" > 无
                </p>
             </div> 
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;有无车贷：</label>
                <p class="label_p">
                   <input value="有" name="car_mortgage" type="radio"> 有&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="无" name="car_mortgage" checked="checked" type="radio"> 无
                </p>
             </div>
             <div class="con con6">
             <input type="hidden" id="apply_order_id" value="${apply_order_id}" />
                <p class="label_p5"><a href="#" onclick="save();">保存并继续</a></p>
             </div>
          </div>
        </div>
        
     </div>
  </div>  
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
<script src="<%=__PUBLIC__%>/js/select.js"></script>
<script type="text/javascript">
	
		$("input[name='housing'][value='${user_authenticate_assets_info.m.housing}']").attr("checked",'checked');
		$("input[name='housing_mortgage'][value='${user_authenticate_assets_info.m.housing_mortgage}']").attr("checked",'checked');
		$("input[name='car'][value='${user_authenticate_assets_info.m.car}']").attr("checked",'checked');
		$("input[name='car_mortgage'][value='${user_authenticate_assets_info.m.car_mortgage}']").attr("checked",'checked');
	
	
	function save(){
		var housing=$("input[name='housing']:checked").val();
		var housing_mortgage=$("input[name='housing_mortgage']:checked").val();
		var car=$("input[name='car']:checked").val();
		var car_mortgage=$("input[name='car_mortgage']:checked").val();
		var apply_order_id=$("#apply_order_id").val();
		$.ajax({
			url:"<%=__ROOT_PATH__%>/user/authentication/assets_info/save_assets_info.html",
			type:'post',
			cache:false,
			async:false,
			data:{"housing":housing,"housing_mortgage":housing_mortgage,"car":car,"car_mortgage":car_mortgage,"apply_order_id":apply_order_id},
			success:function(data){
				if(data=="ok"){
					window.location.href="<%=__ROOT_PATH__%>/user/authentication/upload_info/upload_info_index.html";
				}
			}
		},"html")
	}


</script>


</body>
</html>

