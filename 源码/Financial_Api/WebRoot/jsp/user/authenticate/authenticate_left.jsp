<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<html xmlns="http://www.w3.org/1999/xhtml">

	<body>
		<div class="left">
			<p class="p1">
				<span>信息完整度</span><span class="a">${rate}%<s></s> </span><a href="<%=__ROOT_PATH__ %>/user/authenticate/authenticate_info_preview/preview_index.html" target="_blank">预览</a>
			</p>
			<p class="p2">
				<span style="width:${rate}%"></span>
			</p>
			<p class="p3">
				<a id="persional" 
					href="<%=__ROOT_PATH__%>/user/authentication/persional_info/persional_index.html">个人信息</a>
				<a id="family" 
					href="<%=__ROOT_PATH__%>/user/authentication/family_info/family_index.html">家庭信息</a>
				<a id="work" 
					href="<%=__ROOT_PATH__%>/user/authentication/work_info/work_index.html">工作信息</a>
				<a id="assets" 
					href="<%=__ROOT_PATH__%>/user/authentication/assets_info/assets_index.html">资产信息</a>
				<a id="upload" style="border-bottom: 0px;"
					href="<%=__ROOT_PATH__%>/user/authentication/upload_info/upload_info_index.html">上传资料</a>
			</p>
			<p class="p4">
				<a id="submit" onclick="ok()" href="#">提交申请</a>
			</p>
		</div>

		<script type="text/javascript">
	var the_id = "${the_id}";
	$(function(){
		$("#"+the_id).attr("style","font-size: 16px; font-weight: bold;");
		$("#"+the_id).append("<s></s>");
		
		if(${user_authenticate_left_status.m.persional_info_status}==1){$("#persional").addClass("a")}
		if(${user_authenticate_left_status.m.family_info_status}==1){$("#family").addClass("a")}
		if(${user_authenticate_left_status.m.work_info_status}==1){$("#work").addClass("a")}
		if(${user_authenticate_left_status.m.assets_info_status}==1){$("#assets").addClass("a")}
		if(${user_authenticate_left_status.m.upload_info_status}==1){$("#upload").addClass("a")}
		if(${user_authenticate_left_status.m.persional_info_status}==1 && ${user_authenticate_left_status.m.family_info_status}==1 && ${user_authenticate_left_status.m.work_info_status}==1 && ${user_authenticate_left_status.m.assets_info_status}==1 && ${user_authenticate_left_status.m.upload_info_status}==1){
			$("#submit").css({"background-color":"#ed5050"})
		}
	})
</script>


	</body>
</html>

