<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
			             <script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
			             <link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;债权转让
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    

	<div class="geren_jyjl geren_wdzq geren_zqzr">
       <div class="left">
          <p>债权转让盈亏&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>债权转让盈亏 = 债权折溢价交易盈亏 - 债权交易费用

<img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a>
          </p><span><s>0.00</s>元</span>
          <p class="p_1">转出中的份额&nbsp;&nbsp;0份</p>
          <p class="p_1">转出中的数量&nbsp;&nbsp;0个</p>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td>成功转入金额</td>
                <td>债权转入盈亏</td>
                <td>已转入债权数量</td>
             </tr>
             <tr class="tr_2">
                <td>0.00元</td>
                <td>0.00元</td>
                <td>0份</td>
             </tr>
             <tr class="tr_1">
                <td>成功转出金额</td>
                <td>债权转出盈亏</td>
                <td>已转出债权数量</td>
             </tr>
             <tr>
                <td>0.00元</td>
                <td>0.00元</td>
                <td>0份</td>
             </tr>
          </table>
       </div>    
    </div> 
    
    <div class="geren_zqzr_con">
       <p>债权转让中常遇到的问题</p>
       <span>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
          <a href="#">什么是债权转让？</a>
       </span>
    </div>
    
    <div class="geren_wdzq_head"><span><a class="hover" href="#">转让中的债权</a><a href="#">可转出的债权</a><a href="#">已转出的债权</a><a 

href="#">已转入的债权</a></span></div>
    <div class="geren_wdzq_con geren_zqzr_con2" style="display:block">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>转让ID</td>
             <td>债权ID</td>
             <td>剩余期数</td>
             <td>年利率</td>
             <td class="td_a"><div>债权价值</div><div class="a"><a><img src="<%=__PUBLIC__%>/images/loan_3.png" /></a><span>根据定价公式计算出的债权当前的公允价值

，会根据债权的交易日期、状态等进行更新，具体公式请<a href="#">查看详情</a><img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></div></td>
             <td>转让价格</td>
             <td>转让系数</td>
             <td>剩余份数</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="8" class="td_1">没有转让中的债权</td>
          </tr>
       </table>
    </div> 
    <div class="geren_wdzq_con geren_zqzr_con2">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>剩余期数</td>
             <td>下个还款日</td>
             <td>年利率</td>             
             <td>待收本息</td>
             <td>债权价值</td>
             <td>可转份数</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="7" class="td_1">没有可转让的债权</td>
          </tr>
       </table>
       <div class="down"><input type="checkbox" /><span>全选&nbsp;&nbsp;&nbsp;&nbsp;选中 0笔 债权，所选债权的当前总价值 0.00元</span><a href="#">批

量转让</a></div>
    </div>  
    <div class="geren_wdzq_con geren_zqzr_con2">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>成交份数</td>
             <td>转出时债权总价值</td>
             <td>转出时总成交金额</td>             
             <td>实际收入</td>
             <td>交易费用</td>
             <td>转让盈亏</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="7" class="td_1">没有已转出的债权</td>
          </tr>
       </table>
    </div>  
    <div class="geren_wdzq_con geren_zqzr_con2">



  </div>    
</div>



<div class="bottom"></div>

	<script type="text/javascript">



		
	</script>

</body>
</html>

