<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fn" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
	<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
	<link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>



<%@ include file="/jsp/index/index_top.jsp"%>
 <%--<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
--%><span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">借款管理</a>&nbsp;<span>></span>&nbsp;我的申请
    </div>
    <%@include file="/jsp/user/usercenter/user_center_left.jsp" %>


 		<div class="geren_jksqcx">
       <div class="up">借款申请查询</div>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="tr_1">
             <td>借款标题</td>
             <td>借款金额</td>
             <td>年利率</td>
             <td>借款期限</td>
             <td>状态</td>
          </tr>
          
          <c:if test="${fn:length(borrower_bulk_standard_apply_order_list)<=0}">
			<tr class="tr_2">
				<td colspan="5">没有记录</td>
			</tr>
          </c:if>
          
          <c:forEach items="${borrower_bulk_standard_apply_order_list}" var="loan_apply_list" >
          	<tr>
          	<td>${loan_apply_list.m.borrow_title}</td>
          	<td>${loan_apply_list.m.borrow_all_money}</td>
          	<td>${loan_apply_list.m.annulized_rate_int}%</td>
          	<td>${loan_apply_list.m.borrow_duration}个月</td>
          	<c:if test="${loan_apply_list.m.state == 1}">
          		<td>填写借款信息</td>          	
          	</c:if>
          	<c:if test="${loan_apply_list.m.state == 2}">
          		<td>审核中</td>          	
          	</c:if>
          	<c:if test="${loan_apply_list.m.state == 3}">
          		<td>审核中</td>          	
          	</c:if>
          	<c:if test="${loan_apply_list.m.state == 4}">
          		<td>修改借款信息</td>          	
          	</c:if>
          	<c:if test="${loan_apply_list.m.state == 5}">
          		<td>审核失败</td>          	
          	</c:if>
          	<c:if test="${loan_apply_list.m.state == 6}">
          		<td>审核成功</td>          	
          	</c:if>
          </tr>
          
          </c:forEach>
          
          
       </table>
    </div> 
  

    </div>


  </div>    
</div>



<div class="bottom"></div>

	<script src="<%=__PUBLIC__%>/js/loan_manage.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(function(){
			get_repaying_debt();
		});


		function myFunction(){
			x=document.getElementById("submenu6"); 
			x.style.display="none";
		}	

		

	</script>

</body>
</html>

