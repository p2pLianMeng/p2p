<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fn" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
	<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
	<link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>



<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">借款管理</a>&nbsp;<span>></span>&nbsp;借款统计
    </div>
    <%@include file="/jsp/user/usercenter/user_center_left.jsp" %>

	<div class="geren_lctj">
       <div class="up">借款账户统计</div>
       <div class="up_2">您在联富金融总计借款&nbsp;&nbsp;0.00元，总计需支付&nbsp;&nbsp;0.00元的利息和费用</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td width="81%" class="td_2">借款利息</td>
                <td width="19%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">逾期费用</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">提前还款违约金</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">借款管理费</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">总计</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right">您在联富金融还没有借款</div>
       
       <div class="up_2">您在联富金融的借款待还总额为&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td width="81%" class="td_2">待还本金</td>
                <td width="19%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">待还利息</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">待还逾期费用</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">待还借款管理费</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">总计</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right">您没有待还金额</div>
       
       <div class="up_2">您在联富金融的借款已还总额为&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td width="81%" class="td_2">已还本金</td>
                <td width="19%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">已还利息</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">已交逾期费用</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">已交提前还款违约金</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">已交借款管理费</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">总计</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right">您没有已还金额</div>
       
       <div class="up_2">最近一年您在联富金融成功借款&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td width="81%" class="td_2">2013年第4季度</td>
                <td width="19%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">2014年第1季度</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">2014年第2季度</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">2014年第3季度</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">2014年第4季度</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">总计</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right"><img src="<%=__PUBLIC__%>/images/geren_2.jpg" /></div>
       
       <div class="up_2">最近六个月您在联富金融还款总计为&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td width="81%" class="td_2">2014年06月</td>
                <td width="19%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">2014年07月</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">2014年08月</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">2014年09月</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">2014年10月</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">2014年11月</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">总计</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right"><img src="<%=__PUBLIC__%>/images/geren_2.jpg" /></div>
    </div>
  

    </div>


  </div>    
</div>



<div class="bottom"></div>

	<script type="text/javascript">


	</script>

</body>
</html>

