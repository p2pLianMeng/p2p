<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">账户管理</a>&nbsp;<span>></span>&nbsp;<a href="#">个人基础信息</a>
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jcxx" style="padding:40px; width:740px;">
       <div class="up"><span>个人基础信息</span></div>
       <div class="left"><a href="#"><img src="<%=__PUBLIC__ %>/images/geren_2.png" /></a></div>
       <div class="con"><label class="label_1"><span>*</span>昵称：</label><p>${nickname2}</p></div>
       <div class="con"><label class="label_1"><span>*</span>真实姓名：</label><p><span>${name1==null?name:name1 }</span><s id="name">${name1==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>身份证号：</label><p><span>${user_identity2==null?user_identity1:user_identity2 }</span><s id="useridentity">${user_identity2==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>手机号码：</label><p><span>${phone2==null?phone1:phone2 }</span><s id="phone">${phone2==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>邮箱地址：</label><p><span>${email==null?null:email }</span><s id="email">${email==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>性别：	  </label><p>${user_identity2==null?user_identity1:a/2==0?"女":"男"}</p></div>
       <div class="con"><label class="label_1"><span>*</span>出生日期：</label><p>${user_identity2==null?user_identity:birthday }</p></div>
    </div>
       
  </div>    
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var name = $("#name").html();
		if(name=="未认证"){
			$("#name").css("color","red");
		}else{
			$("#name").css("color","green");
		}
		var useridentity = $("#useridentity").html();
		if(useridentity=="未认证"){
			$("#useridentity").css("color","red");
		}else{
			$("#useridentity").css("color","green");
		}
		var phone = $("#phone").html();
		if(phone=="未认证"){
			$("#phone").css("color","red");
		}else{
			$("#phone").css("color","green");
		}
		var email = $("#email").html();
		if(email=="未认证"){
			$("#email").css("color","red");
		}else{
			$("#email").css("color","green");
		}
	});
</script>


<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>
