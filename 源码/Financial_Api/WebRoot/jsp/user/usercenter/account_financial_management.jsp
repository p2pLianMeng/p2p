<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;理财统计
    </div>
   <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_lctj">
       <div class="up">理财账户统计</div>
       <div class="up_2">您通过联富金融平台已赚取&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td class="td_2" width="30%">U计划收益</td>
                <td width="57%"></td>
                <td width="13%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2" rowspan="4">债权收益</td>
                <td class="td_1">已赚利息</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_1">已赚罚息</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_1">已赚违约金</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td>债权转让盈亏</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">总计</td>
                <td>&nbsp;</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right"><img src="images/geren_2.jpg" /></div>
       
       <div class="up_2">您通过联富金融平台累计投资&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td class="td_2" width="30%">联富金融</td>
                <td width="57%"></td>
                <td width="13%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2" rowspan="4">债权</td>
                <td class="td_1">信用认证标</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_1">机构担保标</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_1">实地认证标</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td>智能理财标</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">总计</td>
                <td>&nbsp;</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right">您在联富金融没有投资</div>
       
       <div class="up_2">您当前在联富金融的理财账户资产为&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td class="td_2" width="30%">联富金融</td>
                <td width="57%"></td>
                <td width="13%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2" rowspan="4">债权</td>
                <td class="td_1">信用认证标</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_1">机构担保标</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_1">实地认证标</td>
                <td class="td_1">0.00</td>
             </tr>
             <tr class="tr_2">
                <td>智能理财标</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">总计</td>
                <td>&nbsp;</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right">您当前在联富金融没有投资</div>
       
       <div class="up_2">您当前在联富金融投资的债权待收收益为&nbsp;&nbsp;0.00元</div>
       <div class="left">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td width="81%" class="td_2">信用认证标</td>
                <td width="19%">0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">机构担保标</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">实地认证标</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_2">
                <td class="td_2">智能理财标</td>
                <td>0.00</td>
             </tr>
             <tr class="tr_1">
                <td class="td_2">总计</td>
                <td>0.00</td>
             </tr>
          </table>
       </div>
       <div class="right">您在联富金融没有债权待收收益</div>
    </div>
    
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>

