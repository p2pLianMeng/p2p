<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@taglib uri="http://com.tfoll.web/lable/menu_show_tag" prefix="tfoll-menu" %>

     <script type="text/javascript">
		function change_style(obj){
			//alert("ss");
			//移除所有的a标签的 样式
			$("#left_ul li a").removeAttr("style");
			//当前对象的样式改变
			obj.style.background = "#f8bbb2";
			obj.style.color = "#fff";
			//当前对象的父级li 样式呈现
			//获取当前对象的父级节点
			var parentObject = obj.parentNode;
			//让当前a标签所在不的li列表呈现
			parentObject.style.display = "block";
			//===============
			//获取父级节点的上一个兄弟节点
			var parentObjectId = parentObject.id;
			//alert(parentObjectId);
			var object = $("#"+parentObjectId);
			object.prev().children().css("background","#ed5050");
			object.prev().children().css("color","#fff");
			var currentObjectId = obj.id;
			var parentObjectId = "";
			//alert(object.prev().children());
			if(obj.id == "recharge_record" || obj.id == "chongzhi" || obj.id == "tixian"){
				parentObjectId = "zijinguanli";//资金管理
			}
			if(obj.id == "wodezhaiquan" || obj.id == "wodelianfubao" || obj.id == "zhaiquanzhuanrang" || obj.id =="licaitongji"){
				parentObjectId = "licaiguanli";//理财管理
			}
			if(obj.id == "wodejiekuan" || obj.id == "jiekuanshengqingchaxun" || obj.id == "jiekuantongji"){
				parentObjectId = "jiekuanguanli";//借款管理
			}
			if(obj.id == "gerenjichuxinxi" || obj.id == "renzhengxinxi" || obj.id == "anquanxinxi" || obj.id == "yinhangkaxinxi"){
				parentObjectId = "zhangfuguanli";//账户管理
			}

			
			$.ajax({
				url:"<%=__ROOT_PATH__%>/user/usercenter/save_or_update_dump_path.html",
				async:false,//同步
				type: "post",
				data:{"currentObjectId":currentObjectId ,"parentObjectId":parentObjectId},
				success:function(data){
					//alert("返回值 "+data);
				}
				
			});
			
		}

		//点击父级 改变父级样式
//		function change_parent_object_style(obj){
//			$("#left_ul li a").removeAttr("style");
//			obj.style.background = "#ed5050";
//			obj.style.color = "#fff";
			
//		}

		function showsubmenu(sid){
		    var whichEl = document.getElementById("submenu" + sid);
		    whichEl.style.display = whichEl.style.display =='none'?'':'none';
		}
</script>
    <div class="geren_left" >
      <ul id="left_ul">
      <% String url = request.getRequestURI();%>
        <li>
        	<a id="gerenxinxi" class="a" style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/user/usercenter/into_user_center.html">
        		<span style="background:url(<%=__PUBLIC__%>/images/geren_1.png) right 0 no-repeat;"></span>
        		个人信息
        	</a>
       </li>
       
        <li onClick="showsubmenu(1)">
        	<a id="zijinguanli" onclick="change_parent_object_style(this);" class="b" href="#" ><span></span>资金管理</a>
        </li>
        <li id="submenu1" class="submenu">
           <a id="chongzhi" onclick="change_style(this)" href="<%=__ROOT_PATH__%>/user/usercenter/recharge/into_user_recharge_index.html"><span></span>充值</a>
           <a id="tixian" onclick="change_style(this)" href="<%=__ROOT_PATH__ %>/user/usercenter/withdrawal/to_user_withdraw.html" ><span></span>提现</a>
           <a id="recharge_record" onclick="change_style(this)" href="<%=__ROOT_PATH__%>/user/user_center/money_mange/recharge_records/query_recharge_records.html"  ><span></span>交易记录</a>
        </li>
        <c:if test="${sessionScope.user.m.user_type==1}">
        <!-- lend -->
        <li onClick="showsubmenu(2)">
        	<a id="licaiguanli" onclick="change_parent_object_style(this);" class="c" href="#"><span></span>理财管理</a>
        </li>
        <li id="submenu2" class="submenu">
           <a id="wodelianfubao" onclick="change_style(this);" href="<%=__ROOT_PATH__%>/user/financial/lfollinvest_user_info/goto_user_fix_bid_manage.html" ><span></span>联富宝</a>
           <a id="wodezhaiquan" onclick="change_style(this);" href="<%=__ROOT_PATH__%>/user/usercenter/financial_manage/to_my_debt.html" ><span></span>我的债权</a>
           <a id="zhaiquanzhuanrang"  onclick="change_style(this);" href="<%=__ROOT_PATH__%>/user/usercenter/financial_manage/to_debt_transfer.html"><span></span>债权转让</a>
           <a id="licaitongji" onclick="change_style(this);" href="<%=__ROOT_PATH__%>/user/usercenter/financial_manage/to_finance_statistics.html"><span></span>理财统计</a>
        </li>
        <!-- lend -->
        </c:if>
         <c:if test="${sessionScope.user.m.user_type==2}">
        <!-- borrow -->
        <li onClick="showsubmenu(3)">
        	<a id="jiekuanguanli" onclick="change_parent_object_style(this);" class="d" href="#"><span></span>借款管理</a>
        </li>
        <li id="submenu3" class="submenu">
           <a id="jiekuanshengqingchaxun" onclick="change_style(this);"  href="<%=__ROOT_PATH__%>/user/usercenter/loan_manage/to_my_loan_apply.html"><span></span>我的申请</a>
           <a id="wodejiekuan" onclick="change_style(this);"  href="<%=__ROOT_PATH__%>/user/usercenter/loan_manage/to_my_loan_page.html"><span></span>借款查询</a>
           <a id="jiekuantongji" onclick="change_style(this);" href="<%=__ROOT_PATH__%>/user/usercenter/loan_manage/to_loan_statistics.html"><span></span>借款统计</a>
        </li>
        <!-- borrow -->
        </c:if>
        <li onClick="showsubmenu(4)">
        	<a id="zhangfuguanli" onclick="change_parent_object_style(this);"  class="e" href="#"><span></span>账户管理</a>
        </li>
        <li id="submenu4" class="submenu" >
           <a id="gerenjichuxinxi" onclick="change_style(this);"  href="<%=__ROOT_PATH__ %>/user/usercenter/to_personal_information.html"><span></span>基础信息</a>
           <a id="anquanxinxi" onclick="change_style(this);"  href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html"><span></span>安全管理</a>
           <c:if test="${sessionScope.user.m.user_type==2}">
             <a id="renzhengxinxi" onclick="change_style(this);"  href="<%=__ROOT_PATH__ %>/user/usercenter/user_credit_files.html"><span></span>信用认证</a>
           </c:if>
           <a id="yinhangkaxinxi" onclick="change_style(this);"  href="<%=__ROOT_PATH__ %>/user/usercenter/bank_info/bank_info_index.html"><span></span>银行卡管理</a>
        </li>
        <li><a class="g" href="<%=__ROOT_PATH__ %>/index/activity/to_activity.html" style="background:#ed5050;color:#fff;"><span style="background:url(<%=__ROOT_PATH__ %>/images/geren_1.png) right -138px no-repeat;"></span>平台活动</a></li>
        <li>
        	<a id="zhanleixin" onclick="change_parent_object_style(this);" class="f" href="<%=__ROOT_PATH__ %>/user/usercenter/to_station_information.html"><span></span>站内消息</a>
        </li>
        <li id="submenu5" class="submenu" style="display:none;"></li>
      </ul>
    </div>
    <input type="hidden" id="v1_curr_id" value="${sessionScope.curr_id}">
    <input type="hidden" id="v2_pare_id" value="${sessionScope.pare_id}">
<script type="text/javascript">
	$(document).ready(function(){
	 	//alert("Dom树加载后执行!!");
	 	var v1 = $("#v1_curr_id").val();
		var v2 = $("#v2_pare_id").val();
		//alert(v1);
		//alert(v2);
		if(v1 != "" && v2 != ""){
			$("#gerenxinxi").removeAttr("style");
			$("#"+v1).css("background","#f8bbb2");
			$("#"+v1).css("color","#fff");

			$("#"+v2).css("background","#ed5050");
			$("#"+v2).css("color","#fff");
		}
		
		
	});

	$(".submenu").bind("click",show_function);

	function show_function(event){

		
		var li = event.currentTarget;
		var $li = $(li);
		$li.parent().siblings().children().hide();
	}



	
	
</script>