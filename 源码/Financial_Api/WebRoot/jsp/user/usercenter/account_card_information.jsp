<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>${applicationScope.title}</title>

		<link href="style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
	</head>
	<body>

		<%@ include file="/jsp/index/index_top.jsp"%>
		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="geren_up">
					<a href="geren.html">我的联富金融</a>&nbsp;
					<span>></span>&nbsp;
					<a href="#">账户管理</a>&nbsp;
					<span>></span>&nbsp;
					<a href="#">银行卡信息</a>
				</div>
				<%@ include file="/jsp/user/usercenter/user_center_left.jsp"%>
				<div class="geren_yhkxx">
					<div class="up">
						已添加银行卡<input type="button" id="a" style="width:80px;height:30px;  font-size:15px;color:white;cursor:pointer;  background-color: #ed5050;margin-left: 30px;"  value="我要提现"/>
					</div>
					<div id="bank_one" class="con">
						<img id="img1" src="" />
						<span>${bank_card_num_one}</span>
						<link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
						<p>
							<a onClick="openme2('div1','div2')">修改</a><a
								onclick="del('_one','bank_one');">删除</a>
						</p>
						<div id="div1" class="loginDiv1"></div>
						<div id="div2" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="geren_yhkxx_con">
								<input type="button" class="guanbi"
									onClick="closeme2('div1','div2')" value="ｘ" />
								<div class="up">
									修改银行卡
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户名
									</label>
									<p class="p_1">
										${user.m.real_name}
									</p>
									<p class="p_1">
										请添加相同开户名的银行卡
									</p>

								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>银行卡号
									</label>
									<p class="p_1">
										${bank_card_num_one}
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>选择银行
									</label>
									<select id="bank_name_one" class="select_1">
										<option>

										</option>
										<option>
											中国工商银行
										</option>
										<option>
											中国农业银行
										</option>
										<option>
											招商银行
										</option>
										<option>
											中国银行
										</option>
										<option>
											中国建设银行
										</option>
										<option>
											广发银行
										</option>
										<option>
											兴业银行
										</option>
										<option>
											交通银行
										</option>
										<option>
											中国邮政储蓄银行
										</option>
										<option>
											浦发银行
										</option>
										<option>
											中国民生银行
										</option>
										<option>
											中国光大银行
										</option>
										<option>
											中信银行
										</option>
										<option>
											平安银行
										</option>
										<option>
											华夏银行
										</option>
										<option>
											城市信用社
										</option>
										<option>
											恒丰银行
										</option>
										<option>
											中国农业发展银行
										</option>
										<option>
											中国商业银行
										</option>
										<option>
											农村信用社
										</option>
										<option>
											农村合作银行
										</option>
										<option>
											浙商银行
										</option>
										<option>
											上海农商银行
										</option>
										<option>
											中国进出口银行
										</option>
										<option>
											渤海银行
										</option>
										<option>
											国家开发银行
										</option>
										<option>
											徽商银行
										</option>
									</select>
									<p id="0_one" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写银行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行所在地
									</label>
									<input id="bank_address_one" class="text_1" type="text" />
									<p id="1_one" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写开户行所在地
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行
									</label>
									<input id="opening_bank_one" class="text_1" type="text" />
									<p id="2_one" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写开户行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>资金密码
									</label>
									<input id="code_one" class="text_1" type="text" />
									<p id="3_one" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写资金密码
									</p>
								</div>
								<div class="con_1 con_2">
									<a
										onclick="update('_one','div1','div2','bank_name_one','bank_address_one','opening_bank_one','code_one');"
										class="a_2">保存</a><a href="#" onclick="closeme2('div1','div2')" class="a_3">取消</a>
								</div>
								<div class="con_1 con_3">
									<s>温馨提示</s>
								</div>
								<div class="con_1 con_4">
									<s>1、如果您填写的开户行支行不正确，提现交易将无法成功，提现费用不予返还。<br />2、如果您不确定开户行支行名称，可打电话到当地所在银行的营业网点询问或上网查询。<br />3、提现时，不能选择将资金利息等提到信用卡账户中。</s>
								</div>
							</div>
						</div>
					</div>

					<div id="bank_two" class="con">
						<img id="img2" src="" />
						<span>${bank_card_num_two }</span>
						<link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
						<p>
							<a onClick="openme2('div3','div4')">修改</a>
							<a onclick="del('_two','bank_two');">删除</a>
						</p>
						<div id="div3" class="loginDiv1"></div>
						<div id="div4" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="geren_yhkxx_con">
								<input type="button" class="guanbi"
									onClick="closeme2('div3','div4')" value="ｘ" />
								<div class="up">
									修改银行卡
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户名
									</label>
									<p class="p_1">
										${user.m.real_name}
									</p>
									<p class="p_1">
										请添加相同开户名的银行卡
									</p>

								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>银行卡号
									</label>
									<p class="p_1">
										${bank_card_num_two }
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>选择银行
									</label>
									<select id="bank_name_two" class="select_1">
										<option>

										</option>
										<option>
											中国工商银行
										</option>
										<option>
											中国农业银行
										</option>
										<option>
											招商银行
										</option>
										<option>
											中国银行
										</option>
										<option>
											中国建设银行
										</option>
										<option>
											广发银行
										</option>
										<option>
											兴业银行
										</option>
										<option>
											交通银行
										</option>
										<option>
											中国邮政储蓄银行
										</option>
										<option>
											浦发银行
										</option>
										<option>
											中国民生银行
										</option>
										<option>
											中国光大银行
										</option>
										<option>
											中信银行
										</option>
										<option>
											平安银行
										</option>
										<option>
											华夏银行
										</option>
										<option>
											城市信用社
										</option>
										<option>
											恒丰银行
										</option>
										<option>
											中国农业发展银行
										</option>
										<option>
											中国商业银行
										</option>
										<option>
											农村信用社
										</option>
										<option>
											农村合作银行
										</option>
										<option>
											浙商银行
										</option>
										<option>
											上海农商银行
										</option>
										<option>
											中国进出口银行
										</option>
										<option>
											渤海银行
										</option>
										<option>
											国家开发银行
										</option>
										<option>
											徽商银行
										</option>
									</select>
									<p id="0_two" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写银行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行所在地
									</label>
									<input id="bank_address_two" class="text_1" type="text" />
									<p id="1_two" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写开户行所在地
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行
									</label>
									<input id="opening_bank_two" class="text_1" type="text" />
									<p id="2_two" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写开户行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>资金密码
									</label>
									<input id="code_two" class="text_1" type="text" />
									<p id="3_two" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写资金密码
									</p>
								</div>
								<div class="con_1 con_2">
									<a
										onclick="update('_two','div3','div4','bank_name_two','bank_address_two','opening_bank_two','code_two');"
										class="a_2">保存</a><a href="#" onClick="closeme2('div3','div4')" class="a_3">取消</a>
								</div>
								<div class="con_1 con_3">
									<s>温馨提示</s>
								</div>
								<div class="con_1 con_4">
									<s>1、如果您填写的开户行支行不正确，提现交易将无法成功，提现费用不予返还。<br />2、如果您不确定开户行支行名称，可打电话到当地所在银行的营业网点询问或上网查询。<br />3、提现时，不能选择将资金利息等提到信用卡账户中。</s>
								</div>
							</div>
						</div>
					</div>

					<div id="bank_three" class="con">
						<img id="img3" src="" />
						<span>${bank_card_num_three}</span>
						<link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
						<p>
							<a onClick="openme2('div5','div6')">修改</a><a
								onclick="del('_three','bank_three');">删除</a>
						</p>
						<div id="div5" class="loginDiv1"></div>
						<div id="div6" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="geren_yhkxx_con">
								<input type="button" class="guanbi"
									onClick="closeme2('div5','div6')" value="ｘ" />
								<div class="up">
									修改银行卡
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户名
									</label>
									<p class="p_1">
										${user.m.real_name }
									</p>
									<p class="p_1">
										请添加相同开户名的银行卡
									</p>

								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>银行卡号
									</label>
									<p class="p_1">
										${bank_card_num_three}
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>选择银行
									</label>
									<select id="bank_name_three" class="select_1">
										<option>

										</option>
										<option>
											中国工商银行
										</option>
										<option>
											中国农业银行
										</option>
										<option>
											招商银行
										</option>
										<option>
											中国银行
										</option>
										<option>
											中国建设银行
										</option>
										<option>
											广发银行
										</option>
										<option>
											兴业银行
										</option>
										<option>
											交通银行
										</option>
										<option>
											中国邮政储蓄银行
										</option>
										<option>
											浦发银行
										</option>
										<option>
											中国民生银行
										</option>
										<option>
											中国光大银行
										</option>
										<option>
											中信银行
										</option>
										<option>
											平安银行
										</option>
										<option>
											华夏银行
										</option>
										<option>
											城市信用社
										</option>
										<option>
											恒丰银行
										</option>
										<option>
											中国农业发展银行
										</option>
										<option>
											中国商业银行
										</option>
										<option>
											农村信用社
										</option>
										<option>
											农村合作银行
										</option>
										<option>
											浙商银行
										</option>
										<option>
											上海农商银行
										</option>
										<option>
											中国进出口银行
										</option>
										<option>
											渤海银行
										</option>
										<option>
											国家开发银行
										</option>
										<option>
											徽商银行
										</option>
									</select>
									<p id="0_three" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写银行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行所在地
									</label>
									<input id="bank_address_three" class="text_1" type="text" />
									<p id="1_three" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写开户行所在地
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行
									</label>
									<input id="opening_bank_three" class="text_1" type="text" />
									<p id="2_three" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写开户行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>资金密码
									</label>
									<input id="code_three" class="text_1" type="text" />
									<p id="3_three" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;请填写资金密码
									</p>
								</div>
								<div class="con_1 con_2">
									<a
										onclick="update('_three','div5','div6','bank_name_three','bank_address_three','opening_bank_three','code_three');"
										class="a_2">保存</a><a href="#" onClick="closeme2('div5','div6')" class="a_3">取消</a>
								</div>
								<div class="con_1 con_3">
									<s>温馨提示</s>
								</div>
								<div class="con_1 con_4">
									<s>1、如果您填写的开户行支行不正确，提现交易将无法成功，提现费用不予返还。<br />2、如果您不确定开户行支行名称，可打电话到当地所在银行的营业网点询问或上网查询。<br />3、提现时，不能选择将资金利息等提到信用卡账户中。</s>
								</div>
							</div>
						</div>
					</div>

					<div class="con">
						<img src="<%=__PUBLIC__ %>/images/chongzhi_0.jpg" />
						<p>
							<a onClick="openme2('loginDiv3','loginDiv4');">新增银行卡</a>
						</p>
						<div id="loginDiv3" class="loginDiv1"></div>
						<div id="loginDiv4" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="geren_yhkxx_con">
								<input type="button" class="guanbi"
									onClick="closeme2('loginDiv3','loginDiv4')" value="ｘ" />
								<div class="up">
									添加银行卡
								</div>
								<p id="no_money_password" style="color: red; display: none"
									class="label_p7">
									&nbsp;&nbsp;请到用户中心补充资金密码
								</p>
								<p id="no_real_name" style="color: red; display: none"
									class="label_p7">
									&nbsp;&nbsp;请到用户中心实名认证
								</p>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户名
									</label>
									<p class="p_1">
										${user.m.real_name}
									</p>
									<p class="p_1">
										请添加相同开户名的银行卡
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>选择银行
									</label>
									<select id="bank_name" class="select_1">
										<option>

										</option>
										<option>
											中国工商银行
										</option>
										<option>
											中国农业银行
										</option>
										<option>
											招商银行
										</option>
										<option>
											中国银行
										</option>
										<option>
											中国建设银行
										</option>
										<option>
											广发银行
										</option>
										<option>
											兴业银行
										</option>
										<option>
											交通银行
										</option>
										<option>
											中国邮政储蓄银行
										</option>
										<option>
											浦发银行
										</option>
										<option>
											中国民生银行
										</option>
										<option>
											中国光大银行
										</option>
										<option>
											中信银行
										</option>
										<option>
											平安银行
										</option>
										<option>
											华夏银行
										</option>
										<option>
											城市信用社
										</option>
										<option>
											恒丰银行
										</option>
										<option>
											中国农业发展银行
										</option>
										<option>
											中国商业银行
										</option>
										<option>
											农村信用社
										</option>
										<option>
											农村合作银行
										</option>
										<option>
											浙商银行
										</option>
										<option>
											上海农商银行
										</option>
										<option>
											中国进出口银行
										</option>
										<option>
											渤海银行
										</option>
										<option>
											国家开发银行
										</option>
										<option>
											徽商银行
										</option>
									</select>
									<p id="0" style="color: red; display: none" class="label_p7">
										&nbsp;&nbsp;请选择银行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行所在地
									</label>
									<input id="bank_address" class="text_1" type="text" />
									<p id="1" style="color: red; display: none" class="label_p7">
										&nbsp;&nbsp;请填写开户行所在地
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>开户行
									</label>
									<input id="opening_bank" class="text_1" type="text" />
									<p id="2" style="color: red; display: none" class="label_p7">
										&nbsp;&nbsp;请填写开户行
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>银行卡号
									</label>
									<input id="bank_card_num" class="text_1" type="text" />
									<p id="3" style="color: red; display: none" class="label_p7">
										&nbsp;&nbsp;请填写银行卡号
									</p>
									<p id="31" style="color: red; display: none" class="label_p7">
										&nbsp;&nbsp;请填写正确的银行卡号
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>确认卡号
									</label>
									<input id="bank_card_num_two" class="text_1" type="text" />
									<p id="card_num" style="color: red; display: none"
										class="label_p7">
										&nbsp;&nbsp;两次输入不同，请重新填写
									</p>
								</div>
								<div class="con_1">
									<label class="label_1">
										<s>*</s>资金密码
									</label>
									<input id="code" class="text_1" type="password" />
									<p id="4" style="color: red; display: none" class="label_p7">
										&nbsp;&nbsp;请输入资金密码
									</p>
									<p id="err" style="color: red; display: none" class="label_p7">
										&nbsp;&nbsp;资金密码错误
									</p>
								</div>
								<div class="con_1 con_2">
									<a onclick="add()" class="a_2">保存</a><a href="#" onClick="closeme2('loginDiv3','loginDiv4')" class="a_3">取消</a>
								</div>
								<div class="con_1 con_3">
									<s>温馨提示</s>
								</div>
								<div class="con_1 con_4">
									<s>1、如果您填写的开户行支行不正确，提现交易将无法成功，提现费用不予返还。<br />2、如果您不确定开户行支行名称，可打电话到当地所在银行的营业网点询问或上网查询。<br />3、提现时，不能选择将资金利息等提到信用卡账户中。</s>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		</div>
		</div>



		<%@ include file="/jsp/index/index_foot.jsp"%>
		<script src="<%= __PUBLIC__ %>/js/bank_img.js"></script>
		<script type="text/javascript">
		$("#a").click(function(){
			window.location.href="<%=__ROOT_PATH__%>/user/usercenter/withdrawal/to_user_withdraw.html";
		});
function add(){
	var bank_name = $("#bank_name").val();
	
	var bank_address = $("#bank_address").val();
	var opening_bank = $("#opening_bank").val();

	var bank_card_num = $("#bank_card_num").val();
	if(bank_card_num.length>19||bank_card_num.length<16){
		$("#31").show();
		return;
	}
	var bank_card_num_two = $("#bank_card_num_two").val();
	var code = $("#code").val();
	if(bank_card_num != bank_card_num_two){$("#card_num").show();return;};
	var i = true;
	if(i){
	i=false;
	$.ajax({
		type:'post',
		url:"<%=__ROOT_PATH__%>/user/usercenter/bank_info/bank_info_add.html",
		async:false,
		cache:false,
		data:{"bank_name":bank_name,"bank_address":bank_address,"opening_bank":opening_bank,"bank_card_num":bank_card_num,"code":code},
		success:function(data){
			if(data == "no_money_password"){
				 $("#no_money_password").show(); 
			}else if(data=="over"){
				closeme2('loginDiv3','loginDiv4');
				showMessage(["提示","添加银行卡上限为三张!"]); 
			}else if(data=="ok"){
				closeme2('loginDiv3','loginDiv4');
				window.location.href="<%=__ROOT_PATH__%>/user/usercenter/bank_info/bank_info_index.html";
			}else if(data=="no"){
				closeme2('loginDiv3','loginDiv4');
				showMessage(["提示","失败!"]); 
			}else if(data=="err"){
				$("#err").show(); 
			}else {
				$("#"+data).show(); 
			}
		}
	},"html","application/x-www-form-urlencoded; charset=utf-8");
	i=true;
	}
}

function update(num,i,j,bank_name,bank_address,opening_bank,code){
	var bank_name = $("#"+bank_name).val();
	var bank_address = $("#"+bank_address).val();
	var opening_bank = $("#"+opening_bank).val();
	var code = $("#"+code).val();
	$.ajax({
		type:'post',
		url:"<%=__ROOT_PATH__%>/user/usercenter/bank_info/bank_info_update.html",
		async:false,
		cache:false,
		data:{"bank_name":bank_name,"bank_address":bank_address,"opening_bank":opening_bank,"code":code,"num":num},
		success:function(data){
			if(data=="ok"){
				closeme2(i,j); 
				showMessage(["提示","成功!"]); 
			}else if(data=="no"){
				closeme2(i,j); 
				showMessage(["提示","失败!"]); 
			}else {
				$("#"+data).show(); 
			}
		}
	},"html","application/x-www-form-urlencoded; charset=utf-8");
}

function del(num,i){
	$.ajax({
		type:'post',
		url:"<%=__ROOT_PATH__%>/user/usercenter/bank_info/bank_info_delete.html",
		async:false,
		cache:false,
		data:{"num":num},
		success:function(data){
			if(data=="ok"){ 
				showMessage(["提示","成功!"]); 
				$("#"+i).hide();
			}
			else if(data=="no"){ showMessage(["提示","失败!"]); }
			else if(data=="err"){ showMessage(["提示","错误!"]); }
		}
	},"html","application/x-www-form-urlencoded; charset=utf-8");
}

$(function(){
	var bank_name_one = "${user_bank_info.m.bank_name_one}";
	var bank_name_two = "${user_bank_info.m.bank_name_two}";
	var bank_name_three = "${user_bank_info.m.bank_name_three}";
	bank_img(bank_name_one,bank_name_two,bank_name_three);
	
	if(${user_bank_info.m.bank_card_num_one==null}){$("#bank_one").hide()}
	if(${user_bank_info.m.bank_card_num_two==null}){$("#bank_two").hide()}
	if(${user_bank_info.m.bank_card_num_three==null}){$("#bank_three").hide()}
})


</script>
	</body>


</html>

