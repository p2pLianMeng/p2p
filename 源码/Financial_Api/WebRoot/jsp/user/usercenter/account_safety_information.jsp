<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>${applicationScope.title}</title>
		<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
	</head>
	<body>

		<%@ include file="/jsp/index/index_top.jsp"%>

		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="geren_up">
					<a href="geren.html">我的联富金融</a>&nbsp;
					<span>></span>&nbsp;
					<a href="#">账户管理</a>&nbsp;
					<span>></span>&nbsp;
					<a href="">安全信息</a>
				</div>
				<%@ include file="/jsp/user/usercenter/user_center_left.jsp"%>

				<div class="geren_aqxx">
					<div class="up">
						安全信息
					</div>
					<div class="geren_aqxx_con">
						<ul>
							<li>
								<div class="con">
									<h6></h6>
									<p>
										昵称
									</p>
									<span>已设置</span><s>${nickname2}</s>
								</div>
							</li>
							<li>
								<div class="con">
									<h6 style="background-position: 0 -88px"></h6>
									<p>
										实名认证
									</p>
									<span style="color: gray;">${user_identity2==null?"未设置":user_identity2}</span><s><c:if
											test="${ user_identity2==null }">
											<a onClick="showsubmenu(6)">设置</a>
										</c:if> <c:if test="${ user_identity2!=null }">${name1}</c:if> </s>
								</div>
							</li>
							<li id="submenu6" class="submenu_1" style="display: none;">
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;真实姓名：
									</label>
									<input class="password_1" id="real_name" type="text">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;身份证号：
									</label>
									<input class="password_1" id="identity" type="text">
								</div>
								<div class="con_4">
									<a onclick="authenticate();">提交</a>
								</div>
							</li>
							<li>
								<div class="con">
									<h6 style="background-position: 0 -44px"></h6>
									<p>
										绑定邮箱
									</p>
									<span style="color: #ff9900;">${email==null?"未绑定":"已绑定"
										}</span><s><c:if test="${email==null}">
											<a onClick="showsubmenu(30)">设置</a>
										</c:if> <c:if test="${email!=null}">${email }</c:if> </s>
								</div>
							</li>

							<li id="submenu30" class="submenu_1 submenu_2"
								style="display: none;">
								<div class="con_2">
									为了您的账户安全，请验证邮箱！
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										邮箱地址：
									</label>
									<input style="margin-left: 5px;" id="email" class="password_1"
										type="text">
								</div>

								<div class="con_2 con_3">
									<label class="label_1">
										邮箱验证码：
									</label>
									<input style="margin-left: 5px;" id="email_code"
										class="password_1 password_2" type="text">
									<input class="button_1" id="send_email" type="button"
										value="发送验证码" />
								</div>
								<div class="con_4">
									<a id="bd_email">提交</a>
								</div>
								<li>
									<div class="con">
										<h6 style="background-position: 0 -137px"></h6>
										<p>
											登录密码
										</p>
										<span>已设置</span><s><a onClick="showsubmenu(10)">修改</a> </s>
									</div>
								</li>
									<li id="submenu10" class="submenu_1" style="display: none;">
								<div class="con_2">
									为了您的账户安全，请定期更换登录密码，并确保登录密码设置与提现密码不同。
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;原密码：
									</label>
									<input class="password_1" id="old_password" type="password">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;新密码：
									</label>
									<input class="password_1" id="new_password" type="password">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;确认新密码：
									</label>
									<input class="password_1" id="ok_password" type="password">
								</div>
								<div class="con_4">
									<a id="tijiao" onclick="onsumbit();">提交</a>
								</div>
							</li>
							<li>
								<div class="con">
									<h6 style="background-position: 0 -188px"></h6>
									<p>
										绑定手机
									</p>
									<span><c:if test="${phone2 == null}">未绑定</c:if> <c:if
											test="${phone2 != null}">已绑定</c:if> </span><s> <c:if
											test="${phone2==null}">
											<a onClick="showsubmenu(7)">设置</a>
										</c:if> <c:if test="${phone2!=null}">
											<a onClick="showsubmenu(11)">修改</a>
										</c:if> </s>
								</div>
							</li>
							<li id="submenu7" class="submenu_1 submenu_2"
								style="display: none;">
								<div class="con_2">
									为了您的账户安全，请验证手机！
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										新手机号：
									</label>
									<input class="password_1" id="phone" type="text">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										验证码：
									</label>
									<input class="password_1 password_2" id="binding_phone_code"
										type="text">
									<input class="button_1" type="button" id="binding_phone_send_message" value="发送验证码" />
									<input class="button_1" style="display:none;" type="button" id="binding_phone_send_voice" value="语音验证码" />
									<br /><span id="bingd_phone" style="display:none;">没有收到短信验证码 ?<a style="corsur:pointer;color:red;" id="bingd_phone_send">发送语音验证码</a></span>
								</div>
								<div class="con_4">
									<a id="tijiao4">提交</a>
								</div>
							</li>
									<li id="submenu11" class="submenu_1 submenu_2"
								style="display: none;">
								<div class="con_2">
									为了您的账户安全，请验证手机！
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										旧手机号：
									</label>
									<input class="password_1" id="old_tel" type="text">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										新手机号：
									</label>
									<input class="password_1" id="tel" type="text">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										验证码：
									</label>
									<input class="password_1 password_2" id="update_phone_code"
										type="text">
									<input class="button_1" id="update_phone_send_message"
										type="button" value="发送验证码" />
									<input class="button_1" style="display: none;" type="button"
										id="update_phone_send_voice" value="语音验证码" />
									<br />
									<span id="update_phone" style="display: none;">没有收到短信验证码
										?<a style="corsur: pointer; color: red;"
										id="update_phone_send">发送语音验证码</a>
									</span>
								</div>
								<div class="con_4">
									<a id="tijiao5">提交</a>
								</div>
								<div class="con_2">
									如果您在操作过程中出现问题，请点击页面右侧在线客服，或拨打联富金融客服电话：4006-888-923
								</div>
							</li>
									<li>
								<div class="con">
									<h6 style="background-position: 0 2px"></h6>
									<p>
										提现密码
									</p>
									<span><c:if test="${user.m.money_password==null}">未设置</c:if>
										<c:if test="${user.m.money_password!=null}">已设置</c:if> </span>
									<s><c:if test="${user.m.money_password==null}">
											<a onClick="showsubmenu(9)">设置</a>
										</c:if> <c:if test="${user.m.money_password!=null}">
											<a onClick="showsubmenu(8)">修改</a>|<a
												onClick="showsubmenu(20)">找回</a>
										</c:if> </s>
								</div>
							</li>
									<li id="submenu8" class="submenu_1" style="display: none;">
								<div class="con_2">
									为了您的账户安全，请定期更换提现密码，并确保提现密码设置与登录密码不同。
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;原提现密码：
									</label>
									<input class="password_1" id="old_withdraw_password"
										type="password">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;新提现密码：
									</label>
									<input class="password_1" id="new_withdraw_password"
										type="password">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;确认提现密码：
									</label>
									<input class="password_1" id="new_withdraw_password2"
										type="password">
								</div>
								<div class="con_4">
									<a id="tijiao3">提交</a>
								</div>
								<div class="con_2">
									如果您在操作过程中出现问题，请点击页面右侧在线客服，或拨打联富金融客服电话：4006-888-923
								</div>
							</li>
									<li id="submenu20" class="submenu_1 submenu_2"
								style="display: none;">
								<div class="con_2">
									为了您的账户安全，请验证手机！
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										绑定的手机号码：
									</label>
									<span class="haoma">${phone2 }</span>
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										验证码：
									</label>
									<input class="password_1 password_2"
										id="back_withdraw_password_code" type="text">
									<input class="button_1" id="back_withdraw_send_message"
										type="button" value="发送验证码" />
									<input class="button_1" style="display: none;" type="button"
										id="back_withdraw_send_voice" value="语音验证码" />
									<br />
									<span id="back_withdraw_password2" style="display: none;">没有收到短信验证码
										?<a style="corsur: pointer; color: red;"
										id="back_withdraw_password2_voice">发送语音验证码</a>
									</span>
								</div>
								<div class="con_4">
									<a id="tijiao7">提交</a>
								</div>
							</li>
									<li id="submenu22" class="submenu_1" style="display: none;">
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;输入新提现密码：
									</label>
									<input class="password_1" id="new_withdraw_" type="password">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;再次输入新提现密码：
									</label>
									<input class="password_1" id="new_withdraw_2" type="password">
								</div>
								<div class="con_4">
									<a id="tijiao8">提交</a>
								</div>
							</li>
									<li id="submenu21" class="submenu_1" style="display: none;">
								<div class="con_5">
									提现密码设置成功
								</div>
								<div class="con_4">
									<a id="tijiao9">确定</a>
								</div>
							</li>
									<li id="submenu9" class="submenu_1 submenu_2"
								style="display: none;">
								<div class="con_2">
									为了您的账户安全，请定期更换提现密码，并确保提现密码设置与登录密码不同。
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;提现密码：
									</label>
									<input class="password_1" id="password" type="password">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										<span>*</span>&nbsp;确认提现密码：
									</label>
									<input class="password_1" id="ok_password2" type="password">
								</div>
								<div class="con_2 con_3">
									<label class="label_1">
										验证码：
									</label>
									<input class="password_1 password_2" id="page_code" type="text">
									<input class="button_1" id="withdraw_password_send_message"
										type="button" value="发送验证码" />
									<input class="button_1" style="display: none;" type="button"
										id="withdraw_password_send_voice" value="语音验证码" />
									<br />
									<span id="bingd_withdraw_password" style="display: none;">没有收到短信验证码
										?<a style="corsur: pointer; color: red;"
										id="bingd_withdraw_password_voice">发送语音验证码</a>
									</span>
								</div>
								<div class="con_4">
									<a id="tijiao2">提交</a>
								</div>
								<div class="con_2">
									如果您在操作过程中出现问题，请点击页面右侧在线客服，或拨打联富金融客服电话：4006-888-923
								</div>
							</li>
						</ul>
					</div>
				</div>

			</div>
		</div>



		<%@ include file="/jsp/index/index_foot.jsp"%>



	</body>
</html>

<script type="text/javascript">
//绑定邮箱
$("#bd_email").click(function(){
	var email = $("#email").val();
	if(email==null||email==""){
		showMessage(["提示","请填写邮箱"]);
		return;
	}
	if(!( /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/).test(email)){
	showMessage(["提示","邮箱格式错误"]);
	return;
	}
	var code = $("#email_code").val();
	if(code==null||code==""){
		showMessage(["提示","验证码为空"]);
		return;
	}
	$.post('<%=__ROOT_PATH__%>/user/usercenter/bingding_email.html',
		{
			"code":code,
			"email":email	
		},function(data){
				if(data == 1){
			showMessage(["提示","邮箱为空"]);
					}else if(data == 2){
						showMessage(["提示","验证码为空"]);
						}else if(data == 3){
							showMessage(["提示","验证码错误"]);
						}else if(data == 4){
							showMessage(["提示","绑定失败"]);
						}else if(data == 5){
							window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
						}
			}
	);
	
});
//绑定邮箱发送邮箱验证码
var ac = 1;
var bc;
function countdownac(){
	if(ac==90){
		ac= 0;
		clearTimeout(bc);
		$("#send_email").val("发送验证码");
		$("#send_email").attr("disabled",false);
		return;		
	}else{
		$("#send_email").val((90-ac)+"秒后可重发");
		$("#send_email").attr("disabled",true);
		ac++;
		bc = setTimeout("countdownac()",1000);
	}
}
$("#send_email").click(function(){
	var email = $("#email").val();
	if(email==null||email==""){
			showMessage(["提示","请填写邮箱"]);
			return;
		}
	if(!( /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/).test(email)){
		showMessage(["提示","邮箱格式错误"]);
		return;
		}
	$.post(
		'<%=__ROOT_PATH__%>/user/usercenter/bingding_email_send_email.html',
		{
			"email":email
		},
		function(data){
			if(data == 1){
				showMessage(["提示","邮箱为空"]);
				}else if(data == 2){
					showMessage(["提示","该邮箱已经被绑定"]);
				}else if(data == 3){
					showMessage(["提示","邮箱验证码发送失败"]);
				}else{
					countdownac();
				}
		},'html');
});


//
$("#tijiao9").click(function(){
	window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
	$("#back_withdraw_send_message").attr("disabled",false);
	document.getElementById("submenu20").style.display="none";
	document.getElementById("submenu22").style.display="none";
	document.getElementById("submenu21").style.display="none";
});
//找回提现密码
	$("#tijiao8").click(function(){
		var new_withdraw_password = $("#new_withdraw_").val();
		if(new_withdraw_password==null||new_withdraw_password==""){
			showMessage(["提示","新密码不能为空"]);
			return;
		}
		if(new_withdraw_password.length<6||new_withdraw_password.length>35){
			showMessage(["提示","密码长度应该在6~35位"]);
			return;
		}
		var new_withdraw_password2 = $("#new_withdraw_2").val();
		if(new_withdraw_password2==null||new_withdraw_password2==""){
			showMessage(["提示","确认密码不能为空"]);
			return;
		} 
		if(new_withdraw_password!=new_withdraw_password2){
			showMessage(["提示","两次输入的密码不一致"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/usercenter/back_whthdraw_password.html',
				{
					"new_withdraw_password":new_withdraw_password,
					"new_withdraw_password2":new_withdraw_password2
				},function(data){
					if(data ==1){
						showMessage(["提示","提现密码不能为空"]);			
					}else if(data ==2){
						showMessage(["提示","密码长度应该在6~35位"]);
					}else if(data ==3){
						showMessage(["提示","确认密码不能为空"]);
					}else if(data ==4){
						showMessage(["提示","两次输入的密码不一致"]);
					}else if(data ==5){
						showMessage(["提示","请先登录"]);
					}else if(data ==6){
						showMessage(["提示","找回提现密码失败"]);
					}else if(data ==7){
						//showMessage(["提示","找回提现密码成功"]);
						$("#new_withdraw_").attr("value","");
						$("#new_withdraw_2").attr("value","");
						document.getElementById("submenu20").style.display="none";
						document.getElementById("submenu22").style.display="none";
						document.getElementById("submenu21").style.display="block";
					}
				},'html');
	});
//找回提现密码的验证码是否正确
$("#tijiao7").click(function(){
	var back_withdraw_password_code = $("#back_withdraw_password_code").val();
	if(back_withdraw_password_code==null||back_withdraw_password_code==""){
		showMessage(["提示","验证码不能为空"]);
		return;
	}
	$.post('<%=__ROOT_PATH__%>/user/usercenter/back_whthdraw_code_isok',
			{
				"page_code":back_withdraw_password_code
			},function(data){
					if(data == 1){
						showMessage(["提示","验证码为空"]);
					}else if(data == 2){
						showMessage(["提示","验证码错误"]);
					}else if(data == 3){
						$("#back_withdraw_password_code").attr("value","");
						document.getElementById("submenu20").style.display="none";
						document.getElementById("submenu22").style.display="block";
						document.getElementById("submenu21").style.display="none";
					}
			},'html');
});
var e = 1;
var f;
function countdown3(){
	if(e==90){
		e= 0;
		clearTimeout(f);
		$("#back_withdraw_send_message").val("发送验证码");
		$("#back_withdraw_password2").show();
		return;		
	}else{
		$("#back_withdraw_send_message").val((90-e)+"秒后可重发");
		$("#back_withdraw_send_message").attr("disabled",true);
		e++;
		f = setTimeout("countdown3()",1000);
	}
}
$("#back_withdraw_send_message").click(function(){
	$.post('<%=__ROOT_PATH__%>/user/sendmessage/back_withdraw_password_send_message.html',
		{
				
		},function(data){
				if(data == 1){
					showMessage(["提示","请先登录"]);
				}else if(data == 2){
					showMessage(["提示","请使用语音验证码"]);
				}else if(data == 3){
					//showMessage(["提示","短信验证码发送成功"]);
					countdown3();
				}else if(data == 4){
					showMessage(["提示","请先绑定手机号"]);
				}
		},'html');
});

//<input class="button_2" id="back_withdraw_send_message" type="button" value="发送验证码" />
//<input class="button_2" style="display:none;" type="button" id="back_withdraw_send_voice" value="语音验证码" />
//<span id="back_withdraw_password2" style="display:none;">没有收到短信验证码 ?<a style="corsur:pointer" id="back_withdraw_password2_voice">发送语音验证码</a></span>
$("#back_withdraw_password2_voice").click(function(){
		$("#back_withdraw_send_message").hide();
		$("#back_withdraw_send_voice").show();
	});
var e1 = 1;
var f1;
function countdown31(){
	if(e1==90){
		e1= 0;
		clearTimeout(f1);
		$("#back_withdraw_send_voice").val("语音验证码");
		$("#back_withdraw_send_voice").attr("disabled",false);
		return;		
	}else{
		$("#back_withdraw_send_voice").val((90-e1)+"秒后可重发");
		$("#back_withdraw_send_voice").attr("disabled",true);
		e1++;
		f1 = setTimeout("countdown31()",1000);
	}
}
$("#back_withdraw_send_voice").click(function(){
	//alert(11);
	$.post('<%=__ROOT_PATH__%>/user/sendvoice/back_withdraw_password_send_voice.html',
		{
				
		},function(data){
				if(data == 1){
					showMessage(["提示","请先登录"]);
				}else if(data == 2){
					showMessage(["提示","语音验证码发送失败"]);
				}else if(data == 3){
					//showMessage(["提示","短信验证码发送成功"]);
					countdown31();
				}
		},'html');
});
//修改手机号码
 $("#tijiao5").click(function(){
	 var old_tel = $("#old_tel").val();
		if(old_tel==null||old_tel==""){
			showMessage(["提示","请填写原手机号"]);
			return;
		}
		var tel = $("#tel").val();
		if(tel==null||tel==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][3458][0-9]{9}$/.test(tel))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		var update_phone_code = $("#update_phone_code").val();
		if(update_phone_code==null||update_phone_code==""){
			showMessage(["提示","验证码为空"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/usercenter/update_phone.html',
			{
				"old_tel":old_tel,
				"tel":tel,
				"update_phone_code":update_phone_code	
			},function(data){
				if(data == 1){
					showMessage(["提示","请填写原手机号码"]);
				}else if(data == 2){
					showMessage(["提示","原手机号填写错误"]);
				}else if(data == 3){
					showMessage(["提示","请填写新手机号码"]);
				}else if(data == 4){
					showMessage(["提示","新手机号码已经被使用"]);
				}else if(data == 5){
					showMessage(["提示","请填写短信验证码"]);
				}else if(data == 6){
					showMessage(["提示","短信验证码错误"]);
				}else if(data == 7){
					showMessage(["提示","手机号码修改失败"]);
				}else if(data == 8){
					//showMessage(["提示","手机号码修改成功"]);
					window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
				}
			},'html');
			
 });
//倒计时
var c = 1;
var d;
function countdown2(){
	if(c==90){
		c= 0;
		clearTimeout(d);
		$("#update_phone_send_message").val("发送验证码");
		$("#update_phone").show();
		return;		
	}else{
		$("#update_phone_send_message").val((90-c)+"秒后可重发");
		$("#update_phone_send_message").attr("disabled",true);
		c++;
		d = setTimeout("countdown2()",1000);
	}	
}
	//修改手机发送验证码
	$("#update_phone_send_message").click(function(){
		var old_tel = $("#old_tel").val();
		if(old_tel==null||old_tel==""){
			showMessage(["提示","请填写原手机号"]);
			return;
		}
		var tel = $("#tel").val();
		if(tel==null||tel==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][3458][0-9]{9}$/.test(tel))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendmessage/update_phone_send_message.html',
				{
					"old_tel":old_tel,
					"tel":tel
				},function(data){
					if(data == 5){
						showMessage(["提示","请填写旧手机号码"]);
					}else if(data == 6){
						showMessage(["提示","旧手机号码错误"]);
					}else if(data == 1){
						showMessage(["提示","请填写新手机号"]);
					}else if(data == 2){
						showMessage(["提示","新手机号已经被使用"]);
					}else if(data == 3){
						showMessage(["提示","请使用语音验证码"]);
						$("#update_phone").show();
					}else if(data == 4){
						//showMessage(["提示","验证码发送成功"]);
						countdown2();
					}
				},'html');	
	});
																																																																																																																																																																																																																																																																																																																																
	$("#update_phone_send").click(function(){
		$("#update_phone_send_message").hide();
		$("#update_phone_send_voice").show();
	});
	var c1 = 1;
	var d1;
	function countdown21(){
		if(c1==90){
			c1= 0;
			clearTimeout(d1);
			$("#update_phone_send_voice").val("语音验证码");
			$("#update_phone_send_voice").attr("disabled",false);
			return;		
		}else{
			$("#update_phone_send_voice").val((90-c1)+"秒后可重发");
			$("#update_phone_send_voice").attr("disabled",true);
			c1++;
			d = setTimeout("countdown21()",1000);
		}
	}
	$("#update_phone_send_voice").click(function(){
		//alert(11);
		var old_tel = $("#old_tel").val();
		if(old_tel==null||old_tel==""){
			showMessage(["提示","请填写原手机号"]);
			return;
		}
		var tel = $("#tel").val();
		if(tel==null||tel==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][3458][0-9]{9}$/.test(tel))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendvoice/update_phone_send_voice.html',
				{
					"old_tel":old_tel,
					"tel":tel
				},function(data){
					if(data == 5){
						showMessage(["提示","请填写旧手机号码"]);
					}else if(data == 6){
						showMessage(["提示","旧手机号码错误"]);
					}else if(data == 1){
						showMessage(["提示","请填写新手机号"]);
					}else if(data == 2){
						showMessage(["提示","新手机号已经被使用"]);
					}else if(data == 3){
						showMessage(["提示","短信验证码发送失败"]);
					}else if(data == 4){
						//showMessage(["提示","验证码发送成功"]);
						countdown21();
					}
				},'html');	
	});
	//绑定手机
	$("#tijiao4").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][3458][0-9]{9}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		var binding_phone_code = $("#binding_phone_code").val();
		if(binding_phone_code==null||binding_phone_code==""){
			showMessage(["提示","请填写短信验证码"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/usercenter/binding_phone.html',
			{
				"phone":phone,
				"page_code":binding_phone_code
			},
			function(data){
				if(data == 1){
					showMessage(["提示","请填写手机号码"]);
				}else if(data == 2){
					showMessage(["提示","该手机号已经被绑定"]);
				}else if(data == 3){
					showMessage(["提示","请填写短信验证码"]);
				}else if(data == 4){
					showMessage(["提示","验证码错误"]);
				}else if(data == 5){
					showMessage(["提示","手机号码绑定失败"]);
				}else{
					//showMessage(["提示","绑定成功"]);
					window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
				}
			}
		);
	});
//绑定手机发送验证码
	//倒计时
					var a = 1;
					var b;
					function countdown1(){
						if(a==90){
							a= 0;
							clearTimeout(b);
							$("#binding_phone_send_message").val("发送验证码");
							$("#bingd_phone").show();
							return;		
						}else{
							$("#binding_phone_send_message").val((90-a)+"秒后可重发");
							$("#binding_phone_send_message").attr("disabled",true);
							a++;
							b = setTimeout("countdown1()",1000);
						}
					}
					
			
	$("#binding_phone_send_message").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][3458][0-9]{9}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendmessage/binding_phone_send_message.html',
			{
				"phone":phone
			},
			function(data){
				if(data == 4){
					showMessage(["提示","手机号不能为空"]);
				}else if(data == 1){
					showMessage(["提示","该手机号已经被绑定"]);
				}else if(data == 2){
					showMessage(["提示","请使用语音验证码"]);
					$("#bingd_phone").show();
				}else{
					//showMessage(["提示","短信验证码发送成功"]);
					countdown1();
				}
			},'html');
	});
	$("#bingd_phone_send").click(function(){
		$("#binding_phone_send_message").hide();
		$("#binding_phone_send_voice").show();
	});
	var a1 = 1;
	var b1;
	function countdown11(){
		if(a1==90){
			a1= 0;
			clearTimeout(b1);
			$("#binding_phone_send_voice").val("语音验证码");
			$("#bingd_phone").show();
			$("#binding_phone_send_voice").attr("disabled",false);
			return;		
		}else{
			$("#binding_phone_send_voice").val((90-a1)+"秒后可重发");
			$("#binding_phone_send_voice").attr("disabled",true);
			a1++;
			b1 = setTimeout("countdown11()",1000);
		}
	}
	$("#binding_phone_send_voice").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^1[3|5][0-9]\d{4,8}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendvoice/binding_phone_send_voice.html',
				{
					"phone":phone
				},
				function(data){
					if(data == 4){
						showMessage(["提示","手机号不能为空"]);
					}else if(data == 1){
						showMessage(["提示","该手机号已经被绑定"]);
					}else if(data == 2){
						showMessage(["提示","验证码发送失败"]);
					}else{
						//showMessage(["提示","短信验证码发送成功"]);
						countdown11();
					}
				},'html','application/x-www-form-urlencoded; charset=utf-8');
	});

	$("#tijiao3").click(function(){
		var old_withdraw_password = $("#old_withdraw_password").val();
		if(old_withdraw_password==null||old_withdraw_password==""){
			showMessage(["提示","原密码不能为空"]);
			return;
		}
		var new_withdraw_password = $("#new_withdraw_password").val();
		if(new_withdraw_password==null||new_withdraw_password==""){
			showMessage(["提示","新密码不能为空"]);
			return;
		}
		if(new_withdraw_password.length<6||new_withdraw_password.length>35){
			showMessage(["提示","密码长度为6~35位"]);
			return;
		}
		var new_withdraw_password2 = $("#new_withdraw_password2").val();
		if(new_withdraw_password2!=new_withdraw_password){
			showMessage(["提示","两次输入的密码不一致"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/usercenter/update_withdraw_password.html',
			{
				"old_password":old_withdraw_password,
				"new_password":new_withdraw_password,
				"new_password2":new_withdraw_password2
			},
			function(data){
				if(data == 1){
					showMessage(["提示","原密码不能为空"]);
				}else if(data == 2){
					showMessage(["提示","原密码错误"]);
				}else if(data == 3){
					showMessage(["提示","新密码不能为空"]);
				}else if(data == 4){
					showMessage(["提示","密码长度应为6~35位"]);
				}else if(data ==5){
					showMessage(["提示","第二次输入的密码为空"])
				}else if(data == 6){
					showMessage(["提示","两次输入的密码不一致"]);
				}else if(data == 7){
					showMessage(["提示","提现密码修改失败"]);
				}else{
					window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
					showMessage(["提示","提现密码修改成功"]);
				}
			},'html','application/x-www-form-urlencoded; charset=utf-8');
	});
	function onsumbit(){
		//alert(11);
		var old_password = $("#old_password").val();
		if(old_password==null||old_password==""){
			showMessage(["提示","原密码不能为空"]);
			return;
		}
		var new_password = $("#new_password").val();
		if(new_password==null||new_password==""){
			showMessage(["提示","新密码不能为空"]);
			return;
		}
		if(new_password.length<6||new_password.length>35){
			showMessage(["提示","密码长度为6~35位"]);
			return;
		}
		var ok_password = $("#ok_password").val();
		if(ok_password!=new_password){
			showMessage(["提示","两次输入的密码不一致"]);
			return;
		}
		$.post(
		'<%=__ROOT_PATH__%>/user/usercenter/update_login_password.html',
		 { "old_password" : old_password,
		  "new_password" : new_password,
		  "new_password2" : ok_password},
		  function(data){
		   if(data == 1){
		   	showMessage(["提示","原密码不能为空"]);
		   }else if(data == 2){
		   	showMessage(["提示","原密码填写错误"]);
		   }else if(data == 3){
		   	showMessage(["提示","新密码不能为空"]);
		   }else if(data == 4){
		   	showMessage(["提示","密码长度应在6~35位"]);
		   }else if(data == 5){
		   	showMessage(["提示","第二次输入的密码不能为空"]);
		   }else if(data == 6){
		   	showMessage(["提示","两次输入的新密码不一致"]);
		   }else if(data == 7){
		   	showMessage(["提示","密码修改失败"]);
		   }else if(data == 8){
		   	//showMessage(["提示","密码修改成功"]);
		   	window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
		   }	
		},'html','application/x-www-form-urlencoded; charset=utf-8');
	}
		var i = true;
  		function authenticate() {
		if(i){
			i = false;
			var identity = $("#identity").val();
			var real_name = $("#real_name").val();
			$.ajax({
						url:'<%=__ROOT_PATH__%>/user/usercenter/real_name.html',
						type:'post',
						cache:false,
						async:false,
						data:{"identity":identity,"real_name":real_name},
						success:function(data){
							if(data==null){
									showMssage(["警告","<span class=\"hongse\">交易失败,请检查网络连接是否通畅,或服务器处于正常状态！</span>"]);
							}else{
									if(data == 0){//请输入姓名
										showMessage(["提示","请输入姓名"]);
									}
									if(data == 1){//包含非法字符
										showMessage(["提示","包含非法字符"]);
									}
									if(data == 2){//请输入您的二代身份证号码
										showMessage(["提示","请输入您的二代身份证号码"]);
									} 
									if(data == 3){//您的身份证号码有误
										showMessage(["提示","您的身份证号码有误"]);
									}
									if(data == 4){//认证成功
										//window.location.href="<%=__ROOT_PATH__%>/index/index.html";
										//showMessage(["提示","认证成功"]);
										window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
									}
									if(data == 5){//不可重复认证
										showMessage(["提示","不可重复认证"]);
									}
									if(data == 6){
										showMessage(["提示","实名认证失败"]);
									}
								}
						}
					},'html','application/x-www-form-urlencoded; charset=utf-8');
			}
			i=true;
		}
		
		
		
				//倒计时
					var mx = 1;
					var tx;
					function countdown(){
						if(mx==90){
							mx= 0;
							clearTimeout(tx);
							$("#withdraw_password_send_message").val("发送验证码");
							$("#bingd_withdraw_password").show();
							return;		
						}else{
							$("#withdraw_password_send_message").val((90-mx)+"秒后可重发");
							$("#withdraw_password_send_message").attr("disabled",true);
							mx++;
							tx = setTimeout("countdown()",1000);
						}
					}
					//发送验证码
				$("#withdraw_password_send_message").click(function(){
					var password = $("#password").val();
					if(password==null||password==""){
						showMessage(["提示","新密码不能为空"]);
						return;
					}
					if(password.length<6||password.length>35){
						showMessage(["提示","密码长度为6~35位"]);
						return;
					}
					var ok_password = $("#ok_password2").val();
					if(ok_password!=password){
						showMessage(["提示","两次输入的密码不一致"]);
						return;
					}
					$.post('<%=__ROOT_PATH__%>/user/sendmessage/binding_withdrow_password_send_message.html',
						{
							"withdraw_password":password,
							"withdraw_password2":ok_password
						},
						function(data){
							//alert(data);
							if(data == 1){
								showMessage(["提示","提现密码不能为空"]);
							}else if(data == 2){
								showMessage(["提示","密码长度应该在6~35位"]);
							}else if(data == 3){
								showMessage(["提示","第二次输入密码不能为空"]);
							}else if(data == 4){
								showMessage(["提示","两次密码不一致"]);
							}else if(data == 5){
								showMessage(["提示","您还没有绑定手机号码"]);
							}else if(data == 6){
								showMessage(["提示","请使用语音验证码"]);
								$("#bingd_withdraw_password").show();
								
							}else{
								//showMessage(["提示","设置成功"]);
								countdown();
							}
						},'html','application/x-www-form-urlencoded; charset=utf-8');
				});
				//<input class="button_2" id="withdraw_password_send_message" type="button" value="发送验证码" />
				//<input class="button_2" style="display:none;" type="button" id="withdraw_password_send_voice" value="语音验证码" />
				//<span id="bingd_withdraw_password" style="display:none;">没有收到短信验证码 ?<a style="corsur:pointer" id="bingd_withdraw_password_voice">发送语音验证码</a></span>
				$("#bingd_withdraw_password").click(function(){
				$("#withdraw_password_send_message").hide();
				$("#withdraw_password_send_voice").show();
				});
				var mx1 = 1;
				var tx1;
				function countdownn(){
					if(mx1==90){
						mx1= 0;
						clearTimeout(tx1);
						$("#withdraw_password_send_voice").val("语音验证码");
						$("#withdraw_password_send_voice").attr("disabled",false);
						return;		
					}else{
						$("#withdraw_password_send_voice").val((90-mx1)+"秒后可重发");
						$("#withdraw_password_send_voice").attr("disabled",true);
						mx1++;
						tx1 = setTimeout("countdownn()",1000);
					}
				}
				$("#withdraw_password_send_voice").click(function(){
					var password = $("#password").val();
					if(password==null||password==""){
						showMessage(["提示","新密码不能为空"]);
						return;
					}
					if(password.length<6||password.length>35){
						showMessage(["提示","密码长度为6~35位"]);
						return;
					}
					var ok_password = $("#ok_password2").val();
					if(ok_password!=password){
						showMessage(["提示","两次输入的密码不一致"]);
						return;
					}
					$.post('<%=__ROOT_PATH__%>/user/sendvoice/binding_withdrow_password_send_voice.html',
						{
							"withdraw_password":password,
							"withdraw_password2":ok_password
						},
						function(data){
							//alert(data);
							if(data == 1){
								showMessage(["提示","提现密码不能为空"]);
							}else if(data == 2){
								showMessage(["提示","密码长度应该在6~35位"]);
							}else if(data == 3){
								showMessage(["提示","第二次输入密码不能为空"]);
							}else if(data == 4){
								showMessage(["提示","两次密码不一致"]);
							}else if(data == 5){
								showMessage(["提示","您还没有绑定手机号码"]);
							}else if(data == 6){
								showMessage(["提示","语音验证码发送失败"]);
							}else{
								//showMessage(["提示","设置成功"]);
								countdownn();
							}
						},'html','application/x-www-form-urlencoded; charset=utf-8');
				});
		
		//设置提现密码
		$("#tijiao2").click(function(){
			var password = $("#password").val();
			if(password==null||password==""){
				showMessage(["提示","新密码不能为空"]);
				return;
			}
			if(password.length<6||password.length>35){
				showMessage(["提示","密码长度为6~35位"]);
				return;
			}
			var ok_password = $("#ok_password2").val();
			if(ok_password!=password){
				showMessage(["提示","两次输入的密码不一致"]);
				return;
			} 
			var page_code = $("#page_code").val();
			if(page_code==null||page_code==""){
				showMessage(["提示","请填写您收到的验证码"]);
				return;
			}
			$.post('<%=__ROOT_PATH__%>/user/usercenter/binding_withdraw_password.html',
				{
					"withdraw_password":password,
					"withdraw_password2":ok_password,
					"page_code":page_code
				},
				function(data){
					if(data == 1){
						showMessage(["提示","密码不能为空"]);
					}else if(data ==2){
						showMessage(["提示","密码长度为6~36位"]);
					}else if(data ==3){
						showMessage(["提示","请确认您的密码"]);
					}else if(data ==4){
						showMessage(["提示","两次输入的密码不一致"]);
					}else if(data ==5){
						showMessage(["提示","请输入您的验证码"]);
					}else if(data ==6){
						showMessage(["提示","验证码错误"]);
					}else if(data ==7){
						showMessage(["提示","提现密码设置失败"]);
					}else if(data ==8){
						//showMessage(["提示","提现密码设置成功"]);
						window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
					}
				},'html','application/x-www-form-urlencoded; charset=utf-8');
		});
</script>
