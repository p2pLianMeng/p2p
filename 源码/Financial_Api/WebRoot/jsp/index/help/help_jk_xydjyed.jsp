﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html" style="background:#f8bbb2;color:#fff;"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">产品介绍</a>
             <a href="#">借款费用</a>
             <a href="#">如何申请</a>
             <a href="#">认证资料</a>
             <a href="#">信用审核</a>
             <a href="#">信用等级与额度</a>
             <a href="#">筹款与提现</a>
             <a href="#">如何还款</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div class="down_up"><span>&gt;</span>信用等级与额度</div>
             
             <div onClick="showsubmenu(57)" class="district"><s>&bull;</s><span>什么是信用分数和信用等级？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu57" class="area" style="display:none;">
                用户的信用分数是在通过联富金融审核后获得的，信用等级由信用分数转化而来，每个信用等级都对应的信用分数范围，信用分数和信用等级是借款人的信用属性，也是理财人判断借款人违约风险的重要依据之一。通常来讲借款人信用等级越高，其违约率越低，相应的借款成功率越高。<br />
                目前信用等级由高到低分为AA、A、B、C、D、E、HR，具体请参考信用等级的分数区间：<br />
                <table class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="16%">信用等级</td>
                      <td width="12%"><span>HR</span></td>
                      <td width="12%"><span>E</span></td>
                      <td width="12%"><span>D</span></td>
                      <td width="12%"><span>C</span></td>
                      <td width="12%"><span>B</span></td>
                      <td width="12%"><span>A</span></td>
                      <td width="12%"><span>AA</span></td>
                   </tr>
                   <tr>
                      <td width="16%">分数区间</td>
                      <td width="12%">0-99</td>
                      <td width="12%">100-109</td>
                      <td width="12%">110-119</td>
                      <td width="12%">120-129</td>
                      <td width="12%">130-144</td>
                      <td width="12%">145-159</td>
                      <td width="12%">160+</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(58)" class="district"><s>&bull;</s><span>什么是信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu58" class="area" style="display:none;">
                用户的信用额度是在通过联富金融审核员对所提供材料的审核后获得的，既是借款人单笔借款的上限也是借款者累积尚未还清借款的上限。<br />
                例如，如果一个借款人信用额度为5万元，则在没有其他借款的情况下，用户可以发布总额最高为5万元的借款请求。也可以分多次发布借款请求，但尚未还清借款（以整笔借款金额计算）的总额不得超过5万元。<br />
             </div>
             
             <div onClick="showsubmenu(59)" class="district"><s>&bull;</s><span>如何获得信用分数、信用等级与信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu59" class="area" style="display:none;">
                请您申请借款并上传认证资料，审核完成后，您将获得相应的信用分数、信用等级和信用额度。
             </div>
             
             <div onClick="showsubmenu(60)" class="district"><s>&bull;</s><span>如何查看我的信用分数、信用等级与信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu60" class="area" style="display:none;">
                登陆联富金融网站，打开【我的联富金融】--【账户管理】--【认证信息】页面，即可查看您的信用分数、信用等级、信用额度，及您进行的各项信用认证的情况。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(61)" class="district"><s>&bull;</s><span>如何提高我的信用等级（信用分数）？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu61" class="area" style="display:none;">
                您可以通过提交【可选认证资料】来提高信用分数，另外您在联富金融网站上良好的还款记录也会增加您的信用分数。
             </div>
             
             <div onClick="showsubmenu(62)" class="district"><s>&bull;</s><span>如何提高我的信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu62" class="area" style="display:none;">
                <h1>提额申请资格说明</h1>
                您之前已获得信用额度，且至少已成功借款一笔，目前您名下的借款已全部结清，无在还借款。<br />
                <h1>提额申请办法</h1>
                如果您目前的状况满足上述情况，请您重新发布借款申请，申请金额请按照实际需求填写，不限定在已获得的信用额度范围之内。
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
