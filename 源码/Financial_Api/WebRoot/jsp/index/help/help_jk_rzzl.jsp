﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html" style="background:#f8bbb2;color:#fff;"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">产品介绍</a>
             <a href="#">借款费用</a>
             <a href="#">如何申请</a>
             <a href="#">认证资料</a>
             <a href="#">信用审核</a>
             <a href="#">信用等级与额度</a>
             <a href="#">筹款与提现</a>
             <a href="#">如何还款</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div class="down_up"><span>&gt;</span>认证资料</div>
             
             <div onClick="showsubmenu(21)" class="district"><s>&bull;</s><span>必要认证资料和可选认证资料有哪些？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                认证项如下：（点击某项认证，查看详细说明）<br />
                必要认证资料<br />
                <h1>工薪贷</h1>
                <a href="#">身份认证</a>、<a href="#">个人信用报告</a>、<a href="#">劳动合同或在职证明</a>、<a href="#">近3个月工资卡银行流水</a><br />
                <h1>生意贷</h1>
                <a href="#">身份认证</a>、<a href="#">个人信用报告</a>、<a href="#">经营证明</a>、<a href="#">近6个月常用银行卡流水</a><br />
                <h1>网商贷</h1>
                <a href="#">身份认证</a>、提供网店地址及阿里旺旺账号、<a href="#">需进行QQ视频审核</a><br />
                <h1>可选认证资料</h1>
                <a href="#">房产认证</a>、<a href="#">购车认证</a>、<a href="#">结婚认证</a>、<a href="#">学历认证</a>、<a href="#">技术职称认证</a>、<a href="#">手机实名认证</a>、<a href="#">微博认证</a>、<a href="#">居住地证明</a><br />
                （点击各项认证，链接至各项认证说明处）
             </div>
             
             <div onClick="showsubmenu(22)" class="district"><s>&bull;</s><span>身份认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                您上传的身份证照片须和您绑定的身份证一致，否则将无法通过认证。<br />
                <h1>请同时提供下面两项资料：</h1>
                1.本人身份证原件的正、反两面照片。<br />
                2.本人手持身份证正面头部照，（确保身份证上的信息没有被遮挡，避免证件与头部重叠，确保身份证内容完整清晰可见）。<br />
                <h1>认证示例：</h1>
                <a href="#">身份证正反面照片</a><br />
                <a href="#">本人手持身份证正面头部照片</a><br />
                <h1>认证有效期：</h1>
                永久
             </div>
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>信用报告</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                个人信用报告是由中国人民银行出具，全面记录个人信用活动，反映个人信用基本状况的文件。本报告是联富金融了解您信用状况的一个重要参考资料。<br />
                <h1>认证说明：</h1>
                个人信用报告需15日内开具。<br />
                <h1>认证条件：</h1>
                信用记录良好。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">个人信用报告</a><br />
                <h1>如何办理：</h1>
                可去当地人民银行打印，部分地区可登录个人信用信息服务平台。<br />
                <a href="#">全国各地征信中心联系方式查询</a><br />
                <a href="#">个人信用信息服务平台</a><br />
                <a href="#">关于个人查询本人信用报告实施收费的公告</a><br />
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>（工薪贷）工作认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none;">
                您的工作状况是联富金融评估您信用状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.加盖单位公章（或劳动合同专用章）的劳动合同。<br />
                2.最近1个月内开具的加盖单位公章（或人力章、财务章）的机打（手写无效）在职证明。<br />
                3.带有姓名、照片、工作单位名称的工作证。<br />
                <h1>认证条件：</h1>
                本人需在现单位工作满3个月。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">劳动合同</a><br />
                <a href="#">在职证明</a><br />
                <a href="#">工作证</a><br />
             </div>
             
             <div onClick="showsubmenu(25)" class="district"><s>&bull;</s><span>（生意贷）工作认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                您的工作状况是联富金融评估您信用状况的主要依据之一。<br />
                2. 经营场地租赁合同＋90天内的租金发票或水电单据。<br />
                <h1>认证条件：</h1>
                本人名下的企业经营时间需满1年。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">营业执照正、副本</a><br />
                <a href="#">租赁合同和水电发票 </a><br />
             </div>
             
             <div onClick="showsubmenu(26)" class="district"><s>&bull;</s><span>（网商贷）工作认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                您的工作状况是联富金融评估您信用状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                网商用户无需上传任何工作认证相关资料，但需进行视频审核，联富金融客服会尽快主动联络您进行视频审核。在审核过程中，需远程展示以下资料：<br />
                1.淘宝/天猫近3个月交易明细或支付宝近12个月账单。<br />
                2.淘宝/天猫的支付宝实名认证页面。<br />
                3.淘宝/天猫信用以及订单借款账户页面。<br />
                4.淘宝/天猫处罚积分页面。<br />
                <h1>认证条件：</h1>
                本人名下的店铺需在淘宝或天猫经营满半年，且近期无淘宝严重处罚，店铺等级达到2钻, 好评率达到95%，动态评分达到4.5，无法估价产品和虚拟产品占比不超过40%。近3个月交易总额满3万，且交易笔数超过50笔。<br />
                <h1>认证有效期：</h1>
                6个月
             </div>
                          
             <div onClick="showsubmenu(27)" class="district"><s>&bull;</s><span>（工薪贷）收入认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu27" class="area" style="display:none;">
                您的银行流水单是联富金融评估您收入状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.可体现工资项的最近3个月的工资卡银行流水单。<br />
                2.可体现工资项的最近3个月的网银电脑截屏。<br />
                <h1>认证条件：</h1>
                本人名下近3个月的月收入均在2000以上。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">银行卡流水</a><br />
                <a href="#">网银电脑截图</a>
             </div>
             
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>（生意贷）收入认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                您的银行流水单是联富金融评估您收入状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.可体现经营情况的最近6个月的个人银行卡流水单，或网银电脑截屏。<br />
                2.可体现经营情况的最近6个月的企业银行卡流水单，或网银电脑截屏。<br />
                <h1>认证条件：</h1>
                本人提交的流水可反映真实有效的经营情况。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">银行卡流水</a><br />
                <a href="#">网银电脑截图</a>
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>房产认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                房产认证是证明借入者资产及还款能力的重要凭证之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.房屋产权证明。<br />
                2.购房合同 + 近3个月的还贷流水。<br />
                3.购房发票 + 近3个月的还贷流水。<br />
                4.按揭合同 + 近3个月的还贷流水。<br />
                <h1>认证条件：</h1>
                必须是商品房，且房产是本人名下所有或共有的。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">房产证</a><br />
                <a href="#">购房发票</a><br />
                <a href="#">购房合同</a>
             </div>
             
             <div onClick="showsubmenu(30)" class="district"><s>&bull;</s><span>购车认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu30" class="area" style="display:none;">
                购车认证是证明借入者资产及还款能力的重要凭证之一。<br />
                <h1>认证说明：</h1>
                请同时提供下面两项资料：<br />
                1.车辆行驶证的原件照片。<br />
                2.本人和车辆的合影（照片需露出车牌号码）。<br />
                <h1>认证条件：</h1>
                车辆必须是本人名下所有。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">车辆行驶证</a><br />
                <a href="#">与车辆合影 </a>
             </div>
             
             <div onClick="showsubmenu(31)" class="district"><s>&bull;</s><span>结婚认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu31" class="area" style="display:none;">
                借入者婚姻状况的稳定性，是联富金融考核借款人信用的评估因素之一。<br />
                <h1>认证说明：</h1>
                请同时提供下面三项资料：<br />
                1.结婚证书的原件照片。<br />
                2.配偶身份证原件的正、反两面照片。<br />
                3.本人和配偶的近照合影一张。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">结婚证</a>
             </div>
             
             <div onClick="showsubmenu(32)" class="district"><s>&bull;</s><span>学历认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu32" class="area" style="display:none;">
                借出者在选择借款申请投标时，借入者的学历也是一个重要的参考因素。为了让借出者更好、更快地相信您的学历是真实的，建议您对学历进行在线验证。<br />
                <h1>认证说明：</h1>
                2001年至今获得学历，需学历证书编号：<br />
                1.访问<a href="#">学信网</a>，找到“零散查询”信息，点击【查询】；<br />
                2.输入证书编号、姓名、查询码（通过手机短信获得，为12位学历查询码）、验证码，进行查询；<br />
                3.查询成功后，您将查获得《教育部学历证书电子注册备案表》；<br />
                4.将该表右下角的12位在线验证码（见学<a href="#">历认证1</a>）输入文本框，点击【提交审核】。<br />
                1991年至今获得学历，无需学历证书编号：<br />
                1.访问<a href="#">学信网</a>，找到“本人查询”信息，点击【查询】；<br />
                2.注册学信网账号，登录<a href="#">学信网</a>，访问【学历信息】页面；<br />
                3.选择您的最高学历，点击【申请验证报告】（通过手机短信获得12位学历查询码，此查询码与联富金融所需验证码不同）；<br />
                4.申请成功后，您将获得12位在线验证码<a href="#">（见学历认证2）</a>；<br />
                5.将12位在线验证码输入文本框，点击【提交审核】。<br />
                <h1>认证条件：</h1>
                大专或以上学历（普通全日制）。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">学历认证1</a><br />
                <a href="#">学历认证2</a>
             </div>
             
             <div onClick="showsubmenu(33)" class="district"><s>&bull;</s><span>技术职称认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu33" class="area" style="display:none;">
                技术职称是经专家评审、反映一个人专业技术水平并作为聘任专业技术职务依据的一种资格。不与工资挂钩，是联富金融考核借款人信用的评估因素之一。<br />
                <h1>认证说明：</h1>
                技术职称证书的原件照片。<br />
                <h1>认证条件：</h1>
                国家承认的二级及以上等级证书。例如律师证、会计证、工程师证等。<br />
                <h1>认证有效期：</h1>
                永久<br />
             </div>
             
             <div onClick="showsubmenu(34)" class="district"><s>&bull;</s><span>手机实名认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu34" class="area" style="display:none;">
                手机实名认证需要用户上传手机流水单。手机流水单是用户最近一段时间内的详细通话记录，是联富金融用以验证借入者真实性的重要凭证之一。<br />
                <h1>认证说明：</h1>
                绑定的手机号码最近3个月的手机详单原件的照片。如详单数量较多可分月打印并上传每月前5日部分（每月详单均需清晰显示机主手机号码）。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>如何办理：</h1>
                前往最近的手机运营商营业厅，提供手机服务密码或机主身份证明即可打印。
             </div>
             
             <div onClick="showsubmenu(35)" class="district"><s>&bull;</s><span>微博认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu35" class="area" style="display:none;">
                随着网络时代的发展，网络信息已成为了解借入者真实性的最新渠道之一。联富金融与时俱进，将借入者网络关系评估，作为评估借入者的信用的一种方式。<br />
                <h1>认证说明：</h1>
                1.成为联富金融粉丝：<br />
                请点击链接<a href="#">http://e.weibo.com/renrendai</a>，把联富金融加为关注<br />
                请点击链接<a href="#">http://t.qq.com/renrendai</a>，把联富金融加为关注；<br />
                2. 发送验证信息<br />
                在微博上，转发一条最新的联富金融的微博，并用“私信”功能，发送您的联富金融昵称给联富金融；<br />
                3. 在输入框输入您的新浪微博昵称输入文本框，点击【提交审核】。<br />
                <h1>认证条件：</h1>
                50粉丝 + 50微博（目前仅支持新浪微博）<br />
                <h1>认证有效期：</h1>
                永久
             </div>
             
             <div onClick="showsubmenu(36)" class="district"><s>&bull;</s><span>居住地证明</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu36" class="area" style="display:none;">
                居住地的稳定性，是联富金融考核借款人的主要评估因素之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.用本人姓名登记的水、电、气最近3个月缴费单。<br />
                2.用本人姓名登记的固定电话最近3个月缴费单。<br />
                3.本人的信用卡最近2个月的月结单。<br />
                4.本人的自有房产证明。<br />
                5.本人父母的房产证明，及证明本人和父母关系的证明材料。<br />
                <h1>认证有效期：</h1>
                6个月
             </div>
             
             <div onClick="showsubmenu(37)" class="district"><s>&bull;</s><span>（工薪贷）工作单位的注册地必须在中国大陆吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu37" class="area" style="display:none;">
                联富金融网站支持的用户是中国大陆地区公民，且工作单位或经营场所注册地必须在中国大陆，港澳台地区目前不支持。
             </div>
             
             <div onClick="showsubmenu(38)" class="district"><s>&bull;</s><span>（工薪贷）工资发现金，没有发到银行卡上怎么办？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu38" class="area" style="display:none;">
                如果您无法提供工资卡流水，也可提供能体现您收支情况的常用银行卡流水。
             </div>
             
             <div onClick="showsubmenu(39)" class="district"><s>&bull;</s><span>（生意贷）没有经营地证明怎么办？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu39" class="area" style="display:none;">
                经营地证明中的经营场地租赁合同是您必须要提供的，90天内的租金发票或水电单据您可选择其中一项提供，如果您的水电单据存在特殊情况时，客户经理将致电向您了解具体情况并沟通解决办法。
             </div>
             
             <div onClick="showsubmenu(40)" class="district"><s>&bull;</s><span>（生意贷）借款人不是法人本人，可以申请吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu40" class="area" style="display:none;">
                生意贷产品要求申请人限于法人代表。如遇申请人身份特殊时，客户经理将致电向您了解具体情况并沟通解决办法。
             </div> 
             
             <div onClick="showsubmenu(41)" class="district"><s>&bull;</s><span>（生意贷）借款人是法人本人，但银行流水在别人名下，可以申请吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu41" class="area" style="display:none;">
                生意贷产品要求提供法人名下或企业对公银行流水。如遇申请人情况特殊时，客户经理将致电向您了解具体情况并沟通解决办法。
             </div> 
             
             <div onClick="showsubmenu(42)" class="district"><s>&bull;</s><span>（网商贷）借款人不是淘宝或天猫商户，可以申请吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu42" class="area" style="display:none;">
                目前联富金融网站暂不支持非淘宝或天猫的网商申请借款，要求至少在淘宝或天猫经营网店满半年。
             </div>
             
             <div onClick="showsubmenu(43)" class="district"><s>&bull;</s><span>“完整的银行流水，中间不能出现断层”是什么意思？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu43" class="area" style="display:none;">
                要求银行流水有连续月份，内容包括交易日期、交易金额、余额、交易地点、交易摘要等。（点击<a href="#">银行卡流水</a>，查看示例图片） 
             </div> 
             
             <div onClick="showsubmenu(44)" class="district"><s>&bull;</s><span>如何上传、补充、修改或撤销资料？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu44" class="area" style="display:none;">
                1.登陆联富金融网站，打开【我要借款】--【工薪贷】查看详情页面（以工薪贷为例），点击【申请借款】，【填写借款申请】后，您可以在【填写借款信息】的过程中上传【必要认证资料】；<br />
                2.您也可以在【上传资料】页面中上传所有【必要上传资料】和【可选上传资料】。请您确保所有资料上传完整后点击【提交申请】，【提交申请】后您将无法补充上传资料。<br />
                <a href="#">查看操作流程>></a><br />
                确认【提交申请】后，我们的客户经理将检查您的资料，如果您的资料上传有误或未上传齐全，我们将驳回您的资料，您可继续操作补充资料（建议您一次性将资料补充完整）。<br />
                资料上传后，无法操作修改和撤销，您可按照上述说明继续补充上传。
             </div>    
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


	<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
