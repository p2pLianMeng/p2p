﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html" style="background:#f8bbb2;color:#fff;"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">产品介绍</a>
             <a href="#">借款费用</a>
             <a href="#">如何申请</a>
             <a href="#">认证资料</a>
             <a href="#">信用审核</a>
             <a href="#">信用等级与额度</a>
             <a href="#">筹款与提现</a>
             <a href="#">如何还款</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div class="down_up"><span>&gt;</span>如何还款</div>
             
             <div onClick="showsubmenu(65)" class="district"><s>&bull;</s><span>如何进行还款？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu65" class="area" style="display:none;">                
                1.当账户余额不足支付当期还款时，您可先通过网银为账户充值；<br />
                2.充值完成后，您可以自行点击还款；<br />
                3.充值完成后，您也可以等待系统自动扣款，无需自行点击还款。<a href="#">查看操作流程>></a> 
             </div>
             
             <div onClick="showsubmenu(66)" class="district"><s>&bull;</s><span>我会收到哪些形式的还款提醒？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu66" class="area" style="display:none;">                
                您会在每月还款日的前三天以电子邮件、站内信的方式收到还款提醒，并在还款日的前一天接到客服人员的电话提醒。
             </div>
             
             <div onClick="showsubmenu(67)" class="district"><s>&bull;</s><span>是否可以提前还款？如何操作？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu67" class="area" style="display:none;">                
                您可以随时进行提前还款。<br />
                登陆联富金融网站，打开【我的联富金融】--【借款管理】--【我的借款】页面，点击【提前还款】进行提前还款操作（与还款操作类似）。<br />
                提前还款的用户需要支付给理财人借款剩余本金的1%作为违约金，不用再支付后续的利息及管理费用。提前还款金额公式如下：<br /> 
                <img src="<%=__PUBLIC__%>/images/help_4.png" /><br />
                注：提前归还部分借款不视为提前还款，仍需支付全部借款利息及账户管理费。
             </div>
             
             <div onClick="showsubmenu(68)" class="district"><s>&bull;</s><span>是否可以申请延期还款？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu68" class="area" style="display:none;">                
                您不可以申请延期还款。您需每月按时还款，以免影响您的信用记录。
             </div>
             
             <div onClick="showsubmenu(69)" class="district"><s>&bull;</s><span>如果逾期还款，会有什么惩罚？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu69" class="area" style="display:none;">                
                如果逾期还款，您要承担罚息与逾期后的管理费用，并会被扣去相应的信用分数。联富金融强烈建议您避免逾期还款，一旦发生逾期请尽快还清借款。<br />
                <img src="<%=__PUBLIC__%>/images/help_3.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="30%">逾期天数</td>
                      <td width="30%">1-30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="30%">罚息利率</td>
                      <td width="30%">0.05%</td>
                      <td width="30%">0.1%</td>
                   </tr>
                </table>
                <img src="<%=__PUBLIC__%>/images/help_18.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="30%">逾期天数</td>
                      <td width="30%">1-30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="30%">逾期管理费率</td>
                      <td width="30%">0.1%</td>
                      <td width="30%">0.5%</td>
                   </tr>
                </table>
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
</body>

</html>
