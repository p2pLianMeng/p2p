﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">产品介绍</a>
             <a href="#">借款费用</a>
             <a href="#">如何申请</a>
             <a href="#">认证资料</a>
             <a href="#">信用审核</a>
             <a href="#">信用等级与额度</a>
             <a href="#">筹款与提现</a>
             <a href="#">如何还款</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>产品介绍</div>
             
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>工薪贷</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none">
                <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="7%">&nbsp;</td>
                      <td colspan="2">工薪贷</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">产品介绍</td>
                      <td width="67%">工薪贷（适用工薪阶层）</td>
                   </tr>
                   <tr class="tr_3">
                      <td rowspan="3" width="7%">&nbsp;</td>
                      <td rowspan="3" width="26%">申请条件</td>
                      <td width="67%">（1）22-55周岁的中国公民</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（2）在现单位工作满3个月</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（3）月收入2000以上</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">主要借款用途</td>
                      <td width="67%">装修、结婚、买房、买车、教育（进修、出国留学）、其他消费</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款额度</td>
                      <td width="67%">3,000 - 500,000</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款年利率</td>
                      <td width="67%">10% - 24%</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款期限</td>
                      <td width="67%">3、6、9、12、15、18、24、36个月</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">审核时间</td>
                      <td width="67%">1-3个工作日</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">还款方式</td>
                      <td width="67%">等额本息，每月还款</td>
                   </tr>
                   <tr class="tr_2">
                      <td rowspan="4" width="7%">&nbsp;</td>
                      <td rowspan="4" width="26%">必要申请资料</td>
                      <td width="67%">（1）身份证</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（2）个人信用报告</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（3）劳动合同或在职证明</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（4）近3个月工资卡银行流水</td>
                   </tr>
                </table>
             </div>
             
             
             
             <div onClick="showsubmenu(7)" class="district"><s>&bull;</s><span>生意贷</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1 tr_4">
                      <td width="7%">&nbsp;</td>
                      <td colspan="2">生意贷</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">产品介绍</td>
                      <td width="67%">生意贷（适用私营企业主）</td>
                   </tr>
                   <tr class="tr_3">
                      <td rowspan="3" width="7%">&nbsp;</td>
                      <td rowspan="3" width="26%">申请条件</td>
                      <td width="67%">（1）22-55周岁的中国公民</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（2）企业经营时间满1年</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（3）申请人限于法人代表</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">主要借款用途</td>
                      <td width="67%">流动资金、采购设备或原材料、市场推广费用</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款额度</td>
                      <td width="67%">3,000 - 500,000</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款年利率</td>
                      <td width="67%">10% - 24%</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款期限</td>
                      <td width="67%">3、6、9、12、15、18、24、36个月</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">审核时间</td>
                      <td width="67%">1-3个工作日</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">还款方式</td>
                      <td width="67%">等额本息，每月还款</td>
                   </tr>
                   <tr class="tr_2">
                      <td rowspan="4" width="7%">&nbsp;</td>
                      <td rowspan="4" width="26%">必要申请资料</td>
                      <td width="67%">（1）身份证</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（2）个人信用报告</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（3）经营证明</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（4）近6个月常用银行卡流水</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>网商贷</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1 tr_5">
                      <td width="7%">&nbsp;</td>
                      <td colspan="2">网商贷</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">产品介绍</td>
                      <td width="67%">网商贷（适用淘宝网店商户）</td>
                   </tr>
                   <tr class="tr_3">
                      <td rowspan="5" width="7%">&nbsp;</td>
                      <td rowspan="5" width="26%">申请条件</td>
                      <td width="67%">（1）22-55周岁的中国公民</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（2）在淘宝或天猫经营网店满半年</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（3）店铺等级达到2钻，好评率达到95%，动态评分达到4.5，无法估价产品和虚拟产品占比不超过40% </td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（4）店铺在申请人名下，且近期无淘宝严重处罚</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（5）近3个月交易总额满3万，且交易笔数超过50笔</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">主要借款用途</td>
                      <td width="67%">流动资金、采购设备或原材料、市场推广费用</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款额度</td>
                      <td width="67%">3,000 - 500,000</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款年利率</td>
                      <td width="67%">10% - 24%</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款期限</td>
                      <td width="67%">3、6、9、12、15、18、24、36个月</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">审核时间</td>
                      <td width="67%">1-3个工作日</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">还款方式</td>
                      <td width="67%">等额本息，每月还款</td>
                   </tr>
                   <tr class="tr_2">
                      <td rowspan="3" width="7%">&nbsp;</td>
                      <td rowspan="3" width="26%">必要申请资料</td>
                      <td width="67%">（1）身份证</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（2）提供网店地址及阿里旺旺账号</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（3）需QQ视频审核</td>
                   </tr>
                </table>
             </div>
             
             <div class="down_up"><span>&gt;</span>借款费用</div>
             
             <div onClick="showsubmenu(9)" class="district"><s>&bull;</s><span>借款服务费</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none">
                联富金融收取的借款服务费将全部存于风险备用金账户用于联富金融的本金保障计划。<br />
                服务费将按照借款人的信用等级来收取：<br />
                <table class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="16%">信用等级</td>
                      <td width="12%"><span>AA</span></td>
                      <td width="12%"><span>A</span></td>
                      <td width="12%"><span>B</span></td>
                      <td width="12%"><span>C</span></td>
                      <td width="12%"><span>D</span></td>
                      <td width="12%"><span>E</span></td>
                      <td width="12%"><span>HR</span></td>
                   </tr>
                   <tr>
                      <td width="16%">服务费率</td>
                      <td width="12%">0%</td>
                      <td width="12%">1%</td>
                      <td width="12%">1.5%</td>
                      <td width="12%">2%</td>
                      <td width="12%">2.5%</td>
                      <td width="12%">3%</td>
                      <td width="12%">5%</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(10)" class="district"><s>&bull;</s><span>借款管理费</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                联富金融将按照借款人的借款期限，每月向借款人收取其借款本金的0.3%作为借款管理费。
             </div>
             
             <div onClick="showsubmenu(11)" class="district"><s>&bull;</s><span>充值提现费用</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                充值/提现时由第三方支付收取的支付费用。<br />
                <h1>充值费用</h1>
                第三方支付平台将收取您充值金额的0.5%作为转账费用。扣除手续费的上限为100元。超过100元的部分将由联富金融承担。<br />
                在充值时，用户也可以使用<a href="#">免费充值券</a>，抵去该笔充值费用。<a href="#">如何获得免费充值券？</a><br />
                <h1>提现费用</h1>
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="25%">金额</td>
                      <td width="25%">2万元以下</td>
                      <td width="25%">2万(含)-5万元</td>
                      <td width="25%">5万(含)-100万元</td>
                   </tr>
                   <tr>
                      <td width="25%">手续费</td>
                      <td width="25%">1元/笔</td>
                      <td width="25%">3元/笔</td>
                      <td width="25%">5元/笔</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>逾期罚息</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                当用户的借款发生逾期时，正常利息停止计算。按照下面公式计算罚息：<br />
                <img src="<%=__PUBLIC__ %>/images/help_3.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="40%">逾期天数</td>
                      <td width="30%">1—30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="40%">罚息利率</td>
                      <td width="30%">0.05%</td>
                      <td width="30%">0.1%</td>
                   </tr>
                </table>
             </div>
                          
             <div onClick="showsubmenu(13)" class="district"><s>&bull;</s><span>逾期管理费</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                用户的借款发生逾期时，正常借款管理费用停止计算。按照下面公式计算：<br />
                <img src="<%=__PUBLIC__ %>/images/help_18.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="40%">逾期天数</td>
                      <td width="30%">1—30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="40%">逾期管理费率</td>
                      <td width="30%">0.1%</td>
                      <td width="30%">0.5%</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(14)" class="district"><s>&bull;</s><span>联富金融年利率范围</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu14" class="area" style="display:none;">
                联富金融目前的利率范围为10%-24%。在联富金融平台上借贷的最高年利率设定为同期银行借款年利率的4倍。且随着银行借款利率的调整，联富金融的利率上限也将随之调整。<br />
                注：<br />
                1.联富金融的利率的调整会在商业银行借款年利率调整后1个月内进行调整。<br />
                2.在利率调整之前成功的借款不受调整的影响。
             </div>
             
             <div class="down_up"><span>&gt;</span>如何申请</div>
             
             <div onClick="showsubmenu(15)" class="district"><s>&bull;</s><span>借款申请条件有哪些？是否需要担保和抵押？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu15" class="area" style="display:none;">
                不同产品的申请人需要符合不同的申请条件。<a href="#">查看详情>></a><br />
                您通过联富金融网站申请借款不需要抵押和担保。
             </div>
             
             <div onClick="showsubmenu(16)" class="district"><s>&bull;</s><span>借款申请步骤有哪些？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu16" class="area" style="display:none;">
                1.填写借款申请<br />
                2.填写个人信息和上传认证资料<br />
                3.通过审核<br />
                4.筹集借款<br />
                5.获得借款
             </div>
             
             <div onClick="showsubmenu(17)" class="district"><s>&bull;</s><span>我能获得多少借款金额？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu17" class="area" style="display:none;">
                您可申请的借款额度范围是3,000-500,000（请您按照实际需求填写）。<br />
                您可获得的借款额度将在信审部门审核完成后，由您的整体资质决定（可能与您申请的借款金额不一致）。
             </div>
             
             <div onClick="showsubmenu(18)" class="district"><s>&bull;</s><span>我能选择哪些借款期限和还款方式？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu18" class="area" style="display:none;">
                目前您可选择的借款期限有3个月、6个月、9个月、12个月、15个月、18个月、24个月、36个月。<br />
                联富金融网站的还款方式为等额本息，即每个月还相同金额的本金加利息。（点击<a href="#">借款计算器</a>设置并计算，查看还款明细）
             </div>             
             
             <div onClick="showsubmenu(19)" class="district"><s>&bull;</s><span>如何填写借款申请表？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu19" class="area" style="display:none;">
                1.登陆联富金融网站，打开【我要借款】--【工薪贷】查看详情页面（以工薪贷为例），点击【申请借款】；<br />
                2.进入第①步【填写借款申请】,找到“借款申请信息填写”，输入借款标题、借款用途、借款金额、借款期限、年化利率、借款描述，点击【保存并继续】，进行借款申请。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(20)" class="district"><s>&bull;</s><span>如何修改或取消借款申请？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu20" class="area" style="display:none;">
                在您发布借款申请后，无法自行修改或取消申请，请您在发布时注意认真填写各项信息。如遇特殊情况需修改或取消申请时，您可以联系客服寻求帮助（4006-888-923）。
             </div>
             
             <div class="down_up"><span>&gt;</span>认证资料</div>
             
             <div onClick="showsubmenu(21)" class="district"><s>&bull;</s><span>必要认证资料和可选认证资料有哪些？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                认证项如下：（点击某项认证，查看详细说明）<br />
                必要认证资料<br />
                <h1>工薪贷</h1>
                <a href="#">身份认证</a>、<a href="#">个人信用报告</a>、<a href="#">劳动合同或在职证明</a>、<a href="#">近3个月工资卡银行流水</a><br />
                <h1>生意贷</h1>
                <a href="#">身份认证</a>、<a href="#">个人信用报告</a>、<a href="#">经营证明</a>、<a href="#">近6个月常用银行卡流水</a><br />
                <h1>网商贷</h1>
                <a href="#">身份认证</a>、提供网店地址及阿里旺旺账号、<a href="#">需进行QQ视频审核</a><br />
                <h1>可选认证资料</h1>
                <a href="#">房产认证</a>、<a href="#">购车认证</a>、<a href="#">结婚认证</a>、<a href="#">学历认证</a>、<a href="#">技术职称认证</a>、<a href="#">手机实名认证</a>、<a href="#">微博认证</a>、<a href="#">居住地证明</a><br />
                （点击各项认证，链接至各项认证说明处）
             </div>
             
             <div onClick="showsubmenu(22)" class="district"><s>&bull;</s><span>身份认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                您上传的身份证照片须和您绑定的身份证一致，否则将无法通过认证。<br />
                <h1>请同时提供下面两项资料：</h1>
                1.本人身份证原件的正、反两面照片。<br />
                2.本人手持身份证正面头部照，（确保身份证上的信息没有被遮挡，避免证件与头部重叠，确保身份证内容完整清晰可见）。<br />
                <h1>认证示例：</h1>
                <a href="#">身份证正反面照片</a><br />
                <a href="#">本人手持身份证正面头部照片</a><br />
                <h1>认证有效期：</h1>
                永久
             </div>
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>信用报告</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                个人信用报告是由中国人民银行出具，全面记录个人信用活动，反映个人信用基本状况的文件。本报告是联富金融了解您信用状况的一个重要参考资料。<br />
                <h1>认证说明：</h1>
                个人信用报告需15日内开具。<br />
                <h1>认证条件：</h1>
                信用记录良好。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">个人信用报告</a><br />
                <h1>如何办理：</h1>
                可去当地人民银行打印，部分地区可登录个人信用信息服务平台。<br />
                <a href="#">全国各地征信中心联系方式查询</a><br />
                <a href="#">个人信用信息服务平台</a><br />
                <a href="#">关于个人查询本人信用报告实施收费的公告</a><br />
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>（工薪贷）工作认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none;">
                您的工作状况是联富金融评估您信用状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.加盖单位公章（或劳动合同专用章）的劳动合同。<br />
                2.最近1个月内开具的加盖单位公章（或人力章、财务章）的机打（手写无效）在职证明。<br />
                3.带有姓名、照片、工作单位名称的工作证。<br />
                <h1>认证条件：</h1>
                本人需在现单位工作满3个月。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">劳动合同</a><br />
                <a href="#">在职证明</a><br />
                <a href="#">工作证</a><br />
             </div>
             
             <div onClick="showsubmenu(25)" class="district"><s>&bull;</s><span>（生意贷）工作认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                您的工作状况是联富金融评估您信用状况的主要依据之一。<br />
                2. 经营场地租赁合同＋90天内的租金发票或水电单据。<br />
                <h1>认证条件：</h1>
                本人名下的企业经营时间需满1年。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">营业执照正、副本</a><br />
                <a href="#">租赁合同和水电发票 </a><br />
             </div>
             
             <div onClick="showsubmenu(26)" class="district"><s>&bull;</s><span>（网商贷）工作认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                您的工作状况是联富金融评估您信用状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                网商用户无需上传任何工作认证相关资料，但需进行视频审核，联富金融客服会尽快主动联络您进行视频审核。在审核过程中，需远程展示以下资料：<br />
                1.淘宝/天猫近3个月交易明细或支付宝近12个月账单。<br />
                2.淘宝/天猫的支付宝实名认证页面。<br />
                3.淘宝/天猫信用以及订单借款账户页面。<br />
                4.淘宝/天猫处罚积分页面。<br />
                <h1>认证条件：</h1>
                本人名下的店铺需在淘宝或天猫经营满半年，且近期无淘宝严重处罚，店铺等级达到2钻, 好评率达到95%，动态评分达到4.5，无法估价产品和虚拟产品占比不超过40%。近3个月交易总额满3万，且交易笔数超过50笔。<br />
                <h1>认证有效期：</h1>
                6个月
             </div>
                          
             <div onClick="showsubmenu(27)" class="district"><s>&bull;</s><span>（工薪贷）收入认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu27" class="area" style="display:none;">
                您的银行流水单是联富金融评估您收入状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.可体现工资项的最近3个月的工资卡银行流水单。<br />
                2.可体现工资项的最近3个月的网银电脑截屏。<br />
                <h1>认证条件：</h1>
                本人名下近3个月的月收入均在2000以上。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">银行卡流水</a><br />
                <a href="#">网银电脑截图</a>
             </div>
             
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>（生意贷）收入认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                您的银行流水单是联富金融评估您收入状况的主要依据之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.可体现经营情况的最近6个月的个人银行卡流水单，或网银电脑截屏。<br />
                2.可体现经营情况的最近6个月的企业银行卡流水单，或网银电脑截屏。<br />
                <h1>认证条件：</h1>
                本人提交的流水可反映真实有效的经营情况。<br />
                <h1>认证有效期：</h1>
                6个月<br />
                <h1>认证示例：</h1>
                <a href="#">银行卡流水</a><br />
                <a href="#">网银电脑截图</a>
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>房产认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                房产认证是证明借入者资产及还款能力的重要凭证之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.房屋产权证明。<br />
                2.购房合同 + 近3个月的还贷流水。<br />
                3.购房发票 + 近3个月的还贷流水。<br />
                4.按揭合同 + 近3个月的还贷流水。<br />
                <h1>认证条件：</h1>
                必须是商品房，且房产是本人名下所有或共有的。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">房产证</a><br />
                <a href="#">购房发票</a><br />
                <a href="#">购房合同</a>
             </div>
             
             <div onClick="showsubmenu(30)" class="district"><s>&bull;</s><span>购车认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu30" class="area" style="display:none;">
                购车认证是证明借入者资产及还款能力的重要凭证之一。<br />
                <h1>认证说明：</h1>
                请同时提供下面两项资料：<br />
                1.车辆行驶证的原件照片。<br />
                2.本人和车辆的合影（照片需露出车牌号码）。<br />
                <h1>认证条件：</h1>
                车辆必须是本人名下所有。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">车辆行驶证</a><br />
                <a href="#">与车辆合影 </a>
             </div>
             
             <div onClick="showsubmenu(31)" class="district"><s>&bull;</s><span>结婚认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu31" class="area" style="display:none;">
                借入者婚姻状况的稳定性，是联富金融考核借款人信用的评估因素之一。<br />
                <h1>认证说明：</h1>
                请同时提供下面三项资料：<br />
                1.结婚证书的原件照片。<br />
                2.配偶身份证原件的正、反两面照片。<br />
                3.本人和配偶的近照合影一张。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">结婚证</a>
             </div>
             
             <div onClick="showsubmenu(32)" class="district"><s>&bull;</s><span>学历认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu32" class="area" style="display:none;">
                借出者在选择借款申请投标时，借入者的学历也是一个重要的参考因素。为了让借出者更好、更快地相信您的学历是真实的，建议您对学历进行在线验证。<br />
                <h1>认证说明：</h1>
                2001年至今获得学历，需学历证书编号：<br />
                1.访问<a href="#">学信网</a>，找到“零散查询”信息，点击【查询】；<br />
                2.输入证书编号、姓名、查询码（通过手机短信获得，为12位学历查询码）、验证码，进行查询；<br />
                3.查询成功后，您将查获得《教育部学历证书电子注册备案表》；<br />
                4.将该表右下角的12位在线验证码（见学<a href="#">历认证1</a>）输入文本框，点击【提交审核】。<br />
                1991年至今获得学历，无需学历证书编号：<br />
                1.访问<a href="#">学信网</a>，找到“本人查询”信息，点击【查询】；<br />
                2.注册学信网账号，登录<a href="#">学信网</a>，访问【学历信息】页面；<br />
                3.选择您的最高学历，点击【申请验证报告】（通过手机短信获得12位学历查询码，此查询码与联富金融所需验证码不同）；<br />
                4.申请成功后，您将获得12位在线验证码<a href="#">（见学历认证2）</a>；<br />
                5.将12位在线验证码输入文本框，点击【提交审核】。<br />
                <h1>认证条件：</h1>
                大专或以上学历（普通全日制）。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>认证示例：</h1>
                <a href="#">学历认证1</a><br />
                <a href="#">学历认证2</a>
             </div>
             
             <div onClick="showsubmenu(33)" class="district"><s>&bull;</s><span>技术职称认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu33" class="area" style="display:none;">
                技术职称是经专家评审、反映一个人专业技术水平并作为聘任专业技术职务依据的一种资格。不与工资挂钩，是联富金融考核借款人信用的评估因素之一。<br />
                <h1>认证说明：</h1>
                技术职称证书的原件照片。<br />
                <h1>认证条件：</h1>
                国家承认的二级及以上等级证书。例如律师证、会计证、工程师证等。<br />
                <h1>认证有效期：</h1>
                永久<br />
             </div>
             
             <div onClick="showsubmenu(34)" class="district"><s>&bull;</s><span>手机实名认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu34" class="area" style="display:none;">
                手机实名认证需要用户上传手机流水单。手机流水单是用户最近一段时间内的详细通话记录，是联富金融用以验证借入者真实性的重要凭证之一。<br />
                <h1>认证说明：</h1>
                绑定的手机号码最近3个月的手机详单原件的照片。如详单数量较多可分月打印并上传每月前5日部分（每月详单均需清晰显示机主手机号码）。<br />
                <h1>认证有效期：</h1>
                永久<br />
                <h1>如何办理：</h1>
                前往最近的手机运营商营业厅，提供手机服务密码或机主身份证明即可打印。
             </div>
             
             <div onClick="showsubmenu(35)" class="district"><s>&bull;</s><span>微博认证</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu35" class="area" style="display:none;">
                随着网络时代的发展，网络信息已成为了解借入者真实性的最新渠道之一。联富金融与时俱进，将借入者网络关系评估，作为评估借入者的信用的一种方式。<br />
                <h1>认证说明：</h1>
                1.成为联富金融粉丝：<br />
                请点击链接<a href="#">http://e.weibo.com/renrendai</a>，把联富金融加为关注<br />
                请点击链接<a href="#">http://t.qq.com/renrendai</a>，把联富金融加为关注；<br />
                2. 发送验证信息<br />
                在微博上，转发一条最新的联富金融的微博，并用“私信”功能，发送您的联富金融昵称给联富金融；<br />
                3. 在输入框输入您的新浪微博昵称输入文本框，点击【提交审核】。<br />
                <h1>认证条件：</h1>
                50粉丝 + 50微博（目前仅支持新浪微博）<br />
                <h1>认证有效期：</h1>
                永久
             </div>
             
             <div onClick="showsubmenu(36)" class="district"><s>&bull;</s><span>居住地证明</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu36" class="area" style="display:none;">
                居住地的稳定性，是联富金融考核借款人的主要评估因素之一。<br />
                <h1>认证说明：</h1>
                请提供下面任意一项资料：<br />
                1.用本人姓名登记的水、电、气最近3个月缴费单。<br />
                2.用本人姓名登记的固定电话最近3个月缴费单。<br />
                3.本人的信用卡最近2个月的月结单。<br />
                4.本人的自有房产证明。<br />
                5.本人父母的房产证明，及证明本人和父母关系的证明材料。<br />
                <h1>认证有效期：</h1>
                6个月
             </div>
             
             <div onClick="showsubmenu(37)" class="district"><s>&bull;</s><span>（工薪贷）工作单位的注册地必须在中国大陆吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu37" class="area" style="display:none;">
                联富金融网站支持的用户是中国大陆地区公民，且工作单位或经营场所注册地必须在中国大陆，港澳台地区目前不支持。
             </div>
             
             <div onClick="showsubmenu(38)" class="district"><s>&bull;</s><span>（工薪贷）工资发现金，没有发到银行卡上怎么办？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu38" class="area" style="display:none;">
                如果您无法提供工资卡流水，也可提供能体现您收支情况的常用银行卡流水。
             </div>
             
             <div onClick="showsubmenu(39)" class="district"><s>&bull;</s><span>（生意贷）没有经营地证明怎么办？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu39" class="area" style="display:none;">
                经营地证明中的经营场地租赁合同是您必须要提供的，90天内的租金发票或水电单据您可选择其中一项提供，如果您的水电单据存在特殊情况时，客户经理将致电向您了解具体情况并沟通解决办法。
             </div>
             
             <div onClick="showsubmenu(40)" class="district"><s>&bull;</s><span>（生意贷）借款人不是法人本人，可以申请吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu40" class="area" style="display:none;">
                生意贷产品要求申请人限于法人代表。如遇申请人身份特殊时，客户经理将致电向您了解具体情况并沟通解决办法。
             </div> 
             
             <div onClick="showsubmenu(41)" class="district"><s>&bull;</s><span>（生意贷）借款人是法人本人，但银行流水在别人名下，可以申请吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu41" class="area" style="display:none;">
                生意贷产品要求提供法人名下或企业对公银行流水。如遇申请人情况特殊时，客户经理将致电向您了解具体情况并沟通解决办法。
             </div> 
             
             <div onClick="showsubmenu(42)" class="district"><s>&bull;</s><span>（网商贷）借款人不是淘宝或天猫商户，可以申请吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu42" class="area" style="display:none;">
                目前联富金融网站暂不支持非淘宝或天猫的网商申请借款，要求至少在淘宝或天猫经营网店满半年。
             </div>
             
             <div onClick="showsubmenu(43)" class="district"><s>&bull;</s><span>“完整的银行流水，中间不能出现断层”是什么意思？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu43" class="area" style="display:none;">
                要求银行流水有连续月份，内容包括交易日期、交易金额、余额、交易地点、交易摘要等。（点击<a href="#">银行卡流水</a>，查看示例图片） 
             </div> 
             
             <div onClick="showsubmenu(44)" class="district"><s>&bull;</s><span>如何上传、补充、修改或撤销资料？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu44" class="area" style="display:none;">
                1.登陆联富金融网站，打开【我要借款】--【工薪贷】查看详情页面（以工薪贷为例），点击【申请借款】，【填写借款申请】后，您可以在【填写借款信息】的过程中上传【必要认证资料】；<br />
                2.您也可以在【上传资料】页面中上传所有【必要上传资料】和【可选上传资料】。请您确保所有资料上传完整后点击【提交申请】，【提交申请】后您将无法补充上传资料。<br />
                <a href="#">查看操作流程>></a><br />
                确认【提交申请】后，我们的客户经理将检查您的资料，如果您的资料上传有误或未上传齐全，我们将驳回您的资料，您可继续操作补充资料（建议您一次性将资料补充完整）。<br />
                资料上传后，无法操作修改和撤销，您可按照上述说明继续补充上传。
             </div> 
             
             <div class="down_up"><span>&gt;</span>信用审核</div>
             
             <div onClick="showsubmenu(45)" class="district"><s>&bull;</s><span>什么是信用审核？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu45" class="area" style="display:none;">
                申请借款的用户需要根据不同的产品填写借款信息，包括个人信息、家庭信息、工作信息、资产信息、信用信息，并提交相应的信用认证材料。信审部门会综合评估借款人的个人、家庭、工作、资产、信用情况，最终根据借款人的整体信用资质给出相应的信用分数、信用等级及借款额度。
             </div> 
             
             <div onClick="showsubmenu(46)" class="district"><s>&bull;</s><span>提交认证资料后何时开始审核？审核时间多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu46" class="area" style="display:none;">
                我们将在您点击【提交申请】后，确认您的必要认证资料已上传齐全，才会为您提交审核。
    您的借款申请将于3个工作日内为您审核完成。
             </div> 
             
             <div onClick="showsubmenu(47)" class="district"><s>&bull;</s><span>为何我提交一部分认证资料，但3个工作日后没有收到审核结果？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu47" class="area" style="display:none;">
                请确认您的必要认证资料已上传完整，且已点击【提交申请】，才会进入审核。
             </div> 
             
             <div onClick="showsubmenu(48)" class="district"><s>&bull;</s><span>如何查看我的审核进度和结果？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu48" class="area" style="display:none;">
                登陆联富金融网站，打开【我要借款】--【工薪贷】查看详情页面（以工薪贷为例），点击【申请借款】，【填写借款信息】并点击【提交申请】后，即可查看您的审核进度。<a href="#">查看操作流程>></a><br />
                审核完成后，如果您的申请通过，您将收到审核通过的站内信和邮件通知，同时客户经理将致电您沟通借款金额、期限和利率；<br />
                如果您的申请未通过，您将收到申请驳回的短信、站内信和邮件通知，您可登陆联富金融账户或邮箱查看驳回原因。
             </div> 
             
             <div onClick="showsubmenu(49)" class="district"><s>&bull;</s><span>如何联系审核人员？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu49" class="area" style="display:none;">
                您无需主动联系审核员，审核员在需要时会主动联系您。如遇特殊情况时，客户经理将致电与您沟通，您也可以联系客服寻求帮助（4006-888-923）。
             </div> 
             
             <div onClick="showsubmenu(50)" class="district"><s>&bull;</s><span>审核员会打电话给我的工作单位或亲属朋友吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu50" class="area" style="display:none;">
                审核人员会根据您在申请表中填写的工作单位或亲属朋友联系人信息，进行电话核实。在致电您的亲属朋友联系人核实前，会先与您进行电话确认。
             </div> 
             
             <div onClick="showsubmenu(51)" class="district"><s>&bull;</s><span>审核过程主要看哪些方面？如何提高我的借款成功率？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu51" class="area" style="display:none;">
                信审部门会综合评估您的整体资质，包括您的个人情况、家庭情况、工作情况、资产情况、信用情况等。我们建议您尽量全面的上传真实有效的必要认证资料和可选认证资料，以提高您的借款成功率。
             </div>
             
             <div onClick="showsubmenu(52)" class="district"><s>&bull;</s><span>资料被驳回的原因是什么？可否重新认证？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu52" class="area" style="display:none;">
                1.资料上传不完整：您可以按照资料驳回的站内信和邮件通知，重新认证上传；<br />
                2.资料暂不符合认证要求：您的资料未能通过审核，无法再次认证。
             </div>
             
             <div onClick="showsubmenu(53)" class="district"><s>&bull;</s><span>被拒贷或流标的原因是什么？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu53" class="area" style="display:none;">
                可能的原因有：<br />
                1.您的年龄暂不符合借款要求（22-55周岁）。<br />
                2.您本人不符合申请人身份条件。<br />
                3.您的工作年限或经营时间不符合申请条件。<br />
                4.您的企业或网店不符合申请条件。<br />
                5.您的月收入或月交易额不符合申请条件。<br />
                6.您在审核过程中未配合完成补充资料或信息核实工作，或您主动放弃申请。<br />
                7.审核人员对您的身份信息、工作信息、信用记录、还款能力等进行审核后认为您目前综合评分不足，暂时不符合借款要求。
             </div>
             
             <div onClick="showsubmenu(54)" class="district"><s>&bull;</s><span>审核失败后如何操作？何时能再次申请？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu54" class="area" style="display:none;">
                审核失败后，您将收到资料驳回和借款申请驳回的短信、站内信和邮件通知，您可登陆联富金融账户或邮箱查看驳回原因。（<a href="#">点击这里</a>了解可能导致审核失败的原因）<br />
                如果您因个人信息或认证资料未提交齐全导致审核失败，您可随时再次发布借款申请；<br />
                如果您因申请条件不符合要求或认证资料未通过审核，您可在半年后再次发布借款申请。
             </div>
             
             <div onClick="showsubmenu(55)" class="district"><s>&bull;</s><span>审核成功后如何操作？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu55" class="area" style="display:none;">
                审核成功后，您将收到审核通过的站内信和邮件通知，同时客户经理将致电您沟通借款金额、期限和利率，确认后帮助您进入筹款。                
             </div>
             
             <div onClick="showsubmenu(56)" class="district"><s>&bull;</s><span>申请资料能否退还给我？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu56" class="area" style="display:none;">
                根据行业规范，对于用户申请过程中提交的信息和资料不予退还。<br />
                联富金融严格遵守国家相关的法律法规，对用户的隐私信息进行保护。未经您的同意，联富金融不会向任何第三方公司、组织和个人披露您的个人信息、账户信息以及交易信息（法律法规另有规定的除外）。<br />
                同时，联富金融公司内部也设有严格、完善的权限管理体系，以保证每一位内部员工都只能查看自己职责和权限之内的数据和信息。
             </div>
             
             <div class="down_up"><span>&gt;</span>信用等级与额度</div>
             
             <div onClick="showsubmenu(57)" class="district"><s>&bull;</s><span>什么是信用分数和信用等级？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu57" class="area" style="display:none;">
                用户的信用分数是在通过联富金融审核后获得的，信用等级由信用分数转化而来，每个信用等级都对应的信用分数范围，信用分数和信用等级是借款人的信用属性，也是理财人判断借款人违约风险的重要依据之一。通常来讲借款人信用等级越高，其违约率越低，相应的借款成功率越高。<br />
                目前信用等级由高到低分为AA、A、B、C、D、E、HR，具体请参考信用等级的分数区间：<br />
                <table class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="16%">信用等级</td>
                      <td width="12%"><span>HR</span></td>
                      <td width="12%"><span>E</span></td>
                      <td width="12%"><span>D</span></td>
                      <td width="12%"><span>C</span></td>
                      <td width="12%"><span>B</span></td>
                      <td width="12%"><span>A</span></td>
                      <td width="12%"><span>AA</span></td>
                   </tr>
                   <tr>
                      <td width="16%">分数区间</td>
                      <td width="12%">0-99</td>
                      <td width="12%">100-109</td>
                      <td width="12%">110-119</td>
                      <td width="12%">120-129</td>
                      <td width="12%">130-144</td>
                      <td width="12%">145-159</td>
                      <td width="12%">160+</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(58)" class="district"><s>&bull;</s><span>什么是信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu58" class="area" style="display:none;">
                用户的信用额度是在通过联富金融审核员对所提供材料的审核后获得的，既是借款人单笔借款的上限也是借款者累积尚未还清借款的上限。<br />
                例如，如果一个借款人信用额度为5万元，则在没有其他借款的情况下，用户可以发布总额最高为5万元的借款请求。也可以分多次发布借款请求，但尚未还清借款（以整笔借款金额计算）的总额不得超过5万元。<br />
             </div>
             
             <div onClick="showsubmenu(59)" class="district"><s>&bull;</s><span>如何获得信用分数、信用等级与信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu59" class="area" style="display:none;">
                请您申请借款并上传认证资料，审核完成后，您将获得相应的信用分数、信用等级和信用额度。
             </div>
             
             <div onClick="showsubmenu(60)" class="district"><s>&bull;</s><span>如何查看我的信用分数、信用等级与信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu60" class="area" style="display:none;">
                登陆联富金融网站，打开【我的联富金融】--【账户管理】--【认证信息】页面，即可查看您的信用分数、信用等级、信用额度，及您进行的各项信用认证的情况。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(61)" class="district"><s>&bull;</s><span>如何提高我的信用等级（信用分数）？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu61" class="area" style="display:none;">
                您可以通过提交【可选认证资料】来提高信用分数，另外您在联富金融网站上良好的还款记录也会增加您的信用分数。
             </div>
             
             <div onClick="showsubmenu(62)" class="district"><s>&bull;</s><span>如何提高我的信用额度？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu62" class="area" style="display:none;">
                <h1>提额申请资格说明</h1>
                您之前已获得信用额度，且至少已成功借款一笔，目前您名下的借款已全部结清，无在还借款。<br />
                <h1>提额申请办法</h1>
                如果您目前的状况满足上述情况，请您重新发布借款申请，申请金额请按照实际需求填写，不限定在已获得的信用额度范围之内。
             </div>
             
             <div class="down_up"><span>&gt;</span>筹款与提现</div>
             
             <div onClick="showsubmenu(63)" class="district"><s>&bull;</s><span>审核成功后如何进行筹款？筹款时间多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu63" class="area" style="display:none;">
                审核成功后，您可得到相应的信用等级和信用额度，同时客户经理将致电您沟通借款金额、期限和利率，确认后帮助您进入筹款。<br />
                您的借款金额将于1-3天内为您筹集完成。
             </div>
             
             <div onClick="showsubmenu(64)" class="district"><s>&bull;</s><span>筹款成功后如何进行提现？到账时间多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu64" class="area" style="display:none;">                
                <a href="#">查看提现流程>></a><br />
                资金将在申请提现后1-2个工作日内到达您指定的银行账户。 
             </div>
             
             <div class="down_up"><span>&gt;</span>如何还款</div>
             
             <div onClick="showsubmenu(65)" class="district"><s>&bull;</s><span>如何进行还款？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu65" class="area" style="display:none;">                
                1.当账户余额不足支付当期还款时，您可先通过网银为账户充值；<br />
                2.充值完成后，您可以自行点击还款；<br />
                3.充值完成后，您也可以等待系统自动扣款，无需自行点击还款。<a href="#">查看操作流程>></a> 
             </div>
             
             <div onClick="showsubmenu(66)" class="district"><s>&bull;</s><span>我会收到哪些形式的还款提醒？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu66" class="area" style="display:none;">                
                您会在每月还款日的前三天以电子邮件、站内信的方式收到还款提醒，并在还款日的前一天接到客服人员的电话提醒。
             </div>
             
             <div onClick="showsubmenu(67)" class="district"><s>&bull;</s><span>是否可以提前还款？如何操作？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu67" class="area" style="display:none;">                
                您可以随时进行提前还款。<br />
                登陆联富金融网站，打开【我的联富金融】--【借款管理】--【我的借款】页面，点击【提前还款】进行提前还款操作（与还款操作类似）。<br />
                提前还款的用户需要支付给理财人借款剩余本金的1%作为违约金，不用再支付后续的利息及管理费用。提前还款金额公式如下：<br /> 
                <img src="<%=__PUBLIC__ %>/images/help_4.png" /><br />
                注：提前归还部分借款不视为提前还款，仍需支付全部借款利息及账户管理费。
             </div>
             
             <div onClick="showsubmenu(68)" class="district"><s>&bull;</s><span>是否可以申请延期还款？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu68" class="area" style="display:none;">                
                您不可以申请延期还款。您需每月按时还款，以免影响您的信用记录。
             </div>
             
             <div onClick="showsubmenu(69)" class="district"><s>&bull;</s><span>如果逾期还款，会有什么惩罚？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu69" class="area" style="display:none;">                
                如果逾期还款，您要承担罚息与逾期后的管理费用，并会被扣去相应的信用分数。联富金融强烈建议您避免逾期还款，一旦发生逾期请尽快还清借款。<br />
                <img src="<%=__PUBLIC__ %>/images/help_3.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="30%">逾期天数</td>
                      <td width="30%">1-30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="30%">罚息利率</td>
                      <td width="30%">0.05%</td>
                      <td width="30%">0.1%</td>
                   </tr>
                </table>
                <img src="<%=__PUBLIC__ %>/images/help_18.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="30%">逾期天数</td>
                      <td width="30%">1-30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="30%">逾期管理费率</td>
                      <td width="30%">0.1%</td>
                      <td width="30%">0.5%</td>
                   </tr>
                </table>
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


	<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
