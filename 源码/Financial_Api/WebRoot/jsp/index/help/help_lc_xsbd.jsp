﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html" style="background:#f8bbb2;color:#fff;"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">理财新手必读</a>
             <a href="#">产品介绍</a>
             <a href="#">收益与费用</a>
             <a href="#">联富宝</a>
             <a href="#">散标投资</a>
             <a href="#">债权转让</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
                       
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>我可以理财吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none;">可以，年满18周岁，具有完全民事权利能力和民事行为能力，可以在联富金融网站上进行注册、完成实名认证、绑定银行卡，成为理财人。</div>
             
             <div onClick="showsubmenu(7)" class="district"><s>&bull;</s><span>怎样进行投资？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                <h1>请您按照以下步骤进行投资：</h1>
                1. 在联富金融网站上进行注册、通过实名认证、成功绑定银行卡；<br />
                2. 账户充值；<br />
                3. 浏览联富宝、散标投资列表、债权转让列表，选择自己感兴趣的投资方式；<br />
                4. 确认投资，投资成功。
             </div>
             
             <div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>理财收益有多少？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                <h1>联富宝</h1>
                联富宝A预期年化收益7%。<br />
                联富宝B预期年化收益9%。<br />
                联富宝C预期年化收益11%。<br />
                <h1>散标</h1>
                年利率区间8%—24%。
                <h1>债权转让</h1>
                年利率区间8%—24%。
             </div>
             
             <div onClick="showsubmenu(9)" class="district"><s>&bull;</s><span>理财期限有多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none;">
                <h1>联富宝</h1>
                联富宝A锁定期3个月，到期后自动退出。<br />
                联富宝B锁定期6个月，到期后自动退出。<br />
                联富宝C锁定期12个月，到期后自动退出。<br />
                <h1>散标投资</h1>
                借款期限3-36个月，持有90天后可提前转让退出。
                <h1>购买转让的债权</h1>
                借款期限3-36个月，随时可提前转让退出。<br />
                相关阅读：<a href="#">锁定期是什么？</a>
             </div>
             
             <div onClick="showsubmenu(10)" class="district"><s>&bull;</s><span>投资后是否可以提前退出？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                <h1>可以</h1>
                联富宝可随时提前退出。<br />
                散标投资持有90天后，可通过转出债权退出。<br />
                买入的转让债权可随时退出。<br />
             </div>
             
             <div onClick="showsubmenu(11)" class="district"><s>&bull;</s><span>理财是否有额外费用？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                理财过程中可能产生的费用如下：<br />
                1.充值/提现时由第三方支付收取的支付费用。<br />
                <h1>充值费用</h1>
                第三方支付平台将收取您充值金额的0.5%作为转账费用。扣除手续费的上限为100元。超过100元的部分将由联富金融承担。<br />
                在充值时，用户也可以使用<a href="#">免费充值券</a>，抵去该笔充值费用。<a href="#">如何获得免费充值券？</a><br />
                <h1>提现费用</h1>
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="25%">金额</td>
                      <td width="25%">2万元以下</td>
                      <td width="25%">2万(含)-5万元</td>
                      <td width="25%">5万(含)-100万元</td>
                   </tr>
                   <tr>
                      <td width="25%">手续费</td>
                      <td width="25%">1元/笔</td>
                      <td width="25%">3元/笔</td>
                      <td width="25%">5元/笔</td>
                   </tr>
                </table>
                2.联富宝相关费用，详情可参见<a href="#">联富宝费用。</a><br />
                3.提前赎回散标或债权时产生的债权转让管理费（目前为转出价格的0.5%）。
             </div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>什么是等额本息？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<br />
                具体计算公式如下：<br />
                <img src="<%=__PUBLIC__ %>/images/help_2.png" /><br />
                P&nbsp;:每月还款额<br />
                A&nbsp;:借款本金<br />
                b&nbsp;:月利率<br />
                n&nbsp;:还款总期数<br />
                因计算中存在四舍五入，最后一期还款金额与之前略有不同。
             </div>      
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


	<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
