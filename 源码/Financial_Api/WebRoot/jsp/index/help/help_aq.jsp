﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">本金保障计划</a>
             <a href="#">法律与政策保障</a>
             <a href="#">借款审核与风控</a>
             <a href="#">账户及隐私安全</a>
             <a href="#">用户的自我保护</a>
             <a href="#">网站相关协议</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>本金保障计划</div>
             
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>在联富金融投资能否保障本金安全？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none;">
                为保障理财人的出借安全，联富金融设有适用于全体理财用户的本金保障计划。
             </div>
             
             <div onClick="showsubmenu(7)" class="district"><s>&bull;</s><span>什么是本金保障计划</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                “本金保障计划”是联富金融商务顾问（北京）有限公司(下称“联富金融”，运营www.renrendai.com网站（下称“平台”））为保护平台全体理财人的共同权益而建立的信用风险共担机制。除另有说明外，平台的所有理财人经过平台身份认证后，在平台的投资行为均适用于“本金保障计划”，理财人无需为此支付任何费用。<br />
                “本金保障计划”是指在平台发生的适用本金保障计划的每笔借款（下称“受保障借款”，是否适用以平台明示为准）成交时，提取一定比例的金额放入“风险备用金账户”。当理财人投资的某笔受保障借款出现严重逾期时（即逾期超过30天），联富金融将根据“风险备用金账户使用规则”通过“风险备用金”向理财人偿付此笔借款的剩余出借本金或剩余出借本金和逾期当期利息（具体情况视投资标的类型的具体偿付规则为准）。“本金保障计划”为理财人提供了有效的风险共担机制，分散了理财人投资行为所带来的信用风险，营造了一个安全健康的投资环境，保障了理财人的权益。<a href="#">查看详情>></a>
             </div>
             
             <div class="down_up"><span>&gt;</span>法律与政策保障</div>
             
             <div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>联富金融平台提供居间撮合服务的合法性</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                《合同法》第23章专门对“居间合同”作出规定，其第424条明确定义为：“居间合同是居间人向委托人报告订立合同的机会或者提供订立合同的媒介服务，委托人支付报酬的合同”。联富金融平台是合法设立的中介服务机构，致力于为民间借贷业务提供优质高效的撮合服务，以促成借贷双方形成借贷关系，然后收取相关报酬。此种居间服务有着明确的法律依据。
             </div>
             
             <div onClick="showsubmenu(9)" class="district"><s>&bull;</s><span>理财人及借款人之间的借贷关系的合法性</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none;">
                《合同法》第196条规定：“借款合同是借款人向贷款人借款，到期返还借款并支付利息的合同”；根据《合同法》第十二章“借款合同”和最高人民法院《关于人民法院审理借贷案件的若干意见》，我国法律允许自然人等普通民事主体之间发生借贷关系，并允许出借方到期可以收回本金和符合法律规定的利息。理财人作为借款人，与贷款人之间形成的借贷关系受到法律保护。
             </div>
             
             <div onClick="showsubmenu(10)" class="district"><s>&bull;</s><span>理财人通过联富金融平台获得的出借理财收益的合法性</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                根据最高人民法院《关于人民法院审理借贷案件的若干意见》第6条：“民间借贷的利率可以适当高于银行的利率，各地人民法院可以根据本地区的实际情况具体掌握，但最高不得超过银行同类贷款利率的四倍（包含利率本数）。超出此限度的，超出部分的利息不予保护。”联富金融平台上理财人向借款人出借资金并按照约定利率收取利息，该利率未超过银行同类贷款利率的四倍，为合法利息收益，受到法律保护。
             </div>
             
             <div onClick="showsubmenu(11)" class="district"><s>&bull;</s><span>电子合同的合法性</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                根据《合同法》第11条的规定，当事人可以采用合同书、信件和数据电文（包括电报、电传、传真、电子数据交换和电子邮件）等形式订立合同。电子合同是法律认可的书面合同形式之一。联富金融采取用户网上点击确认的方式签署电子合同。点击确认后的电子合同符合《中华人民共和国合同法》规定的合同成立、生效的要件，其有效性也被人民法院的司法实践所接受。
             </div>
             
             <div class="down_up"><span>&gt;</span>借款审核与风控</div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>严格的贷前审核</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                在客户提出借款申请后，联富金融会对客户的基本资料进行分析。通过网络、电话及其他可以掌握的有效渠道进行详实、仔细的调查。避免不良客户的欺诈风险。在资料信息核实完成后，根据个人信用风险分析系统进行评估，由经验丰富的借款审核人员进行双重审核确认后最终决定批核结果。
             </div>   
             
             <div onClick="showsubmenu(13)" class="district"><s>&bull;</s><span>完善的贷后管理</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                如果用户逾期未归还借款，贷后管理部门将第一时间通过短信、电话等方式提醒用户进行还款。如果用户在5天内还未归还当期借款，联富金融将会联系该用户的紧急联系人、直系亲属、单位等督促用户尽快还款。如果用户仍未还款，交由专业的高级催收团队与第三方专业机构合作进行包括上门等一系列的催收工作，直至采取法律手段。
             </div>
             
             <div class="down_up"><span>&gt;</span>账户及隐私安全</div>
             
             <div onClick="showsubmenu(14)" class="district"><s>&bull;</s><span>网站技术保障</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu14" class="area" style="display:none;">
                联富金融网站运用各种先进的安全技术保护用户在联富金融账户中存储的个人信息、账户信息以及交易记录，以免用户账户遭受未经授权的访问、使用以及信息泄露。<br />
                联富金融网站有着完善的安全监测系统，可以及时发现网站的非正常访问并做相应的安全响应。对于用户的账户信息，联富金融网站会对其进行高强度的加密，以防止用户信息的外泄。<br />
                同时，我们还会持续更新和改进网站的安全策略，以保证网站安全策略的有效性和健壮性。
             </div>
             
             <div onClick="showsubmenu(15)" class="district"><s>&bull;</s><span>权限管理和隐私安全</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu15" class="area" style="display:none;">
                我们严格遵守国家相关的法律法规，对用户的隐私信息进行保护。未经您的同意，联富金融不会向任何第三方公司、组织和个人披露您的个人信息、账户信息以及交易信息（法律法规另有规定的除外）。<br />
                同时，联富金融公司内部也设有严格、完善的权限管理体系，以保证每一位内部员工都只能查看自己职责和权限之内的数据和信息。
             </div>
             
             <div class="down_up"><span>&gt;</span>用户的自我保护</div>
             
             <div onClick="showsubmenu(16)" class="district"><s>&bull;</s><span>牢记联富金融官方网址</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu16" class="area" style="display:none;">
                牢记联富金融官方网址：<a href="#">www.lfoll.com</a><br />
                不要点击来历不明的链接访问联富金融，谨防网站钓鱼和欺诈。我们建议您将联富金融官方网址加入浏览器收藏夹，以方便您的下次登录。
             </div>     
             
             <div onClick="showsubmenu(17)" class="district"><s>&bull;</s><span>为您的联富金融账户设置高强度的登录密码</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu17" class="area" style="display:none;">
                您在密码设置时，最好使用数字和字母混合，不要使用纯数字或纯字母，且密码长度要在6位以上。<br />
                不要使用您的生日、姓名拼音、身份证号码、手机号或是邮箱名作为登录密码。<br />
                不要使用连续的，或重复的字母、数字组合作为密码，例如：aaaaaa，111111，abcdef，123456。<br />
                不要使用以下常用密码，例如：qazwsx，qwerty，mima123，password等。
             </div>
             
             <div onClick="showsubmenu(18)" class="district"><s>&bull;</s><span>注重电脑运行环境的安全</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu18" class="area" style="display:none;">
                1.及时为您的电脑进行系统更新，安装系统安全补丁，以防系统漏洞被黑客利用。<br />
                2.为您的电脑安装杀毒软件或防火墙，并定期为电脑进行查毒、杀毒。<br />
                3.避免在网吧等公共场所使用网上银行，不要打开来历不明的电子邮件。<br />
                4.不要访问危险的网站，不要使用来历不明的软件。
             </div>   
             
             <div onClick="showsubmenu(19)" class="district"><s>&bull;</s><span>时刻注意保护个人隐私</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu19" class="area" style="display:none;">
               用户在平台上交流的过程中，不要向其他用户透露自己真实姓名与住址等，以防个人信息被盗取造成损失。
             </div>
             
             <div onClick="showsubmenu(20)" class="district"><s>&bull;</s><span>避免私下交易</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu20" class="area" style="display:none;">
                联富金融建议用户避免私下交易。私下交易的约束力极低，造成逾期的风险非常高，同时您的个人信息将有可能被泄漏，存在遭遇诈骗甚至受到严重犯罪侵害的隐患。
             </div>
             
             <div class="down_up"><span>&gt;</span>网站相关协议</div>
             
             <div onClick="showsubmenu(21)" class="district"><s>&bull;</s><span>电子合同的有效性</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                通过联富金融审核的借款方向投资方借贷，双方通过平台的电子借贷协议，明确双方的债务与债权关系。依据中华人民共和国合同法第十一条规定：“书面形式是指合同书、信件和数据电文（包括电报、电传、传真、电子数据交换和电子邮件）等可以有形地表现所载内容的形式”，联富金融上电子合同与传统合同具有同等的法律效力，联富金融服务仅向符合中华人民共和国有关法律法规及本公司相关规定的合格投资人和借款人提供。
             </div>
             
             <div onClick="showsubmenu(22)" class="district"><s>&bull;</s><span>如何查询我的借款协议？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                1.如果您已在网上成功的获得借款，您可以在网站上打开【我的联富金融】--【借款管理】--【我的借款】--【借款协议】<br />
                2.如果您已在网上成功的投标理财，您可以在网站上打开【我的联富金融】--【理财管理】--【我的债权】--【借款协议】</br>
                3.您也可以在帮助中查看“网站借款协议范本”
             </div>  
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>网站借款协议范本</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                <a href="#">点击查看网站借款协议范本</a>
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>联富金融预定协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none">
                <a href="#">点击查看联富金融预定协议</a>
             </div>
             
             <div onClick="showsubmenu(25)" class="district"><s>&bull;</s><span>联富金融服务协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                <a href="#">点击查看联富金融服务协议</a>
             </div>
             
             <div onClick="showsubmenu(26)" class="district"><s>&bull;</s><span>添加银行卡，为什么要填写省市和开户行字段？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                联富金融是通过第三方支付平台为用户提供付款服务，第三方支付平台将用户银行卡信息上传至银行做付款操作时，可能会进行省市和开户行字段的校验，如果为空或不正确可能会导致付款失败。<br />
                所以为了您提现的便利和安全，请您认真填写省市和开户行。
             </div> 
             
             <div onClick="showsubmenu(27)" class="district"><s>&bull;</s><span>联富金融产品说明</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu27" class="area" style="display:none">
                点击查看联富金融产品说明
             </div>
             
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>债权转让说明书</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                <a href="#">点击查看债权转让说明书</a>
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>债权转让协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                <a href="#">债权转让及受让协议范本</a>
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
