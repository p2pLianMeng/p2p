﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html" style="background:#f8bbb2;color:#fff;"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">登录注册</a>
             <a href="#">账户密码</a>
             <a href="#">充值</a>
             <a href="#">提现</a>
             <a href="#">安全认证</a>
             <a href="#">消息中心</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div class="down_up"><span>&gt;</span>账户密码</div>
             
             <div onClick="showsubmenu(11)" class="district"><s>&bull;</s><span>联富金融账户的密码都有哪些？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                <h1>登录密码</h1>
                登录联富金融、合作账号关联联富金融账号时，需要输入的密码。<br />
                <h1>提现密码</h1>
                退出优选理财计划、债权转让、提现时需要输入的密码。
             </div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>如何修改登录密码？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，找到【登录密码】信息栏，点击【修改】，重置登录密码。<a href="#">查看操作流程>></a>
             </div>   
             
             <div onClick="showsubmenu(13)" class="district"><s>&bull;</s><span>如何找回登录密码？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                打开登录页面，点击【忘记密码】；跳转至【找回密码】页面，可以选择通过绑定邮箱或者绑定手机号找回登录密码。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(14)" class="district"><s>&bull;</s><span>通过邮箱找回登录密码时，收不到邮件，怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu14" class="area" style="display:none;">
                请您在垃圾邮件中查找，如果没有，则可能是您邮箱服务器存在问题，你可以选择<a href="#">更换绑定邮箱</a>。
             </div>
             
             <div onClick="showsubmenu(15)" class="district"><s>&bull;</s><span>如何修改提现密码？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu15" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，找到【提现密码】信息栏，点击【修改】，重置提现密码。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(16)" class="district"><s>&bull;</s><span>如何通过手机号找回提现密码?</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu16" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，找到【提现密码】信息栏，点击【找回】，通过绑定手机号找回并重置提现密码。<a href="#">查看操作流程>></a>
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp"%>

</body>

</html>
