<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">  

<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>

<%
String __ROOT_PATH__THIS = WebApp.getWebRootPath(request);
String __PUBLIC____THIS = WebApp.getPublicPath(request,__ROOT_PATH__THIS);
 %>
<script type="text/javascript" src="<%=__PUBLIC____THIS %>/zclip/js/jquery.zclip.min.js"></script>
<script type="text/javascript">
$(function(){
   
	$('#copy_button').zclip({
		path: '<%=__PUBLIC____THIS %>/zclip/js/ZeroClipboard.swf',
		copy: function(){
			return $('#url').val();
		},
		afterCopy: function(){
		
			$("<span id='msg'/>").insertAfter($('#copy_button')).text('复制成功').fadeOut(2000);
		}
	});
	
	
});
</script>
<div class="activities">
  
  <div class="activities_con">
     <div class="activities_top">
        <div class="head"></div>        
        <div class="center"></div> 
        <div class="center_2">
           <input class="button_1" id="huodong2" type="button" />
        </div> 
        <div class="center_3"></div> 
        <dl>
           <dt></dt>
           <dd>2014年<span>12</span>月<span>12</span>日-2015年<span>2</span>月<span>12</span>日</dd>
        </dl>  
        <dl>
           <dt style="background-position:0 -140px"></dt>
           <dd>
              1、推荐人不限注册时间，不限投资条件，用户均可参加；<br />
              2、邀请1到5个好友理财投标成功或者贷款成功每一位送30元现金；<br />
              3、邀请6到10个好友理财投标成功或者贷款成功每一位送现金40元；<br />
              4、邀请10个以上好友理财投标成功或者贷款成功每一位送现金50元;<br />
              5、邀请成功，红包以现金方式直接到账，可以用于理财也可以直接提现；<br />
              6、理财投标包括精英散标和联富宝，但是不包括新手精英散标和新手联富宝。
           </dd>
        </dl> 
        <dl>
           <dt style="background-position:0 -210px"></dt>
           <dd>
              1、禁止用户通过任何方式作弊破坏活动的违规行为，联富金融有权冻结或者取消任何违规的红包；<br />
              2、此活动解释权归于联富金融最终所有，如您有任何疑问，请致电4006-888-923咨询
           </dd>
        </dl> 
        <div class="center_2">
           <input class="button_1" id="huodong"  type="button" />
        </div> 
        <div class="center_2 center_4"><input class="button_2" id="zhuce" type="button" /></div> 
        <div class="down"></div>
     </div>
  </div>
  
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>


<script>
	$("#zhuce").click(function(){
		window.location.href="<%=__ROOT_PATH__ %>/index/registration.html";
	});
	$("#huodong").click(function(){
		window.location.href="<%=__ROOT_PATH__ %>/index/activity/to_activity.html";
	});
	$("#huodong2").click(function(){
		window.location.href="<%=__ROOT_PATH__ %>/index/activity/to_activity.html";
	});
</script>


