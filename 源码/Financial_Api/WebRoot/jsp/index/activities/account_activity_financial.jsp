<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">  

<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>

<div class="activities_tb">
  
  <div class="activities_con">
     <div class="activities_top">
        <div class="up"><span>投标</span>返现金</div>
        <div class="con">
           <div class="left">
              <div class="left_up">活动奖励</div>
              <div class="left_down">
                 成功参与平台精英散标或者联富宝理财的客户，<s>返还投资总额的1%现金。</s><br />
                 <p>活动时间：<span><s>12</s>月<s>12</s>日至<s>2</s>月<s>12</s>日</span></p>
              </div>
           </div>
           <div class="right">
              <input class="button_1" id="toubiao" type="button" value="立即投标" />
              <a href="<%=__ROOT_PATH__ %>/index/registration.html">还没有账号？立即注册>></a>
              <img src="<%=__PUBLIC__ %>/images/activities_tb_3.png" />
           </div>
        </div>
     </div>
     <div class="activities_top2">
        <div class="up">活动规则</div>
        <div class="center">
           1、新手标不参加此次活动；<br />
           2、获赠金额每24小时结算一次并自动存入客户账户；<br />
           3、获赠金额既可以用于投资也可以用于提现。
        </div>
        <div class="down">此活动解释权归于联富金融最终所有，如您有任何疑问，请致电4006-888-923咨询</div>
     </div>
  </div>
  
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>
<script>
	$("#toubiao").click(function(){
		window.location.href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html";
	});
</script>




