<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
    String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">

<script language="JavaScript" type="text/javascript" src="script/tab.js"></script>

</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="back_password_cz">
        <div class="con">重置密码：请输入您的新密码</div>
        <div class="con_a">
           <label class="label_1"><span>*</span>新密码：</label>
           <input class="input_1" type="password" name="password" id="password">
           <p id='1' class="con_b" style="display:none"><s></s>新密码为空</p>
           <p id='11' class="con_b" style="display:none"><s></s>长度应在6~35位之间</p>
        </div>
        <div class="con_a">
           <label class="label_1"><span>*</span>请确认新密码：</label>
           <input class="input_1" type="password" name="password2" id="password2">
           <p id='2' class="con_b" style="display:none"><s></s>密码为空</p>
           <p id='21' class="con_b" style="display:none"><s></s>两次密码不一致</p>
      	   <p id='22' class="con_b" style="display:none"><s></s>密码重置失败</p>
        </div>
        
        <div class="con_c"><a id="sub" href="javascript:void(0);">提交</a></div>
     </div>
  </div>  
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>
<<script type="text/javascript">
	$("#sub").click(function(){
		var password = $("#password").val();
		var password2 = $("#password2").val();
		$.post(
			'<%=__ROOT_PATH__%>/index/find_password.html',
			{
				"password" : password,
				"password2" : password2
			},
			function(data){
				if(data == 1){
					$("#1").show();
					$("#11").hide();
				}else if(data == 2){
					$("#1").hide();
					$("#11").show();
				}else if(data == 3){
					$("#2").show();
					$("#21").hide();
					$("#22").hide();
				}else if(data == 4){
					$("#2").hide();
					$("#21").show();
					$("#22").hide();
				}else if(data == 5){
					$("#2").hide();
					$("#21").hide();
					$("#22").show();
				}else if(data == 6){
					window.location.href="<%=__ROOT_PATH__%>/index/to_cg_page.html";
				}
			}
		);
	});
</script>
