<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>


		<title>${applicationScope.title}</title>
		<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__ %>/style/jisuanji.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__ %>/style/one.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__ %>/style/prod.css" rel="stylesheet"
			type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

		<script language="JavaScript" type="text/javascript"
			src="<%=__PUBLIC__ %>/script/tab.js"></script>
		<script language="JavaScript" type="text/javascript"
			src="<%=__PUBLIC__ %>/script/pngAlaph.js"></script>
		<script language="JavaScript" type="text/javascript"
			src="<%=__PUBLIC__ %>/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
		<link type="text/css" rel="stylesheet"
			href="<%=__PUBLIC__%>/css/easydialog.css" />

		<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	</head>

	<body>
		<div class="head_top">
			<h1>
				<a href="<%=__ROOT_PATH__ %>/index/index.html"><img src="<%=__PUBLIC__ %>/images/logo.png" /></a>
			</h1>
			<div class="right">
				注册
			</div>
		</div>

		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="registration">
					<div class="registration_top">
						<div class="up">
							<span style="background:#ed5050; color:#fff;">1</span>
				              <s></s>
				              <span>2</span>
				              <s></s>
				              <span>3</span> 
						</div>
						<div class="down">
							<span style="color:#cc0000; margin-left:17px;">填写账户信息</span>
				              <span style="margin-left:135px;">手机信息验证</span>
				              <span style="margin-left:130px;">注册成功</span>
						</div>
					</div>
					<div class="registration_center">
						<span>已有账号？<a
							href="<%=__ROOT_PATH__ %>/index/to_login.html">立即登录</a> </span>
					</div>
					<div class="registration_down">
						<div class="con">
							<label class="label_1">
								<span>*</span>&nbsp;昵称：
							</label>
							<input class="input_1"  onkeyup="nickName2();"
								type="text" name="nickName" id="nickName">
							<p id="4" class="label_p" style="display: none;color:red;">
								昵称不能为空
							</p>
							<p id="41" class="label_p" style="display: none;color:red;">
								昵称长度为4-16位
							</p>
							<p id="42" class="label_p" style="display: none;color:red;">
								该昵称已经被使用
							</p>
							<p id="43" class="label_p" style="display: none;color:green;">
								此昵称将做展示，一旦注册成功不能修改
							</p>
						</div>
						<div class="con">
							<label class="label_2">
								<span>*</span>&nbsp;手机号：
							</label>
							<input class="input_1 input_2" type="text" name="phone"
								id="phone">
							<p id='1' class="label_p" style="display: none;color:red;">
								该手机号已经被使用
							</p>
							<p id='11' class="label_p" style="display: none;color:red;">
								请填写您的手机号
							</p>
							<p id='12' class="label_p" style="display: none;color:red;">
								手机号格式有误
							</p>
							<p id='13' class="label_p" style="display: none;color:green;">
								手机号填写正确
							</p>
						</div>
						<div class="con">
							<label class="label_3">
								<span>*</span>&nbsp;登录密码：
							</label>
							<input class="input_1 input_3" onkeyup="qiangdu();"
								type="password" id="password" name="password">
							<p id='password_message' class="label_p" style="display: none;color:red;">
								请填写您的密码
							</p>
							<p id='6' class="label_p" style="display: none;color:red;">
								密码长度不能小于6且不能大于35
							</p>
							<p id='61' class="label_p" style="display: none;color:green;">
								密码格式填写正确
							</p>
							<p class="label_p1">
								<span id="ruo" style="background: gray">弱</span>
								<span id="zhong" style="background: gray">中</span>
								<span id="qiang" style="background: gray">强</span>
							</p>

						</div>
						<div class="con">
							<label class="label_3">
								<span>*</span>&nbsp;重复密码：
							</label>
							<input class="input_1 input_3" type="password" id="password2"
								name="password2">
							<p id='2' class="label_p" style="display: none;color:red;">
								两次输入的密码不一致
							</p>
							<p id='21' class="label_p" style="display: none;color:red;">
								密码不能为空
							</p>
							<p id='22' class="label_p" style="display: none;color:green;">
								第二次输入的密码正确
							</p>
						</div>
						 <div class="con">
            				  <label class="label_3"><span>*</span>&nbsp;选择角色：</label>
             				  <p class="p1"><a href="javascript:void(0);" id="user_type1" style="margin-right:22px;">我要理财</a><a id="user_type2" href="javascript:void(0);">我要借款</a></p>
          				 </div>  
          				 <input type="hidden" name="id1" id ="id1" value="0"/> 
						<div class="con">
							<label class="label_3">
								<span>*</span>&nbsp;验证码：
							</label>
							<input class="input_1 input_4" type="text" id="verification_code"
								name="verification_code">
							<p class="label_p2">
								<a href="javascript:void(0);"> <img id="CheckCodeServlet" width="122"
										height="42" src="<%=__ROOT_PATH__%>/code.jpg"
										onclick="refresh();" style="border: 1px; font-color: white;" />
								</a>
							</p>
							<p class="label_p3">
								<a href="javascript:void(0);"><img
										src="<%=__PUBLIC__ %>/images/registration_pic6.png"
										onclick="refresh();" /> </a>
							</p>
							<p id='3' class="label_p" style="display: none;color:red;">
								验证码不能为空
							</p>
							<p id='31' class="label_p" style="display: none;color:red;">
								验证码错误
							</p>
						</div>
						<script>
				function refresh(){
				var CheckCodeServlet=document.getElementById("CheckCodeServlet");
				CheckCodeServlet.src="<%=__ROOT_PATH__%>/code.jpg?id="+ Math.random();
				}
			</script>
						<div class="con">
							<label class="label_3"></label>
							<input class="agree" name="agree" id="checked"
								onclick="return false" checked="checked" type="checkbox">
							<p class="label_p4">
								我已阅读并同意
								<a href="#">《联富金融网站服务协议》</a>
							</p>
						</div>
						<div class="con">
							<label class="label_3"></label>
							<p id="zhuce" class="label_p5">
								<a onclick="registration();" href="javascript:void(0);">立即注册</a>
							</p>
							<p id='7' class="label_p" style="display: none;color:red;">
								注册失败
							</p>
							<p id='71' class="label_p" style="display: none;color:red;">
								发送邮箱验证码失败
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/jsp/index/index_foot.jsp" %>
	</body>
</html>
<script type="text/javascript">
	$("#user_type1").click(function(){
		//$("#user_type1").css("","width:120px; height:42px; float:left; border:1px #ed5050 solid; text-align:center; line-height:42px;color:#ed5050;");
		$("#user_type1").css("background","url(<%=__PUBLIC__%>/images/loan_b_2.png) 100px center no-repeat");
		$("#user_type1").css("border","1px #ed5050 solid");
		$("#user_type1").css("background-color","#ed5050");
		$("#user_type1").css("color","white");
		$("#user_type2").css("background","");
		$("#user_type2").css("border","");
		$("#user_type2").css("color","#ed5050");
		$("#id1").attr("value",1);
	});
	$("#user_type2").click(function(){
		$("#user_type2").css("background","url(<%=__PUBLIC__%>/images/loan_b_2.png) 100px center no-repeat");
		$("#user_type2").css("border","1px #ed5050 solid");
		$("#user_type2").css("background-color","#ed5050");
		$("#user_type2").css("color","white");
		$("#user_type1").css("background","");
		$("#user_type1").css("border","");
		$("#user_type1").css("color","#ed5050");
		$("#id1").attr("value",2);
	});

	$("#password2").keyup(function(){
		var password2 = $("#password2").val();
		var password = $("#password").val();
		if(password2==null||password2==""){
			$("#2").hide();
			$("#21").show();
		}else{
			$("#21").hide();
		}
		if(password!=password2){
			$("#2").show();
			$("#21").hide();
			$("#22").hide();
		}else{
			$("#2").hide();
			$("#22").show();
		}
	});
	$("#password").keyup(function(){
		var password = $("#password").val();
		if(password==null||email==""){
			$("#password_message").show();
			$("#6").hide();
			$("#61").hide();
		}else{
			$("#password_message").hide();
			$("#61").hide();
		}
		if(password.length<6||password.length>35){
			$("#password_message").hide();
			$("#6").show();
			$("#61").hide();
		}else{
			$("#6").hide();
			$("#61").show();
		}
	});
	
	
	 $("#phone").keyup(function(){
	 
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			$("#11").show();
			$("#1").hide();
			$("#12").hide();
			return;
		}else{
			var reg_phone=/^[1][3458][0-9]{9}$/;
		if(!reg_phone.test(phone)){
			$("#11").hide();
			$("#1").hide();
			$("#12").show();
			$("#13").hide();
			return;
		}else{
			$("#12").hide();
		}
		}
		
		$.post(
			'<%=__ROOT_PATH__%>/index/phone_onblur_validate.html',
			{
				"phone" : phone
			},
			function(data){
				if(data == 1){
					$("#11").show();
					$("#1").hide();
					$("#12").hide();
					$("#13").hide();
				}else if(data == 2){
					$("#11").hide();
					$("#1").show();
					$("#12").hide();
					$("#13").hide();
				}else if(data == 3){
					$("#11").hide();
					$("#1").hide();
					$("#12").hide();
					$("#13").show();
				}else if(data == 4){
					$("#11").hide();
					$("#1").hide();
					$("#12").show();
					$("#13").hide();
				}
			},'html',"application/x-www-form-urlencoded; charset=utf-8");
	});
	
	
	
	
	function registration(){
		var user_type = $("#id1").val();
		if(user_type!=1&&user_type!=2){
			showMessage(["提示","请填写用户类型"]);
			return;
		}
		//$("#zhuce a").html("请稍等...");
		//$("#zhuce a").attr("onclick","return false");
		var nickName = $("#nickName").val();
		if(nickName.length<2||nickName.length>10){
			$("#41").show();
			$("#4").hide();
			return;
		}
		if(nickName==""||nickName==null){
			$("#41").hide();
			$("#4").show();
			return;
		}
		var check = $("#checked").attr("checked");
		var phone = $("#phone").val();
		if(!(/^1[3|4|5|8|]\d{9}$/).test(phone)){
			$("#11").hide();
			$("#1").hide();
			$("#12").show();
			return;
		}
		var password = $("#password").val();
		if(password.length<6||password.length>35){
			$("#password_message").hide();
			$("#6").show();
		}
		if(password==""||password==null){
			$("#password_message").show();
			$("#6").hide();
		}
		var password2 = $("#password2").val();
		var verification_code = $("#verification_code").val();
		
				
		$.ajax({
				 type:"post",
				 cache:false,
				 url:'<%=__ROOT_PATH__%>/index/user_register.html',
				 data:{"user_type":user_type,"nickName" : nickName,"phone" : phone,"check":check,"password" : password,"password2" : password2,"verification_code" : verification_code},
			   success: function(data){
				if(data == 1){
					//showMssage(["提示","<span class='hongse'>昵称不能为空!</span>"]);
					$("#4").show();
				}else if(data == 10){
					$("#42").show();
				}else if(data == 2){
					//showMssage(["提示","<span class='hongse'>邮箱不能为空!</span>"]);
					$("#11").show();
					("#12").hide();
					$("#1").hide();
					
				}else if(data == 3){
					//showMssage(["提示","<span class='hongse'>密码不能为空!</span>"]);
					$("#21").show();
				}else if(data == 4){
					//showMssage(["提示","<span class='hongse'>第二次输入密码不能为空!</span>"]);
					$("#2").show();
				}else if(data == 5){
					//showMssage(["提示","<span class='hongse'>验证码不能为空!</span>"]);
					$("#3").show();
					$("#31").hide();
				}else if(data ==6){
					//showMssage(["提示","<span class='hongse'>验证码错误!</span>"]);
					$("#31").show();
					$("#3").hide();
				}else if(data == 7){
					//showMssage(["提示","<span class='hongse'>该邮箱已经被注册!</span>"]);
					$("#1").show();
					$("#12").hide();
					$("#11").hide();
				}else if(data == 8){
					//showMssage(["提示","<span class='hongse'>邮箱格式有误!</span>"]);
					$("#12").show();
					$("#1").hide();
					$("#11").hide();
				}else if(data == 9){
					//showMssage(["提示","<span class='hongse'>邮箱格式有误!</span>"]);
					$("#71").show();
					$("#7").hide();
					
				}else if(data == 100){
					//showMssage(["提示","<span class='hongse'>该邮箱已经被注册!</span>"]);
					$("#7").show();
					$("#71").hide();
				}else if(data == 0){
					window.location.href="<%= __ROOT_PATH__%>/index/registration2.html";
				}else if(data == 10){
					showMessage(["提示","请填写用户类型"]);
				}
			}
		},'html',"application/x-www-form-urlencoded; charset=utf-8");
	}
	
	/*
	* method:showMssage
    * param:message 
	* explain:显示对话信息
	*/
	function showMssage(message)
	{
		var btnFn = function(){
			  easyDialog.close();
			  };
	   var btnFs = function(){
		   window.location.reload();
	   };
	   /**
	   var btnTz = function(){
		   
		   	easyDialog.close();
		  
		   	window.location.href(message[3]);
	   }**/
		if(message[2]!=null){
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFs,
					 noFn:false
					}
			});	
			//实名认证成功以后 跳向安全设置页面
		}else{
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFn,
					 noFn:false
					}
			});	
		}
	}
	function nickName2(){
	
	if($("#nickName").val()!= null && ""!=$("#nickName").val()){
			$("#41").hide();
			$("#4").hide();
			$("#42").hide();
			$("#43").hide();
		}else{
			$("#4").show();
			$("#41").hide();
			$("#42").hide();
			$("#43").hide();
			return;
		}
						//<p id="4" class="label_p" style="display: none;color:red;">
						//		昵称不能为空
						//	</p>
						//	<p id="41" class="label_p" style="display: none;color:red;">
						//		昵称长度为4-16位
						//	</p>
						//	<p id="42" class="label_p" style="display: none;color:red;">
						//		昵称不能为空
						//	</p>
		$.post(
			'<%= __ROOT_PATH__%>/index/nickname_onblur_validate.html',
			{
				"nickname":$("#nickName").val()
			},
			function(data){
				if(data == 1){
					$("#4").show();
					$("#41").hide();
					$("#42").hide();
					$("#43").hide();
				}else if(data == 2){
					$("#4").hide();
					$("#41").hide();
					$("#42").show();
					$("#43").hide();
				}else if(data == 3){
					$("#4").hide();
					$("#41").hide();
					$("#42").hide();
					$("#43").show();
				}else if(data == 4){
					$("#4").hide();
					$("#41").show();
					$("#42").hide();
					$("#43").hide();
				}
			}
		);
		
	}
	
	　
	function qiangdu() {
　　if ($("#password").val() != "") {
　　　　var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
 
　　　　var mediumRegex = new RegExp ("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
 
　　　　var enoughRegex = new RegExp("(?=.{6,}).*", "g");
　　　　　if (strongRegex.test($("#password").val())) {
　　　　　　//强,密码为八位及以上并且字母数字特殊字符三项都包括
			$("#ruo").css("background", "gray");
			$("#zhong").css("background", "gray");
			$("#qiang").css("background", "green");
			
　　　　}
　　　　else if (mediumRegex.test($("#password").val())) {
　　　　　　//中等,密码为七位及以上并且字母、数字、特殊字符三项中有两项，强度是中等
		   $("#ruo").css("background", "gray");
　　　　　　$("#zhong").css("background", "orange");
			$("#qiang").css("background", "gray");
　　　　}
　　　　else {
　　　　　　//弱,如果密码为6为及以下，就算字母、数字、特殊字符三项都包括，强度也是弱的
　　　　　　$("#ruo").css("background", "red");
			$("#zhong").css("background", "gray");
　　　　　　 $("#qiang").css("background", "gray");
　　　　}
                 
　　}
 
}
function showMessage(message){
				var btnFn = function(){
					  easyDialog.close();
					  };
			   var btnFs = function(){
				   window.location.reload();
			   };
				if(message[2]!=null){
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFs,
							 noFn:false
							}
					});	
				}else{
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFn,
							 noFn:false
							}
					});	
				}
			}

</script>
