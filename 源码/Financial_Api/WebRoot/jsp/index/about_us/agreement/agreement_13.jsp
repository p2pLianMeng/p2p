<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>

</head>
<body>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__%>/images/logo.png" /></a></h1>
 <div class="agreement_head"><span>联富金融出借人服务协议（2014年版）</span><a href="#"><img src="<%=__PUBLIC__%>/images/agreement_1.png" />点击下载</a></div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="agreement_con agreement_con2">
      <p class="p_1">联富金融出借人服务协议</p>  
      <p class="p_3">本服务协议双方为联富金融出借人用户（以下简称用户或出借人用户）和联富金融平台，本服务协议具有合同效力。
用户注册成为本网站用户前应充分阅读、理解本协议的全部内容。用户点击下方的“确认”按钮并按照本网站规定的程序进入下一步，表示同意并签署了本服务协议。</p>      
      <p class="p_2 p_4">一、用户限制</p>  
      <p class="p_3">用户自愿通过本网站出借借款，成为出借人。<br />
         用户的资金来源必须合法。用户对资金有完全的、独立的处分权。该资金不存在第三人对其归属、合法性问题发生争议的情况。<br />
         用户保证不从事洗钱、信用卡套现等违反法律规定的行为。</p> 
      <p class="p_2 p_4">二、用户提供资料及隐私权保护</p>  
      <p class="p_3">（一）、用户在注册时需如实提供本人的个人信息及信用资料。包括但不限于身份证明、联系电话、联系地址、EMAIL等。并且，在上述资料发生变化时，用户应立即登陆网站进行变更。<br />
         协议履行过程中，平台通过上述指定联系方式与用户联系，联系内容包括但不限于，借款内容的确认、《借款人列表》、《出借人列表》、《借款基本信息表》等文件寄发等。<br />
         本协议项下的文件、通知等，采用数据电文形式订立合同、履行合同项下的通知义务，用户指定特定系统、EMAIL地址等方式接收数据电文的，该         数据电文进入该特定系统、EMAIL等的时间，视为到达时间。<br />
         为了保证交易安全，平台与用户通话，平台将进行录音，并作为具有证明效力的委托依据。<br />
         （二）、出借人用户在此同意本网站将其信息在本网站进行适当披露，并同意本网站在交易成交后，将出借人的出借金额、比例等提供给借款人用户。同时，为了保护出借人用户的隐私权，出借人用户的真实姓名及地址不会在列表上显示，本平台上所有协议出现的仅为出借人用户的注册名字。<br />
         （三）、为了更好的保护出借人的利益，本平台将在借款匹配成功后，向出借人提供《借款人列表》，该列表包括借款人注册姓名（不是真实姓名）、身份证号。出借人确保仅用于出借参考，不向任何第三人透露。如果出借人擅自、不恰当地向他人透露借款人的信用信息或平台的商业秘密，由此对借款人或平台造成的损失，由出借人承担全部责任<br />
         （四）、用户不得使用虚假身份资料或他人资料进行注册的。</p> 
      <p class="p_2 p_4">四、费用</p>  
      <p class="p_3">本平台为借款人和出借人双方提供撮合、信用咨询、评估、信用管理、合同签订、促成双方交易成功居间服务、还款提醒、账户管理、提前还款、还款特殊情况下还款处理诉讼协助等系列居间信用相关服务，本网站有权收取一定的费用。<br />
         （一）、服务费用<br />
         在用户注册成为网站用户后，竞标成功前，不需要向本平台支付服务费用。<br />
         用户借款竞标、匹配成功后，本网站将按照出借人用户的借款应收利息总额的10%收取服务费用：<br />
         （二）、扣划费用<br />
         银行账户扣划费用等由客户负担，但由本平台代收，然后委托银行或本平台合作机构进行扣划，并由平台与扣划机构统一结算。<br />
         （三）、上述费用将从用户的指定账户或者借款款项中直接扣除。</p> 
      <p class="p_2 p_4">五、押金、违约金</p>  
      <p class="p_3">（一）、押金<br />
         用户成功申请借款后，本平台将扣除借款总额的5%作为押金，并进行冻结。<br />
         该押金作为借款人用户履行还款义务的保证，在该借款项下所有还款义务（包括但不限于本金、利息、罚金、违约金等）全部得到履行后，本平台将押金全额无息返还给借款人。<br />
         如借款人用户出现逾期还款达到90天以上或出现其他严重违约行为，本平台将利用该押金进行支付，不足部分由借款人用户补齐。<br />
         （二）、逾期违约金<br />
         逾期违约金：借款人逾期偿还借款时，按照当月应还本金的2%乘以逾期天数来计算逾期违约金，每月单独计算；平台将支付该违约金数额的50%给出借人。</p> 
      <p class="p_2 p_4">六、风险金及出借人本金保证</p>  
      <p class="p_3">（一）、平台从收取的服务费用中提取一定金额作为风险金专用账户，并审慎管理。<br />
         （二）、发生逾期时，平台会向借款人采取一定的催收措施，催收无果的，平台会首先用冻结的借款人押金进行支付，不足支付的，由平台使用风险金专用账户的钱进行补偿，平台有权决定风险金补偿支付比例和金额。</p>
      <p class="p_2 p_4">七、提前还款违约金</p>  
      <p class="p_3">用户有提前还款需求，应通知平台，由平台协助办理提前还款手续。<br />
         提前还款时，借款人需额外支付当期利息的两倍作为违约金，出借人可获得其中的50%，另外50%归平台所有。</p>
      <p class="p_2 p_4">八、指定银行账户</p>  
      <p class="p_3">出借人用户指定银行账户作为放款、资金回收等本协议项下资金往来的专用账户。出借人授权平台通过其合作机构从该账户中划扣（或划回）出借本金、利息、提前还款违约金、罚息违约金、其他应收款等。
         所有款项均支付到出借人在平台的虚拟账户，如出借人需要资金回收到本人银行帐户的，需要通过平台“申请取现”。</p>
    </div>
  </div>
  
</div>



<div class="bottom_2">   
   <div class="content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="64%" class="td_1"><img src="<%=__PUBLIC__%>/images/footer_dh_2.png" />4006-888-923</td>
            <td width="36%" class="td_2">
               粤ICP备14022597号-1 可信网站 CopyRight © 2014<br />
               广州天的富网络科技有限公司版权所有     
            </td>
         </tr>
      </table>
   </div>
</div>

</body>
</html>

