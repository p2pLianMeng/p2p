<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>

</head>
<body>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__%>/images/logo.png" /></a></h1>
  <div class="agreement_head"><span>联富宝服务计划授权委托书</span><a href="#"><img src="<%=__PUBLIC__%>/images/agreement_1.png" />点击下载</a></div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="agreement_con">
      <p class="p_1">联富宝服务计划授权委托书</p>
      <h3>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <td colspan="4">委托人：____________</td>
             </tr>
             <tr>
                <td colspan="4">证件类型：__________</td>
             </tr>
             <tr>
                <td colspan="4">证件号码：__________</td>
             </tr>
             <tr>
                <td class="td_1" colspan="4">&nbsp;</td>
             </tr>
             <tr>
                <td colspan="2">受托人：广州天的富网络科技有限公司</td>
                <td width="20%">（以下简称:天的富公司）</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td colspan="4">住所地：广州市白云区石井大道银马广场C栋610室</td>
             </tr>
             <tr>
                <td class="td_1" colspan="4">&nbsp;</td>
             </tr>
             <tr>
                <td colspan="4">鉴于：委托人参加“联富金融服务计划”（天的富公司经营的P2P借贷交易居间服务的互联网平台（即“联富金融”）推出的一项服务计划），为了让受托人能够更好的为委托人提供服务，现委托人特此向受托人做出如下授权：</td>
             </tr>
             <tr>
                <td class="td_1" colspan="4">&nbsp;</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">1、</td>
                <td colspan="3">授权范围：</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（1）代委托人在联富金融上点击确认出借资金、债权转让的相关协议（如《借款及服务协议》、《债权转让协议》等）；</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（2）注册成功后，锁定期内代委托人进行资金的出借、回款管理（催收）、回款再出借；（前述“出借”、“回款再出借”之具体含义详见“《联富金融出借咨询与服务协议》”）；</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（3）退出期间内委托人向天的富公司申请（以债权转让方式）退出计划；或锁定期内委托人向天的富公司申请（以债权转让方式）紧急退出时，代委托人进行债权转让。</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（上述“锁定期”、“退出期间”的具体含义以委托人与受托人间签署的《联富金融出借咨询与服务协议》中的相关约定为准。）</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">2、</td>
                <td colspan="3">授权出借信息：</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（1）授权出借的账户</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">联富金融平台账号：_______________</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（2）授权出借的金额：</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">人民币（大写）________________元（￥______）</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">3、</td>
                <td colspan="3">授权期限：</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">本授权期限自委托人与受托人间签署《联富金融出借咨询与服务协议》之日起至完全退出计划（或紧急退出）之日止。</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">4、</td>
                <td colspan="3">本协议经出借人在联富金融网络平台以线上点击确认的方式签署。</td>
             </tr>
             <tr>
                <td class="td_3" colspan="4">&nbsp;</td>
             </tr>
             <tr>
                <td class="td_4" colspan="4">委 托 人：__________________</td>
             </tr>
             <tr>
                <td class="td_4" colspan="4">联富金融ID：_________________</td>
             </tr>
             <tr>
                <td class="td_4" colspan="4">身份证号码：________________</td>
             </tr>
             <tr>
                <td class="td_4" colspan="4">签署日期：____年____月____日</td>
             </tr>
          </table> 
      </h3>       
    </div>
  </div>
  
</div>



<div class="bottom_2">   
   <div class="content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="64%" class="td_1"><img src="<%=__PUBLIC__%>/images/footer_dh_2.png" />4006-888-923</td>
            <td width="36%" class="td_2">
               粤ICP备14022597号-1 可信网站 CopyRight © 2014<br />
               广州天的富网络科技有限公司版权所有     
            </td>
         </tr>
      </table>
   </div>
</div>

</body>
</html>

