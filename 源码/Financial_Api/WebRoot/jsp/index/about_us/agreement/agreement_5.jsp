<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>

</head>
<body>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__%>/images/logo.png" /></a></h1>
  <div class="agreement_head"><span>联富金融债权转让协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/agreement_1.png" />点击下载</a></div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="agreement_con agreement_con2">
      <p class="p_1">联富金融债权转让协议</p>
      <h3>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <td colspan="4">债权出让人（即“原出借人”，简称“出让人”，联富金融昵称：______，联富金融ID：____）</td>
             </tr>
             <tr>
                <td colspan="4">债权受让人（即“新出借人”，简称“受让人”，联富金融昵称：______，联富金融ID：____）</td>
             </tr>
             <tr>
                <td colspan="4">（出让人、受让人单独称“各方”，合称“双方”。）</td>
             </tr>
             <tr>
                <td colspan="4">出让人和受让人双方已认真阅读本协议条款，并以网络在线点击确认的方式接受本协议全部条款，自愿按照本协议及网站其他协议（如：《借款与服务协议》等）行使权力并履行相关义务。</td>
             </tr>  
             <tr>
                <td class="td_1" colspan="4">&nbsp;</td>
             </tr> 
          </table> 
      </h3>  
      <p class="p_3">鉴于：</p>  
      <dl>
         <dt>1.</dt>
         <dd>出让人有意将其对债务人享有的债权（借款交易编号：______，第____期至第____期）转让予受让人；</dd>
      </dl>  
      <dl>
         <dt>2.</dt>
         <dd>截止本协议签署之日，出让人对债务人享有的债权为人民币______元（合称债权）；</dd>
      </dl> 
      <dl>
         <dt>3.</dt>
         <dd>出让人同意将上述债权转让予受让人，同时受让人同意受让该笔债权。</dd>
      </dl>
      <p class="p_3"><br />为明确各方权利、义务，各方本着平等互利的原则，经协商一致，在此达成如下协议条款，以资信守：<br />&nbsp;</p>
      <dl>
         <dt>1.</dt>
         <dd>出让人向受让人转让债权的转让对价为人民币______元（下称债权转让对价）；</dd>
      </dl>
      <dl>
         <dt>2.</dt>
         <dd>受让人通过联富金融网站将债权转让对价支付给出让人；</dd>
      </dl>
      <dl>
         <dt>3.</dt>
         <dd>自本协议签署之日起，出让人将鉴于条款第1项下的债权转让予受让人（简称债权转让），该债权的交割日为本协议签署且受让人向出让人支付债权转让对价之日；</dd>
      </dl>
      <dl>
         <dt>4.</dt>
         <dd>本协议的订立、有效性。解释、签署以及与本协议有关的一切争议的解决均受中华人民共和国法律的管辖，并据其进行解释；</dd>
      </dl>
      <dl>
         <dt>5.</dt>
         <dd>如果出让人、受让人双方在本协议履行过程中发生任何争议，应友好协商解决；如协商不成，双方一致同意将争议提交北京仲裁委员会按有效的仲裁规则解决。</dd>
      </dl>
    </div>
  </div>
  
</div>



<div class="bottom_2">   
   <div class="content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="64%" class="td_1"><img src="<%=__PUBLIC__%>/images/footer_dh_2.png" />4006-888-923</td>
            <td width="36%" class="td_2">
               粤ICP备14022597号-1 可信网站 CopyRight © 2014<br />
               广州天的富网络科技有限公司版权所有     
            </td>
         </tr>
      </table>
   </div>
</div>

</body>
</html>

