<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>

</head>
<body>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__%>/images/logo.png" /></a></h1>
 <div class="agreement_head"><span>联富金融自动投标服务协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/agreement_1.png" />点击下载</a></div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="agreement_con agreement_con2">
      <p class="p_1">联富金融自动投标服务协议</p>
      <p class="p_2">第一章 基础说明</p>
      <dl>
         <dt>第1条：</dt>
         <dd>广州天的富网络科技有限公司（简称“天的富公司”）依据本协议的规定在联富金融网站（天的富公司运营的提供专业个人对个人借贷交易居间服务的互联网平台；网址：www.yirendai.com，简称“联富金融”）上为联富金融出借人（简称“出借人”）提供自动投标服务。本协议在出借人和天的富公司间具有法律效力。</dd>
      </dl> 
      <dl>
         <dt>第2条：</dt>
         <dd>联富金融建议您认真阅读本协议，并在充分理解各条款内容后再选择是否接受本协议。除非您接受本协议所有条款，否则无权使用联富金融于本协议项下所提供的自动投标服务。您一经选定自动投标服务即视为完全接受本协议全部条款。</dd>
      </dl> 
      <p class="p_2">第二章 自动投标服务说明</p> 
      <dl>
         <dt>第3条：</dt>
         <dd>本协议中的“投标”即为“出借”之意。</dd>
      </dl> 
      <dl>
         <dt>第4条：</dt>
         <dd>自动投标服务是天的富公司为了方便出借人而提供的一个智能投标委托代理工具。</dd>
      </dl> 
      <dl>
         <dt>第5条：</dt>
         <dd>自动投标的对象为联富金融平台上的精英标。出借人启用自动投标服务，即视为同意并接受联富金融《借款及服务协议》（精英标）及其他相关限定。</dd>
      </dl> 
      <dl>
         <dt>第6条：</dt>
         <dd>启用自动投标服务，出借人可以设定投标方案。天的富公司会根据出借人设定的方案筛选合适的标的进行自动出借。</dd>
      </dl>  
      <dl>
         <dt>第7条：</dt>
         <dd>出借人在此不可撤销的授权天的富公司、天的富公司已同事接受其授权代出借人通过联富金融的自动投标服务将出借人的账户余额进行投标。天的富公司的投标行为等同于出借人的自行投标，交易中产生的任何损失由出借人自行承担。</dd>
      </dl> 
      <dl>
         <dt>第8条：</dt>
         <dd>出借人应知悉自动投标服务无法保证不错过任何满足其定制条件的标的。由此产生的损失，天的富公司不承担任何责任。</dd>
      </dl>
      <dl>
         <dt>第9条：</dt>
         <dd>自动投标发生后，联富金融会向出借人的绑定邮箱发送通知。邮件从联富金融发出时即视为已送达，出借人不会以任何理由否认该笔出借。</dd>
      </dl>
      <p class="p_2">第三章 其他说明</p> 
      <dl>
         <dt>第10条：</dt>
         <dd>天的富公司拥有对本协议的修改权及最终解释权。</dd>
      </dl>
    </div>
  </div>
  
</div>



<div class="bottom_2">   
   <div class="content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="64%" class="td_1"><img src="<%=__PUBLIC__%>/images/footer_dh_2.png" />4006-888-923</td>
            <td width="36%" class="td_2">
               粤ICP备14022597号-1 可信网站 CopyRight © 2014<br />
               广州天的富网络科技有限公司版权所有     
            </td>
         </tr>
      </table>
   </div>
</div>

</body>
</html>

