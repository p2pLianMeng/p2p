<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	
	 <title>${applicationScope.title}</title>
		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>

	</head>
	<body>
		<%@ include file="/jsp/index/index_top.jsp"%>
			<span id="ny_pic"></span>
			<div class="about">
				<div class="about_con">
					<div class="about_left">
					<%-- 
						<ul>
							<li>
								<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=intro" >公司简介</a>
							</li>
							<li>
								<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=team" >管理团队</a>
							</li>
							
							<li>
								<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=consult" >最新资讯</a>
							</li>
							<li>
								<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=notice" class="hover">网站公告</a>
							</li>
							<li>
								<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=agreement">网站协议</a>
							</li>
						
							<li>
								<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=join_us">加入我们</a>
							</li>
							<li>
								<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=contact" >联系我们</a>
							</li>
						</ul>
						--%>
						${list_str}
					</div>
					<div class="about_right">
				      <h2>网站公告</h2>
				      <div class="news">
				        <ul>
				        	<c:if test="${not empty record_list}">
				        		<c:forEach items="${record_list}" var="record">
				           			<li>
				           				<span>${record.r.add_time_str }</span>
				           				<p></p>
				           				<a href="<%=__ROOT_PATH__%>/index/about/site_notice_detail.html?id=${record.r.id}">${record.r.content}</a>
				           			</li>
				           		</c:forEach>
				          	</c:if>
				        </ul>
				       		${pageDiv}
				      </div>
	    			</div>
				</div>
			</div>
			<%@ include file="/jsp/index/index_foot.jsp" %>
	</body>
</html>
