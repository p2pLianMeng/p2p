<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

	 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">   
		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>

	</head>

	<body>

		
<%@ include file="/jsp/index/index_top.jsp"%>
		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="about_left">
				<%-- 
					<ul>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=intro" >公司简介</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=team"  class="hover">管理团队</a>
						</li>
						
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=consult" >最新资讯</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=notice">网站公告</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=agreement">网站协议</a>
						</li>
					
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=join_us">加入我们</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=contact" >联系我们</a>
						</li>
					</ul>
				--%>
					${list_str}
				</div>
				<div class="about_right">
					<h2>
						公司简介
					</h2>
					<h3>
						联富金融(lfoll.com)，系人人友信集团旗下公司及独立品牌。
						<br />
						自2010年5月成立至今，联富金融的服务已覆盖了全国30余个省的2000多个地区，服务了几十万名客户，成功帮助他们通过信用申请获得融资借款，或通过自主出借获得稳定收益。
						<br />
						作为中国最早的一批基于互联网的P2P信用借贷服务平台，联富金融以其诚信、透明、公平、高效、创新的特征赢得了良好的用户口碑。现在，联富金融已成为行业内最具影响力的品牌之一。
						<br />
						<br />
						使命
						<br />
						实现个人的信用自主和金融自由。
						<br />
						<br />
						愿景
						<br />
						打造一个人人参与、人人自由、人人平等的互联网个人金融服务平台。
						<br />
						<br />
						核心价值
						<br />
						释放用户借款和理财的自主权利
						<br />
						借款人通过个人信用申请借款，获得资金；理财人通过公开信息自主选择进行投资，获得收益。
						<br />
						建立互联网时代的个人金融风险定价体系
						<br />
						通过有效的个人信用模型和大数据挖掘，逐步建立并完善符合时代特征的个人金融风险定价体系。
						<br />
						实现更低借款成本和更稳健投资收益
						<br />
						受益于精准的个人金融风险定价体系和安全分散有保障的投资机制，借款人和理财人的权益均可得到充分的 保障和满足。
					</h3>
				</div>
			</div>

		</div>



		<%@ include file="/jsp/index/index_foot.jsp" %>


	</body>
</html>
