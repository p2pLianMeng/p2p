  var path = "http://"+window.location.host;
  var fix_bid_buy_path = path + "/user/financial/lfollinvest/buy_lfoll_invest.html";//联富宝购买
  var fix_bid_order_ajax_path = path + "/user/financial/lfollinvest_user_info/get_user_fix_bid_order_list.html";//Ajax异步 联富宝预定列表
  var login_url = path+"/index/to_login.html";//
  var validate_is_login = path + "/user/financial/lfollinvest/validate_user_is_logined.html";//

  /**
   * 购买联富宝输入框，输入内容的校验
   * @return
   */
  function  calculate_deposit(){
	  var can_used_cny = Number($("#can_used").html());//可用的金额
	  var bid_money =  Number($("#bid_money").val());//投资金额
	  var bid_money_var = $("#bid_money").val();
	  var least_money = $("#least_money").val();//最少投资金额
	  var userreg = /^[1-9]\d*$/;//判断正整数
	  	/**
	  	 * 判断是否有空格
	  	 */
	  	var arr_1 = bid_money_var.split(" ");
  		if(arr_1.length > 1){
  			  $("#bid_money").val("");
			  $("#should_pay").html("0.00");
			  prompt("show_message","请输入正确的金额");
			  return;
  		}
  		var arr_2 = bid_money_var.split(".");
  		if(arr_2.length > 1){
  			  $("#bid_money").val("");
	  		  $("#should_pay").html("0.00");
			  prompt("show_message","请输入正确的金额");
			  return;
  		}
  		/**
  		 * 判断是否为正整数
  		 */
		  if(userreg.test(bid_money)){
			  //判断可用金额是否足够
			  if(can_used_cny < bid_money){
				  $("#bid_money").val("");
				  $("#should_pay").html("0.00");
				  prompt("show_message","可用金额不足");
				  return;
			  }else{
				  $("#should_pay").html(bid_money);
				  return;
			  }
		   }else{
		     $("#bid_money").val("");
		     $("#should_pay").html("0.00");
		     return;
		   }   
		
		

  	}

  	/**
  		提交加入联富宝的订单
  	**/
  	
  	function join_lfoll_bid(verificte_code){
  	  	var remaining = Number($("#remaining").html());
  	  	var can_used_cny = Number($("#can_used").html());
  	  	//least_money
  	  	var least_money = $("#least_money").val();
  		//ID
  	  	var order_id = $("#order_id").val();
  	  	//should_pay
  	  	var should_pay_str = $("#should_pay").html();
  	  	var should_pay = Number(should_pay_str);
  	  	var bid_money = Number($("#bid_money").val());
  	  	if(bid_money == 0 || bid_money == null){
  	  		prompt("error_msg","请输入有效的金额");
	  		return;
  	  	}
  	  	if(bid_money < least_money){
  	  		prompt("error_msg","输入金额小于"+least_money);
  			return;
  	  	}
  	  	if(bid_money % least_money != 0){
  	  		prompt("error_msg","当前金额必须为"+least_money+"的整数倍!");
  	  		return;
  	  	}
  	  	if(verificte_code == null || verificte_code == ""){
  	  		prompt("error_msg","验证码不能为空!");
	  		return;
  	  	}
  	  	var i = true;
  	  	if(i){
  	  	  	i = false;
  	  	  	$.ajax({
	  	  	  	  	url :fix_bid_buy_path,
	  	  	  		async :false,//同步
	  	  	  	  	type :"post",
	  	  	  		data :{"order_id":order_id,"should_pay":should_pay,"bid_money":bid_money,"least_money":least_money,"verificte_code":verificte_code},
	  	  	  	  	success:function(data){
	  	  	  			if(data == "非理财身份"){
	  	  	  				prompt("error_msg","非理财账户，请先注册理财账户!");
	  	  	  			}else if(data == "NoLogined"){
							window.location.href = login_url;
	  	  	  	  	  	}else if(data == 1){
							prompt("error_msg","非法请求!");
						}else if(data == 2){
							prompt("error_msg","应付款金额不能为0!");
						}else if(data == 3){
							prompt("error_msg","投资金额不能为0!");
						}else if(data == 4){
							prompt("error_msg","投资金额必须为"+least_money+"的整数倍!");
						}else if(data == 5){
							prompt("error_msg","验证码为空!");
						}else if(data == 6){
							prompt("error_msg","验证码错误");
							refresh_order_pay();
						}else if(data == 7){
							prompt("error_msg","可用资金不足!");
						}else if(data == 8){
							prompt("error_msg","请求超时");
						}else if(data == 9){
							prompt("error_msg","没有可投资的联富宝!");
						}else if(data == 10){
							prompt("error_msg","联富宝类型出错!");
						}else if(data == 11){
							prompt("error_msg","购买失败!");
						}else if(data == 12){
	  						close_order_pay();
							var can_used_new = can_used_cny - bid_money;
							$("#can_used").html(can_used_new);
							var remaining_new = remaining - bid_money;
							$("#remaining").html(remaining_new);
							prompt_success("show_message","付款成功","green");
							
						}
						i = true;
						return;
	  	  	  		}
  	  	  	  	});
  	  	}
  	  	
		
  	}
  	
  	/**
  	 * 
  	 * 查询用户预定中的联富宝（查询所有的用户的预定的）
  	 * @return
  	 */
  	function find_fix_bid_order_all_list(){
  			//alert("查询用户预定中的联富宝");
	  		return;
	  		$.ajax({
	  			url:"",
	  			async:true,
	  			type:"post",
	  			success:function (data){
		  			var order_table = "#order_table";
	  				var order_table_first_tr = "#order_table_first_tr";
	  				$(order_table + " tr:not(:first)").empty(); 
	  				
		  			if(data!=null){
		  				if(data == "NoLogined"){
		  					window.location.href = login_url;
		  	  	  	  	}
		  				var arrays = eval("("+data+")");
		  				var str = "";
		  				for ( var i in arrays) {
		  					
		  					var id = arrays[i].object_1;
		  					var bid_name = arrays[i].object_2;
		  					var bid_moeny = arrays[i].object_3;
		  					var alread_pay_bond = arrays[i].object_4;
		  					var wait_pay_amount = arrays[i].object_5;
		  					var pay_deadline = arrays[i].object_6;
		  					var is_pay = arrays[i].object_7;
		  					var is_pay_str ="";
		  					var nick_name = arrays[i].object_8;
		  					var annualized_rate = arrays[i].object_9;
		  					var closed_period = arrays[i].object_10;
		  					var can_used_cny = arrays[i].object_11;
		  					//alert(nick_name);
		  					if(is_pay == 0){
		  						is_pay_str = "<td><a href=\"###\" onclick=\"dump_order_pay('"+bid_name+"','" + id + "','"+nick_name+"','"+annualized_rate+"','"+closed_period+"','"+wait_pay_amount+"','"+can_used_cny+"')\" style=\"color:green\">未支付</></td> ";
		  					}
		  					if(is_pay == 1){
		  						is_pay_str = "<td>已支付</td> ";
		  					}
		  						
		  					
		  					var str = str + "<tr class=\"tr_2\">" +
			  								"<td>"+bid_name+"</td>" +
			  								"<td>"+bid_moeny+"</td>" +
			  								"<td>"+alread_pay_bond+"</td> " +
					  						"<td>"+wait_pay_amount+"</td>" +
					  						"<td>"+pay_deadline+"</td>" 
					  							+ is_pay_str +
					  						"</tr>";
						}
		  				
		  				$(order_table_first_tr).after(str);
		  				
		  			}else{
		  				var str = "<tr class=\"tr_2\"> <td colspan=\"5\" class=\"td_2\">暂无数据</td>";
		  				$(order_table_first_tr).after(str);
		  			}
	  			}
	  			
	  		});
  	}
  	
  	/**
  	 * 查询用户 加入联富宝记录
  	 * @return
  	 */
  	function find_fix_bid_user_hold_all_list(){
  		
  		
  	}
  	
  	/**
  	 * 弹出 购买 用户支付购买的联富宝 的对话框
  	 * @param id
  	 * @param nick_name
  	 * @param annualized_rate
  	 * @param closed_period
  	 * @return
  	 */
  	function dump_fix_bid_pay(){
  		var bid_money_var = $("#bid_money").val();
  		var bid_money  = bid_money_var.replace(/[ ]/g,"");//去掉空格
  		var least_money = $("#least_money").val();//最小金额
  		//alert(bid_money);
  		if(bid_money == null || bid_money == "" ||bid_money <= 0){
  			prompt("show_message","请输入有效的金额");
	  		return;
  		}
  		if(bid_money == 0 || bid_money == null){
  	  		prompt("show_message","请输入有效的金额");
	  		return;
  	  	}
  	  	if(bid_money < least_money){
  	  		prompt("show_message","输入金额小于"+least_money);
  			return;
  	  	}
  	  	if(bid_money % least_money != 0){
  	  		prompt("show_message","当前金额必须为"+least_money+"的整数倍!");
  	  		return;
  	  	}
  	  	$("#wait_pay_amount").html(bid_money + "元");
  	  	/**
  		 * 判断用户是否登陆
  		 */
  	  	$.ajax({
  	  		url:validate_is_login,
  	  		type:"post",
  	  		async:false,
  	  		success:function(data){
  	  			if(data != null){
  	  				if(data == "非理财身份"){
  	  					prompt("show_message","非理财账户，请先注册理财账户!");
  	  					return;
  	  				}else if(data == "NoLogined"){
  	  					window.location.href = login_url;
  	  					return;
  	  				}else{
  	  					$("#submit_buy_order").attr("onclick","pay_fix_bid_buy_cny()");//设置点击事件
  	  					refresh_order_pay();//生成验证码
  	  					document.getElementById('loginDiv1').style.display='block';
  	  					document.getElementById('loginDiv2').style.display='block';
  	  				}
  	  			}
  	  		}
  	  		
  	  		
  	  	});
  	  	
  	  	
  	}
  	/**
  	 * 关闭div
  	 * @return
  	 */
  	function close_order_pay(){
  		document.getElementById('loginDiv1').style.display='none';
  		document.getElementById('loginDiv2').style.display='none';
  	}

  	/**
  	 * 获取验证码：
  	 * @return
  	 */
  	function refresh_order_pay(){
		var CheckCodeServlet=document.getElementById("CheckCodeServlet");
			CheckCodeServlet.src= path+"/code.jpg?id="+ Math.random();
	}
  	/**
  	 * 购买联富宝订单
  	 * @return
  	 */
  	function pay_fix_bid_buy_cny(){
  		if(document.getElementById("check_box").checked == false){
  			prompt("error_msg","请先同意协议");
  			return;
  		}
  		//ID
  		var order_id = $("#order_id").val();
  		//联富宝名称
  		var bid_name = $("#bid_name").html();
  		//昵称
  		var nick_name = $("#nick_name").html();
  		//年化利率
  		var rate = $("#rate").html();
  		//投资周期
  		var close_period = $("#close_period").html();
  		//待付金额
  		var wait_pay_amount_var = $("#wait_pay_amount").html();
  		var wait_pay_amount = wait_pay_amount_var.substring(0,wait_pay_amount_var.length-1);
  		//alert("待付金额"+wait_pay_amount);
  		//可用资金
  		var can_used_cny_var = $("#can_used_cny").html();
  		var can_used_cny = can_used_cny_var.substring(0,can_used_cny_var.length - 1);
  		//验证码
  		var verificte_code = $("#verificte_code").val();
  		if(verificte_code == "" || verificte_code == null){
  			prompt("error_msg","请输入验证码");
  			return;
  		}
  		if(Number(can_used_cny) < Number(wait_pay_amount)){
  			prompt("error_msg","可用资金不足,请先充值!");
  			return
  		}
  		
  		join_lfoll_bid(verificte_code);
  	}
  	