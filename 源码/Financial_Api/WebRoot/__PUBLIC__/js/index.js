var path = "http://"+window.location.host;
var PUBLIC = "http://"+window.location.host+"/__PUBLIC__";
$(function(){
		//get_first_three_data();
		get_lfoll_bid_L12();
		show_elite_bid();
});
	
function show_elite_bid(){
	var url = path +　"/user/financial/financial/get_loan_list_index.html";

	$.ajax({
		url:url,
		type:'post',
		cache:false,
		success:function(data){
		var jsObject = JSON.parse(data);
		$("#table1 tr:not(:first)").empty(); 
		var tr_str ='';
		for(var i=0;i<jsObject.gather_order_list.length;i++ ){

			var gather_str = 		'<td height="45" align="center" class="con2_kk"><a href="'+path+'/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.gather_order_list[i].id+'"><p class="con2_anniu" style="background: #e85e48;">投标</p></a></td>';
			var full_str = 			'<td height="45" align="center" class="con2_kk"><p class="con2_anniu">已满标</p></td>';
			var out_of_date_str =	'<td height="45" align="center" class="con2_kk"><p class="con2_anniu">流标</p></td>';
			var repaying_str = 		'<td height="45" align="center" class="con2_kk"><p class="con2_anniu">还款中</p></td>';
			var finish_repay_str = 	'<td height="45" align="center" class="con2_kk"><p class="con2_anniu">还款完成</p></td>';
		
			
			var str ;
			
			if(jsObject.gather_order_list[i].gather_state==1){
				str = gather_str;
			}else if(jsObject.gather_order_list[i].gather_state==2){//流标
				str = out_of_date_str;
			}else if(jsObject.gather_order_list[i].gather_state==3){//筹款成功
				str = full_str;
				if(jsObject.gather_order_list[i].payment_state == 2){//还款中
					str = repaying_str;
				}else if(jsObject.gather_order_list[i].payment_state == 3){//还款完毕
					str = finish_repay_str;
				}
			}
			
			var rate_style = '';

			if(jsObject.gather_order_list[i].credit_rating == 'A+'){
				rate_style = 'style="background:#ffd700;"';
			}else if(jsObject.gather_order_list[i].credit_rating == 'A'){
				rate_style = 'style="background:#fded94;"';
			}else if(jsObject.gather_order_list[i].credit_rating == 'B+'){
				rate_style = 'style="background:#7ae805;"';
			}else if(jsObject.gather_order_list[i].credit_rating == 'B'){
				rate_style = 'style="background:#d0fba2;"';
			}else if(jsObject.gather_order_list[i].credit_rating == 'C+'){
				rate_style = 'style="background:#908442;"';
			}else if(jsObject.gather_order_list[i].credit_rating == 'C'){
				rate_style = 'style="background:#2a2715;"';
			}

			tr_str += 
			
			'<tr>'
		    + '<td height="45" class="con2_kk"><img src="'+PUBLIC+'/images/icon1.jpg" />&nbsp;&nbsp;&nbsp;<a href="'+path+'/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.gather_order_list[i].id+'">'+jsObject.gather_order_list[i].borrow_title+'</a></td>'
		    + '<td height="45" class="con2_kk"><span class="con2_yuan" '+ rate_style +'>'+ jsObject.gather_order_list[i].credit_rating +'</span></td>'
		    + '<td height="45" class="con2_kk">'+ jsObject.gather_order_list[i].annulized_rate_int +'%</td>'
		    + '<td height="45" class="con2_kk">'+ jsObject.gather_order_list[i].borrow_all_money +'元</td>'
		    + '<td height="45" class="con2_kk">'+ jsObject.gather_order_list[i].borrow_duration +'个月</td>'
		    + '<td height="45" class="con2_kk"><span class="hongse">'+ jsObject.gather_order_list[i].gather_progress +'%</span></td>'
		    + str
		  + '</tr>';
			
			
		}
		$("#table1 tr:eq(0)").after(tr_str);
	
		}
	},"json");	
	
}
		
/**
 * 获取加入联富宝人数
 * 累计成功投资金额
 * 投资人已赚取金额
 * @return
 */
function get_first_three_data(){
	var get_three_data_url = path + "/index/get_three_data.html";
	$.ajax({
		url:get_three_data_url,
		type:"post",
		success:function(data){
			var jsonObject = eval("("+data+")");
			var join_count = jsonObject.join_count;
			var invest_money = jsonObject.n_w;
			$("#join_count").html(join_count+"人");
			$("#invest_money").html(invest_money+"万");
			//alert(join_count);
			//alert(join_total_money);
		}
	});
}

/**
 * 获取联富宝 当期发行的L12期
 * @return
 */
function get_lfoll_bid_L12(){
	var lfoll_bid_L12_url = path + "/index/get_current_lfoll_bid_l12.html";
	$.ajax({
		url:lfoll_bid_L12_url,
		type:"post",
		success:function(data){
			if(data != null && data != "" && data != "[]"){
				var jsonObject = eval("("+data+")");
				var id = jsonObject.id;
				var bid_name = jsonObject.bid_name;
				var least_money = jsonObject.least_money;
				var rate = jsonObject.annualized_rate * 100;
				var closed_period = jsonObject.closed_period;
				var total_money = Number(jsonObject.total_money)/Number(10000);
				var public_state_var = jsonObject.public_state_var;
				$("#bid_name").html("<span>联富宝</span>"+bid_name);
				$("#least_money").html(least_money);
				$("#rate").html(rate);
				$("#closed_period").html(closed_period);
				$("#total_money").html("总发售金额:"+total_money+"万");
				$("#buy_fix_bid").attr("onclick","dump_to_buy_page('"+id+"')");
				if(public_state_var == 1){
					$("#img").attr("src",PUBLIC + "/images/sanbiao_lfb_1.png");
				}else if(public_state_var == 2){
					$("#img").attr("src",PUBLIC + "/images/sanbiao_lfb_2.png");
				}else if(public_state_var == 3){
					$("#img").attr("src",PUBLIC + "/images/sanbiao_lfb_3.png");
				}else if(public_state_var == 4){
					$("#img").attr("src",PUBLIC + "/images/sanbiao_lfb_4.png");
				}else if(public_state_var == 5){
					$("#img").attr("src",PUBLIC +"/images/sanbiao_lfb_5.png");
				}else if(public_state_var == 6){
					$("#img").attr("src",PUBLIC +"/images/sanbiao_lfb_6.png");
				}else if(public_state_var == 7){
					$("#img").attr("src",PUBLIC +"/images/sanbiao_lfb_7.png");
				}
			}
	
		}
	});
}

function dump_to_buy_page(id){
	window.location.href = path+"/user/financial/lfollinvest/get_lfoll_invest_info.html?id="+id;
}