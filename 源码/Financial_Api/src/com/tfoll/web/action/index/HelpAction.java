package com.tfoll.web.action.index;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;

/**
 * 帮助中心
 * 
 * @author hx
 */
@ActionKey("/help")
public class HelpAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_index() {
		renderJsp("/index/help/help.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_aq_bjbzjh() {
		renderJsp("/index/help/help_aq_bjbzjh.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_aq_flyzcbz() {
		renderJsp("/index/help/help_aq_flyzcbz.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_aq_jkshyfk() {
		renderJsp("/index/help/help_aq_jkshyfk.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_aq_wzxgxy() {
		renderJsp("/index/help/help_aq_wzxgxy.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_aq_yhdzwbh() {
		renderJsp("/index/help/help_aq_yhdzwbh.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_aq_zhjysaq() {
		renderJsp("/index/help/help_aq_zhjysaq.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_aq() {
		renderJsp("/index/help/help_aq.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_ckytx() {
		renderJsp("/index/help/help_jk_ckytx.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_cpjs() {
		renderJsp("/index/help/help_jk_cpjs.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_jkfy() {
		renderJsp("/index/help/help_jk_jkfy.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_rhhk() {
		renderJsp("/index/help/help_jk_rhhk.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_rhsq() {
		renderJsp("/index/help/help_jk_rhsq.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_rzzl() {
		renderJsp("/index/help/help_jk_rzzl.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_xydjyed() {
		renderJsp("/index/help/help_jk_xydjyed.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk_xysh() {
		renderJsp("/index/help/help_jk_xysh.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_jk() {
		renderJsp("/index/help/help_jk.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_lc_cpjs() {
		renderJsp("/index/help/help_lc_cpjs.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_lc_lfb() {
		renderJsp("/index/help/help_lc_lfb.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_lc_sbtz() {
		renderJsp("/index/help/help_lc_sbtz.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_lc_syyfy() {
		renderJsp("/index/help/help_lc_syyfy.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_lc_xsbd() {
		renderJsp("/index/help/help_lc_xsbd.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_lc_zqzr() {
		renderJsp("/index/help/help_lc_zqzr.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_lc() {
		renderJsp("/index/help/help_lc.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_mc() {
		renderJsp("/index/help/help_mc.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_zh_aqrz() {
		renderJsp("/index/help/help_zh_aqrz.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_zh_cz() {
		renderJsp("/index/help/help_zh_cz.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_zh_dlzc() {
		renderJsp("/index/help/help_zh_dlzc.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_zh_tx() {
		renderJsp("/index/help/help_zh_tx.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_zh_xxzx() {
		renderJsp("/index/help/help_zh_xxzx.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_zh_zhmm() {
		renderJsp("/index/help/help_zh_zhmm.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "hx")
	public void help_zh() {
		renderJsp("/index/help/help_zh.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入帮助中心首页", last_update_author = "向旋")
	public void find_help_zh() {
		int type = getParameterToInt("type", 0);
		setAttribute("type", type);
		renderJsp("/index/help/help_mc.jsp");
		return;
	}

}
