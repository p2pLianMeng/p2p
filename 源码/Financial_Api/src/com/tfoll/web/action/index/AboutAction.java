package com.tfoll.web.action.index;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.model.AboutInfoM;
import com.tfoll.web.model.AboutTypeM;
import com.tfoll.web.util.HtmlRegexpUtil;
import com.tfoll.web.util.page.PageDiv2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * 关于我们
 * 
 * @author tf
 * 
 */
@ActionKey("/index/about")
public class AboutAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入关于我们页面", last_update_author = "hx")
	public void about_page() {
		String type = getParameter("flag", "intro");
		if ("".equals(type) || type == null) {
			renderText("非法访问!");
			return;
		}
		if (type.equals("intro")) {

			/**
			 * 公司简介
			 */
			get_about_list("intro");
			renderJsp("/index/about_us/about_us.jsp");
			return;
		} else if (type.equals("team")) {
			/**
			 * 管理团队
			 */
			get_about_list("team");
			renderJsp("/index/about_us/team_manage.jsp");
			return;
		} else if (type.equals("news")) {
			/**
			 * 最新动态
			 */
			get_about_list("news");
			renderJsp("/index/about_us/last_news.jsp");
			return;
		} else if (type.equals("notice")) {
			/**
			 *网站公告 分页缓存查询
			 */
			get_about_list("notice");
			int pn = getParameterToInt("pn", 1);
			get_about_info_list_page(pn, "notice");

			renderJsp("/index/about_us/site_notice.jsp");
			return;
		} else if (type.equals("contact")) {
			/**
			 * 联系我们
			 */
			get_about_list("contact");
			renderJsp("/index/about_us/contact_us.jsp");
			return;
		} else if (type.equals("consult")) {
			/**
			 * 最新资讯 分页缓存查询
			 */
			get_about_list("consult");
			int pn = getParameterToInt("pn", 1);
			get_about_info_list_page(pn, "consult");

			renderJsp("/index/about_us/last_consult.jsp");
			return;
		} else if (type.equals("agreement")) {
			/**
			 * 协议
			 */
			get_about_list("agreement");
			renderJsp("/index/about_us/agreement/agreement.jsp");
			return;
		} else if (type.equals("join_us")) {
			/**
			 * 加入我们
			 */
			get_about_list("join_us");
			renderJsp("/index/about_us/join_us.jsp");
			return;
		} else if (type.equals("expert_adviser")) {
			/**
			 * 专家顾问
			 */
			get_about_list("expert_adviser");
			renderJsp("/index/about_us/expert_adviser.jsp");
			return;
		} else if (type.equals("feed_back")) {
			/**
			 * 意见反馈
			 */
			get_about_list("feed_back");
			renderJsp("/index/about_us/feed_back.jsp");
			return;
		} else if (type.equals("tariff")) {
			get_about_list("tariff");
			renderJsp("/index/about_us/tariff_description.jsp");
			return;
		} else {
			renderText("非法访问!");
			return;
		}

	}

	private void get_about_list(String tag_type) {
		String sql = "SELECT type_name,url,tag FROM about_type WHERE is_effect = 1 ORDER BY sort ASC ";
		String str = "<ul>";
		StringBuffer buffer = new StringBuffer(str);
		List<AboutTypeM> about_type_list = AboutTypeM.dao.find(sql);
		HttpServletRequest request = getRequest();
		String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
		for (AboutTypeM about_type : about_type_list) {
			String tag = about_type.getString("tag");
			if (tag.equals(tag_type)) {
				buffer.append("<li>");
				buffer.append("<a href=\"" + __ROOT_PATH__ + about_type.getString("url") + "\"" + "class=\"hover\">" + about_type.getString("type_name") + "</a>");
				buffer.append("</li>");
			} else {
				buffer.append("<li>");
				buffer.append("<a href=\"" + __ROOT_PATH__ + about_type.getString("url") + "\">" + about_type.getString("type_name") + "</a>");
				buffer.append("</li>");
			}
		}
		buffer.append("</ul>");
		setAttribute("list_str", buffer.toString());
	}

	/**
	 * 分页 查询 最新资讯 和 网站公告
	 * 
	 * @param pn
	 *            当前页数
	 * @param string
	 */
	private void get_about_info_list_page(int pn, String string) {
		String count_sql = "";
		String sql_list = "";
		if ("consult".equals(string)) {
			count_sql = "SELECT count(1) FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '最新资讯' ORDER BY t1.id DESC ";
			sql_list = "SELECT t1.* FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '最新资讯' ORDER BY t1.id DESC";
		} else if ("notice".equals(string)) {
			count_sql = "SELECT count(1) FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '网站公告' ORDER BY t1.id DESC ";
			sql_list = "SELECT t1.* FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '网站公告' ORDER BY t1.id DESC";
		}
		Long total_rows = Db.queryLong(count_sql);
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn, 4);//
			List<AboutInfoM> list = AboutInfoM.dao.find(page.creatLimitSql(sql_list));
			List<Record> record_list = new ArrayList<Record>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			for (AboutInfoM about_info : list) {
				Record record = new Record();

				int id = about_info.getInt("id");
				String content = about_info.getString("content");
				Date add_time = about_info.getTimestamp("add_time");
				content = HtmlRegexpUtil.filterHtmlContent4Show(content);
				if (content.length() > 30) {
					content = content.substring(0, 25);
					content = content + "...";
				}
				String add_time_str = format.format(add_time);
				record.add("id", id);
				record.add("content", content);
				record.add("add_time_str", add_time_str);
				record_list.add(record);
			}
			String url = PageDiv2.createUrl(getRequest(), "/index/about/about_page");
			String pageDiv = PageDiv2.getDiv(url, "&flag=" + string, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("record_list", record_list);
			setAttribute("pageDiv", pageDiv);

		} else {
			setAttribute("tips", "暂时无数据");
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "最新资讯详情", last_update_author = "hx")
	public void last_consult_detail() {
		Integer id = getParameterToInt("id", 0);
		if (id != null && id > 0) {
			AboutInfoM aboutInfo = AboutInfoM.dao.findById(id);
			if (aboutInfo == null) {
				renderText("非法请求路径");
				return;
			}
			String content = aboutInfo.getString("content");
			// content = HtmlRegexpUtil.filterHtmlContent4Show(content);
			aboutInfo.set("content", content);
			setAttribute("aboutInfo", aboutInfo);
		}
		renderJsp("/index/about_us/last_consult_detail.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "网站公告详情", last_update_author = "hx")
	public void site_notice_detail() {
		Integer id = getParameterToInt("id", 0);
		if (id != null && id > 0) {
			AboutInfoM aboutInfo = AboutInfoM.dao.findById(id);
			if (aboutInfo == null) {
				renderText("非法请求路径");
				return;
			}
			String content = aboutInfo.getString("content");
			// content = HtmlRegexpUtil.filterHtmlContent4Show(content);
			aboutInfo.set("content", content);
			setAttribute("aboutInfo", aboutInfo);
		}
		renderJsp("/index/about_us/site_notice_detail.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement() {
		renderJsp("/index/about_us/agreement/agreement.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_1() {
		renderJsp("/index/about_us/agreement/agreement_1.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_2() {
		renderJsp("/index/about_us/agreement/agreement_2.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_3() {
		renderJsp("/index/about_us/agreement/agreement_3.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_4() {
		renderJsp("/index/about_us/agreement/agreement_4.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_5() {
		renderJsp("/index/about_us/agreement/agreement_5.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_6() {

		renderJsp("/index/about_us/agreement/agreement_6.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_7() {

		renderJsp("/index/about_us/agreement/agreement_7.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_8() {

		renderJsp("/index/about_us/agreement/agreement_8.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_9() {

		renderJsp("/index/about_us/agreement/agreement_9.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_10() {

		renderJsp("/index/about_us/agreement/agreement_10.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_11() {

		renderJsp("/index/about_us/agreement/agreement_11.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_12() {

		renderJsp("/index/about_us/agreement/agreement_12.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_13() {
		renderJsp("/index/about_us/agreement/agreement_13.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_14() {
		renderJsp("/index/about_us/agreement/agreement_14.jsp");
		return;

	}

}
