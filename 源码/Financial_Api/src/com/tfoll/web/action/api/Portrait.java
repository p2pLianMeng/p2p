package com.tfoll.web.action.api;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.upload.CosUpload;
import com.tfoll.trade.upload.UploadFile;

import java.io.File;
import java.util.List;

@ActionKey("/finacial_api/api/portrait")
public class Portrait extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "登陆后用户可见", function_description = "更改头像", last_update_author = "zjb")
	public void change_img() {
		int user_id = getParameterToInt("user_id");

		CosUpload cosupload = new CosUpload(getRequest(), null, new String[] { ".jpg", ".jpeg", ".bmp", ".png" }, new String[] { "name" });
		List<UploadFile> upload_file_list = cosupload.getFiles();

		UploadFile upload_file = upload_file_list.get(0);
		File file = upload_file.getFile();

		if (file.length() > 1572864) {
			cosupload.deleteFiles();
			renderText("out");
			return;
		}
		/**
		 * 文件夹
		 */
		String path = null;

		File f = new File(path);
		if (!f.exists()) {
			f.mkdirs();
		}

		// file.renameTo(fol);
	}
}
