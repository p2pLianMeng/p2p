package com.tfoll.web.action.user.financial;

import com.tfoll.console.util.ConsoleInit;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.LenderAop;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.model.FixBidUserHoldM;
import com.tfoll.web.model.FixBidUserNewerBidM;
import com.tfoll.web.model.FixBidUserNewerBidWaitingM;
import com.tfoll.web.model.FixBidUserOrderM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.model._____Activity___Fix_Bid_WaitingM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@ActionKey("/user/financial/lfollinvest")
public class LfollInvestAction extends Controller {

	/**
	 * 查询所已经发布的联富宝期限为12个月（联富宝L12）的信息列表
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "查询所已经发布的定投信息 （联富宝L12）;(集合)", last_update_author = "hx")
	public void get_12month_already_released_invests() {
		int pn = getParameterToInt("pn", 1);
		String count_sql = "SELECT COUNT(1) FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 12 ORDER BY S1.id DESC";
		String select_list_sql = "SELECT S1.id AS id, S1.bid_name AS bid_name, S1.total_money AS total_money, S1.booking_number AS booking_number, S1.annualized_rate AS annualized_rate, S1.advance_notice_start_time_long AS advance_notice_start_time_long, S1.reserve_start_time_long AS reserve_start_time_long, S1.reserve_end_time_long AS reserve_end_time_long, S1.pay_end_time_long AS pay_end_time_long, S1.invite_start_time_long AS invite_start_time_long, S1.invite_end_time_long AS invite_end_time_long, S1.repayment_time_long AS repayment_time_long FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 12 ORDER BY S1.id DESC";

		Long total_rows = Db.queryLong(count_sql);
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(page.creatLimitSql(select_list_sql));

			// List<Record> fix_bid_system_order_list_record = new
			// ArrayList<Record>();

			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				Record record = new Record();
				get_lfoll_fix_bid_cycle(fix_bid_system_order);

				record.add("annualized_rate", fix_bid_system_order.getBigDecimal("annualized_rate").multiply(new BigDecimal("100")).setScale(1, BigDecimal.ROUND_DOWN));// 预期年化收益
			}
			setAttribute("fix_bid_system_order_list", fix_bid_system_order_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/financial/lfollinvest/get_12month_already_released_invests");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);

		} else {
			setAttribute("tips", "暂无数据!");
		}
		renderJsp("/user/lfoll_invest/lfoll_invest_list_12.jsp");// 跳转到发布定投信息列表的页面
		return;
	}

	/**
	 * 查询所已经发布的联富宝期限为06个月（联富宝L06）的信息列表
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "查询所已经发布的定投信息（联富宝L06）;(集合)", last_update_author = "hx")
	public void get_06month_already_released_invests() {
		int pn = getParameterToInt("pn", 1);
		String count_sql = "SELECT COUNT(1) FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 6 ORDER BY S1.id DESC";
		String select_list_sql = "SELECT S1.id AS id, S1.bid_name AS bid_name, S1.total_money AS total_money, S1.booking_number AS booking_number, S1.annualized_rate AS annualized_rate, S1.advance_notice_start_time_long AS advance_notice_start_time_long, S1.reserve_start_time_long AS reserve_start_time_long, S1.reserve_end_time_long AS reserve_end_time_long, S1.pay_end_time_long AS pay_end_time_long, S1.invite_start_time_long AS invite_start_time_long, S1.invite_end_time_long AS invite_end_time_long, S1.repayment_time_long AS repayment_time_long FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 6 ORDER BY S1.id DESC";

		Long total_rows = Db.queryLong(count_sql);
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(page.creatLimitSql(select_list_sql));

			// List<Record> fix_bid_system_order_list_record = new
			// ArrayList<Record>();

			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				Record record = new Record();
				get_lfoll_fix_bid_cycle(fix_bid_system_order);

				record.add("annualized_rate", fix_bid_system_order.getBigDecimal("annualized_rate").multiply(new BigDecimal("100")).setScale(1, BigDecimal.ROUND_DOWN));// 预期年化收益
			}
			setAttribute("fix_bid_system_order_list", fix_bid_system_order_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/financial/lfollinvest/get_06month_already_released_invests");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);

		} else {
			setAttribute("tips", "暂无数据!");
		}
		renderJsp("/user/lfoll_invest/lfoll_invest_list_06.jsp");// 跳转到发布定投信息列表的页面
		return;

	}

	/**
	 * 查询所已经发布的联富宝期限为03个月（联富宝L03）的信息列表
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "查询所已经发布的定投信息（联富宝L03）;(集合)", last_update_author = "hx")
	public void get_03month_already_released_invests() {
		int pn = getParameterToInt("pn", 1);
		String count_sql = "SELECT COUNT(1) FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 3 ORDER BY S1.id DESC";
		String select_list_sql = "SELECT S1.id AS id, S1.bid_name AS bid_name, S1.total_money AS total_money, S1.booking_number AS booking_number, S1.annualized_rate AS annualized_rate, S1.advance_notice_start_time_long AS advance_notice_start_time_long, S1.reserve_start_time_long AS reserve_start_time_long, S1.reserve_end_time_long AS reserve_end_time_long, S1.pay_end_time_long AS pay_end_time_long, S1.invite_start_time_long AS invite_start_time_long, S1.invite_end_time_long AS invite_end_time_long, S1.repayment_time_long AS repayment_time_long FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 3 ORDER BY S1.id DESC";

		Long total_rows = Db.queryLong(count_sql);
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(page.creatLimitSql(select_list_sql));

			// List<Record> fix_bid_system_order_list_record = new
			// ArrayList<Record>();

			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				Record record = new Record();
				get_lfoll_fix_bid_cycle(fix_bid_system_order);

				record.add("annualized_rate", fix_bid_system_order.getBigDecimal("annualized_rate").multiply(new BigDecimal("100")).setScale(1, BigDecimal.ROUND_DOWN));// 预期年化收益
			}
			setAttribute("fix_bid_system_order_list", fix_bid_system_order_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/financial/lfollinvest/get_03month_already_released_invests");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);

		} else {
			setAttribute("tips", "暂无数据!");
		}
		renderJsp("/user/lfoll_invest/lfoll_invest_list_03.jsp");// 跳转到发布定投信息列表的页面
		return;

	}

	public static void get_lfoll_fix_bid_cycle(FixBidSystemOrderM fix_bid_system_order) {
		// 公告开始时间的毫秒数
		Long advance_notice_start_time_long = fix_bid_system_order.getLong("advance_notice_start_time_long");
		// 预定开始时间的毫秒数
		Long reserve_start_time_long = fix_bid_system_order.getLong("reserve_start_time_long");
		// 预定结束时间的毫秒数
		Long reserve_end_time_long = fix_bid_system_order.getLong("reserve_end_time_long");
		// 支付截止时间的毫秒数
		Long pay_end_time_long = fix_bid_system_order.getLong("pay_end_time_long");
		// 投标开始时间毫秒数
		Long invite_start_time_long = fix_bid_system_order.getLong("invite_start_time_long");
		// 投标结束时间的毫秒数
		Long invite_end_time_long = fix_bid_system_order.getLong("invite_end_time_long");
		// 退出定投时间毫秒数
		Long repayment_time_long = fix_bid_system_order.getLong("repayment_time_long");

		Long current_time = System.currentTimeMillis();

		if ((advance_notice_start_time_long < current_time) && (current_time < reserve_start_time_long)) {
			fix_bid_system_order.set("public_state_var", 1);// 公告阶段 等待预定
		} else if ((reserve_start_time_long < current_time) && (current_time < reserve_end_time_long)) {
			fix_bid_system_order.set("public_state_var", 2);// 预定中
		} else if ((reserve_end_time_long < current_time) && (current_time < pay_end_time_long)) {
			fix_bid_system_order.set("public_state_var", 3);// 等待支付
		} else if ((pay_end_time_long < current_time) && (current_time < invite_start_time_long)) {
			fix_bid_system_order.set("public_state_var", 4);// 等待开标
		} else if ((invite_start_time_long < current_time) && (current_time < invite_end_time_long)) {
			fix_bid_system_order.set("public_state_var", 5);// 开放中
		} else if ((invite_end_time_long < current_time) && (current_time < repayment_time_long)) {
			fix_bid_system_order.set("public_state_var", 6);// 收益中
		} else if (current_time > repayment_time_long) {
			fix_bid_system_order.set("public_state_var", 7);// 已结束
		}
	}

	/**
	 * 查询单条联富定投基本信息
	 * 
	 * @throws ParseException
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "查询单条联富定投基本信息", last_update_author = "hx")
	public void get_lfoll_invest_info() throws ParseException {
		final Integer id = getParameterToInt("id", 0);
		if (id <= 0 || id == null) {
			renderText("非法访问!");
			return;
		} else {
			String select_sql = "select * from fix_bid_system_order t where t.id = ? and t.is_show = 1";
			FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.dao.findFirst(select_sql, new Object[] { id });
			UserM userM = getSessionAttribute("user");
			if (userM != null) {
				UserNowMoneyM userNowMoneyM = UserNowMoneyM.get_user_current_money(userM.getInt("id"));
				setAttribute("cny_can_used", userNowMoneyM.getBigDecimal("cny_can_used").setScale(2, BigDecimal.ROUND_DOWN));
				setAttribute("nick_name", userM.getString("nickname"));// request设置昵称
			}

			if (fix_bid_system_order == null) {
				renderText("非法访问!");
				return;
			} else {
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy年MM月dd日 ");
				SimpleDateFormat format3 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				// 格式化退出时间 为 yyyy年MM月dd日
				Date repayment_time = fix_bid_system_order.getTimestamp("repayment_time");
				String exit_time_str = format1.format(repayment_time);
				fix_bid_system_order.set("repayment_time", exit_time_str);
				// 格式化预定开始时间
				SimpleDateFormat format2 = new SimpleDateFormat("MM月dd日  HH:mm");
				Date reserve_start_time = fix_bid_system_order.getTimestamp("reserve_start_time");
				String reserve_start_time_str = format2.format(reserve_start_time);// MM月dd日
				// HH:mm
				// 预定开始时间 用于倒计时
				String reserve_start_time_str3 = format3.format(reserve_start_time);// yyyy/MM/dd
				// HH:mm:ss
				// 公告页面倒计时
				fix_bid_system_order.set("reserve_start_time", reserve_start_time_str);
				setAttribute("reserve_start_time_str3", reserve_start_time_str3);
				// 预定结束时间
				Date reserve_end_time = fix_bid_system_order.getTimestamp("reserve_end_time");
				String reserve_end_time_str = format2.format(reserve_end_time);
				fix_bid_system_order.set("reserve_end_time", reserve_end_time_str);
				// 支付截止时间
				Date pay_end_time = fix_bid_system_order.getTimestamp("pay_end_time");
				String pay_end_time_str = format2.format(pay_end_time);
				String pay_end_time_str3 = format3.format(pay_end_time);
				setAttribute("pay_end_time_str3", pay_end_time_str3);// //yyyy/MM/dd
				// HH:mm:ss
				// 预定截止
				// 支付页面倒计时
				fix_bid_system_order.set("pay_end_time", pay_end_time_str);
				// 定投开始时间
				Date invite_start_time = fix_bid_system_order.getTimestamp("invite_start_time");
				String invite_start_time_str = format2.format(invite_start_time);
				String invite_start_time_str3 = format3.format(invite_start_time);
				setAttribute("invite_start_time_str3", invite_start_time_str3);
				fix_bid_system_order.set("invite_start_time", invite_start_time_str);
				// 锁定时间
				Date invite_end_time = fix_bid_system_order.getTimestamp("invite_end_time");
				String invite_end_time_str = format2.format(invite_end_time);
				fix_bid_system_order.set("invite_end_time", invite_end_time_str);

				// 公告开始时间的毫秒数
				Long advance_notice_start_time_long = fix_bid_system_order.getLong("advance_notice_start_time_long");
				// 预定开始时间的毫秒数
				Long reserve_start_time_long = fix_bid_system_order.getLong("reserve_start_time_long");
				// 预定结束时间的毫秒数
				Long reserve_end_time_long = fix_bid_system_order.getLong("reserve_end_time_long");
				// 支付截止时间的毫秒数
				Long pay_end_time_long = fix_bid_system_order.getLong("pay_end_time_long");
				// 投标开始时间毫秒数
				Long invite_start_time_long = fix_bid_system_order.getLong("invite_start_time_long");
				// 投标结束时间的毫秒数
				Long invite_end_time_long = fix_bid_system_order.getLong("invite_end_time_long");
				// 退出定投时间毫秒数
				Long repayment_time_long = fix_bid_system_order.getLong("repayment_time_long");
				/**
				 * 系统当前时间
				 */
				Long current_time = System.currentTimeMillis();
				if ((advance_notice_start_time_long < current_time) && (current_time < reserve_start_time_long)) {
					setAttribute("state", 1);// 公告阶段 等待预定
					setAttribute("fix_bid_system_order", fix_bid_system_order);
					get_user_order_and_hold_list(fix_bid_system_order);
					renderJsp("/user/lfoll_invest/lfoll_invest_detail_before_order.jsp");// 跳转到//
					// （等待预定）//
					// 基本信息页面
					return;
				} else if ((reserve_start_time_long < current_time) && (current_time < reserve_end_time_long)) {
					setAttribute("state", 2);// 预定中
					setAttribute("fix_bid_system_order", fix_bid_system_order);
					get_user_order_and_hold_list(fix_bid_system_order);
					renderJsp("/user/lfoll_invest/lfoll_invest_detail_ordering.jsp");// 跳转到//
					// （开始预订）//
					// 基本信息页面
					return;
				} else if ((reserve_end_time_long < current_time) && (current_time < pay_end_time_long)) {
					setAttribute("state", 3);// 等待支付
					setAttribute("fix_bid_system_order", fix_bid_system_order);
					get_user_order_and_hold_list(fix_bid_system_order);
					renderJsp("/user/lfoll_invest/lfoll_invest_detail_order_over.jsp");// 跳转到//
					// （预定结束）//
					// 基本信息页面
					return;
				} else if ((pay_end_time_long < current_time) && (current_time < invite_start_time_long)) {
					setAttribute("state", 4);// 等待开发加入
					setAttribute("fix_bid_system_order", fix_bid_system_order);
					get_user_order_and_hold_list(fix_bid_system_order);
					renderJsp("/user/lfoll_invest/lfoll_invest_detail_pay_deadline.jsp");// 跳转到//
					// （支付截止）//
					// 基本信息页面
					return;
				} else if ((invite_start_time_long < current_time) && (current_time < invite_end_time_long)) {
					setAttribute("state", 5);// 开放中
					setAttribute("fix_bid_system_order", fix_bid_system_order);
					get_user_order_and_hold_list(fix_bid_system_order);

					renderJsp("/user/lfoll_invest/lfoll_invest_detail_joining.jsp");// 跳转到//
					// （开发加入中）//
					// 基本信息页面
					return;
				} else if ((invite_end_time_long < current_time) && (current_time < repayment_time_long)) {
					setAttribute("state", 6);// 收益中
					setAttribute("fix_bid_system_order", fix_bid_system_order);
					get_user_order_and_hold_list(fix_bid_system_order);
					renderJsp("/user/lfoll_invest/lfoll_invest_detail_earning.jsp");// 跳转到//
					// （收益中）//
					// 基本信息页面
					return;
				} else if (current_time > repayment_time_long) {
					setAttribute("state", 7);// 已结束
					setAttribute("fix_bid_system_order", fix_bid_system_order);
					get_user_order_and_hold_list(fix_bid_system_order);
					renderJsp("/user/lfoll_invest/lfoll_invest_detail_all_over.jsp");// 跳转到//
					// （结束退出）//
					// 基本信息页面
					return;
				}

			}
			return;
		}

	}

	/**
	 * 获取所有用户预定记录 和 加入记录
	 * 
	 * @param fix_bid_system_order
	 */
	private void get_user_order_and_hold_list(FixBidSystemOrderM fix_bid_system_order) {
		// 查询预订中的人数纪录
		List<FixBidUserOrderM> fix_bid_user_order_list = get_order_users_list(fix_bid_system_order);
		if (fix_bid_user_order_list != null && fix_bid_user_order_list.size() > 0) {
			setAttribute("fix_bid_user_order_list", fix_bid_user_order_list);
		} else {
			setAttribute("tips_order", "暂无数据!");
		}
		// 查询加入联富宝人数得记录
		List<FixBidUserHoldM> fix_bid_user_hold_list = get_hold_fix_bid_user_list(fix_bid_system_order);
		if (fix_bid_user_hold_list != null && fix_bid_user_hold_list.size() > 0) {
			setAttribute("fix_bid_user_hold_list", fix_bid_user_hold_list);
		} else {
			setAttribute("tips_hold", "暂无数据!");
		}
	}

	/**
	 * 查询联富宝预定的用户数量
	 * 
	 * @param fix_bid_system_order
	 * @return
	 */
	private List<FixBidUserOrderM> get_order_users_list(FixBidSystemOrderM fix_bid_system_order) {
		String fix_bid_user_order_list_sql = "select * from fix_bid_user_order where fix_bid_sys_id = ? ";
		Long system_id = fix_bid_system_order.getLong("id");
		List<FixBidUserOrderM> fix_bid_user_order_list = FixBidUserOrderM.dao.find(fix_bid_user_order_list_sql, new Object[] { system_id });
		if (fix_bid_user_order_list == null) {
			fix_bid_user_order_list = new ArrayList<FixBidUserOrderM>();
		}

		for (FixBidUserOrderM fix_bid_user_order : fix_bid_user_order_list) {
			Date date = fix_bid_user_order.getTimestamp("add_time");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String date_str = format.format(date);

			fix_bid_user_order.set("add_time", date_str);
		}
		return fix_bid_user_order_list;
	}

	/**
	 * 查询联富宝持有的用户集合
	 * 
	 * @param fix_bid_system_order
	 * @return
	 */
	private List<FixBidUserHoldM> get_hold_fix_bid_user_list(FixBidSystemOrderM fix_bid_system_order) {
		String fix_bid_user_hold_list_sql = "select * from fix_bid_user_hold t1 where t1.fix_bid_sys_id = ?";
		Long system_id = fix_bid_system_order.getLong("id");
		List<FixBidUserHoldM> fix_bid_user_hold_list = FixBidUserHoldM.dao.find(fix_bid_user_hold_list_sql, new Object[] { system_id });
		for (FixBidUserHoldM fix_bid_user_hold : fix_bid_user_hold_list) {
			Date date = fix_bid_user_hold.getTimestamp("add_time");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String date_str = format.format(date);

			fix_bid_user_hold.set("add_time", date_str);
		}

		return fix_bid_user_hold_list;
	}

	/**
	 * 购买联富宝
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "购买联富宝", last_update_author = "hx")
	public void buy_lfoll_invest() {
		final Long order_id = getParameterToLong("order_id", 0);// 联富宝ID
		final Integer should_pay = getParameterToInt("should_pay", 0);// 应付款金额-购买多少联富宝-输入多少就是多少-和预定不一样
		final Integer bid_money = getParameterToInt("bid_money", 0);// 投标金额-input框
		final BigDecimal bid_money_bigdecimal = new BigDecimal(bid_money + "");
		final Integer least_money = getParameterToInt("least_money", 0);// 最少付款金额的整数倍
		final String verificte_code = getParameter("verificte_code");// 验证码

		if (order_id <= 0) {
			renderText("1");// 非法请求
			return;
		}
		if (should_pay <= 0) {
			renderText("2");// 应付款金额为0
			return;
		}
		if (bid_money <= 0) {
			renderText("3");// 投资金额不能0
			return;
		}
		if (least_money <= 0) {
			renderText("黑客操作");
			return;
		}
		if (bid_money % least_money != 0) {
			renderText("4");// 投资金额不是least_money的整数倍
			return;
		}
		final int least_money_from_page = least_money;// 数据库里面需要进行校验

		if (Utils.isNullOrEmptyString(verificte_code)) {
			renderText("5");// 验证码为空
			return;
		}
		if (!verificte_code.equals(getSessionAttribute("code"))) {
			renderText("6");// 验证码错误
			return;
		}

		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");

		final String real_name = user.getString("real_name");
		final String nick_name = user.getString("nickname");

		// 获取用户当前资金
		UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user_id);
		final BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用资金

		if (cny_can_used.compareTo(bid_money_bigdecimal) < 0) {
			renderText("7");// 可用RMB不足
			return;
		}
		// 获取原子事务里的返回状态
		final AtomicReference<String> error_code = new AtomicReference<String>();
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				Date date = new Date();
				/**
				 * 
				 *第1步： 用户资金表减去投资的金额 (可用资金 = 可用资金 - 投资金额) (冻结资金 = 冻结资金 + 投资金额)
				 ***/
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				if (user_now_money == null) {
					error_code.set("请求超时");// 可能是非法操作 or 直接超时提示
					return false;
				}
				/**
				 * 事务里面和外面都要进行资金判断
				 */
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用资金
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
				if (cny_can_used.compareTo(bid_money_bigdecimal) < 0) {
					error_code.set("资金不足");// 可能是非法操作 or 直接超时提示
					return false;
				}
				/**
				 * 第2步：记录资金表
				 */
				BigDecimal cny_can_used_new = cny_can_used.subtract(bid_money_bigdecimal);
				boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used_new).update();
				if (!is_ok_update_user_now_money) {
					error_code.set("请求超时");// 可能是非法操作 or 直接超时提示
					return false;
				}
				/**
				 * 第3步：联富宝 ，已售出金额增加，当前用户购买的定投数量。 同时够买人数增加
				 */
				FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.get_month_fix_bid_by_id(order_id);
				// 若 本次投资金额 大于 剩余可投资的联富宝金额
				if (fix_bid_system_order == null) {
					error_code.set("请求超时");// 可能是非法操作 or 直接超时提示
					return false;
				}

				String bid_name = fix_bid_system_order.getString("bid_name");// 联富宝名称
				int closed_period = fix_bid_system_order.getInt("closed_period");// 封闭期
				int bid_type = 0;
				if (closed_period == 3) {
					bid_type = 1;// 持有联富宝的类型 1 ：联富宝L03；2 ：联富宝L06；3 ： 联富宝L12'
				} else if (closed_period == 6) {
					bid_type = 2;
				} else if (closed_period == 12) {
					bid_type = 3;
				} else {
					error_code.set("联富宝类型出错");// 可能是非法操作 or 直接超时提示
					return false;

				}

				BigDecimal annualized_rate = fix_bid_system_order.getBigDecimal("annualized_rate");// 年化利率
				Date repayment_time = fix_bid_system_order.getTimestamp("repayment_time");// 到期退出定投时间
				Long repayment_time_long = fix_bid_system_order.getLong("repayment_time_long");// 到期退出定投时间Linux时间戳
				Date invite_end_time = fix_bid_system_order.getTimestamp("invite_end_time");// 锁定时间
				Long invite_end_time_long = fix_bid_system_order.getLong("invite_end_time_long");// 锁定时间的Linux时间戳

				int total_money = fix_bid_system_order.getInt("total_money");// 总发售金额
				int sold_money = fix_bid_system_order.getInt("sold_money");// 已售出金额
				int least_money = fix_bid_system_order.getInt("least_money");
				int join_number = fix_bid_system_order.getInt("join_number");// 最终加入人数
				/**
				 * 页面传参least_money漏洞解决办法
				 */
				if (least_money_from_page != least_money) {
					error_code.set("请求超时");// 可能是非法操作 or 直接超时提示
					return false;
				}
				int remain_fix_bid_money = total_money - sold_money;// 剩余的联富宝投资金额
				if (remain_fix_bid_money < bid_money) {
					error_code.set("没有可投资的联富宝");// 可能是非法操作 or 直接超时提示
					return false;
				}

				int sold_money_new = sold_money + bid_money;// 更改后的已售出金额
				int join_number_new = join_number + 1;// 加入人数增加1
				boolean is_ok_update_fix_bid_system_order = fix_bid_system_order.set("sold_money", sold_money_new).set("join_number", join_number_new).update();
				if (!is_ok_update_fix_bid_system_order) {
					error_code.set("请求超时");// 可能是非法操作 or 直接超时提示
					return false;
				}
				/**
				 *第4步： 增加一条联富宝持有信息
				 */
				FixBidUserHoldM fix_bid_user_hold = new FixBidUserHoldM();
				boolean is_ok_add_fix_bid_user_hold = fix_bid_user_hold//
						.set("bid_name", bid_name)//
						.set("bid_type", bid_type)//
						.set("fix_bid_sys_id", order_id)//
						.set("user_id", user_id)//
						.set("nick_name", nick_name)//
						.set("real_name", real_name)//
						.set("annual_earning", annualized_rate)//
						.set("bid_money", bid_money)//
						.set("earned_incom", 0)// 已获收益
						.set("bid_num", 1)// 投标次数
						.set("state", 1)// 0 ： 持有后退出 (要给违约金）；1 ：持有中
						.set("add_time", date)//
						.set("add_time_long", System.currentTimeMillis())//
						.set("exit_time", repayment_time)//
						.set("exit_time_long", repayment_time_long)//
						.set("lock_time", invite_end_time)//
						.set("lock_time_long", invite_end_time_long)//
						.save();//
				if (!is_ok_add_fix_bid_user_hold) {
					error_code.set("请求超时");// 可能是非法操作 or 直接超时提示
					return false;
				}

				/***************************************************
				 *******************添加活动表************************
				 ***************************************************/
				_____Activity___Fix_Bid_WaitingM activity_fix_bid_waiting = new _____Activity___Fix_Bid_WaitingM();
				activity_fix_bid_waiting.set("user_id", user_id);
				activity_fix_bid_waiting.set("hold_id", fix_bid_user_hold.getLong("id"));
				activity_fix_bid_waiting.set("add_time", date);
				activity_fix_bid_waiting.set("add_time_long", System.currentTimeMillis());
				activity_fix_bid_waiting.save();
				/**
				 * 用户资金明细表
				 */
				boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_24, cny_can_used.add(cny_freeze), new BigDecimal(bid_money + ""), new BigDecimal("0"), cny_can_used_new.add(cny_freeze), "联福宝开放投标,ID--->" + fix_bid_user_hold.getLong("id"));
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				/**
				 * 用户交易记录
				 */
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_23, cny_can_used.add(cny_freeze), new BigDecimal(bid_money + ""), new BigDecimal("0"), cny_can_used_new.add(cny_freeze), "联福宝开放投标");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				return true;

			}
		});

		if (is_ok) {
			renderText("12");// 购买买成功
			return;
		} else {
			if (error_code.get() != null) {
				String msg = error_code.get();
				if ("请求超时".equals(msg)) {
					renderText("8");// 请求超时
					return;
				} else if ("资金不足".equals(msg)) {
					renderText("7");
					return;
				} else if ("没有可投资的联富宝".equals(msg)) {
					renderText("9");// 没有可投资的联富宝
					return;
				} else if ("联富宝类型出错".equals(msg)) {
					renderText("10");// 联富宝类型出错
					return;
				} else {
					renderText("11");// 购买失败！
					return;
				}

			}

		}
	}

	/**
	 * 预定联富宝
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "预定联富宝", last_update_author = "hx")
	public void order_lfoll_inveset() {

		final long order_id = getParameterToLong("order_id", 0);// 联富宝ID
		final long deposit = getParameterToInt("deposit", 0);// 应付订金 定金是投资金额的1%
		final int bid_money = getParameterToInt("bid_money", 0);// （预定的金额）整数类型-真正投的部分
		int least_money = getParameterToInt("least_money", 0);// 最少起投金额
		/**
		 * 基本检验
		 */
		if (order_id <= 0) {
			renderText("1");// 联富宝ID出错
			return;
		}
		if (deposit <= 0) {
			renderText("2");// 订金为空
			return;
		}
		if (bid_money <= 0) {
			renderText("3");// 预定的金额
			return;
		}
		if (least_money <= 0) {
			renderText("黑客操作");
			return;
		}
		/**
		 * 判断起投金额是否为最少起投金额的整数倍
		 */
		if (bid_money % least_money != 0) {
			renderText("6");// 不是最少起投金额的倍数
			return;
		}

		UserM user = getSessionAttribute("user");
		final String nick_name = user.getString("nickname");
		final String real_name = user.getString("real_name");
		final int user_id = user.getInt("id");

		final String validate_code = getParameter("validate_code");// 验证码
		final String session_code = getSessionAttribute("code");// session 中的验证码

		if (!Utils.isNotNullAndNotEmptyString(validate_code)) {
			renderText("4");// 验证码不能为空
			return;
		}
		if (!validate_code.equals(session_code)) {
			renderText("5");// 验证码不正确
			return;
		}

		// 查询当前的可用资金
		UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user_id);
		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用RMB

		/**
		 * 判断支付的订金是否足够
		 */
		if (cny_can_used.compareTo(new BigDecimal(deposit + "")) < 0) {
			renderText("7");
			return;// 可用资金不足
		}

		final int least_money_from_page = least_money;// 数据库里面需要进行校验

		// 获取原子事务里的返回状态
		final AtomicReference<String> error_code = new AtomicReference<String>();

		Boolean is_ok = Db.tx(new IAtomic() {
			public boolean transactionProcessing() throws Exception {
				Date now = new Date();
				BigDecimal deposit_decimal = new BigDecimal(deposit + "");
				/**
				 * 第1步对用户可用资金和冻结资金进行加减
				 */
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				if (user_now_money == null) {
					error_code.set("请求超时");// 非法操作 or 请求超时,请重新操作
					return false;
				}
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用RMB
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");// 冻结RMB

				if (cny_can_used.compareTo(deposit_decimal) < 0) {
					error_code.set("请求超时");// 非法操作 or 请求超时,请重新操作
					return false;
				}

				BigDecimal cny_can_used_new = cny_can_used.subtract(deposit_decimal);// 作减后的可用RMB
				boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used_new).update();// 不会出现冻结

				if (!is_ok_update_user_now_money) {
					error_code.set("请求超时");// 非法操作 or 请求超时,请重新操作
					return false;
				}

				/**
				 * 查询联富宝
				 */
				FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.get_month_fix_bid_by_id(order_id);
				if (fix_bid_system_order == null) {
					error_code.set("请求超时");// 非法操作 or 请求超时,请重新操作
					return false;
				}
				final String bid_name = fix_bid_system_order.getString("bid_name");// 联富宝名称
				final Date payment_deadline = fix_bid_system_order.getTimestamp("pay_end_time");// 支付截止时间

				//
				int total_money = fix_bid_system_order.getInt("total_money");
				int sold_money = fix_bid_system_order.getInt("sold_money");
				int least_money = fix_bid_system_order.getInt("least_money");
				int booking_number = fix_bid_system_order.getInt("booking_number");// 预定人次

				/**
				 * 页面传参least_money漏洞解决办法
				 */
				if (least_money_from_page != least_money) {
					error_code.set("请求超时");// 非法操作 or 请求超时,请重新操作
					return false;
				}
				// 剩余出售金额
				BigDecimal remaining_amount = new BigDecimal(total_money + "").subtract(new BigDecimal(sold_money + ""));
				// 如果剩余可订金额 小于 预定的金额 ，则不通过
				if (remaining_amount.compareTo(new BigDecimal(bid_money + "")) < 0) {
					error_code.set("请求超时");// 非法操作 or 请求超时,请重新操作
					return false;
				}
				// 对联富宝的可投资金额 作相减
				int sold_money_new = sold_money + bid_money;//
				int booking_number_new = booking_number + 1;// 更改后的人次
				fix_bid_system_order.set("sold_money", sold_money_new);
				fix_bid_system_order.set("booking_number", booking_number_new);

				boolean is_ok_update_fix_bid_system_order = fix_bid_system_order.update();
				if (!is_ok_update_fix_bid_system_order) {
					return false;
				}

				// 持有定投的份额 = 预定的金额 / 最少起投金额
				final int bid_share = bid_money / least_money;

				// 新建预定FixBidUserOrderM
				FixBidUserOrderM fix_bid_user_order = new FixBidUserOrderM();
				fix_bid_user_order//
						.set("user_id", user_id)//
						.set("bid_name", bid_name)// 联富宝名称
						.set("fix_bid_sys_id", order_id)// fix_bid_system_order主键
						.set("user_id", user_id)// 对应用户表ID
						.set("real_name", real_name)// 真实姓名
						.set("nick_name", nick_name)// 用户昵称
						.set("bid_money", bid_money)// 预订的投资金额-int
						.set("bid_share", bid_share)// 预订的份额
						.set("already_pay_bond", deposit)// 订金
						.set("wait_pay_amount", bid_money - deposit)// 待付金额
						.set("payment_deadline", payment_deadline)// 支付截止时间
						.set("is_pay", 0)// 0 : 未支付；1：已支付
						.set("add_time", now)// 预约时间
						.set("add_time_long", now.getTime());
				boolean is_ok_add_fix_bid_user_order = fix_bid_user_order.save();
				if (!is_ok_add_fix_bid_user_order) {
					return false;
				}
				/**
				 * 添加财务记录user_money_change_records
				 */
				boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_21, cny_can_used.add(cny_freeze), deposit_decimal, new BigDecimal("0"), cny_can_used_new.add(cny_freeze), "预定支出-->联富宝ID：" + fix_bid_user_order.getLong("id"));
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				/**
				 * 添加用户交易记录表user_transaction_records
				 */
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_23, cny_can_used.add(cny_freeze), deposit_decimal, new BigDecimal("0"), cny_can_used_new.add(cny_freeze), "联富宝预定支付");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				return true;
			}
		});

		if (is_ok) {
			renderText("9");// 预定成功
			return;
		} else {
			if (error_code.get() != null) {
				String msg = error_code.get();
				if ("请求超时".equals(msg)) {
					renderText("10");// 请求超时
					return;
				}

			}
			renderText("11");// 预定失败!
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "查询所有人预定的联富宝集合", last_update_author = "hx")
	public void get_fix_bid_order_list_async() {

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "查询所有人预定的联富宝集合", last_update_author = "hx")
	public void validate_user_is_logined() {
		renderText("1");
		return;
	}

	/**
	 * 新手标投资
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "投资新手标", last_update_author = "hx")
	public void new_users_fix_bid() {
		String validate_code = getParameter("validate_code");
		String sessiom_code = getSessionAttribute("code");
		if (Utils.isNullOrEmptyString(validate_code)) {
			renderText("1");// 验证码为空
			return;
		}
		if (Utils.isNullOrEmptyString(sessiom_code)) {
			renderText("2");// Session验证码为空
			return;
		}
		if (!validate_code.equals(sessiom_code)) {
			renderText("3");// 验证码不相同
			return;
		}

		UserM userM = getSessionAttribute("user");
		final String real_name = userM.getString("real_name");
		final int user_id = userM.getInt("id");

		String find_first_sql = "select * from fix_bid_user_newer_bid tt where tt.user_id = ?";
		FixBidUserNewerBidM fix_bid_user_new_bid = FixBidUserNewerBidM.dao.findFirst(find_first_sql, new Object[] { user_id });
		if (fix_bid_user_new_bid != null) {
			renderText("4");// 您已经购买了新手体验
			return;
		}
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				FixBidUserNewerBidM fix_bid_user_newer_bid = new FixBidUserNewerBidM();
				fix_bid_user_newer_bid.set("user_name", real_name);
				fix_bid_user_newer_bid.set("user_id", user_id);
				fix_bid_user_newer_bid.set("bid_money", 10000);
				fix_bid_user_newer_bid.set("annual_rate", new BigDecimal("0.12"));
				fix_bid_user_newer_bid.set("add_time", new Date());
				fix_bid_user_newer_bid.set("add_time_ux", System.currentTimeMillis());
				fix_bid_user_newer_bid.set("days", 3);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 3);
				Date end_time = calendar.getTime();
				fix_bid_user_newer_bid.set("end_time", end_time);
				fix_bid_user_newer_bid.set("end_time_ux", end_time.getTime());
				fix_bid_user_newer_bid.set("is_end", 0);
				fix_bid_user_newer_bid.set("earnings", new BigDecimal("0"));
				boolean bool1 = fix_bid_user_newer_bid.save();

				/**
				 * 添加到联富宝临时表
				 */
				FixBidUserNewerBidWaitingM fix_bid_newer_bid_waiting = new FixBidUserNewerBidWaitingM();
				long user_newer_bid_id = fix_bid_user_newer_bid.getLong("id");
				String recieve_date = FixBidUserNewerBidWaitingM.Date.format(end_time);
				fix_bid_newer_bid_waiting.set("user_newer_bid_id", user_newer_bid_id);
				fix_bid_newer_bid_waiting.set("recieve_date", recieve_date);
				boolean bool2 = fix_bid_newer_bid_waiting.save();

				if (bool1 && bool2) {
					return true;
				} else {
					return false;
				}

			}
		});
		if (is_ok) {
			renderText("5");// 投标成功
			return;
		} else {
			renderText("6");// 新手标失败
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "跳转到联富宝新手详情页面", last_update_author = "hx")
	public void goto_new_fix_bid_detail() {
		renderJsp("/user/lfoll_invest/lfoll_invest_new_bid_detail.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "跳转到联富宝新手详情页面", last_update_author = "hx")
	public void validate_user_isnot_fix_bid() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		String sql = "select * from fix_bid_user_newer_bid tt where tt.user_id = ?";
		FixBidUserNewerBidM fix_bid_user_newer_bid = FixBidUserNewerBidM.dao.findFirst(sql, new Object[] { user_id });
		if (fix_bid_user_newer_bid != null) {
			renderText("1");// 已经投资新手标
			return;
		} else {
			renderText("2");// 没有投资新手标
			return;
		}
	}

	public static void main(String[] args) throws Exception {
		String select_list_sql = "SELECT * from fix_bid_system_order WHERE is_show = 1 AND closed_period = 3 ORDER BY id DESC";
		ConsoleInit.initTables();
		System.out.println(Db.find(select_list_sql));
	}
}
