package com.tfoll.web.action.user.authentication;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.action.user.financial.BorrowAction;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateFamilyInfoM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebLogRecordsUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ActionKey("/user/authentication/family_info")
public class FamilyInfoAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "家庭信息认证页面", last_update_author = "zjb")
	public void family_index() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");
		int borrow_type = user.getInt("borrow_type");
		int rate = UserAuthenticateLeftStatusM.get_rate(user_id);

		setAttribute("rate", rate);
		setAttribute("the_id", "family");
		setAttribute("borrow_type", borrow_type + "");

		// 判断是否有状态为 1 的订单
		String apply_order_id_string = BorrowAction.get_value_from_request_and_session_of_apply_order_id();
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}

		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		// 单子状态和关联保存的申请单ID
		setAttribute("apply_order_id", apply_order_id);
		setAttribute("apply_order_state", state);// 如果是2则要提示在多久后系统会自动提交这个单子

		UserAuthenticateFamilyInfoM user_authenticate_family_info = UserAuthenticateFamilyInfoM.dao.findFirst("select * from user_authenticate_family_info where user_id = ? ", user_id);
		setAttribute("user_authenticate_family_info", user_authenticate_family_info);

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		setAttribute("user_authenticate_left_status", user_authenticate_left_status);

		renderJsp("/user/authenticate/family.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "家庭信息认证页面", last_update_author = "zjb")
	public void save_family_info() throws Exception {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		// 判断是否有状态为 1 的订单
		long apply_order_id = getParameterToInt("apply_order_id", 0);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		String marriage = getParameter("marriage", null);
		String children = getParameter("children", null);

		String lineal_name = getParameter("lineal_name", null);
		String lineal_relation = getParameter("lineal_relation", null);
		String lineal_phone = getParameter("lineal_phone", null);

		String other_name = getParameter("other_name", null);
		String other_relation = getParameter("other_relation", null);
		String other_phone = getParameter("other_phone", null);

		String other_name_two = getParameter("other_name_two", null);
		String other_relation_two = getParameter("other_relation_two", null);
		String other_phone_two = getParameter("other_phone_two", null);

		String[] arr = { marriage, children, lineal_name, lineal_relation, lineal_phone, other_name, other_relation, other_phone, other_name_two, other_relation_two, other_phone_two };
		String[] arr2 = { "marriage", "children", "lineal_relatives_name", "lineal_relatives_relation", "lineal_relatives_phone", "other_contacts_name", "other_contacts_relation", "other_contacts_phone", "other_contacts_name_two", "other_contacts_relation_two", "other_contacts_phone_two" };

		for (int i = 2; i < arr.length; i++) {
			if (!Utils.isNotNullAndNotEmptyString(arr[i])) {
				renderText(String.valueOf(i));
				return;
			}
		}

		// 名字验证
		String name = "^([\u4e00-\u9fa5]{2,7})$";
		Pattern pat = Pattern.compile(name);
		for (int i = 2; i < arr.length - 2; i += 3) {
			Matcher mat = pat.matcher(arr[i]);
			boolean rs = mat.find();
			if (!rs) {
				renderText(i + "1");
				return;
			}
		}

		// 关系验证
		String relation = "^([\u4e00-\u9fa5]){2,7}$";
		pat = Pattern.compile(relation);
		for (int i = 3; i < arr.length - 1; i += 3) {
			Matcher mat = pat.matcher(arr[i]);
			boolean rs = mat.find();
			if (!rs) {
				renderText(i + "1");
				return;
			}
		}

		// 手机验证
		String phone = "^1[3|4|5|8|]\\d{9}$";
		pat = Pattern.compile(phone);
		for (int i = 4; i < arr.length; i += 3) {
			Matcher mat = pat.matcher(arr[i]);
			boolean rs = mat.find();
			if (!rs) {
				renderText(i + "1");
				return;
			}
		}

		UserAuthenticateFamilyInfoM user_authenticate_family_info = UserAuthenticateFamilyInfoM.dao.findFirst("select * from user_authenticate_family_info where user_id = ? ", user_id);
		for (int i = 0; i < arr.length; i++) {
			user_authenticate_family_info.set(arr2[i], arr[i]);
			renderText(String.valueOf(i));
		}

		boolean ok = user_authenticate_family_info.update();

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		int family_info_status = 1;
		boolean left_status_ok = user_authenticate_left_status.set("family_info_status", family_info_status).update();

		if (ok && left_status_ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}
	}
}
