package com.tfoll.web.action.user.authentication;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.upload.CosUpload;
import com.tfoll.trade.upload.UploadFile;
import com.tfoll.web.action.user.financial.BorrowAction;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.GetTypeByHead;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebApp;
import com.tfoll.web.util.WebLogRecordsUtil;
import com.tfoll.web.util.GetTypeByHead.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

@ActionKey("/user/authentication/upload_info")
public class UploadInfoAction extends Controller {

	public static final Gson Gson = new Gson();

	public static class IndexUrl {
		public int getIndex() {
			return index;
		}

		public String getUrl() {
			return url;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public IndexUrl() {
		};

		private int index;// 索引
		private String url;// 地址
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "跳转到文件上传页面", last_update_author = "zjb")
	public void upload_info_index() throws Exception {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		int borrow_type = user.getInt("borrow_type");
		int rate = UserAuthenticateLeftStatusM.get_rate(user_id);

		setAttribute("rate", rate);
		setAttribute("the_id", "upload");
		setAttribute("borrow_type", borrow_type + "");

		// 判断是否有状态为 1 的订单
		String apply_order_id_string = BorrowAction.get_value_from_request_and_session_of_apply_order_id();
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}

		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		// 单子状态和关联保存的申请单ID
		setAttribute("apply_order_id", apply_order_id);
		setAttribute("apply_order_state", state);// 如果是2则要提示在多久后系统会自动提交这个单子

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		setAttribute("user_authenticate_left_status", user_authenticate_left_status);

		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findFirst("select * from user_authenticate_upload_info where user_id =  ?", user_id);// 外键做主键
		setAttribute("user_authenticate_upload_info", user_authenticate_upload_info);

		setAttribute("borrower_bulk_standard_apply_order", borrower_bulk_standard_apply_order);

		renderJsp("/user/authenticate/upload.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "文件上传", last_update_author = "zjb")
	public void upload_info() throws Exception {

		// 获得上传文件
		// 获得数据，创建目录，再上传文件（创建顺序：日期/用户id/类别）
		// 写库

		CosUpload cosupload = new CosUpload(getRequest(), null, new String[] { ".jpg", ".jpeg", ".bmp", ".png" }, new String[] { "name", "apply_order_id" });
		List<UploadFile> upload_file_list = cosupload.getFiles();

		String attribute[] = { "id_authenticate", "work_authenticate", "credit_authenticate", "income_authenticate", "housing_authenticate", "car_authenticate", "marriage_authenticate", "education_authenticate", "title_authenticate", "living_authenticate" };

		if (!Utils.isHasData(upload_file_list)) {
			cosupload.deleteFiles();
			renderText("no");// 上传不成功
			return;
		}
		UploadFile upload_file = upload_file_list.get(0);
		File file = upload_file.getFile();

		if (file.length() > 1572864) {
			cosupload.deleteFiles();
			renderText("out");
			return;
		}

		int type = Integer.parseInt(cosupload.getFieldMap().get("name"));

		// 通过文件头信息，获得类型
		String file_type = null;
		try {
			file_type = GetTypeByHead.getFileType(file);
		} catch (Exception e) {
			renderText("no");
		}
		if (file_type == null) {
			cosupload.deleteFiles();
			renderText("no");// 不是文件，或 文件格式不符合
			return;
		}
		if (Image.Image_Type_Set.contains(file_type)) {
			String image_name = GetTypeByHead.Image.getImageFormatName(file);
			if (image_name == null) {
				cosupload.deleteFiles();
				renderText("no");// 不是图片
				return;
			}
		} else {
			cosupload.deleteFiles();
			renderText("er");
			return;
		}

		// 目录
		String now = Model.Date.format(new Date());

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		// 判断是否有状态为 1 的订单
		long apply_order_id = Long.parseLong(cosupload.getFieldMap().get("apply_order_id"));
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			cosupload.deleteFiles();
			renderText("");// 非法操作
			return;
		}
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			cosupload.deleteFiles();
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			cosupload.deleteFiles();
			renderText("");
			return;
		}

		/**
		 * 文件夹
		 */
		String path = WebApp.Files_Stored_Palce + WebApp.Windows_File_Separator + now + WebApp.Windows_File_Separator + user_id + WebApp.Windows_File_Separator + attribute[type];

		File f = new File(path);
		if (!f.exists()) {
			f.mkdirs();
		}

		File fol = new File(f, attribute[type] + '.' + file_type);
		fol.createNewFile();

		// file.renameTo(fol);
		FileUtils.copyFile(file, fol);
		// File f = new File(file,path);

		// FileInputStream fis = new FileInputStream(file);
		String absolutepath = fol.getAbsolutePath();

		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);// 外键做主键

		String url = user_authenticate_upload_info.get(attribute[type] + "_url");
		List<IndexUrl> index_url_list = null;
		if (Utils.isNotNullAndNotEmptyString(url)) {
			index_url_list = gson.fromJson(url, new TypeToken<List<IndexUrl>>() {
			}.getType());
			if (index_url_list.size() > 16) {
				renderText("over");
				cosupload.deleteFiles();
				return;
			}
			UploadInfoAction.IndexUrl index_url = new UploadInfoAction.IndexUrl();
			index_url.setIndex(index_url_list.size() + 1);
			index_url.setUrl(absolutepath);
			index_url_list.add(index_url);
		} else {
			index_url_list = new ArrayList<IndexUrl>();
			UploadInfoAction.IndexUrl index_url = new UploadInfoAction.IndexUrl();
			index_url.setIndex(1);
			index_url.setUrl(absolutepath);
			index_url_list.add(index_url);
		}

		url = gson.toJson(index_url_list);

		// boolean
		// ok=user_authenticate_upload_info.set(attribute[type],fis).set(attribute[type]+"_url",absolutepath).update();
		boolean ok = user_authenticate_upload_info.set(attribute[type] + "_url", url).update();

		// 必要上传认证信息
		String id_authenticate_url = user_authenticate_upload_info.getString("id_authenticate_url");
		String work_authenticate_url = user_authenticate_upload_info.getString("work_authenticate_url");
		String credit_authenticate_url = user_authenticate_upload_info.getString("credit_authenticate_url");
		String income_authenticate_url = user_authenticate_upload_info.getString("income_authenticate_url");

		// fis.close();
		cosupload.deleteFiles();
		// 判断 必要上传认证信息 的url地址是否存在，若存在更改左框状态表 上传信息状态
		if (Utils.isNotNullAndNotEmptyString(id_authenticate_url) && Utils.isNotNullAndNotEmptyString(work_authenticate_url) && Utils.isNotNullAndNotEmptyString(credit_authenticate_url) && Utils.isNotNullAndNotEmptyString(income_authenticate_url)) {
			UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
			int upload_info_status = 1;
			user_authenticate_left_status.set("upload_info_status", upload_info_status).update();
			renderText("ok");
			return;
		}
		if (ok) {
			renderText(type + ""); // 上传成功
			return;
		} else {
			renderText("er");
			return;
		}

	}

}
