package com.tfoll.web.action.user.authentication;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.action.user.financial.BorrowAction;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConfig;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserAuthenticateWorkInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebLogRecordsUtil;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ActionKey("/user/authentication/work_info")
public class WorkInfoAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "跳转到工作信息认证页面", last_update_author = "zjb")
	public void work_index() throws Exception {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		int borrow_type = user.getInt("borrow_type");
		int rate = UserAuthenticateLeftStatusM.get_rate(user_id);

		setAttribute("rate", rate);
		setAttribute("the_id", "work");
		setAttribute("borrow_type", borrow_type + "");

		// 判断是否有状态为 1 的订单
		String apply_order_id_string = BorrowAction.get_value_from_request_and_session_of_apply_order_id();
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}

		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		// 单子状态和关联保存的申请单ID
		setAttribute("apply_order_id", apply_order_id);
		setAttribute("apply_order_state", state);// 如果是2则要提示在多久后系统会自动提交这个单子
		if (state == 2) {
			Date commit_time = borrower_bulk_standard_apply_order.getDate("commit_time");
			if (commit_time == null) {
				throw new NullPointerException("commit_time is null");
			}
			long commit_time_next_long = commit_time.getTime() + Day * 3;
			Date commit_time_next = new Date(commit_time_next_long);
			setAttribute("commit_time_next", Time.format(commit_time_next));// 如果是2则要提示在多久后系统会自动提交这个单子
		}

		UserAuthenticateWorkInfoM user_authenticate_work_info = UserAuthenticateWorkInfoM.dao.findFirst("select * from user_authenticate_work_info where user_id = ?", user_id);
		setAttribute("work_info_list", user_authenticate_work_info);

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		setAttribute("user_authenticate_left_status", user_authenticate_left_status);

		setAttribute("work_province_list", SystemConfig.Sys_Province_List);
		renderJsp("/user/authenticate/work.jsp");

		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "工作信息认证", last_update_author = "zjb")
	public void save_work_info() throws Exception {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		// 判断是否有状态为 1 的订单
		long apply_order_id = getParameterToInt("apply_order_id", 0);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		String company_name = getParameter("company_name");
		String position = getParameter("position");
		String monthly_income = getParameter("monthly_income");
		String work_email = getParameter("work_email");
		String work_province = getParameter("work_province");

		String work_city = getParameter("work_city");
		String company_address = getParameter("company_address");
		String company_type = getParameter("company_type");
		String company_trades = getParameter("company_trades");
		String company_size = getParameter("company_size");

		String occupation_status = getParameter("occupation_status");
		String year_limit = getParameter("year_limit");
		String company_phone = getParameter("company_phone");

		String arr[] = { company_name, position, monthly_income, work_email, work_province, work_city, company_address, company_type, company_trades, company_size, year_limit, company_phone, occupation_status };
		String arr2[] = { "company_name", "position", "monthly_income", "work_email", "work_province", "work_city", "company_address", "company_type", "company_trades", "company_size", "year_limit", "company_phone", "occupation_status" };

		for (int i = 0; i < arr.length; i++) {
			if (!Utils.isNotNullAndNotEmptyString(arr[i])) {
				renderText(String.valueOf(i));
				return;
			}
		}
		// 邮箱验证
		Pattern pat = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.([a-zA-Z0-9_-])+)+$");
		Matcher mat = pat.matcher(work_email);
		if (!mat.matches()) {
			renderText("301");
			return;
		}

		// 居住验证
		String addr = "^(?=.*?[\\u4E00-\\u9FA5])[\\dA-Za-z\\u4E00-\\u9FA5]+$";
		pat = Pattern.compile(addr);
		mat = pat.matcher(company_address);
		if (!mat.matches()) {
			renderText("601");
			return;
		}

		// 居住电话验证
		if (Utils.isNotNullAndNotEmptyString(company_phone)) {
			String tel = "^\\d{7,12}$";
			pat = Pattern.compile(tel);
			mat = pat.matcher(company_phone);
			// boolean rs = mat.find();
			if (!mat.find()) {
				renderText("111");
				return;
			}
		}

		UserAuthenticateWorkInfoM user_authenticate_work_info = UserAuthenticateWorkInfoM.dao.findFirst("select * from user_authenticate_work_info where user_id = ?", user_id);
		for (int i = 0; i < arr.length; i++) {
			user_authenticate_work_info.set(arr2[i], arr[i]);
		}
		boolean ok = user_authenticate_work_info.update();

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		int work_info_status = 1;
		boolean left_status_ok = user_authenticate_left_status.set("work_info_status", work_info_status).update();

		if (ok && left_status_ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}
	}

}
