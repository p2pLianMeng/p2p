package com.tfoll.web.action.user.financial;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.LenderAop;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.model.FixBidUserExitM;
import com.tfoll.web.model.FixBidUserHoldM;
import com.tfoll.web.model.FixBidUserNewerBidM;
import com.tfoll.web.model.FixBidUserOrderM;
import com.tfoll.web.model.SysExpensesRecordsM;
import com.tfoll.web.model.SysIncomeRecordsM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.model._____Activity___Fix_Bid_WaitingM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.JsonMsgObject;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ActionKey("/user/financial/lfollinvest_user_info")
public class LfollInvestUserInfoAction extends Controller {

	/**
	 * 查询用户联富宝查询页面
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "用户联富宝管理页面", last_update_author = "hx")
	public void goto_user_fix_bid_manage() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		/**
		 * 查询持有中 联富宝 数量
		 */
		String user_fix_bid_hold_count_sql = "SELECT count(1) from fix_bid_user_hold t1 where t1.user_id = ? and t1.state = 1 ORDER BY t1.id DESC";
		long hold_num = Db.queryLong(user_fix_bid_hold_count_sql, new Object[] { user_id });
		/**
		 * 查询新手标持有的 数量
		 */
		String new_fix_bid_hold_count_sql = "select count(1) from fix_bid_user_newer_bid where user_id = ? and is_end = 0";
		long new_fix_bid_hold_num = Db.queryLong(new_fix_bid_hold_count_sql, new Object[] { user_id });

		setAttribute("hold_num", hold_num + new_fix_bid_hold_num);

		/******************************************
		 * 查询退出中的联富宝数量
		 ******************************************/
		String fix_bid_exit_num_sql = "select count(1) as num from fix_bid_user_hold t1 where t1.user_id = ? and t1.state = 0";

		long exit_num = Db.queryLong(fix_bid_exit_num_sql, new Object[] { user_id });

		String new_fix_bid_exit_count_sql = "select count(1) from fix_bid_user_newer_bid where user_id = ? and is_end = 1";
		long new_fix_bid_exit_num = Db.queryLong(new_fix_bid_exit_count_sql, new Object[] { user_id });
		setAttribute("exit_num", exit_num + new_fix_bid_exit_num);

		/**
		 * 查询预定中的人数
		 **/
		Date date = new Date();
		String fix_bid_order_num_sql = "SELECT COUNT(1) FROM fix_bid_user_order tt, fix_bid_system_order uu WHERE tt.user_id = ? AND tt.fix_bid_sys_id = uu.id AND uu.reserve_start_time <= ? AND uu.pay_end_time >= ? AND uu.is_show = 1 ORDER BY tt.id DESC";
		Long fix_bid_order_num = Db.queryLong(fix_bid_order_num_sql, new Object[] { user_id, date, date });
		if (fix_bid_order_num == null) {
			fix_bid_order_num = 0l;
			setAttribute("fix_bid_order_num", fix_bid_order_num);
		} else {
			setAttribute("fix_bid_order_num", fix_bid_order_num);
		}

		/**
		 * 查询当前用户联富宝加入金额
		 * 
		 */
		String join_money_sql = "select sum(tt.bid_money) from fix_bid_user_hold tt where tt.user_id = ? AND tt.state = 1 ";
		BigDecimal join_money = Db.queryBigDecimal(join_money_sql, new Object[] { user_id });
		setAttribute("join_money", join_money);
		/**
		 * 查询联富宝已赚金额
		 */

		String fix_bid_in_come_sql = "select ifnull(sum(tt.earned_incom),0)from fix_bid_user_hold tt where tt.state != 1 AND tt.user_id = ? ";
		BigDecimal fix_bid_in_come = Db.queryBigDecimal(fix_bid_in_come_sql, new Object[] { user_id });
		/**
		 * 查询新手标的已赚金额
		 */
		String new_fix_bid_in_come_sql = "SELECT ifnull(SUM(tt.earnings),0) FROM fix_bid_user_newer_bid tt WHERE tt.user_id = ?";
		BigDecimal new_fix_bid_in_come = Db.queryBigDecimal(new_fix_bid_in_come_sql, new Object[] { user_id });
		/**
		 * 联富宝总的已赚金额
		 */
		BigDecimal totail_in_come = fix_bid_in_come.add(new_fix_bid_in_come).setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("totail_in_come", totail_in_come);

		renderJsp("/user/usercenter/user_lfoll_fix_bid.jsp");// 跳转到用户联富宝信息列表的页面
		return;
	}

	/**
	 * 查询用户预定的联富宝信息
	 * 
	 * @throws ParseException
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "查询当前用户的联富宝列表信息", last_update_author = "hx")
	public void get_user_fix_bid_order_list() throws ParseException {
		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");

		// String fix_bid_user_order_list_sql =
		// "SELECT tt.id AS id, tt.bid_name AS bid_name, tt.bid_money AS bid_money, tt.already_pay_bond AS already_pay_bond, tt.wait_pay_amount AS wait_pay_amount, tt.payment_deadline AS payment_deadline, tt.is_pay AS is_pay, tt.nick_name AS nick_name, uu.annualized_rate AS annualized_rate, uu.closed_period AS closed_period FROM fix_bid_user_order tt, fix_bid_system_order uu WHERE tt.user_id = ? AND tt.fix_bid_sys_id = uu.id ORDER BY tt.id DESC";
		Date date = new Date();
		String fix_bid_user_order_list_sql = "SELECT tt.id AS id, tt.bid_name AS bid_name, tt.bid_money AS bid_money, tt.already_pay_bond AS already_pay_bond, tt.wait_pay_amount AS wait_pay_amount, tt.payment_deadline AS payment_deadline, tt.is_pay AS is_pay, tt.nick_name AS nick_name, uu.annualized_rate AS annualized_rate, uu.closed_period AS closed_period FROM fix_bid_user_order tt, fix_bid_system_order uu WHERE tt.user_id = ? AND tt.fix_bid_sys_id = uu.id AND uu.reserve_start_time <= ? AND uu.pay_end_time >= ? AND uu.is_show = 1 ORDER BY tt.id DESC ";
		List<Record> fix_bid_user_order_list = Db.find(fix_bid_user_order_list_sql, new Object[] { user_id, date, date });

		UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user_id);
		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used").setScale(2, BigDecimal.ROUND_DOWN);// 可用的CNY

		if (Utils.isHasData(fix_bid_user_order_list)) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			List<JsonMsgObject> json_msg_object_list = new ArrayList<JsonMsgObject>();
			for (Record fix_bid_user_order : fix_bid_user_order_list) {
				JsonMsgObject json_msg_object = new JsonMsgObject();
				Long id = fix_bid_user_order.getLong("id");// 主键
				String bid_name = fix_bid_user_order.getString("bid_name");// 联富宝名称
				int bid_money = fix_bid_user_order.getInt("bid_money");// 加入金额
				int already_pay_bond = fix_bid_user_order.getInt("already_pay_bond");// 已付定金
				int wait_pay_amount = fix_bid_user_order.getInt("wait_pay_amount");// 待付金额
				int is_pay = fix_bid_user_order.getInt("is_pay");// 0 :
				// 未支付；1：已支付
				Date payment_deadline = fix_bid_user_order.getTimestamp("payment_deadline");
				String payment_deadline_str = format.format(payment_deadline);
				String nick_name = fix_bid_user_order.getString("nick_name");// 昵称
				BigDecimal annualized_rate = fix_bid_user_order.getBigDecimal("annualized_rate");// 年化利率
				BigDecimal annualized_rate_real = annualized_rate.multiply(new BigDecimal("100"));// 乘以100后的年化利率
				int closed_period = fix_bid_user_order.getInt("closed_period");// 封闭期

				json_msg_object.setObject_1(id);
				json_msg_object.setObject_2(bid_name);
				json_msg_object.setObject_3(bid_money);
				json_msg_object.setObject_4(already_pay_bond);
				json_msg_object.setObject_5(wait_pay_amount);
				json_msg_object.setObject_6(payment_deadline_str);
				json_msg_object.setObject_7(is_pay);
				json_msg_object.setObject_8(nick_name);
				json_msg_object.setObject_9(annualized_rate_real);
				json_msg_object.setObject_10(closed_period);
				json_msg_object.setObject_11(cny_can_used);
				json_msg_object_list.add(json_msg_object);
			}
			Gson gson = new Gson();

			renderText(gson.toJson(json_msg_object_list));
		} else {
			renderText("");// 无数据
		}

		return;

	}

	/**
	 * 查询用户已退出的联富宝 集合
	 * 
	 * @throws ParseException
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "查询当前用户的已退出的联富宝 列表信息", last_update_author = "hx")
	public void get_user_fix_bid_exited_list() {
		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");
		String fix_bid_user_exited_list_sql = "SELECT tt.id AS id, tt.bid_name AS bid_name, tt.bid_money AS bid_money, tt.earned_incom AS earned_incom, tt.annual_earning AS annual_earning, tt.state AS state, tt.user_exit_time AS user_exit_time FROM fix_bid_user_exit tt WHERE tt.user_id = ? ORDER BY tt.id DESC";
		List<FixBidUserExitM> fix_bid_user_exit_list = FixBidUserExitM.dao.find(fix_bid_user_exited_list_sql, new Object[] { user_id });
		List<JsonMsgObject> json_msg_object_list = new ArrayList<JsonMsgObject>();
		if (Utils.isHasData(fix_bid_user_exit_list)) {
			/**
			 * 联富宝 非新手标退出
			 */
			for (FixBidUserExitM fix_bid_user_exit : fix_bid_user_exit_list) {
				JsonMsgObject json_msg_object = new JsonMsgObject();
				long id = fix_bid_user_exit.getLong("id");// 主键
				String bid_name = fix_bid_user_exit.getString("bid_name");// 联富宝名称
				int bid_money = fix_bid_user_exit.getInt("bid_money");// 加入金额
				BigDecimal earned_incom = fix_bid_user_exit.getBigDecimal("earned_incom").setScale(2, BigDecimal.ROUND_DOWN);// 已收益
				BigDecimal annual_earning = fix_bid_user_exit.getBigDecimal("annual_earning");// 年化利率
				BigDecimal annual_earning_real = annual_earning.multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_DOWN);// 乘以100后的年化利率

				int state = fix_bid_user_exit.getInt("state");// 1 正常退出 ； 2
				// 提前退出'
				Date user_exit_time = fix_bid_user_exit.getTimestamp("user_exit_time");// 退出时间
				String exit_time_str = "";
				if (user_exit_time != null) {
					exit_time_str = Model.Time.format(user_exit_time);
				}

				json_msg_object.setObject_1(id);
				json_msg_object.setObject_2(bid_name);
				json_msg_object.setObject_3(bid_money);
				json_msg_object.setObject_4(earned_incom);
				json_msg_object.setObject_5(annual_earning_real + "%");
				json_msg_object.setObject_6(state);// 1 为到期退出 ，2 提前退出
				json_msg_object.setObject_7(exit_time_str);
				json_msg_object_list.add(json_msg_object);
			}

		} else {
			renderText("");
		}
		/**
		 * 联富宝新手标退出
		 */
		// 0 没有结束； 1 已经结束
		String find_fix_bid_newer_exited_sql = "select * from fix_bid_user_newer_bid where is_end = 1 and user_id = ?";
		FixBidUserNewerBidM fix_bid_user_newer_bid = FixBidUserNewerBidM.dao.findFirst(find_fix_bid_newer_exited_sql, new Object[] { user_id });
		if (fix_bid_user_newer_bid != null) {
			JsonMsgObject json_msg_object_new_bid = new JsonMsgObject();
			json_msg_object_new_bid.setObject_1(fix_bid_user_newer_bid.getLong("id"));
			json_msg_object_new_bid.setObject_2("联富宝新手标");
			json_msg_object_new_bid.setObject_3(fix_bid_user_newer_bid.getInt("bid_money"));
			json_msg_object_new_bid.setObject_4(fix_bid_user_newer_bid.getBigDecimal("earnings"));
			BigDecimal rate = fix_bid_user_newer_bid.getBigDecimal("annual_rate").multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_DOWN);
			json_msg_object_new_bid.setObject_5(rate + "%");
			json_msg_object_new_bid.setObject_6(1);// 1 为到期退出 ，2 提前退出
			Date end_time = fix_bid_user_newer_bid.getTimestamp("end_time");// 退出时间
			String end_time_str = "";
			if (end_time != null) {
				end_time_str = Model.Time.format(end_time);
			}
			json_msg_object_new_bid.setObject_7(end_time_str);
			json_msg_object_list.add(json_msg_object_new_bid);
		}

		Gson gson = new Gson();
		renderText(gson.toJson(json_msg_object_list));
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "用户支付联富宝剩余待支付的", last_update_author = "hx")
	public void user_pay_fix_bid_order_waitpaycny() {
		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");

		final long order_id = getParameterToLong("order_id", 0);// 用户预定的联富宝表的ID
		String bid_name = getParameter("bid_name");
		String nick_name = getParameter("nick_name");
		String rate = getParameter("rate");
		String close_period = getParameter("close_period");
		String wait_pay_amount_str = getParameter("wait_pay_amount");// 待付金额
		Integer wait_pay_amount_param = Integer.parseInt(wait_pay_amount_str);

		String can_used_cny = getParameter("can_used_cny");// 可用CNY
		BigDecimal can_used_cny_decimal = new BigDecimal(can_used_cny);

		String verificte_code = getParameter("verificte_code");// 验证码
		String session_code = getSessionAttribute("code");// SESSION 中的CODE

		if (order_id == 0) {
			renderText("1");// 订单ID为空
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(bid_name)) {
			renderText("2");// 联富宝名称为空
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(nick_name)) {
			renderText("3");// 昵称
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(rate)) {
			renderText("4");// 年化利率为空
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(close_period)) {
			renderText("5");// 联富宝期限为空
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(can_used_cny)) {
			renderText("6");// 可用的RMB为空
			return;
		}
		//
		if (!Utils.isNotNullAndNotEmptyString(verificte_code)) {
			renderText("7");// 验证码为空 请输入验证码
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(session_code)) {
			renderText("8");// Session中的验证码为空
			return;
		}
		if (!verificte_code.equals(session_code)) {
			renderText("9");// 验证码不正确
			return;
		}
		/**
		 * 可以让别人支付-不存在安全问题
		 */
		String fix_bid_user_order_sql = "SELECT t1.id AS id, t1.bid_name AS bid_name, t1.fix_bid_sys_id AS fix_bid_sys_id, t1.user_id AS user_id, t1.real_name AS real_name, t1.nick_name AS nick_name, t1.bid_money AS bid_money, t1.bid_share AS bid_share, t1.already_pay_bond AS already_pay_bond, t1.wait_pay_amount AS wait_pay_amount, t1.payment_deadline AS payment_deadline, t1.is_pay AS is_pay, t1.add_time AS add_time, t2.annualized_rate AS rate, t2.invite_end_time AS invite_end_time, t2.invite_end_time_long AS invite_end_time_long, t2.repayment_time AS repayment_time, t2.repayment_time_long AS repayment_time_long FROM fix_bid_user_order t1, fix_bid_system_order t2 WHERE t1.id = ? AND t1.fix_bid_sys_id = t2.id";

		final Record record = Db.findFirst(fix_bid_user_order_sql, new Object[] { order_id });
		if (record == null) {
			renderText("");
			return;
		}
		final String bid_name_old = record.getString("bid_name");// 联富宝名称
		int type = 0;
		// 联富宝L03-141103期
		if (bid_name_old.contains("联富宝L03")) {
			type = 1;

		} else if (bid_name_old.contains("联富宝L06")) {
			type = 2;

		} else if (bid_name_old.contains("联富宝L12")) {
			type = 3;
		} else {
			renderText("16");// 联富宝名称有问题
			return;
		}
		final int bid_type = type;// 用户持有的联富宝的类型 1 ：联富宝L03；2 ：联富宝L06；3 ： 联富宝L12
		final Long fix_bid_sys_id = record.getLong("fix_bid_sys_id");// (fix_bid_system_order主键)
		final int user_id_old = record.getInt("user_id");//
		final String real_name_old = record.getString("real_name");
		final String nick_name_old = record.getString("nick_name");
		final int bid_money_old = record.getInt("bid_money");// 投资金额
		@SuppressWarnings("unused")
		final int bid_share_old = record.getInt("bid_share");// 持有份额
		final int already_pay_bond_old = record.getInt("already_pay_bond");// 已付定金
		final int wait_pay_amount_old = record.getInt("wait_pay_amount");// 待付定金
		@SuppressWarnings("unused")
		final Date payment_deadline_old = record.getTimestamp("payment_deadline");// 截止时间
		final int is_pay = record.getInt("is_pay");// 是否付款
		@SuppressWarnings("unused")
		final Date add_time = record.getTimestamp("add_time");// 添加时间

		final BigDecimal rate_old = record.getBigDecimal("rate");// 年化利率
		final Date invite_end_time = record.getTimestamp("invite_end_time");// 锁定时间
		final Long invite_end_time_long = record.getLong("invite_end_time_long");// //锁定时间linux时间戳
		final Date repayment_time = record.getTimestamp("repayment_time");// 到期退出时间
		final Long repayment_time_long = record.getLong("repayment_time_long");// 到期退出时间Linux时间戳

		// 0 : 未支付；1：已支付
		if (is_pay == 1) {// 安全问题解决办法
			renderText("10");// 您已经支付了该预订单
			return;
		}

		if (wait_pay_amount_old != wait_pay_amount_param) {
			renderText("11");// 待付金额不正确
			return;
		}
		// 当前资金
		UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user_id);
		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用RMB
		if (can_used_cny_decimal.compareTo(cny_can_used) != 0) {
			renderText("12");// 可用资金不正确
			return;
		}
		if (cny_can_used.compareTo(new BigDecimal(wait_pay_amount_old + "")) < 0) {
			renderText("13");// 可用资金不足
			return;
		}

		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				Date date = new Date();
				FixBidUserOrderM fix_bid_user_order = FixBidUserOrderM.dao.findById(order_id);
				// 更改后的已付订金
				int already_pay_bond_new = already_pay_bond_old + wait_pay_amount_old;
				// 更改后的待付订金
				int wait_pay_amount_new = 0;
				BigDecimal wait_pay_amount_old_big_decimal = new BigDecimal(wait_pay_amount_old + "");

				fix_bid_user_order.set("is_pay", 1);
				fix_bid_user_order.set("already_pay_bond", already_pay_bond_new);
				fix_bid_user_order.set("wait_pay_amount", wait_pay_amount_new);
				boolean is_ok_update_fix_bid_user_order = fix_bid_user_order.update();
				if (!is_ok_update_fix_bid_user_order) {
					return false;
				}

				// 用户持有的联富宝信息
				FixBidUserHoldM fix_bid_user_hold = new FixBidUserHoldM();
				// 联富宝L03-141103期
				fix_bid_user_hold.set("bid_name", bid_name_old);
				fix_bid_user_hold.set("bid_type", bid_type);// 持有联富宝的类型 1
				// ：联富宝L03；2
				// ：联富宝L06；3 ：
				// 联富宝L12
				fix_bid_user_hold.set("fix_bid_sys_id", fix_bid_sys_id);
				fix_bid_user_hold.set("user_id", user_id_old);
				fix_bid_user_hold.set("nick_name", nick_name_old);
				fix_bid_user_hold.set("real_name", real_name_old);
				fix_bid_user_hold.set("annual_earning", rate_old);// 年化收益率
				fix_bid_user_hold.set("bid_money", bid_money_old);
				fix_bid_user_hold.set("earned_incom", 0);// 已获收益
				fix_bid_user_hold.set("bid_num", 1);// 投标次数
				fix_bid_user_hold.set("state", 1);// 0 ： 持有后退出 (要给违约金）；1 ：持有中
				fix_bid_user_hold.set("add_time", date);
				fix_bid_user_hold.set("add_time_long", System.currentTimeMillis());
				fix_bid_user_hold.set("exit_time", repayment_time);
				fix_bid_user_hold.set("exit_time_long", repayment_time_long);
				fix_bid_user_hold.set("lock_time", invite_end_time);
				fix_bid_user_hold.set("lock_time_long", invite_end_time_long);
				boolean is_ok_add_fix_bid_user_hold = fix_bid_user_hold.save();
				if (!is_ok_add_fix_bid_user_hold) {
					return false;
				}
				/***************************************************
				 *******************添加活动表************************
				 ***************************************************/
				_____Activity___Fix_Bid_WaitingM activity_fix_bid_waiting = new _____Activity___Fix_Bid_WaitingM();
				activity_fix_bid_waiting.set("user_id", user_id);
				activity_fix_bid_waiting.set("hold_id", fix_bid_user_hold.getLong("id"));
				activity_fix_bid_waiting.set("add_time", date);
				activity_fix_bid_waiting.set("add_time_long", System.currentTimeMillis());
				activity_fix_bid_waiting.save();

				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用RMB
				BigDecimal cny_can_used_new = cny_can_used.subtract(new BigDecimal(wait_pay_amount_old + ""));// 可用RMB新

				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");// 冻结CNY

				user_now_money.set("cny_can_used", cny_can_used_new);
				boolean is_ok_update_user_now_money = user_now_money.update();
				if (!is_ok_update_user_now_money) {
					return false;
				}
				/**
				 * 财务记录
				 */

				boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_old, UserMoneyChangeRecordsM.Type_23, cny_can_used.add(cny_freeze), wait_pay_amount_old_big_decimal, new BigDecimal("0"), cny_can_used.add(cny_freeze).subtract(wait_pay_amount_old_big_decimal), "联富宝支付剩下部分-->联富宝ID：" + fix_bid_user_order.getLong("id"));
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				/**
				 * 添加用户交易记录表user_transaction_records
				 */
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_23, cny_can_used.add(cny_freeze), wait_pay_amount_old_big_decimal, new BigDecimal("0"), cny_can_used.add(cny_freeze).subtract(wait_pay_amount_old_big_decimal), "联富宝预定支付剩余部分");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				return true;
			}
		});
		if (is_ok) {
			renderText("14");// 付款成功
			return;
		} else {
			renderText("15");// 付款不成功
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "根据类型 查询当前用户持有的联富宝列表信息", last_update_author = "hx")
	public void find_fix_bid_user_hold_by_type() throws ParseException {
		int type = getParameterToInt("type", 0);
		String select_sql = "SELECT * FROM fix_bid_user_hold t WHERE t.user_id = ? AND t.state = 1 ";
		// t.bid_type = ? ORDER BY t.id DESC
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		List<Object> params = new ArrayList<Object>();
		params.add(user_id);// 用户ID
		StringBuffer buffer = new StringBuffer(select_sql);
		if (type == 1) {
			// 查询所有的联富宝持有记录
		} else if (type == 2) {
			// 查询L03 联富宝持有记录
			buffer.append(" and t.bid_type = ?");
			params.add(1);// bid_type == 1 -->L03
		} else if (type == 3) {
			// 查询L06 联富宝持有记录
			buffer.append(" and t.bid_type = ?");
			params.add(2);// bid_type == 2 -->L06
		} else if (type == 4) {
			// 查询L012 联富宝持有记录
			buffer.append(" and t.bid_type = ?");
			params.add(3);// bid_type == 3 -->L12
		} else {
			renderText("0");// 传参数有错
			return;
		}

		buffer.append(" ORDER BY t.id DESC");
		List<FixBidUserHoldM> fix_bid_user_hold_list = FixBidUserHoldM.dao.find(buffer.toString(), params.toArray());
		List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
		/**
		 * 查询用户是否购买联富宝 新手标
		 */
		String fix_bid_user_newer_bid_sql = "select * from fix_bid_user_newer_bid where user_id = ? and is_end = 0";
		FixBidUserNewerBidM fix_bid_user_nower_bid = FixBidUserNewerBidM.dao.findFirst(fix_bid_user_newer_bid_sql, new Object[] { user_id });
		if (fix_bid_user_nower_bid != null) {
			// Map<String,Object> map_new_fix_bid =
			Long add_time_ux = fix_bid_user_nower_bid.getLong("add_time_ux");// 联富宝新手表锁定开始时间戳
			int bid_money = fix_bid_user_nower_bid.getInt("bid_money");// 联富宝新手标投资金额
			BigDecimal annual_rate = fix_bid_user_nower_bid.getBigDecimal("annual_rate");// 年化利率
			BigDecimal earned_incom = get_begain_locked_to_now_earned_incom(add_time_ux, bid_money, annual_rate);
			// fix_bid_user_nower_bid.getM();
			Map<String, Object> map_new_fix_bid = new HashMap<String, Object>();
			map_new_fix_bid.put("bid_name", "联富宝新手标");
			map_new_fix_bid.put("annual_earning", fix_bid_user_nower_bid.getBigDecimal("annual_rate"));
			map_new_fix_bid.put("bid_money", fix_bid_user_nower_bid.getInt("bid_money"));
			map_new_fix_bid.put("earned_incom", earned_incom);
			map_new_fix_bid.put("bid_num", 1);
			int is_end = fix_bid_user_nower_bid.getInt("is_end");// '0 没有结束； 1
			// 已经结束',
			int state = 0;
			if (is_end == 0) {
				state = 1;
			}
			map_new_fix_bid.put("state", state);
			map_new_fix_bid.put("is_locked", 2);// 联富宝新手表 不可退出
			lists.add(map_new_fix_bid);
		}

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (FixBidUserHoldM fix_bid_user_hold : fix_bid_user_hold_list) {
			Long lock_time_long = fix_bid_user_hold.getLong("lock_time_long");// 锁定时间的Linux时间戳
			int bid_money = fix_bid_user_hold.getInt("bid_money");
			BigDecimal annual_earning = fix_bid_user_hold.getBigDecimal("annual_earning");
			BigDecimal earned_incom = get_begain_locked_to_now_earned_incom(lock_time_long, bid_money, annual_earning);
			fix_bid_user_hold.set("earned_incom", earned_incom);

			Map<String, Object> map = fix_bid_user_hold.getM();

			Long now_date = System.currentTimeMillis();// 系统当前的Linux时间戳
			/**
			 * 判断是否进入锁定期 ，只有进入了锁定期后才能 退出
			 */
			if (now_date >= lock_time_long) {
				// 计算进入锁定期后 用户 为到退出日期 提前退出联富宝 应该收获的利息（按天计算）
				calc_locked_days_interest(format, fix_bid_user_hold, map);
				lists.add(map);
			} else {
				map.put("is_locked", 0);// 0 表示未进入锁定期
				lists.add(map);
				/***
				 * 测试阶段使用 calc_locked_days_interest(format, fix_bid_user_hold,
				 * map); lists.add(map);
				 ***/
			}

		}

		Gson gson = new Gson();
		renderText(gson.toJson(lists));
		return;
	}

	/**
	 * 锁定期到现在时间的收益
	 * 
	 * @param add_time_ux
	 * @param bidMoney
	 * @param annualRate
	 * @return
	 */
	private BigDecimal get_begain_locked_to_now_earned_incom(Long add_time_ux, int bidMoney, BigDecimal annualRate) {
		Long now_time_ux = System.currentTimeMillis();// 系统现在的时间戳
		Long interval_time = now_time_ux - add_time_ux;// 相差的时间戳
		int twenty_hours_has_millis = (24 * 60 * 60 * 1000);
		Long twenty_hours_has_millis_long = Long.valueOf(twenty_hours_has_millis);

		Long n = interval_time / twenty_hours_has_millis_long;
		BigDecimal decimal_1 = new BigDecimal(bidMoney + "").multiply(annualRate).multiply(new BigDecimal(n + ""));
		BigDecimal earned_incom = decimal_1.divide(new BigDecimal("366"), 2, BigDecimal.ROUND_DOWN);
		return earned_incom;
	}

	private void calc_locked_days_interest(SimpleDateFormat format, FixBidUserHoldM fix_bid_user_hold, Map<String, Object> map) throws ParseException {
		Date lock_time = fix_bid_user_hold.getTimestamp("lock_time");// 开始锁定时间
		Date date = new Date();// 当前时间
		String now_date_str = format.format(date);
		String lock_time_str = format.format(lock_time);
		// Date now_date = format.parse(now_date_str);
		/**
		 * 退出应收利息
		 */
		BigDecimal income = CalculationFormula.get_fix_bid_back_money(lock_time_str, now_date_str, fix_bid_user_hold.getBigDecimal("annual_earning"), fix_bid_user_hold.getInt("bid_money"));
		BigDecimal bid_money_decimal = new BigDecimal(fix_bid_user_hold.getInt("bid_money") + "");
		/**
		 * 计算 回收金额
		 */
		BigDecimal back_money = bid_money_decimal.add(income).setScale(2, BigDecimal.ROUND_DOWN);
		/**
		 * 计算退出 费用 提前退出费用=加入计划金额*2.0%
		 */
		BigDecimal exit_fee = bid_money_decimal.multiply(new BigDecimal("0.02"));
		map.put("is_locked", 1);// 1表示进入了锁定期
		map.put("back_money", back_money);
		map.put("exit_fee", exit_fee);
	}

	/**
	 * 提前退出联富宝
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "用户提前退出持有的联富宝", last_update_author = "hx")
	public void user_advance_exit_lfoll_fix_bid() {
		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");

		String exit_verificte_code = getParameter("exit_verificte_code");// 客服端验证码
		String session_code = getSessionAttribute("code");// session 中的code

		String money_pwd = getParameter("money_pwd");// 资金密码
		String money_password = user.getString("money_password");// Session中的资金密码

		final Integer id = getParameterToInt("id");// 客服端ID
		String exie_fee = getParameter("exie_fee");// 客服端 退出费

		if (Utils.isNullOrEmptyString(money_pwd)) {
			renderText("1");// 资金密码为空
			return;
		}
		if (Utils.isNullOrEmptyString(money_password)) {
			renderText("2");// Session中资金密码为空
			return;
		}
		if (!MD5.md5(money_pwd).equals(money_password)) {
			renderText("3");// 资金密码不正确
			return;
		}
		//
		if (Utils.isNullOrEmptyString(exit_verificte_code)) {
			renderText("4");// 验证码为空
			return;
		}
		if (!exit_verificte_code.equals(session_code)) {
			renderText("5");// 验证码不正确
			return;
		}
		//
		if (Utils.isNullOrEmptyString(exie_fee)) {
			renderText("6");// 退出费用为空
			return;
		}
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				Date now = new Date();
				String now_date = Model.Time.format(now);

				// 更改 联富宝 持有表的状态 为提前退出
				FixBidUserHoldM fix_bid_user_hold = FixBidUserHoldM.dao.findById(id);
				if (fix_bid_user_hold == null) {
					return false;
				}
				int state = fix_bid_user_hold.getInt("state");// 0 ： 提前退出
				// (要给违约金）；1
				// ：持有中 ;2
				// ：正常退出'
				// 不是持有中 返回错误
				if (state != 1) {
					return false;
				}
				int user_id_of_fix_bid_user_hold = fix_bid_user_hold.getInt("user_id");
				if (user_id_of_fix_bid_user_hold != user_id) {
					return false;
				}

				Date lock_time = fix_bid_user_hold.getTimestamp("lock_time");
				String lock_time_str = Model.Time.format(lock_time);// 锁定时间
				BigDecimal annual_earning = fix_bid_user_hold.getBigDecimal("annual_earning");// 年化利率
				int bid_money = fix_bid_user_hold.getInt("bid_money");// 投资金额
				BigDecimal bid_money_decimal = new BigDecimal(bid_money + "");

				fix_bid_user_hold.set("state", 0);// 提前退出
				fix_bid_user_hold.set("advance_exit_time", new Date());//
				fix_bid_user_hold.set("advance_exit_time_long", System.currentTimeMillis());
				boolean is_ok_update_fix_bid_user_hold = fix_bid_user_hold.update();
				if (!is_ok_update_fix_bid_user_hold) {
					return false;
				}
				/**
				 * 从锁定日期 到 退出日期 按年化利率 获取收益 退出计算退出时的已获收益
				 */
				BigDecimal income = CalculationFormula.get_fix_bid_back_money(lock_time_str, now_date, annual_earning, bid_money);
				// 保存用户退出记录表
				FixBidUserExitM fix_bid_user_exit = new FixBidUserExitM();
				boolean is_ok_add_fix_bid_user_exit = fix_bid_user_exit//
						.set("bid_name", fix_bid_user_hold.getString("bid_name"))//
						.set("bid_type", fix_bid_user_hold.getInt("bid_type"))//
						.set("fix_bid_user_hold_id", fix_bid_user_hold.getLong("id"))//
						.set("fix_bid_sys_id", fix_bid_user_hold.getLong("fix_bid_sys_id"))//
						.set("user_id", fix_bid_user_hold.getInt("user_id"))//
						.set("nick_name", fix_bid_user_hold.getString("nick_name"))//
						.set("real_name", fix_bid_user_hold.getString("real_name"))//
						.set("annual_earning", fix_bid_user_hold.getBigDecimal("annual_earning"))//
						.set("bid_money", fix_bid_user_hold.getInt("bid_money"))//
						.set("earned_incom", income)// 提前退出有收益
						.set("exit_fee", fix_bid_user_hold.getInt("bid_money") * 0.02)//
						.set("state", 2)// 设置提前退出
						.set("exit_time", fix_bid_user_hold.getTimestamp("exit_time"))//
						.set("exit_time_long", fix_bid_user_hold.getLong("exit_time_long"))//
						.set("lock_time", fix_bid_user_hold.getTimestamp("lock_time"))//
						.set("lock_time_long", fix_bid_user_hold.getLong("lock_time_long"))//
						.set("user_exit_time", now)//
						.set("user_exit_time_long", now.getTime())//
						.save();
				if (!is_ok_add_fix_bid_user_exit) {
					return false;
				}

				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				BigDecimal old_cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");// 冻结
				BigDecimal money_total_1 = old_cny_can_used.add(cny_freeze);// 退出之前的总资产

				/**
				 * 退还 97%的联富宝金额到 账户 3%为退出 违约金
				 */
				BigDecimal exit_fix_bid_money = new BigDecimal(bid_money + "").multiply(new BigDecimal("0.97"));// 归返97%

				/**
				 * 可用+97%本金+利息
				 */
				BigDecimal new_cny_can_used = old_cny_can_used.add(exit_fix_bid_money).add(income);
				BigDecimal money_total_2 = new_cny_can_used.add(cny_freeze);// 退出之后的总资产
				user_now_money.set("cny_can_used", new_cny_can_used);
				boolean is_ok_update_user_now_money = user_now_money.update();
				if (!is_ok_update_user_now_money) {
					return false;
				}

				/**
				 * 用户资金更改记录 1 : 归还本金 user_money_change_records
				 */
				boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_25, money_total_1, new BigDecimal("0"), bid_money_decimal, money_total_1.add(bid_money_decimal), "本金-联富宝提前退出");
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				/**
				 * 用户资金更改记录 2 : 利息 user_money_change_records
				 */
				if (income.compareTo(new BigDecimal("0")) > 0) {
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_26, money_total_1.add(bid_money_decimal), new BigDecimal("0"), income, money_total_1.add(bid_money_decimal).add(income), "利息-联富宝提前退出");
					if (!is_ok_add_user_money_change_records_by_type) {
						return false;
					}
				}
				/**
				 * 用户资金更改记录 3 : 手续费 投资金额的 3%作为手续费 user_money_change_records
				 */
				is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_27, money_total_1.add(bid_money_decimal).add(income), new BigDecimal(bid_money + "").multiply(new BigDecimal("0.03")), new BigDecimal("0"), money_total_2, "手续费-联富宝提前退出");
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;

				}
				/**
				 * 添加有户交易记录信息 记录 进user_transaction_records
				 */
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_22, money_total_1, new BigDecimal(bid_money + "").multiply(new BigDecimal("0.03")), exit_fix_bid_money.add(income), new_cny_can_used, "联富宝提前退出");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}

				// 本金-联富宝提前退出 ---系统财务本金支出
				boolean is_ok_add_sys_expenses_records_1 = SysExpensesRecordsM.add_sys_expenses_records(user_id, UserMoneyChangeRecordsM.Type_25, new BigDecimal(bid_money + ""), "联富宝提前退出归还本金，持有表ID->" + id);
				if (!is_ok_add_sys_expenses_records_1) {
					return false;
				}
				// 利息-联富宝提前退出---系统财务利息支出
				boolean is_ok_add_sys_expenses_records_2 = SysExpensesRecordsM.add_sys_expenses_records(user_id, UserMoneyChangeRecordsM.Type_26, income, "联富宝提前退出归还利息，持有表ID->" + id);
				if (!is_ok_add_sys_expenses_records_2) {
					return false;
				}
				// 手续费 -联富宝提前退出收取3%的手续费 ------系统财务收入
				boolean is_ok_add_sys_expenses_records_3 = SysIncomeRecordsM.add_sys_income_records(user_id, UserMoneyChangeRecordsM.Type_27, new BigDecimal(bid_money + "").multiply(new BigDecimal("0.03")), "联富宝提前退出系统收取手续费，持有表ID->" + id);
				if (!is_ok_add_sys_expenses_records_3) {
					return false;
				}
				return true;

			}
		});

		if (is_ok) {
			renderText("7");// 退出成功
			return;
		} else {
			renderText("8");// 退出失败
			return;
		}

	}
}
