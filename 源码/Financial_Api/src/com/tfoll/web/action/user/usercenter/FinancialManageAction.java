package com.tfoll.web.action.user.usercenter;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.LenderAop;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.model.LenderBulkStandardNewerBidGatherM;
import com.tfoll.web.model.LenderBulkStandardNewerBidRecordM;
import com.tfoll.web.model.UserM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ActionKey("/user/usercenter/financial_manage")
// 个人信息-->理财模块的Action
public class FinancialManageAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "用户中心 理财管理  我的债权页面", last_update_author = "lh")
	public void to_my_debt() {
		renderJsp("/user/usercenter/financial_manage/account_my_debt.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "用户中心 理财管理  债权转让页面", last_update_author = "lh")
	public void to_debt_transfer() {
		renderJsp("/user/usercenter/financial_manage/debt_transfer.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "用户中心 理财管理  理财统计页面", last_update_author = "lh")
	public void to_finance_statistics() {
		renderJsp("/user/usercenter/financial_manage/finance_statistics.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "获取投标中的债权", last_update_author = "lh")
	public void get_bidding_debt() {
		// int lend_user_id = getParameterToInt("lend_user_id");
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		/* 债权ID(其实是理财单id) 原始投资金额 年利率 期限 信用等级 剩余时间 投标进度 状态 */
		String sql = "SELECT b.id as gather_id, a.invest_money,a.invest_share, b.annulized_rate,b.annulized_rate_int, b.borrow_duration, b.credit_rating,b.deadline, b.gather_progress FROM lender_bulk_standard_order a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND a.lend_user_id = ? AND a.state = 1";
		List<Record> bidding_debt_record_list = Db.find(sql, new Object[] { user_id });
		List<Map<String, Object>> bidding_debt_list = new ArrayList<Map<String, Object>>();
		for (Record bidding_debt_record : bidding_debt_record_list) {
			Date deadline = bidding_debt_record.getTimestamp("deadline");
			Date now = new Date();
			int remain_second = (int) ((deadline.getTime() - now.getTime()) / 1000);

			int day = remain_second / (3600 * 24);
			int hour = (remain_second % (3600 * 24)) / 3600;
			int minute = (remain_second % 3600) / 60;
			int second = remain_second % 60;
			String remain_time_str = day + "天" + hour + "时" + minute + "分";

			Map<String, Object> record_inside = bidding_debt_record.getR();
			record_inside.put("remain_time_str", remain_time_str);

			bidding_debt_list.add(record_inside);
		}

		// 获取投标中的的新手标
		String sql_newer = "SELECT * FROM lender_bulk_standard_newer_bid_gather WHERE id = ( SELECT newer_bid_gather_id FROM lender_bulk_standard_newer_bid_record WHERE user_id = ? ) AND is_full = 0";
		LenderBulkStandardNewerBidGatherM lender_bulk_standard_newer_bid_gather = LenderBulkStandardNewerBidGatherM.dao.findFirst(sql_newer, new Object[] { user_id });
		Map<String, Object> newer_bid_gather = new HashMap<String, Object>();
		if (lender_bulk_standard_newer_bid_gather != null) {
			newer_bid_gather = lender_bulk_standard_newer_bid_gather.getM();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bidding_debt_list", bidding_debt_list);

		// 没有投标中的新手标
		map.put("bidding_newer_bid", new HashMap<String, Object>());

		Gson gson = new Gson();
		String str = gson.toJson(map);

		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "获取已结清的债权", last_update_author = "lh")
	public void get_pay_off_debt() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		// 已经结清的新手精英标
		String sql_newer = "SELECT * FROM `lender_bulk_standard_newer_bid_record` where user_id = ? and is_return_interest = 1";
		LenderBulkStandardNewerBidRecordM lender_bulk_standard_newer_bid_record = LenderBulkStandardNewerBidRecordM.dao.findFirst(sql_newer, new Object[] { user_id });
		Map<String, Object> pay_off_newer_bid = new HashMap<String, Object>();
		if (lender_bulk_standard_newer_bid_record != null) {
			pay_off_newer_bid = lender_bulk_standard_newer_bid_record.getM();
		}

		// 接下来获取已结清新手标list

		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pay_off_newer_bid", pay_off_newer_bid);
		String str = gson.toJson(map);
		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "获取回收中的债权", last_update_author = "lh")
	public void get_repaying_debt() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		String sql = "SELECT * FROM ( SELECT b.id AS debt_id, a.hold_share, b.annulized_rate, b.annulized_rate_int, c.total_periods, c.current_period, b.borrow_all_share, c.should_repayment_total,c.automatic_repayment_date, c.repay_end_time FROM lender_bulk_standard_creditor_right_hold a, borrower_bulk_standard_gather_money_order b, borrower_bulk_standard_repayment_plan c WHERE a.gather_money_order_id = b.id AND c.gather_money_order_id = b.id AND b.payment_state = 2 AND a.user_id = ? AND ? < c.repay_end_time ) AS new_table WHERE current_period = ( SELECT min(current_period) FROM ( SELECT b.id AS debt_id, a.hold_share, b.annulized_rate, b.annulized_rate_int, c.total_periods, c.current_period, b.borrow_all_share, c.should_repayment_total,c.automatic_repayment_date, c.repay_end_time FROM lender_bulk_standard_creditor_right_hold a, borrower_bulk_standard_gather_money_order b, borrower_bulk_standard_repayment_plan c WHERE a.gather_money_order_id = b.id AND c.gather_money_order_id = b.id AND b.payment_state = 2 AND a.user_id = ? AND ? < c.repay_end_time ) AS new_table )";
		List<Record> repaying_debt_record_list = Db.find(sql, new Object[] { user_id, new Date(), user_id, new Date() });

		List<Map<String, Object>> repaying_debt_list = new ArrayList<Map<String, Object>>();
		for (Record repaying_debt_record : repaying_debt_record_list) {
			Map<String, Object> repaying_debt = repaying_debt_record.getR();
			int hold_share = repaying_debt_record.getInt("hold_share");// 持有的份数
			BigDecimal should_repayment_total = repaying_debt_record.getBigDecimal("should_repayment_total");// 借款人要还的总额
			int borrow_all_share = repaying_debt_record.getInt("borrow_all_share");// 总份数
			int total_periods = repaying_debt_record.getInt("total_periods");// 总期数
			long current_period = repaying_debt_record.getLong("current_period");// 下次还款期数

			int origin_money = hold_share * 50;// 原始投资金额
			// 月收本息（借款人要还的本息 * ( 持有的份数 / 总份数 ) ）
			double month_benxi = should_repayment_total.doubleValue() * (new Double(hold_share) / borrow_all_share);
			double remian_benxi = month_benxi * (total_periods - current_period + 1);// 待收本息

			repaying_debt.put("origin_money", origin_money);
			repaying_debt.put("month_benxi", month_benxi);
			repaying_debt.put("remian_benxi", remian_benxi);

			repaying_debt_list.add(repaying_debt);
		}

		// 获取回收中的新手标
		String sql_newer = "SELECT * FROM lender_bulk_standard_newer_bid_record where user_id = ? and is_return_interest = 0";
		LenderBulkStandardNewerBidRecordM lender_bulk_standard_newer_bid_record = LenderBulkStandardNewerBidRecordM.dao.findFirst(sql_newer, new Object[] { user_id });
		Map<String, Object> repaying_newer_bid = new HashMap<String, Object>();
		if (lender_bulk_standard_newer_bid_record != null) {
			repaying_newer_bid = lender_bulk_standard_newer_bid_record.getM();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("repaying_debt_list", repaying_debt_list);
		map.put("repaying_newer_bid", repaying_newer_bid);
		Gson gson = new Gson();
		String str = gson.toJson(map);

		renderText(str);
		return;

	}

}
