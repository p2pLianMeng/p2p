package com.tfoll.web.action.user.financial;

import com.google.gson.Gson;
import com.tfoll.console.util.ConsoleInit;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.ProcedureCall;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.LenderAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.common.code.CheckCodeServlet;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardOrderUserInfoM;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.model.LenderBulkStandardNewerBidGatherM;
import com.tfoll.web.model.LenderBulkStandardNewerBidRecordM;
import com.tfoll.web.model.LenderBulkStandardNewerBidRecordWaitingM;
import com.tfoll.web.model.LenderBulkStandardOrderM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.util.CommonRepayMethod;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.AjaxAsyncPageDiv;
import com.tfoll.web.util.page.AjaxAsyncPageLfollFixBidDiv;
import com.tfoll.web.util.page.PageDiv;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@ActionKey("/user/financial/financial")
public class FinancialAction extends Controller {

	final static int Each_Debt_Value = 50;// 每份债权初始50元

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "理财列表导航精英表", last_update_author = "lh")
	public void loan_list_elite() {
		/**
		 * 清除条件
		 */
		// setSessionAttribute("borrow_duration_type", 0);
		// setSessionAttribute("credit_rating", 0);
		// setSessionAttribute("pn", 1);
		// 13580428652

		// 查询正在筹款的精英新手标的id
		String sql = "select id,gather_progress from lender_bulk_standard_newer_bid_gather where is_full = 0 order by id limit 1";

		Record record = Db.findFirst(sql);

		long newer_gather_id = 0;
		int gather_progress = 0;

		if (record != null) {
			newer_gather_id = record.getLong("id");
			gather_progress = record.getInt("gather_progress");
		}

		setAttribute("newer_gather_id", newer_gather_id);
		setAttribute("gather_progress", gather_progress);

		renderJsp("/user/financial/loan_list_elite.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "理财列表导航联富宝", last_update_author = "hx")
	public void loan_list_lfoll_invest() {
		int pn = getParameterToInt("pn", 1);
		String count_sql = "SELECT COUNT(1) FROM ( SELECT * FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 ORDER BY S1.id DESC LIMIT 3, 1000 ) AS T1";
		String select_list_sql = "SELECT * FROM ( SELECT S1.id AS id, S1.bid_name AS bid_name, S1.total_money AS total_money, S1.least_money AS least_money, S1.booking_number AS booking_number, S1.annualized_rate AS annualized_rate, S1.advance_notice_start_time_long AS advance_notice_start_time_long, S1.reserve_start_time_long AS reserve_start_time_long, S1.reserve_end_time_long AS reserve_end_time_long, S1.pay_end_time_long AS pay_end_time_long, S1.invite_start_time_long AS invite_start_time_long, S1.invite_end_time_long AS invite_end_time_long, S1.repayment_time_long AS repayment_time_long, S1.join_number AS join_number, S1.all_pay_back_money AS all_pay_back_money, S1.closed_period AS closed_period FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 ORDER BY S1.id DESC LIMIT 3, 1000 ) AS T1";
		Long total_rows = Db.queryLong(count_sql);
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn, 4);
			List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(page.creatLimitSql(select_list_sql));

			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				// Record record = new Record();

				// 公告开始时间的毫秒数
				Long advance_notice_start_time_long = fix_bid_system_order.getLong("advance_notice_start_time_long");
				/**
				 * 预定开始时间的毫秒数
				 */
				Long reserve_start_time_long = fix_bid_system_order.getLong("reserve_start_time_long");

				/**
				 * 预定结束时间的毫秒数
				 */
				Long reserve_end_time_long = fix_bid_system_order.getLong("reserve_end_time_long");

				/**
				 * 支付截止时间的毫秒数
				 */
				Long pay_end_time_long = fix_bid_system_order.getLong("pay_end_time_long");

				/**
				 * 投标开始时间毫秒数
				 */
				Long invite_start_time_long = fix_bid_system_order.getLong("invite_start_time_long");

				/**
				 * 投标结束时间的毫秒数
				 */

				Long invite_end_time_long = fix_bid_system_order.getLong("invite_end_time_long");

				/**
				 * 退出定投时间毫秒数
				 */
				Long repayment_time_long = fix_bid_system_order.getLong("repayment_time_long");

				Long current_time = System.currentTimeMillis();

				if ((advance_notice_start_time_long < current_time) && (current_time < reserve_start_time_long)) {
					fix_bid_system_order.set("public_state_var", 1);// 公告阶段 等待预定
				} else if ((reserve_start_time_long < current_time) && (current_time < reserve_end_time_long)) {
					fix_bid_system_order.set("public_state_var", 2);// 预定中
				} else if ((reserve_end_time_long < current_time) && (current_time < pay_end_time_long)) {
					fix_bid_system_order.set("public_state_var", 3);// 等待支付
				} else if ((pay_end_time_long < current_time) && (current_time < invite_start_time_long)) {
					fix_bid_system_order.set("public_state_var", 4);// 等待开标
				} else if ((invite_start_time_long < current_time) && (current_time < invite_end_time_long)) {
					fix_bid_system_order.set("public_state_var", 5);// 开放中
				} else if ((invite_end_time_long < current_time) && (current_time < repayment_time_long)) {
					fix_bid_system_order.set("public_state_var", 6);// 收益中
				} else if (current_time > repayment_time_long) {
					fix_bid_system_order.set("public_state_var", 7);// 已结束
				}

				int total_money = fix_bid_system_order.getInt("total_money");
				int total_wan = total_money / 10000;
				fix_bid_system_order.set("total_money", total_wan);
				fix_bid_system_order.set("annualized_rate", fix_bid_system_order.getBigDecimal("annualized_rate").multiply(new BigDecimal("100")).setScale(1, BigDecimal.ROUND_DOWN));// 预期年化收益
			}
			setAttribute("fix_bid_system_order_list", fix_bid_system_order_list);
			String url = PageDiv.createUrl(getRequest(), "/user/financial/financial/loan_list_lfoll_invest");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}

		String top_three_fix_bid_sql = "SELECT * from  fix_bid_system_order t WHERE t.is_show =1 ORDER BY t.id DESC LIMIT 0,3";
		List<FixBidSystemOrderM> fix_bid_system_order_top_three_list = FixBidSystemOrderM.dao.find(top_three_fix_bid_sql);
		if (fix_bid_system_order_top_three_list != null) {
			SimpleDateFormat format3 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_top_three_list) {
				// 公告开始时间的毫秒数
				Long advance_notice_start_time_long = fix_bid_system_order.getLong("advance_notice_start_time_long");
				// 预定开始时间的毫秒数
				Long reserve_start_time_long = fix_bid_system_order.getLong("reserve_start_time_long");
				// 预定结束时间的毫秒数
				Long reserve_end_time_long = fix_bid_system_order.getLong("reserve_end_time_long");
				// 支付截止时间的毫秒数
				Long pay_end_time_long = fix_bid_system_order.getLong("pay_end_time_long");
				// 投标开始时间毫秒数
				Long invite_start_time_long = fix_bid_system_order.getLong("invite_start_time_long");
				// 投标结束时间的毫秒数
				Long invite_end_time_long = fix_bid_system_order.getLong("invite_end_time_long");
				// 退出定投时间毫秒数
				Long repayment_time_long = fix_bid_system_order.getLong("repayment_time_long");

				Long current_time = System.currentTimeMillis();

				if ((advance_notice_start_time_long < current_time) && (current_time < reserve_start_time_long)) {
					fix_bid_system_order.set("public_state_var", 1);// 公告阶段 等待预定
					// 预定开始时间
					Date reserve_start_time = fix_bid_system_order.getTimestamp("reserve_start_time");// 预定开始时间
					String reserve_start_time_str = format3.format(reserve_start_time);// 格式化后——预定开始时间
					fix_bid_system_order.set("reserve_start_time", reserve_start_time_str);
				} else if ((reserve_start_time_long < current_time) && (current_time < reserve_end_time_long)) {
					fix_bid_system_order.set("public_state_var", 2);// 预定中
					// 预定结束时间
					Date reserve_end_time = fix_bid_system_order.getTimestamp("reserve_end_time");
					String reserve_end_time_str = format3.format(reserve_end_time);// 格式化后——预定结束时间
					fix_bid_system_order.set("reserve_end_time", reserve_end_time_str);
				} else if ((reserve_end_time_long < current_time) && (current_time < pay_end_time_long)) {
					fix_bid_system_order.set("public_state_var", 3);// 等待支付
					// 支付截止时间
					Date pay_end_time = fix_bid_system_order.getTimestamp("pay_end_time");
					String pay_end_time_str = format3.format(pay_end_time);
					fix_bid_system_order.set("pay_end_time", pay_end_time_str);// 格式化后——支付截止时间
				} else if ((pay_end_time_long < current_time) && (current_time < invite_start_time_long)) {
					fix_bid_system_order.set("public_state_var", 4);// 等待开标
					// 投标开始时间
					Date invite_start_time = fix_bid_system_order.getTimestamp("invite_start_time");
					String invite_start_time_str = format3.format(invite_start_time);
					fix_bid_system_order.set("invite_start_time", invite_start_time_str);// 格式化后——投标开始时间
				} else if ((invite_start_time_long < current_time) && (current_time < invite_end_time_long)) {
					fix_bid_system_order.set("public_state_var", 5);// 开放中
					// 投标结束时间
					Date invite_end_time = fix_bid_system_order.getTimestamp("invite_end_time");
					String invite_end_time_str = format3.format(invite_end_time);
					fix_bid_system_order.set("invite_end_time", invite_end_time_str);// 格式化后——投标结束时间
				} else if ((invite_end_time_long < current_time) && (current_time < repayment_time_long)) {
					fix_bid_system_order.set("public_state_var", 6);// 收益中
					// 退出时间
					Date repayment_time = fix_bid_system_order.getTimestamp("repayment_time");
					String repayment_time_str = format3.format(repayment_time);
					fix_bid_system_order.set("repayment_time", repayment_time_str);// 格式化后——退出时间
				} else if (current_time > repayment_time_long) {
					fix_bid_system_order.set("public_state_var", 7);// 已结束
				}

				fix_bid_system_order.set("annualized_rate", fix_bid_system_order.getBigDecimal("annualized_rate").multiply(new BigDecimal("100")).setScale(1, BigDecimal.ROUND_DOWN));// 预期年化收益
			}
			setAttribute("fix_bid_system_order_top_three_list", fix_bid_system_order_top_three_list);

		}
		renderJsp("/user/financial/loan_list_lfoll_invest.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "ajax异步分页查询联富宝L03", last_update_author = "hx")
	public void loan_list_fix_bid_public() {
		int pn = getParameterToInt("pn", 1);
		int type = getParameterToInt("type");
		if (!(type == 3 || type == 6 || type == 12)) {
			return;
		}
		String count_sql = "";
		String select_list_sql = "";
		if (type == 3) {
			count_sql = "SELECT COUNT(1) FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 3 ORDER BY S1.id DESC";
			select_list_sql = "SELECT S1.id AS id, S1.bid_name AS bid_name, S1.total_money AS total_money, S1.booking_number AS booking_number, S1.annualized_rate AS annualized_rate, S1.advance_notice_start_time_long AS advance_notice_start_time_long, S1.reserve_start_time_long AS reserve_start_time_long, S1.reserve_end_time_long AS reserve_end_time_long, S1.pay_end_time_long AS pay_end_time_long, S1.invite_start_time_long AS invite_start_time_long, S1.invite_end_time_long AS invite_end_time_long, S1.repayment_time_long AS repayment_time_long, S1.join_number AS join_number, S1.all_pay_back_money AS all_pay_back_money FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 3 ORDER BY S1.id DESC";
		} else if (type == 6) {
			count_sql = "SELECT COUNT(1) FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 6 ORDER BY S1.id DESC";
			select_list_sql = "SELECT S1.id AS id, S1.bid_name AS bid_name, S1.total_money AS total_money, S1.booking_number AS booking_number, S1.annualized_rate AS annualized_rate, S1.advance_notice_start_time_long AS advance_notice_start_time_long, S1.reserve_start_time_long AS reserve_start_time_long, S1.reserve_end_time_long AS reserve_end_time_long, S1.pay_end_time_long AS pay_end_time_long, S1.invite_start_time_long AS invite_start_time_long, S1.invite_end_time_long AS invite_end_time_long, S1.repayment_time_long AS repayment_time_long, S1.join_number AS join_number, S1.all_pay_back_money AS all_pay_back_money FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 6 ORDER BY S1.id DESC";
		} else if (type == 12) {
			count_sql = "SELECT COUNT(1) FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 12 ORDER BY S1.id DESC";
			select_list_sql = "SELECT S1.id AS id, S1.bid_name AS bid_name, S1.total_money AS total_money, S1.booking_number AS booking_number, S1.annualized_rate AS annualized_rate, S1.advance_notice_start_time_long AS advance_notice_start_time_long, S1.reserve_start_time_long AS reserve_start_time_long, S1.reserve_end_time_long AS reserve_end_time_long, S1.pay_end_time_long AS pay_end_time_long, S1.invite_start_time_long AS invite_start_time_long, S1.invite_end_time_long AS invite_end_time_long, S1.repayment_time_long AS repayment_time_long, S1.join_number AS join_number, S1.all_pay_back_money AS all_pay_back_money FROM fix_bid_system_order AS S1 WHERE S1.is_show = 1 AND S1.closed_period = 12 ORDER BY S1.id DESC";
		}

		Long total_rows = Db.queryLong(count_sql);
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn, 4);
			List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(page.creatLimitSql(select_list_sql));

			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				// Record record = new Record();
				Map<String, Object> hashMap = fix_bid_system_order.getM();
				// 公告开始时间的毫秒数
				Long advance_notice_start_time_long = fix_bid_system_order.getLong("advance_notice_start_time_long");
				// 预定开始时间的毫秒数
				Long reserve_start_time_long = fix_bid_system_order.getLong("reserve_start_time_long");
				// 预定结束时间的毫秒数
				Long reserve_end_time_long = fix_bid_system_order.getLong("reserve_end_time_long");
				// 支付截止时间的毫秒数
				Long pay_end_time_long = fix_bid_system_order.getLong("pay_end_time_long");
				// 投标开始时间毫秒数
				Long invite_start_time_long = fix_bid_system_order.getLong("invite_start_time_long");
				// 投标结束时间的毫秒数
				Long invite_end_time_long = fix_bid_system_order.getLong("invite_end_time_long");
				// 退出定投时间毫秒数
				Long repayment_time_long = fix_bid_system_order.getLong("repayment_time_long");

				Long current_time = System.currentTimeMillis();
				int public_state_var = 0;
				if ((advance_notice_start_time_long < current_time) && (current_time < reserve_start_time_long)) {
					public_state_var = 1;
					fix_bid_system_order.set("public_state_var", 1);// 公告阶段 等待预定
				} else if ((reserve_start_time_long < current_time) && (current_time < reserve_end_time_long)) {
					public_state_var = 2;
					fix_bid_system_order.set("public_state_var", 2);// 预定中
				} else if ((reserve_end_time_long < current_time) && (current_time < pay_end_time_long)) {
					public_state_var = 3;
					fix_bid_system_order.set("public_state_var", 3);// 等待支付
				} else if ((pay_end_time_long < current_time) && (current_time < invite_start_time_long)) {
					public_state_var = 4;
					fix_bid_system_order.set("public_state_var", 4);// 等待开标
				} else if ((invite_start_time_long < current_time) && (current_time < invite_end_time_long)) {
					public_state_var = 5;
					fix_bid_system_order.set("public_state_var", 5);// 开放中
				} else if ((invite_end_time_long < current_time) && (current_time < repayment_time_long)) {
					public_state_var = 6;
					fix_bid_system_order.set("public_state_var", 6);// 收益中
				} else if (current_time > repayment_time_long) {
					public_state_var = 7;
					fix_bid_system_order.set("public_state_var", 7);// 已结束
				}
				hashMap.put("public_state_var", public_state_var);// 状态
				hashMap.put("annualized_rate", fix_bid_system_order.getBigDecimal("annualized_rate").multiply(new BigDecimal("100")).setScale(1, BigDecimal.ROUND_DOWN));
				list.add(hashMap);
			}
			String AsyncPageDiv = AjaxAsyncPageLfollFixBidDiv.getDiv(type, page.getTotalPageNum(), page.getPageSize(), page.getCurrentPageNum());

			Map<String, Object> json_map = new HashMap<String, Object>();
			json_map.put("list", list);
			json_map.put("AsyncPageDiv", AsyncPageDiv);
			Gson gson = new Gson();
			renderText(gson.toJson(json_map));
		} else {
			setAttribute("tips", "暂无数据!");
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "理财列表导航债权转让", last_update_author = "lh")
	public void loan_list_creditor_right() {

		renderJsp("/user/financial/loan_list_creditor_right.jsp");
		return;
	}

	@Deprecated
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "理财列表异步查询,异步分页(旧版本)", last_update_author = "lh")
	public void loan_list_query() {

		int borrow_duration_type = getParameterToInt("borrow_duration_type", 0);
		int credit_rating = getParameterToInt("credit_rating", 0);
		int pn = getParameterToInt("pn", 1);

		/**
		 * 点击借款期限-认证级别默认为-1,点击认证级别-借款期限默认-1,只要是-1就从session中获取,不为-1则放入session
		 */

		if (borrow_duration_type == -1) {
			borrow_duration_type = getSessionAttribute("borrow_duration_type");
		} else {
			setSessionAttribute("borrow_duration_type", borrow_duration_type);
		}
		if (credit_rating == -1) {
			credit_rating = getSessionAttribute("credit_rating");
		} else {
			setSessionAttribute("credit_rating", credit_rating);
		}

		// 查询语句
		StringBuilder sql_count = new StringBuilder("select count(1) from borrower_bulk_standard_gather_money_order where 1=1");
		StringBuilder sql_list = new StringBuilder("select * from borrower_bulk_standard_gather_money_order where 1=1 ");

		if (borrow_duration_type == 0) {

		} else if (borrow_duration_type == 1) {
			sql_count.append(" and (borrow_duration = 3 or borrow_duration = 6)");//
			// 注释不能删
			sql_list.append(" and (borrow_duration = 3 or borrow_duration = 6)");// 注释不能删
		} else if (borrow_duration_type == 2) {
			sql_count.append(" and (borrow_duration = 9 or borrow_duration = 12 or borrow_duration = 15)");
			sql_list.append(" and (borrow_duration = 9 or borrow_duration = 12 or borrow_duration = 15)");

		} else if (borrow_duration_type == 3) {
			sql_count.append(" and (borrow_duration = 18 or borrow_duration = 24)");
			sql_list.append(" and (borrow_duration = 18 or borrow_duration = 24)");

		} else if (borrow_duration_type == 4) {
			sql_count.append(" and borrow_duration = 36 ");
			sql_list.append(" and borrow_duration = 36 ");
		} else {
			renderText("非法操作");
			return;
		}

		if (credit_rating == 0) {

		} else if (credit_rating == 1) {
			sql_count.append(" and credit_rating = 'A+' ");// 'A+'
			sql_list.append(" and credit_rating = 'A+' ");// 'A+'
		} else if (credit_rating == 2) {
			sql_count.append(" and credit_rating = 'A' ");// 'A'
			sql_list.append(" and credit_rating = 'A' ");// 'A'
		} else if (credit_rating == 3) {
			sql_count.append(" and credit_rating = 'B+' ");// 'B+'
			sql_list.append(" and credit_rating = 'B+' ");// 'B+'
		} else if (credit_rating == 4) {
			sql_count.append(" and credit_rating = 'B' ");// 'B'
			sql_list.append(" and credit_rating = 'B' ");// 'B'
		} else if (credit_rating == 5) {
			sql_count.append(" and credit_rating = 'C+' ");// 'C+'
			sql_list.append(" and credit_rating = 'C+' ");// 'C+'
		} else if (credit_rating == 6) {
			sql_count.append(" and credit_rating = 'C' ");// 'C'
			sql_list.append(" and credit_rating = 'C' ");// 'C'
		} else {
			renderText("非法操作");
			return;
		}

		sql_list.append(" limit ?,? ");
		List<Object> param = new ArrayList<Object>();

		Long total_row = Db.queryLong(sql_count.toString(), param.toArray());

		Page page = new Page(total_row, pn, 5);
		List<Map<String, Object>> models_inside = new ArrayList<Map<String, Object>>();

		if (total_row > 0) {
			List<BorrowerBulkStandardGatherMoneyOrderM> gather_money = BorrowerBulkStandardGatherMoneyOrderM.dao.find(sql_list.toString(), new Object[] { page.getBeginIndex(), page.getEndIndex() });
			for (BorrowerBulkStandardGatherMoneyOrderM model : gather_money) {
				Map<String, Object> model_inside = model.getM();
				models_inside.add(model_inside);
			}
		}
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bid_list", models_inside);
		String asyncPageDiv = AjaxAsyncPageDiv.getDiv(page.getTotalPageNum(), page.getPageSize(), page.getCurrentPageNum());
		map.put("asyncPageDiv", asyncPageDiv);
		renderText(gson.toJson(map));
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "理财列表异步查询,异步分页(新版本)", last_update_author = "lh")
	public void loan_list_query2() {

		int borrow_duration = getParameterToInt("borrow_duration", 0);
		int annulized_rate_int = getParameterToInt("annulized_rate_int", 0);
		int borrow_all_money_level = getParameterToInt("borrow_all_money", 0);
		int gather_progress_level = getParameterToInt("gather_progress", 0);
		int pn = getParameterToInt("pn", 1);

		Date now = new Date();

		//
		StringBuilder sql_count = new StringBuilder("select count(1) from borrower_bulk_standard_gather_money_order where 1=1");
		StringBuilder sql_list = new StringBuilder("select * from borrower_bulk_standard_gather_money_order where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (borrow_duration == 0) {

		} else {
			sql_count.append(" and borrow_duration = ?");
			sql_list.append(" and borrow_duration = ?");
			params.add(borrow_duration);
		}
		if (annulized_rate_int == 0) {

		} else {
			sql_count.append(" and annulized_rate_int = ? ");
			sql_list.append(" and annulized_rate_int = ? ");
			params.add(annulized_rate_int);
		}
		if (borrow_all_money_level == 0) {

		} else {
			sql_count.append(" and borrow_all_money_level = ? ");
			sql_list.append(" and borrow_all_money_level = ? ");
			params.add(borrow_all_money_level);
		}
		if (gather_progress_level == 0) {

		} else {
			sql_count.append(" and gather_progress_level = ? ");
			sql_list.append(" and gather_progress_level = ? ");
			params.add(gather_progress_level);
		}

		sql_list.append(" order by gather_state,id desc limit ?,? ");

		Long total_row = Db.queryLong(sql_count.toString(), params.toArray());

		Page page = new Page(total_row, pn, 5);
		params.add(page.getBeginIndex());
		params.add(page.getEndIndex());
		List<Map<String, Object>> models_inside = new ArrayList<Map<String, Object>>();

		if (total_row > 0) {
			List<BorrowerBulkStandardGatherMoneyOrderM> gather_money = BorrowerBulkStandardGatherMoneyOrderM.dao.find(sql_list.toString(), params.toArray());
			for (BorrowerBulkStandardGatherMoneyOrderM model : gather_money) {
				Map<String, Object> model_inside = model.getM();
				int payment_state = model.getInt("payment_state");

				if (payment_state == 2) {
					Date finish_time = model.getTimestamp("finish_time");

					// 判断当前时间是否 是 筹满时间的5小时之内
					boolean full_state = (now.getTime() - finish_time.getTime()) < 5 * Hour;

					// 如果是筹满时间5小时之内，就在页面的显示状态改成 “已满标”
					if (full_state) {
						model_inside.put("payment_state", 1);
					}
				}

				models_inside.add(model_inside);
			}
		}
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bid_list", models_inside);
		String asyncPageDiv = AjaxAsyncPageDiv.getDiv(page.getTotalPageNum(), page.getPageSize(), page.getCurrentPageNum());
		map.put("asyncPageDiv", asyncPageDiv);
		renderText(gson.toJson(map));
		return;
	}

	public static long Seconds = 1000L;

	public static long Time_Of_Last_Update_$_get_loan_list_index = 0L;
	private static Lock lock_$_get_loan_list_index = new ReentrantLock();
	private static String msg_$_get_loan_list_index = null;

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "首页的3条散标", last_update_author = "lh")
	public void get_loan_list_index() {

		long now_time = System.currentTimeMillis();

		lock_$_get_loan_list_index.lock();
		try {
			if (Time_Of_Last_Update_$_get_loan_list_index == 0L || (msg_$_get_loan_list_index == null) || now_time - Time_Of_Last_Update_$_get_loan_list_index >= 2 * Seconds) {
				String data = get_loan_list_index_refresh(); // 获取数据
				msg_$_get_loan_list_index = data;
				Time_Of_Last_Update_$_get_loan_list_index = now_time;
				renderText(data);
				return;
			} else {
				renderText(msg_$_get_loan_list_index);
				return;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			lock_$_get_loan_list_index.unlock();
		}

	}

	private String get_loan_list_index_refresh() {

		Date now = new Date();
		String sql = "select * from borrower_bulk_standard_gather_money_order where 1=1 order by gather_state,id desc limit 5";
		List<BorrowerBulkStandardGatherMoneyOrderM> borrower_bulk_standard_gather_money_order_list = BorrowerBulkStandardGatherMoneyOrderM.dao.find(sql, new Object[] {});
		List<Map<String, Object>> gather_order_list = new ArrayList<Map<String, Object>>();

		for (BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order : borrower_bulk_standard_gather_money_order_list) {
			Map<String, Object> gather_order = borrower_bulk_standard_gather_money_order.getM();

			int payment_state = borrower_bulk_standard_gather_money_order.getInt("payment_state");
			if (payment_state == 2) {
				Date finish_time = borrower_bulk_standard_gather_money_order.getTimestamp("finish_time");

				// 判断当前时间是否 是 筹满时间的5小时之内
				boolean full_state = (now.getTime() - finish_time.getTime()) < 5 * Hour;

				// 如果是筹满时间5小时之内，就在页面的显示状态改成 “已满标”
				if (full_state) {
					gather_order.put("payment_state", 1);
				}
			}

			gather_order_list.add(gather_order);
		}
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("gather_order_list", gather_order_list);
		String str = gson.toJson(map);
		return str;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "首页的3条散标", last_update_author = "lh")
	public void loan_list_query_index_page() {

		String sql = "select * from borrower_bulk_standard_gather_money_order where 1=1 order by gather_state,id desc limit 3";
		List<BorrowerBulkStandardGatherMoneyOrderM> borrower_bulk_standard_gather_money_order_list = BorrowerBulkStandardGatherMoneyOrderM.dao.find(sql, new Object[] {});
		List<Map<String, Object>> gather_order_list = new ArrayList<Map<String, Object>>();

		for (BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order : borrower_bulk_standard_gather_money_order_list) {
			Map<String, Object> gather_order = borrower_bulk_standard_gather_money_order.getM();
			gather_order_list.add(gather_order);
		}
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("gather_order_list", gather_order_list);
		renderText(gson.toJson(map));
		return;

	}

	@Deprecated
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "jsonp格式demo", last_update_author = "lh")
	public void jsonp_demo() {
		String sql = "SELECT * FROM borrower_bulk_standard_gather_money_order limit 1";
		BorrowerBulkStandardGatherMoneyOrderM gather_money = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst(sql);
		String callback = getParameter("callback");
		Map<String, Object> model_inside = gather_money.getM();
		Gson gson = new Gson();
		String str = gson.toJson(model_inside);
		String jsonp_str = callback + "(" + str + ")";
		renderText(jsonp_str);
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入理财详情", last_update_author = "lh")
	public void loan_detail() {
		int gather_order_id = getParameterToInt("gather_order_id", 0); // 筹款单id
		if (gather_order_id == 0) {
			renderText("非法输入");
			return;
		}
		String sql = "select aa.*,bb.nickname,cc.describe from borrower_bulk_standard_gather_money_order aa , user_info bb , borrower_bulk_standard_apply_order cc where aa.user_id = bb.id and aa.apply_order_id=cc.id and aa.id = ?";
		Record gather_order = Db.findFirst(sql, gather_order_id);

		BigDecimal remain_money = gather_order.getBigDecimal("remain_money").setScale(2, BigDecimal.ROUND_DOWN);
		BigDecimal monthly_principal_and_interest = gather_order.getBigDecimal("monthly_principal_and_interest").setScale(2, BigDecimal.ROUND_DOWN);

		setSessionAttribute("remain_money", remain_money);
		setSessionAttribute("monthly_principal_and_interest", monthly_principal_and_interest);

		if (gather_order == null) {
			renderText("查询不到凑集单信息");
			return;
		}

		int gather_state = gather_order.getInt("gather_state");
		int payment_state = gather_order.getInt("payment_state");

		setAttribute("gather_order", gather_order);

		int user_id = gather_order.getInt("user_id");

		UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);
		BorrowerBulkStandardOrderUserInfoM borrower_bulk_standard_order_user_info = BorrowerBulkStandardOrderUserInfoM.get_last_order_user_info(user_id);

		setAttribute("user_credit_files", user_credit_files);
		setAttribute("borrower_bulk_standard_order_user_info", borrower_bulk_standard_order_user_info);

		if (gather_state == 1) { // 投标中

			String sql_deadline = "select deadline from borrower_bulk_standard_gather_money_order where id = ? ";
			Date deadline = Db.queryTimestamp(sql_deadline, new Object[] { gather_order_id });
			Date now = new Date();

			int remain_second = (int) ((deadline.getTime() - now.getTime()) / 1000); // 距截止时间剩余的秒数
			int day = remain_second / (60 * 60 * 24);
			int hour = remain_second % (60 * 60 * 24) / (60 * 60);
			int minute = remain_second % (60 * 60) / 60;
			int second = remain_second % 60;

			setSessionAttribute("day", day);
			setSessionAttribute("hour", hour);
			setSessionAttribute("minute", minute);

			renderJsp("/user/financial/loan_detail_gather.jsp");
			return;
		} else if (gather_state == 2) { // 筹款失败
			renderJsp("/user/financial/loan_detail_fail.jsp");
			return;
		} else if (gather_state == 3) { // 筹款成功
			if (payment_state == 1) {
				renderJsp("/user/financial/loan_detail_full.jsp");
				return;
			} else if (payment_state == 2) { // 还款中 还要查询一些还款信息

				// 待还本息
				String daihuanbenxi_sql = "SELECT SUM(should_repayment_total) FROM borrower_bulk_standard_repayment_plan WHERE is_repay = 0 AND gather_money_order_id = ? ";
				BigDecimal benxi_to_repay = Db.queryBigDecimal(daihuanbenxi_sql, new Object[] { gather_order_id });

				//
				String sql_ = "select current_period,total_periods,automatic_repayment_date from borrower_bulk_standard_repayment_plan where SYSDATE() < repay_end_time and gather_money_order_id = ? order by repay_end_time limit 1";
				Record record = Db.findFirst(sql_, new Object[] { gather_order_id });
				long current_period = record.getLong("current_period");
				int total_periods = record.getInt("total_periods");
				int remain_period = (int) (total_periods - current_period + 1);
				String automatic_repayment_date = record.getString("automatic_repayment_date");

				setSessionAttribute("benxi_to_repay", benxi_to_repay);
				setSessionAttribute("remain_period", remain_period);
				setSessionAttribute("automatic_repayment_date", automatic_repayment_date);
				renderJsp("/user/financial/loan_detail_repay.jsp");
				return;
			} else if (payment_state == 3) { // 还款完成

			}
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "获取用户可用余额-不需要登录", last_update_author = "lh")
	public void get_money_can_use() {
		UserM user = getSessionAttribute("user");
		if (user == null) {
			renderText("0");
			return;
		} else {
			int user_id = user.getInt("id");
			String sql = "select cny_can_used from user_now_money where user_id =?";
			BigDecimal money_can_use = Db.queryBigDecimal(sql, new Object[] { user_id });
			money_can_use = money_can_use.setScale(2, BigDecimal.ROUND_DOWN);
			String money_can_use_str = String.valueOf(money_can_use);
			renderText(money_can_use_str);
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "检测资金是否足够", last_update_author = "lh")
	public void check_enough_money() {

		String param = getParameter("invest_money");
		BigDecimal invest_money = new BigDecimal("0");
		if (!"NaN".equals(param)) {
			invest_money = new BigDecimal(getParameter("invest_money"));
		}
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		String sql = "select cny_can_used from user_now_money where user_id =?";
		BigDecimal money_can_use = Db.queryBigDecimal(sql, new Object[] { user_id });
		int result = money_can_use.compareTo(invest_money);
		if (result >= 0) {
			renderText("1");// 可以投资
			return;
		} else {
			renderText("2");// 资金不足
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "到投资新手标页面", last_update_author = "lh")
	public void to_new_bid() {
		int newer_gather_id = getParameterToInt("newer_gather_id");// 新手标筹集单id

		String sql = "select * from lender_bulk_standard_newer_bid_gather where id = ?";
		LenderBulkStandardNewerBidGatherM lender_bulk_standard_newer_bid_gather = LenderBulkStandardNewerBidGatherM.dao.findFirst(sql, new Object[] { newer_gather_id });

		setAttribute("newer_bid_gather", lender_bulk_standard_newer_bid_gather);

		renderJsp("/user/financial/elite_new_bid_detail.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "确认投资散标新手标", last_update_author = "lh")
	public void confirm_elite_new_bid() throws Exception {

		String check_code = getParameter("check_code");
		String code = getSessionAttribute("code");

		if (!(Utils.isNotNullAndNotEmptyString(check_code) && Utils.isNotNullAndNotEmptyString(code) && check_code.equalsIgnoreCase(code))) {
			renderText("1");// 验证码错误
			return;
		}

		final long newer_gather_id = getParameterToLong("newer_gather_id");// 新手标筹集单id
		final String sql_newer_gather_order = "select * from lender_bulk_standard_newer_bid_gather where id = ? for update";

		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");

		final String sql_count = "select count(1) from lender_bulk_standard_newer_bid_record where user_id = ? ";
		long count = Db.queryLong(sql_count, new Object[] { user_id });

		final AtomicReference<String> error_code = new AtomicReference<String>();

		if (count != 0) {
			renderText("2"); // 已经投过新手标，不能再投了
			return;
		} else {

			boolean isOk = Db.tx(new IAtomic() {

				@SuppressWarnings("deprecation")
				public boolean transactionProcessing() throws Exception {
					Date now = new Date();
					Date repay_date = new Date(now.getTime() + Day);// 还款日期
					String repay_date_str = Model.Date.format(repay_date);

					boolean ok0 = true;

					LenderBulkStandardNewerBidGatherM lender_bulk_standard_newer_bid_gather = LenderBulkStandardNewerBidGatherM.dao.findFirst(sql_newer_gather_order, new Object[] { newer_gather_id });
					int is_full = lender_bulk_standard_newer_bid_gather.getInt("is_full");
					if (is_full == 1) {
						error_code.set("5"); // 该标已满，请投下一个新手标
						return false;
					}

					// 已筹到金额
					BigDecimal have_gather_money_before = lender_bulk_standard_newer_bid_gather.getBigDecimal("have_gather_money");
					// 增加后的已筹到金额
					BigDecimal have_gather_money_after = have_gather_money_before.add(new BigDecimal("20000"));

					// 剩余金额
					BigDecimal remain_money_before = lender_bulk_standard_newer_bid_gather.getBigDecimal("remain_money");
					// 减少剩余金额
					BigDecimal remain_money_after = remain_money_before.subtract(new BigDecimal("20000"));

					// 剩余份额
					int remain_share_befor = lender_bulk_standard_newer_bid_gather.getInt("remain_share");
					// 减少剩余份额
					int remain_share_after = remain_share_befor - 400;

					// 筹集进度
					int gather_progress = lender_bulk_standard_newer_bid_gather.getInt("gather_progress");
					// 加入人数
					int have_join_people_number = lender_bulk_standard_newer_bid_gather.getInt("have_join_people_number");

					lender_bulk_standard_newer_bid_gather.set("have_gather_money", have_gather_money_after);
					lender_bulk_standard_newer_bid_gather.set("remain_money", remain_money_after);
					lender_bulk_standard_newer_bid_gather.set("remain_share", remain_share_after);
					lender_bulk_standard_newer_bid_gather.set("gather_progress", gather_progress + 4);
					lender_bulk_standard_newer_bid_gather.set("have_join_people_number", have_join_people_number + 1);

					if (have_join_people_number == 24) { // 此人投了之后 ，就满了

						String repay_str = Model.Date.format(repay_date); // 还款日期字符串
						lender_bulk_standard_newer_bid_gather.set("gather_full_time", now);
						lender_bulk_standard_newer_bid_gather.set("gather_full_time_long", now.getTime());
						// lender_bulk_standard_newer_bid_gather.set("repayment_date",repay_str);
						lender_bulk_standard_newer_bid_gather.set("is_full", 1);// 已经投满

						// 添加筹集单等待表的记录
						/*
						 * LenderBulkStandardNewerBidGatherWaitingM
						 * lender_bulk_standard_newer_bid_gather_waiting = new
						 * LenderBulkStandardNewerBidGatherWaitingM();
						 * lender_bulk_standard_newer_bid_gather_waiting
						 * .set("newer_bid_gather_id",newer_gather_id);
						 * lender_bulk_standard_newer_bid_gather_waiting
						 * .set("recieve_date",repay_str); ok0 =
						 * lender_bulk_standard_newer_bid_gather_waiting.save();
						 */
					}

					boolean ok1 = lender_bulk_standard_newer_bid_gather.update();

					// 添加新手标投标记录
					LenderBulkStandardNewerBidRecordM lender_bulk_standard_newer_bid_record = new LenderBulkStandardNewerBidRecordM();

					lender_bulk_standard_newer_bid_record.set("user_id", user_id);
					lender_bulk_standard_newer_bid_record.set("newer_bid_gather_id", newer_gather_id);
					lender_bulk_standard_newer_bid_record.set("bid_money", 20000);
					lender_bulk_standard_newer_bid_record.set("annulize_rate", 0.16);
					lender_bulk_standard_newer_bid_record.set("add_time", now);
					lender_bulk_standard_newer_bid_record.set("add_time_long", now.getTime());
					lender_bulk_standard_newer_bid_record.set("recieve_interest", 8.8);
					lender_bulk_standard_newer_bid_record.set("recieve_time", repay_date);
					lender_bulk_standard_newer_bid_record.set("recieve_time_long", repay_date.getTime());
					lender_bulk_standard_newer_bid_record.set("recieve_date", repay_date_str);
					lender_bulk_standard_newer_bid_record.set("is_return_interest", 0);

					boolean ok2 = lender_bulk_standard_newer_bid_record.save();

					// 添加新手标投标记录等待表
					LenderBulkStandardNewerBidRecordWaitingM lender_bulk_standard_newer_bid_record_waiting = new LenderBulkStandardNewerBidRecordWaitingM();

					lender_bulk_standard_newer_bid_record_waiting.set("newer_bid_record_id", lender_bulk_standard_newer_bid_record.getLong("id"));
					lender_bulk_standard_newer_bid_record_waiting.set("recieve_date", repay_date_str);

					boolean ok3 = lender_bulk_standard_newer_bid_record_waiting.save();

					if (ok0 && ok1 && ok2 && ok3) {
						return true;
					} else {
						return false;
					}
				}
			});

			if (isOk) {
				renderText("3");// 投标成功
				return;
			} else {
				String error = error_code.get();
				renderText(error);// 投标失败
				return;
			}

		}

	}

	/**
	 *<pre>
	 *      执行存储过程的例子-标满
	 * 		ConsoleInit.initTables();
	 * 		Map<Integer, Object> in = new HashMap<Integer, Object>();
	 * 		in.put(1, 1);// 入参位置和数值
	 * 		Map<Integer, Integer> out = new HashMap<Integer, Integer>();
	 * 		out.put(2, Types.INTEGER);// 出参数位置和类型
	 * 		Map<Integer, Object> map =ProcedureCall.call("{call procedure_gather_money_order_waiting_success_handling(?,?)}", in, out);
	 * 		System.out.println(map .get(2));
	 *</pre>
	 */

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "是否为理财账户", last_update_author = "lh")
	public void judge_borrower() {

		UserM user = (UserM) getSessionAttribute("user");
		int user_type = user.getInt("user_type");
		if (user_type == 1) { // 是理财用户
			renderText("1");
			return;
		} else {
			renderText("2");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class, LenderAop.class })
	@Function(for_people = "所有人", function_description = "确认投资散标", last_update_author = "lh")
	public void confirm_elite_bid() throws Exception {

		UserM user = (UserM) getSessionAttribute("user");
		final int lend_user_id = user.getInt("id");// 理财人id

		String check_code = getParameter("check_code");
		String code = getSessionAttribute("code");

		if (!(Utils.isNotNullAndNotEmptyString(check_code)//
				&& //
				Utils.isNotNullAndNotEmptyString(code)//
		&& check_code.equalsIgnoreCase(code))) {
			renderText("1");// 验证码错误
			return;
		}

		final int gather_order_id = getParameterToInt("gather_order_id", 0);// 筹款单id
		if (gather_order_id == 0) {
			renderText("非法参数");//
			return;
		}
		int invest_money_int = getParameterToInt("invest_money", 0);
		if (invest_money_int == 0) {
			renderText("非法参数");//
			return;
		}
		if (invest_money_int % 50 != 0) {
			renderText("非法参数");//
			return;
		}
		final BigDecimal invest_money = new BigDecimal(invest_money_int + ""); // 投资的金额
		final int invest_share = invest_money_int / Each_Debt_Value; // 投资的份额

		/**
		 * 判断资金够不
		 */

		String sql = "select cny_can_used from user_now_money where user_id = ? ";
		BigDecimal money_can_use = Db.queryBigDecimal(sql, new Object[] { lend_user_id });
		int result = money_can_use.compareTo(invest_money);
		if (result < 0) {
			renderText("2"); // 你的余额不足，请重新填写投资金额
			return;
		}

		final AtomicReference<String> error_code = new AtomicReference<String>("3");// 框架报错

		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				// 处理筹款单
				// 线程安全获取筹款单信息，加上等待超时功能
				BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.get_borrower_bulk_standard_gather_money_order_short_wait(gather_order_id);
				if (borrower_bulk_standard_gather_money_order == null) {
					error_code.set("4");// 请求超时，请重新操作
					return false;
				}
				int borrow_user_id = borrower_bulk_standard_gather_money_order.getInt("user_id");// 筹款人id

				@SuppressWarnings("unused")
				BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");
				int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");

				BigDecimal have_gather_money = borrower_bulk_standard_gather_money_order.getBigDecimal("have_gather_money");// 已筹集的金额
				int have_gather_share = borrower_bulk_standard_gather_money_order.getInt("have_gather_share");// 已筹集的份额

				BigDecimal remain_money = borrower_bulk_standard_gather_money_order.getBigDecimal("remain_money");// 剩余金额
				int remain_share = borrower_bulk_standard_gather_money_order.getInt("remain_share");// 剩余份额

				int gather_progress = ((have_gather_share + invest_share) * 100) / borrow_all_share;// 筹集进度-先*100防止计算精度失败
				int gather_progress_level = 0;// 筹集进度分类
				if (gather_progress < 50) {
					gather_progress_level = 1;
				} else if ((50 <= gather_progress) && gather_progress < 80) {
					gather_progress_level = 2;
				} else if ((80 <= gather_progress) && gather_progress < 100) {
					gather_progress_level = 3;
				} else if (gather_progress == 100) {
					gather_progress_level = 4;
				}

				int join_people_num = borrower_bulk_standard_gather_money_order.getInt("join_people_num");

				/**
				 *优先级 凑集时间>>凑集状态>>剩下的份额进行
				 */

				Date deadline = borrower_bulk_standard_gather_money_order.getDate("deadline");// 筹款截止时间
				Date now = new Date();
				if (now.compareTo(deadline) > 0) { // 该筹款单已经过期，不能投资
					error_code.set("5");
					return false;
				}
				int gather_state = borrower_bulk_standard_gather_money_order.get("gather_state");
				if (gather_state != 1) {
					error_code.set("6");
					return false;
				}
				if (remain_share < invest_share) { // 剩余的份额不足，不能投资
					error_code.set("7");
					return false;
				}

				borrower_bulk_standard_gather_money_order.set("have_gather_money", have_gather_money.add(invest_money));// 增加已筹集金额
				borrower_bulk_standard_gather_money_order.set("have_gather_share", have_gather_share + invest_share);// 增加已筹集金额

				borrower_bulk_standard_gather_money_order.set("remain_money", remain_money.subtract(invest_money));// 减少剩余金额
				borrower_bulk_standard_gather_money_order.set("remain_share", remain_share - invest_share); // 减少剩余份额

				borrower_bulk_standard_gather_money_order.set("gather_progress", gather_progress); // 筹集进度
				borrower_bulk_standard_gather_money_order.set("gather_progress_level", gather_progress_level);
				borrower_bulk_standard_gather_money_order.set("join_people_num", join_people_num + 1); // 加入人次加1

				if (remain_share == invest_share) { // 如果投资金额正好=剩余金额,就是筹集成功

					Date add_time = borrower_bulk_standard_gather_money_order.getDate("add_time"); // 筹款开始时间
					int full_scale_use_time = (int) ((now.getTime() - add_time.getTime()) / 1000); // 满标用时（秒）
					borrower_bulk_standard_gather_money_order.set("full_scale_use_time", full_scale_use_time);
					borrower_bulk_standard_gather_money_order.set("finish_time", now);

				}
				boolean is_ok_update_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.update();

				// 添加 理财人 投资订单表
				LenderBulkStandardOrderM lender_bulk_standard_order = new LenderBulkStandardOrderM();
				lender_bulk_standard_order.set("gather_money_order_id", gather_order_id);
				lender_bulk_standard_order.set("lend_user_id", lend_user_id); // 理财人id
				lender_bulk_standard_order.set("borrow_user_id", borrow_user_id);// 借款人id
				lender_bulk_standard_order.set("invest_money", invest_money);
				lender_bulk_standard_order.set("invest_share", invest_share);
				lender_bulk_standard_order.set("bid_time", new Date());
				lender_bulk_standard_order.set("bid_time_long", System.currentTimeMillis());
				lender_bulk_standard_order.set("state", 1);

				boolean is_ok_save_lender_bulk_standard_order = lender_bulk_standard_order.save();

				// 处理 理财人 资金表
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(lend_user_id);
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");

				BigDecimal cny_can_used_later = cny_can_used.subtract(invest_money);
				BigDecimal cny_freeze_later = cny_freeze.add(invest_money);

				user_now_money.set("cny_can_used", cny_can_used_later);
				user_now_money.set("cny_freeze", cny_freeze_later);

				boolean is_ok_update_user_now_money = user_now_money.update();
				if (is_ok_update_borrower_bulk_standard_gather_money_order && is_ok_save_lender_bulk_standard_order && is_ok_update_user_now_money) {
					error_code.set("8");
					return true;
				} else {
					error_code.set("9");
					return false;
				}
			}
		});

		if (is_ok) {
			renderText(error_code.get());
			CheckCodeServlet.clearCode(getRequest());
			return;
		} else {
			renderText(error_code.get());
			return;

		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "异步获取散标的投标记录", last_update_author = "lh")
	public void get_bid_record() {
		int gather_order_id = getParameterToInt("gather_order_id");

		String sql = "SELECT b.nickname,a.invest_money,a.bid_time from lender_bulk_standard_order a,user_info b where a.lend_user_id=b.id and a.gather_money_order_id = ?";

		String sql_person_time = "select count(1) from lender_bulk_standard_order where gather_money_order_id = ? ";

		String sql_sum_invest_money = "select sum(invest_money) from lender_bulk_standard_order where gather_money_order_id = ?";

		String sql_full_scale_use_time = "select full_scale_use_time from borrower_bulk_standard_gather_money_order where id = ?";

		List<Record> bid_records = Db.find(sql, new Object[] { gather_order_id });

		long person_time = Db.queryLong(sql_person_time, new Object[] { gather_order_id });

		BigDecimal sum_invest_money = Db.queryBigDecimal(sql_sum_invest_money, new Object[] { gather_order_id });
		if (sum_invest_money == null) {
			sum_invest_money = new BigDecimal("0");
		}

		int full_scale_use_time = Db.queryInt(sql_full_scale_use_time, new Object[] { gather_order_id });
		int day = full_scale_use_time / (3600 * 24);
		int hour = (full_scale_use_time % (3600 * 24)) / 3600;
		int minute = (full_scale_use_time % 3600) / 60;
		int second = full_scale_use_time % 60;
		String full_scale_use_time_str = hour + "时" + minute + "分" + second + "秒";

		List<Map<String, Object>> records_inside = new ArrayList<Map<String, Object>>();
		for (Record bid_record : bid_records) {
			Map<String, Object> record_inside = bid_record.getR();
			records_inside.add(record_inside);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bid_record", records_inside);
		map.put("person_time", person_time);
		map.put("sum_invest_money", sum_invest_money);
		map.put("full_scale_use_time_str", full_scale_use_time_str);

		Gson gson = new Gson();
		String str = gson.toJson(map);
		renderText(str);
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "异步获取散标的还款表现", last_update_author = "lh")
	public void get_repayment_behavior() {
		int gather_order_id = getParameterToInt("gather_order_id");

		String str = CommonRepayMethod.get_repayment_info(gather_order_id);
		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "校验验证码是否正确", last_update_author = "lh")
	public void verify_check_code() {
		String check_code = getParameter("check_code");
		String code = getSessionAttribute("code");
		if (code.equalsIgnoreCase(check_code)) {
			renderText("");
			return;
		} else {
			renderText("验证码输入错误");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "生产散标借款协议范本", last_update_author = "lh")
	public void to_loan_contract() {
		renderJsp("/user/financial/loan_contract.jsp");
		return;
	}

	/**
	 * 指定联富宝 指定期数的 协议范本
	 * 
	 * @throws UnsupportedEncodingException
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "所有人", function_description = "生产联富宝协议范本", last_update_author = "hx")
	public void get_lfoll_fix_bid_contract() throws UnsupportedEncodingException {
		// Integer id = getParameterToInt("id");
		String bid_name_str = getParameter("bid_name");
		String bid_name = new String(bid_name_str.getBytes("ISO-8859-1"), "UTF-8");
		if (bid_name != null && bid_name != "") {
			setAttribute("bid_name", bid_name);
		}
		UserM userM = getSessionAttribute("user");
		if (userM != null) {
			setAttribute("userM", userM);
		}
		renderJsp("/user/lfoll_invest/lfoll_invest_contract.jsp");
		return;
	}

	public static void main(String[] args) throws Exception {
		/**
		 * 执行存储过程的例子
		 */
		ConsoleInit.initTables();
		Map<Integer, Object> in = new HashMap<Integer, Object>();
		in.put(1, 1);// 入参位置和数值
		Map<Integer, Integer> out = new HashMap<Integer, Integer>();
		out.put(2, Types.INTEGER);// 出参数位置和类型
		Map<Integer, Object> map = ProcedureCall.call("{call procedure_gather_money_order_waiting_success_handling(?,?)}", in, out);
		System.out.println(map.get(2));
	}

}
