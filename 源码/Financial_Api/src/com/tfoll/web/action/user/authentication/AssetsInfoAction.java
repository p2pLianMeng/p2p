package com.tfoll.web.action.user.authentication;

import com.tfoll.tcache.action.Utils;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.action.user.financial.BorrowAction;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateAssetsInfoM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.WebLogRecordsUtil;

@ActionKey("/user/authentication/assets_info")
public class AssetsInfoAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "跳转到资产信息认证页面", last_update_author = "zjb")
	public void assets_index() throws Exception {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		int borrow_type = user.getInt("borrow_type");
		int rate = UserAuthenticateLeftStatusM.get_rate(user_id);

		setAttribute("rate", rate);
		setAttribute("the_id", "assets");
		setAttribute("borrow_type", borrow_type + "");

		// 判断是否有状态为 1 的订单
		String apply_order_id_string = BorrowAction.get_value_from_request_and_session_of_apply_order_id();
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}

		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		// 单子状态和关联保存的申请单ID
		setAttribute("apply_order_id", apply_order_id);
		setAttribute("apply_order_state", state);// 如果是2则要提示在多久后系统会自动提交这个单子

		UserAuthenticateAssetsInfoM user_authenticate_assets_info = UserAuthenticateAssetsInfoM.dao.findFirst("select * from user_authenticate_assets_info where user_id = ? ", user_id);
		setAttribute("user_authenticate_assets_info", user_authenticate_assets_info);

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		setAttribute("user_authenticate_left_status", user_authenticate_left_status);

		renderJsp("/user/authenticate/assets.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "资产信息认证页面", last_update_author = "zjb")
	public void save_assets_info() throws Exception {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		// 判断是否有状态为 1 的订单
		long apply_order_id = getParameterToInt("apply_order_id", 0);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		String housing = getParameter("housing");
		String housing_mortgage = getParameter("housing_mortgage");

		String car = getParameter("car");
		String car_mortgage = getParameter("car_mortgage");

		UserAuthenticateAssetsInfoM user_authenticate_assets_info = UserAuthenticateAssetsInfoM.dao.findFirst("select * from user_authenticate_assets_info where user_id = ? ", user_id);
		boolean assets_info_ok = user_authenticate_assets_info.set("housing", housing).set("housing_mortgage", housing_mortgage).set("car", car).set("car_mortgage", car_mortgage).update();

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		int assets_info_status = 1;
		boolean left_status_ok = user_authenticate_left_status.set("assets_info_status", assets_info_status).update();

		if (assets_info_ok && left_status_ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

}
