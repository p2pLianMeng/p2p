package com.tfoll.web.action.user.usercenter;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.SysIncomeRecordsM;
import com.tfoll.web.model.UserBankInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMessageRequestLogM;
import com.tfoll.web.model.UserMessageRequestWaitingM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.model.UserWithdrawCashLogM;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ActionKey("/user/usercenter/withdrawal")
public class UserWithdrawalAction extends Controller {

	/**
	 * 跳转都人民币体现页面
	 */
	@SuppressWarnings("static-access")
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "跳到用户提现页面", last_update_author = "向旋")
	public void to_user_withdraw() {
		int pn = getParameterToInt("pn", 1);
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int id = user.getInt("id");
		UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(id);
		UserBankInfoM user_bank_info = UserBankInfoM.dao.findById(user.getInt("id"));
		String bank_card_num_one = user_bank_info.getString("bank_card_num_one");
		setSessionAttribute("bank_card_num_one", bank_card_num_one);
		String bank_card_num_two = user_bank_info.getString("bank_card_num_two");
		setSessionAttribute("bank_card_num_two", bank_card_num_two);
		String bank_card_num_three = user_bank_info.getString("bank_card_num_three");
		setSessionAttribute("bank_card_num_three", bank_card_num_three);
		if (!Utils.isNotNullAndNotEmptyString(bank_card_num_one) && !Utils.isNotNullAndNotEmptyString(bank_card_num_two) && !Utils.isNotNullAndNotEmptyString(bank_card_num_three)) {
			renderAction("/user/usercenter/bank_info/bank_info_index");
			return;
		}
		if (Utils.isNotNullAndNotEmptyString(bank_card_num_one)) {
			String bank_card_num_one_1 = bank_card_num_one.substring(0, 4);
			String bank_card_num_one_2 = bank_card_num_one.substring(bank_card_num_one.length() - 4, bank_card_num_one.length());
			String bank_card_num_one_3 = bank_card_num_one_1 + " **** **** " + bank_card_num_one_2;
			user_bank_info.set("bank_card_num_one", bank_card_num_one_3);
		}
		if (Utils.isNotNullAndNotEmptyString(bank_card_num_two)) {
			String bank_card_num_two_1 = bank_card_num_two.substring(0, 4);
			String bank_card_num_two_2 = bank_card_num_two.substring(bank_card_num_two.length() - 4, bank_card_num_two.length());
			String bank_card_num_two_3 = bank_card_num_two_1 + " **** **** " + bank_card_num_two_2;
			user_bank_info.set("bank_card_num_two", bank_card_num_two_3);
		}

		if (Utils.isNotNullAndNotEmptyString(bank_card_num_three)) {
			String bank_card_num_three_1 = bank_card_num_three.substring(0, 4);
			String bank_card_num_three_2 = bank_card_num_three.substring(bank_card_num_three.length() - 4, bank_card_num_three.length());
			String bank_card_num_three_3 = bank_card_num_three_1 + " **** **** " + bank_card_num_three_2;
			user_bank_info.set("bank_card_num_three", bank_card_num_three_3);
		}
		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
		cny_can_used = cny_can_used.setScale(2, BigDecimal.ROUND_DOWN);
		user_now_money.set("cny_can_used", cny_can_used);
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DAY_OF_MONTH, 2);
		String d = Model.Date.format(calendar.getTime());
		setAttribute("d", d);
		setSessionAttribute("user_now_money", user_now_money);
		setSessionAttribute("user_bank_info", user_bank_info);

		StringBuilder sql_count = new StringBuilder("select COUNT(1) from user_withdraw_cash_log w,user_info u where w.user_id = u.id");
		StringBuilder sql_select = new StringBuilder("select u.id uid,w.*  from user_withdraw_cash_log w,user_info u where w.user_id = u.id ");
		List<Object> parma = new ArrayList<Object>();
		sql_count.append(" and u.id = ? ");
		sql_select.append(" and u.id = ? order by w.add_time desc");
		parma.add(id);

		Long totalRow = Db.queryLong(sql_count.toString(), parma.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn, 8);
			List<Record> user_withdraw_list = Db.find(page.creatLimitSql(sql_select.toString()), parma.toArray());
			for (Record record : user_withdraw_list) {
				BigDecimal amount = record.getBigDecimal("amount");
				amount = amount.setScale(2, BigDecimal.ROUND_DOWN);
				Date add_time = record.getTimestamp("add_time");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String time = sdf.format(add_time);
				// String thrount_time = record.getString("thrount_time");
				// thrount_time = thrount_time.substring(0,10);
				record.add("amount", amount);
				record.add("add_time", time);
			}
			setAttribute("user_withdraw_list", user_withdraw_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/usercenter/withdrawal/to_user_withdraw");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/usercenter/account_withdraw.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "用户提现", last_update_author = "向旋")
	public void user_withdraw() {
		final UserM user = getSessionAttribute(SystemConstantKey.User);
		int id = user.getInt("id");
		/**
		 * 系统账户 不能提现
		 */
		if (id == 1 || id == 2 || id == 3 || id == 4) {
			renderText("");
			return;
		}
		final UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user.getInt("id"));
		final BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
		if (cny_can_used.compareTo(new BigDecimal("0")) < 0) {
			renderText("1");
			return;
		}
		final BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
		if (cny_freeze.compareTo(new BigDecimal("0")) < 0) {
			renderText("2");
			return;
		}
		final int card = getParameterToInt("card");
		if (card != 1 && card != 2 && card != 3) {
			renderText("3");
			return;
		}
		final String account_holder = user.getString("real_name");
		if (!Utils.isNotNullAndNotEmptyString(account_holder)) {
			renderText("4");
			return;
		}

		final String amount = getParameter("amount");
		if (!Utils.isNotNullAndNotEmptyString(amount)) {
			renderText("5");
			return;
		}
		if (cny_can_used.compareTo(new BigDecimal(amount)) < 0) {
			renderText("6");
			return;
		}
		String password = getParameter("password");
		if (!Utils.isNotNullAndNotEmptyString(password)) {
			renderText("10");
			return;
		}
		if (!MD5.md5(password).equals(user.getString("money_password"))) {
			renderText("11");
			return;
		}
		final String fee = getParameter("fee");
		boolean withdraw_isok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				Date d = new Date();

				UserWithdrawCashLogM user_withdraw_cash_log = new UserWithdrawCashLogM();
				String bank_name = null;
				String bank_address = null;
				String bank_card_num = null;
				if (card == 1) {
					bank_name = getParameter("bank_name_one");
					bank_address = getParameter("bank_address_one");
					bank_card_num = getSessionAttribute("bank_card_num_one");
				} else if (card == 2) {
					bank_name = getParameter("bank_name_two");
					bank_address = getParameter("bank_address_two");
					bank_card_num = getSessionAttribute("bank_card_num_two");
				} else if (card == 3) {
					bank_name = getParameter("bank_name_three");
					bank_address = getParameter("bank_address_three");
					bank_card_num = getSessionAttribute("bank_card_num_three");
				}
				boolean save_ok = user_withdraw_cash_log.set("user_id", user.getInt("id")).set("add_time_ux", System.currentTimeMillis()).set("account_holder", account_holder).set("account_holder", account_holder).set("bank_name", bank_name).set("bank_address", bank_address).set("bank_card_num", bank_card_num).set("balance", cny_can_used).set("amount", amount).set("fee", fee).set("add_time",
						new Date()).set("is_through", 1).save();
				int id = user_withdraw_cash_log.getInt("id");

				SysIncomeRecordsM sys_income_record = new SysIncomeRecordsM();
				boolean is_ok_add_sys_income_record_ok = sys_income_record.set("user_id", user.getInt("id")).//
						set("type", UserMoneyChangeRecordsM.Type_14).//
						set("money", fee).//
						set("detail", "提现手续费##" + id).//
						set("add_time_long", d.getTime()).//
						set("add_time_date", Date.format(d)).//
						save();
				boolean is_ok_add_user_money_change_record = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user.getInt("id"), UserMoneyChangeRecordsM.Type_13, cny_can_used, new BigDecimal(amount), new BigDecimal("0"), cny_can_used.subtract(new BigDecimal(amount)), "提现##" + id);
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user.getInt("id"), UserTransactionRecordsM.Type_12, cny_can_used, new BigDecimal(amount), new BigDecimal("0"), cny_can_used.subtract(new BigDecimal(amount)), "提现");
				boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used.subtract(new BigDecimal(amount))).update();

				if (!is_ok_add_user_money_change_record || !is_ok_add_sys_income_record_ok || !is_ok_update_user_now_money || !save_ok || !is_ok_add_user_transaction_records) {
					return false;
				}
				return true;
			}
		});

		if (!withdraw_isok) {
			renderText("8");
			return;
		}
		String[] a = new String[] { amount };
		boolean send_message = UserMessageRequestWaitingM.send_rmb_withdraw_message(user.getString("phone"), a, user.getInt("id"));
		if (send_message) {
			renderText("9");
			return;
		} else {
			renderText("20");
			boolean b = UserMessageRequestLogM.send_rmb_withdraw_message(user.getString("phone"), a, user.getInt("id"));
			if (b) {
				return;
			} else {
				return;
			}
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "用户提现撤单", last_update_author = "向旋")
	public void user_withdraw_revoke() {
		final int id = getParameterToInt("id");
		final UserWithdrawCashLogM user_withdraw_log = UserWithdrawCashLogM.dao.findById(id);
		final BigDecimal amount = user_withdraw_log.getBigDecimal("amount");
		final int user_id = user_withdraw_log.getInt("user_id");

		final int is_through = user_withdraw_log.getInt("is_through");
		boolean ok = Db.tx(new IAtomic() {
			public boolean transactionProcessing() throws Exception {
				boolean is_ok_update_user_money = false;
				boolean is_ok_update_user_withdraw_log = false;
				/**
				 * 防止重复撤销
				 */
				if (is_through == 1) {
					final UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user_id);
					BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
					BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");

					is_ok_update_user_money = user_now_money.set("cny_can_used", cny_can_used.add(amount)).update();
					is_ok_update_user_withdraw_log = user_withdraw_log.set("is_through", 2).update();

					boolean is_ok_add_user_money_change_record = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_15, cny_can_used.add(cny_freeze), new BigDecimal("0"), amount, cny_can_used.add(cny_freeze).add(amount), "提现撤销,id:" + id);
					boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_13, cny_can_used.add(cny_freeze), new BigDecimal("0"), amount, cny_can_used.add(cny_freeze).add(amount), "提现撤销");
					if (!is_ok_add_user_money_change_record) {
						return false;
					}
					if (!is_ok_add_user_transaction_records) {
						return false;
					}
				}
				return is_ok_update_user_money && is_ok_update_user_withdraw_log;
			}
		});
		if (ok) {
			renderAction("/user/usercenter/withdrawal/to_user_withdraw");
			return;
		} else {
			renderAction("/user/usercenter/withdrawal/to_user_withdraw");
			return;
		}
	}
}
