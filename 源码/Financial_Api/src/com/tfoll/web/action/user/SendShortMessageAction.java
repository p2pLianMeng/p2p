package com.tfoll.web.action.user;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMessageRequestWaitingM;
import com.tfoll.web.util.GetCode;
import com.tfoll.web.util.Utils;

@ActionKey("/user/sendmessage")
public class SendShortMessageAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "绑定手机发送短信验证码", last_update_author = "向旋")
	public void binding_phone_send_message() {
		String tel = getSessionAttribute("phone");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("4");
			return;
		}
		String code = GetCode.get_code();
		String[] a = new String[2];
		a[0] = code;
		setSessionAttribute("binding_phone", code);

		boolean send_ok = UserMessageRequestWaitingM.add_phone_message(tel, a, 0);
		if (!send_ok) {
			renderText("2");// 发送失败
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAjax.class)
	@Function(for_people = "所有人", function_description = "修改手机发送短信验证码", last_update_author = "向旋")
	public void update_phone_send_message() {
		String old_tel = getParameter("old_tel");
		if (!Utils.isNotNullAndNotEmptyString(old_tel)) {
			renderText("5");// 原手机号为空
			return;
		}
		UserM user2 = getSessionAttribute(SystemConstantKey.User);
		String phone = user2.getString("phone");
		if (!old_tel.equals(phone)) {
			renderText("6");// 原手机号填写错误
			return;
		}
		String tel = getParameter("tel");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("1");// 手机号为空
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where phone=?", tel);
		if (user != null) {
			renderText("2");// 该手机号已经被使用
			return;
		}
		String code = GetCode.get_code();
		String[] a = new String[2];
		a[0] = code;
		setSessionAttribute("update_phone_code", code);
		boolean send_ok = UserMessageRequestWaitingM.add_update_phone_message(old_tel, a, user2.getInt("id"));
		if (!send_ok) {
			renderText("3");
			return;
		}
		renderText("4");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "人民币提现发送短信验证码", last_update_author = "向旋")
	public void cny_withdraw_send_message() {
		String cny = getParameter("rmb");

		UserM user = getSessionAttribute(SystemConstantKey.User);

		if (user == null) {
			renderText("1");
			return;
		}
		String tel = user.getString("phone");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("2");//
			return;
		}
		String code = GetCode.get_code();
		String[] a = new String[2];
		a[0] = cny;
		a[1] = code;
		setSessionAttribute("cny_withdraw", code);
		boolean send_ok = UserMessageRequestWaitingM.add_cny_withdraw_message(tel, a, user.getInt("id"));
		if (!send_ok) {
			renderText("3");// 发送失败
			return;
		}
		renderText("4");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "绑定提现密码发送验证码", last_update_author = "向旋")
	public void binding_withdrow_password_send_message() {
		String withdraw_password = getParameter("withdraw_password");
		if (!Utils.isNotNullAndNotEmptyString(withdraw_password)) {
			renderText("1");
			return;
		}
		if (withdraw_password.length() < 6 || withdraw_password.length() > 35) {
			renderText("2");
			return;
		}
		String withdraw_password2 = getParameter("withdraw_password2");
		if (!Utils.isNotNullAndNotEmptyString(withdraw_password2)) {
			renderText("3");
			return;
		}
		if (!withdraw_password.equals(withdraw_password2)) {
			renderText("4");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String tel = user.getString("phone");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("5");
			return;
		}
		String code = GetCode.get_code();
		String[] a = new String[2];
		a[0] = code;
		setSessionAttribute("binding_withdraw_code", code);
		boolean send_ok = UserMessageRequestWaitingM.binding_withdraw_message(tel, a, user.getInt("id"));
		if (!send_ok) {
			renderText("6");
			return;
		}
		renderText("7");
		return;
	}

	// public static void main(String[] args) {
	// Date date = new Date();
	// SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
	// String d = sdf.format(date);
	// System.out.println(d);
	// SDKTestSendTemplateSMSUtil.sms_templateId("13697309712", "6668", new
	// String[]{d});
	// }
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "找回提现密码发送验证码", last_update_author = "向旋")
	public void back_withdraw_password_send_message() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		if (user == null) {
			renderText("1");
			return;
		}
		String phone = user.getString("phone");
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			renderText("4");
			return;
		}
		String code = GetCode.get_code();
		String[] a = new String[2];
		a[0] = code;
		setSessionAttribute("back_withdraw_code", code);
		boolean send_ok = UserMessageRequestWaitingM.back_withdraw_message(phone, a, user.getInt("id"));
		if (!send_ok) {
			renderText("2");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "登录", function_description = "找回登录密码发送验证码", last_update_author = "向旋")
	public void back_password_send_message() {
		String phone = getParameter("phone");
		UserM user = UserM.dao.findFirst("select * from user_info where phone = ?", phone);
		String code = GetCode.get_code();
		String[] a = new String[2];
		a[0] = code;
		setSessionAttribute("back_password_code", code);
		boolean send_ok = UserMessageRequestWaitingM.send_back_password_message(phone, a, user.getInt("id"));
		if (!send_ok) {
			renderText("2");
			return;
		}
		renderText("3");
		return;
	}
}
