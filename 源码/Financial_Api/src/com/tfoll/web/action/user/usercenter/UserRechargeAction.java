package com.tfoll.web.action.user.usercenter;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.IpUtil;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.SysErrorInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserRechargeLogM;
import com.tfoll.web.model.UserRechargeuserStreamLogM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

@ActionKey("/user/usercenter/recharge")
public class UserRechargeAction extends Controller {
	/**
	 * 保付提供的MD5加密方式-必须使用这个
	 */
	@Function(for_people = "所有人", function_description = "保付提供的MD5加密方式", last_update_author = "曹正辉")
	public static final String baofu_md5(String string) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(string.getBytes("utf-8"));
			byte[] digest = md5.digest();

			StringBuffer hex = new StringBuffer();
			String strTemp;
			int length = digest.length;
			for (int i = 0; i < length; i++) {
				strTemp = Integer.toHexString((digest[i] & 0x000000FF) | 0xFFFFFF00).substring(6);
				hex.append(strTemp);
			}
			return hex.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	/**
	 * 解析保付出响应的信息-必须使用这个
	 */
	@Function(for_people = "所有人", function_description = "解析保付出响应的信息", last_update_author = "曹正辉")
	public String getErrorInfo(String result, String resultDesc) {
		String retInfo = "";
		int a;
		int b;
		if (!"".equals(result)) {
			a = Integer.parseInt(result);
		} else {
			a = 0;
		}
		if (!"".equals(resultDesc)) {
			b = Integer.parseInt(resultDesc);
		} else {
			b = 0;
		}

		if (a == 1) {
			retInfo = "支付成功";
			return retInfo;
		} else {
			switch (b) {
			case 0:
				retInfo = "充值失败";
				break;
			case 1:
				retInfo = "系统错误";
				break;
			case 2:
				retInfo = "订单超时";
				break;
			case 11:
				retInfo = "系统维护";
				break;
			case 12:
				retInfo = "无效商户";
				break;
			case 13:
				retInfo = "余额不足";
				break;
			case 14:
				retInfo = "超过支付限额";
				break;
			case 15:
				retInfo = "卡号或卡密错误";
				break;
			case 16:
				retInfo = "不合法的IP地址";
				break;
			case 17:
				retInfo = "重复订单金额不符";
				break;
			case 18:
				retInfo = "卡密已被使用";
				break;
			case 19:
				retInfo = "订单金额错误";
				break;
			case 20:
				retInfo = "支付的类型错误";
				break;
			case 21:
				retInfo = "卡类型有误";
				break;
			case 22:
				retInfo = "卡信息不完整";
				break;
			case 23:
				retInfo = "卡号，卡密，金额不正确";
				break;
			case 24:
				retInfo = "不能用此卡继续做交易";
				break;
			case 25:
				retInfo = "订单无效";
				break;
			case 26:
				retInfo = "卡无效";
				break;
			default:
				retInfo = "支付失败";
				break;
			}

			return retInfo;
		}
	}

	/**
	 * <pre>
	 * <select name="PayID" id="PayID" >
	 *          只需要关注前十个
	 * 			<option value="1001">招商银行</option>
	 * 			<option value="1002">工商银行</option>
	 * 			<option value="1003">建设银行</option>
	 * 			<option value="1004">浦发银行</option>
	 * 			<option value="1005">农业银行</option>
	 * 			<option value="1006">民生银行</option>
	 * 			<option value="1009">兴业银行</option>
	 * 			<option value="1020">交通银行</option>
	 * 			<option value="1022">光大银行</option>
	 * 			<option value="1026">中国银行</option>
	 * 			<option value="1032">北京银行</option>		
	 * 			<option value="1035">平安银行</option>
	 * 			<option value="1036">广发银行</option>
	 * 			<option value="1039">中信银行</option>
	 * 			<option value="1080">银联在线</option>
	 * 			
	 * 			<option value="3001">招商银行(借)</option>
	 * 			<option value="3002">工商银行(借)</option>
	 * 			<option value="3003">建设银行(借)</option>
	 * 			<option value="3004">浦发银行(借)</option>
	 * 			<option value="3005">农业银行(借)</option>
	 * 			<option value="3006">民生银行(借)</option>
	 * 			<option value="3009">兴业银行(借)</option>
	 * 			<option value="3020">交通银行(借)</option>
	 * 			<option value="3022">光大银行(借)</option>
	 * 			<option value="3026">中国银行(借)</option>
	 * 			<option value="3032">北京银行(借)</option>		
	 * 			<option value="3035">平安银行(借)</option>
	 * 			<option value="3036">广发银行(借)</option>
	 * 			<option value="3039">中信银行(借)</option>
	 * 
	 * 			<option value="4001">招商银行(贷)</option>
	 * 			<option value="4002">工商银行(贷)</option>
	 * 			<option value="4003">建设银行(贷)</option>
	 * 			<option value="4004">浦发银行(贷)</option>
	 * 			<option value="4005">农业银行(贷)</option>
	 * 			<option value="4006">民生银行(贷)</option>
	 * 			<option value="4009">兴业银行(贷)</option>
	 * 			<option value="4020">交通银行(贷)</option>
	 * 			<option value="4022">光大银行(贷)</option>
	 * 			<option value="4026">中国银行(贷)</option>
	 * 			<option value="4032">北京银行(贷)</option>		
	 * 			<option value="4035">平安银行(贷)</option>
	 * 			<option value="4036">广发银行(贷)</option>
	 * 			<option value="4039">中信银行(贷)</option>
	 * 			
	 * 			<!--other-->
	 * 			   <option value="101">神州行</option>
	 *             <option value="1022">联通卡</option>
	 *             <option value="1033">电信卡</option>
	 *             <option value="111">盛大卡</option>
	 *             <option value="112">完美卡</option>
	 *             <option value="114">征途卡</option>
	 *             <option value="115">骏网一卡通</option>
	 *             <option value="116">网易卡</option>
	 *   </select>
	 * </pre>
	 */
	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "进入用户中心充值页面", last_update_author = "曹正辉")
	public void into_user_recharge_index() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user.getInt("id"));
		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
		cny_can_used = cny_can_used.setScale(2, BigDecimal.ROUND_DOWN);
		BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
		cny_freeze = cny_freeze.setScale(2, BigDecimal.ROUND_DOWN);
		user_now_money.set("cny_can_used", cny_can_used);
		user_now_money.set("cny_freeze", cny_freeze);
		setAttribute("user_now_money", user_now_money);
		setSessionAttribute(Recharge_Key, null);
		renderJsp("/user/usercenter/account_recharge.jsp");
		return;

	}

	public static Set<String> PayIDSet = new HashSet<String>();
	/**
	 * IP地址检查 电信：202.107.192.（34 -46） 联通：121.52.235.34-46
	 */
	public static final Set<String> Ip_Set = new HashSet<String>();

	static {
		PayIDSet.add("3001");// 招商银行
		PayIDSet.add("3002");// 工商银行
		PayIDSet.add("3003");// 建设银行

		PayIDSet.add("3004");// 浦发银行
		PayIDSet.add("3005");// 农业银行
		PayIDSet.add("3006");// 民生银行

		PayIDSet.add("3009");// 兴业银行
		PayIDSet.add("3020");// 交通银行
		PayIDSet.add("3022");// 光大银行

		PayIDSet.add("3026");// 中国银行
		PayIDSet.add("3032");// 北京银行
		PayIDSet.add("3035");// 平安银行

		PayIDSet.add("3036");// 广发银行
		PayIDSet.add("3039");// 中信银行

		//

		PayIDSet.add("4001");// 招商银行
		PayIDSet.add("4002");// 工商银行
		PayIDSet.add("4003");// 建设银行

		PayIDSet.add("4004");// 浦发银行
		PayIDSet.add("4005");// 农业银行
		PayIDSet.add("4006");// 民生银行

		PayIDSet.add("4009");// 兴业银行
		PayIDSet.add("4020");// 交通银行
		PayIDSet.add("4022");// 光大银行

		PayIDSet.add("4026");// 中国银行
		PayIDSet.add("4032");// 北京银行
		PayIDSet.add("4035");// 平安银行

		PayIDSet.add("4036");// 广发银行
		PayIDSet.add("4039");// 中信银行

		for (int i = 34; i <= 46; i++) {
			// IP地址检查 电信：202.107.192.（34 -46） 联通：121.52.235.34-46
			Ip_Set.add("202.107.192." + i);
			Ip_Set.add("121.52.235." + i);
		}

	}
	public static final String Recharge_Key = "recharge";

	public static final String Member_Id = "408376";// 商户号100000178
	public static final String Terminal_Id = "21972";// 终端ID10000001
	public static final String Pay_Url = "http://gw.baofoo.com/payindex";
	public static final String Md5_Key = "2s3b796uzhup4udz"; // 6214835710331586
	/**
	 * <pre>
	 * 测试链接:http://gw.baofoo.com/merchant_page?CODE=542009a9d7ed009663e64ea224d5ce9e7991e189eb5148fa01038c260d7d927db708e2eac6481ff7746c65207f097815a6beb894b6933068
	 * 商户通知参数
	 * http://115.29.29.125/user/usercenter/recharge/make_sure_user_recharge_order_info.html?Result=1&ResultDesc=01&FactMoney=1&AdditionalInfo=&SuccTime=20141212164749&Md5Sign=f945470dc9cfa9ef89f2ed4c316ad6fb&MemberID=408376&TerminalID=21972&TransID=40837620141212044440&BankID=3001
	 * </pre>
	 */

	/**
	 * 返回IP
	 */
	// public static final String IP = "115.29.29.125";
	public static final String IP = "www.lfoll.com";

	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "进入第三方充值页面", last_update_author = "曹正辉")
	public void get_user_recharge_order_info() throws Exception {
		/**
		 * 配置参数参考PHP配置－Java的不行
		 */
		String MemberID = UserRechargeAction.Member_Id;// 分配的商户号
		String TerminalID = UserRechargeAction.Terminal_Id;// 终端ID
		String InterfaceVersion = "4.0";
		String KeyType = "1";
		/**
		 * <pre>
		 * 第一点payID不能用1开头的
		 * 用3或4开头的，3开头的是借记卡，4开头的是信用卡
		 * </pre>
		 */
		// *************************************
		String PayID = getParameter("PayID");// 支付渠道
		/**
		 * <pre>
		 * PayID = &quot;1&quot;;
		 * if (Utils.isNullOrEmptyString(PayID)) {
		 * 	renderText(&quot;bug:PayID is null&quot;);
		 * 	return;
		 * }
		 * if (!PayIDSet.contains(PayID)) {
		 * 	renderText(&quot;bug:PayID not contains  in PayIDSet&quot;);
		 * 	return;
		 * }
		 * </pre>
		 */
		PayID = "";
		// 获取当前交易时间
		Date now;
		synchronized (UserRechargeAction.class) {
			TimeUnit.SECONDS.sleep(1);// 一秒内只能有一个订单
			now = new Date();
		}
		String webdate = new String(Model.Time.format(now));// 交易时间
		String TradeDate = new String(Model.$Time.format(now));// 商户流水号-下单日期
		// 前6位为商户号，后10位为用户流水号
		String TransID = MemberID + TradeDate;// 商户流水号
		//
		// *************************************
		String OrderMoney = getParameter("OrderMoney");// 订单金额-元
		if (Utils.isNullOrEmptyString(OrderMoney)) {
			renderText("1");
			return;
		}
		int OrderMoneyNum = getParameterToInt("OrderMoney", 0);
		if (OrderMoneyNum <= 0) {
			renderText("2");
			return;
		}
		OrderMoney = (OrderMoneyNum * 100) + "";// 分
		// OrderMoney = "100";// 一分
		String ProductName = "";
		String Amount = "1";
		String Username = "";
		String AdditionalInfo = "";

		String PageUrl = "http://" + IP + "/user/usercenter/recharge/return_user_recharge_order_info.html";// 通知商户页面端地址
		String ReturnUrl = "http://" + IP + "/user/usercenter/recharge/make_sure_user_recharge_order_info.html";// 服务器底层通知地址
		String NoticeType = "1";// 通知类型
		String Md5key = Md5_Key;// md5密钥（KEY）abcdefg
		String MARK = "|";
		String md5 = new String(//
				MemberID + MARK + //
						PayID + MARK + //
						TradeDate + MARK + //
						TransID + MARK + //
						OrderMoney + MARK + //
						PageUrl + MARK + //
						ReturnUrl + MARK + //
						NoticeType + MARK + //
						Md5key);// MD5签名格式
		String Signature = baofu_md5(md5);// 计算MD5值
		{// 创建订单
			UserM user = getSessionAttribute("user");
			UserRechargeuserStreamLogM user_recharge_stream_log = new UserRechargeuserStreamLogM();
			user_recharge_stream_log.//
					set("user_id", user.getInt("id")).//
					set("add_time", now).//
					set("add_time_long", now.getTime()).//
					set("pay_id", 0).//
					set("trans_id", TransID).//
					set("money", new BigDecimal(OrderMoneyNum + "")).// 便于资金表数据对接
					set("is_pay_ok", new BigDecimal("0")).// Y F P N等待出来
					set("is_add_recharge_log", 0).//
					save();

		}

		/**
		 * 支付访问
		 */
		String payUrl = Pay_Url;
		renderHttp(payUrl + //
				"?MemberID=" + MemberID + //
				"&TerminalID=" + TerminalID + //
				"&InterfaceVersion=" + InterfaceVersion + //

				"&KeyType=" + KeyType + //
				"&PayID=" + PayID + //
				"&TradeDate=" + TradeDate + //
				"&TransID=" + TransID + //

				"&OrderMoney=" + OrderMoney + //
				"&ProductName=" + ProductName + //
				"&Amount=" + Amount + //
				"&Username=" + Username + //
				"&AdditionalInfo=" + AdditionalInfo + // 后面的不是必须但是要写

				"&PageUrl=" + PageUrl + //
				"&ReturnUrl=" + ReturnUrl + //
				"&Signature=" + Signature + //
				"&NoticeType=" + NoticeType //

		);

		return;

	}

	@AllowNotLogin
	@ClearInterceptor
	// @Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "服务器端接收保付通知-校验订单信息", last_update_author = "曹正辉")
	public void make_sure_user_recharge_order_info() throws Exception {
		String ip = IpUtil.getRemoteAddrIP(getRequest());
		if (Utils.isNullOrEmptyString(ip)) {
			renderText("非法参数");
			return;
		}
		String MemberID = getParameter("MemberID");
		String TerminalID = getParameter("TerminalID");
		/**
		 * 校验商户参数
		 */
		if (Utils.isNullOrEmptyString(MemberID)) {
			renderText("非法参数");
			return;
		}
		if (Utils.isNullOrEmptyString(TerminalID)) {
			renderText("非法参数");
			return;
		}

		if (!UserRechargeAction.Member_Id.equals(MemberID)) {
			renderText("非法参数");
			return;
		}

		if (!UserRechargeAction.Terminal_Id.equals(TerminalID)) {
			renderText("非法参数");
			return;
		}
		/**
		 * 判断商户号和终端号是否一致
		 */
		if (!(Member_Id.equals(MemberID) && Terminal_Id.equals(TerminalID))) {
			renderText("非法参数");
			return;
		}

		String TransID = getParameter("TransID");
		final String Result = getParameter("Result");

		String ResultDesc = getParameter("ResultDesc");
		String FactMoney = getParameter("FactMoney");// 分为单位

		String AdditionalInfo = getParameter("AdditionalInfo");
		String SuccTime = getParameter("SuccTime");

		String Md5Sign = getParameter("Md5Sign");
		String Md5key = Md5_Key;
		String MARK = "~|~";

		String md5 = "MemberID=" + MemberID + MARK + "TerminalID=" + TerminalID + MARK + "TransID=" + TransID + MARK + "Result=" + Result + MARK + "ResultDesc=" + ResultDesc + MARK + "FactMoney=" + FactMoney + MARK + "AdditionalInfo=" + AdditionalInfo + MARK + "SuccTime=" + SuccTime + MARK + "Md5Sign=" + Md5key;
		if (!Md5Sign.equals(baofu_md5(md5))) {
			renderText("校验失败");
			return;
		} else {

			// money
			/**
			 * 判断返回值是不是YFPN
			 */
			if (!("1".equals(Result) || "0".equals(Result))) {
				renderText("草你母");
				return;
			}

			final UserRechargeuserStreamLogM user_recharge_stream_log = UserRechargeuserStreamLogM.dao.findFirst("SELECT * from user_recharge_stream_log WHERE trans_id=? and is_pay_ok=0 for update", new Object[] { TransID });
			if (user_recharge_stream_log == null) {
				renderText("查询失败,该订单失效或者不存在");
				return;
			}
			final int FactMoneyNum = Integer.parseInt(FactMoney) / 100;
			// 虽然是校验成功-但是需要和数据库里面的数据进行比较只是是小才行
			BigDecimal money = user_recharge_stream_log.getBigDecimal("money");
			if (money.intValue() != FactMoneyNum) {
				renderText("资金不对");
				return;
			}
			final BigDecimal FactMoneyNumNumDecimal = new BigDecimal(FactMoneyNum + "");
			/**
			 * 如果为成功的话-需要提取金额-来更新数据库里面的记录
			 */
			Db.tx(new IAtomic() {

				public boolean transactionProcessing() throws Exception {
					boolean is_ok_update_user_recharge_stream_log = user_recharge_stream_log.set("is_pay_ok", Integer.parseInt(Result)).update();// 0
					if (!is_ok_update_user_recharge_stream_log) {
						return false;
					}
					if ("1".equals(Result)) {

						int user_id = user_recharge_stream_log.getInt("user_id");

						UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
						BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
						BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");

						UserRechargeLogM user_recharge_log = new UserRechargeLogM();
						Date now = new Date();

						boolean is_ok_save_user_recharge_log = user_recharge_log.//
								set("user_id", user_id).//
								set("balance", cny_can_used.add(cny_freeze)).//
								set("amount", FactMoneyNum).//
								set("add_time_long", now.getTime()).//
								set("actual_amount", FactMoneyNum).// 前期不需要手续费,后期actual_amount=amount-fee
								set("fee", new BigDecimal("0")).// 
								set("type", 1).// '类别1默认充值2其他奖励'
								save();

						// sys_expenses_records-no
						/**
						 * <pre>
						 * SysIncomeRecordsM sys_income_record = new SysIncomeRecordsM();
						 * boolean save_sys_income_record_ok = sys_income_record.set(&quot;user_id&quot;, user_id).set(&quot;type&quot;, UserTransactionRecordsM.Type_12).set(&quot;start_money&quot;, user_now_money.getBigDecimal(&quot;cny_can_used&quot;)).set(&quot;pay&quot;, new BigDecimal(0)).set(&quot;income&quot;, new BigDecimal(FactMoneyNUm)).set(&quot;end_money&quot;, user_now_money.getBigDecimal(&quot;cny_can_used&quot;).add(new BigDecimal(FactMoneyNUm))).set(&quot;detail&quot;, &quot;人民币充值&quot;).set(
						 * 		&quot;add_time_long&quot;, System.currentTimeMillis()).set(&quot;add_time_date&quot;, d1).save();
						 * </pre>
						 */
						// sys_income_records-no
						// user_money_change_records-yes
						// user_transaction_records-yes
						boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_11, cny_can_used.add(cny_freeze), new BigDecimal("0"), FactMoneyNumNumDecimal, cny_can_used.add(cny_freeze).add(FactMoneyNumNumDecimal), "充值");
						boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserMoneyChangeRecordsM.Type_11, cny_can_used.add(cny_freeze), new BigDecimal("0"), FactMoneyNumNumDecimal, cny_can_used.add(cny_freeze).add(FactMoneyNumNumDecimal), "充值");

						boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used.add(FactMoneyNumNumDecimal)).update();
						return is_ok_add_user_money_change_records_by_type && is_ok_add_user_transaction_records && is_ok_save_user_recharge_log && is_ok_update_user_now_money;

					}

					return true;
				}
			});
			SysErrorInfoM sys_error_info = new SysErrorInfoM();
			sys_error_info.set("is_deal", 0).set("detail", "user_id为" + user_recharge_stream_log.getInt("user_id") + "充值资金失败,TransID=" + TransID).save();

			// 校验合格-有两个结果
			renderText("校验成功" + Result + ResultDesc);
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor
	// @Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "处理保付返回响应信息-用户显示", last_update_author = "曹正辉")
	public void return_user_recharge_order_info() {
		String ip = IpUtil.getRemoteAddrIP(getRequest());

		String MemberID = getParameter("MemberID");// 商户号
		String TerminalID = getParameter("TerminalID");// 商户终端号
		/**
		 * 校验商户参数
		 */
		if (Utils.isNullOrEmptyString(MemberID)) {
			renderText("1非法参数");
			return;
		}
		if (Utils.isNullOrEmptyString(TerminalID)) {
			renderText("2非法参数");
			return;
		}

		if (!UserRechargeAction.Member_Id.equals(MemberID)) {
			renderText("3非法参数");
			return;
		}

		if (!UserRechargeAction.Terminal_Id.equals(TerminalID)) {
			renderText("4非法参数");
			return;
		}

		String TransID = getParameter("TransID");// 商户流水号
		setAttribute("TransId", TransID);
		String Result = getParameter("Result");// 支付结果
		String ResultDesc = getParameter("ResultDesc");// 支付结果描述
		String FactMoney = getParameter("FactMoney");
		String FactMoneyNum = new BigDecimal(getParameter("FactMoney")).divide(BigDecimal.valueOf(100)).setScale(2).toString();// 实际成功金额，以分为单位
		String AdditionalInfo = getParameter("AdditionalInfo");// 订单附加消息
		String SuccTime = getParameter("SuccTime");// 支付完成时间
		String Md5Sign = getParameter("Md5Sign");// MD5签名
		String Md5key = Md5_Key; // /////////md5密钥（KEY）
		String MARK = "~|~";
		String md5 = //
		"MemberID=" + MemberID + MARK + //
				"TerminalID=" + TerminalID + MARK + //
				"TransID=" + TransID + MARK + //
				"Result=" + Result + MARK + //
				"ResultDesc=" + ResultDesc + MARK + //
				"FactMoney=" + FactMoney + MARK + //
				"AdditionalInfo=" + AdditionalInfo + MARK + //
				"SuccTime=" + SuccTime + MARK + //
				"Md5Sign=" + Md5key;//

		String WaitSign = baofu_md5(md5);// 计算MD5值
		@SuppressWarnings("unused")
		String lbresultDesc = getErrorInfo(Result, ResultDesc);// 支付结果文字描述

		if (WaitSign.compareTo(Md5Sign) == 0) {
			renderJsp("/user/usercenter/account_recharge_success.jsp");
			return;
		} else {
			renderJsp("/user/usercenter/account_recharge_fail.jsp");
			return;
		}

	}

	public static final String Query_Path = "https://vgw.baofoo.com/order/query";

	@AllowNotLogin
	@ClearInterceptor
	// @Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "管理员查询订单信息", last_update_author = "曹正辉")
	public void query_user_recharge_order_info() {
		String MemberID = UserRechargeAction.Member_Id;
		String TerminalID = UserRechargeAction.Terminal_Id;
		String TransID = getParameter("TransID");
		if (Utils.isNullOrEmptyString(TransID)) {
			renderText("订单号为空");
			return;
		}
		String Md5key = "abcdefg";// md5密钥（KEY）
		String MARK = "|";
		String Md5Sign = baofu_md5((MemberID + MARK + TerminalID + MARK + TransID + MARK + Md5key));
		/**
		 * 采用HttpClient获取对方的返回的参数
		 */

		HttpClient client = new HttpClient();
		// 100000178|10000001|10000017820141122154048|abcdefg
		// https://vgw.baofoo.com/order/query?MemberID=100000178&TerminalID=10000001&TransID=10000017820141122154048&Md5Sign=fa075b308a8b9bdb5cd26f23a4b7450d
		// 100000178|10000001|10000017820141122154048|Y|1|20141122154130|2cb7cdbc04de0bff6212c3efaff0d861
		String path = Query_Path + "?" + "MemberID=" + MemberID + "&TerminalID=" + TerminalID + "&TransID=" + TransID + "&Md5Sign=" + Md5Sign;
		PostMethod post = new PostMethod(path);
		// 请求头
		List<Header> headers = new ArrayList<Header>();
		headers.add(new Header("Content-Type", "application/json-rpc"));
		client.getHostConfiguration().getParams().setParameter("http.default-headers", headers);

		try {
			int code = client.executeMethod(post);

			if (code != HttpStatus.SC_OK) {
				renderText("查询失败");
				return;

			} else {
				String msg = post.getResponseBodyAsString();
				String[] results = msg.split("|");
				String yes_or_no = results[3];// 支付结果-Y：成功 F：失败 P：处理中 N：没有订单
				String money = results[4];// 实际成功金额
				String time = results[5];// 支付完成时

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		} finally {
			if (post != null) {
				post.releaseConnection();// 释放连接
			}
		}

	}

	public static void main(String[] args) {
		// https://vgw.baofoo.com/order/query?MemberID=100000178&TerminalID=10000001&TransID=10000017820141125105414&Md5Sign=b420fa0086ebbc910f080f4f54b6eadf
		// 100000178|10000001|10000017820141125105414|abcdefg
		System.out.println(UserRechargeAction.baofu_md5("100000178|10000001|10000017820141125105414|abcdefg"));
		;
	}
}
