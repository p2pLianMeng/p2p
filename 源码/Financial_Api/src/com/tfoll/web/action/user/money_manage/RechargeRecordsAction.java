package com.tfoll.web.action.user.money_manage;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.util.page.PageDiv;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ActionKey("/user/user_center/money_mange/recharge_records")
public class RechargeRecordsAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "查询交易记录", function_description = "跳转用户交易记录页面", last_update_author = "向旋")
	public void query_recharge_records() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		UserNowMoneyM user_now_money = UserNowMoneyM.dao.findById(user.getInt("id"));
		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
		cny_can_used = cny_can_used.setScale(2, BigDecimal.ROUND_DOWN);
		BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
		cny_freeze = cny_freeze.setScale(2, BigDecimal.ROUND_DOWN);
		List<Record> user_recharge_log = Db.find("select * from user_recharge_log where user_id = ?", new Object[] { user.getInt("id") });
		List<BigDecimal> list = new ArrayList<BigDecimal>();
		for (Record record : user_recharge_log) {
			list.add(record.getBigDecimal("amount"));
		}
		BigDecimal num = new BigDecimal("0");
		for (BigDecimal amount : list) {
			num = num.add(amount);
		}
		if (num != null) {
			num = num.setScale(2, BigDecimal.ROUND_DOWN);
		}
		List<Record> user_withdraw_log = Db.find("select amount from user_withdraw_cash_log where user_id = ?", new Object[] { user.getInt("id") });
		List<BigDecimal> withdraw_list = new ArrayList<BigDecimal>();
		for (Record record : user_withdraw_log) {
			withdraw_list.add(record.getBigDecimal("amount"));
		}
		BigDecimal withdraw_num = new BigDecimal("0");
		for (BigDecimal withdraw_amount : withdraw_list) {
			withdraw_num = withdraw_num.add(withdraw_amount);
		}
		if (withdraw_num != null) {
			withdraw_num = withdraw_num.setScale(2, BigDecimal.ROUND_DOWN);
		}
		user_now_money.set("cny_can_used", cny_can_used);
		user_now_money.set("cny_freeze", cny_freeze);
		setAttribute("num", num);
		setSessionAttribute("withdraw_num", withdraw_num);
		setSessionAttribute("user_now_money", user_now_money);
		renderJsp("/user/usercenter/money_manage/recharge_record.jsp");

		/**
		 * 初始化查询条件
		 */
		setSessionAttribute(SystemConstantKey.Query_Type, 10);
		setSessionAttribute(SystemConstantKey.Query_Time, 1);
		//
		setAttribute(SystemConstantKey.Query_Type, 10);
		setAttribute(SystemConstantKey.Query_Time, 1);
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登陆", function_description = "用户交易记录查询", last_update_author = "向旋")
	public void transaction_records_query() {
		int pn = getParameterToInt("pn", 1);
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int id = user.getInt("id");
		int query_type = getParameterToInt("type", 10);
		int query_time = getParameterToInt("time", 1);
		setSessionAttribute(SystemConstantKey.Query_Type, query_type);
		setSessionAttribute(SystemConstantKey.Query_Time, query_time);
		//
		setAttribute(SystemConstantKey.Query_Type, query_type);
		setAttribute(SystemConstantKey.Query_Time, query_time);

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		long add_time_long = 0;
		if (query_time == 1) {
			calendar.add(calendar.DAY_OF_MONTH, -3);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 2) {
			calendar.add(calendar.DAY_OF_MONTH, -7);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 3) {
			calendar.add(calendar.MONTH, -1);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 4) {
			calendar.add(calendar.MONTH, -3);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 5) {
			calendar.add(calendar.YEAR, -1);
			add_time_long = calendar.getTime().getTime();
		} else {
			renderText("else部分");
			return;
		}
		StringBuilder sql_count = new StringBuilder("select count(1) from user_transaction_records t,user_info u where t.user_id=u.id ");
		StringBuilder sql_select = new StringBuilder("select * from user_transaction_records t,user_info u where t.user_id=u.id ");
		List<Object> parma = new ArrayList<Object>();
		{
			sql_count.append(" and u.id=? ");
			sql_select.append(" and u.id=? ");
			parma.add(id);
		}
		if (query_type != 10) {
			sql_count.append(" and t.type=? ");
			sql_select.append(" and t.type=? ");
			parma.add(query_type);
		}
		{
			sql_count.append(" and t.add_time_long>? ");
			sql_select.append(" and t.add_time_long>? ");
			parma.add(add_time_long);
		}
		Long totalRow = Db.queryLong(sql_count.toString(), parma.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<Record> transaction_records = Db.find(page.creatLimitSql(sql_select.toString()), parma.toArray());
			for (Record record : transaction_records) {
				BigDecimal pay = record.getBigDecimal("pay");
				BigDecimal income = record.getBigDecimal("income");
				BigDecimal end_money = record.getBigDecimal("end_money");
				pay = pay.setScale(2, BigDecimal.ROUND_DOWN);
				income = income.setScale(2, BigDecimal.ROUND_DOWN);
				end_money = end_money.setScale(2, BigDecimal.ROUND_DOWN);
				record.add("pay", pay);
				record.add("income", income);
				record.add("end_money", end_money);
			}
			setSessionAttribute("transaction_records", transaction_records);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/user_center/money_mange/recharge_records/transaction_records_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}
		renderJsp("/user/usercenter/money_manage/recharge_record.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登陆", function_description = "用户交易记录查询", last_update_author = "向旋")
	public void transaction_records_page() {
		int pn = getParameterToInt("pn", 1);
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int id = user.getInt("id");
		int query_type = getSessionAttributeToInt("type", 10);
		int query_time = getSessionAttributeToInt("time", 1);

		//
		setAttribute(SystemConstantKey.Query_Type, query_type);
		setAttribute(SystemConstantKey.Query_Time, query_time);

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		long add_time_long = 0;
		if (query_time == 1) {
			calendar.add(calendar.DAY_OF_MONTH, -3);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 2) {
			calendar.add(calendar.DAY_OF_MONTH, -7);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 3) {
			calendar.add(calendar.MONTH, -1);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 4) {
			calendar.add(calendar.MONTH, -3);
			add_time_long = calendar.getTime().getTime();
		} else if (query_time == 5) {
			calendar.add(calendar.YEAR, -1);
			add_time_long = calendar.getTime().getTime();
		} else {
			renderText("else部分");
			return;
		}
		StringBuilder sql_count = new StringBuilder("select count(1) from user_transaction_records t,user_info u where t.user_id=u.id ");
		StringBuilder sql_select = new StringBuilder("select * from user_transaction_records t,user_info u where t.user_id=u.id ");
		List<Object> parma = new ArrayList<Object>();
		{
			sql_count.append(" and u.id=? ");
			sql_select.append(" and u.id=? ");
			parma.add(id);
		}
		if (query_type != 10) {
			sql_count.append(" and t.type=? ");
			sql_select.append(" and t.type=? ");
			parma.add(query_type);
		}
		{
			sql_count.append(" and t.add_time_long>? ");
			sql_select.append(" and t.add_time_long>? ");
			parma.add(add_time_long);
		}
		Long totalRow = Db.queryLong(sql_count.toString(), parma.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<Record> transaction_records = Db.find(page.creatLimitSql(sql_select.toString()), parma.toArray());
			for (Record record : transaction_records) {
				BigDecimal pay = record.getBigDecimal("pay");
				BigDecimal income = record.getBigDecimal("income");
				BigDecimal end_money = record.getBigDecimal("end_money");
				pay = pay.setScale(2, BigDecimal.ROUND_DOWN);
				income = income.setScale(2, BigDecimal.ROUND_DOWN);
				end_money = end_money.setScale(2, BigDecimal.ROUND_DOWN);
				record.add("pay", pay);
				record.add("income", income);
				record.add("end_money", end_money);
			}
			setSessionAttribute("transaction_records", transaction_records);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/user_center/money_mange/recharge_records/transaction_records_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}
		renderJsp("/user/usercenter/money_manage/recharge_record.jsp");
		return;

	}
}
