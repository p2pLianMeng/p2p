package com.tfoll.web.util.page;

import com.tfoll.trade.util.page.IPageDiv;

public class ArticleJsonPageDiv implements IPageDiv {
	public static String getDiv(int type_id, long totalPageNum, long pageSize, long currentPageNum) {
		// 上三页-下三页-最小页数和最大页数
		// 当前页左右分别判断
		// 前面到当前页小于3可能为0,1,2三种情况
		if (totalPageNum == 1 || totalPageNum == 0) {
			return ""; // 只有一页
		} else {
			StringBuilder div = new StringBuilder(240);
			div.append("<ul class=\"page\">");
			// currentPageNum一定大于等于1小于totalPageNum同时会出现currentPageNum永远不会出现在分页的链接两边的情况

			if (currentPageNum - 5 > 1) {
				div.append("<a href='javascript:void(0)' onclick=\"change_page_of_news(" + type_id + ",1);\">" + "首页" + "</a>");
			}
			for (long i = currentPageNum - 5; i <= currentPageNum + 5; i++) {
				// 可能存在三种情况:第一个,中间一个,最后一个

				if (i <= 0 || i > totalPageNum) {
					// 则不进行分页
				} else {
					// 判断是不是[当前页,第一页,最后一页]
					// i>=1
					if (i == currentPageNum) {
						div.append("<a class='hover' style='color:#FFFFFF;'>" + currentPageNum + "</a>");
					} else if (i == 1) {
						div.append("<a href='javascript:void(0)'  onclick=\"change_page_of_news(" + type_id + ",1);\">" + "首页" + "</a>");
					} else if (i == totalPageNum) {
						div.append("<a href='javascript:void(0)'  onclick=\"change_page_of_news(" + type_id + "," + totalPageNum + ");\">" + "末页" + "</a>");
					} else {
						String now = (i + "");
						div.append("<a href='javascript:void(0)'  onclick=\"change_page_of_news(" + type_id + "," + now + ");\">" + now + "</a>");
					}
				}
			}
			if (currentPageNum + 5 < totalPageNum) {
				div.append("<a href='javascript:void(0)'  onclick=\"change_page_of_news(" + type_id + "," + totalPageNum + ");\">" + "末页" + "</a>");
			}
			div.append("</ul>");
			return div.toString();
		}

	}

}