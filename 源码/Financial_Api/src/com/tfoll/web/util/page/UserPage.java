package com.tfoll.web.util.page;

/**
 * @author 曹正辉 --非原创 mysql分页工具类:通过构造方法建立mysql计算信息。public Page(int totalRow, int
 *         currentCount)。public Page(int totalRow, int currentCount, int
 *         pageSize)。
 * 
 *         <pre>
 * 使用方法
 * long totalRow = Db.queryLong("select count(*) from log where 1=1 " + sb.toString(), list.toArray());
 * 		if (totalRow>0) {
 * 			Page page = new Page(totalRow, currentPageNum);
 * 			List<Record> logList = Db.find("select * from log where 1=1 " + sb.toString() + " limit " + page.getBeginIndex() + "," + page.getEndIndex(), list.toArray());
 * 			setAttribute("logList", logList);
 * 		}
 * renderJsp("logList.jsp");
 * 
 * <pre>
 * 说明：数据库可以查询总的条数.人可以设定每页显示的条数。从以上数据我们可以得到当前页数,页面总数。
 */

public class UserPage {

	/**
	 * 数据库数据的总的条数
	 */
	private long totalRow;// 从数据库里面查询

	/**
	 * 总页数
	 */
	private long totalPageNum;// 通过公式得到
	/**
	 * 当前页码
	 */
	private long currentPageNum;// 需要查询的页码

	/**
	 * 设定每页显示的条数：默认是16
	 */
	private long pageSize = 10;

	public long getPageSize() {
		return pageSize;
	}

	/**
	 * 默认是20
	 */
	public UserPage(long totalRow, long currentPageNum) {
		this.totalRow = totalRow;
		this.currentPageNum = currentPageNum;
		calculate();
	}

	public UserPage(long totalRow, long currentCount, long pageSize) {
		this.totalRow = totalRow;
		this.currentPageNum = currentCount;
		this.pageSize = pageSize;
		calculate();
	}

	private void calculate() {

		totalPageNum = totalRow / pageSize + ((totalRow % pageSize) > 0 ? 1 : 0);

		// 计算当前的页数
		if (currentPageNum > totalPageNum) {
			currentPageNum = totalPageNum;
		} else if (currentPageNum < 1) {
			currentPageNum = 1;
		}

	}

	public long getBeginIndex() {

		if (currentPageNum <= 0) {
			currentPageNum = 1;
		}
		return (currentPageNum - 1) * pageSize;
	}

	/**
	 * 生成limit语句
	 * 
	 * @param sql
	 * @param page
	 */
	public String creatLimitSql(String sql) {
		return sql.toString() + "  limit " + this.getBeginIndex() + "," + this.getEndIndex();
	}

	public long getEndIndex() {
		return pageSize;
	}

	public long getCurrentPageNum() {
		return currentPageNum;
	}

	public long getTotalPageNum() {
		return totalPageNum;
	}

	/**
	 * 得到总的行数--由于数据的可变动性。这个方法不建议使用
	 */
	@SuppressWarnings("unused")
	private long getTotalRow() {
		return totalRow;
	}

}