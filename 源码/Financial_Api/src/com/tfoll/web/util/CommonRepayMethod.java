package com.tfoll.web.util;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonRepayMethod {

	/**
	 * 根据筹款单id获取还款计划详情 统计 总的 未还本息，已还本息，管理费 ，已还总额，未还总额 未还总额包括 该还本息 正常管理费 逾期费用
	 * 
	 * @param gather_order_id
	 *            筹款单id
	 * @return
	 */
	public static String get_repayment_info(int gather_order_id) {

		BigDecimal borrow_all_money = Db.queryBigDecimal("select borrow_all_money from borrower_bulk_standard_gather_money_order where id = ?", new Object[] { gather_order_id });

		String sql = "select * from borrower_bulk_standard_repayment_plan where gather_money_order_id = ?";
		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find(sql, new Object[] { gather_order_id });

		List<Map<String, Object>> repayment_plans = new ArrayList<Map<String, Object>>();

		BigDecimal yihuan_total = new BigDecimal("0");
		BigDecimal weihuan_total = new BigDecimal("0");

		BigDecimal weihuan_benxi_total = new BigDecimal("0");// 总的 未还本息
		BigDecimal weihuan_normal_manage_total = new BigDecimal("0");// 未还的
		// 总的正常管理费
		BigDecimal weihuan_over_fee = new BigDecimal("0");// 未还的
		// 总的逾期费用(逾期罚息+逾期管理费)

		BigDecimal yihuan_benxi_total = new BigDecimal("0");// 总的 已还本息

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
			Map<String, Object> repayment_plan = borrower_bulk_standard_repayment_plan.getM();
			repayment_plans.add(repayment_plan);

			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");

			BigDecimal total_total = new BigDecimal("0");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
			// 正常管理费
			BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
			// 逾期罚息
			BigDecimal over_faxi = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
			// 逾期管理费
			BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);

			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_fee = over_faxi.add(over_manage_fee);

			// 应还总额=月还本息+管理费 +逾期费用
			total_total = total_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);

			repayment_plan.put("over_faxi", over_faxi);
			repayment_plan.put("manage_fee", normal_manage_fee);
			repayment_plan.put("over_total_fee", over_fee);
			repayment_plan.put("total_total", total_total);

			// 一下是计算总的东西
			if (is_repay == 0) {// 未还
				weihuan_total = weihuan_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);
				weihuan_benxi_total = weihuan_benxi_total.add(should_repayment_total);
				weihuan_normal_manage_total = weihuan_normal_manage_total.add(normal_manage_fee);
				weihuan_over_fee = weihuan_over_fee.add(over_fee);
			} else if (is_repay == 1) {// 已还
				yihuan_total = yihuan_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);
				yihuan_benxi_total = yihuan_benxi_total.add(should_repayment_total);
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("repayment_plans", repayment_plans);

		weihuan_total = weihuan_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_total", weihuan_total);

		weihuan_benxi_total = weihuan_benxi_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_beixi_total", weihuan_benxi_total);

		yihuan_benxi_total = yihuan_benxi_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("yihuan_benxi_total", yihuan_benxi_total);

		weihuan_normal_manage_total = weihuan_normal_manage_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_normal_manage_total", weihuan_normal_manage_total);

		weihuan_over_fee = weihuan_over_fee.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_over_fee", weihuan_over_fee);

		Gson gson = new Gson();
		String str = gson.toJson(map);

		return str;
	}

}
