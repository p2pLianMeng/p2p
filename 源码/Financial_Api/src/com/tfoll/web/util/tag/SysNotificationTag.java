package com.tfoll.web.util.tag;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.WebApp;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

//<a href="#" style="color: #0C0">【消息】</a>
@SuppressWarnings("serial")
public class SysNotificationTag extends TagSupport {
	@Override
	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest) pageContext.getAttribute(PageContext.REQUEST);
		UserM user = (UserM) request.getSession().getAttribute("user");
		System.out.println(1);
		/**
		 * 三种状态
		 */
		int state;
		if (user == null) {
			state = 1;
		} else {
			int user_id = user.getInt("id");
			long count = 0;
			try {
				count = Db.queryLong("SELECT COUNT(1) from  sys_notification WHERE user_id=? and system_status=0", new Object[] { user_id });
				if (count > 0) {
					state = 2;
				} else {
					state = 3;
				}
			} catch (Exception e) {
				e.printStackTrace();
				state = 4;
			}

		}
		StringBuilder sb = new StringBuilder(100);
		String web_root_path = WebApp.getWebRootPath(request);
		if (state == 1) {
			sb.append("<a href=\"#\" style=\"color: #0C0\"></a>");// 未登录
		} else if (state == 2) {
			sb.append("<a href=\"" + web_root_path + "/user/usercenter/to_station_information.html" + "\" style=\"color: #0C0\">【未读消息】</a>");// 未读信息
		} else if (state == 3) {
			sb.append("<a href=\"" + web_root_path + "/user/usercenter/to_station_information.html" + "\" style=\"color: #e5e5e5\">【消息】</a>");// 没有未读信息
		} else if (state == 4) {
			sb.append("<a href=\"#\" style=\"color: #0C0\">系统异常</a>");// 系统异常
		} else {
			sb.append("<a href=\"#\" style=\"color: #0C0\"></a>");// 其他
		}
		try {
			pageContext.getOut().write(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Tag.SKIP_BODY;

	}

}