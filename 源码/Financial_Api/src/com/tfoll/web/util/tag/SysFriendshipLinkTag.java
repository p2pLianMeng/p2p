package com.tfoll.web.util.tag;

import com.tfoll.web.model.AboutFriendShipConnectionM;
import com.tfoll.web.util.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 显示友情链接
 * 
 */
@SuppressWarnings("serial")
public class SysFriendshipLinkTag extends TagSupport {
	static final ReadWriteLock lock = new ReentrantReadWriteLock();
	private static List<AboutFriendShipConnectionM> sys_friendship_link_list = new ArrayList<AboutFriendShipConnectionM>();
	private static final long $2 = 1 * 60 * 1000;
	// 获取系统当前的
	private static long lastTime = System.currentTimeMillis();

	@Override
	public int doStartTag() throws JspException {
		// 从静态变量中获取文章
		List<AboutFriendShipConnectionM> sys_friendship_link_list2 = SysFriendshipLinkTag.readSysFriendshipLinkList();
		try {
			if (!Utils.isHasData(sys_friendship_link_list2)) {
				try {
					pageContext.getOut().write("");
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				int size = sys_friendship_link_list2.size();
				if (size > 0) {
					StringBuffer buffer = new StringBuffer("<tr>");
					int n = 0;
					for (AboutFriendShipConnectionM m : sys_friendship_link_list2) {
						String site_name = m.getString("site_name").trim();
						String site_url = m.getString("site_url").trim();
						n = n + 1;
						if (n == 1) {
							buffer.append("<td width=\"10%\">友情链接</td>");
						}
						if (n < 10) {
							buffer.append("<td width=\"9%\"><a target=\"_blank\" href=\"" + site_url + "\">" + site_name + "</a></td>");
						} else if (n % 10 == 0) {
							buffer.append("<td width=\"9%\"><a target=\"_blank\" href=\"" + site_url + "\">" + site_name + "</a></td></tr><tr><td width=\"10%\"></td>");
						} else if (n > 10) {
							buffer.append("<td width=\"9%\"><a target=\"_blank\" href=\"" + site_url + "\">" + site_name + "</a></td>");
						}

					}
					buffer.append("</tr>");
					// System.out.println("---------------------------------->>>>"+buffer.toString());
					pageContext.getOut().write(buffer.toString());
				}

				return Tag.SKIP_BODY;
			}
		} catch (Exception e) {
		}
		return Tag.SKIP_BODY;

	}

	// 读取内存中的静态数据
	private static List<AboutFriendShipConnectionM> readSysFriendshipLinkList() {
		long currentTime = System.currentTimeMillis();

		if (!Utils.isHasData(sys_friendship_link_list) || (currentTime - lastTime) > $2) {
			List<AboutFriendShipConnectionM> list = SysFriendshipLinkTag.getSysFriendshipLinkList();

			WriteLock writeLock = (WriteLock) lock.writeLock();
			try {
				writeLock.lock();
				sys_friendship_link_list.clear();
				sys_friendship_link_list.addAll(list);
				lastTime = currentTime;
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				writeLock.unlock();
			}
			return list;
		} else {
			Lock readLock = lock.readLock();
			try {
				readLock.lock();
				List<AboutFriendShipConnectionM> list = new ArrayList<AboutFriendShipConnectionM>();
				list.addAll(sys_friendship_link_list);
				return list;
			} finally {
				readLock.unlock();
			}
		}
	}

	private static List<AboutFriendShipConnectionM> getSysFriendshipLinkList() {
		return AboutFriendShipConnectionM.dao.find("select * from about_friendship_connetion where 1=1 and is_effect = 1 order by id asc ");
	}

}
