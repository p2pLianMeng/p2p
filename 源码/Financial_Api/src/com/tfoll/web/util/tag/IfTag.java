package com.tfoll.web.util.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class IfTag extends BodyTagSupport {

	@Override
	public int doStartTag() throws JspException {

		if (url != null && need_url != null) {
			// 使用#进行分割
			if (need_url.indexOf(url) != -1) {
				return EVAL_BODY_INCLUDE;
			}

		}
		return SKIP_BODY;

	}

	private static final long serialVersionUID = 1L;
	private String url;

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setNeed_url(String need_url) {
		this.need_url = need_url;
	}

	public String getNeed_url() {
		return need_url;
	}

	private String need_url;// need_url

}