package com.tfoll.web.util.filter;

import com.tfoll.web.util.Utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdminLoginFilter implements Filter {
	public static final String isLogined = AdminLoginFilter.class.getName();

	/**
	 * 设置管理员登录界面
	 */

	public void destroy() {

	}

	@SuppressWarnings("static-access")
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpSession session = request.getSession();
		String isLogined = (String) session.getAttribute(AdminLoginFilter.isLogined);
		if (Utils.isNotNullAndNotEmptyString(isLogined) && "1".equals(isLogined)) {
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			((HttpServletResponse) servletResponse).sendError(((HttpServletResponse) servletResponse).SC_FORBIDDEN, "该后台禁止非管理员用户登录");
		}

	}

	public void init(FilterConfig filterConfig) throws ServletException {

	}

}
