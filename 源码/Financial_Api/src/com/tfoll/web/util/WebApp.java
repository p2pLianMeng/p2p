package com.tfoll.web.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebApp {
	// 设置编码
	static final String Utf_8 = "utf-8";

	public static void setCharacterEncoding(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.setCharacterEncoding(Utf_8);
			response.setCharacterEncoding(Utf_8);
		} catch (Exception e) {
			throw new RuntimeException("系统Jsp编码设置失败");
		}

	}

	// 设置路径信息
	static final boolean Is_On_Line = false;
	static final String Cdn_Path = "http://tf-api.oss-cn-hangzhou.aliyuncs.com";

	/**
	 * 返回http://www.baidu.com:8080
	 */
	public static String getWebRootPath(HttpServletRequest request) {
		String path = request.getContextPath();
		StringBuilder __ROOT_PATH__ = new StringBuilder();
		__ROOT_PATH__.append(request.getScheme()).append("://").append(request.getServerName()).append(":").append(request.getServerPort()).append(path);
		return __ROOT_PATH__.toString();

	}

	public static String getWebRootPathBak(HttpServletRequest request) {
		String path = request.getContextPath();
		String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
		return __ROOT_PATH__;

	}

	/**
	 * 返回http://www.baidu.com:8080/__PUBLIC__
	 */
	public static String getPublicPath(HttpServletRequest request) {
		if (Is_On_Line) {
			return Cdn_Path;
		} else {
			String __PUBLIC__ = getWebRootPath(request) + "/__PUBLIC__";// 资源文件
			return __PUBLIC__;
		}

	}

	/**
	 * 性能优化 返回http://www.baidu.com:8080/__PUBLIC__
	 */
	public static String getPublicPath(HttpServletRequest request, String webRootPath) {
		if (Is_On_Line) {
			return Cdn_Path;
		} else {
			String __PUBLIC__ = webRootPath + "/__PUBLIC__";// 资源文件
			return __PUBLIC__;
		}

	}

	public static final String Windows_File_Separator = "/";

	/**
	 * 
	 * @param request
	 * @return D:\apache\webapps\ROOT
	 */
	public static String getWebAppRealPath(HttpServletRequest request) {
		return request.getSession().getServletContext().getRealPath("").replace("\\", "/");
	}

	public static final String Files_Stored_Palce = "D:\\upload";
	public static final String Zip_Files_Stored_Palce = "D:\\zip";

}
