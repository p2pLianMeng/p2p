package com.tfoll.web.aop.ajax;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.TextRender;
import com.tfoll.web.model.UserM;

import javax.servlet.http.HttpSession;

public class LenderAjax implements Interceptor {
	public void doIt(ActionExecutor ae) {
		HttpSession session = ActionContext.getRequest().getSession();
		UserM user = (UserM) session.getAttribute("user");
		if (user == null) {
			ActionContext.setRender(new TextRender("请先登录"));
			return;
		}
		Integer user_type_int = user.get("user_type");
		boolean is_lender = false;
		if (user_type_int != null) {
			int user_type = user_type_int;
			if (user_type == 1) {
				is_lender = true;
				ae.invoke();
			}
		}
		if (!is_lender) {
			ActionContext.setRender(new TextRender("非理财身份"));
			return;
		}

	}
}
