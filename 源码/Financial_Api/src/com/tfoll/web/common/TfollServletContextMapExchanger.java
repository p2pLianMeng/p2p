package com.tfoll.web.common;

import com.tfoll.web.util.Utils;

import javax.servlet.ServletContext;

public class TfollServletContextMapExchanger {
	public static volatile ServletContext servletContext = null;

	public static ServletContext getServletContext() {
		return servletContext;
	}

	/**
	 * 注册
	 * 
	 * @param listener
	 */
	public static void registerServletContext(ServletContext context) {
		if (context == null) {
			throw new RuntimeException("servletContext is null");
		}
		TfollServletContextMapExchanger.servletContext = context;
	}

	/**
	 * 添加数据在servletContext
	 * 
	 * @param key
	 * @param o
	 */
	public static void addValue(String key, Object o) {
		if (servletContext == null) {
			throw new RuntimeException("servletContext is null");
		}
		if (!Utils.isNotNullAndNotEmptyString(key)) {
			throw new RuntimeException("key is null");
		}
		if (servletContext.getAttribute(key.trim()) != null) {
			throw new RuntimeException("key has existed in this servletContext");
		}
	}

	/**
	 * 删除servletContext的数据
	 * 
	 * @param key
	 */
	public static void removeValue(String key) {
		if (servletContext == null) {
			throw new RuntimeException("servletContext is null");
		}

		if (key == null) {
			throw new RuntimeException("servletContext is null");
		}
		if ("".equals(key.trim())) {// 这个必要
			throw new RuntimeException("servletContext is \"\"");
		}

		if (servletContext.getAttribute(key.trim()) != null) {
			servletContext.removeAttribute(key.trim());
		}
	}

	public static void updateValue(String key, Object o) {
		if (servletContext == null) {
			throw new RuntimeException("servletContext is null");
		}

		if (!Utils.isNotNullAndNotEmptyString(key)) {
			throw new RuntimeException("key is null");
		}

		if (servletContext.getAttribute(key.trim()) == null) {
			throw new RuntimeException("key has not existed in this servletContext");
		}
		servletContext.setAttribute(key.trim(), o);

	}

}
