package com.tfoll.web.common;

import com.tfoll.web.domain.BulkStandardBiddingStatus;
import com.tfoll.web.model.SysProvinceCityM;
import com.tfoll.web.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统设置
 * 
 */
public class SystemConfig {
	/*
	 * 省份和直辖市-市
	 */
	/**
	 * 系统中用户信息-省份和直辖市
	 */
	public static List<String> Sys_Province_List = new ArrayList<String>();
	/**
	 * 系统中用户信息-省份和直辖市-市-map
	 */
	public static Map<String, List<String>> Sys_Province_City_Map = new HashMap<String, List<String>>();

	private static void init_sys_province_city() {
		List<SysProvinceCityM> sys_province_city_list = SysProvinceCityM.dao.find("SELECT province,city from sys_province_city");
		if (Utils.isHasData(sys_province_city_list)) {
			// 先所有的数据放到Map里面-然后遍历Keys就可构建两个不同的数据集合
			for (SysProvinceCityM sys_province_city : sys_province_city_list) {
				String province = sys_province_city.getString("province");
				String city = sys_province_city.getString("city");
				List<String> sys_province_list = Sys_Province_City_Map.get(province);
				if (!Utils.isHasData(sys_province_list)) {
					sys_province_list = new ArrayList<String>();
				}
				sys_province_list.add(city);
				Sys_Province_City_Map.put(province, sys_province_list);
			}
			for (String key : Sys_Province_City_Map.keySet()) {
				Sys_Province_List.add(key);
			}
		}
	}

	public static void init() {
		SystemConfig.init_sys_province_city();// 读取省市信息
		BulkStandardBiddingStatus.Manager.schedule();// 进行构建散标过期清除机制

	}

}
