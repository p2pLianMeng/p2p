package com.tfoll.web.common;

/**
 * 系统SessionKey
 * 
 */
public class SystemConstantKey {

	/**
	 * 用户
	 */
	public static String User = "user";
	/**
	 * 资金
	 */
	public static String User_Now_Money = "user_now_money";

	/**
	 * 正在进行编辑的申请单apply_order
	 */
	public static String Applying_Order_In_Request = "apply_order_in_request";
	public static String Applying_Order_In_Session = "apply_order_in_session";

	public static String Applying_Order_Id_In_Request = "apply_order_id_in_request";
	public static String Applying_Order_Id_In_Session = "apply_order_id_in_session";

	/**
	 * 散标查询条件-session记录法
	 */
	/**
	 * 借款期限
	 */
	public static String Borrow_Duration_Type_In_Request = "borrow_duration_type_in_request";
	public static String Borrow_Duration_Type_In_Session = "borrow_duration_type_in_session";
	/**
	 * 信用等级
	 */
	public static String Credit_Rating_In_Request = "credit_rating_in_request";
	public static String Credit_Rating_In_Session = "credit_rating_in_session";

	public static String Query_Type = "query_type";
	public static String Query_Time = "query_time";
	/**
	 * 用户推荐者url
	 */
	public static final String Recommend_Url = "url";

}
