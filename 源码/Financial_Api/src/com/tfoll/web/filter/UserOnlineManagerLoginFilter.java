package com.tfoll.web.filter;

import com.tfoll.web.action.user_online_manager.UserOnlineManagerLoginAction;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserOnlineManagerLoginFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;

		HttpSession session = request.getSession();
		Integer user_online_manager_login = (Integer) session.getAttribute(UserOnlineManagerLoginAction.class.getName());
		if (user_online_manager_login == null) {
			HttpServletResponse response = (HttpServletResponse) servletResponse;
			response.sendRedirect("https://www.baidu.com");
		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}

	}

	public void init(FilterConfig filterConfig) throws ServletException {

	}

}
