package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_order_creditor_right_transfer_in", primaryKey = "id")
public class LenderBulkStandardOrderCreditorRightTransferInM extends Model<LenderBulkStandardOrderCreditorRightTransferInM> {
	public static LenderBulkStandardOrderCreditorRightTransferInM dao = new LenderBulkStandardOrderCreditorRightTransferInM();
}
