package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_authenticate_family_info", primaryKey = "user_id")
public class UserAuthenticateFamilyInfoM extends Model<UserAuthenticateFamilyInfoM> {
	public static UserAuthenticateFamilyInfoM dao = new UserAuthenticateFamilyInfoM();
}
