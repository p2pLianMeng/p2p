package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "about_info", primaryKey = "id")
public class AboutInfoM extends Model<AboutInfoM> implements Serializable {

	private static final long serialVersionUID = 1262241552051672915L;
	public static AboutInfoM dao = new AboutInfoM();
}
