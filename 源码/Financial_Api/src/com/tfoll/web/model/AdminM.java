package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "admin", primaryKey = "id")
public class AdminM extends Model<AdminM> {
	public static AdminM dao = new AdminM();
}
