package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_newer_bid_record", primaryKey = "id")
public class LenderBulkStandardNewerBidRecordM extends Model<LenderBulkStandardNewerBidRecordM> {
	public static LenderBulkStandardNewerBidRecordM dao = new LenderBulkStandardNewerBidRecordM();
}
