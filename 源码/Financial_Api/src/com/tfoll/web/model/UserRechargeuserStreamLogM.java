package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_recharge_stream_log", primaryKey = "id")
public class UserRechargeuserStreamLogM extends Model<UserRechargeuserStreamLogM> {
	public static UserRechargeuserStreamLogM dao = new UserRechargeuserStreamLogM();
}
