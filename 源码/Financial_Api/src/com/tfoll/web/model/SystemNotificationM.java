package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.util.Date;

@TableBind(tableName = "sys_notification", primaryKey = "id")
public class SystemNotificationM extends Model<SystemNotificationM> {
	public static SystemNotificationM dao = new SystemNotificationM();

	public static boolean send_system_notification(int admin_id, int user_id, String title, String content) {
		SystemNotificationM system_notification = new SystemNotificationM();
		return system_notification.set("admin_id", admin_id).set("user_id", user_id).set("title", title).set("content", content).set("system_status", 0).set("add_time", new Date()).save();
	}
}
