package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_order_user_info", primaryKey = "gather_money_order_id")
public class BorrowerBulkStandardOrderUserInfoM extends Model<BorrowerBulkStandardOrderUserInfoM> {
	public static BorrowerBulkStandardOrderUserInfoM dao = new BorrowerBulkStandardOrderUserInfoM();

	// 查询用户最后一条订单
	public static BorrowerBulkStandardOrderUserInfoM get_last_order_user_info(int user_id) {
		return BorrowerBulkStandardOrderUserInfoM.dao.findFirst("select * from borrower_bulk_standard_order_user_info where user_id = ?  order by  gather_money_order_id desc limit 1", new Object[] { user_id });
	}
}
