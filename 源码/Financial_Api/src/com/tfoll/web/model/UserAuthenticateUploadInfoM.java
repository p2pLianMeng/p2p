package com.tfoll.web.model;

import com.tfoll.tcache.action.Utils;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_authenticate_upload_info", primaryKey = "user_id")
public class UserAuthenticateUploadInfoM extends Model<UserAuthenticateUploadInfoM> {
	public static UserAuthenticateUploadInfoM dao = new UserAuthenticateUploadInfoM();
	public static String user_authenticate_upload_info_array[] = { "id", "work", "credit", "income", "housing", "car", "marriage", "education", "title", "living" };

	// 获得上传信息信用总分
	public static int get_upload_sum_scores(int user_id) {
		int upload_sum_scores = 0;
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);
		for (int i = 0; i < user_authenticate_upload_info_array.length; i++) {
			String arrt = user_authenticate_upload_info_array[i] + "_authenticate_scores";
			int scores = user_authenticate_upload_info.getInt(arrt);
			upload_sum_scores += scores;
		}
		return upload_sum_scores;
	}

	/**
	 * 需要上传十个资料
	 */
	// 获得上传信息 数量
	public static int get_upload_num(int user_id) {
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);

		int upload_num = 0;
		int length = user_authenticate_upload_info_array.length;
		for (int i = 0; i < length; i++) {
			String arrt = user_authenticate_upload_info_array[i] + "_authenticate_url";
			String url = user_authenticate_upload_info.getString(arrt);
			if (Utils.isNotNullAndNotEmptyString(url)) {
				upload_num += 1;
			}
		}
		return upload_num;
	}

}
