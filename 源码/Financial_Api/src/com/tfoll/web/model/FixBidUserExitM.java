package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "fix_bid_user_exit", primaryKey = "id")
public class FixBidUserExitM extends Model<FixBidUserExitM> implements Serializable {

	private static final long serialVersionUID = -2685283432187735909L;
	public static FixBidUserExitM dao = new FixBidUserExitM();
}
