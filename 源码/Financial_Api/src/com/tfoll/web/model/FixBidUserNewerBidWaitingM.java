package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "fix_bid_user_newer_bid_waiting", primaryKey = "id")
public class FixBidUserNewerBidWaitingM extends Model<FixBidUserNewerBidWaitingM> implements Serializable {
	private static final long serialVersionUID = 7357208154827614007L;
	public static FixBidUserNewerBidWaitingM dao = new FixBidUserNewerBidWaitingM();
}
