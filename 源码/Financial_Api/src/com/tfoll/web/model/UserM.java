package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

@TableBind(tableName = "user_info", primaryKey = "id")
public class UserM extends Model<UserM> {
	public static UserM dao = new UserM();

	public static UserM getUserM(int user_id) {
		return UserM.dao.findFirst("select * from user_info WHERE id=?  for UPDATE", new Object[] { user_id });
	}

	// 根据用户id获得性别
	public static String get_sex(int user_id) {
		UserM user = UserM.dao.findById(user_id);
		String user_identity = user.getString("user_identity");

		int sex_num = user_identity.charAt(16);
		String sex = null;
		if (sex_num % 2 == 0) {
			sex = "女";
		} else {
			sex = "男";
		}
		return sex;
	}

	// 根据用户id获得年龄
	public static String get_age(int user_id) throws Exception {
		UserM user = UserM.dao.findById(user_id);
		String user_identity = user.getString("user_identity");
		String birthday = user_identity.substring(6, 14);

		Date date = new Date();
		date = Model.$Date.parse(birthday);

		Calendar calendar = Calendar.getInstance();
		// 获取当前时间毫秒值
		long now = (new Date()).getTime();
		long birthdate = date.getTime();
		long time = now - birthdate;
		int count = 0;
		// 时间换算
		long days = time / 1000 / 60 / 60 / 24;
		// 判断闰年
		int birth_year = Integer.parseInt((birthday.substring(0, 4)));

		for (int i = calendar.get(Calendar.YEAR); i >= birth_year; i--) {
			if ((i % 4 == 0 && !(i % 100 == 0)) || (i % 400 == 0)) {
				count++;
			}
		}
		// 加入闰年因素进行整理换算
		String age = String.valueOf((days - count) / 365);
		return age;
	}

	// 根据用户id获得生日
	public static String get_birthday(int user_id) throws Exception {
		UserM user = UserM.dao.findById(user_id);
		String user_identity = user.getString("user_identity");
		String birthday = user_identity.substring(6, 14);

		String year = birthday.substring(0, 4);
		String month = birthday.substring(4, 6);
		String day = birthday.substring(6, 8);
		String birthday2 = year + "-" + month + "-" + day;

		return birthday2;
	}

	/**
	 * 根据借款人的借款信息-查询该人最近的借款流程状态信息
	 */
	public static UserM.LoanProcessInformation get_loan_process_information(int user_id, UserM user) {
		if (user == null) {
			throw new RuntimeException("user is null");
		}
		//
		UserM.LoanProcessInformation loan_process_information = new UserM.LoanProcessInformation();
		int user_type = user.getInt("user_type");
		if (user_type != 2) {
			throw new RuntimeException("user_type!=2");
		}
		String user_identity = user.getString("user_identity");
		if (Utils.isNullOrEmptyString(user_identity)) {
			loan_process_information.setIndex("0_-1");// -1表示没有实名认证
			return loan_process_information;
		}

		int borrow_type = user.getInt("borrow_type");
		// 不用查询后面信息
		if (borrow_type == 0) {// 如果不是0 则必须含有申请单
			loan_process_information.setIndex("0_0");// 直接导航到消费贷借款页面
			return loan_process_information;
		}
		if (borrow_type != 1 && borrow_type != 2) {
			loan_process_information.setIndex("0_1");// 错误的理财类型
			return loan_process_information;
		}
		/**
		 * 查询最近的借款申请单
		 */
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findFirst("SELECT * from borrower_bulk_standard_apply_order WHERE user_id=?  order by id desc LIMIT 1", new Object[] { user_id });
		if (borrower_bulk_standard_apply_order == null) {
			loan_process_information.setIndex("1_0");
			return loan_process_information;
		} else {
			/**
			 * <pre>
			 * `state`'单子状态 
			 * 1.创建订单完成-*
			 * 2填写认证信息完成-但是由于非必填的资料没有上传-那么可以在三天内仍然可以修改---同时这个状态所做的事和系统撤回请求修改的信息一致-*
			 * 3已经提交申请
			 * 4被驳回修改认证信息-*
			 * 5系统审核失败
			 * 6系统审核成功
			 * </pre>
			 */
			long apply_order_id = borrower_bulk_standard_apply_order.getLong("id");
			int state = borrower_bulk_standard_apply_order.getInt("state");
			// 单子刚刚创建
			// state=6;
			if (state == 1) {// * 124-235
				int rate = UserAuthenticateLeftStatusM.get_rate(user_id);
				int all_rate = 100;
				String rate_ratio = transfer_num_to_string(new BigDecimal(rate), new BigDecimal(all_rate));

				loan_process_information.setIndex("1_1");

				loan_process_information.setPart(rate);
				loan_process_information.setAll(all_rate);
				loan_process_information.setRatio(rate_ratio);
				return loan_process_information;

			} else if (state == 2) {// *
				int rate = UserAuthenticateLeftStatusM.get_rate(user_id);
				int all_rate = 100;
				String rate_ratio = transfer_num_to_string(new BigDecimal(rate), new BigDecimal(all_rate));

				loan_process_information.setIndex("1_2");

				loan_process_information.setPart(rate);
				loan_process_information.setAll(all_rate);
				loan_process_information.setRatio(rate_ratio);
				return loan_process_information;

			} else if (state == 3) {

				loan_process_information.setIndex("1_3");
				return loan_process_information;
			} else if (state == 4) {// *
				int rate = UserAuthenticateLeftStatusM.get_rate(user_id);
				int all_rate = 100;
				String rate_ratio = transfer_num_to_string(new BigDecimal(rate), new BigDecimal(all_rate));

				loan_process_information.setIndex("1_4");
				loan_process_information.setPart(rate);
				loan_process_information.setAll(all_rate);
				loan_process_information.setRatio(rate_ratio);
				return loan_process_information;

			} else if (state == 5) {
				loan_process_information.setIndex("1_5");
				return loan_process_information;
			} else if (state == 6) {
				// 需要讨论凑集状态
				BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT * from borrower_bulk_standard_gather_money_order WHERE  apply_order_id=?", new Object[] { apply_order_id });
				if (borrower_bulk_standard_gather_money_order == null) {

					loan_process_information.setIndex("2_0");
					return loan_process_information;
				} else {
					/**
					 * <pre>
					 *  `gather_state` 
					 *   '凑集状态
					 *    1筹集中
					 *    2在规定的时间内凑集失败-这个凑集过程的监控需要定时器一直监控-需要采用临时表#-这个字段在申请表里面没有
					 *    3筹集成功',
					 * </pre>
					 */
					int gather_state = borrower_bulk_standard_gather_money_order.getInt("gather_state");
					// gather_state=3;
					if (gather_state == 1) {

						loan_process_information.setIndex("2_1");
						/**
						 * 输入凑集的进度
						 */
						int gather_progress = borrower_bulk_standard_gather_money_order.getInt("gather_progress");
						int all_rate = 100;
						String rate_ratio = transfer_num_to_string(new BigDecimal(gather_progress), new BigDecimal(all_rate));

						loan_process_information.setPart(gather_progress);
						loan_process_information.setAll(all_rate);
						loan_process_information.setRatio(rate_ratio);

						return loan_process_information;
					} else if (gather_state == 2) {

						loan_process_information.setIndex("2_2");
						return loan_process_information;
					} else if (gather_state == 3) {
						/**
						 * <pre>
						 * `payment_state` 
						 *  '还款状态 
						 *  1.还款之前
						 *  2.正在还款 3.还款完成',
						 * </pre>
						 */
						int payment_state = borrower_bulk_standard_gather_money_order.getInt("payment_state");
						long gather_money_order_id = borrower_bulk_standard_gather_money_order.getLong("id");
						if (payment_state == 1) {
							loan_process_information.setIndex("3_1");// 错误的状态
							return loan_process_information;

						} else if (payment_state == 2) {
							/**
							 * 首先判断是否有逾期的还款-如果有逾期的那么则需要进行逾期的提示，同时我们需要关注非逾期正常还款
							 */
							int pay_for_object = borrower_bulk_standard_gather_money_order.getInt("pay_for_object");
							// SELECT
							// total_periods,current_period,should_repayment_total,automatic_repayment_date
							// from borrower_bulk_standard_repayment_plan WHERE
							// gather_money_order_id=1 and is_repay=0

							/*
							 * `repay_start_time` timestamp NULL DEFAULT NULL
							 * COMMENT '还款开始时间', `repay_start_time_long`
							 * bigint(20) DEFAULT NULL COMMENT '还款开始时间毫秒数',
							 * `repay_end_time` timestamp NULL DEFAULT NULL
							 * COMMENT '还款截止时间', `repay_end_time_long`
							 * bigint(20) DEFAULT NULL COMMENT '还款截止时间毫秒数',
							 */
							loan_process_information.setIndex("3_2");
							long now = (new Date()).getTime();

							/**
							 * 只能查询出一个当月未还的还款计划
							 */
							BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = BorrowerBulkStandardRepaymentPlanM.dao.findFirst("SELECT total_periods, current_period, should_repayment_total, automatic_repayment_date FROM borrower_bulk_standard_repayment_plan WHERE gather_money_order_id =? AND is_repay = 0 AND repay_start_time_long >= ? order by id asc LIMIT 1",
									new Object[] { gather_money_order_id, now });
							// borrower_bulk_standard_repayment_plan=null;
							// pay_for_object=0;
							if (borrower_bulk_standard_repayment_plan == null) {
								// 只关注是否逾期
								if (pay_for_object == 0) {
									String msg = "尊敬的" + user.getString("nickname") + "先生/女生你好,系统出现了错误,麻烦你把这个情况告诉我们客服！谢谢";
									loan_process_information.setMsg(msg);
								} else if (pay_for_object == 1) {
									String msg = "尊敬的" + user.getString("nickname") + "先生/女生你好,您在我们平台申请的借款已经逾期还款,请尽快还款";
									loan_process_information.setMsg(msg);
								}
							} else {
								// 不需要关注逾期
								// should_repayment_total,
								// automatic_repayment_date
								BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
								String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
								// 您本期应还金额6000元，到期还款日12月29号，请按时还款，保持良好信用记录
								String msg = "您本期应还金额" + should_repayment_total.setScale(2, BigDecimal.ROUND_DOWN).toString() + "元，到期还款日" + automatic_repayment_date + ",请按时还款，保持良好信用记录";
								loan_process_information.setMsg(msg);
							}

							// loan_process_information.setMsg("");

							return loan_process_information;
						} else if (payment_state == 3) {
							loan_process_information.setIndex("3_3");
							return loan_process_information;
						} else {
							// 不存在该状态
							loan_process_information.setIndex("3_e");// 错误的状态
							return loan_process_information;
						}
					} else {

						// 不存在该状态
						loan_process_information.setIndex("2_e");// 错误的状态
						return loan_process_information;
					}
				}

			} else {

				// 不存在该状态
				loan_process_information.setIndex("1_e");// 错误的状态
				return loan_process_information;
			}

		}

	}

	/**
	 * 测试-分成屏蔽
	 * 
	 * @param user_id
	 * @return
	 */
	public static UserM.LoanProcessInformation get_loan_process_information_test(int user_id) {
		UserM.LoanProcessInformation loan_process_information = new UserM.LoanProcessInformation();
		loan_process_information.setIndex("0_0");
		loan_process_information.setPart(5);
		loan_process_information.setAll(10);
		loan_process_information.setRatio("0.5");
		return loan_process_information;

	}

	/**
	 * 
	 *用户借贷信息
	 */
	public static class LoanProcessInformation {
		public String getIndex() {
			return index;
		}

		public void setIndex(String index) {
			this.index = index;
		}

		public int getPart() {
			return part;
		}

		public void setPart(int part) {
			this.part = part;
		}

		public int getAll() {
			return all;
		}

		public void setAll(int all) {
			this.all = all;
		}

		public void setRatio(String ratio) {
			this.ratio = ratio;
		}

		public String getRatio() {
			return ratio;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public String getMsg() {
			return msg;
		}

		/**
		 * 记录该阶段申请凑集还款流程
		 */
		private String index = "";// 
		/**
		 * 信息完整度
		 */
		private int part = 0;// 部分
		private int all = 0;// 所有
		private String ratio = "0";// ratio
		/**
		 * 提示信息-用户还款提示
		 */
		private String msg = "";
	}

	/**
	 * 转换数字
	 */
	public static String transfer_num_to_string(BigDecimal num_a, BigDecimal num_b) {
		return (num_a.divide(num_b)).toString();
	}

}
