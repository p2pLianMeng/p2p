package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.trade.core.Controller;
import com.tfoll.web.domain.SysExpenses;
import com.tfoll.web.domain.SysIncome;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户和系统的资金变动由三部分构成:用户资金明细[user_money_change_records]-这个是最重要的-用户交易记录这个是给用户看的[
 * user_transaction_records]-系统财务盈利点[sys_income_records]-三张表
 * 
 * @author 曹正辉
 * 
 */
@TableBind(tableName = "user_money_change_records", primaryKey = "id")
public class UserMoneyChangeRecordsM extends Model<UserMoneyChangeRecordsM> {
	public static UserMoneyChangeRecordsM dao = new UserMoneyChangeRecordsM();

	/**
	 * 用户的每一步都需要记录-如果是含有精度的数字-在保存在数据库里面尽量以字符串的形式保存到数据库里面
	 */

	/**
	 * 充值与提现
	 */
	// OK
	//
	public static final int Type_11 = 11;// 充值操作-收入
	@SysIncome
	// 暂时不做-不然很多空的数据
	public static final int Type_12 = 12;// 充值操作-支出-手续费

	//
	public static final int Type_13 = 13;// 提现操作-支出
	public static final int Type_14 = 14;// 提现操作-支出-手续费
	public static final int Type_15 = 15;// 提现撤销操作-收入

	/**
	 * 联富宝相关
	 */
	// OK
	//
	public static final int Type_21 = 21;// 联富宝预定支付-支出
	@SysIncome
	public static final int Type_22 = 22;// 联富宝到预定截至时间未支付剩下的部分-支出-那么前面预定的部分则自动打给系统[系统盈利部分]-需要定时对这部分的资金进行资金处理

	public static final int Type_23 = 23;// 联富宝到预定截至时间支付全额-支出-支出剩余部分

	public static final int Type_24 = 24;// 联富宝开发阶段支付全额-收入-支出全额
	//
	// 提前退出
	public static final int Type_25 = 25;// 本金-联富宝提前退出-收入-收入投资本金及利息-同时需要扣除3%的手续费
	@SysExpenses
	public static final int Type_26 = 26;// 利息-联富宝提前退出-收入-收入投资本金及利息-同时需要扣除3%的手续费
	@SysIncome
	public static final int Type_27 = 27;// 手续费-联富宝提前退出-支出-收入投资本金及利息-同时需要扣除3%的手续费

	// 自动退出
	public static final int Type_28 = 28;// 联富宝回款-收入--收入全部投资本金[及利息]
	@SysExpenses
	public static final int Type_29 = 29;// 联富宝回款-收入--收入全部投资本金所对应的利息

	/**
	 * 散标借入还款事项
	 */
	// 向借款人打钱由两部分组成
	public static final int Type_31 = 31;// 打入借入者借款总额-收入
	@SysIncome
	public static final int Type_32 = 32;// 收取借入者借款服务费-支出

	// 向借款人收取本息由...部分组成.这个是正常还款的记录
	public static final int Type_33 = 33;// 偿还本息-支出
	@SysIncome
	public static final int Type_34 = 34;// 返还服务费..特别收取-支出
	//
	@SysIncome
	public static final int Type_35 = 35;// 借款管理费-支出
	@SysIncome
	public static final int Type_36 = 36;// 借款超期管理费-支出
	@SysIncome
	public static final int Type_37 = 37;// 普通罚息-1-30天内-支出
	@SysIncome
	public static final int Type_38 = 38;// 严重超期罚息-大于30天-支出
	//

	public static final int Type_51 = 51;// 提前还款本息-支出-需要支付给理财人借款剩余本金的1%作为违约金，不用再支付后续的利息及管理费用。
	@SysIncome
	public static final int Type_52 = 52;// 提前还款1%违约金-支出

	/**
	 * 散标理财收费事项
	 */
	// OK
	public static final int Type_71 = 71;// 理财支付费用-支出
	// 转让由两部分构成-如果是系统因为逾期转让则不收取手续费-但是要记录转让记录
	//
	public static final int Type_72 = 72;// 债权转让所得资金-收入
	@SysIncome
	public static final int Type_73 = 73;// 债权转让管理费-支出
	//
	public static final int Type_74 = 74;// 每月本息回收-收入
	public static final int Type_75 = 75;// 提前回收剩下的本息-收入

	/**
	 * 其他非核心的业务要求Key为200以上
	 */
	@SysExpenses
	public static final int Type_201 = 201;// 散标新手标利息
	@SysExpenses
	public static final int Type_202 = 202;// 联富宝新手标利息

	/**
	 *按照类型进行添加财务记录
	 * 
	 * @param user_id
	 *            用户ID
	 * @param type
	 *            交易类型-需要后台代码里面和设计的时候进行约定
	 * @param start_money
	 *            划扣或者收入前的可用资金
	 * @param pay
	 *            理应支出
	 * @param income
	 *            理应收入
	 * @param end_money
	 *            划扣或者收入后的可用资金
	 * @return
	 */
	public static boolean add_user_money_change_records_by_type(int user_id, int type, BigDecimal start_money, BigDecimal pay, BigDecimal income, BigDecimal end_money, String detail) {
		Date now = new Date();
		String add_time_date = Controller.Date.format(now);
		UserMoneyChangeRecordsM user_money_change_records = new UserMoneyChangeRecordsM();
		return user_money_change_records.//
				set("user_id", user_id).//
				set("type", type).//
				set("start_money", start_money).//
				set("pay", pay).//
				set("income", income).//
				set("end_money", end_money).//
				set("add_time_long", now.getTime()).//
				set("add_time_date", add_time_date).//
				set("detail", detail).//
				save();

	}

}
