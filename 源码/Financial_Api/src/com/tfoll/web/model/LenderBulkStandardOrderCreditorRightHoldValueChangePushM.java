package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.util.Date;

@TableBind(tableName = "lender_bulk_standard_order_creditor_right_hold_value_change_push", primaryKey = "id")
public class LenderBulkStandardOrderCreditorRightHoldValueChangePushM extends Model<LenderBulkStandardOrderCreditorRightHoldValueChangePushM> {

	public static LenderBulkStandardOrderCreditorRightHoldValueChangePushM dao = new LenderBulkStandardOrderCreditorRightHoldValueChangePushM();

	/**
	 * 系统或者用户还钱的时候需要进行债权转让推送消息-通知需要进行债权转入的人在这个时候不能进行操作-因为债权的价值这个时候会改变的 <br/>
	 * 理财还款债权价值修改消息推送类
	 */
	public static LenderBulkStandardOrderCreditorRightHoldValueChangePushM create_creditor_right_hold_value_change_push(long creditor_right_transfer_out_id) {
		LenderBulkStandardOrderCreditorRightHoldValueChangePushM lender_bulk_standard_order_creditor_right_hold_value_change_push = new LenderBulkStandardOrderCreditorRightHoldValueChangePushM();
		lender_bulk_standard_order_creditor_right_hold_value_change_push.//
				set("creditor_right_transfer_out_id", creditor_right_transfer_out_id).//
				set("failure_time", new Date());//

		return lender_bulk_standard_order_creditor_right_hold_value_change_push;
	}
}
