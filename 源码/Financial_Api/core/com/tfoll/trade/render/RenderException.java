package com.tfoll.trade.render;

public class RenderException extends RuntimeException {

	private static final long serialVersionUID = -4402731944301013633L;

	public RenderException() {
		super();
	}

	public RenderException(String message) {
		super(message);
	}

	public RenderException(String message, Throwable cause) {
		super(message, cause);
	}

	public RenderException(Throwable cause) {
		super(cause);
	}
}
