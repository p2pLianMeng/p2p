package com.tfoll.trade.plugin;

/**
 * 开发者可以将功能强大的组件添加到框架中
 */
// 本框架不提供插件
public interface IPlugin {
	boolean start();

	boolean stop();
}