package com.tfoll.trade.activerecord.db;

import com.tfoll.trade.activerecord.ActiveRecordException;
import com.tfoll.trade.activerecord.DataSourceKit;
import com.tfoll.trade.activerecord.transaction.IAtomic;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

/**
 * 存储过程调用
 * 
 * <pre>
 * public static void main(String[] args) {
 * 	Db.tx(new IAtomic() {
 * 
 * 		public boolean transactionProcessing() throws Exception {
 * 			Map&lt;Integer, Object&gt; in = new HashMap&lt;Integer, Object&gt;();
 * 			in.put(1, 1);//入参位置和数值
 * 			Map&lt;Integer, Integer&gt; out = new HashMap&lt;Integer, Integer&gt;();
 * 			out.put(2, Types.INTEGER);//出参数位置和类型
 * 
 * 			Map&lt;Integer, Object&gt; map = ProcedureCall.call(&quot;{call auto_exchange_funds_clear(?,?)}&quot;, in, out);//返回的是map
 * 
 * 			System.out.println(map.get(2));
 * 			return false;
 * 		}
 * 	});
 * }
 * </pre>
 */
public class ProcedureCall {
	public static Logger logger = Logger.getLogger(Db.class);

	public static Map<Integer, Object> call(String call, Map<Integer, Object> in, Map<Integer, Integer> out) {
		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();
			return call(conn, call, in, out);
		} catch (Exception e) {
			logger.error(e);
			throw new ActiveRecordException(e);
		} finally {
			DataSourceKit.close(conn);
		}
	}

	private static Map<Integer, Object> call(Connection conn, String call, Map<Integer, Object> in, Map<Integer, Integer> out) {
		CallableStatement cs = null;
		try {
			// 可以直接传入参数
			cs = conn.prepareCall(call);
			if (in != null && in.size() > 0) {
				for (Entry<Integer, Object> entry : in.entrySet()) {
					cs.setObject(entry.getKey(), entry.getValue());
				}
			}
			if (out != null && out.size() > 0) {
				for (Entry<Integer, Integer> entry : out.entrySet()) {
					cs.registerOutParameter(entry.getKey().intValue(), entry.getValue().intValue());
				}
			}
			cs.execute();
			if (out != null && out.size() > 0) {
				Map<Integer, Object> out_map = new HashMap<Integer, Object>();
				for (Entry<Integer, Integer> entry : out.entrySet()) {
					int index = entry.getKey().intValue();
					Object o = cs.getObject(index);
					out_map.put(index, o);
				}
				return out_map;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
		return null;
	}

	public static void main(String[] args) {

		Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				Map<Integer, Object> in = new HashMap<Integer, Object>();
				in.put(1, 1);
				Map<Integer, Integer> out = new HashMap<Integer, Integer>();
				out.put(2, Types.INTEGER);

				Map<Integer, Object> map = ProcedureCall.call("{call auto_exchange_funds_clear(?,?)}", in, out);

				System.out.println(map.get(2));
				return false;
			}
		});
	}
}
