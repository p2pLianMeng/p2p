package com.tfoll.web.timer.email_engine;

import com.tfoll.trade.config.Constants;
import com.tfoll.web.model.UserEmailRequestErrorM;
import com.tfoll.web.model.UserEmailRequestSuccessM;
import com.tfoll.web.model.UserEmailRequestWaitingM;

import org.apache.log4j.Logger;

public class EmailEngine {
	private static Logger logger = Logger.getLogger(EmailEngine.class);

	public static void doSubTaskForUserEmailRequestWaitingKey(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {
		if (user_email_request_waiting == null) {
			throw new NullPointerException("user_email_request_waiting is null");
		}
		int request_type = user_email_request_waiting.getInt("request_type");
		if (request_type == 1) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType1(user_email_request_waiting);
		}
		if (request_type == 2) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType2(user_email_request_waiting);
		}
		if (request_type == 3) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType3(user_email_request_waiting);
		}
		if (request_type == 4) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType4(user_email_request_waiting);
		}
		if (request_type == 5) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType5(user_email_request_waiting);
		}
		if (request_type == 6) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType6(user_email_request_waiting);
		}
		if (request_type == 7) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType7(user_email_request_waiting);
		}
		if (request_type == 8) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType8(user_email_request_waiting);
		}
		if (request_type == 9) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType9(user_email_request_waiting);
		}
		if (request_type == 10) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType10(user_email_request_waiting);
		}
		if (request_type == 11) {
			doSubTaskForUserEmailRequestWaitingKeyByRequestType11(user_email_request_waiting);
		}
	}

	/**
	 * 1发送用户注册邮箱验证码 1
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType1(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 1) {
			boolean send_ok = false;
			String failure_cause = "";
			try {

				send_ok = EmailEngineTemplate.send_emial_rgister_code(nickname, email, content);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 找回密码发送邮箱验证码 2
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType2(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String content = user_email_request_waiting.getString("content");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 2) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_find_password_email(email, content);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 人民币提现驳回
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType3(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 3) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_user_withdraw_back_email(email, nickname);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 人民币借款驳回 4
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType4(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 4) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_borrow_back_email(email, nickname);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 人民币借款审核通过 5
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType5(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 5) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_borrow_success_email(email, nickname);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 绑定邮箱 6
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType6(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 6) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_bingdingemail(nickname, email, content);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 人民币提现成功 7
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType7(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 7) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_user_withdraw_success_email(email, nickname);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 7) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 回款成功 8
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType8(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 8) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_outstanding_success_email(b[0], b[1], email, b[3], b[4], b[5], b[6], b[7]);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 7) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 还款成功 9
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType9(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 9) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_reimbursement_success_email(email, b[0], b[1], b[2], b[3]);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 7) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 提前还款成功 10
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType10(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 10) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_early_repayment_of_success_message(email, b[0], b[1], b[2], b[3]);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 7) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 严重逾期通知 11
	 * 
	 * @param user_email_request_waiting
	 * @throws Exception
	 */
	private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType11(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {

		long id = user_email_request_waiting.getLong("id");
		int user_id = user_email_request_waiting.getInt("user_id");
		String ip = user_email_request_waiting.getString("ip");
		String email = user_email_request_waiting.getString("email");
		String nickname = user_email_request_waiting.getString("nickname");
		String content = user_email_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_email_request_waiting.getInt("request_type");
		int retry_times = user_email_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 11) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = EmailEngineTemplate.send_seriously_overdue_notice_email(email, b[1]);// 关注这一句
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserEmailRequestSuccessM user_email_request_success = new UserEmailRequestSuccessM();
				user_email_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("email", email).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_email_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 7) {
					UserEmailRequestErrorM user_email_request_error = new UserEmailRequestErrorM();
					user_email_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("email", email).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_email_request_waiting.delete();
				} else {
					user_email_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}
}
