package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_email_request_success", primaryKey = "id")
public class UserEmailRequestSuccessM extends Model<UserEmailRequestSuccessM> {
	public static UserEmailRequestSuccessM dao = new UserEmailRequestSuccessM();
}
