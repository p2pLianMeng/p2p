package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_email_request_waiting", primaryKey = "id")
public class UserEmailRequestWaitingM extends Model<UserEmailRequestWaitingM> {
	public static UserEmailRequestWaitingM dao = new UserEmailRequestWaitingM();
}
