package com.tfoll.web.aop;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.TextRender;
import com.tfoll.web.action.system_manager.UserOnlineManagerLoginAction;

import javax.servlet.http.HttpSession;

/**
 * 判读用户在线管理是否登录
 * 
 */
public class UserOnLineManagerLoginedAop implements Interceptor {

	public void doIt(ActionExecutor ae) {
		HttpSession session = ActionContext.getRequest().getSession();
		Integer user_online_manager_login = (Integer) session.getAttribute(UserOnlineManagerLoginAction.class.getName());
		if (user_online_manager_login == null) {
			ActionContext.setRender(new TextRender("请先登录"));
			return;
		} else {
			ae.invoke();
		}

	}

}
