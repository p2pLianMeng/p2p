btc_add_address= 您正在使用天富网添加比特币提现地址,验证码为:code,请勿向任何人包括客服提供验证码
btc_withdrawal= 您正在使用天富网进行比特币提现,数量为:count验证码为:code,请勿向任何人包括客服提供验证码.

ltc_add_address= 您正在使用天富网添加莱特币提现地址,验证码为:code,请勿向任何人包括客服提供验证码.
ltc_withdrawal= 您正在使用天富网进行莱特币提现,数量为:count,验证码为:code,请勿向任何人包括客服提供验证码.

ctc_add_address = 您正在使用天富网添加凯特币提现地址,验证码为:code,请勿向任何人包括客服提供验证码.
ctc_withdrawal= 您正在使用天富网进行凯特币提现,数量为:count,验证码为:code,请勿向任何人包括客服提供验证码.

cny_withdrawal = 您正在使用天富网提现:count元,验证码为:code请勿向任何人包括客服提供验证码.
mony_pwd_update= 您正在修改资金密码,验证码为:code,请勿向任何人包括客服提供验证码.



user_phone_send_message=您本次使用的验证码为:code请勿向任何人包括TFoll客服提供验证码.

send_modify_login_pwd=您正在修改系统登录密码,验证码为:code.勿向任何人包括客服提供验证码.
send_message_4_update_pwd_money=您正在修改资金密码,验证码为:code,请勿向任何人包括客服提供验证码.

send_message_2_set_phoneNum=您正在绑定手机,验证码为:code,请勿向任何人包括客服提供验证码.
send_message_2_update_phoneNum=您正在修改绑定手机,验证码为:code,请勿向任何人包括客服提供验证码.

send_message_2_money_pwd=您正在设置资金密码,验证码为:code,请勿向任何人包括客服提供验证码.
send_message_2_money_pwd_update=您正在设置资金密码,验证码为:code,请勿向任何人包括客服提供验证码.