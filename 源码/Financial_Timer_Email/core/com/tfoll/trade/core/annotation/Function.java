package com.tfoll.trade.core.annotation;

/**
 * Function功能描述
 * 
 */
public @interface Function {
	/**
	 * 那些用户才能使用此功能。如果加在方法上面则不需要填写
	 */
	public String for_people() default "";

	/**
	 * 功能描述
	 */
	public String function_description();

	/**
	 * 最后一次用户更新作者
	 * 
	 */
	public String last_update_author();

}
