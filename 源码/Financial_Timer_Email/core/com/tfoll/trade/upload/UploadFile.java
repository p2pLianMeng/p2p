package com.tfoll.trade.upload;

import java.io.File;

public class UploadFile {
	public UploadFile(String multipartRequestFileNames, String saveDirectory, String filesystemName, String originalFileName, String contentType) {
		this.multipartRequestFileNames = multipartRequestFileNames;
		this.saveDirectory = saveDirectory;
		this.filesystemName = filesystemName;
		this.originalFileName = originalFileName;
		this.contentType = contentType;
	}

	/**
	 * Cos上传组件构建的文件名
	 */
	private String multipartRequestFileNames;
	private String saveDirectory;
	/**
	 * 上传后的文件名
	 */
	private String filesystemName;
	/**
	 * 上传以前的文件名
	 */
	private String originalFileName;

	/**
	 * 文件类型
	 */
	private String contentType;

	public String getContentType() {
		return contentType;
	}

	public File getFile() {
		if (saveDirectory == null || filesystemName == null) {
			throw new RuntimeException("文件上传保存路径为空或者上传后的文件名为空");
		} else {
			return new File(saveDirectory + File.separator + filesystemName);
		}
	}

	/**
	 * 上传临时文件后的文件名.<br/>
	 * 复制/移动该文件操作方法：fileUtil.copyFile/(uploadFile.getFilePath(),
	 * "D:\\1\\"+uploadFile.getFileName());
	 */
	public String getFileName() {
		return filesystemName;
	}

	/**
	 * 获取该文件在windows系统的文件路径
	 */
	public String getFilePath() {
		return new File(saveDirectory + File.separator + filesystemName).getAbsolutePath();
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public String getParameterName() {
		return multipartRequestFileNames;
	}

	public String getSaveDirectory() {
		return saveDirectory;
	}
}
