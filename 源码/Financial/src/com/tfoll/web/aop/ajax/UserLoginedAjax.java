package com.tfoll.web.aop.ajax;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.TextRender;
import com.tfoll.web.model.UserM;

import javax.servlet.http.HttpSession;

/**
 * 判读普通用户是否登录
 * 
 */
public class UserLoginedAjax implements Interceptor {

	public void doIt(ActionExecutor actionInvocation) {
		HttpSession session = ActionContext.getRequest().getSession();
		UserM user = (UserM) session.getAttribute("user");
		if (user == null) {
			ActionContext.setRender(new TextRender("NoLogined"));
			return;
		}
		Object user_id_object = user.get("id");
		if (user_id_object != null) {
			actionInvocation.invoke();
		} else {
			ActionContext.setRender(new TextRender("user_id_object is null"));
			return;
		}

	}

}
