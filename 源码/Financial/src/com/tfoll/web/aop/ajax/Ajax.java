package com.tfoll.web.aop.ajax;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;

import javax.servlet.http.HttpServletResponse;

/**
 * 清除IE缓存
 * 
 */
public class Ajax implements Interceptor {

	public void doIt(ActionExecutor actionExecutor) {
		HttpServletResponse response = actionExecutor.getAction().getController().getResponse();
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "-1");
		actionExecutor.invoke();
	}

}
