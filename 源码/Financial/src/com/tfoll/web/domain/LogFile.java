package com.tfoll.web.domain;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 日志文件信息封装类
 * 
 * @author xiangxuan
 * 
 */
public class LogFile {
	// 文件名
	private String file_name;
	// 文件路径
	private String file_path;
	// 文件最后修改时间
	private long last_modify_time;

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String fileName) {
		file_name = fileName;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String filePath) {
		file_path = filePath;
	}

	public long getLast_modify_time() {
		return last_modify_time;
	}

	public void setLast_modify_time(long lastModifyTime) {
		last_modify_time = lastModifyTime;
	}

	/**
	 * 得到日志集合
	 * 
	 * @return
	 */
	public static List<LogFile> get_log_file_list() {
		File log_folder = new File("D:/log4j/all");
		if (log_folder == null) {
			throw new RuntimeException("日志文件夹不存在!");
		}
		File[] log_files = log_folder.listFiles(new FileFilter() {

			public boolean accept(File file) {
				if (file.getName().contains("'")) {
					return true;
				} else {
					return false;
				}
			}
		});
		if (log_files == null || (log_files != null && log_files.length == 0)) {
			return null;
		}
		List<LogFile> log_file_list = new ArrayList<LogFile>();
		/**
		 * 将日志文件遍历 放入list集合里面
		 */
		for (File file : log_files) {
			String file_name = file.getName();
			String file_path = file.getAbsolutePath();
			long last_modify_time = file.lastModified();
			LogFile logFile = new LogFile();
			logFile.setFile_name(file_name);
			logFile.setFile_path(file_path);
			logFile.setLast_modify_time(last_modify_time);
			log_file_list.add(logFile);
		}
		Collections.sort(log_file_list, new Comparator<LogFile>() {

			public int compare(LogFile o1, LogFile o2) {

				long last_modify_time_this = o1.getLast_modify_time();
				long last_modify_time_other = o2.getLast_modify_time();

				if (last_modify_time_this <= last_modify_time_other) {
					return 1;
				} else {
					return -1;

				}

			}
		});
		return log_file_list;
	}

}
