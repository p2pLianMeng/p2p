package com.tfoll.web.action.user_online_manager;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserOnLineManagerLoginedAop;
import com.tfoll.web.domain.LogFile;
import com.tfoll.web.util.Utils;

import java.io.File;
import java.util.List;

@ActionKey("/log_manager")
public class LogManagerAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserOnLineManagerLoginedAop.class)
	@Function(for_people = "所有人", function_description = "日志登录认证", last_update_author = "向旋")
	public void index() {
		List<LogFile> log_file_list = LogFile.get_log_file_list();
		setAttribute("log_file_list", log_file_list);
		renderJsp("/user_online_manager/log_manager/log_manager.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserOnLineManagerLoginedAop.class)
	@Function(for_people = "所有人", function_description = "日志文件下载", last_update_author = "向旋")
	public void download() {
		String path = getParameter("path");
		if (!Utils.isNotNullAndNotEmptyString(path)) {
			renderText("路径为空");
			return;
		}
		File log_file = new File(path);
		renderFile(log_file);
		return;
	}
}