package com.tfoll.web.action.index;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.model.AboutFriendShipConnectionM;
import com.tfoll.web.model.AboutInfoM;
import com.tfoll.web.model.AboutTypeM;
import com.tfoll.web.util.WebApp;
import com.tfoll.web.util.page.PageDiv2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * 关于我们
 * 
 * @author tf
 * 
 */
@ActionKey("/index/about")
public class AboutAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入关于我们页面", last_update_author = "hx")
	public void about_page() {
		String type = getParameter("flag", "intro");
		if ("".equals(type) || type == null) {
			renderText("非法访问!");
			return;
		}
		if (type.equals("intro")) {

			/**
			 * 公司简介
			 */
			get_about_list("intro");
			renderJsp("/index/about_us/about_us.jsp");
			return;
		} else if (type.equals("team")) {
			/**
			 * 管理团队
			 */
			get_about_list("team");
			renderJsp("/index/about_us/team_manage.jsp");
			return;
		} else if (type.equals("news")) {
			/**
			 * 最新动态
			 */
			get_about_list("news");
			renderJsp("/index/about_us/last_news.jsp");
			return;
		} else if (type.equals("notice")) {
			/**
			 *网站公告 分页缓存查询
			 */
			get_about_list("notice");
			int pn = getParameterToInt("pn", 1);
			get_about_info_list_page(pn, "notice");

			renderJsp("/index/about_us/site_notice.jsp");
			return;
		} else if (type.equals("contact")) {
			/**
			 * 联系我们
			 */
			get_about_list("contact");
			renderJsp("/index/about_us/contact_us.jsp");
			return;
		} else if (type.equals("consult")) {
			/**
			 * 最新资讯 分页缓存查询
			 */
			get_about_list("consult");
			int pn = getParameterToInt("pn", 1);
			get_about_info_list_page(pn, "consult");

			renderJsp("/index/about_us/last_consult.jsp");
			return;
		} else if (type.equals("agreement")) {
			/**
			 * 协议
			 */
			get_about_list("agreement");
			renderJsp("/index/about_us/agreement/agreement.jsp");
			return;
		} else if (type.equals("join_us")) {
			/**
			 * 加入我们
			 */
			get_about_list("join_us");
			renderJsp("/index/about_us/join_us.jsp");
			return;
			/*
			 * } else if (type.equals("expert_adviser")) {
			 *//**
			 * 专家顾问
			 */
			/*
			 * get_about_list("expert_adviser");
			 * renderJsp("/index/about_us/expert_adviser.jsp"); return;
			 */
		} else if (type.equals("feed_back")) {
			/**
			 * 意见反馈
			 */
			get_about_list("feed_back");
			renderJsp("/index/about_us/feed_back.jsp");
			return;
		} else if (type.equals("tariff")) {
			/**
			 * 资费说明
			 */
			get_about_list("tariff");
			renderJsp("/index/about_us/tariff_description.jsp");
			return;
		} else if (type.equals("friendship_link")) {
			/**
			 * 友情链接
			 */
			get_about_list("friendship_link");
			// 查询文字链接
			String words_link_sql = "SELECT * FROM about_friendship_connetion WHERE is_effect = 1 AND type = 1";
			// 查询图片链接
			String images_link_sql = "SELECT * FROM about_friendship_connetion WHERE is_effect = 1 AND type = 2";
			/**
			 * 文字链接 集合
			 */
			List<AboutFriendShipConnectionM> about_friend_ship_conn_words_list = AboutFriendShipConnectionM.dao.find(words_link_sql);
			/**
			 * 图片链接 集合
			 */
			List<AboutFriendShipConnectionM> about_friend_ship_conn_images_list = AboutFriendShipConnectionM.dao.find(images_link_sql);

			StringBuffer words_link_buffer = new StringBuffer("<tr><td class=\"td_1\">文字链接</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr>");
			if (about_friend_ship_conn_words_list != null) {
				int n = 0;
				for (AboutFriendShipConnectionM about_friend_ship_conn_words : about_friend_ship_conn_words_list) {
					String url = about_friend_ship_conn_words.getString("site_url");
					String site_name = about_friend_ship_conn_words.getString("site_name");
					n = n + 1;
					if (n % 4 == 0) {
						words_link_buffer.append("<td width=\"25%\"><a target=\"_blank\" href=\"" + url + "\">" + site_name + "</a></td></tr><tr>");
					} else {
						words_link_buffer.append("<td width=\"25%\"><a target=\"_blank\" href=\"" + url + "\">" + site_name + "</a></td>");
					}

				}
				String words_link_str = "";
				if (n % 4 == 0) {
					words_link_str = words_link_buffer.toString();
					words_link_str = words_link_str.substring(0, words_link_str.length() - 4);
				} else {
					int m = 4 - (n % 4);
					for (int i = 1; i <= m; i++) {
						words_link_buffer.append("<td>&nbsp;</td>");
					}
					words_link_buffer.append("</tr>");
					words_link_str = words_link_buffer.toString();
				}
				// System.out.println("==========文字链接===============>>>>"+words_link_str);
				setAttribute("words_link_str", words_link_str);
			}
			/**
			 * 图片链接
			 */

			if (about_friend_ship_conn_images_list != null) {
				StringBuffer image_link_buffer = new StringBuffer(" <tr><td class=\"td_1\" width=\"20%\">图片链接</td><td width=\"20%\">&nbsp;</td><td width=\"20%\">&nbsp;</td> <td width=\"20%\">&nbsp;</td><td width=\"20%\">&nbsp;</td></tr><tr>");
				int n = 0;
				HttpServletRequest request = getRequest();
				String __ROOT_PATH__ = WebApp.getWebRootPath(request);
				String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
				for (AboutFriendShipConnectionM about_friend_ship_conn_images : about_friend_ship_conn_images_list) {
					String site_url = about_friend_ship_conn_images.getString("site_url");
					String image_name = about_friend_ship_conn_images.getString("image_name");
					n = n + 1;
					if (n % 5 == 0) {
						image_link_buffer.append("<td width=\"20%\"><a target=\"_blank\" href=\"" + site_url + "\">" + "<img src=\"" + __PUBLIC__ + "/images/" + image_name + "\" />" + "</a></td></tr><tr>");
					} else {
						image_link_buffer.append("<td width=\"20%\"><a target=\"_blank\" href=\"" + site_url + "\">" + "<img src=\"" + __PUBLIC__ + "/images/" + image_name + "\" />" + "</a></td>");
					}

				}
				String images_link_str = "";
				if (n % 5 == 0) {
					images_link_str = image_link_buffer.toString();
					images_link_str = images_link_str.substring(0, images_link_str.length() - 4);
				} else {
					int m = 5 - (n % 5);
					for (int i = 1; i <= m; i++) {
						image_link_buffer.append("<td width=\"20%\">&nbsp;</td>");
					}
					image_link_buffer.append("</tr>");
					images_link_str = image_link_buffer.toString();
				}
				// System.out.println("==========图片链接===============>>>>"+images_link_str);
				setAttribute("images_link_str", images_link_str);
			}

			renderJsp("/index/about_us/friendship_link.jsp");
		} else {

			renderText("非法访问!");
			return;
		}

	}

	private void get_about_list(String tag_type) {
		String sql = "SELECT type_name,url,tag FROM about_type WHERE is_effect = 1 ORDER BY sort ASC ";
		String str = "<ul>";
		StringBuffer buffer = new StringBuffer(str);
		List<AboutTypeM> about_type_list = AboutTypeM.dao.find(sql);
		HttpServletRequest request = getRequest();
		String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
		for (AboutTypeM about_type : about_type_list) {
			String tag = about_type.getString("tag");
			if (tag.equals(tag_type)) {
				buffer.append("<li>");
				buffer.append("<a href=\"" + __ROOT_PATH__ + about_type.getString("url") + "\"" + "class=\"hover\">" + about_type.getString("type_name") + "</a>");
				buffer.append("</li>");
			} else {
				buffer.append("<li>");
				buffer.append("<a href=\"" + __ROOT_PATH__ + about_type.getString("url") + "\">" + about_type.getString("type_name") + "</a>");
				buffer.append("</li>");
			}
		}
		buffer.append("</ul>");
		setAttribute("list_str", buffer.toString());
	}

	/**
	 * 分页 查询 最新资讯 和 网站公告
	 * 
	 * @param pn
	 *            当前页数
	 * @param string
	 */
	private void get_about_info_list_page(int pn, String string) {
		String count_sql = "";
		String sql_list = "";
		if ("consult".equals(string)) {
			count_sql = "SELECT count(1) FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '最新资讯' ORDER BY t1.id DESC ";
			sql_list = "SELECT t1.* FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '最新资讯' ORDER BY t1.id DESC";
		} else if ("notice".equals(string)) {
			count_sql = "SELECT count(1) FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '网站公告' ORDER BY t1.id DESC ";
			sql_list = "SELECT t1.* FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '网站公告' ORDER BY t1.id DESC";
		}
		Long total_rows = Db.queryLong(count_sql);
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn, 4);//
			List<AboutInfoM> list = AboutInfoM.dao.find(page.creatLimitSql(sql_list));
			List<Record> record_list = new ArrayList<Record>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			for (AboutInfoM about_info : list) {
				Record record = new Record();

				int id = about_info.getInt("id");
				// String content = about_info.getString("content");
				String info_title = about_info.getString("info_title");
				Date add_time = about_info.getTimestamp("add_time");
				// content = HtmlRegexpUtil.filterHtmlContent4Show(content);

				if (info_title.length() > 30) {
					info_title = info_title.substring(0, 25);
					info_title = info_title + "...";
				}
				String add_time_str = format.format(add_time);
				record.add("id", id);
				record.add("content", info_title);
				record.add("add_time_str", add_time_str);
				record_list.add(record);
			}
			String url = PageDiv2.createUrl(getRequest(), "/index/about/about_page");
			String pageDiv = PageDiv2.getDiv(url, "&flag=" + string, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("record_list", record_list);
			setAttribute("pageDiv", pageDiv);

		} else {
			setAttribute("tips", "暂时无数据");
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "最新资讯详情", last_update_author = "hx")
	public void last_consult_detail() {
		Integer id = getParameterToInt("id", 0);
		if (id != null && id > 0) {
			AboutInfoM aboutInfo = AboutInfoM.dao.findById(id);
			if (aboutInfo == null) {
				renderText("非法请求路径");
				return;
			}
			String content = aboutInfo.getString("content");
			// content = HtmlRegexpUtil.filterHtmlContent4Show(content);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date add_time = aboutInfo.getTimestamp("add_time");
			String add_time_str = sdf.format(add_time);
			aboutInfo.set("add_time", add_time_str);
			aboutInfo.set("content", content);
			setAttribute("aboutInfo", aboutInfo);
		}
		// 设置左部导航
		get_about_list("consult");
		renderJsp("/index/about_us/last_consult_detail.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "网站公告详情", last_update_author = "hx")
	public void site_notice_detail() {
		Integer id = getParameterToInt("id", 0);
		if (id != null && id > 0) {
			AboutInfoM aboutInfo = AboutInfoM.dao.findById(id);
			if (aboutInfo == null) {
				renderText("非法请求路径");
				return;
			}
			String content = aboutInfo.getString("content");
			// content = HtmlRegexpUtil.filterHtmlContent4Show(content);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date add_time = aboutInfo.getTimestamp("add_time");
			String add_time_str = sdf.format(add_time);
			aboutInfo.set("add_time", add_time_str);
			aboutInfo.set("content", content);
			setAttribute("aboutInfo", aboutInfo);
		}
		// 设置左部导航
		get_about_list("notice");
		renderJsp("/index/about_us/site_notice_detail.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement() {
		renderJsp("/index/about_us/agreement/agreement.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_1() {
		renderJsp("/index/about_us/agreement/agreement_1.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_2() {
		renderJsp("/index/about_us/agreement/agreement_2.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_3() {
		renderJsp("/index/about_us/agreement/agreement_3.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_4() {
		renderJsp("/index/about_us/agreement/agreement_4.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_5() {
		renderJsp("/index/about_us/agreement/agreement_5.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_6() {

		renderJsp("/index/about_us/agreement/agreement_6.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_7() {

		renderJsp("/index/about_us/agreement/agreement_7.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_8() {

		renderJsp("/index/about_us/agreement/agreement_8.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_9() {

		renderJsp("/index/about_us/agreement/agreement_9.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_10() {

		renderJsp("/index/about_us/agreement/agreement_10.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_11() {

		renderJsp("/index/about_us/agreement/agreement_11.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_12() {

		renderJsp("/index/about_us/agreement/agreement_12.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_13() {
		renderJsp("/index/about_us/agreement/agreement_13.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "协议", last_update_author = "hx")
	public void agreement_14() {
		renderJsp("/index/about_us/agreement/agreement_14.jsp");
		return;

	}

}
