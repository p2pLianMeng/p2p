package com.tfoll.web.action.index;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;

/**
 * 新手指导
 * 
 * @author hx
 * 
 */

@ActionKey("/index/newguidance")
public class NewGuidanceAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入新手指导投资页面", last_update_author = "hx")
	public void invest() {
		renderJsp("/index/new_guidance_invest.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入新手指导借款页面", last_update_author = "hx")
	public void borrow() {
		renderJsp("/index/new_guidance_borrow.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入新手指导安全保障页面", last_update_author = "hx")
	public void security() {
		renderJsp("/index/new_guidance_security.jsp");
		return;
	}
}
