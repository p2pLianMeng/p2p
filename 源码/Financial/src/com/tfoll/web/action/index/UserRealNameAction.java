package com.tfoll.web.action.index;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.domain.GetUserIdentityInfo;
import com.tfoll.web.domain.GetUserIdentityInfo.UserIdentityInfo;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ActionKey("/index")
public class UserRealNameAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "实名认证", last_update_author = "zjb")
	public void real_name() {
		String email = getParameter("email", null);
		String identity = getParameter("identity");
		String real_name = getParameter("real_name");

		if (!Utils.isNotNullAndNotEmptyString(real_name)) {
			renderText("0"); // 请输入姓名
			return;
		}

		String regEx = "[\u4E00-\u9FA5]{2,5}(?:·[\u4E00-\u9FA5]{2,5})*"; // 表示a或F
		Pattern pat = Pattern.compile(regEx);
		Matcher mat = pat.matcher(real_name);
		boolean rs = mat.find();
		if (!rs) {
			renderText("1"); // 姓名有误
			return;
		}

		if (!Utils.isNotNullAndNotEmptyString(identity)) {
			renderText("2"); // 请输入身份证号码
			return;
		}
		UserIdentityInfo useridentityinfo = GetUserIdentityInfo.getUserIdentityInfo(identity);
		// useridentityinfo=new UserIdentityInfo();
		if (useridentityinfo == null) {
			renderText("3"); // 身份证号码有误
			return;
		} else {
			UserM user = UserM.dao.findFirst("select * from user_info where email = ? ", email);
			if (Utils.isNotNullAndNotEmptyString(email)) {
				if (!Utils.isNotNullAndNotEmptyString(user.getString("identity"))) {
					user.set("user_identity", identity).set("real_name", real_name).update();
					renderText("4"); // 认证成功
					return;
				} else {
					renderText("5");// 用户已经实名认证
					return;
				}
			} else {
				renderText("6");// 用户不存在
				return;
			}

		}

	}

	public static void main(String[] args) {
		UserIdentityInfo useridentityinfo = GetUserIdentityInfo.getUserIdentityInfo("51302919880613035X");
		System.out.println(useridentityinfo + "---------------" + useridentityinfo.getBirthday());
	}

}
