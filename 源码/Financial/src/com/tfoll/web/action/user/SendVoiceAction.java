package com.tfoll.web.action.user;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.common.voice.VoiceVerifySDK;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.GetCode;
import com.tfoll.web.util.Utils;

@ActionKey("/user/sendvoice")
public class SendVoiceAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "登录", function_description = "绑定手机发送语音验证码", last_update_author = "向旋")
	public void binding_phone_send_voice() {
		String tel = getParameter("phone");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("4");
			return;
		}
		String code = GetCode.get_code();
		setSessionAttribute("binding_phone_voice", code);
		boolean send_ok = VoiceVerifySDK.voiceVerify(code, tel);
		if (!send_ok) {
			renderText("2");// 发送失败
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "修改手机发语音验证码", last_update_author = "向旋")
	public void update_phone_send_voice() {
		String old_tel = getParameter("old_tel");
		if (!Utils.isNotNullAndNotEmptyString(old_tel)) {
			renderText("5");// 原手机号为空
			return;
		}
		UserM user2 = getSessionAttribute(SystemConstantKey.User);
		String phone = user2.getString("phone");
		if (!old_tel.equals(phone)) {
			renderText("6");// 原手机号填写错误
			return;
		}
		String tel = getParameter("tel");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("1");// 手机号为空
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where phone=?", tel);
		if (user != null) {
			renderText("2");// 该手机号已经被使用
			return;
		}
		String code = GetCode.get_code();
		setSessionAttribute("update_phone_voice", code);
		boolean send_ok = VoiceVerifySDK.voiceVerify(code, tel);
		if (!send_ok) {
			renderText("3");
			return;
		}
		renderText("4");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "绑定提现密码发送语音验证码", last_update_author = "向旋")
	public void binding_withdrow_password_send_voice() {
		String withdraw_password = getParameter("withdraw_password");
		if (!Utils.isNotNullAndNotEmptyString(withdraw_password)) {
			renderText("1");
			return;
		}
		if (withdraw_password.length() < 6 || withdraw_password.length() > 35) {
			renderText("2");
			return;
		}
		String withdraw_password2 = getParameter("withdraw_password2");
		if (!Utils.isNotNullAndNotEmptyString(withdraw_password2)) {
			renderText("3");
			return;
		}
		if (!withdraw_password.equals(withdraw_password2)) {
			renderText("4");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String tel = user.getString("phone");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("5");
			return;
		}
		String code = GetCode.get_code();
		setSessionAttribute("binding_withdraw_voice", code);
		boolean send_ok = VoiceVerifySDK.voiceVerify(code, tel);
		if (!send_ok) {
			renderText("6");
			return;
		}
		renderText("7");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "找回提现密码发送语音验证码", last_update_author = "向旋")
	public void back_withdraw_password_send_voice() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		if (user == null) {
			renderText("1");
			return;
		}
		String phone = user.getString("phone");
		String code = GetCode.get_code();
		setSessionAttribute("back_withdraw_voice", code);
		boolean send_ok = VoiceVerifySDK.voiceVerify(code, phone);
		if (!send_ok) {
			renderText("2");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "登录", function_description = "找回密码发送语音验证码", last_update_author = "向旋")
	public void back_password_send_voice() {
		String phone = getParameter("phone");
		String code = GetCode.get_code();
		setSessionAttribute("back_password_voice", code);
		boolean send_ok = VoiceVerifySDK.voiceVerify(code, phone);
		if (!send_ok) {
			renderText("1");
			return;
		}
		renderText("2");
		return;
	}
}
