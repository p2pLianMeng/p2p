package com.tfoll.web.action.user.financial;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.UserAppointmentM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserAutomaticallyBidM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.CommonRepayMethod;
import com.tfoll.web.util.RiskParameterFilter;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebApp;
import com.tfoll.web.util.WebLogRecordsUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ActionKey("/user/financial/borrow")
public class BorrowAction extends Controller {

	/**
	 * <pre>
	 * 关于处理生意贷和消费贷点击申请借款和填写申请借款成功跳转到第二步和点击申请借款列表进来的数据<br/>
	 * 	public static String Applying_Order_Id_In_Request = "apply_order_id_in_request";<br/>
	 *  public static String Applying_Order_Id_In_Session = "apply_order_id_in_session";<br/>
	 * </pre>
	 */

	public static String get_value_from_request_and_session_of_apply_order_id() {// 提交页面导航
		String value = (String) ActionContext.getRequest().getParameter(SystemConstantKey.Applying_Order_Id_In_Request);
		if (value != null) {
			ActionContext.getHttpSession().setAttribute(SystemConstantKey.Applying_Order_Id_In_Session, value);
			return value;
		} else {
			value = (String) ActionContext.getHttpSession().getAttribute(SystemConstantKey.Applying_Order_Id_In_Session);
			return value;
		}

	}

	/**
	 * <pre>
	 * 列表进入借贷流程
	 *  setRequestAttributeToSessionAttribute-of-apply_order_id申请单ID
	 * </pre>
	 */
	public static void set_request_and_session_of_apply_order_id(long apply_order_id) {
		ActionContext.getHttpSession().setAttribute(SystemConstantKey.Applying_Order_Id_In_Session, apply_order_id + "");
		ActionContext.getRequest().setAttribute(SystemConstantKey.Applying_Order_Id_In_Request, apply_order_id + "");

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入我要借款-该界面不需要用户登录的拦截器进行权限验证", last_update_author = "hx")
	public void loan_applay() {
		/**
		 * 清空
		 */
		setAttribute(SystemConstantKey.Applying_Order_Id_In_Session, null);
		setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, null);

		int borrow_type = getParameterToInt("borrow_type", 0);
		if (!(borrow_type == 1 || borrow_type == 2 || borrow_type == 3)) {
			renderText("页面错误");
			return;
		}
		if (borrow_type == 1) {
			renderJsp("/user/financial/loan_applay_index_consume.jsp");
			return;
		} else if (borrow_type == 2) {
			renderJsp("/user/financial/loan_applay_index_business.jsp");
			return;
		} else if (borrow_type == 3) {
			renderJsp("/user/financial/loan_applay_index_net.jsp");
			return;
		} else {
			renderHttp(WebApp.getWebRootPath(getRequest()));
			return;
		}

	}

	/**
	 * 查询某人进行申请过没-有了申请单才能在系统认证后进行凑集
	 */
	public static final String Sql_Count_Apply_Order = "SELECT COUNT(1) from borrower_bulk_standard_apply_order WHERE user_id=?";
	/**
	 * 查询申请单-该SQL以后需要性能优化
	 */
	public static final String Sql_Last_Apply_Order = "select * from borrower_bulk_standard_apply_order where user_id = ? order by save_time desc limit 1";
	/**
	 * 根据申请单ID查询凑集单
	 */
	public static final String Sql_Last_Gather_Money_Order = "select * from borrower_bulk_standard_gather_money_order where apply_order_id = ? ";// 不需要关联use_id进行查询

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class })
	@Function(for_people = "所有人", function_description = "判断是否有借款资格    1_表示基本信息  2_表示申请信息审核对应的状态 3_凑集的状态4_还款状态", last_update_author = "lh")
	public void judge_borrow_qualification() { //
		UserM user = getSessionAttribute(SystemConstantKey.User);
		if (user == null) { // 没有登录跳转到登录页面
			renderText("1_1");
			return;
		}
		int user_id = user.getInt("id");
		user = UserM.dao.findById(user_id);

		/**
		 * 判断该用户是否邮箱认证成功
		 */
		int is_active = user.getInt("is_active");
		if (is_active == -1) {// 还没有邮箱认证
			renderText("1_2");
			return;
		}
		if (is_active == 0) {// 该用户被禁用
			renderText("1_3");
			return;
		}
		String user_identity = user.getString("user_identity");
		if (Utils.isNullOrEmptyString(user_identity)) {// 没有进行实名认证
			renderText("1_4");
			return;
		}
		String phone = user.getString("phone");
		if (Utils.isNullOrEmptyString(phone)) {// 手机没有绑定
			renderText("1_5");
			return;
		}

		/**
		 * 区分生意贷和消费贷需要查看之前是不是理财和借贷用户
		 */

		/**
		 * 页面接受的borrow_type-user.borrow_type只能借一种 0 1 2 3
		 */
		int borrow_type_from_page = getParameterToInt("borrow_type", -1);
		if (!(borrow_type_from_page == 1 || borrow_type_from_page == 2 || borrow_type_from_page == 3)) {
			renderText("2_1");// 不需要返回
			return;
		}
		int borrow_type = user.getInt("borrow_type");// 保存后这个需要修改-session and db
		if (borrow_type == 1) {
			if (!(borrow_type_from_page == 1)) {// 只能消费贷
				renderText("2_2");
				return;
			}
		} else if (borrow_type == 2) {
			if (!(borrow_type_from_page == 2)) {// 只能生意贷
				renderText("2_3");
				return;
			}
		} else if (borrow_type == 3) {// 只能净值贷
			if (borrow_type_from_page != 3) {
				renderText("2_4");
				return;
			}
		} else if (borrow_type == 0) {// 用户还没有进行选择
			// 判断数据库里面对应的数据是否还了 is_not_pay
			int is_not_pay = user.getInt("is_not_pay");
			if (is_not_pay == 1) {
				/**
				 * 提示需要管理员修改数据-程序需要修改borrow_type=0 and
				 * is_not_pay=0:包含审核失败-审核成功，但凑集失败-审核成功，凑集成功，且还款成功
				 */
				renderText("2_5");
				return;
			} else {
				/**
				 * 进行生意贷和消费贷，无论理财与否，净值贷都可以
				 */
				// OK可以提交新的单子(生意贷 ，消费贷，净值贷)-2_6[可以进行借贷采用这个Key-不变了]
				renderText("2_6");
				return;

			}

		} else {
			renderText("错误的借贷类型");// 不需要返回
			return;
		}

		/**
		 * 后面的表示有申请单或者更甚至的是有凑集单存在
		 */
		// 借款款单中有记录
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findFirst(Sql_Last_Apply_Order, new Object[] { user_id });
		if (borrower_bulk_standard_apply_order == null) {
			renderText("3_0");// 借款单中没有记录，查找不到...记录
			return;
		}

		/**
		 * <pre>
		 * 单子状态 
		 * 1创建订单完成，
		 * 2填写认证信息完成-但是由于非必填的资料没有上传-那么可以在三天内仍然可以修改---同时这个状态所做的事和系统撤回请求修改的信息一致，
		 * 3已经提交申请，
		 * 4被驳回修改认证信息， 
		 * 5系统审核失败，
		 * 6系统审核成功
		 *</pre>
		 */
		int apply_state = borrower_bulk_standard_apply_order.getInt("state"); // 申请状态
		if (apply_state == 1) {// 单子创建完成,后面该填写和修改用户认证信息
			/**
			 * 页面渲染对象-三种路径来源-两种方式来传递apply_order_id
			 */
			setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, borrower_bulk_standard_apply_order.getLong("id") + "");
			renderText("3_1");
			return;
		} else if (apply_state == 2) {// 填写认证信息完成-但是由于非必填的资料没有上传-那么可以在三天内仍然可以修改
			setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, borrower_bulk_standard_apply_order.getLong("id") + "");
			renderText("3_2");
			return;
		} else if (apply_state == 3) {// 已经提交，跳到审核页面
			setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, borrower_bulk_standard_apply_order.getLong("id") + "");
			renderText("3_3");
			return;
		} else if (apply_state == 4) {// 被审核员驳回，填写和修改用户认证信息
			setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, borrower_bulk_standard_apply_order.getLong("id") + "");
			renderText("3_4");
			return;
		} else if (apply_state == 5) {// 审核不通过，可以借款-基本上不会执行到这里-如果执行到这里那么程序就是错的
			renderText("3_5");
			return;
		} else if (apply_state != 6) { // 有审核通过的单子
			renderText("3_e");// 非法状态
			return;
		} else {// apply_state==6
			long apply_order_id = borrower_bulk_standard_apply_order.getLong("id");

			BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst(Sql_Last_Gather_Money_Order, new Object[] { apply_order_id });
			if (borrower_bulk_standard_gather_money_order == null) {
				renderText("4_0"); // 提示根据审核成功申请单去凑集单查不到记录
				return;
			}
			// 凑集状态
			// 采用临时表监控凑集状态:1凑集中,2在规定的时间内凑集失败,3凑集成功
			int gather_state = borrower_bulk_standard_gather_money_order.getInt("gather_state"); // 筹款状态
			if (gather_state == 1) {// 有单子正在筹款，不能借
				setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, borrower_bulk_standard_apply_order.getLong("id") + "");
				renderText("4_1");
				return;
			} else if (gather_state == 2) {// 筹集失败，可以继续申请借款-基本上不会执行到这里-如果执行到这里那么程序就是错的
				renderText("4_2");
				return;
			} else if (gather_state == 3) {// 筹款成功-需要判断还款状态
				// renderText("4_3");
				// return;
				int payment_state = borrower_bulk_standard_gather_money_order.getInt("payment_state"); // 还款状态
				if (payment_state == 1) {
					renderText("5_1");// 该状态值没有使用
					return;
				} else if (payment_state == 2) {
					setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, borrower_bulk_standard_apply_order.getLong("id") + "");
					renderText("5_2");// 有单子正在还款，不能借
					return;
				} else if (payment_state == 3) {
					renderText("5_3");
					return; // 还款完成，可以借款-基本上不会执行到这里-如果执行到这里那么程序就是错的
				} else {
					renderText("5_e");
					return;
				}
			} else {
				renderText("4_e");
				return;
			}
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录 有借款资格", function_description = "进入填写借款申请页面", last_update_author = "lh")
	public void loan_to_write_borrow_apply() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		int borrow_type = getParameterToInt("borrow_type", 0);
		if (!(borrow_type == 1 || borrow_type == 2 || borrow_type == 3)) {
			renderText("页面错误");
			return;
		}
		setAttribute("borrow_type", borrow_type + "");// 传递bid类型-生意贷和消费贷
		// 净值贷-可能在这之前没有借贷类型

		if (borrow_type == 1 || borrow_type == 2) {
			renderJsp("/user/financial/loan_write_borrow_apply.jsp");
			return;
		} else if (borrow_type == 3) { // 跳到净值贷申请填写页面

			UserNowMoneyM user_now_money = UserNowMoneyM.dao.findById(user_id);
			BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");// 可用余额
			BigDecimal to_collect_priciple_all = (BigDecimal) CommonRepayMethod.get_elite_bid_account(user_id).get("to_collect_pricipal");// 散标待回收本金
			BigDecimal total_bid_money = (BigDecimal) CommonRepayMethod.get_lfoll_fix_bid_account_info(user_id).get("total_bid_money");

			cny_can_used = cny_can_used.setScale(2, BigDecimal.ROUND_DOWN);
			to_collect_priciple_all = to_collect_priciple_all.setScale(2, BigDecimal.ROUND_DOWN);
			BigDecimal net_amount = cny_can_used.add(to_collect_priciple_all).add(total_bid_money);
			setAttribute("cny_can_used", cny_can_used);
			setAttribute("to_collect_priciple_all", to_collect_priciple_all);
			setAttribute("net_amount", net_amount);

			renderJsp("/user/financial/loan_write_borrow_apply_net.jsp");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(Ajax.class)
	@Function(for_people = "", function_description = "获取年化利率", last_update_author = "lh")
	public void get_monthly_principal_interst() {

		int borrow_all_money = getParameterToInt("borrow_all_money", 0);
		double annulized_rate_int = getParameterToDouble("annulized_rate_int", 0F);// 年化利率
		// *
		// 100
		double annulized_rate = annulized_rate_int / 100;// 年化利率
		String borrow_duration_str = ActionContext.getParameter("borrow_duration");// 3个月
		BigDecimal monthly_principal_interst = new BigDecimal("0");

		BigDecimal all_money = new BigDecimal(borrow_all_money + "");
		BigDecimal year_rates = new BigDecimal(annulized_rate + "");

		int borrow_duration = 0;
		int borrow_duration_day = 0;
		if (borrow_duration_str.contains("个月")) {
			borrow_duration = Integer.parseInt(borrow_duration_str.replace("个月", ""));
			monthly_principal_interst = CalculationFormula.get_principal_and_interest_by_year_rate(all_money, year_rates, borrow_duration);
		} else if (borrow_duration_str.contains("天")) {
			borrow_duration_day = Integer.parseInt(borrow_duration_str.replace("天", ""));
			monthly_principal_interst = CalculationFormula.get_principal_and_interest_by_year_rate_by_day(all_money, year_rates, borrow_duration_day);
		}

		monthly_principal_interst = monthly_principal_interst.setScale(2, BigDecimal.ROUND_DOWN);

		String str = String.valueOf(monthly_principal_interst);
		renderText(str);

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录 有借款资格", function_description = "提交借款申请单", last_update_author = "lh")
	public void loan_submit_borrow_money_apply_order() {

		final String borrow_title = ActionContext.getParameter("borrow_title"); // 借款标题
		final String borrow_purpose = ActionContext.getParameter("borrow_purpose"); // 借款用途
		final int borrow_all_money = getParameterToInt("borrow_all_money", 0); // 借款金额
		final String borrow_duration_string = ActionContext.getParameter("borrow_duration"); // 3个月
		final int annulized_rate = getParameterToInt("annulized_rate_int", 0);
		final String describe = getParameter("describe");// 借款描述
		// final int borrow_type = getParameterToInt("borrow_type"); //借款类型

		final int borrow_all_money_level;
		if ((borrow_all_money >= 3000) && (borrow_all_money <= 50000)) {
			borrow_all_money_level = 1;
		} else if (50000 < borrow_all_money && borrow_all_money <= 100000) {
			borrow_all_money_level = 2;
		} else if (100000 < borrow_all_money && borrow_all_money <= 200000) {
			borrow_all_money_level = 3;
		} else if (200000 < borrow_all_money && borrow_all_money <= 500000) {
			borrow_all_money_level = 4;
		} else {
			borrow_all_money_level = 0;
		}

		/**
		 * 依次对参数进行判断
		 */
		//
		if (Utils.isNullOrEmptyString(borrow_title)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		if (borrow_title.length() > 14) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		if (RiskParameterFilter.validateXss(borrow_title)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		//
		if (Utils.isNullOrEmptyString(borrow_purpose)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		if (RiskParameterFilter.validateXss(borrow_purpose)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		//
		if (borrow_all_money == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;

		}
		if (!((borrow_all_money >= 1000) && (borrow_all_money <= 500000) && (borrow_all_money % 50 == 0))) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}

		//
		if (Utils.isNullOrEmptyString(borrow_duration_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		if (!(("5天".equals(borrow_duration_string)) || //
				("10天".equals(borrow_duration_string)) || //
				("15天".equals(borrow_duration_string)) || //
				("20天".equals(borrow_duration_string)) || //
				("25天".equals(borrow_duration_string)) || //
				("1个月".equals(borrow_duration_string)) || //
				("2个月".equals(borrow_duration_string)) || //
				("3个月".equals(borrow_duration_string)) || //
				("4个月".equals(borrow_duration_string)) || //
				("5个月".equals(borrow_duration_string)) || //
				("6个月".equals(borrow_duration_string)) || //
				("7个月".equals(borrow_duration_string)) || //
				("8个月".equals(borrow_duration_string)) || //
				("9个月".equals(borrow_duration_string)) || //
				("10个月".equals(borrow_duration_string)) || //
				("11个月".equals(borrow_duration_string)) || //
				("12个月".equals(borrow_duration_string)) || //

				("3个月".equals(borrow_duration_string)) || //
				("6个月".equals(borrow_duration_string)) || //
				("9个月".equals(borrow_duration_string)) || //
				("12个月".equals(borrow_duration_string)) || //
				("15个月".equals(borrow_duration_string)) || //
				("18个月".equals(borrow_duration_string)) || //
				("21个月".equals(borrow_duration_string)) || //
		("24个月".equals(borrow_duration_string))//

		)) {

			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		//
		if (annulized_rate == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		if (!(annulized_rate >= 10 && annulized_rate <= 24)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}

		if (Utils.isNotNullAndNotEmptyString(describe)) {
			if (RiskParameterFilter.validateXss(describe)) {
				logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
				renderText("");// 非法操作
				return;
			}
		}

		final int borrow_all_share = borrow_all_money / CalculationFormula.Each_Share_Price; // 总份数

		final BigDecimal annulized_rate_in_db = new BigDecimal(annulized_rate + "").multiply(new BigDecimal("0.01"));// 年化利

		/**
		 * 保存数据库之前需要查询该用户是否具备提交新单子的资格
		 */
		UserM user = getSessionAttribute(SystemConstantKey.User);
		final int user_id = user.getInt("id");
		user = UserM.dao.findById(user_id);
		/**
		 * 判断该用户是否邮箱认证成功
		 */
		int is_active = user.getInt("is_active");
		if (is_active == -1) {// 还没有邮箱认证
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		if (is_active == 0) {// 该用户被禁用
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		String user_identity = user.getString("user_identity");
		if (Utils.isNullOrEmptyString(user_identity)) {// 没有进行实名认证
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		String phone = user.getString("phone");
		if (Utils.isNullOrEmptyString(phone)) {// 手机没有绑定
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}

		/**
		 * 上面是对用户基本信息的验证-下面是对借贷类型的验证
		 */
		/**
		 * 页面接受的borrow_type-user.borrow_type只能借一种 0 1 2
		 */
		final int borrow_type_from_page = getParameterToInt("borrow_type", -1);
		if (!(borrow_type_from_page == 1 || borrow_type_from_page == 2 || borrow_type_from_page == 3)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("0");// 不需要返回
			return;
		}
		int borrow_type = user.getInt("borrow_type");// 保存后这个需要修改-session and db
		if (borrow_type != 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("0");// 不需要返回
			return;
		}

		// OK可以提交新的单子(生意贷 ，消费贷，净值贷)-2_6[可以进行借贷采用这个Key-不变了]

		final AtomicLong apply_order_id = new AtomicLong(0);
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				int borrow_type_to_db = borrow_type_from_page;

				int borrow_duration = 0; // 借款期限
				int borrow_duration_day = 0;// 净值贷要使用的含天数的借款期限

				if (borrow_duration_string.contains("个月")) {
					borrow_duration = Integer.valueOf((borrow_duration_string.replace("个月", "")));
				} else if (borrow_duration_string.contains("天")) {
					borrow_duration_day = Integer.valueOf((borrow_duration_string.replace("天", "")));
				}

				BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = new BorrowerBulkStandardApplyOrderM();
				boolean is_ok_save_borrower_bulk_standard_apply_order = borrower_bulk_standard_apply_order.//
						set("user_id", user_id).//
						set("borrow_type", borrow_type_to_db).//
						set("bid_type", 1).//
						set("borrow_title", borrow_title).//
						set("borrow_purpose", borrow_purpose).//
						set("borrow_all_money", borrow_all_money).//
						set("borrow_all_money_level", borrow_all_money_level).//
						set("borrow_all_share", borrow_all_share).//
						set("borrow_duration", borrow_duration).//
						set("borrow_duration_day", borrow_duration_day).//
						set("annulized_rate", annulized_rate_in_db).//
						set("annulized_rate_int", annulized_rate).// 年化利率的100倍
						set("describe", describe).//
						set("repayment_way", 1).//
						set("initial_service_fee", new BigDecimal("0")).// initial_service_fee以后需要考虑的
						set("save_time", new Date()).//
						set("state", 1).//
						set("commit_time", null).//
						save();
				/**
				 * 如果借贷类型以前用户没有选择-那么在此刻需要进行修改
				 */
				UserM user = UserM.getUserM(user_id);
				boolean is_ok_update_user = user.set("borrow_type", borrow_type_to_db).update();

				if (borrow_type_to_db == 3) {
					UserNowMoneyM user_now_money = UserNowMoneyM.dao.findById(user_id);
					BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
					BigDecimal to_collect_priciple_all = (BigDecimal) CommonRepayMethod.get_elite_bid_account(user_id).get("to_collect_pricipal");// 待回收本金
					BigDecimal total_bid_money = (BigDecimal) CommonRepayMethod.get_lfoll_fix_bid_account_info(user_id).get("total_bid_money");
					BigDecimal net_amount = cny_can_used.add(to_collect_priciple_all).add(total_bid_money);

					boolean is_update_user_now_money_ok = user_now_money.set("net_amount", net_amount).set("total_net_cash", new BigDecimal("0")).update();
					if (!is_update_user_now_money_ok) {
						return false;
					}
				}
				/**
				 * 清空上传资料
				 */
				UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.dao.findById(user_id);
				boolean left_ok = user_authenticate_left_status.set("upload_info_status", 0).update();
				if (!left_ok) {
					return false;
				}
				UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);
				String att[] = { "id_authenticate", "work_authenticate", "credit_authenticate", "income_authenticate", "housing_authenticate", "car_authenticate", "marriage_authenticate", "education_authenticate", "title_authenticate", "living_authenticate" };
				boolean upload_ok = false;

				for (int i = 0; i < att.length; i++) {
					upload_ok = user_authenticate_upload_info.set(att[i] + "_url", null).set(att[i] + "_status", 0).set(att[i] + "_scores", 0).update();
				}
				if (!upload_ok) {
					return false;
				}

				// 保存一个借款单就+1
				UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findFirst("select * from user_credit_files where user_id = ?", new Object[] { user_id });
				int loan_application = user_credit_files.getInt("loan_application");
				user_credit_files.set("loan_application", loan_application + 1);

				boolean is_ok_user_credit = user_credit_files.update();
				/**
				 * 只要开始借贷就不能再进行自动开启投标
				 */
				UserAutomaticallyBidM user_automatically_bid = UserAutomaticallyBidM.dao.findFirst("SELECT * from user_automatically_bid WHERE user_id=?", new Object[] { user_id });
				if (user_automatically_bid != null) {
					int disjunctor = user_automatically_bid.getInt("disjunctor");
					if (disjunctor == 1) {
						boolean is_ok_update_user_automatically_bid = user_automatically_bid.set("disjunctor", 0).update();
						if (!is_ok_update_user_automatically_bid) {
							return false;
						}
					}
				}
				// 用户基本信息和信用额度信息-需要在管理员审核通过的时候
				if (is_ok_save_borrower_bulk_standard_apply_order && is_ok_update_user && is_ok_user_credit) {
					apply_order_id.set(borrower_bulk_standard_apply_order.getLong("id"));
					setSessionAttribute(SystemConstantKey.User, user);

					return true;
				} else {
					return false;
				}
			}
		});

		if (is_ok) {
			user.set("borrow_type", borrow_type_from_page);
			setSessionAttribute(SystemConstantKey.User, user);
			setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, apply_order_id.get() + "");
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录 有借款资格", function_description = "进入填写借款-上传-信息页面", last_update_author = "lh")
	public void to_write_borrow_info() {
		/**
		 * 在这里就有了借贷的类别了-用户信息
		 */
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int borrow_type = user.getInt("borrow_type");
		setAttribute("borrow_type", borrow_type + "");// 借贷类型

		/**
		 * 不用考虑单子信息-在保存的时候来进行单子信息验证
		 */
		renderJsp("/user/authenticate/upload.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "", function_description = "进入第四步 筹集借款", last_update_author = "lh")
	public void to_gather_money_page() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		int borrow_type = user.getInt("borrow_type");
		setAttribute("borrow_type", borrow_type + "");// 借贷类型

		/**
		 * 不用考虑单子信息-在保存的时候来进行单子信息验证
		 */

		/**
		 * 接收订单信息ID-只是进行显示-不进行修改
		 */
		// 判断是否有状态为 1 的订单
		String apply_order_id_string = BorrowAction.get_value_from_request_and_session_of_apply_order_id();
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		/**
		 * 查询该订单-该订单状态为6
		 */

		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findById(apply_order_id);
		if (borrower_bulk_standard_apply_order == null) {
			logger.error("该申请单查询失败:apply_order_id" + apply_order_id);
			renderText("该订单查询失败");
			return;
		}
		int user_id_of_apply_order = borrower_bulk_standard_apply_order.getInt("user_id");
		if (user_id != user_id_of_apply_order) {
			renderText("该订单对应用户..." + user_id + "..." + user_id_of_apply_order);
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (state != 6) {
			logger.error("该申请单申请状态错误:apply_order_id" + apply_order_id + "state:" + state);
			renderText("该申请单申请状态错误:apply_order_id" + apply_order_id + "state:" + state);
			return;
		}

		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT id,borrow_title,borrow_all_money,borrow_all_share,monthly_principal_and_interest,have_gather_money,have_gather_share,deadline from borrower_bulk_standard_gather_money_order WHERE apply_order_id=?", new Object[] { apply_order_id });// 查询后只有一条数据

		BigDecimal have_gather_money = borrower_bulk_standard_gather_money_order.getBigDecimal("have_gather_money");
		BigDecimal monthly_principal_and_interest = borrower_bulk_standard_gather_money_order.getBigDecimal("monthly_principal_and_interest");

		have_gather_money = have_gather_money.setScale(2, BigDecimal.ROUND_DOWN);
		monthly_principal_and_interest = monthly_principal_and_interest.setScale(2, BigDecimal.ROUND_DOWN);

		setAttribute("gather_money_order", borrower_bulk_standard_gather_money_order);
		setAttribute("have_gather_money", have_gather_money);
		setAttribute("monthly_principal_and_interest", monthly_principal_and_interest);

		renderJsp("/user/financial/loan_gather_money.jsp");
		return;
	}

	static BigDecimal $0_5 = new BigDecimal("0.5");
	static BigDecimal $0 = new BigDecimal("0");

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { UserLoginedAop.class })
	@Function(for_people = "", function_description = "进入第五步 借款成功", last_update_author = "lh")
	public void to_gather_success_page() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String sql = "select id,borrow_all_money,monthly_principal_and_interest,borrow_duration from borrower_bulk_standard_gather_money_order where gather_state = 3 and user_id = ? order by id desc limit 1";
		Record gather_order = Db.findFirst(sql, new Object[] { user_id });
		// 筹款单id
		long gather_order_id = gather_order.getLong("id");
		// 借款总额
		BigDecimal borrow_all_money = gather_order.getBigDecimal("borrow_all_money");
		BigDecimal manage_fee_by_hand = (borrow_all_money.multiply(new BigDecimal("0.003"))).setScale(2, BigDecimal.ROUND_DOWN);// 手动计算，不是从数据库查的
		// 月还本息
		BigDecimal monthly_principal_and_interest = gather_order.getBigDecimal("monthly_principal_and_interest");
		// 借款期限
		int borrow_duration = gather_order.getInt("borrow_duration");
		// 应还总额
		BigDecimal should_repay_total = monthly_principal_and_interest.multiply(new BigDecimal(borrow_duration + ""));

		String sql_repayment_plan = "select * from borrower_bulk_standard_repayment_plan where gather_money_order_id = ?";
		List<Record> borrower_bulk_standard_repayment_plan_list = Db.find(sql_repayment_plan, gather_order_id);

		BigDecimal pricipal_balance_all = borrow_all_money;

		for (Record borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			borrower_bulk_standard_repayment_plan.add("manage_fee_by_hand", manage_fee_by_hand);
			// 这里计算本金余额
			pricipal_balance_all = pricipal_balance_all.subtract(borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle"));
			BigDecimal pricipal_balance = pricipal_balance_all;
			if (pricipal_balance.compareTo($0_5) < 0) {
				pricipal_balance = $0;
			}

			borrower_bulk_standard_repayment_plan.add("pricipal_balance", pricipal_balance);

		}

		setAttribute("repayment_plan_list", borrower_bulk_standard_repayment_plan_list);

		borrow_all_money = borrow_all_money.setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("borrow_all_money", borrow_all_money);

		monthly_principal_and_interest = monthly_principal_and_interest.setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("monthly_principal_and_interest", monthly_principal_and_interest);

		should_repay_total = should_repay_total.setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("should_repay_total", should_repay_total);

		renderJsp("/user/financial/gather_success.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "电话预约", last_update_author = "zjb")
	public void submit_user_appointment() {
		String phone = getParameter("phone");
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			System.out.println(phone);
			renderText("err");
			return;
		}

		UserAppointmentM user_appointment = new UserAppointmentM();

		Pattern pat = Pattern.compile("^[1]\\d{10}$");
		Matcher mat = pat.matcher(phone);
		if (!mat.matches()) {
			renderText("err");
			return;
		}

		List<UserAppointmentM> user_appointment_list = UserAppointmentM.dao.find("select * from user_appointment where is_deal = 0 and phone = ? ", phone);
		// String phone_2 = user_appointment_phone.get("phone",null);
		// if(phone_2 != null){
		// renderText("wait");
		// return;
		// }
		if (Utils.isHasData(user_appointment_list)) {
			renderText("wait");
			return;
		}

		boolean ok = user_appointment.set("phone", phone).save();

		if (ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}
	}
}
