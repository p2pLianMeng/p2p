package com.tfoll.web.action.user.financial;

import com.lowagie.text.DocumentException;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.model.TemplateM;

import java.io.File;
import java.io.IOException;

@ActionKey("/user/financial/contractdownload")
public class ContractDownLoadAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "HTML 转为PDF 文档", last_update_author = "hx")
	public void dowon_PDF_by_templateId() throws DocumentException, IOException {
		int template_id = getParameterToInt("template_id", 0);
		TemplateM template = TemplateM.dao.findById(template_id);
		String template_path = template.getString("template_url");
		if (template == null) {
			renderText("非法请求");
			return;
		}

		String path = template.getString("template_url");

		String absolute_path = System.getProperty("user.dir");
		// D:\apache-tomcat-6.0.37\bin
		String absolute_path_2 = absolute_path.substring(0, absolute_path.length() - 4);
		// System.out.println("============absolute_path_2=========="+absolute_path_2);
		template.getString("template_url");
		String real_path = absolute_path_2 + "\\webapps\\ROOT" + template_path;
		// System.out.println("======real_path======"+real_path);

		File file = new File(real_path);

		if (file.exists()) {
			renderFile(file);
			return;
		} else {
			renderText("文件不存在!");
			return;
		}

	}

	public static void main(String[] args) {

		String s = "sdf\\a\\aa";
		// 把s中的反斜杠\ 替换为\\
		System.out.println(s);
		System.out.println(s.replaceAll("\\\\", "\\\\\\\\"));
		System.out.println(s.replace("\\", "\\\\"));

	}

}
