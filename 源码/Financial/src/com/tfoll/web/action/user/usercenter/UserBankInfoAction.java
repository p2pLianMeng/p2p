package com.tfoll.web.action.user.usercenter;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserBankInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ActionKey("/user/usercenter/bank_info")
public class UserBankInfoAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "跳转银行卡信息页面", last_update_author = "zjb")
	public void bank_info_index() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ?  ", new Object[] { user_id });
		String bank_card_num_one = user_bank_info.getString("bank_card_num_one");
		String bank_card_num_two = user_bank_info.getString("bank_card_num_two");
		String bank_card_num_three = user_bank_info.getString("bank_card_num_three");

		setAttribute("bank_card_num_one", bank_card_num_one);
		setAttribute("bank_card_num_two", bank_card_num_two);
		setAttribute("bank_card_num_three", bank_card_num_three);

		setAttribute("user_bank_info", user_bank_info);

		renderJsp("/user/usercenter/account_card_information.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "添加银行卡信息", last_update_author = "zjb")
	public void bank_info_add() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");
		String money_password = user.getString("money_password");
		String real_name = user.getString("real_name");

		if (!Utils.isNotNullAndNotEmptyString(money_password)) {
			renderText("no_money_password");
			return;
		}

		if (!Utils.isNotNullAndNotEmptyString(real_name)) {
			renderText("no_real_name");
			return;
		}
		String opening_bank = getParameter("opening_bank");
		String bank_name = getParameter("bank_name");
		String bank_address = getParameter("bank_address");
		String bank_card_num = getParameter("bank_card_num");
		String code = getParameter("code");

		String bank[] = { bank_name, bank_address, opening_bank, bank_card_num, code };
		String bank2[] = { "bank_name", "bank_address", "opening_bank", "bank_card_num" };
		for (int i = 0; i < bank.length; i++) {
			if (!Utils.isNotNullAndNotEmptyString(bank[i])) {
				renderText(i + "");
				return;
			}
		}

		// 验证输入信息长度（银行地址，开户行）
		Pattern pat = Pattern.compile("[\u4e00-\u9fa5_a-zA-Z0-9_]{2,50}");
		for (int j = 1; j < 3; j++) {
			Matcher mat = pat.matcher(bank[j]);
			System.out.println(bank2[j]);
			if (!mat.matches()) {
				renderText(j + "_mat");
				return;
			}
		}
		if (!money_password.equals(MD5.md5(code))) {
			renderText("err");
			return;
		}

		String sql_bank_card_num_count = "SELECT COUNT(1) FROM user_bank_info WHERE bank_card_num_one = ? OR bank_card_num_two = ? OR bank_card_num_three = ? ";
		long bank_card_num_count = Db.queryLong(sql_bank_card_num_count, new Object[] { bank_card_num, bank_card_num, bank_card_num });

		if (bank_card_num_count >= 1) {
			renderText("repeat");
			return;
		}

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ?  ", new Object[] { user_id });
		String bank_card_num_one = user_bank_info.get("bank_card_num_one");
		String bank_card_num_two = user_bank_info.get("bank_card_num_two");
		String bank_card_num_three = user_bank_info.get("bank_card_num_three");
		String num;

		if (!Utils.isNotNullAndNotEmptyString(bank_card_num_one)) {
			num = "_one";
		} else if (!Utils.isNotNullAndNotEmptyString(bank_card_num_two)) {
			num = "_two";
		} else if (!Utils.isNotNullAndNotEmptyString(bank_card_num_three)) {
			num = "_three";
		} else {
			renderText("over");
			return;
		}

		for (int i = 0; i < bank.length - 1; i++) {
			user_bank_info.set(bank2[i] + num, bank[i]);
		}
		boolean ok = user_bank_info.update();

		if (ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "修改银行卡信息", last_update_author = "zjb")
	public void bank_info_update() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		String money_password = user.getString("money_password");

		if (!Utils.isNotNullAndNotEmptyString(money_password)) {
			renderText("no_money_password");
			return;
		}
		String opening_bank = getParameter("opening_bank");// 开户行
		String bank_name = getParameter("bank_name");// 银行名
		String bank_address = getParameter("bank_address");// 银行地址
		String code = getParameter("code");// 提醒密码
		String num = getParameter("num");// 第几张银行卡

		String bank[] = { bank_name, bank_address, opening_bank, code };
		String bank2[] = { "bank_name", "bank_address", "opening_bank" };
		for (int i = 0; i < bank.length; i++) {
			if (!Utils.isNotNullAndNotEmptyString(bank[i])) {
				renderText(i + num);
				return;
			}
		}
		// 验证输入信息长度（银行地址，开户行）
		Pattern pat = Pattern.compile("[\u4e00-\u9fa5_a-zA-Z0-9_]{2,50}");
		for (int j = 1; j < 3; j++) {
			Matcher mat = pat.matcher(bank[j]);
			System.out.println(bank2[j]);
			if (!mat.matches()) {
				renderText(j + num + "_mat");
				return;
			}
		}

		if (!money_password.equals(MD5.md5(code))) {
			renderText("err");
			return;
		}

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ? ", new Object[] { user_id });

		for (int i = 0; i < bank.length - 1; i++) {
			user_bank_info.set(bank2[i] + num, bank[i]);
		}
		boolean ok = user_bank_info.update();

		if (ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "删除银行卡信息", last_update_author = "zjb")
	public void bank_info_delete() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		String money_password = user.getString("money_password");

		if (!Utils.isNotNullAndNotEmptyString(money_password)) {
			renderText("no_money_password");
			return;
		}

		String num = getParameter("num");

		String bank_string[] = { "bank_name", "bank_address", "opening_bank", "bank_card_num" };

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ? ", new Object[] { user_id });

		for (int i = 0; i < bank_string.length; i++) {
			user_bank_info.set(bank_string[i] + num, null);
		}
		boolean ok = user_bank_info.update();

		if (ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

}
