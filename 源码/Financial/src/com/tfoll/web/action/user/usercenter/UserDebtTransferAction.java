package com.tfoll.web.action.user.usercenter;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.CommonRepayMethod;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ActionKey("/user/user_center/user_debt_transfer")
public class UserDebtTransferAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "查询可转让的债权", last_update_author = "hx")
	public void get_user_can_transfered_debt() {
		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");
		/**
		 * 可以进行债权转让的前提条件是借款三个月之后且没有严重逾期且持有不为0
		 */
		String select_creditor_right_hold_sql = "SELECT t1.*, t2.borrow_type AS borrow_type, t2.borrow_all_share AS borrow_all_share FROM lender_bulk_standard_creditor_right_hold t1, borrower_bulk_standard_gather_money_order t2 WHERE t1.user_id = ? AND t1.gather_money_order_id = t2.id AND t2.finish_time <= DATE_SUB(NOW(), INTERVAL 3 MONTH) AND t1.hold_share != 0 AND t2.pay_for_object = 0 ORDER BY t1.id DESC";
		// 债权持有集合
		List<Record> creditor_right_hold_list = Db.find(select_creditor_right_hold_sql, new Object[] { user_id });
		// 要重新封装的可转出的债权集合
		List<Map<String, Object>> can_transfer_debt_list = new ArrayList<Map<String, Object>>();
		if (Utils.isHasData(creditor_right_hold_list)) {
			for (Record record : creditor_right_hold_list) {
				Map<String, Object> can_transfer_debt = new HashMap<String, Object>();
				// 筹集单ID 作为页面债权ID
				Long gather_money_order_id = record.getLong("gather_money_order_id");// 筹集单ID
				// 作为页面债权ID
				String annulized_rate_int_sql = "SELECT t.annulized_rate_int AS annulized_rate_int FROM borrower_bulk_standard_gather_money_order t WHERE t.id = ?";
				// 年化利率 * 100
				int annulized_rate_int = Db.queryInt(annulized_rate_int_sql, new Object[] { gather_money_order_id });
				// 获取总的债权价值
				// BigDecimal debt_value =
				// CalculationFormula.get_debt_value(gather_money_order_id).setScale(2,
				// BigDecimal.ROUND_DOWN);//总的价值
				// 获取每份的债权价值（每份债权价值会改变）
				BigDecimal debt_value_per = CalculationFormula.get_debt_value_per_share(gather_money_order_id).setScale(2, BigDecimal.ROUND_DOWN);

				int gather_money_order_user_id_borrower = record.getInt("gather_money_order_user_id_borrower");// 借款用户ID
				String select_repayment_plan_sql = "SELECT t.total_periods AS total_periods, t.current_period AS current_period, t.automatic_repayment_date AS automatic_repayment_date FROM borrower_bulk_standard_repayment_plan t WHERE t.borrower_user_id = ? AND t.is_repay = 0 LIMIT 1";
				Record standard_repayment_plan_record = Db.findFirst(select_repayment_plan_sql, new Object[] { gather_money_order_user_id_borrower });//
				int total_periods = 0;// 总期数
				int current_period = 0;// 当前期数
				String next_payment_date = "";// 下一个自动还款期数
				if (standard_repayment_plan_record != null) {
					total_periods = standard_repayment_plan_record.getInt("total_periods");
					current_period = standard_repayment_plan_record.getInt("current_period");
					next_payment_date = standard_repayment_plan_record.getString("automatic_repayment_date");
				}
				int remain_periods_int = (total_periods + 1) - current_period;// 剩余期数
				// 借入者还款信息
				Map<String, Object> borrower_repayment_info_map = CommonRepayMethod.get_repayment_info(gather_money_order_id);
				// 借入者每份未还本息
				BigDecimal weihuan_benxi_total_per_share = (BigDecimal) borrower_repayment_info_map.get("weihuan_benxi_total_per_share");

				// //应还的本金和利息
				// BigDecimal should_repayment_total =
				// standard_repayment_plan_record.getBigDecimal("should_repayment_total").setScale(2,
				// BigDecimal.ROUND_DOWN);
				// 持有份额
				int hold_share = record.getInt("hold_share");
				int origin_money = hold_share * 50;// 原始投资金额

				// 转出份额-冻结
				int transfer_share = record.getInt("transfer_share");
				// 可转出的份额
				int can_transfer_share = hold_share - transfer_share;
				// 当前拥有的债权价值
				BigDecimal debt_value = debt_value_per.multiply(new BigDecimal(can_transfer_share + ""));// 这个是剩下的价值
				// 借出者剩余总共待还本息
				BigDecimal should_repayment_total = new BigDecimal(can_transfer_share + "").multiply(weihuan_benxi_total_per_share).setScale(2, BigDecimal.ROUND_DOWN);
				// 查询borrow_type 借款类型
				int borrow_type = record.getInt("borrow_type");

				int borrow_all_share = record.getInt("borrow_all_share");// 借款所有的份额
				String money_sql = "SELECT SUM(should_repayment_total) AS sum_should_repayment_total, SUM(should_repayment_principle) AS sum_should_repayment_principle, SUM(should_repayment_interest) AS sum_should_repayment_interest FROM borrower_bulk_standard_repayment_plan WHERE is_repay = 0 AND gather_money_order_id = ?";
				Record money_record = Db.findFirst(money_sql, new Object[] { gather_money_order_id });

				BigDecimal sum_should_repayment_total = money_record.getBigDecimal("sum_should_repayment_total");// 待还总额
				BigDecimal sum_should_repayment_principle = money_record.getBigDecimal("sum_should_repayment_principle");// 待还本金
				BigDecimal sum_should_repayment_interest = money_record.getBigDecimal("sum_should_repayment_interest");// 待还利息
				// 每份的 待收本息
				BigDecimal daishou_benxi_per_share = sum_should_repayment_total.divide(new BigDecimal(borrow_all_share), 2, BigDecimal.ROUND_DOWN);

				// 债权持有ID
				Long creditor_right_hold_id = record.getLong("id");
				can_transfer_debt.put("borrow_type", borrow_type);// 1消费贷2生意贷3净值贷，默认为0
				can_transfer_debt.put("gather_money_order_id", gather_money_order_id);// 债权ID,即筹集单ID
				can_transfer_debt.put("remain_periods", remain_periods_int + "/" + total_periods);// 剩余期数
				can_transfer_debt.put("remain_periods_int", remain_periods_int);
				can_transfer_debt.put("next_payment_date", next_payment_date);// 下个还款日
				can_transfer_debt.put("annulized_rate", annulized_rate_int + "%");// 年利率
				can_transfer_debt.put("should_repayment_total", should_repayment_total + "");// 待收本息
				can_transfer_debt.put("debt_value", debt_value);// 当前拥有的债权总价值
				can_transfer_debt.put("can_transfer_share", can_transfer_share);// 可转份额
				can_transfer_debt.put("origin_money", origin_money);// 原始投资金额
				can_transfer_debt.put("hold_share", hold_share);// 持有份额
				can_transfer_debt.put("annulized_rate_int", annulized_rate_int);// 年化利率
				// int
				can_transfer_debt.put("daishou_benxi_per_share", daishou_benxi_per_share);// 待收本息每份的本息
				can_transfer_debt.put("debt_value_per", debt_value_per);// 债权转让每份的价值
				can_transfer_debt.put("creditor_right_hold_id", creditor_right_hold_id);// 债权持有ID
				// lender_bulk_standard_creditor_right_hold表的ID

				// 添加入集合
				can_transfer_debt_list.add(can_transfer_debt);
			}
			Gson gson = new Gson();
			renderText(gson.toJson(can_transfer_debt_list));
			return;
		} else {
			renderText("");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "查询正在转让中的债权", last_update_author = "hx")
	public void get_user_transfering_debt() {
		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");
		String select_transfering_debt_sql = "select * from lender_bulk_standard_order_creditor_right_transfer_out t where t.creditor_right_hold_user_id = ? and t.state = 1 order by t.id desc";
		List<Record> record_list = Db.find(select_transfering_debt_sql, new Object[] { user_id });

		if (Utils.isHasData(record_list)) {
			List<Map<String, Object>> map_list = new ArrayList<Map<String, Object>>();
			for (Record record : record_list) {
				Long id = record.getLong("id");// 转让ID
				Long gather_money_order_id = record.getLong("gather_money_order_id");// 借款方凑集单ID(债权ID)
				// 查询当前剩余期数
				String select_repayment_plan_sql = "SELECT t.total_periods AS total_periods, t.current_period AS current_period, t.automatic_repayment_date AS automatic_repayment_date FROM borrower_bulk_standard_repayment_plan t WHERE t.gather_money_order_id = ? AND t.is_repay = 0 LIMIT 1";
				Record standard_repayment_plan_record = Db.findFirst(select_repayment_plan_sql, new Object[] { gather_money_order_id });//
				int current_period = standard_repayment_plan_record.getInt("current_period");
				int total_periods = standard_repayment_plan_record.getInt("total_periods");
				int remain_periods = (total_periods - current_period) + 1;// 剩余期数
				// 查询年利率
				String annulized_rate_int_sql = "select t.annulized_rate_int as annulized_rate_int,t.borrow_type as borrow_type from borrower_bulk_standard_gather_money_order t where t.id = ?";
				Record annulized_rate_int_record = Db.findFirst(annulized_rate_int_sql, new Object[] { gather_money_order_id });
				// 年化利率
				int annulized_rate_int = annulized_rate_int_record.getInt("annulized_rate_int");
				// 借款类型 1消费贷，2 生意贷，3 净值贷
				int borrow_type = annulized_rate_int_record.getInt("borrow_type");
				// 当前债权价值
				BigDecimal current_creditor_right_value = record.getBigDecimal("creditor_right_value").setScale(2, BigDecimal.ROUND_DOWN);
				// 转让价格
				BigDecimal transfer_price = record.getBigDecimal("transfer_price").setScale(2, BigDecimal.ROUND_DOWN);// 转让价格（元/份）
				// 转让系数
				BigDecimal transfer_factor = record.getBigDecimal("transfer_factor").multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_DOWN);
				// 要转让的份数
				int transfer_share = record.getInt("transfer_share");
				// 剩下没有转让的份数
				int transfer_share_remainder = record.getInt("transfer_share_remainder");

				//
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", id);// 转让ID
				map.put("borrow_type", borrow_type);// 借款类型
				map.put("gather_money_order_id", gather_money_order_id);// 债权ID(筹集单ID)
				map.put("remain_periods", remain_periods + "/" + total_periods);// 剩余期数
				map.put("annulized_rate_int", annulized_rate_int + "%");// 年化利率
				map.put("current_creditor_right_value", current_creditor_right_value);// 当前债权价值
				map.put("transfer_price", transfer_price);// 转让价格
				map.put("transfer_factor", transfer_factor + "%");// 转让系数
				map.put("remain_share", transfer_share_remainder + "/" + transfer_share);// 剩余份数
				map_list.add(map);
			}
			Gson gson = new Gson();
			renderText(gson.toJson(map_list));
			return;
		} else {
			renderText("");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "查询已经转出的债权", last_update_author = "hx")
	public void get_already_transfered_out_debt() {
		final UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		// String creditor_right_transfer_in_sql =
		// "SELECT t1.id AS id, t1.transfer_out_order_id AS transfer_out_order_id, t1.transfer_out_user_id AS transfer_out_user_id, t1.transfer_share AS total_transfer_share, t1.transfer_debt_value AS total_transfer_debt_value, t1.transfer_all_money AS transfer_all_money, t1.transfer_fee AS transfer_fee FROM lender_bulk_standard_order_creditor_right_transfer_in t1 WHERE t1.transfer_out_user_id = ? GROUP BY t1.transfer_out_order_id ORDER BY t1.id ASC";
		String creditor_right_transfer_in_sql = "SELECT t1.id AS id, t1.transfer_out_order_id AS transfer_out_order_id, t1.transfer_out_user_id AS transfer_out_user_id, SUM(t1.transfer_share) AS total_transfer_share, SUM( t1.transfer_debt_value * t1.transfer_share ) AS total_transfer_debt_value, SUM(t1.transfer_all_money) AS transfer_all_money, SUM(t1.transfer_fee) AS transfer_fee FROM lender_bulk_standard_order_creditor_right_transfer_in t1 WHERE t1.transfer_out_user_id = ? GROUP BY t1.gather_money_order_id ORDER BY t1.id ASC";
		List<Record> creditor_right_transfer_in_list = Db.find(creditor_right_transfer_in_sql, new Object[] { user_id });
		if (Utils.isHasData(creditor_right_transfer_in_list)) {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (Record record : creditor_right_transfer_in_list) {
				Record record_new = new Record();
				// 转出订单表id
				Long transfer_out_order_id = record.getLong("transfer_out_order_id");// 转出订单表id
				String creditor_right_transfer_out_sql = "select gather_money_order_id from lender_bulk_standard_order_creditor_right_transfer_out t where t.id = ?";
				Record creditor_right_transfer_out_record = Db.findFirst(creditor_right_transfer_out_sql, new Object[] { transfer_out_order_id });
				Long gather_money_order_id = creditor_right_transfer_out_record.getLong("gather_money_order_id");// 借款方凑集单ID
				String gather_money_order_sql = "select borrow_type from borrower_bulk_standard_gather_money_order t where t.id = ?";
				Record gather_money_order_record = Db.findFirst(gather_money_order_sql, new Object[] { gather_money_order_id });
				int borrow_type = gather_money_order_record.getInt("borrow_type");// 借款类型
				// 转出订单表id
				record_new.add("transfer_out_order_id", transfer_out_order_id);
				// 借款类型
				record_new.add("borrow_type", borrow_type);
				// 债权ID(筹集单ID)
				record_new.add("gather_money_order_id", gather_money_order_id + "");
				// 成交份数
				record_new.add("total_transfer_share", record.getBigDecimal("total_transfer_share"));
				// 转出时债权总价值
				record_new.add("total_transfer_debt_value", record.getBigDecimal("total_transfer_debt_value").setScale(2, BigDecimal.ROUND_DOWN));
				// 转出时总成交金额
				BigDecimal transfer_all = record.getBigDecimal("transfer_all_money").setScale(2, BigDecimal.ROUND_DOWN);
				record_new.add("total_transfer_price", transfer_all);// 转出时总成交金额
				// 交易费用
				BigDecimal transfer_fee = record.getBigDecimal("transfer_fee").setScale(2, BigDecimal.ROUND_DOWN);
				record_new.add("transfer_fee", transfer_fee);// 交易费用 = 转出时总成交金额
				// * 0.5%
				// 实际收入
				BigDecimal actual_income_price = transfer_all.subtract(transfer_fee).setScale(2, BigDecimal.ROUND_DOWN);
				record_new.add("actual_income_price", actual_income_price + "");
				// 转让盈亏
				record_new.add("transfer_profit_loss", transfer_fee + "");

				// 明细集合
				List<Map<String, Object>> out_detail_list = get_debt_transfer_out_detail(transfer_out_order_id);
				System.out.println("----------out_detail_list---------" + out_detail_list.size());
				record_new.add("out_detail_list", out_detail_list);
				list.add(record_new.getR());
			}
			Gson gson = new Gson();
			renderText(gson.toJson(list));
			return;
		} else {
			renderText("");
			return;
		}
	}

	private List<Map<String, Object>> get_debt_transfer_out_detail(long transfer_out_order_id) {
		/*
		 * long transfer_out_order_id =
		 * getParameterToLong("transfer_out_order_id",0); if
		 * (transfer_out_order_id==0) { renderText(""); return; }
		 */
		String select_ransfered_out_detail_sql = "select t1.*,t2.nickname from lender_bulk_standard_order_creditor_right_transfer_in t1,user_info t2 where t1.transfer_out_order_id = ? and t2.id = t1.transfer_in_user_id  order by t1.id asc";
		List<Record> record_list = Db.find(select_ransfered_out_detail_sql, new Object[] { transfer_out_order_id });
		if (Utils.isHasData(record_list)) {
			List<Map<String, Object>> map_record_list = new ArrayList<Map<String, Object>>();
			for (Record record : record_list) {
				Record r = new Record();
				// 昵称
				String nickname = record.getString("nickname");
				// 每份债权价值
				BigDecimal transfer_debt_value = record.getBigDecimal("transfer_debt_value").setScale(2, BigDecimal.ROUND_DOWN);
				// 转入价格（元/份）
				BigDecimal transfer_price = record.getBigDecimal("transfer_price");// 转入价格
				// 转让份额
				int transfer_share_int = record.getInt("transfer_share");
				BigDecimal transfer_share = new BigDecimal(transfer_share_int + "");
				// 管理费
				BigDecimal transfer_fee = record.getBigDecimal("transfer_fee").setScale(2, BigDecimal.ROUND_DOWN);//
				// 实际收入 = 转入价格（元/份） * 转让份额 - 管理费
				BigDecimal real_income = record.getBigDecimal("transfer_all_money").subtract(transfer_fee).setScale(2, BigDecimal.ROUND_DOWN);
				// 盈亏
				BigDecimal profit_loss = transfer_fee;
				// 成交时间
				Date add_time = record.getTimestamp("add_time");
				String add_time_str = Model.Date.format(add_time);
				r.add("nickname", nickname);
				r.add("transfer_debt_value", transfer_debt_value);
				r.add("transfer_price", transfer_price);
				r.add("transfer_share", transfer_share);
				r.add("transfer_fee", transfer_fee);
				r.add("real_income", real_income);
				r.add("profit_loss", profit_loss);
				r.add("add_time_str", add_time_str);
				map_record_list.add(r.getR());
			}
			// Gson gson = new Gson();
			// renderText(gson.toJson(map_record_list));
			return map_record_list;
		} else {
			// renderText("");
			return null;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "查询已经转让的债权的明细", last_update_author = "hx")
	public void get_already_transfered_in_debt() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		String creditor_right_transfer_in_sql = "SELECT * FROM lender_bulk_standard_order_creditor_right_transfer_in T WHERE T.transfer_in_user_id = ? ORDER BY T.add_time DESC";
		List<Record> creditor_right_transfer_in_list = Db.find(creditor_right_transfer_in_sql, new Object[] { user_id });
		if (Utils.isHasData(creditor_right_transfer_in_list)) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			List<Map<String, Object>> map_list = new ArrayList<Map<String, Object>>();
			for (Record record : creditor_right_transfer_in_list) {
				// 转出订单表id
				Long transfer_out_order_id = record.getLong("transfer_out_order_id");
				// 筹集单ID
				Long gather_money_order_id = record.getLong("gather_money_order_id");
				//
				String gather_money_order_sql = "SELECT T.borrow_type AS borrow_type,T.annulized_rate_int AS annulized_rate_int  FROM borrower_bulk_standard_gather_money_order T WHERE T.id = ?";
				// 借款类型和年化利率
				Record borrow_type_rate_record = Db.findFirst(gather_money_order_sql, new Object[] { gather_money_order_id });
				// 借款类型
				int borrow_type = borrow_type_rate_record.getInt("borrow_type");
				// 年化利率
				int annulized_rate_int = borrow_type_rate_record.getInt("annulized_rate_int");
				// 查询还款计划表的剩余月份数
				String select_repayment_plan_sql = "SELECT t.total_periods AS total_periods, t.current_period AS current_period FROM borrower_bulk_standard_repayment_plan t WHERE t.gather_money_order_id = ? AND t.is_repay = 0 AND t.repay_actual_time IS NULL AND t.repay_actual_time_long = 0 LIMIT 1";
				Record repayment_plan_record = Db.findFirst(select_repayment_plan_sql, new Object[] { gather_money_order_id });
				// 还款总期数
				int total_periods = repayment_plan_record.getInt("total_periods");
				// 当期期数
				int current_period = repayment_plan_record.getInt("current_period");
				// 剩余期数
				int remain_period = (total_periods - current_period) + 1;
				// transfer_debt_value转入时的债权价值
				BigDecimal transfer_debt_value = record.getBigDecimal("transfer_debt_value").setScale(2, BigDecimal.ROUND_DOWN);// 每份价值
				// 转入份额
				int transfer_share = record.getInt("transfer_share");
				// 转入价格(元/份)
				BigDecimal transfer_price = record.getBigDecimal("transfer_price");
				// 交易金额
				BigDecimal transction_amount = transfer_price.multiply(new BigDecimal(transfer_share + "")).setScale(2, BigDecimal.ROUND_DOWN);
				// 应计利率（盈亏）
				BigDecimal should_count_interest = CalculationFormula.get_should_count_interest(gather_money_order_id).setScale(2, BigDecimal.ROUND_DOWN);
				// 债权转入时间
				Date add_time = record.getTimestamp("add_time");
				String add_time_str = format.format(add_time);

				Record new_record = new Record();
				new_record.add("borrow_type", borrow_type);// 借款类型
				new_record.add("gather_money_order_id", gather_money_order_id);// 筹集单ID
				new_record.add("remain_period", remain_period);// 剩余期数
				new_record.add("annulized_rate_int", annulized_rate_int);// 年化利率
				new_record.add("transfer_debt_value", transfer_debt_value);// 转入时债权价值
				new_record.add("transfer_share", transfer_share);// 转入份额
				new_record.add("transction_amount", transction_amount);// 交易金额
				new_record.add("should_count_interest", should_count_interest);// 盈亏
				new_record.add("add_time_str", add_time_str);// 转入时间
				map_list.add(new_record.getR());
			}
			Gson gson = new Gson();
			renderText(gson.toJson(map_list));
			return;
		} else {
			renderText("");
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "正在转让中的债权--撤单", last_update_author = "hx")
	public void user_cancel_transfering_debt() {
		// 客户端参数：债权转出表id
		final Long id = getParameterToLong("id", 0L);
		// 客户端参数：筹集单id
		final Long gather_money_order_id = getParameterToLong("gather_money_order_id", 0L);
		if (id == null || id == 0l) {
			renderText("1");// 客户端参数：债权转出表id不正确
			return;
		}
		if (gather_money_order_id == null || gather_money_order_id == 0l) {
			renderText("2");// 客户端参数：筹集单id
			return;
		}
		/**
		 * 债权持有人（当前登陆用户）
		 */
		final UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");

		// 事务处理
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				String transfer_out_sql = "SELECT * FROM lender_bulk_standard_order_creditor_right_transfer_out t WHERE t.id = ? AND t.gather_money_order_id = ? AND t.creditor_right_hold_user_id = ? for update";
				LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out = LenderBulkStandardOrderCreditorRightTransferOutM.dao.findFirst(transfer_out_sql, new Object[] { id, gather_money_order_id, user_id });
				// 债券持有表id
				Long creditor_right_hold_id = lender_bulk_standard_order_creditor_right_transfer_out.getLong("creditor_right_hold_id");
				// 剩下没有转让的份数
				int transfer_share_remainder = lender_bulk_standard_order_creditor_right_transfer_out.getInt("transfer_share_remainder");

				/**
				 * 更新操作
				 */
				boolean is_upadte = lender_bulk_standard_order_creditor_right_transfer_out.set("state", 3).update();
				/**
				 * 查询债权持有记录
				 */
				String creditor_right_hold_sql = "select * from lender_bulk_standard_creditor_right_hold t where t.user_id = ? and t.gather_money_order_id = ? and t.id = ? for update";
				LenderBulkStandardCreditorRightHoldM creditor_right_hold = LenderBulkStandardCreditorRightHoldM.dao.findFirst(creditor_right_hold_sql, new Object[] { user_id, gather_money_order_id, creditor_right_hold_id });
				// BigDecimal hold_money =
				// creditor_right_hold.getBigDecimal("hold_money");//持有金额
				// int hold_share =
				// creditor_right_hold.getInt("hold_share");//持有份额

				BigDecimal transfer_money = creditor_right_hold.getBigDecimal("transfer_money");// 转出金额-冻结
				int transfer_share = creditor_right_hold.getInt("transfer_share");// 转出份额-冻结

				/**
				 * 撤单后，没有转出的债权要，归还到用户持有债权表中
				 */
				// 同一份筹集单下的债权，在转让的份数 - 撤单的份数
				int hold_share_new = transfer_share - transfer_share_remainder;
				BigDecimal transfer_money_new = transfer_money.subtract(new BigDecimal(transfer_share_remainder * 50)).setScale(2, BigDecimal.ROUND_DOWN);
				/**
				 * 数据对比transfer_money_new
				 */
				int transfer_money_new_int = transfer_money_new.intValue();
				if (hold_share_new * 50 != transfer_money_new_int) {
					throw new RuntimeException("资金和份数对不上");
				}

				/**
				 * 更改债权持有操作
				 */
				boolean is_update_creditor_right_hold_ok = creditor_right_hold.set("transfer_share", hold_share_new).set("transfer_money", transfer_money_new).update();

				if (is_upadte && is_update_creditor_right_hold_ok) {
					return true;
				} else {
					return false;
				}
			}
		});

		if (is_ok) {
			renderText("3");// 撤单成功!
			return;
		} else {
			renderText("4");// 撤单失败
			return;
		}

	}

}
