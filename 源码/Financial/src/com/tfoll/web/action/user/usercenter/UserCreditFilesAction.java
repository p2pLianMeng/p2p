package com.tfoll.web.action.user.usercenter;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserM;

@ActionKey("/user/usercenter")
public class UserCreditFilesAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { UserLoginedAop.class })
	@Function(for_people = "登录", function_description = "跳转信用认证信息页面", last_update_author = "zjb")
	public void user_credit_files() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");
		UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);

		int sum_scores = UserCreditFilesM.get_sum_scores(user_id);
		int upload_sum_scores = UserAuthenticateUploadInfoM.get_upload_sum_scores(user_id);
		String credit_rating = UserCreditFilesM.get_credit_rating(user_id);

		setAttribute("upload_sum_scores", upload_sum_scores);
		setAttribute("sum_scores", sum_scores);
		setAttribute("credit_rating", credit_rating);
		setAttribute("user_credit_files", user_credit_files);
		setAttribute("user_authenticate_upload_info", user_authenticate_upload_info);

		renderJsp("/user/usercenter/user_credit_files.jsp");
		return;

	}
}
