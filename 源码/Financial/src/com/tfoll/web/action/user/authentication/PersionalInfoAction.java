package com.tfoll.web.action.user.authentication;

import com.tfoll.tcache.action.Utils;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.action.user.financial.BorrowAction;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConfig;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserAuthenticatePersionalInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.WebLogRecordsUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ActionKey("/user/authentication/persional_info")
public class PersionalInfoAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "跳转到个人信息认证页面", last_update_author = "zjb")
	public void persional_index() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		int borrow_type = user.getInt("borrow_type");
		int rate = UserAuthenticateLeftStatusM.get_rate(user_id);

		setAttribute("rate", rate);
		setAttribute("the_id", "persional");
		setAttribute("borrow_type", borrow_type + "");

		// 判断是否有状态为 1 的订单
		String apply_order_id_string = BorrowAction.get_value_from_request_and_session_of_apply_order_id();
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}

		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		// 单子状态和关联保存的申请单ID
		setAttribute("apply_order_id", apply_order_id);
		setAttribute("apply_order_state", state);// 如果是2则要提示在多久后系统会自动提交这个单子

		UserAuthenticatePersionalInfoM user_authenticate_persional_info = UserAuthenticatePersionalInfoM.dao.findFirst("select * from user_authenticate_persional_info where user_id = ? ", new Object[] { user_id });
		setAttribute("user_authenticate_persional_info", user_authenticate_persional_info);
		setAttribute("persional_province_list", SystemConfig.Sys_Province_List);

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		setAttribute("user_authenticate_left_status", user_authenticate_left_status);

		renderJsp("/user/authenticate/persional.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "个人信息认证", last_update_author = "zjb")
	public void save_persional_info() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		// 判断是否有状态为 1 的订单
		long apply_order_id = getParameterToInt("apply_order_id", 0);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		long apply_order_id_2 = borrower_bulk_standard_apply_order.getLong("id");
		if (apply_order_id_2 != apply_order_id) {
			logger.error("bug apply_order_id_2:" + apply_order_id_2 + " != apply_order_id:" + apply_order_id);
			renderText("");
			return;
		}

		int state = borrower_bulk_standard_apply_order.getInt("state");
		if (!(state == 1)) {
			logger.error("bug apply_order_id: !(state == 1 )");
			renderText("");
			return;
		}

		String highest_education = getParameter("highest_education");
		String native_place_province = getParameter("native_place_province");
		String native_place_city = getParameter("native_place_city");

		String domicile_place_province = getParameter("domicile_place_province");
		String domicile_place_city = getParameter("domicile_place_city");
		String address = getParameter("address");

		String zip_code = getParameter("zip_code");
		String landline = getParameter("landline");
		String school = getParameter("school");

		String year_of_admission = getParameter("year_of_admission");
		String arr[] = { highest_education, native_place_province, native_place_city, domicile_place_province, domicile_place_city, address, zip_code, year_of_admission, school, landline };
		String arr2[] = { "highest_education", "native_place_province", "native_place_city", "domicile_place_province", "domicile_place_city", "address", "zip_code", "year_of_admission", "school", "landline" };

		for (int i = 0; i < arr.length - 3; i++) {
			if (!Utils.isNotNullAndNotEmptyString(arr[i])) {
				renderText(String.valueOf(i));
				return;
			}
		}

		// 居住验证
		String addr = "^(?=.*?[\\u4E00-\\u9FA5])[\\dA-Za-z\\u4E00-\\u9FA5]+$";
		Pattern pat = Pattern.compile(addr);
		Matcher mat = pat.matcher(address);
		// boolean rs = mat.find();
		if (!mat.matches()) {
			renderText(5 + "1");
			return;
		}

		// 邮政编码验证
		String zc = "^[1-9]\\d{5}(?!\\d)$";
		pat = Pattern.compile(zc);
		mat = pat.matcher(zip_code);
		// boolean rs = mat.find();
		if (!mat.matches()) {
			renderText(6 + "1");
			return;
		}

		// 验证毕业学校长度
		if (Utils.isNotNullAndNotEmptyString(school)) {
			pat = Pattern.compile("^.{4,15}$");
			mat = pat.matcher(school);
			if (!mat.matches()) {
				renderText("school2");
				return;
			}
		}
		// 居住电话验证
		// String tel = "^\\d{12}$";
		// pat = Pattern.compile(tel);
		// mat = pat.matcher(landline);
		// // boolean rs = mat.find();
		// if (!mat.find()) {
		// renderText(7 + "1");
		// return;
		// }

		UserAuthenticatePersionalInfoM user_authenticate_persional_info = UserAuthenticatePersionalInfoM.dao.findFirst("select * from user_authenticate_persional_info where user_id = ? ", user_id);
		for (int i = 0; i < arr.length; i++) {
			user_authenticate_persional_info.set(arr2[i], arr[i]);
		}

		boolean ok = user_authenticate_persional_info.update();

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.get_user_authenticate_left_status(user_id);
		int persional_info_status = 1;
		boolean left_status_ok = user_authenticate_left_status.set("persional_info_status", persional_info_status).update();

		if (ok && left_status_ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}
}
