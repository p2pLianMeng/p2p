package com.tfoll.web.action.user;

import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;

/**
 * com.tfoll.web.action.user.authentication包下面是修改和填写该用户借款认证的一些操作
 * 
 */
@ActionKey("/user/authentication")
public class AuthenticationAction extends Controller {

}
