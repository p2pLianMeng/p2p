package com.tfoll.web.action.user.authentication;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateAssetsInfoM;
import com.tfoll.web.model.UserAuthenticateFamilyInfoM;
import com.tfoll.web.model.UserAuthenticatePersionalInfoM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserAuthenticateWorkInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.CalculationFormula;

import java.math.BigDecimal;

@ActionKey("/user/authenticate/authenticate_info_preview")
public class AuthenticateInfoPreviewAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "跳转到预览页面", last_update_author = "zjb")
	public void preview_index() throws Exception {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
		UserAuthenticateAssetsInfoM user_authenticate_assets_info = UserAuthenticateAssetsInfoM.dao.findById(user_id);
		UserAuthenticateFamilyInfoM user_authenticate_family_info = UserAuthenticateFamilyInfoM.dao.findById(user_id);
		UserAuthenticatePersionalInfoM user_authenticate_persional_info = UserAuthenticatePersionalInfoM.dao.findById(user_id);
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);
		UserAuthenticateWorkInfoM user_authenticate_work_info = UserAuthenticateWorkInfoM.dao.findById(user_id);

		String sex = UserM.get_age(user_id);
		String birthday = UserM.get_birthday(user_id);
		String user_identity = user.getString("user_identity");
		String phone = user.getString("phone");

		String phone_new = phone.substring(0, 3) + "****" + phone.substring(7);
		String user_identity_new = user_identity.substring(0, 2) + " **** **** **** ****";

		BigDecimal borrow_all_money = borrower_bulk_standard_apply_order.getBigDecimal("borrow_all_money");
		BigDecimal annulized_rate = borrower_bulk_standard_apply_order.getBigDecimal("annulized_rate");
		int borrow_duration = borrower_bulk_standard_apply_order.getInt("borrow_duration");
		BigDecimal monthly_principal_interest = CalculationFormula.get_principal_and_interest_by_year_rate(borrow_all_money, annulized_rate, borrow_duration).setScale(2, BigDecimal.ROUND_DOWN);

		setAttribute("sex", sex);
		setAttribute("birthday", birthday);
		setAttribute("phone", phone_new);
		setAttribute("user_identity", user_identity_new);

		setAttribute("user_authenticate_assets_info", user_authenticate_assets_info);
		setAttribute("user_authenticate_family_info", user_authenticate_family_info);
		setAttribute("user_authenticate_persional_info", user_authenticate_persional_info);
		setAttribute("user_authenticate_upload_info", user_authenticate_upload_info);
		setAttribute("user_authenticate_work_info", user_authenticate_work_info);

		setAttribute("monthly_principal_interest", monthly_principal_interest);
		setAttribute("borrower_bulk_standard_apply_order", borrower_bulk_standard_apply_order);

		renderJsp("/user/authenticate/authenticate_info_preview.jsp");

	}
}
