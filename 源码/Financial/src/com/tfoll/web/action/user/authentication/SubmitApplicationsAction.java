package com.tfoll.web.action.user.authentication;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.action.user.financial.BorrowAction;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrdeWaitingM;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebLogRecordsUtil;

import java.math.BigDecimal;
import java.util.Date;

@ActionKey("/user/authentication/submit_application")
public class SubmitApplicationsAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "提交审核申请", last_update_author = "zjb")
	public void submit() throws Exception {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		final int user_id = user.getInt("id");
		final int borrow_type = user.getInt("borrow_type");

		if (!(borrow_type == 1 || borrow_type == 2 || borrow_type == 3)) {
			renderText("");
			return;
		}

		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.dao.findById(user_id);
		int persional_info_status = user_authenticate_left_status.get("persional_info_status");
		int family_info_status = user_authenticate_left_status.get("family_info_status");
		int work_info_status = user_authenticate_left_status.get("work_info_status");
		int assets_info_status = user_authenticate_left_status.get("assets_info_status");
		int upload_info_status = user_authenticate_left_status.get("upload_info_status");

		// 必要上传认证信息状态 0 未认证，1 认证通过， 2 认证不通过
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);// 外键做主键
		int id_authenticate_status = user_authenticate_upload_info.getInt("id_authenticate_status");
		int work_authenticate_status = user_authenticate_upload_info.getInt("work_authenticate_status");
		int credit_authenticate_status = user_authenticate_upload_info.getInt("credit_authenticate_status");
		int income_authenticate_status = user_authenticate_upload_info.getInt("income_authenticate_status");

		if (persional_info_status == 1 && family_info_status == 1 && work_info_status == 1 && assets_info_status == 1 && upload_info_status == 1) {
			if (!((borrow_type == 3 && (id_authenticate_status == 0 || id_authenticate_status == 1)) || ((id_authenticate_status == 0 || id_authenticate_status == 1) && (work_authenticate_status == 0 || work_authenticate_status == 1) && (credit_authenticate_status == 0 || credit_authenticate_status == 1) && (income_authenticate_status == 0 || income_authenticate_status == 1)))) {
				renderText("请补全不通过的必要上传信息！");
				return;
			}

			final BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.get_last_apply_order(user_id);
			int state = borrower_bulk_standard_apply_order.get("state");
			if (state == 1) {
				boolean is_ok = Db.tx(new IAtomic() {

					public boolean transactionProcessing() throws Exception {
						Date commit_time = new Date();
						int state_change = 0;
						boolean is_ok_borrower_bulk_standard_apply_order_waiting = false;
						if (borrow_type == 1 || borrow_type == 2) {
							state_change = 2;
							BorrowerBulkStandardApplyOrdeWaitingM borrower_bulk_standard_apply_order_waiting = new BorrowerBulkStandardApplyOrdeWaitingM();
							is_ok_borrower_bulk_standard_apply_order_waiting = borrower_bulk_standard_apply_order_waiting.//
									set("user_id", user_id).//
									set("apply_order_id", borrower_bulk_standard_apply_order.getLong("id")).//
									set("add_time", commit_time).//
									save();
						} else {
							state_change = 3;
							is_ok_borrower_bulk_standard_apply_order_waiting = true;
						}

						boolean is_ok_borrower_bulk_standard_apply_order = borrower_bulk_standard_apply_order.set("state", state_change).set("commit_time", commit_time).update();

						return is_ok_borrower_bulk_standard_apply_order && is_ok_borrower_bulk_standard_apply_order_waiting;
					}
				});

				if (is_ok) {
					renderText("ok");
					return;
				} else {
					renderText("no");
					return;
				}
			} else {
				renderText("no");
				return;
			}
		} else {
			renderText("no");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "跳转审核页面", last_update_author = "zjb")
	public void auditing_index() throws Exception {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");
		String credit_rating = UserCreditFilesM.get_credit_rating(user_id);// 得到信用等级

		// 判断是否有状态为 2 的订单
		String apply_order_id_string = BorrowAction.get_value_from_request_and_session_of_apply_order_id();
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		if (apply_order_id == 0) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");
			return;
		}
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findById(apply_order_id);
		Date commit_time = borrower_bulk_standard_apply_order.getDate("commit_time");
		if (commit_time == null) {
			throw new NullPointerException("commit_time is null");
		}
		long commit_time_next_long = commit_time.getTime();
		@SuppressWarnings("unused")
		Date commit_time_next = new Date(commit_time_next_long);
		long now = new Date().getTime();
		setAttribute("commit_time_next_long", commit_time_next_long);// 如果是2则要提示在多久后系统会自动提交这个单子
		setAttribute("now", now);

		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findFirst("select * from user_authenticate_upload_info where user_id =  ?", user_id);// 外键做主键

		BigDecimal borrow_all_money = borrower_bulk_standard_apply_order.getBigDecimal("borrow_all_money");
		BigDecimal annulized_rate = borrower_bulk_standard_apply_order.getBigDecimal("annulized_rate");

		BigDecimal monthly_principal_interest = new BigDecimal("0");
		int borrow_duration = borrower_bulk_standard_apply_order.getInt("borrow_duration");
		int borrow_duration_day = borrower_bulk_standard_apply_order.getInt("borrow_duration_day");

		if (borrow_duration != 0) {
			monthly_principal_interest = CalculationFormula.get_principal_and_interest_by_year_rate(borrow_all_money, annulized_rate, borrow_duration).setScale(2, BigDecimal.ROUND_DOWN);
		} else {
			monthly_principal_interest = CalculationFormula.get_principal_and_interest_by_year_rate_by_day(borrow_all_money, annulized_rate, borrow_duration_day).setScale(2, BigDecimal.ROUND_DOWN);
		}

		BigDecimal manage_fee = borrow_all_money.multiply(new BigDecimal("0.003")).setScale(2, BigDecimal.ROUND_DOWN);

		BigDecimal borrow_all_money_1 = borrow_all_money.multiply(new BigDecimal("0.01")).setScale(2, BigDecimal.ROUND_DOWN);
		BigDecimal borrow_all_money_2 = borrow_all_money.multiply(new BigDecimal("0.02")).setScale(2, BigDecimal.ROUND_DOWN);
		BigDecimal borrow_all_money_3 = borrow_all_money.multiply(new BigDecimal("0.03")).setScale(2, BigDecimal.ROUND_DOWN);
		BigDecimal borrow_all_money_4 = borrow_all_money.multiply(new BigDecimal("0.04")).setScale(2, BigDecimal.ROUND_DOWN);
		BigDecimal borrow_all_money_5 = borrow_all_money.multiply(new BigDecimal("0.05")).setScale(2, BigDecimal.ROUND_DOWN);

		setAttribute("borrow_all_money_1", borrow_all_money_1);
		setAttribute("borrow_all_money_2", borrow_all_money_2);
		setAttribute("borrow_all_money_3", borrow_all_money_3);
		setAttribute("borrow_all_money_4", borrow_all_money_4);
		setAttribute("borrow_all_money_5", borrow_all_money_5);

		setAttribute("credit_rating", credit_rating);
		setAttribute("monthly_principal_interest", monthly_principal_interest);
		setAttribute("manage_fee", manage_fee);

		setAttribute("user_authenticate_upload_info", user_authenticate_upload_info);
		setAttribute("borrower_bulk_standard_apply_order", borrower_bulk_standard_apply_order);

		renderJsp("/user/authenticate/auditing.jsp");
		return;
	}
}
