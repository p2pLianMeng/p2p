package com.tfoll.web.action.api;

import com.google.gson.reflect.TypeToken;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.ZipUtil;
import com.tfoll.web.action.user.authentication.UploadInfoAction.IndexUrl;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.util.FileUtil;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebApp;

import it.sauronsoftware.base64.Base64;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * 该类无须登录即可访问
 * 
 */
@ActionKey("/admin_visit_file")
public class AdminVisitFileAction extends Controller {

	/**
	 * <pre>
	 * http://+ip或者域名+/admin_visit_file/download.html?password=459b58fdea4bc9564b4bb19c0709190a&path=RDpcdXBsb2FkXGVyZGV3cnFlcmZ3ZS50eHQ=
	 * @param  password=459b58fdea4bc9564b4bb19c0709190a
	 * @param  path=base64加密后的文件真实路径地址
	 * 
	 * System.out.println(Base64.encode("D:\\upload\\erdewrqerfwe.txt", "UTF-8"));
	 *</pre>
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "下载文件", last_update_author = "曹正辉")
	public void download() throws Exception {
		String password = getParameter("password");
		if (Utils.isNullOrEmptyString(password)) {
			renderText("没有权限");
			return;
		}
		if (!"459b58fdea4bc9564b4bb19c0709190a".equals(password)) {// 963852741
			renderText("没有权限");
			return;
		}
		String path = getParameter("path");
		if (Utils.isNullOrEmptyString(path)) {
			renderText("没有权限");
			return;
		}
		path = Base64.decode(path, "UTF-8");
		System.out.println(path);
		File file = new File(path);
		if (!file.exists()) {
			renderText("服务器删除了");
			return;
		}
		String file_path = file.getAbsolutePath();
		System.out.println(file_path);
		System.out.println(WebApp.Files_Stored_Palce);
		if (!file_path.contains(WebApp.Files_Stored_Palce)) {
			renderText("服务器删除了...");
			logger.error("黑客正在尝试下载服务器的敏感文件");
			return;
		}
		renderFile(file);
		return;
	}

	/**
	 * <pre>
	 * http://+ip或者域名+/admin_visit_file/download_zip.html?password=459b58fdea4bc9564b4bb19c0709190a&apply_order_id=RDpcdXBsb2FkXGVyZGV3cnFlcmZ3ZS50eHQ=
	 * @param  password=459b58fdea4bc9564b4bb19c0709190a
	 * @param  apply_order_id=真实订单id
	 * 
	 *</pre>
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "下载zip文件", last_update_author = "zjb")
	public void download_zip() throws Exception {
		String password = getParameter("password");
		if (Utils.isNullOrEmptyString(password)) {
			renderText("没有权限");
			return;
		}
		if (!"459b58fdea4bc9564b4bb19c0709190a".equals(password)) {// 963852741
			renderText("没有权限");
			return;
		}

		String apply_order_id_string = getParameter("apply_order_id");
		if (Utils.isNullOrEmptyString(apply_order_id_string)) {
			renderText("没有权限");
			return;
		}
		long apply_order_id = Long.parseLong(apply_order_id_string);
		//
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findById(apply_order_id);
		if (borrower_bulk_standard_apply_order == null) {
			renderText("查询不到信息");
			return;
		}
		int user_id = borrower_bulk_standard_apply_order.getInt("user_id");
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);

		String[] user_authenticate_upload_info_array = UserAuthenticateUploadInfoM.user_authenticate_upload_info_array;
		String[] user_authenticate_upload_info_array_chinese = { "身份信息", "工作信息", "信用信息", "收入信息", "房产信息", "车产信息", "婚姻信息", "教育信息", "职称", "居住信息" };
		;
		String zip_folder_path = WebApp.Zip_Files_Stored_Palce;
		/**
		 * 建立一个文件夹
		 */
		String apply_order_path = zip_folder_path + WebApp.Windows_File_Separator + apply_order_id + WebApp.Windows_File_Separator;// 申请单根路径
		File apply_order_folder = new File(apply_order_path);
		if (apply_order_folder.exists()) {
			// Files.delete(new Path(apply_order_path));//需要把里面所有的数据删除
			FileUtil.removeFile(apply_order_folder);

		}
		apply_order_folder.mkdirs();
		// 从数据库提出地址，创建相应file，添加到文件数组list
		int user_authenticate_upload_info_array_length = user_authenticate_upload_info_array.length;
		for (int i = 0; i < user_authenticate_upload_info_array_length; i++) {
			String url = user_authenticate_upload_info.getString(user_authenticate_upload_info_array[i] + "_authenticate_url");
			if (Utils.isNotNullAndNotEmptyString(url)) {
				List<IndexUrl> index_url_list = gson.fromJson(url, new TypeToken<List<IndexUrl>>() {
				}.getType());
				int index_url_list_size = index_url_list.size();
				for (int j = 0; j < index_url_list_size; j++) {
					IndexUrl index_url = index_url_list.get(j);
					File file = new File(index_url.getUrl());
					String file_name = file.getName();
					String[] names = file_name.split("\\.");
					/**
					 * 构建新的地址
					 */
					String user_authenticate_upload_info_path = apply_order_path + user_authenticate_upload_info_array_chinese[i] + WebApp.Windows_File_Separator;
					File user_authenticate_upload_info_path_file = new File(user_authenticate_upload_info_path);
					if (!user_authenticate_upload_info_path_file.exists()) {
						user_authenticate_upload_info_path_file.mkdirs();
					}
					String image_file_path = user_authenticate_upload_info_path + (j + 1) + "." + names[1];
					FileUtils.copyFile(file, new File(image_file_path));
				}

			}
		}
		// 在zip申请单文件夹添加一个
		File zip = new File(zip_folder_path + WebApp.Windows_File_Separator + apply_order_id + ".zip");
		ZipUtil.ZipFiles(zip, "信用认证", apply_order_folder);
		FileUtil.removeFile(apply_order_folder);
		renderFile(zip);// 这个不能删除
		return;
	}

	public static void main(String[] args) {
		// System.out.println(MD5.md5("123456"));
		System.out.println(Base64.encode("37", "UTF-8"));
	}
}
