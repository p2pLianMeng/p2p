package com.tfoll.web.common;

import com.tfoll.trade.activerecord.ActiveRecord;
import com.tfoll.trade.config.InterceptorList;
import com.tfoll.trade.config.PluginList;
import com.tfoll.trade.config.RouteSet;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.trade.plugin.AsynServicePlugin;
import com.tfoll.trade.plugin.DbcpPlugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TfollConfig extends UserConfig {

	@Override
	public void configActiveRecord() {
		UserConfig.logging.info("====================[开始配置ActiveRecord组件]====================");
		ActiveRecord autoTableBindPlugin = new ActiveRecord(DbcpPlugin.getDataSource());
		autoTableBindPlugin.buildAllModelAndTableInfoMapping();
		UserConfig.logging.info("====================[成功配置ActiveRecord组件]====================");

	}

	@Override
	public void configInterceptor(InterceptorList me) {

		UserConfig.logging.info("====================开始配置拦截器====================");
		/**
		 * 系统安全角色拦截器
		 */

		UserConfig.logging.info("====================成功配置拦截器====================");

	}

	@Override
	public void configPlugin(PluginList me) {

		UserConfig.logging.info("====================开始配置插件集====================");
		Properties properties = new Properties();
		InputStream in = null;
		try {
			if (isWindows()) {
				in = TfollConfig.class.getResourceAsStream("dbcp.properties");
			} else {
				in = TfollConfig.class.getResourceAsStream("dbcp_linux.properties");
			}

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			properties.load(in);
		} catch (IOException e) {
			throw new RuntimeException("静态资源文件不存在");
		}
		DbcpPlugin dbcpPlugin = null;
		try {
			dbcpPlugin = new DbcpPlugin(properties);
		} catch (Exception e) {
			logging.error(e);
			e.printStackTrace();

		}
		me.add(dbcpPlugin);
		me.add(new AsynServicePlugin());// 加载异步服务

		UserConfig.logging.info("====================成功配置插件集====================");

	}

	@Override
	public void configRoute() {
		RouteSet.setBaseViewPath("/jsp");

	}

	@Override
	public void afterBuildOK() {
		{
			// do somthing
			SystemConfig.init();
		}
		// 告诉整个系统或者定时器框架初始化完成
		UserConfig.System_Is_Start_Ok.set(true);

		UserConfig.logging.info("====================框架启动OK====================");
	}

	/**
	 * 读取配置文件
	 */
	public boolean isWindows() {
		boolean flag = false;
		if (System.getProperties().getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1) {
			flag = true;
		}
		return flag;
	}

}
