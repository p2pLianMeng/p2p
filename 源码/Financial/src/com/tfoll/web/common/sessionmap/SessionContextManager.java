package com.tfoll.web.common.sessionmap;

import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserM;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 
 *用户登录之后才能进行被管理
 */
public class SessionContextManager {
	/**
	 * session管理容器
	 */
	public static final ConcurrentHashMap<Integer, HttpSession> Session_Content_Map = new ConcurrentHashMap<Integer, HttpSession>();
	public static final Logger logger = Logger.getLogger(SessionContextManager.class.getName());

	public static Map<Integer, HttpSession> getSessionContextMap() {
		return SessionContextManager.Session_Content_Map;
	}

	/**
	 * 只能接受登錄的Session
	 */
	public static void addSessionContent(HttpServletRequest request) {
		if (request == null) {
			throw new RuntimeException("request为空");
		}
		HttpSession session = request.getSession();

		UserM user = (UserM) session.getAttribute(SystemConstantKey.User);
		if (user == null) {
			return;
		}

		Object user_id_object = user.get("user_id");
		if (user_id_object == null) {
			return;
		}

		int user_id = user.getInt("user_id");
		/**
		 * 首先检测需要进行可以同一个账户在多个地方同时登录的限制?
		 */
		if (SessionContextManager.Session_Content_Map.containsKey(user_id)) {
			HttpSession session_of_the_same = SessionContextManager.Session_Content_Map.remove(user_id);
			try {
				session_of_the_same.invalidate();
			} catch (Exception e) {
			}
		}
		SessionContextManager.Session_Content_Map.put(user_id, session);

	}

	/**
	 * 直接从SessionContext-Map中删除一个SessionContext
	 */
	public static void removeSessionContent(HttpServletRequest request) {
		if (request == null) {
			logger.info("request为空");
			return;
		}
		HttpSession session = request.getSession();
		UserM user = (UserM) session.getAttribute("user");
		if (user == null) {
			return;
		}

		Object user_id_object = user.get("user_id");
		if (user_id_object == null) {
			return;
		}

		int user_id = user.getInt("user_id");

		if (SessionContextManager.Session_Content_Map.containsKey(user_id)) {
			SessionContextManager.Session_Content_Map.remove(user_id);
		} else {
			logger.info("不能在sessionContextMap找到特定的sessionContext");
		}

	}

	public static HttpSession removeSessionContent(Integer user_id) {
		if (user_id == null) {
			return null;
		}
		if (SessionContextManager.Session_Content_Map.containsKey(user_id)) {
			HttpSession session = Session_Content_Map.get(user_id);
			SessionContextManager.Session_Content_Map.remove(user_id);
			return session;
		} else {
			return null;
		}

	}

	public static HttpSession getSessionContent(Integer user_id) {
		if (user_id == null) {
			return null;
		}
		return Session_Content_Map.get(user_id);

	}

}
