package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_email_request_error", primaryKey = "id")
public class UserEmailRequestErrorM extends Model<UserEmailRequestErrorM> {
	public static UserEmailRequestErrorM dao = new UserEmailRequestErrorM();
}
