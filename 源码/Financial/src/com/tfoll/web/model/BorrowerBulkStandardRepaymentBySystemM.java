package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableBind(tableName = "borrower_bulk_standard_repayment_by_system", primaryKey = "id")
public class BorrowerBulkStandardRepaymentBySystemM extends Model<BorrowerBulkStandardRepaymentBySystemM> implements Serializable {

	private static final long serialVersionUID = -7124140993536632670L;
	public static BorrowerBulkStandardRepaymentBySystemM dao = new BorrowerBulkStandardRepaymentBySystemM();

	public static boolean add_borrower_bulk_standard_repayment_by_system(long gather_money_order_id,//
			int borrower_user_id,//
			int inttotal_periods,//
			BigDecimal all_repayment_total,//
			BigDecimal all_repayment_principle,//
			BigDecimal all_repayment_interest,//
			BigDecimal all_punish_interest,//
			BigDecimal actual_repayment,//
			String repayment_periods//
	) {
		BorrowerBulkStandardRepaymentBySystemM borrower_bulk_standard_repayment_by_system = new BorrowerBulkStandardRepaymentBySystemM();
		Date date = new Date();
		return borrower_bulk_standard_repayment_by_system.//
				set("gather_money_order_id", gather_money_order_id).//
				set("borrower_user_id", borrower_user_id).//
				set("inttotal_periods", inttotal_periods).//
				set("all_repayment_total", all_repayment_total).//
				set("all_repayment_principle", all_repayment_principle).//
				set("all_repayment_interest", all_repayment_interest).//
				set("all_punish_interest", all_punish_interest).//
				set("actual_repayment", actual_repayment).//
				set("repayment_periods", repayment_periods).//
				set("add_time", date).//
				set("add_time_long", date.getTime()).//
				set("overdue_repayment_by_self_or_risk_money", 0).//
				save();
	}
}
