package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_newer_bid_gather", primaryKey = "id")
public class LenderBulkStandardNewerBidGatherM extends Model<LenderBulkStandardNewerBidGatherM> {
	public static LenderBulkStandardNewerBidGatherM dao = new LenderBulkStandardNewerBidGatherM();
}
