package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "bbs_module", primaryKey = "id")
public class BbsModuleM extends Model<BbsModuleM> {
	public static BbsModuleM dao = new BbsModuleM();
}
