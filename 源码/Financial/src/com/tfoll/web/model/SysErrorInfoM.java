package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "sys_error_info", primaryKey = "id")
public class SysErrorInfoM extends Model<SysErrorInfoM> {
	public static SysErrorInfoM dao = new SysErrorInfoM();
}
