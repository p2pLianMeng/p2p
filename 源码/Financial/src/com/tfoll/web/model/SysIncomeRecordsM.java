package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.math.BigDecimal;
import java.util.Date;

@TableBind(tableName = "sys_income_records", primaryKey = "id")
public class SysIncomeRecordsM extends Model<SysIncomeRecordsM> {
	public static SysIncomeRecordsM dao = new SysIncomeRecordsM();

	public static boolean add_sys_income_records(int user_id, int type, BigDecimal money, String detail) {
		SysIncomeRecordsM sys_income_records = new SysIncomeRecordsM();
		Date now = new Date();
		return sys_income_records.//
				set("user_id", user_id).//
				set("type", type).//
				set("money", money).//
				set("detail", detail).//
				set("add_time_long", now.getTime()).//
				set("add_time_date", Date.format(now)).//
				save();

	}
}
