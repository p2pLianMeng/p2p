package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_newer_bid_record_waiting", primaryKey = "id")
public class LenderBulkStandardNewerBidRecordWaitingM extends Model<LenderBulkStandardNewerBidRecordWaitingM> {
	public static LenderBulkStandardNewerBidRecordWaitingM dao = new LenderBulkStandardNewerBidRecordWaitingM();
}
