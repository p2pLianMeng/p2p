package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.annotation.Function;

@TableBind(tableName = "user_message_request_log", primaryKey = "id")
public class UserMessageRequestLogM extends Model<UserMessageRequestLogM> {
	public static UserMessageRequestLogM dao = new UserMessageRequestLogM();

	/**
	 * 短信发送失败添加一条错误记录
	 */
	@Function(for_people = "所有人", function_description = "短信发送失败添加一条错误记录", last_update_author = "czh")
	public static boolean send_rmb_withdraw_message(final String phone, final String[] data, final int id) {

		UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
		return user_message_request_log.set("user_id", id).//
				set("ip", ActionContext.getRequest().getRemoteAddr()).//
				set("phone", phone).//
				set("content", data[0]).//
				set("request_type", 9).//
				set("request_type_name", "提现发送短信提示失败").//
				save();

	}

}
