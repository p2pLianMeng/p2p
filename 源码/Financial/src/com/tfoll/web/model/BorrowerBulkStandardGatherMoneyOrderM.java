package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.math.BigDecimal;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@TableBind(tableName = "borrower_bulk_standard_gather_money_order", primaryKey = "id")
public class BorrowerBulkStandardGatherMoneyOrderM extends Model<BorrowerBulkStandardGatherMoneyOrderM> {

	private static final long serialVersionUID = 8714753820330201896L;
	public static BorrowerBulkStandardGatherMoneyOrderM dao = new BorrowerBulkStandardGatherMoneyOrderM();

	/**
	 * 
	 * 根据id加锁 获取 筹款单的信息
	 * 
	 */
	private static BorrowerBulkStandardGatherMoneyOrderM get_borrower_bulk_standard_gather_money_order_by_id_for_update(Long id) {
		String sql = "select * from borrower_bulk_standard_gather_money_order where id = ? for update";
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst(sql, new Object[] { id });
		return borrower_bulk_standard_gather_money_order;
	}

	/**
	 * 多个用户并发访问同一条筹款单时，
	 * 
	 */
	public static class GetGatherOrderByIdTask implements Callable<BorrowerBulkStandardGatherMoneyOrderM> {
		private long order_id;

		public GetGatherOrderByIdTask(long orderId) {
			super();
			this.order_id = orderId;
		}

		public BorrowerBulkStandardGatherMoneyOrderM call() throws Exception {
			return BorrowerBulkStandardGatherMoneyOrderM.get_borrower_bulk_standard_gather_money_order_by_id_for_update(this.order_id);
		}
	}

	/**
	 * <pre>
	 * 1 该方法可能由于数据库锁等待超时，会返回为null
	 * 2 该方法只能在原子事务里面调用-否则会死锁
	 * </pre>
	 */
	public static BorrowerBulkStandardGatherMoneyOrderM get_borrower_bulk_standard_gather_money_order_short_wait(final long order_id) throws Exception {

		BorrowerBulkStandardGatherMoneyOrderM.GetGatherOrderByIdTask get_fix_bid_task = new BorrowerBulkStandardGatherMoneyOrderM.GetGatherOrderByIdTask(order_id);
		ExecutorService get_gather_order_task_service = Executors.newFixedThreadPool(1);
		// 对task对象进行各种set操作以初始化任务
		Future<BorrowerBulkStandardGatherMoneyOrderM> future = get_gather_order_task_service.submit(get_fix_bid_task);
		try {
			return future.get(3, TimeUnit.SECONDS);
		} catch (Exception e) {
			return null;
		} finally {
			if (future.isCancelled()) {
				future.cancel(true);
			}
			get_gather_order_task_service.shutdownNow();
		}
	}

	// 发标时间30分钟以上， 进度 大于25% 小于95% 的筹集标
	public static BorrowerBulkStandardGatherMoneyOrderM get_borrower_bulk_standard_gather_money_order() {
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT * FROM borrower_bulk_standard_gather_money_order WHERE gather_state=1 and  (add_time < NOW() - INTERVAL 30 MINUTE) AND gather_progress <= 95 GROUP BY id LIMIT 1  ");
		return borrower_bulk_standard_gather_money_order;
	}

	/**
	 * annulized_rate_min annulized_rate_max borrow_duration_min
	 * borrow_duration_max borrow_duration_min borrow_duration_day_min
	 * borrow_duration_day_max borrow_duration_day_max credit_rate_min_int
	 * credit_rate_max_int
	 * */

	// 匹配 符合 传入 年化利率， 借款期限，信用等级int的筹集标
	public static BorrowerBulkStandardGatherMoneyOrderM get_borrower_bulk_standard_gather_money_order(//
			BigDecimal annulized_rate_min,//
			BigDecimal annulized_rate_max,//
			int borrow_duration_min,//
			int borrow_duration_max,//
			int borrow_duration_day_min,//
			int borrow_duration_day_max,//
			int credit_rate_min_int,//
			int credit_rate_max_int) {//
		String sql = "SELECT * FROM borrower_bulk_standard_gather_money_order WHERE gather_state=1 and  (add_time < NOW() - INTERVAL 30 MINUTE) AND gather_progress < 95 " + //
				"AND ( annulized_rate >= ? AND annulized_rate <= ? ) " + //
				"AND (" + //
				"( borrow_duration != 0 AND borrow_duration >= ? AND borrow_duration <= ? )" + // 凑集单是月,borrow_duration_min-可以是0--详见JAVA调用处的代码
				" OR " + //
				"( borrow_duration = 0 AND ? = 0 AND borrow_duration_day >= ? AND ( borrow_duration_day <= ? OR ? = 0 ))" + // 凑集单是天的,min为天，max为月
				") " + //
				"AND ( ? >= credit_rating_int && credit_rating_int >= ? ) " + //
				"GROUP BY id LIMIT 1 ";
		Object[] params = new Object[] { annulized_rate_min, annulized_rate_max, borrow_duration_min, borrow_duration_max, borrow_duration_min, borrow_duration_day_min, borrow_duration_day_max, borrow_duration_day_max, credit_rate_min_int, credit_rate_max_int };
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst(sql, params);
		return borrower_bulk_standard_gather_money_order;
	}
}