package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "about_type", primaryKey = "id")
public class AboutTypeM extends Model<AboutTypeM> implements Serializable {

	private static final long serialVersionUID = -1508853254428116749L;
	public static AboutTypeM dao = new AboutTypeM();
}
