package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_authenticate_left_status", primaryKey = "user_id")
public class UserAuthenticateLeftStatusM extends Model<UserAuthenticateLeftStatusM> {
	public static UserAuthenticateLeftStatusM dao = new UserAuthenticateLeftStatusM();

	// 获得左边框状态
	public static UserAuthenticateLeftStatusM get_user_authenticate_left_status(int user_id) {
		return UserAuthenticateLeftStatusM.dao.findFirst("select * from user_authenticate_left_status where user_id = ?   limit 1", new Object[] { user_id });
	}

	// 得到认证信息进度
	public static int get_rate(int user_id) {
		int rate = 0;
		String[] arr = { "persional", "family", "work", "assets" };
		int upload_num_rate = UserAuthenticateUploadInfoM.get_upload_num(user_id) * 2;
		UserAuthenticateLeftStatusM user_authenticate_left_status = UserAuthenticateLeftStatusM.dao.findById(user_id);
		for (int i = 0; i < arr.length; i++) {
			if (user_authenticate_left_status.getInt(arr[i] + "_info_status") == 1) {
				rate += 20;
			}
		}
		rate += upload_num_rate;
		return rate;
	}

}
