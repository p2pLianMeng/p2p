package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "sys_province_city", primaryKey = "id")
public class SysProvinceCityM extends Model<SysProvinceCityM> {
	public static SysProvinceCityM dao = new SysProvinceCityM();
}
