package com.tfoll.web.timer;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.common.sessionmap.SessionContextManager;
import com.tfoll.web.model.UserAutomaticallyBidUserInfoChangeNotifyM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.Utils;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *同步更新自动投标的用户信息
 */
public class Timer_user_automatically_bid_user_info_change_notify extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_user_automatically_bid_user_info_change_notify.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.debug("系统框架还没有启动好,定时任务:" + Timer_user_automatically_bid_user_info_change_notify.class.getName() + "暂时还不能执行");
			return;
		}
		if (isRunning.get()) {
			logger.debug("还有其他的线程正在执行定时任务:" + Timer_user_automatically_bid_user_info_change_notify.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.debug("定时任务:" + Timer_user_automatically_bid_user_info_change_notify.class.getName() + "开始");
				{ // 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}

				}
				logger.debug("定时任务:" + Timer_user_automatically_bid_user_info_change_notify.class.getName() + "结束");
			} catch (Exception e) {
				logger.debug("定时任务:" + Timer_user_automatically_bid_user_info_change_notify.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}

			} finally {
				isRunning.set(false);
			}
		}

	}

	/**
	 * 具体业务处理
	 */
	private void doTask() throws Exception {
		List<UserAutomaticallyBidUserInfoChangeNotifyM> user_automatically_bid_user_info_change_notify_list = UserAutomaticallyBidUserInfoChangeNotifyM.dao.find("SELECT id,user_id,add_time_long from user_automatically_bid_user_info_change_notify");
		if (Utils.isHasData(user_automatically_bid_user_info_change_notify_list)) {
			for (UserAutomaticallyBidUserInfoChangeNotifyM user_automatically_bid_user_info_change_notify : user_automatically_bid_user_info_change_notify_list) {
				doSubTaskFor(user_automatically_bid_user_info_change_notify);
			}

		}
	}

	private void doSubTaskFor(UserAutomaticallyBidUserInfoChangeNotifyM user_automatically_bid_user_info_change_notify) {
		int user_id = user_automatically_bid_user_info_change_notify.getInt("user_id");
		long add_time_long = user_automatically_bid_user_info_change_notify.getLong("add_time_long");
		if (add_time_long >= System.currentTimeMillis() + 1 * Model.Minute) {
			user_automatically_bid_user_info_change_notify.delete();
		} else {
			HttpSession session = SessionContextManager.getSessionContent(user_id);
			if (session == null) {
				user_automatically_bid_user_info_change_notify.delete();
			} else {
				user_automatically_bid_user_info_change_notify.delete();
				UserM user = UserM.getUserForSelect(user_id);
				session.setAttribute(SystemConstantKey.User, user);

			}
		}

	}

}