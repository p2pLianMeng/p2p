package com.tfoll.web.timer.bulk_standard;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutWaitingM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *理财的时候债权价值改变处理
 */
public class Timer_lender_bulk_standard_order_creditor_right_transfer_out_waiting_deal extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_lender_bulk_standard_order_creditor_right_transfer_out_waiting_deal.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.debug("系统框架还没有启动好,定时任务:" + Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class.getName() + "暂时还不能执行");
			return;
		}
		if (isRunning.get()) {
			logger.debug("还有其他的线程正在执行定时任务:" + Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.debug("定时任务:" + Timer_lender_bulk_standard_order_creditor_right_transfer_out_waiting_deal.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}

				}
				logger.debug("定时任务:" + Timer_lender_bulk_standard_order_creditor_right_transfer_out_waiting_deal.class.getName() + "结束");
			} catch (Exception e) {
				logger.debug("定时任务:" + Timer_lender_bulk_standard_order_creditor_right_transfer_out_waiting_deal.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}

			} finally {
				isRunning.set(false);
			}
		}

	}

	/**
	 * 具体业务处理
	 */
	private void doTask() throws Exception {
		String sql_creditor_right_transfer_out_waiting = "select * from lender_bulk_standard_order_creditor_right_transfer_out_waiting";
		List<LenderBulkStandardOrderCreditorRightTransferOutWaitingM> lender_bulk_standard_order_creditor_right_transfer_out_waiting_list = LenderBulkStandardOrderCreditorRightTransferOutWaitingM.dao.find(sql_creditor_right_transfer_out_waiting);
		if (Utils.isHasData(lender_bulk_standard_order_creditor_right_transfer_out_waiting_list)) {
			/**
			 * 整体更新上次deal_level=1的处理平率更新时间
			 */
			long now = System.currentTimeMillis();
			for (LenderBulkStandardOrderCreditorRightTransferOutWaitingM lender_bulk_standard_order_creditor_right_transfer_out_waiting : lender_bulk_standard_order_creditor_right_transfer_out_waiting_list) {
				try {

					doSubTask(lender_bulk_standard_order_creditor_right_transfer_out_waiting, now);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}
			if (Last_Deal_Time.get() + Model.Minute * 5 <= now) {
				Last_Deal_Time.set(now);
			}
		}

	}

	public static final AtomicLong Last_Deal_Time = new AtomicLong(0);

	private void doSubTask(LenderBulkStandardOrderCreditorRightTransferOutWaitingM lender_bulk_standard_order_creditor_right_transfer_out_waiting, long now) {
		// long creditor_right_transfer_out_waiting_id =
		// lender_bulk_standard_order_creditor_right_transfer_out_waiting.getLong("id");
		long creditor_right_transfer_out_id = lender_bulk_standard_order_creditor_right_transfer_out_waiting.getLong("creditor_right_transfer_out_id");
		int deal_level = lender_bulk_standard_order_creditor_right_transfer_out_waiting.getInt("deal_level");

		LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out = LenderBulkStandardOrderCreditorRightTransferOutM.dao.findById(creditor_right_transfer_out_id);
		if (!(lender_bulk_standard_order_creditor_right_transfer_out != null && (lender_bulk_standard_order_creditor_right_transfer_out.getInt("state") == 1))) {
			lender_bulk_standard_order_creditor_right_transfer_out_waiting.delete();
		} else {

			if (deal_level == 1) {
				if (Last_Deal_Time.get() + Model.Minute * 5 <= now) {
					doSubTaskForLenderBulkStandardOrderCreditorRightTransferOutWaitingM(lender_bulk_standard_order_creditor_right_transfer_out);
				}
			} else if (deal_level == 2) {
				doSubTaskForLenderBulkStandardOrderCreditorRightTransferOutWaitingM(lender_bulk_standard_order_creditor_right_transfer_out);
			} else {
				throw new RuntimeException("deal_level 错误");
			}

		}

	}

	private void doSubTaskForLenderBulkStandardOrderCreditorRightTransferOutWaitingM(LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out) {
		if (lender_bulk_standard_order_creditor_right_transfer_out == null) {
			throw new RuntimeException("lender_bulk_standard_order_creditor_right_transfer_out  is null");
		}
		/**
		 * 到转让表去查询数据
		 */
		// BigDecimal creditor_right_value =
		// lender_bulk_standard_order_creditor_right_transfer_out.getBigDecimal("creditor_right_value");//债权价值(元/份)
		// BigDecimal transfer_price =
		// lender_bulk_standard_order_creditor_right_transfer_out.getBigDecimal("transfer_price");

		long gather_money_order_id = lender_bulk_standard_order_creditor_right_transfer_out.getLong("gather_money_order_id");// 筹集单id
		BigDecimal transfer_factor = lender_bulk_standard_order_creditor_right_transfer_out.getBigDecimal("transfer_factor");// 转让系数
		int transfer_share = lender_bulk_standard_order_creditor_right_transfer_out.getInt("transfer_share");

		BigDecimal debt_value = CalculationFormula.get_debt_value_per_share(gather_money_order_id);// 重新计算债权价值
		BigDecimal transfer_price = debt_value.multiply(transfer_factor);// 重新计算转让的价格
		BigDecimal transfer_total_value = transfer_price.multiply(new BigDecimal(transfer_share));

		debt_value = debt_value.setScale(4, BigDecimal.ROUND_DOWN); // 数据库的数据是2位
		lender_bulk_standard_order_creditor_right_transfer_out.set("creditor_right_value", debt_value);
		lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_price", transfer_price);
		lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_total_value", transfer_total_value);

		lender_bulk_standard_order_creditor_right_transfer_out.update();
	}

}