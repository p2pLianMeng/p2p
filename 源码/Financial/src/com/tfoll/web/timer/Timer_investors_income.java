package com.tfoll.web.timer;

import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.util.InvestorsIncome;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *理财的时候债权价值改变消息推送定时器
 */
public class Timer_investors_income extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_investors_income.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.debug("系统框架还没有启动好,定时任务:" + Timer_investors_income.class.getName() + "暂时还不能执行");
			return;
		}
		if (isRunning.get()) {
			logger.debug("还有其他的线程正在执行定时任务:" + Timer_investors_income.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.debug("定时任务:" + Timer_investors_income.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}

				}
				logger.debug("定时任务:" + Timer_investors_income.class.getName() + "结束");
			} catch (Exception e) {
				logger.debug("定时任务:" + Timer_investors_income.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}

			} finally {
				isRunning.set(false);
			}
		}

	}

	/**
	 * 具体业务处理
	 */
	private void doTask() throws Exception {
		InvestorsIncome investors_income = new InvestorsIncome();
		investors_income.investors_income();
	}

}