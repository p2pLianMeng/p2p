package com.tfoll.web.util;

import java.io.File;

public class FileUtil {
	/**
	 * 递归删除文件
	 */
	public static void removeFile(File path) {
		if (path.isDirectory()) {
			File[] child = path.listFiles();
			if (child != null && child.length != 0) {
				int length = child.length;
				for (int i = 0; i < length; i++) {
					removeFile(child[i]);
					child[i].delete();
				}
			}
		}
		path.delete();
	}
}
