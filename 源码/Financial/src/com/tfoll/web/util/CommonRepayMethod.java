package com.tfoll.web.util;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.model.UserAutomaticallyBidUserInfoChangeNotifyM;
import com.tfoll.web.model.UserM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class CommonRepayMethod {

	/**
	 * 根据筹款单id获取还款计划详情 统计 总的 未还本息，已还本息，管理费 ，已还总额，未还总额 未还总额包括 该还本息 正常管理费 逾期费用
	 * 
	 * @param gather_order_id
	 *            筹款单id
	 * @return
	 */
	public static Map<String, Object> get_repayment_info(long gather_order_id) {

		BigDecimal borrow_all_money = Db.queryBigDecimal("select borrow_all_money from borrower_bulk_standard_gather_money_order where id = ?", new Object[] { gather_order_id });
		int borrow_all_share = borrow_all_money.intValue() / 50; // 对应的筹款单的总份数

		String sql = "select * from borrower_bulk_standard_repayment_plan where gather_money_order_id = ?";
		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find(sql, new Object[] { gather_order_id });

		List<Map<String, Object>> repayment_plans = new ArrayList<Map<String, Object>>();

		BigDecimal yihuan_total = new BigDecimal("0");
		BigDecimal weihuan_total = new BigDecimal("0"); // 总的未还 本息+管理费+逾期费用

		BigDecimal weihuan_benjin_total = new BigDecimal("0");// 未还本金

		BigDecimal weihuan_benxi_total = new BigDecimal("0");// 总的 未还本息
		BigDecimal weihuan_normal_manage_total = new BigDecimal("0");// 未还的
		// 总的正常管理费
		BigDecimal weihuan_over_fee = new BigDecimal("0");// 未还的
		// 总的逾期费用(逾期罚息+逾期管理费)

		BigDecimal yihuan_benxi_total = new BigDecimal("0");// 总的 已还本息

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
			Map<String, Object> repayment_plan = borrower_bulk_standard_repayment_plan.getM();
			repayment_plans.add(repayment_plan);

			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");

			BigDecimal total_total = new BigDecimal("0");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
			// 要还的本金
			BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
			// 正常管理费
			BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));

			// 逾期罚息
			BigDecimal over_faxi = new BigDecimal(0 + "");
			// 逾期管理费
			BigDecimal over_manage_fee = new BigDecimal(0 + "");

			if (is_repay == 1) {
				over_faxi = borrower_bulk_standard_repayment_plan.getBigDecimal("over_punish_interest");
				over_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("over_manage_fee");
			} else if (is_repay == 0) {
				over_faxi = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
				over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
			}

			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_fee = over_faxi.add(over_manage_fee);

			// 应还总额=月还本息+管理费 +逾期费用
			total_total = total_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);

			repayment_plan.put("over_faxi", over_faxi);
			repayment_plan.put("manage_fee", normal_manage_fee);
			repayment_plan.put("over_total_fee", over_fee);
			repayment_plan.put("total_total", total_total);

			// 一下是计算总的东西
			if (is_repay == 0) {// 未还
				weihuan_total = weihuan_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);
				weihuan_benxi_total = weihuan_benxi_total.add(should_repayment_total);
				weihuan_benjin_total = weihuan_benjin_total.add(should_repayment_principle);
				weihuan_normal_manage_total = weihuan_normal_manage_total.add(normal_manage_fee);
				weihuan_over_fee = weihuan_over_fee.add(over_fee);
			} else if (is_repay == 1) {// 已还
				yihuan_total = yihuan_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);
				yihuan_benxi_total = yihuan_benxi_total.add(should_repayment_total);
			}

		}

		BigDecimal weihuan_benxi_total_per_share = new BigDecimal("0.00"); // 每份
		// 待还本息
		// ，也是
		// 待收本息
		weihuan_benxi_total_per_share = weihuan_benxi_total.divide(new BigDecimal(borrow_all_share), 2, BigDecimal.ROUND_DOWN);

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("repayment_plans", repayment_plans);

		weihuan_total = weihuan_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_total", weihuan_total);

		weihuan_benxi_total = weihuan_benxi_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_beixi_total", weihuan_benxi_total);

		weihuan_benjin_total = weihuan_benjin_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_benjin_total", weihuan_benjin_total);

		yihuan_benxi_total = yihuan_benxi_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("yihuan_benxi_total", yihuan_benxi_total);

		map.put("weihuan_benxi_total_per_share", weihuan_benxi_total_per_share);

		weihuan_normal_manage_total = weihuan_normal_manage_total.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_normal_manage_total", weihuan_normal_manage_total);

		weihuan_over_fee = weihuan_over_fee.setScale(2, BigDecimal.ROUND_DOWN);
		map.put("weihuan_over_fee", weihuan_over_fee);

		map.put("borrow_all_share", borrow_all_share);

		return map;

	}

	/**
	 * 根据理财用户的id 查询债权投资账户资产
	 * 
	 * 理财账户内的资产， 包括债权投资账户资产和联富宝账户资产， 其中债权投资账户资产等于持有债权的待回收本金
	 */
	public static Map<String, Object> get_elite_bid_account(int user_id) {

		// 查询某个用户持有的所有的债权的id（要查未还的）
		String sql_gather_order_id_list = "SELECT gather_money_order_id from lender_bulk_standard_creditor_right_hold where user_id = ? and gather_money_order_id in (select id from borrower_bulk_standard_gather_money_order where payment_state != 3)";
		List<Long> gather_order_id_list = Db.query(sql_gather_order_id_list, new Object[] { user_id });

		// 理财人的待收本金(所有债权)
		BigDecimal to_collect_pricipal_all = new BigDecimal("0");
		// 理财人的待收本息(所有债权)
		BigDecimal to_collect_pricipal_interest_all = new BigDecimal("0");

		// 所有债权的年化利率之和
		int average_income_rate_all = 0;
		// average_income_rate 加权平均收益率
		BigDecimal average_income_rate = new BigDecimal("0.00");

		// 持有数量
		int count = gather_order_id_list.size();

		for (long gather_order_id : gather_order_id_list) {
			// 查询某个债权的待还本金 待还本息
			String sql_daihuan_benjin = "SELECT sum(should_repayment_principle) as sum_should_repayment_principle, sum(should_repayment_total) as sum_should_repayment_total  FROM borrower_bulk_standard_repayment_plan WHERE is_repay = 0 AND gather_money_order_id = ? ";
			Record record_sum = Db.findFirst(sql_daihuan_benjin, gather_order_id);
			BigDecimal daihuan_benjin = record_sum.getBigDecimal("sum_should_repayment_principle");
			BigDecimal daihuan_benxi = record_sum.getBigDecimal("sum_should_repayment_total");

			// 查询某个债权的 已还利息 和 已还罚息

			// 该债权的总份额的该用户持有的份额
			String sql_ = "SELECT a.hold_share, b.borrow_all_share, b.annulized_rate_int FROM lender_bulk_standard_creditor_right_hold a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id and b.id = ? and a.user_id = ? ";
			Record record = Db.findFirst(sql_, new Object[] { gather_order_id, user_id });

			int hold_share = record.getInt("hold_share"); // 理财人的持有份额
			int borrow_all_share = record.getInt("borrow_all_share");
			int annulized_rate_int = record.getInt("annulized_rate_int");
			// 某个理财人 每个债权的待收本金
			BigDecimal to_collect_pricipal = daihuan_benjin.multiply(new BigDecimal(hold_share)).divide(new BigDecimal(borrow_all_share), 20, BigDecimal.ROUND_DOWN);// 需要先*再/且传入的是字符串
			// BigDecimal to_collect_priciple = daihuan_benjin.multiply(new
			// BigDecimal( new Integer(hold_share).doubleValue() /
			// borrow_all_share ));//需要先*再/且传入的是字符串
			to_collect_pricipal_all = to_collect_pricipal_all.add(to_collect_pricipal);

			// 某个理财人 每个债权的待收本息
			BigDecimal to_collect_pricipal_interest = daihuan_benxi.multiply(new BigDecimal(hold_share)).divide(new BigDecimal(borrow_all_share), 20, BigDecimal.ROUND_DOWN);
			to_collect_pricipal_interest_all = to_collect_pricipal_interest_all.add(to_collect_pricipal_interest);

			average_income_rate_all += annulized_rate_int;
		}
		to_collect_pricipal_all = to_collect_pricipal_all.setScale(2, BigDecimal.ROUND_DOWN);
		to_collect_pricipal_interest_all = to_collect_pricipal_interest_all.setScale(2, BigDecimal.ROUND_DOWN);

		if (count != 0) {
			average_income_rate = new BigDecimal(new Integer(average_income_rate_all).doubleValue() / count);
		}

		average_income_rate = average_income_rate.setScale(2, BigDecimal.ROUND_DOWN);

		/*
		 * 发生债权转让之前已赚金额可以通过借款人已经还的金额来计算，债权转让之后就不知道怎么算
		 */

		Map<String, Object> financial_account_asset = new HashMap<String, Object>();
		financial_account_asset.put("to_collect_pricipal", to_collect_pricipal_all);// 待收本金
		financial_account_asset.put("to_collect_pricipal_interest", to_collect_pricipal_interest_all); // 待收本息
		financial_account_asset.put("eared_money", "0.00");// 已赚金额
		financial_account_asset.put("average_income_rate", average_income_rate);// 加权平均收益率
		// average_income_rate
		financial_account_asset.put("hold_count", count);// 持有数量

		return financial_account_asset;
	}

	// 用户中心中的借款信息
	public static Map<String, Object> get_borrow_info(int user_id) {
		// 获取借款账户的待还金额

		String sql = "SELECT a.* FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";

		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plans = BorrowerBulkStandardRepaymentPlanM.dao.find(sql, new Object[] { user_id });

		// 查找对应的借款单的借款总金额
		String sql_all_money = "SELECT b.borrow_all_money FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";
		BigDecimal borrow_all_money = Db.queryBigDecimal(sql_all_money, new Object[] { user_id });

		// 查找对应的借款单的借款标题
		String sql_borrow_title = "SELECT b.borrow_title FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";
		String borrow_title = Db.queryString(sql_borrow_title, new Object[] { user_id });

		List<Map<String, Object>> repayment_plans = new ArrayList<Map<String, Object>>();

		BigDecimal yihuan_total = new BigDecimal("0");
		BigDecimal weihuan_total = new BigDecimal("0");

		BigDecimal weihuan_beixi_total = new BigDecimal("0");// 未还的 总的未还本息
		BigDecimal weihuan_normal_manage_total = new BigDecimal("0");// 未还的
		// 总的正常管理费
		BigDecimal weihuan_over_fee = new BigDecimal("0");// 未还的
		// 总的逾期费用(逾期罚息+逾期管理费)

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plans) {
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
			Map<String, Object> repayment_plan = borrower_bulk_standard_repayment_plan.getM();
			repayment_plans.add(repayment_plan);

			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");

			BigDecimal total_total = new BigDecimal("0");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
			// 正常管理费
			BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
			// 逾期罚息
			BigDecimal over_faxi = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
			// 逾期管理费
			BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);

			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_fee = over_faxi.add(over_manage_fee);

			// 应还总额=月还本息+管理费 +逾期费用
			total_total = total_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);

			repayment_plan.put("manage_fee", normal_manage_fee);
			repayment_plan.put("over_total_fee", over_fee);
			repayment_plan.put("total_total", total_total);

			if (is_repay == 0) {// 未还
				weihuan_total = weihuan_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);
				weihuan_beixi_total = weihuan_beixi_total.add(should_repayment_total);
				weihuan_normal_manage_total = weihuan_normal_manage_total.add(normal_manage_fee);
				weihuan_over_fee = weihuan_over_fee.add(over_fee);
			} else if (is_repay == 1) {// 已还
				yihuan_total = yihuan_total.add(should_repayment_total).add(normal_manage_fee).add(over_fee);
			}

		}
		Map<String, Object> borrow_info = new HashMap<String, Object>();

		weihuan_total = weihuan_total.setScale(2, BigDecimal.ROUND_DOWN);
		borrow_info.put("weihuan_total", weihuan_total);

		weihuan_beixi_total = weihuan_beixi_total.setScale(2, BigDecimal.ROUND_DOWN);
		borrow_info.put("weihuan_beixi_total", weihuan_beixi_total);

		weihuan_normal_manage_total = weihuan_normal_manage_total.setScale(2, BigDecimal.ROUND_DOWN);
		borrow_info.put("weihuan_normal_manage_total", weihuan_normal_manage_total);

		weihuan_over_fee = weihuan_over_fee.setScale(2, BigDecimal.ROUND_DOWN);
		borrow_info.put("weihuan_over_fee", weihuan_over_fee);

		borrow_info.put("borrow_title", borrow_title);

		return borrow_info;
	}

	/**
	 * 该债权每一份 每个月 应该获得的本息是多少
	 */
	public static BigDecimal get_monthly_principal_and_interest_per_share(long gather_order_id) {

		String sql = "select borrow_all_share, monthly_principal_and_interest from borrower_bulk_standard_gather_money_order where id = ? ";
		Record record = Db.findFirst(sql, new Object[] { gather_order_id });
		int borrow_all_share = record.getInt("borrow_all_share");
		BigDecimal monthly_principal_and_interest = record.getBigDecimal("monthly_principal_and_interest");

		BigDecimal monthly_principal_and_interest_per_share = monthly_principal_and_interest.divide(new BigDecimal(borrow_all_share), 10, BigDecimal.ROUND_DOWN);

		return monthly_principal_and_interest_per_share;
	}

	/**
	 * 获取联富宝账户资产信息 包括 联富宝加入的①账户资产，②已赚金额，③加权平均收益，④持有数量
	 * 
	 * @return
	 */
	public static Map<String, Object> get_lfoll_fix_bid_account_info(int user_id) {
		// -- 查询账户资产 ,加权平均收益，持有数量
		String sql_1 = "SELECT IFNULL(SUM(bid_money),0)AS total_bid_money,IFNULL(avg(annual_earning),0) AS avg_annual_earning ,COUNT(1) AS hold_amount FROM fix_bid_user_hold t WHERE t.state = 1 and t.user_id = ?";
		Record record = Db.findFirst(sql_1, new Object[] { user_id });
		// -- 查询联富宝已赚金额
		String sql_2 = "SELECT SUM(earned_incom)AS total_earned_incom FROM fix_bid_user_hold t WHERE t.user_id = ? and t.state != 1";
		BigDecimal total_earned_incom = Db.queryBigDecimal(sql_2, new Object[] { user_id });
		// --查询联富宝新手标已赚金额
		String sql_3 = "select earnings FROM fix_bid_user_newer_bid  t where t.user_id = ?";
		BigDecimal lfoll_fix_newer_bid = Db.queryBigDecimal(sql_3, new Object[] { user_id });
		if (lfoll_fix_newer_bid == null) {
			lfoll_fix_newer_bid = new BigDecimal("0.00");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		if (record != null) {

			map.put("total_bid_money", record.getBigDecimal("total_bid_money"));
			map.put("avg_annual_earning", record.getBigDecimal("avg_annual_earning").setScale(2, BigDecimal.ROUND_DOWN));
			map.put("hold_amount", new BigDecimal(record.getLong("hold_amount") + ""));

		} else {
			map.put("total_bid_money", new BigDecimal("0.00"));
			map.put("avg_annual_earning", new BigDecimal("0.00"));
			map.put("hold_amount", new BigDecimal("0"));

		}

		if (total_earned_incom != null) {
			map.put("total_earned_incom", total_earned_incom.add(lfoll_fix_newer_bid).setScale(2, BigDecimal.ROUND_DOWN));// 设置已赚金额
		} else {
			map.put("total_earned_incom", new BigDecimal("0.00").add(lfoll_fix_newer_bid).setScale(2, BigDecimal.ROUND_DOWN));// 设置已赚金额
		}

		return map;
	}

	/***
	 * 判断用户是否有理财未到期，如果有没到期的理财，则返回true,否则返回false.
	 * 
	 * @return
	 */

	public static boolean user_financial_whether_end(int user_id) {
		// 判断有没有联富宝理财未到期
		String lfoll_fix_bid_count_sql = "SELECT COUNT(1) AS total_count FROM fix_bid_user_hold t WHERE t.state = 1";
		Long total_count = Db.queryLong(lfoll_fix_bid_count_sql);

		// 判断是否有债权投资未到期
		Map<String, Object> financial_account_asset = get_elite_bid_account(user_id);
		int hold_count = (Integer) financial_account_asset.get("hold_count");
		if (total_count > 0 || hold_count > 0) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 投买联富宝和投资散标的时候 更改user_info 表的financial_type
	 * 
	 * @param user
	 * @param user_id
	 * @param session
	 * @return
	 */
	public static boolean change_user_financial_type_method(final UserM user, final int user_id, final HttpSession session) {
		// 判断是否有理财
		boolean is_financial = CommonRepayMethod.user_financial_whether_end(user_id);
		int financial_type = user.get("financial_type");
		boolean is_finanacial_ok = false;
		if (is_financial) {
			// 0 没有理财 1理财
			if (financial_type == 0) {
				boolean user_is_save_ok = user.set("financial_type", 1).update();
				session.setAttribute("user", user);
				if (user_is_save_ok) {
					return true;
				} else
					return false;
			} else {
				is_finanacial_ok = true;
			}
		} else {
			// 0 没有理财 1理财
			if (financial_type == 0) {
				boolean user_is_save_ok = user.set("financial_type", 1).update();
				session.setAttribute("user", user);
				if (user_is_save_ok) {
					return true;
				} else
					return false;
			} else {
				is_finanacial_ok = true;
			}
		}
		return is_finanacial_ok;
	}

	public static boolean change_user_financial_type_method(final UserM user, final HttpSession session) {
		// 判断是否有理财
		int financial_type = user.get("financial_type");
		if (financial_type == 0) {
			boolean is_ok_update_user = user.set("financial_type", 1).update();
			if (is_ok_update_user) {
				session.setAttribute(SystemConstantKey.User, user);
			}
			return is_ok_update_user;

		}

		return true;
	}

	/**
	 * 通知系统更新理财用户的Session
	 * 
	 * @param lend_user_id
	 * @return <code>false</code> 如果为false的时候 才表示异常信息出现
	 */
	public static boolean change_user_financial_type_method_with_user_id(int lend_user_id) {
		UserM user = UserM.getUserM(lend_user_id);

		int financial_type = user.get("financial_type");
		if (financial_type == 0) {
			boolean is_ok_update_user = user.set("financial_type", 1).update();

			if (is_ok_update_user) {
				/**
				 * 同时通知系统需要更新
				 */
				UserAutomaticallyBidUserInfoChangeNotifyM.add(lend_user_id);
				return true;
			} else {
				return false;
			}

		} else {
			return true;
		}

	}
}
