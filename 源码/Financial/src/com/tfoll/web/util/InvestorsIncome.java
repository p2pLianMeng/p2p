package com.tfoll.web.util;

import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.model.SysUserInvestorsIncomeConfigM;

import java.math.BigDecimal;
import java.util.List;

/**
 * 首页查询-预期收益
 * 
 */
public class InvestorsIncome {

	public Boolean investors_income() {
		SysUserInvestorsIncomeConfigM sys_user_investors_income_config = new SysUserInvestorsIncomeConfigM();
		BigDecimal profit = get_borrower_bulk_standard_profit().add(get_fix_bid_system_order());
		return sys_user_investors_income_config.set("profit", profit).save();
	}

	public BigDecimal get_borrower_bulk_standard_profit() {
		/**
		 * 只有月份+15+N天
		 */
		List<BorrowerBulkStandardGatherMoneyOrderM> borrower_bulk_standard_gather_money_order_list = BorrowerBulkStandardGatherMoneyOrderM.dao.find("SELECT annulized_rate, borrow_all_money, borrow_duration, borrow_duration_day FROM borrower_bulk_standard_gather_money_order WHERE gather_state = 3");

		BigDecimal borrower_bulk_standard_profit = new BigDecimal("0");// 散标总收益

		for (BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order : borrower_bulk_standard_gather_money_order_list) {
			BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");// 借款金额（管理员可能修改过的）
			int borrow_duration = borrower_bulk_standard_gather_money_order.getInt("borrow_duration");// 借款期限
			int borrow_duration_day = borrower_bulk_standard_gather_money_order.getInt("borrow_duration_day");// 借款期限
			BigDecimal annulized_rate = borrower_bulk_standard_gather_money_order.getBigDecimal("annulized_rate");// 年化利率
			// 全部转为天数
			int all_day = borrow_duration * 30 + borrow_duration_day;
			BigDecimal profit = borrow_all_money.multiply(annulized_rate).multiply(new BigDecimal(all_day + "")).divide(new BigDecimal("366"), 2, BigDecimal.ROUND_DOWN);
			borrower_bulk_standard_profit = borrower_bulk_standard_profit.add(profit);
		}
		return borrower_bulk_standard_profit;
	}

	public BigDecimal get_fix_bid_system_order() {
		/**
		 * 只有月份
		 */
		List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find("SELECT sold_money,annualized_rate,closed_period from fix_bid_system_order");

		BigDecimal fix_bid_system_order_profit = new BigDecimal("0");// 散标总收益

		for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
			int sold_money = fix_bid_system_order.getInt("sold_money");// 借款金额（管理员可能修改过的）
			int closed_period = fix_bid_system_order.getInt("closed_period");// 借款期限
			BigDecimal annualized_rate = fix_bid_system_order.getBigDecimal("annualized_rate");// 年化利率

			BigDecimal profit = (new BigDecimal(sold_money + "")).multiply(annualized_rate).multiply(new BigDecimal(closed_period + "")).divide(new BigDecimal("12"), 2, BigDecimal.ROUND_DOWN);
			fix_bid_system_order_profit = fix_bid_system_order_profit.add(profit);
		}
		return fix_bid_system_order_profit;
	}

}
