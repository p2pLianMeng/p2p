package com.tfoll.web.util;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * 文献参数过滤
 * 
 */
public class RiskParameterFilter {
	/**
	 * sql xss 正则表达式-一般Java系统是不用对sql进行过滤的
	 */
	public static final String Sql_Regex = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|" + "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";
	public static final Pattern Sql_Pattern = Pattern.compile(Sql_Regex, Pattern.CASE_INSENSITIVE);

	/**
	 * 参数校验
	 * 
	 */
	public static boolean validateSqlInjection(String param) {
		if (Utils.isNullOrEmptyString(param)) {
			return false;
		}
		if (Sql_Pattern.matcher(param).find()) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 建立xss过滤启动配置池
	 * 
	 * <pre>
	 * String xss = &quot;(\\b(script|function|http|https|html|htm|jsp|jspx|asp|aspx|do|php|window|location|href|drop|execute)\\b)&quot;;
	 * </pre>
	 */
	static Set<String> Xss_Words = new HashSet<String>();
	static {

		/**
		 * 定义函数
		 */
		Xss_Words.add("script");
		Xss_Words.add("language");
		Xss_Words.add("javascript");
		Xss_Words.add("function");
		Xss_Words.add("self");
		Xss_Words.add("top");
		/**
		 * 远程访问
		 */
		Xss_Words.add("http");
		Xss_Words.add("https");

		Xss_Words.add("html");
		Xss_Words.add("htm");

		Xss_Words.add("jsp");
		Xss_Words.add("jspx");

		Xss_Words.add("asp");
		Xss_Words.add("aspx");

		Xss_Words.add("do");
		Xss_Words.add("action");

		Xss_Words.add("php");
		Xss_Words.add("js");

		/**
		 * 链接跳转
		 */
		Xss_Words.add("window");
		Xss_Words.add("location");
		Xss_Words.add("href");
		Xss_Words.add("navigate");
		Xss_Words.add("replace");
		Xss_Words.add("url");
		Xss_Words.add("go");

		/**
		 * 传送机制
		 */
		Xss_Words.add("email");

		/**
		 * cookie读取
		 */
		Xss_Words.add("document");
		Xss_Words.add("cookie");
		Xss_Words.add("jsessionid");

		/**
		 * 连接远程图片
		 */
		Xss_Words.add("new");
		Xss_Words.add("image");
		Xss_Words.add("img");
		Xss_Words.add("src");

		/**
		 * ajax
		 */
		Xss_Words.add("var");
		Xss_Words.add("ActiveXObject");
		Xss_Words.add("open");
		Xss_Words.add("get");
		Xss_Words.add("post");
		Xss_Words.add("send");

		/**
		 * 调用系统命令
		 */
		Xss_Words.add("WScript.shell");
		Xss_Words.add("Run");
		Xss_Words.add("exe");
		Xss_Words.add("bat");
		Xss_Words.add("cmd");
		Xss_Words.add("dll");
		Xss_Words.add("eval");
		/**
		 * 文件操作
		 */

		Xss_Words.add("Scripting.FileSystemObject");
		Xss_Words.add("GetFile");
		Xss_Words.add("OpenTextFile");
		Xss_Words.add("WriteLine");
		Xss_Words.add("Delete");

		/**
		 * 触发事件:一般事件 页面相关事件 表单相关事件 滚动字幕事件 滚动字幕事件 编辑事件 数据绑定 外部事件
		 */
		// 一般事件
		Xss_Words.add("onclick");
		Xss_Words.add("ondblclick");
		Xss_Words.add("onmousedown");
		Xss_Words.add("onmouseup");
		Xss_Words.add("onmouseover");
		Xss_Words.add("onmousemove");
		Xss_Words.add("onmouseout");
		Xss_Words.add("onkeypress");
		Xss_Words.add("onkeydown");
		Xss_Words.add("onkeyup");

		// 页面相关事件
		Xss_Words.add("onbeforeunload");
		Xss_Words.add("onerror");
		Xss_Words.add("onload");
		Xss_Words.add("onmove");
		Xss_Words.add("onresize");
		Xss_Words.add("onscroll");
		Xss_Words.add("onstop");
		Xss_Words.add("onunload");

		// 表单相关事件
		Xss_Words.add("onchange");
		Xss_Words.add("onfocus");
		Xss_Words.add("onreset");
		Xss_Words.add("onsubmit");

		// 滚动字幕事件
		Xss_Words.add("onfinish");
		Xss_Words.add("onstart");

		// 编辑事件
		Xss_Words.add("onbeforecut");
		Xss_Words.add("onbeforeeditfocus");
		Xss_Words.add("onbeforepaste");
		Xss_Words.add("onbeforeupdate");
		Xss_Words.add("oncontextmenu");
		Xss_Words.add("oncopy");
		Xss_Words.add("oncut");
		Xss_Words.add("ondrag");
		Xss_Words.add("ondragdrop");
		Xss_Words.add("ondragend");
		Xss_Words.add("ondragenter");
		Xss_Words.add("ondragleave");
		Xss_Words.add("ondragover");
		Xss_Words.add("ondragstart");
		Xss_Words.add("ondrop");
		Xss_Words.add("onlosecapture");
		Xss_Words.add("onpaste");
		Xss_Words.add("onselect");
		Xss_Words.add("onselectstart");

		// 数据绑定
		Xss_Words.add("oncellchange");
		Xss_Words.add("ondataavailable");
		Xss_Words.add("ondatasetchanged");
		Xss_Words.add("ondatasetcomplete");
		Xss_Words.add("onerrorupdate");
		Xss_Words.add("onrowenter");
		Xss_Words.add("onrowexit");
		Xss_Words.add("onrowsdelete");
		Xss_Words.add("onrowsinserted");

		// 外部事件
		Xss_Words.add("onbeforeprint");
		Xss_Words.add("onfilterchange");
		Xss_Words.add("onhelp");
		Xss_Words.add("onpropertychange");
		Xss_Words.add("onreadystatechange");

		// 特别的几个事件
		Xss_Words.add("event");
		Xss_Words.add("keyCode");
		Xss_Words.add("status");
		Xss_Words.add("clipboardData");
		Xss_Words.add("getData");
		Xss_Words.add("setData");

	}

	/**
	 * 参数校验
	 * 
	 */
	public static boolean validateXss(String param) {
		if (Utils.isNullOrEmptyString(param)) {
			return false;
		}
		for (String key : Xss_Words) {
			boolean is_contains = param.contains(key);
			if (is_contains) {
				return true;
			}
			break;

		}

		return false;

	}

}
