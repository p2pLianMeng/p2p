package com.tfoll.web.util;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.web.domain.FinancialRecipientInformation;
import com.tfoll.web.model.SysNotificationM;
import com.tfoll.web.model.UserM;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * 还款相关的Key对应代码
 * 
 * <pre>
 * 短信引擎 key
 * 13 收到还款
 * private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType13(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {}
 * 14 还款成功
 * private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType14(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {}
 * 15 提前还款成功
 * private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType15(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {}
 * 16 严重逾期通知
 * private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType16(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {}
 *</pre>
 * 
 * <pre>
 * 邮件引擎 key
 * 8回款成功
 * private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType8(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {}
 * 9还款成功
 * private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType9(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {}
 * 10提前还款成功
 * private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType10(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {}
 * 11严重逾期通知
 * private static void doSubTaskForUserEmailRequestWaitingKeyByRequestType11(UserEmailRequestWaitingM user_email_request_waiting) throws Exception {}
 *</pre>
 */
public class SendBackTheSuccessfulInformation {

	public static final Logger logger = Logger.getLogger(SendBackTheSuccessfulInformation.class);

	/**
	 * 发送回款成功信息
	 * 
	 * @param has_pay_repayment_plan_id_list
	 * @param financial_recipient_information_list_map
	 */
	public static boolean send_back_the_successful_information(List<Long> has_pay_repayment_plan_id_list, Map<Long, List<FinancialRecipientInformation>> financial_recipient_information_list_map) {
		if (!Utils.isHasData(has_pay_repayment_plan_id_list)) {
			throw new NullPointerException("has_pay_repayment_plan_id_list is null ");
		}
		if (financial_recipient_information_list_map == null) {
			throw new NullPointerException("financial_recipient_information_list_map is null ");
		}
		if (financial_recipient_information_list_map.size() == 0) {
			throw new NullPointerException("financial_recipient_information_list_map.size()==0 ");
		}
		/**
		 * 依次从map里面检查数据是否存在-financial_recipient_information_list_map.size()
		 * 可以大于等于has_pay_repayment_plan_id_list.size()
		 */
		for (Long has_pay_repayment_plan_id : has_pay_repayment_plan_id_list) {
			if (!financial_recipient_information_list_map.containsKey(has_pay_repayment_plan_id)) {
				throw new NullPointerException("!financial_recipient_information_list_map.containsKey(has_pay_repayment_plan_id) ");
			}
		}
		/**
		 * 担心报错-如果报错则会对还款人有影响
		 */
		try {
			return send_back_the_successful_information_with_can_throw_exception(has_pay_repayment_plan_id_list, financial_recipient_information_list_map);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return false;
		}

	}

	/**
	 * false表示出错了-这个意思-如果出错了需要管理员进行手动的发送-需要记录到系统错误里面-让管理员进行补齐回收信息
	 */
	private static boolean send_back_the_successful_information_with_can_throw_exception(List<Long> has_pay_repayment_plan_id_list, Map<Long, List<FinancialRecipientInformation>> financial_recipient_information_list_map) {
		/**
		 * 在出错的情况下面has_pay_repayment_plan_id_list的元素比financial_recipient_information_list_map少一个
		 */
		for (Long has_pay_repayment_plan_id : has_pay_repayment_plan_id_list) {
			List<FinancialRecipientInformation> financial_recipient_information_list = financial_recipient_information_list_map.get(has_pay_repayment_plan_id);
			/**
			 * 把里面的信息进行封装-然后发送回款消息给理财人
			 */
			for (FinancialRecipientInformation financial_recipient_information : financial_recipient_information_list) {// 有序处理

				try {
					send_back_the_successful_information_to_financial(financial_recipient_information);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(e.getCause().getMessage());
				}

			}
		}

		return true;

	}

	private static boolean send_back_the_successful_information_to_financial(FinancialRecipientInformation financial_recipient_information) {
		int user_id = financial_recipient_information.getUser_id();
		/**
		 * 同时发短信和邮箱
		 */
		UserM user = UserM.getUserM(user_id);
		if (user == null) {
			return true;
		} else {
			String phone = user.getString("phone");// 不能为空
			if (Utils.isNotNullAndNotEmptyString(phone)) {
				phone = phone.trim();
				/**
				 * 如果出现异常则不允许报错-不能影响主要的程序
				 * 
				 * <pre>
				 * 尊敬的用户，您于{1(收款时间)}收到一笔还款,正常本息￥{2（金额）}（共{3（期数 比如第一期 总共有18期（1/18））}）,逾期费用￥{4（逾期费用）}。感谢您对我们的关注与支持。          收到还款
				 * </pre>
				 */
				try {
					String now = Model.Date.format(new Date());
					StringBuilder sb = new StringBuilder();
					sb.append(now).append("#").//
							append(financial_recipient_information.getPrincipal_interest().toString()).append("#").//
							append(financial_recipient_information.getCurrent_period() + "/" + financial_recipient_information.getTotal_periods()).append("#").//
							append(financial_recipient_information.getPunish_interest().toString());//

					SysNotificationM.sys_send_message(phone, sb.toString(), 13);// 理财人收到回款

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			String email = user.getString("email");// 可以为空
			if (Utils.isNotNullAndNotEmptyString(email)) {
				email = email.trim();
				/**
				 * 如果出现异常则不允许报错-不能影响主要的程序
				 */
				try {
					/**
					 * <pre>
					 * "  <h2>尊敬的用户" + nickname1 + "：您好:<br /><br />" + "感谢您对用户"+nickname2+"发布的借款"+borrow_title+"的支持与帮助。</br>您于"+date+"收到来自该用于的一笔还款。正常本息￥"+principal_interest+"(共"+qishu+")。"+"逾期费用￥"+ late_fee+"。</br>点击<a href=\"http://www.lfoll.com/user/usercenter/into_user_center.html\">这里</a>查看您的账户余额。</br>感谢您对我们的关注与支持。</br>"+ "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
					 * </pre>
					 */

					String nickname1 = Db.queryString("SELECT nickname from user_info WHERE id=?", new Object[] { financial_recipient_information.getUser_id() });
					String nickname2 = Db.queryString("SELECT nickname from user_info WHERE id=?", new Object[] { financial_recipient_information.getUser_id_of_borrower() });
					// String email = "";
					String borrow_title = financial_recipient_information.getBorrow_title();
					String date = Model.Date.format(new Date());
					String principal_interest = financial_recipient_information.getPrincipal_interest().toString();
					String qishu = financial_recipient_information.getCurrent_period() + "/" + financial_recipient_information.getTotal_periods();
					String late_fee = financial_recipient_information.getPunish_interest().toString();
					//
					StringBuilder sb = new StringBuilder(100);
					sb.append(nickname1).append("#").//
							append(nickname2).append("#").//
							append(email).append("#").//
							append(borrow_title).append("#").//
							append(date).append("#").//
							append(principal_interest).append("#").//
							append(qishu).append("#").//
							append(late_fee);//

					SysNotificationM.send_sys_notification(0, user_id, "回款通知", sb.toString(), phone, true, 8, email);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return true;

	}

	/**
	 * 一次性还款，系统提前还款
	 */
	public static void send_back_the_successful_information_of_all_with_overdue(List<FinancialRecipientInformation> financial_recipient_information_list) {
		if (Utils.isHasData(financial_recipient_information_list)) {
			for (FinancialRecipientInformation financial_recipient_information : financial_recipient_information_list) {
				try {
					send_back_the_successful_information_to_financial_of_all_with_overdue(financial_recipient_information);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();

				}
			}

		}

	}

	private static boolean send_back_the_successful_information_to_financial_of_all_with_overdue(FinancialRecipientInformation financial_recipient_information) {
		int user_id = financial_recipient_information.getUser_id();
		/**
		 * 同时发短信和邮箱
		 */
		UserM user = UserM.getUserForSelect(user_id);
		if (user == null) {
			return true;
		} else {
			String phone = user.getString("phone");// 不能为空
			if (Utils.isNotNullAndNotEmptyString(phone)) {
				phone = phone.trim();
				/**
				 * 如果出现异常则不允许报错-不能影响主要的程序
				 * 
				 * <pre>
				 * 尊敬的用户，您于{1(收款时间)}收到一笔还款,正常本息￥{2（金额）}（共{3（期数 比如第一期 总共有18期（1/18））}）,逾期费用￥{4（逾期费用）}。感谢您对我们的关注与支持。          收到还款
				 * </pre>
				 */
				try {
					String now = Model.Date.format(new Date());
					StringBuilder sb = new StringBuilder();
					sb.append(now).append("#").//
							append(financial_recipient_information.getPrincipal_interest().toString()).append("#").//
							append(financial_recipient_information.getTotal_periods() + "/" + financial_recipient_information.getTotal_periods()).append("#").//
							append(financial_recipient_information.getPunish_interest().toString());//
					// 提前还款
					SysNotificationM.sys_send_message(phone, sb.toString(), 13);// 理财人收到回款

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			String email = user.getString("email");// 可以为空
			if (Utils.isNotNullAndNotEmptyString(email)) {
				email = email.trim();
				/**
				 * 如果出现异常则不允许报错-不能影响主要的程序
				 */
				try {
					/**
					 * <pre>
					 * "  <h2>尊敬的用户" + nickname1 + "：您好:<br /><br />" + "感谢您对用户"+nickname2+"发布的借款"+borrow_title+"的支持与帮助。</br>您于"+date+"收到来自该用于的一笔还款。正常本息￥"+principal_interest+"(共"+qishu+")。"+"逾期费用￥"+ late_fee+"。</br>点击<a href=\"http://www.lfoll.com/user/usercenter/into_user_center.html\">这里</a>查看您的账户余额。</br>感谢您对我们的关注与支持。</br>"+ "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
					 * </pre>
					 */

					String nickname1 = Db.queryString("SELECT nickname from user_info WHERE id=?", new Object[] { financial_recipient_information.getUser_id() });
					String nickname2 = Db.queryString("SELECT nickname from user_info WHERE id=?", new Object[] { financial_recipient_information.getUser_id_of_borrower() });
					// String email = "";
					String borrow_title = financial_recipient_information.getBorrow_title();
					String date = Model.Date.format(new Date());
					String principal_interest = financial_recipient_information.getPrincipal_interest().toString();
					String qishu = financial_recipient_information.getTotal_periods() + "/" + financial_recipient_information.getTotal_periods();
					String late_fee = financial_recipient_information.getPunish_interest().toString();
					//
					StringBuilder sb = new StringBuilder(100);
					sb.append(nickname1).append("#").//
							append(nickname2).append("#").//
							append(email).append("#").//
							append(borrow_title).append("#").//
							append(date).append("#").//
							append(principal_interest).append("#").//
							append(qishu).append("#").//
							append(late_fee);//

					// 提前还款
					SysNotificationM.send_sys_notification(0, user_id, "回款通知", sb.toString(), phone, true, 10, email);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return true;

	}

}
