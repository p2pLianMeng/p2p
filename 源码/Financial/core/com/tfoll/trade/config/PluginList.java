package com.tfoll.trade.config;

import com.tfoll.trade.plugin.IPlugin;

import java.util.ArrayList;
import java.util.List;

public class PluginList {

	private static List<com.tfoll.trade.plugin.IPlugin> pluginList = new ArrayList<IPlugin>();

	public PluginList add(IPlugin plugin) {
		if (plugin != null) {
			PluginList.pluginList.add(plugin);
		}
		return this;
	}

	public List<IPlugin> getPluginList() {
		return pluginList;
	}
}
