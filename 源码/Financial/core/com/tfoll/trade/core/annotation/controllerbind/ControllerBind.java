package com.tfoll.trade.core.annotation.controllerbind;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE })
public @interface ControllerBind {
	public String controllerKey();

	public String viewPath() default "";

}
