package com.tfoll.tcache.template;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * TCache缓存上下文,记录缓存的开始的时间,失效时间
 */
public class CacheContext {
	public static final ConcurrentHashMap<String, CacheContext> Cache_Context_Map = new ConcurrentHashMap<String, CacheContext>(1000);

	public static final long Second = 1000;
	public static final long Minute = Second * 60;
	public static final long Hour = Minute * 60;
	public static final long Day = Hour * 24;

	private long start_time_long_or_update_cache_time = 0;
	private long lose_effectiveness_interval = 0;
	/**
	 * 支持读写锁
	 */
	private ReadWriteLock lock = new ReentrantReadWriteLock();
	private Object object = null;

	/**
	 * 
	 * @param loseEffectivenessInterval
	 *            失效时间间隔单位数
	 * @param timeType
	 *            时间类型
	 */
	public CacheContext(long loseEffectivenessInterval, long timeType) {
		this.lose_effectiveness_interval = loseEffectivenessInterval * timeType;
	}

	public long getStart_time_long_or_update_cache_time() {
		return start_time_long_or_update_cache_time;
	}

	public void setStart_time_long_or_update_cache_time(long startTimeLongOrUpdateCacheTime) {
		start_time_long_or_update_cache_time = startTimeLongOrUpdateCacheTime;
	}

	public long getLose_effectiveness_interval() {
		return lose_effectiveness_interval;
	}

	public void setLose_effectiveness_interval(long loseEffectivenessInterval) {
		lose_effectiveness_interval = loseEffectivenessInterval;
	}

	public ReadWriteLock getLock() {
		return lock;
	}

	public void setLock(ReadWriteLock lock) {
		this.lock = lock;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Object getObject() {
		return object;
	}

}
