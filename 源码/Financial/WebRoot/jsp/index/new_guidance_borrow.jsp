<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<title>${applicationScope.title}</title>

		<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description"
			content="${applicationScope.description}">
	</head>

	<body>
		<%@ include file="/jsp/index/index_top.jsp"%>

		<span id="ny_pic"></span>


		<div class="zhiyin_con zhiyin_2">
			<h1>
				新手指引
			</h1>
			<div class="zhiyin">
				<h2>
					<a href="<%=__ROOT_PATH__%>/index/newguidance/invest.html">我要理财</a>
					<a class="hover"
						href="<%=__ROOT_PATH__%>/index/newguidance/borrow.html">我要借款</a>
					<a href="<%=__ROOT_PATH__%>/index/newguidance/security.html">安全保障</a>
				</h2>
				<ul>
					<ul>
						<li class="zhiyin_list1">
							<h6 class="zhiyin_list1_h6">
								什么是联富金融
							</h6>
							<img src="<%=__PUBLIC__%>/images/zhiyin_pic1.png" />
							<p>
								联富金融(lfoll.com)是目前中国互联网金融中P2P信贷行业的领军企业，是一个以个人对个人小额借贷为主要产品，为借贷两端搭建的公平、透明、安全、高效的互联网金融服务平台。
								<br />
								借款用户可以在联富金融上获得信用评级、发布借款请求来实现个人快捷的融资需要；理财用户可以把自己的部分闲余资金通过联富金融平台出借给信用良好有资金需求的个人，在获得有保障，高收益的理财回报的同时帮助了优质的借款人。
							</p>
						</li>
						<li class="zhiyin_list2">
							<h6 class="zhiyin_list2_h6">
								为什么选择联富金融
							</h6>
							<p class="a">
								<img src="<%=__PUBLIC__%>/images/zhiyin_pic7.png" />
								<span><s>无须任何抵押、担保，低门槛，纯信用借款</s>联富金融为借款人提供纯信用借款，无须任何抵押或担保，只需要提供必要申请材料并通过审核，即可获得最高50万的借款额度。<s>用户自主选择产品</s>联富金融面向各类借款用户，设计了工薪贷、生意贷两种产品，用户可以根据自身情况选择适合的借款产品。</span>
							</p>
							<p class="b">
								<span><s>全程在线操作，最便捷的借款方式</s>
									无需出门，提交材料、审核、放款全程互联网操作，键盘加鼠标轻松搞定，7*24的互联网金融服务。 <s>最安全的平台保障</s>
									我们采用业界最先进的加密技术，对您的注册信息、账户信息进行加密处理，联富金融绝不会以任何形式将这些信息透露给第三方。</span>
								<img src="<%=__PUBLIC__%>/images/zhiyin_pic8.png" />
							</p>
						</li>
						<li class="zhiyin_list3">
							<h6 class="zhiyin_list3_h6">
								联富金融的业绩
							</h6>
							<p>
								<span>总交易金额达到3000万元</span> 累计总交易额达到3000万元
								<span>累计交易1万人次</span> 网站从开展业务到现在交易次数达到1万人次
								<span>借款用户平均借款用时2.89天<br /> 服务遍及全国2,000多个城市</span>
							</p>
							<img src="<%=__PUBLIC__%>/images/zhiyin_pic4.png" />
						</li>
						<li class="zhiyin_list4">
							<h6 class="zhiyin_list4_h6">
								借款，从未如此轻松
							</h6>
							<img src="<%=__PUBLIC__%>/images/zhiyin_pic9.png" />
							<p>
								<span>轻松借款，只需4步</span> 第一步 注册成为联富金融用户，完成身份认证
								<br />
								第二步 选择借款产品发起申请
								<br />
								第三步 上传必要申请材料
								<br />
								第四步 通过审核后开始筹标
								<br />
								满标后，借款完成立刻放款，之后您只需每月还款即可
								<br />
								<a href="<%=__ROOT_PATH__%>/user/financial/borrow/loan_applay.html?borrow_type=1">我要去借款</a>
							</p>
						</li>
						<li class="zhiyin_list5">
							<h6 class="zhiyin_list5_h6">
								产品介绍
							</h6>
							<p>
								<a href="#">消费贷</a>
								加入联富宝，资金以用户认可的既定规则自动进行投标，用户每月的回款也会循环再投资，更安全，更便捷，更高效。
								<a href="#">生意贷</a>
								平台提供信用认证标、机构担保标、实地认证标等多类产品，用户可以根据借款人信用等级、利率、期限等信息，自选合适的借款标的，构建符合个人意愿的投资组合。
							</p>
							<img src="<%=__PUBLIC__%>/images/zhiyin_pic10.png" />
						</li>
					</ul>
			</div>
		</div>

		<%@ include file="/jsp/index/index_foot.jsp"%>

	</body>
</html>
