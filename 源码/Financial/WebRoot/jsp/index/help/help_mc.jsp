﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a style="background:#ed5050;color:#fff;" href="#"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>A - G</div>
             
             <div onClick="showsubmenu(6)" id="6" class="district"><s>&bull;</s><span>垫付</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none">
                指严重逾期的债权根据联富金融本金保障计划，由联富金融风险备用金代为偿还的动作，垫付完成后债权转移至联富金融名下。
             </div>
             
             <div onClick="showsubmenu(7)" id="7" class="district"><s>&bull;</s><span>等额本息</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<br />
                具体计算公式如下：<br />
                <img src="<%= __PUBLIC__%>/images/help_2.png" /><br />
                P：每月还款额<br />
                A:借款本金<br />
                b:月利率<br />
                n:还款总期数<br />
                因计算中存在四舍五入，最后一期还款金额与之前略有不同。<br />
                您可以点击<a href="<%=__ROOT_PATH__%>/user/financial/financial/to_calculator_borrower.html">借款计算器</a>设置并计算，查看每月还款明细。
             </div>
             
             <div class="down_up"><span>&gt;</span>H - N</div>
             
             <div onClick="showsubmenu(8)" id="8" class="district"><s>&bull;</s><span>借款用户（借款人）</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                已经或准备在网站上进行借款活动的用户称为借款用户。凡22-55周岁以上的中国大陆地区公民，都可以成为借款用户。
             </div>
             
             <div onClick="showsubmenu(9)" id="9" class="district"><s>&bull;</s><span>加权平均利率</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none">
                由于借款时间不定，联富金融采用加权平均的方式计算借出者的收益率。<br />
                具体计算公式如下：<br />
                <img src="<%= __PUBLIC__%>/images/help_19.png" /><br />
                注：此公式未计算可能的资金闲置期。
             </div>
             
             <div onClick="showsubmenu(10)" id="10" class="district"><s>&bull;</s><span>机构担保标</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                机构担保标是指联富金融的合作伙伴为相应的借款承担连带保证责任的借款标的。所谓连带保证责任即连带保证人对债务人负连带责任，无论主债务人的财产是否能够清偿债务，债权人均有权要求保证人履行保证义务。<br />
                针对机构担保标借款申请人，联富金融会通过严格的审核系统进行双重审核，严控风险。此外，一旦合作伙伴违背其应承担的连带保证责任，根据合作协议联富金融有权通过法律手段进行追偿。
             </div>
             
             <div onClick="showsubmenu(11)" id="11" class="district"><s>&bull;</s><span>借款服务费</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                联富金融收取借款本金的一定比例作为借款服务费，将全部存于风险备用金账户用于联富金融的本金保障计划。服务费将按照借款人的信用等级来收取：<br />
                <table class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="16%">信用等级</td>
                      <td width="12%"><span style="background-color:#e62129;">A+</span></td>
                      <td width="12%"><span style="background-color:#ff5a08;">A</span></td>
                      <td width="12%"><span style="background-color:#ffcc30;">B+</span></td>
                      <td width="12%"><span style="background-color:#7ad84c;">B</span></td>
                      <td width="12%"><span style="background-color:#2ec6f5;">C+</span></td>
                      <td width="12%"><span style="background-color:#2a62f7;">C</span></td>
                   </tr>
                   <tr>
                      <td width="16%">服务费率</td>
                      <td width="12%">0%</td>
                      <td width="12%">1%</td>
                      <td width="12%">2%</td>
                      <td width="12%">3%</td>
                      <td width="12%">4%</td>
                      <td width="12%">5%</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(12)" id="12" class="district"><s>&bull;</s><span>借款管理费</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                联富金融将按照借款人的借款期限，每月向借款人收取其借款本金的0.3%作为借款管理费。                
             </div>
                          
             <div onClick="showsubmenu(13)" id="13" class="district"><s>&bull;</s><span>借款利率</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                借款利率是指借款申请人为了获得借款而愿意支付给理财人的年化利率。请注意，在目前的等额本息、每月还款的还款方式下，用户实际支付的年利息低于借款本金x年化利率。
                您可以点击<a href="<%=__ROOT_PATH__%>/user/financial/financial/to_calculator_borrower.html">借款计算器</a>设置并计算，查看每月还款明细。
                相关阅读：<a href="#">联富金融年利率范围</a>
             </div>
             
             <div onClick="showsubmenu(14)" id="14" class="district"><s>&bull;</s><span>理财用户（理财人）</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu14" class="area" style="display:none;">
                已经或准备在网站上进行资金出借活动并完成了实名认证、手机号码绑定和提现密码设置的用户称为理财用户。
             </div>
             
             <div onClick="showsubmenu(15)" id="15" name="15" class="district"><s>&bull;</s><span>年化利率</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu15" class="area" style="display:none;">
                年化利率是把真实利率换算成以年为单位的利率。
             </div>
             
             <div onClick="showsubmenu(16)" id="16" class="district"><s>&bull;</s><span>年化收益率</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu16" class="area" style="display:none;">
                指投资人对应一年投资所获的收益率。
             </div>
             
             <div class="down_up"><span>&gt;</span>O - T</div>
             
             <div onClick="showsubmenu(17)" id="17" class="district"><s>&bull;</s><span>散标</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu17" class="area" style="display:none;">
                散标是信用认证标、机构担保标、实地认证标的统称。
             </div>
             
             <div onClick="showsubmenu(18)" id="18" class="district"><s>&bull;</s><span>实地认证标</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu18" class="area" style="display:none;">
                实地认证标是联富金融与友众信业金融信息服务（上海）有限公司（以下简称“友信”）共同推出的一款全新产品。该产品延续了联富金融求真务实的经营理念，在原有严格审核的基础上，增加了友信前端工作人员对借款人情况的实地走访、审核调查以及后续的贷中、贷后服务环节，进一步加强风险管理控制，达到了双重保障的效果。<br />
                实地认证标相对信用认证标增添了实地认证审核，进一步保障了理财用户资金安全；同时采用本息保障的赔付方式，使得您理财更加省心、放心。
             </div>             
             
             <div onClick="showsubmenu(19)" id="19" class="district"><s>&bull;</s><span>锁定期</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu19" class="area" style="display:none;">
                联富宝具有锁定期限制。锁定期内，您可操作提前退出，但会产生相应费用，提前退出费用=加入计划金额*2.0%。锁定期满后自动退出。
             </div>
             
             <div class="down_up"><span>&gt;</span>U - Z</div>
             
             <div onClick="showsubmenu(20)" id="20" class="district"><s>&bull;</s><span>信用认证标</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu20" class="area" style="display:none;">
                信用认证标是联富金融通过对借款用户的个人信用资质进行全面审核后，允许用户发布的借款标。
             </div>
             
             <div onClick="showsubmenu(21)" id="21" class="district"><s>&bull;</s><span>信用认证</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                申请借款的用户需要根据不同的产品提交相应的信用认证材料，经过联富金融审核后获取相应的信用等级及借款额度。所需提供的认证资料分为必要认证资料和可选认证资料。<br />
                相关阅读：<a href="#">必要认证资料和可选认证资料有哪些？</a>
             </div>
             
             <div onClick="showsubmenu(22)" id="22" class="district"><s>&bull;</s><span>信用报告</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                个人信用报告是由中国人民银行出具，全面记录个人信用活动，反映个人信用基本状况的文件。本报告是联富金融了解用户信用状况的一个重要参考资料。<a href="#">查看详情>></a>
             </div>
             
             <div onClick="showsubmenu(23)" id="23" class="district"><s>&bull;</s><span>信用审核</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                信用审核是指：申请借款的用户根据不同的产品填写借款信息，包括个人信息、家庭信息、工作信息、资产信息、信用信息，并提交相应的信用认证材料，随后信审部门综合评估借款人的个人、家庭、工作、资产、信用情况，最终根据借款人的整体信用资质给出相应的信用分数、信用等级及借款额度。
             </div>
             
             <div onClick="showsubmenu(24)" id="24" class="district"><s>&bull;</s><span>信用等级（分数）</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none;">
                用户的信用分数是在通过联富金融审核后获得的，信用等级由信用分数转化而来，每个信用等级都有对应的信用分数范围。信用分数和信用等级是借款人的信用属性，也是理财人判断借款人违约风险的重要依据之一。通常来讲借款人信用等级越高，其违约率越低，相应的借款成功率越高。<br />
                目前信用等级由高到低分为A+、A、B+、B、C+、C，具体请参考以下信用等级的分数区间。<br />
                <table class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="16%">信用等级</td>
                      <td width="12%"><span style="background: #e62129;">A+</span></td>
                      <td width="12%"><span style="background:#ff5a08;">A</span></td>
                      <td width="12%"><span style="background: #ffcc30;">B+</span></td>
                      <td width="12%"><span style="background: #7ad84c;">B</span></td>
                      <td width="12%"><span style="background: #2ec6f5;">C+</span></td>
                      <td width="12%"><span style="background: #2a62f7;">C</span></td>
                   </tr>
                   <tr>
                      <td width="16%">分数区间</td>
                      <td width="12%">150以上</td>
                      <td width="12%">135-149</td>
                      <td width="12%">120-134</td>
                      <td width="12%">105-119</td>
                      <td width="12%">90-104</td>
                      <td width="12%">0-89</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(25)" id="25" class="district"><s>&bull;</s><span>信用额度</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                用户的信用额度是在通过联富金融审核员对所提供材料的审核后获得的，既是借款人单笔借款的上限也是借款者累积尚未还清借款的上限。<br />
                例如，如果一个借款人信用额度为5万元，则在没有其他借款的情况下，用户可以发布总额最高为5万元的借款请求。也可以分多次发布借款请求，但尚未还清借款（以整笔借款金额计算）的总额不得超过5万元。
             </div>
             
             <div onClick="showsubmenu(26)" id ="26" class="district"><s>&bull;</s><span>原始投资额</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                指投标时理财用户的投标金额。
             </div>
                          
             <div onClick="showsubmenu(27)" id="27" class="district"><s>&bull;</s><span>逾期</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu27" class="area" style="display:none;">
                指借款用户未按协议约定时间进行足额还款，此时标的状态为逾期。<br />
                相关阅读： 如果逾期还款，会有什么惩罚？
             </div>
             
             <div onClick="showsubmenu(28)" id="28" class="district"><s>&bull;</s><span>严重逾期</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                借款用户逾期时间超过31天时，从第31天开始该标的状态为严重逾期。<br />
                相关阅读：<a href="#">如果逾期还款，会有什么惩罚？</a>
             </div>
             
             <div onClick="showsubmenu(29)" id="29" class="district"><s>&bull;</s><span>联富宝</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                联富宝是联富金融推出的便捷高效的自动投标工具。联富宝在用户认可的标的范围内，对符合要求的标的进行自动投标，且回款本金在相应期限内自动复投，期限结束后联富宝会通过联富金融债权转让平台进行转让退出。该计划所对应的标的均100%适用于联富金融本金保障计划并由系统实现标的分散投资。出借所获利息收益可选择每月复投或提取，更好的满足用户多样化的理财需求。<br />
                理财人加入联富宝后，会进入锁定期，锁定期内，投资的回款本金将继续进行投资直到锁定期结束，充分提高资金利用效率。<br />
                锁定期结束后，理财人自动退出联富宝，理财人在该计划内投资的债权将优先进行债权转让。债权转让所得资金及投资回款所得等将不再继续自动投资，系统将在指定时间将此资金转移至用户的主账户供用户自行支配。<br />
                理财人的所有投资均适用于平台的本金保障计划，并且具有风险低、收益稳定、信息透明度高等特点。
             </div>
             
             <div onClick="showsubmenu(30)" id="30" class="district"><s>&bull;</s><span>债权</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu30" class="area" style="display:none;">
                指理财用户与借款用户之间的债务约定。
             </div>
             
             <div onClick="showsubmenu(31)" id="31" class="district"><s>&bull;</s><span>债权价值</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu31" class="area" style="display:none;">
                指根据定价公式计算出的债权当前的公允价值，具体计算公式如下：<br />
                通常算法：债权出售日待收本金与当日距离上一期还款的天数所对应的利息之和。<br />
                <h1>（1）转让时，当期还款处于未还款状态</h1>
                <img src="<%= __PUBLIC__%>/images/help_5.png" /><br />
                F：债权公允价值<br />
                D:应计利息天数<br />
                b:月利率<br />
                x:产品折让参数, 本息保障类产品为1，本金保障类产品为0<br />
                <img src="<%= __PUBLIC__%>/images/help_6.png" /><br />
                产品折让参数：本息保障类产品为1，本金保障类产品为0<br />
                <h1>（2）转让时，当期还款处于已还款状态，但下一期处于未还款状态</h1>
                <img src="<%= __PUBLIC__%>/images/help_7.png" /><br />
                F：债权公允价值<br />
                b:月利率<br />
                <img src="<%= __PUBLIC__%>/images/help_8.png" /><br />
                <h1>（3）转让时，当期还款处于已还款状态，且下面的N期处于已还款状态，但还未完全还清</h1>
                <img src="<%= __PUBLIC__%>/images/help_9.png" /><br />
                F：债权公允价值<br />
                b:月利率<br />
                <img src="<%= __PUBLIC__%>/images/help_8.png" /><br />
                t：最后还款所在期数与成交日期所在期数之差                
             </div>
             
             <div onClick="showsubmenu(32)" id="32" class="district"><s>&bull;</s><span>债权转让</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu32" class="area" style="display:none;">
                指债权持有人通过联富金融债权转让平台将债权挂出且与购买人签订债权转让协议，将所持有的债权转让给购买人的操作。<a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr_js.html">查看详情>></a>
             </div>
             
             <div onClick="showsubmenu(33)" id="33" class="district"><s>&bull;</s><span>债权价格</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu33" class="area" style="display:none;">
                指债权转让时的价格。
             </div>
             
             <div onClick="showsubmenu(34)" id="34" class="district"><s>&bull;</s><span>债权份数</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu34" class="area" style="display:none;">
                债权的原始投资金额每50元为一份。
             </div>
               
          </div>
          
          
       </div>
    </div>
    
  </div>    
</div>
<input type="hidden" value="${type }" name="type" id="type"/>

<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<<script type="text/javascript">
	window.scrollTo(0,100)
window.scrollTo(0,document.body.scrollHeight/2)

	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>
