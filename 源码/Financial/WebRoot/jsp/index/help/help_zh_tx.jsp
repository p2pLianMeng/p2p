﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>


<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html" style="background:#f8bbb2;color:#fff;"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html">登录注册</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html">账户密码</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html">充值</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html">提现</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html">安全认证</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html">消息中心</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
          
             <div class="down_up"><span>&gt;</span>提现</div>
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>申请提现后，多久能到账？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                联富金融收到用户提现申请后即对提现进行转账操作，由于不同银行处理速度不同，提现资金将会当天或下个工作日到账（如遇双休日或法定节假日顺延）。如果用户迟迟未收到提现资金，可能为银行卡信息填写有误，银行做正在退票操作，预计会在7个工作日内完成退票，请用户耐心等候。用户还可以联系客服（4006-888-923），寻求帮助。
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>提现手续费是多少？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none">
                手续费将作为第三方平台的转账费用。第三方支付平台将按以下标准收取相关费用。<br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="20%">金额</td>
                      <td width="20%">2万元以下</td>
                      <td width="20%">2万(含)-5万元</td>
                      <td width="40%">5万(含)-100万元</td>
                   </tr>
                   <tr>
                      <td width="20%">手续费</td>
                      <td width="20%">1元/笔</td>
                      <td width="20%">3元/笔</td>
                      <td width="40%">5元/笔</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(25)" class="district"><s>&bull;</s><span>如何提现？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                您可以随时将您在联富金融账户中的可用余额申请提现到您现有的任何一家银行的账号上。<a href="#">查看操作流程>></a><br />
                注意：请提供申请提现的银行卡账号，并确保该账号的开户人姓名和您在联富金融上提供的身份证上的真实姓名一致，否则无法成功提现。
             </div>
             
             <div onClick="showsubmenu(26)" class="district"><s>&bull;</s><span>添加银行卡，为什么要填写省市和开户行字段？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                联富金融是通过第三方支付平台为用户提供付款服务，第三方支付平台将用户银行卡信息上传至银行做付款操作时，可能会进行省市和开户行字段的校验，如果为空或不正确可能会导致付款失败。<br />
                所以为了您提现的便利和安全，请您认真填写省市和开户行。
             </div> 
             
             <div onClick="showsubmenu(27)" class="district"><s>&bull;</s><span>添加银行卡，不知道银行卡的省市和开户行怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu27" class="area" style="display:none">
                1.您可以持银行卡及有效证件到发卡行的营业网点查询；<br />
                2.您可以拨打您银行卡开户行的24小时客服电话，选择人工服务，按照语音提示输入要查询的银行卡号和户主姓名，客服人员就会告知你开户行和所在地；<br />
                3.各大银行的客服电话：<br />
                <table class="table_4" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="20%">招商银行</td>
                      <td width="20%">中国银行</td>
                      <td width="20%">建设银行</td>
                      <td width="20%">工商银行</td>
                      <td class="td_1" width="20%">中信银行</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="20%">95555</td>
                      <td width="20%">95566</td>
                      <td width="20%">95533</td>
                      <td width="20%">95588</td>
                      <td class="td_1" width="20%">95558</td>
                   </tr>
                   <tr class="tr_1">
                      <td width="20%">农业银行</td>
                      <td width="20%">民生银行</td>
                      <td width="20%">光大银行</td>
                      <td width="20%">交通银行</td>
                      <td class="td_1" width="20%">广发银行</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="20%">95599</td>
                      <td width="20%">95568</td>
                      <td width="20%">95595</td>
                      <td width="20%">95559</td>
                      <td class="td_1" width="20%">400-830-8003</td>
                   </tr>
                   <tr class="tr_1">
                      <td width="20%">浦发银行</td>
                      <td width="20%">邮储银行</td>
                      <td width="20%">华夏银行</td>
                      <td width="20%">兴业银行</td>
                      <td class="td_1" width="20%">平安银行</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="20%">95528</td>
                      <td width="20%">95580</td>
                      <td width="20%">95577</td>
                      <td width="20%">95561</td>
                      <td class="td_1" width="20%">95511转3</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>如果所填写的银行卡信息不正确，能否提现成功？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                1.如果您填写的银行卡号有误或该银行卡开户名不是您实名认证的姓名，不会提现成功；<br />
                2.如果您填写的银行、开户行、开户行所在地信息不正确，则该笔提现在提交到银行做处理时，有可能由于信息校验不正确而提现失败。
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>提现未到账怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                1.可能是银行卡信息填写有误，银行做退票操作，大概会在7个工作日内完成退票，请用户耐心等候。<br />
                2.您还可以联系客服，寻求帮助（4006-888-923）。
             </div>
             
             <div onClick="showsubmenu(30)" class="district"><s>&bull;</s><span>为什么会提现失败？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu30" class="area" style="display:none;">
                造成您提现失败的原因可能有以下几种：<br />
                1.银行账号/户名错误，或是账号和户名不符；<br />
                2.银行账户冻结或正在办理挂失；<br />
                3.省份、城市、开户行等银行信息错误；<br />
                4.使用信用卡提现。<br />
                如果遇到以上情况，我们会在收到支付机构转账失败的通知后解除您的资金冻结（手续费不退还），请您不必担心资金安全。
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp"%>

</body>

</html>
