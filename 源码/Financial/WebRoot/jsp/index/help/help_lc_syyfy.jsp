﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html" style="background:#f8bbb2;color:#fff;"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
            <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html">理财新手必读</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html">产品介绍</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html">收益与费用</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html">联富宝</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html">散标投资</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">债权转让</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">             
             
             <div onClick="showsubmenu(19)" class="district"><s>&bull;</s><span>联富宝收益</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu19" class="area" style="display:none;">
                联富宝分为三种，联富宝L03锁定期3个月，预期年化收益12%；<br />
                联富宝L06锁定期6个月，预期年化收益14%；<br />
                联富宝L12锁定期12个月，预期年化收益16%。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=60#60">收益处理方式</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<%=__ROOT_PATH__ %>/help/help_aq_bjbzjh.html">联富宝安全吗？</a>
             </div>
             
             <div onClick="showsubmenu(20)" class="district"><s>&bull;</s><span>散标收益</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu20" class="area" style="display:none;">
                年利率区间8%—24%，具体收益由您所投资的借款标的利率确定。
             </div>
             
             <div onClick="showsubmenu(21)" class="district"><s>&bull;</s><span>购买债权转让收益</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                年利率区间8%—24%，具体收益由您所投资的转让债权利率及出让人设定的折让系数确定。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzr.html?type=58#58">什么是折价转让？</a>
             </div>
             
             <div onClick="showsubmenu(22)" class="district"><s>&bull;</s><span>等额本息借款的收益该如何计算？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<br />
                具体计算公式如下：<br />
                <img src="<%=__PUBLIC__ %>/images/help_2.png" /><br />
                P&nbsp;:每月还款额<br />
                A&nbsp;:借款本金<br />
                b&nbsp;:月利率<br />
                n&nbsp;:还款总期数<br />
                因计算中存在四舍五入，最后一期还款金额与之前略
             </div>
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>充值提现费用</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                充值/提现时由第三方支付收取的支付费用。<br />
                <h1>充值费用</h1>
                第三方支付平台将收取您充值金额的0.5%作为转账费用。扣除手续费的上限为100元。超过100元的部分将由联富金融承担。<br />
                提现费用<br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="25%">金额</td>
                      <td width="25%">2万元以下</td>
                      <td width="25%">2万(含)-5万元</td>
                      <td width="25%">5万(含)-100万元</td>
                   </tr>
                   <tr>
                      <td width="25%">手续费</td>
                      <td width="25%">1元/笔</td>
                      <td width="25%">3元/笔</td>
                      <td width="25%">5元/笔</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>散标费用</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none;">
                散标投资无费用。
             </div>
             
             <div onClick="showsubmenu(25)" id="25" class="district"><s>&bull;</s><span>联富宝费用</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                加入联富宝服务后，可能产生的费用包括：<br />
                <h1>加入费用：</h1>
                a%；加入计划金额的a%，期初额外收取，即加入计划金额为10万元，则另行收取10万元*a% 作为加入计划费用归联富金融所有。（当前加入费率为0.0%）<br />
                <h1>管理费用：</h1>
                联富宝对应借款实际利息收入中，超过联富宝预期年化收益以外的部分作为管理费用归联富金融所有，若实际收益不及预期年化收益的，联富金融不收取管理费用。详情参见<a href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/get_lfoll_fix_bid_contract.html?id=${fix_bid_system_order.m.id}&bid_name=${fix_bid_system_order.m.bid_name}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">《联富宝服务协议》</a>。<br />
                <h1>提前退出费用：</h1>
                c%；提前退出联富宝时按加入计划金额的c%收取，提前退出费用归联富金融所有。(当前提前退出费率为2.0%)。
             </div>
             
             <div onClick="showsubmenu(26)" id="glf" class="district"><s>&bull;</s><span>债权转让管理费</span><a href=""><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                债权转让的费用为转让管理费。平台向转出人收取，不向购买人收取任何费用。<br />
                转让管理费金额为成交金额*转让管理费率，转让管理费率在目前的运营中按0.5%收取，具体金额以债权转让页面显示为准。债权转让管理费在成交后直接从成交金额中扣除，不成交平台不向用户收取转让管理费。
             </div>    
               
          </div>
       </div>
    </div>
    <input type="hidden" value="${type }" name="type" id="type"/>
  </div>    
</div>


	<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>
