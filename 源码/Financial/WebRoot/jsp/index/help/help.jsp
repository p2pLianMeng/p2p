<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${applicationScope.title}</title>
    <meta http-equiv="keywords" content="${applicationScope.keywords}">
	<meta http-equiv="description" content="${applicationScope.description}">
	
	<link href="style/main.css" rel="stylesheet" type="text/css" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
	<script type="text/javascript">
		function showsubmenu(sid){
		    var whichEl = document.getElementById("submenu" + sid);
		    whichEl.style.display = whichEl.style.display =='none'?'':'none';
		}
	</script>
  </head>
  
  <body>
  
<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="<%=__ROOT_PATH__ %>/help/help_index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__ %>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div>
    
    <div class="help">
       <div class="con">
          <div class="up"><a href="<%=__ROOT_PATH__ %>/help/help_lc.html">理财帮助</a></div>
          <p><a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html">新手必读</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html">产品介绍</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html">收益与费用</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html">联富宝</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html">散标投资</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">债权转让</a></p>
       </div>
       <div class="con con_2">
          <div class="up"  style="background-position:0 -68px"><a href="<%=__ROOT_PATH__%>/help/help_zh.html">账户管理</a></div>
          <p><a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html">登录注册</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html">账户密码</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html">充值</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html">提现</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html">安全认证</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html">消息中心</a></p>
       </div>
       <div class="con con_3">
          <div class="up"  style="background-position:0 -135px;"><a href="<%=__ROOT_PATH__%>/help/help_jk.html">借款帮助</a></div>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html">产品介绍</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html">借款费用</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html">如何申请</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html">认证资料</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html">信用审核</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html">信用等级与额度</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html">筹款与提现</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html">如何还款</a></p>
       </div>
       <div class="con con_2 con_3">
          <div class="up"  style="background-position:0 -203px;"><a href="<%=__ROOT_PATH__%>/help/help_aq.html">安全保障</a></div>
          <p><a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">本金保障计划</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">法律与政策保障</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">借款审核与风控</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">账户及隐私安全</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">用户的自我保护</a></p>
          <p><a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">网站相关协议</a></p>
       </div>
    </div>
    
    <div class="help_2">
       <div class="up">名词解释</div>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="4%">&nbsp;</td>
             <td width="16%"><span>A - G</span></td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=6#6">垫付</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=7#7">等额本息</a></td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr class="tr_1">
             <td colspan="7">&nbsp;</td>
          </tr>
          <tr class="tr_2">
             <td colspan="7">&nbsp;</td>
          </tr>
          <tr>
             <td width="4%">&nbsp;</td>
             <td width="16%"><span>H - N</span></td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=8#8">借款用户(借款人)</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=9#9">加权平均利率</a></td>
            
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=11#11">借款服务费</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=12#12">借款管理费</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=13#13">借款利率</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=14#14">理财用户(理财人)</a></td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=15#15">年化利率</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=16#16">年化收益率</a></td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td width="16%">&nbsp;</td>
          </tr>
          <tr class="tr_1">
             <td colspan="7">&nbsp;</td>
          </tr>
          <tr class="tr_2">
             <td colspan="7">&nbsp;</td>
          </tr>
          <tr>
             <td width="4%">&nbsp;</td>
             <td width="16%"><span>O - T</span></td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=17#17">散标</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=18#18">实地认证标</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=19#19">锁定期</a></td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr class="tr_1">
             <td colspan="7">&nbsp;</td>
          </tr>
          <tr class="tr_2">
             <td colspan="7">&nbsp;</td>
          </tr>
          <tr>
             <td width="4%">&nbsp;</td>
             <td width="16%"><span>U - Z</span></td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=20#20">信用认证标</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=22#22">信用报告</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=23#23">信用审核</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=24#24">信用等级(分数)</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=25#25">信用额度</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=26#26">原始投资额</a></td>
             
             
          </tr>
          <tr>
             <td>&nbsp;</td>
             
             
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=27#27">逾期</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=28#28">严重逾期</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=30#30">债权</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=31#31">债权价值</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=32#32">债权转让</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=33#33">债权价格</a></td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=33#33">债权价格</a></td>
             <td><a href="<%=__ROOT_PATH__%>/help/find_help_zh.html?type=34#34">债权份数</a></td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
             <td width="16%">&nbsp;</td>
          </tr>
       </table>

    </div>
    
    
    
  </div>    
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>
