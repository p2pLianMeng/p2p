﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html" style="background:#f8bbb2;color:#fff;"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
            <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html">理财新手必读</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html">产品介绍</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html">收益与费用</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html">联富宝</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html">散标投资</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">债权转让</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div onClick="showsubmenu(45)" class="district"><s>&bull;</s><span>什么是债权转让？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu45" class="area" style="display:none;">
                指债权持有人通过联富金融债权转让平台将债权挂出且与购买人签订债权转让协议，将所持有的债权转让给购买人的操作。<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzr_js.html">查看详情>></a>
             </div> 
             
             <div onClick="showsubmenu(46)" class="district"><s>&bull;</s><span>什么样的债权可以转让？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu46" class="area" style="display:none;">
                1.债权持有90天后即可进行转让。<br />
                2.债权没有处于逾期状态。如转让挂出时没有处于逾期，但在转让中时债权变为逾期状态，系统将会把债权停止转让。<br />
             </div> 
             
             <div onClick="showsubmenu(47)" class="district"><s>&bull;</s><span>如何转出债权提前收回投资？</span><a href=""><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu47" class="area" style="display:none;">
                您可以在债权转让列表页面进行购买债权。<br />
                登录后可以按照份数购买债权，在确认购买份数及金额等信息后点击确定按钮，即完成债权的购买，此笔债权的持有人即发生了变更。购入人可以在已转入的债权或者还款中的债权页面看到买入的债权。<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzrtqshtz.html">查看操作流程>></a><br />
             </div> 
             
             <div onClick="showsubmenu(48)" class="district"><s>&bull;</s><span>如何买入债权，进行理财？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu48" class="area" style="display:none;">
                当您持有的债权处于可转让状态时，您可以打开【我的联富金融】--【理财管理】--【债权转让】--【可转出的债权】页面进行债权转让操作<br />
                您也可以在【债权转让】--【可转让的债权】页面进行批量债权转让操作，批量债权转让不可以填写份数，执行批量债权转让操作视为所选债权可以全部转让，且转让系数统一设置，不可单独设置。<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzrrhmr.html">查看操作流程>></a><br />
             </div> 
             
             <div onClick="showsubmenu(49)" class="district"><s>&bull;</s><span>买入转让债权的收益有多少？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu49" class="area" style="display:none;">
                年利率区间8%—24%，具体收益由您所投资的转让债权利率及出让人设置的转让系数确定。<br />
             </div> 
             
             <div onClick="showsubmenu(50)" class="district"><s>&bull;</s><span>债权转让管理费如何收取？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu50" class="area" style="display:none;">
                参考<a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html?type=26#glf">债权转让管理费</a>。<br />
             </div> 
             
             <div onClick="showsubmenu(51)" class="district"><s>&bull;</s><span>为什么买入债权后，收益会是负数？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu51" class="area" style="display:none;">
                不必担心，您并未受到损失。<br />
                由于购买转让的债权时您先支付了应计利息给出让人，所以此数值暂时为负，待借款人下次还款时，此负值会被抹平。<br />
                相关阅读：<a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html?type=55#55">债权价值是如何计算的？</a><br />
             </div>
             
             <div onClick="showsubmenu(52)" class="district"><s>&bull;</s><span>什么情况下债权的价值会发生变化？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu52" class="area" style="display:none;">
                1.当借款人还款后，债权价值的待收本金部分发生变化导致债权发生变化。<br />
                2.当日期发生变化时，债权价值的应计利息部分会发生变化。<br />
             </div>
             
             <div onClick="showsubmenu(53)" class="district"><s>&bull;</s><span>什么情况下债权会被锁定？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu53" class="area" style="display:none;">
                1.在借款人发生还款时，债权会被锁定。<br />
                2.联富金融系统每天进行标的状态(还款中、逾期等)扫描时，债权会被锁定。<br />
             </div>
             
             <div onClick="showsubmenu(54)" class="district"><s>&bull;</s><span>什么情况下购买债权会失败？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu54" class="area" style="display:none;">
                1.在购买时债权被锁定。<br />
                2.在购买时债权的价值发生了变化。<br />
                3.购买时债权的状态发生了变化，如变为逾期债权、债权已被借款人还清等。<br />
                4. 您的账户余额不足。
             </div>
             
             <div onClick="showsubmenu(55)" id="55" class="district"><s>&bull;</s><span>债权价值是如何计算的？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu55" class="area" style="display:none;">
                <h1>1.转让时，当期还款处于未还款状态</h1>
                <img src="<%=__PUBLIC__%>/images/help_5.png" /><br />
                F&nbsp;:债权公允价值<br />
                D&nbsp;:应计利息天数<br />
                b&nbsp;:月利率<br />
                x&nbsp;:产品折让参数, 本息保障类产品为1，本金保障类产品为0<br />
                <img src="<%=__PUBLIC__%>/images/help_6.png" /><br />
                注：应计利息天数超过30天按30天计算<br />
                注：产品保障类型参数：本金保障的债权为0，本息保障的债权为1。<br />
                <h1>2.转让时，当期还款处于已还款状态，但下一期处于未还款状态</h1>
                <img src="<%=__PUBLIC__%>/images/help_7.png" /><br />
                F&nbsp;:债权公允价值<br />
                b&nbsp;:月利率<br />
                注：应计利息天数超过30天按30天计算<br />
                注：产品保障类型参数：本金保障的债权为0，本息保障的债权为1。<br />
                <img src="<%=__PUBLIC__%>/images/help_8.png" /><br />
                <h1>3.转让时，当期还款处于已还款状态，且下面的N期处于已还款状态，但还未完全还清</h1>
                <img src="<%=__PUBLIC__%>/images/help_9.png" /><br />
                F&nbsp;:债权公允价值<br />
                b&nbsp;:月利率<br />
                t&nbsp;:最后还款所在期数与成交日期所在期数之差<br />
                <img src="<%=__PUBLIC__%>/images/help_8.png" /><br />
             </div>
             
             <div onClick="showsubmenu(56)" id="56" class="district"><s>&bull;</s><span>债权的转让价格是如何确定的？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu56" class="area" style="display:none;">
                1.转让时，当期还款处于未还款状态<br />
                <img src="<%=__PUBLIC__%>/images/help_11.png" /><br />                
                <h1>1.转让时，当期还款处于未还款状态。</h1>
                <img src="<%=__PUBLIC__%>/images/help_12.png" /><br />
                <img src="<%=__PUBLIC__%>/images/help_13.png" /><br />                
                <img src="<%=__PUBLIC__%>/images/help_6.png" /><br />
                R&nbsp;:转让系数<br />
                b&nbsp;:月利率<br />
                x&nbsp;:产品折让参数, 本息保障类产品为1，本金保障类产品为0<br />
                <h1>2.转让时，当期还款处于已还款状态，但下一期处于未还款状态。</h1>
                <img src="<%=__PUBLIC__%>/images/help_12.png" /><br />
                <img src="<%=__PUBLIC__%>/images/help_14.png" /><br /><br />
                <img src="<%=__PUBLIC__%>/images/help_8.png" /><br />
                b&nbsp;:月利率<br />
                <h1>3.转让时，当期还款处于已还款状态，且下面的N期处于已还款状态，但还未完全还清。</h1>
                <img src="<%=__PUBLIC__%>/images/help_15.png" /><br />
                <img src="<%=__PUBLIC__%>/images/help_16.png" /><br /><br />
                <img src="<%=__PUBLIC__%>/images/help_8.png" /><br />
                b&nbsp;:月利率<br />
                t&nbsp;:最后还款所在期数与成交日期所在期数之差<br />
             </div>
             
             <div onClick="showsubmenu(57)" class="district"><s>&bull;</s><span>什么是应计利息？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu57" class="area" style="display:none;">
                应计利息：在债权转让过程中，在还款日前售出债权，利息不归转出人所有，不过购买债权的人必须依据上一次还款日至交易日的持有时间按比例给转出人相应的补偿，此补偿的利息称为应计利息。即实际上转出人通过应计利息获得该还款期内其持有此债权的利息，而转入人获得之后持有期的利息。<br />
                <img src="<%=__PUBLIC__%>/images/help_17.png" /><br /><br />
                <img src="<%=__PUBLIC__%>/images/help_6.png" /><br />
                b&nbsp;:月利率<br />
                举例：假如A持有一份剩余未还本金为20元的实地认证标，年利率为12%，还款日为每月8号，在6号以100%的转让系数挂出转让（即平价转让）。B在6号成功购买，成交价格包括两部分，即未还本金20元和应计利息20*1%*28/30=0.19元，共计20.19元。成交后<br />
                A回收本金20元，收到应计利息0.19元并计入已赚利息，债权转移至B名下，资产减少20元；<br />
                B支出20.19元获得此债权，其中债权的待收本金为20元，债权记为资产，同时0.19元为应计利息的支出，记（-0.19）元为已赚利息。等到8号，借款人支付该月还款时，B获得利息收入0.2元，与之前的应计利息支出合计为 0.2+(-0.19)=0.01元；此合计利息即为B于6号购买债权后，持有此债权2天所应获得的利息。<br />
                总结：通过债权转让和应计利息的计算，假设该持有期有30天，A获得了28天的利息，B获得了2天的利息。
             </div>
             
             <div onClick="showsubmenu(58)" id="58" class="district"><s>&bull;</s><span>什么是折价转让？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu58" class="area" style="display:none;">
                债权转出人在出售债权时，选择在待收本金价格上打折，给予债权购买人折扣的让利行为。即债权转出人将转让系数设置在100%以下。
             </div>
               
          </div>
       </div>
    </div>
  </div>    
</div>


<div class="bottom">
   <div class="content">
      <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="10%">友情链接</td>
            <td width="9%"><a href="http://www.hc360.com/">慧聪金融</a></td>
            <td width="9%"><a href="http://www.enfodesk.com/">易观智库</a></td>
            <td width="9%"><a href="http://guangzhou.haodai.com/">好贷网</a></td>
            <td width="9%"><a href="http://www.yinhang.com/">银率网</a></td>
            <td width="9%"><a href="http://bank.hexun.com/">和讯银行</a></td>
            <td width="9%"><a href="http://www.gome.com.cn/">国美在线</a></td>
            <td width="9%"><a href="http://www.cjdao.com/index.html">财经道</a></td>
            <td width="9%"><a href="http://www.siilu.com/">电商服务</a></td>
            <td width="9%"><a href="http://www.shopex.cn/">ShopEx</a></td>
            <td width="9%"><a href="http://bj.58.com/danbaobaoxiantouzi/">58投资担保</a></td>
         </tr>
         <tr>
            <td></td>
            <td><a href="http://www.wangdaizhijia.com/">网贷之家</a></td>
            <td><a href="http://bbs.wacai.com/portal.php">挖财社区</a></td>
            <td><a href="http://www.feidee.com/money/">随手记</a></td>
            <td><a href="http://www.rong360.com/licai/">融360理财</a></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td class="td_1" colspan="11"></td>
         </tr>
      </table>
      <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="10%" height="25"><a href="#">关于我们</a></td>
            <td width="9%"><a href="#">帮助中心</a></td>
            <td width="17%"><a href="#">联系我们</a></td>
            <td width="24%" rowspan="2" class="td_1"><a href="#">4006-888-923</a><br /><span>（客服工作时间：早8:00-晚20:00）</span></td>
            <td colspan="2" rowspan="2" class="td_2"><span><a href="#">官方微信</a><br /><a href="#">扫我一下</a></span></td>
            <td colspan="2" rowspan="2" class="td_3"><span><a href="#">新浪微博</a><a href="#">立即关注</a></span></td>
            <td width="12%" rowspan="2" class="td_4"><a>客服QQ</a></td>
         </tr>
         <tr>
            <td height="39"><a href="#">网站协议</a></td>
            <td><a href="#">网站地图</a></td>
            <td><a href="#">意见反馈</a></td>
         </tr>
         <tr>
            <td height="17" colspan="10" class="td_5">&nbsp;</td>
         </tr>
      </table>   
   </div>
   <div class="down">CopyRight © 2014 联富金融 All Rights Reserved. Powered by&nbsp;&nbsp;|&nbsp;&nbsp;广州天的富网络科技有限公司&nbsp;&nbsp;|&nbsp;&nbsp;粤ICP备14022597号-1 可信网站</div>
      <div class="down down_2">
         <a class="a" href="https://search.szfw.org/cert/l/CX20140804008606008701"></a>
         <a class="b" href="http://www.itrust.org.cn/yz/pjwx.asp?wm=1562798085"></a>
         <a class="c" href="http://pinggu.zx110.org/review_url_ali168.com"></a>
         <a class="d" href="http://www.beianbeian.com/"></a>
         <a class="e" href="http://net.china.com.cn/index.htm"></a>
      </div>
      <input type="hidden" name="type" id="type" value="${type }"/>
</div>
</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>