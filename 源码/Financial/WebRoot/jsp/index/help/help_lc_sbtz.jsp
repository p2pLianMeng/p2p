﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html" style="background:#f8bbb2;color:#fff;"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html">理财新手必读</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html">产品介绍</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html">收益与费用</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html">联富宝</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html">散标投资</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">债权转让</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div onClick="showsubmenu(30)" class="district"><s>&bull;</s><span>信用认证标是什么？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu30" class="area" style="display:none;">
                信用认证标是联富金融通过对借款用户的个人信用资质进行全面审核后，允许用户发布的借款标。
             </div>
             
             <div onClick="showsubmenu(32)" class="district"><s>&bull;</s><span>散标的发布时间</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu32" class="area" style="display:none;">
                您可打开【我要理财】--【散标投资列表】查看橙色信息栏中的发标时间或<a href="#">查看相关公告>></a>。
             </div>
             
             <div onClick="showsubmenu(33)" class="district"><s>&bull;</s><span>散标的收益有多少？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu33" class="area" style="display:none;">
                散标年利率区间8%—24%，具体收益由您所投资的借款标的利率确定。
             </div>
             
             <div onClick="showsubmenu(34)" class="district"><s>&bull;</s><span>散标的投资期限有多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu34" class="area" style="display:none;">
                3-36个月，具体期限由您所投资的借款标的确定。
             </div>
             
             <div onClick="showsubmenu(35)" class="district"><s>&bull;</s><span>是否可以提前收回出借的本金？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu35" class="area" style="display:none;">
                可以，持有90天后您可通过债权转让提前收回出借的资金。<br />
                相关阅读：<a href="#">如何转出债权提前收回投资？</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">什么是债权转让？</a>
             </div>
             
             <div onClick="showsubmenu(36)" class="district"><s>&bull;</s><span>散标投资安全吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu36" class="area" style="display:none;">
                为保障理财人的出借安全，联富金融设有适用于全体理财用户的<a href="<%=__ROOT_PATH__ %>/help/help_aq_bjbzjh.html?type=7#7">本金保障计划</a>。
             </div>
             
             <div onClick="showsubmenu(37)" class="district"><s>&bull;</s><span>散标投资有费用吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu37" class="area" style="display:none;">
                散标投资无费用。
             </div>
             
             <div onClick="showsubmenu(38)" class="district"><s>&bull;</s><span>投标前需要注意哪些事项？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu38" class="area" style="display:none;">
                1.投标前认真阅读该笔借款的详细信息（包括借款金额、利息率、借款期限、借款者信用等级等），以确定您所要投的标符合您的风险承受能力和所要求的投资回报率。<br />
                2.投标前应知道若您所投标的借款人发生违约，违约损失由投资该标的所有理财人共同承担。<br />
                3.投标前请核实自己将要理财的金额，确认无误后再进行操作。
             </div>
             
             <div onClick="showsubmenu(39)" class="district"><s>&bull;</s><span>如何投资散标？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu39" class="area" style="display:none;">
                登录联富金融，点击【我要理财】--【散标投资列表】，选择借款标的，输入金额，点击【确认】完成投资。<a href="<%=__ROOT_PATH__ %>/help/help_lc_sbrhtz.html">查看操作流程</a>
             </div>
             
             <div onClick="showsubmenu(40)" class="district"><s>&bull;</s><span>如何分散投标？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu40" class="area" style="display:none;">
                1.尽量进行分散投资，这样可降低单一借款人违约对投资收益的影响。例如您可以把5000元借给10个借款人。<br />
                2.在同等收益的情况下投标给信用等级较高的借款人。<br />
                3. 在投标时注意了解对应标的保障范围。如保障本金还是保障本金+利息。
             </div> 
             
             <div onClick="showsubmenu(41)" class="district"><s>&bull;</s><span>如何收取还款？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu41" class="area" style="display:none;">
                借款人在规定的还款时间内将钱充值到与联富金融合作的第三方支付平台上，联富金融系统会发送邮件通知所有理财人借款人已成功还款。理财人可按个人需求选择提现或继续投资。<br />
                注：借款人也可能提前偿还全部借款或者在到期日前手动提前偿还借款，请理财人注意查收联富金融发出的通知。
             </div> 
             
             <div onClick="showsubmenu(42)" class="district"><s>&bull;</s><span>借款人逾期怎么办？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu42" class="area" style="display:none;">
                自逾期开始之后，正常利息停止计算。按照下面公式计算罚息：<br />
                <img src="<%=__PUBLIC__ %>/images/help_3.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="30%">金额</td>
                      <td width="30%">2万元以下</td>
                      <td width="40%">2万(含)-5万元</td>
                   </tr>
                   <tr>
                      <td width="30%">手续费</td>
                      <td width="30%">1元/笔</td>
                      <td width="40%">3元/笔</td>
                   </tr>
                </table>
                注：本计算公式按50元一份计算。<br />
                联富金融有一套完善的催收机制，通过电话短信提醒、上门拜访、法律诉讼等多种方式，有效的保证了平台上绝大部分借款人的及时还款。延迟还款的借款人需按约定交纳罚息和违约金。<br />
             </div>
             
             <div onClick="showsubmenu(43)" class="district"><s>&bull;</s><span>借款人提前还款怎么办？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu43" class="area" style="display:none;">
                借款人可以提前偿还借款，如果借款人提前偿还全部借款，需要额外支付给理财人借款剩余本金的1%作为违约金，不用再支付后续的利息及管理费用。<br />
                注：部分借款标的违约金费率为0%。<br />
                具体计算公式如下：<br />
                <img src="<%=__PUBLIC__ %>/images/help_4.png" /><br />
             </div> 
             
           <!--   <div onClick="showsubmenu(44)" class="district"><s>&bull;</s><span>自动投标工具是什么？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu44" class="area" style="display:none;">
                是一款依据您设定的规则，由系统自动代您进行投标操作的工具。可以在【我的联富金融】—【理财管理】—【自动投标工具】中进行设置。
             </div> 
                -->
          </div>
       </div>
    </div>
    
  </div>    
</div>
 <input type="hidden" name="type" id="type" value="${type }"/>

	<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>