﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html" style="background:#f8bbb2;color:#fff;"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html">产品介绍</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html">借款费用</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html">如何申请</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html">认证资料</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html">信用审核</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html">信用等级与额度</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html">筹款与提现</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html">如何还款</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>产品介绍</div>
             
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>工薪贷</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none">
                <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="7%">&nbsp;</td>
                      <td colspan="2">工薪贷</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">产品介绍</td>
                      <td width="67%">工薪贷（适用工薪阶层）</td>
                   </tr>
                   <tr class="tr_3">
                      <td rowspan="3" width="7%">&nbsp;</td>
                      <td rowspan="3" width="26%">申请条件</td>
                      <td width="67%">（1）22-55周岁的中国公民</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（2）在现单位工作满3个月</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（3）月收入2000以上</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">主要借款用途</td>
                      <td width="67%">装修、结婚、买房、买车、教育（进修、出国留学）、其他消费</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款额度</td>
                      <td width="67%">3,000 - 500,000</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款年利率</td>
                      <td width="67%">10% - 24%</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款期限</td>
                      <td width="67%">3、6、9、12、15、18、24、36个月</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">审核时间</td>
                      <td width="67%">1-3个工作日</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">还款方式</td>
                      <td width="67%">等额本息，每月还款</td>
                   </tr>
                   <tr class="tr_2">
                      <td rowspan="4" width="7%">&nbsp;</td>
                      <td rowspan="4" width="26%">必要申请资料</td>
                      <td width="67%">（1）身份证</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（2）个人信用报告</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（3）劳动合同或在职证明</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（4）近3个月工资卡银行流水</td>
                   </tr>
                </table>
             </div>
             
             
             
             <div onClick="showsubmenu(7)" class="district"><s>&bull;</s><span>生意贷</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1 tr_4">
                      <td width="7%">&nbsp;</td>
                      <td colspan="2">生意贷</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">产品介绍</td>
                      <td width="67%">生意贷（适用私营企业主）</td>
                   </tr>
                   <tr class="tr_3">
                      <td rowspan="3" width="7%">&nbsp;</td>
                      <td rowspan="3" width="26%">申请条件</td>
                      <td width="67%">（1）22-55周岁的中国公民</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（2）企业经营时间满1年</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（3）申请人限于法人代表</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">主要借款用途</td>
                      <td width="67%">流动资金、采购设备或原材料、市场推广费用</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款额度</td>
                      <td width="67%">3,000 - 500,000</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款年利率</td>
                      <td width="67%">10% - 24%</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款期限</td>
                      <td width="67%">3、6、9、12、15、18、24、36个月</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">审核时间</td>
                      <td width="67%">1-3个工作日</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">还款方式</td>
                      <td width="67%">等额本息，每月还款</td>
                   </tr>
                   <tr class="tr_2">
                      <td rowspan="4" width="7%">&nbsp;</td>
                      <td rowspan="4" width="26%">必要申请资料</td>
                      <td width="67%">（1）身份证</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（2）个人信用报告</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（3）经营证明</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（4）近6个月常用银行卡流水</td>
                   </tr>
                </table>
             </div>
             <div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>净值贷</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
			 <div id="submenu8" class="area" style="display:none;">
				<table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
				   <tr class="tr_1 tr_5">
					  <td width="7%">&nbsp;</td>
					  <td colspan="2">净值贷</td>
				   </tr>
				   <tr class="tr_2">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">产品介绍</td>
					  <td width="67%">净值贷(适用联富金融所有注册用户)</td>
				   </tr>
				   <tr class="tr_3">
					  <td rowspan="2" width="7%">&nbsp;</td>
					  <td rowspan="2" width="26%">申请条件</td>
					  <td width="67%">（1）22-55周岁的中国公民</td>
				   </tr>
				   <tr class="tr_3">
					  <td width="67%">（2）净资产90%为1000元及以上</td>
				   </tr>
				   <tr class="tr_2">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">主要借款用途</td>
					  <td width="67%">平台理财投标用户短期资金周转</td>
				   </tr>
				   <tr class="tr_3">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">借款额度</td>
					  <td width="67%">1,000 - 500,000</td>
				   </tr>
				   <tr class="tr_2">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">借款年利率</td>
					  <td width="67%">10% - 24%</td>
				   </tr>
				   <tr class="tr_3">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">借款期限</td>
					  <td width="67%">5天、 10天、 15天、 20天 、25天、 1个月到12个月</td>
				   </tr>
				   <tr class="tr_2">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">审核时间</td>
					  <td width="67%">24小时内</td>
				   </tr>
				   <tr class="tr_3">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">还款方式</td>
					  <td width="67%">一个月以内一次性还款还息；2个月以上每月还款，等额本息。</td>
				   </tr>
				   <tr class="tr_2">
					  <td width="7%">&nbsp;</td>
					  <td width="26%">必要申请资料</td>
					  <td width="67%">本人身份证原件的正、反两面照片及本人手持身份证正面头部照。</td>
				   </tr>
				</table>
			 </div>
             <%--<div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>网商贷</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1 tr_5">
                      <td width="7%">&nbsp;</td>
                      <td colspan="2">网商贷</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">产品介绍</td>
                      <td width="67%">网商贷（适用淘宝网店商户）</td>
                   </tr>
                   <tr class="tr_3">
                      <td rowspan="5" width="7%">&nbsp;</td>
                      <td rowspan="5" width="26%">申请条件</td>
                      <td width="67%">（1）22-55周岁的中国公民</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（2）在淘宝或天猫经营网店满半年</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（3）店铺等级达到2钻，好评率达到95%，动态评分达到4.5，无法估价产品和虚拟产品占比不超过40% </td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（4）店铺在申请人名下，且近期无淘宝严重处罚</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="67%">（5）近3个月交易总额满3万，且交易笔数超过50笔</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">主要借款用途</td>
                      <td width="67%">流动资金、采购设备或原材料、市场推广费用</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款额度</td>
                      <td width="67%">3,000 - 500,000</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款年利率</td>
                      <td width="67%">10% - 24%</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">借款期限</td>
                      <td width="67%">3、6、9、12、15、18、24、36个月</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">审核时间</td>
                      <td width="67%">1-3个工作日</td>
                   </tr>
                   <tr class="tr_3">
                      <td width="7%">&nbsp;</td>
                      <td width="26%">还款方式</td>
                      <td width="67%">等额本息，每月还款</td>
                   </tr>
                   <tr class="tr_2">
                      <td rowspan="3" width="7%">&nbsp;</td>
                      <td rowspan="3" width="26%">必要申请资料</td>
                      <td width="67%">（1）身份证</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（2）提供网店地址及阿里旺旺账号</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="67%">（3）需QQ视频审核</td>
                   </tr>
                </table>
             </div>  
               
          --%></div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
