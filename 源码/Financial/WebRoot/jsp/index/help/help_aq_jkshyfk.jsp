﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html" style="background:#f8bbb2;color:#fff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
              <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">本金保障计划</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">法律与政策保障</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">借款审核与风控</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">账户及隐私安全</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">用户的自我保护</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">网站相关协议</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div class="down_up"><span>&gt;</span>借款审核与风控</div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>严格的贷前审核</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                在客户提出借款申请后，联富金融会对客户的基本资料进行分析。通过网络、电话及其他可以掌握的有效渠道进行详实、仔细的调查。避免不良客户的欺诈风险。在资料信息核实完成后，根据个人信用风险分析系统进行评估，由经验丰富的借款审核人员进行双重审核确认后最终决定批核结果。
             </div>   
             
             <div onClick="showsubmenu(13)" class="district"><s>&bull;</s><span>完善的贷后管理</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                如果用户逾期未归还借款，贷后管理部门将第一时间通过短信、电话等方式提醒用户进行还款。如果用户在5天内还未归还当期借款，联富金融将会联系该用户的紧急联系人、直系亲属、单位等督促用户尽快还款。如果用户仍未还款，交由专业的高级催收团队与第三方专业机构合作进行包括上门等一系列的催收工作，直至采取法律手段。
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
