﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html" style="background:#f8bbb2;color:#fff;"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html">理财新手必读</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html">产品介绍</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html">收益与费用</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html">联富宝</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html">散标投资</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">债权转让</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>什么是联富宝？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                联富宝是联富金融推出的便捷高效的理财工具。联富宝在用户认可的标的范围内，对符合要求的标的进行投标，且回款本金在相应期限内自动复投，期限结束后联富宝会通过联富金融债权转让平台进行转让退出。该计划所对应的标的均100%适用于联富金融本金保障计划并由系统实现标的分散投资。<br />
                理财人加入联富宝后，会进入锁定期，锁定期内，投资的回款本金将继续进行投资直到锁定期结束，充分提高资金利用效率。<br />
                锁定期结束后，理财人自动退出联富宝，理财人在该计划内投资的债权将优先进行债权转让。债权转让所得资金及投资回款所得等将不再继续自动投资，系统将在指定时间将此资金转移至用户的主账户供用户自行支配。<br />相关阅读：<a href="<%=__ROOT_PATH__ %>/template/lian_fu_bao_instructions.pdf">《联富金融联富宝说明书》</a>
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>联富宝的收益有多少？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                联富宝分为三种，联富宝L03锁定期3个月，预期年化收益12%；联富宝L06锁定期6个月，预期年化收益14%；联富宝L12锁定期12个月，预期年化收益16%。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=60#60">收益处理方式</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=62#62">联富宝安全吗？</a>
             </div> 
             
             <div onClick="showsubmenu(59)" class="district"><s>&bull;</s><span>联富宝的发布时间</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu59" class="area" style="display:none;">
                联富宝的发布时间通常是每周一上午10:00，如遇到假期或者特殊情况，联富金融会调整联富宝发布时间，具体发布时间可在网站首页查看。<br />
                您还可以关注联富金融官方微信获取最新联富宝发布时间。<br />
                <img src="<%=__PUBLIC__ %>/images/qr_code.jpg" />
             </div>
             
             <div onClick="showsubmenu(60)" id="60" class="district"><s>&bull;</s><span>收益处理方式</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu60" class="area" style="display:none;">
                联富宝只提供到期提取至联富金融账户。<br />
                另外，在联富宝退出后，投资本金和所得收益将定时提取至联富金融主账户。               
             </div>
             
             <div onClick="showsubmenu(61)" class="district"><s>&bull;</s><span>联富宝的期限有多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu61" class="area" style="display:none;">
                联富宝L03的锁定期为3个月，到期后自动退出。<br />
                联富宝L06的锁定期为6个月，到期后自动退出。<br />
                联富宝L12的锁定期为12个月，到期后自动退出。<br />               
             </div>
             
             <div onClick="showsubmenu(62)" id="62" class="district"><s>&bull;</s><span>联富宝安全吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu62" class="area" style="display:none;">
                联富宝所投借款100%适用联富金融<a href="<%=__ROOT_PATH__ %>/help/help_aq_bjbzjh.html">本金保障计划</a>。             
             </div>
             
             <div onClick="showsubmenu(63)" class="district"><s>&bull;</s><span>加入联富宝需要什么费用？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu63" class="area" style="display:none;">
                参考<a href="<%=__ROOT_PATH__ %>/help/help_lc_syyfy.html">联富宝费用</a>。        
             </div>
             
             <div onClick="showsubmenu(64)" id="64" class="district"><s>&bull;</s><span>锁定期是什么？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu64" id="64" class="area" style="display:none;">
                联富宝具有锁定期限制。锁定期内，您可操作提前退出，但会产生相应费用，提前退出费用=加入计划金额*3.0%。锁定期满后自动退出。      
             </div>
             
             <div onClick="showsubmenu(65)" class="district"><s>&bull;</s><span>如何加入联富宝</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu65" class="area" style="display:none;">                
                <h1>加入联富宝</h1>
                在申请期内，并且计划开放额度及加入条件均符合的情况下，可以预定联富宝，预定金额的1%视为定金将被冻结，剩余金额需要在规定的时间内另行支付，在规定时间内未完成支付视为主动放弃，定金将不予返还。<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfbjs.html">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(66)" class="district"><s>&bull;</s><span>预定成功后，如何支付剩余金额？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu66" class="area" style="display:none;">                
                登录联富金融，打开【我的联富金融】--【理财管理】--【联富宝】页面；点击【预定中】栏目下将显示您预定的所有联富宝，点击右侧【支付】；核对信息无误后，点击【确认】；即可加入成功。<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfbzf.html">查看操作流程>></a>       
             </div>
             
             <div onClick="showsubmenu(67)" class="district"><s>&bull;</s><span>预定后，未能按时支付剩余金额，定金将如何处理？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu67" class="area" style="display:none;">                
                若您未能在限定时间内完成剩余金额的支付, 则视为您主动放弃加入联富宝, 定金将不予返还。   
             </div>
             
             <div onClick="showsubmenu(68)" class="district"><s>&bull;</s><span>如何退出联富宝并收回本金？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu68" class="area" style="display:none;">    
                <h1>到期退出</h1>
                锁定期结束当日，联富宝将自动退出。
                <h1>提前退出</h1>
                锁定期内，您可操作提前退出，但会产生相应费用，提前退出费用=加入计划金额*3.0%。当前仅支持全额退出。
             </div>
          </div>
       </div>
    </div>
    
  </div>    
</div>
<input type="hidden" name="type" id="type" value="${type }"/>

	<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>