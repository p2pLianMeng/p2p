﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html" style="background:#f8bbb2;color:#fff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
              <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">本金保障计划</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">法律与政策保障</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">借款审核与风控</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">账户及隐私安全</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">用户的自我保护</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">网站相关协议</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             
             <div class="down_up"><span>&gt;</span>网站相关协议</div>
             
             <div onClick="showsubmenu(21)" class="district"><s>&bull;</s><span>电子合同的有效性</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                通过联富金融审核的借款方向投资方借贷，双方通过平台的电子借贷协议，明确双方的债务与债权关系。依据中华人民共和国合同法第十一条规定：“书面形式是指合同书、信件和数据电文（包括电报、电传、传真、电子数据交换和电子邮件）等可以有形地表现所载内容的形式”，联富金融上电子合同与传统合同具有同等的法律效力，联富金融服务仅向符合中华人民共和国有关法律法规及本公司相关规定的合格投资人和借款人提供。
             </div>
             
             <div onClick="showsubmenu(22)" class="district"><s>&bull;</s><span>如何查询我的借款协议？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                1.如果您已在网上成功的获得借款，您可以在网站上打开【我的联富金融】--【借款管理】--【我的借款】--【借款协议】<br />
                2.如果您已在网上成功的投标理财，您可以在网站上打开【我的联富金融】--【理财管理】--【我的债权】--【借款协议】</br>
                3.您也可以在帮助中查看“网站借款协议范本”
             </div>  
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>网站借款协议范本</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                <a href="#">点击查看网站借款协议范本</a>
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>联富宝预定协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none">
                <a href="#">点击查看联富宝预定协议</a>
             </div>
             
             <div onClick="showsubmenu(25)" class="district"><s>&bull;</s><span>联富宝服务协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                <a href="#">点击查看联富宝服务协议</a>
             </div>
             
             <div onClick="showsubmenu(26)" class="district"><s>&bull;</s><span>添加银行卡，为什么要填写省市和开户行字段？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                联富金融是通过第三方支付平台为用户提供付款服务，第三方支付平台将用户银行卡信息上传至银行做付款操作时，可能会进行省市和开户行字段的校验，如果为空或不正确可能会导致付款失败。<br />
                所以为了您提现的便利和安全，请您认真填写省市和开户行。
             </div> 
             
             <div onClick="showsubmenu(27)" class="district"><s>&bull;</s><span>联富宝产品说明</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu27" class="area" style="display:none">
               	<a href="#"> 点击查看联富宝产品说明</a>
             </div>
             
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>债权转让说明书</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                <a href="#">点击查看债权转让说明书</a>
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>债权转让协议</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                <a href="#">债权转让及受让协议范本</a>
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
</body>

</html>
