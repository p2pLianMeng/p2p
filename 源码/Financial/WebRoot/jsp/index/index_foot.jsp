<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%@taglib uri="http://com.tfoll.web/lable/sys_friendship_link"
	prefix="lfoll"%>

<div class="bottom">
	<div class="content">
		<table class="table_1" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<lfoll:sys_friendship_link />
			<tr>
				<td class="td_1" colspan="11"></td>
			</tr>
		</table>
		<table class="table_2" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<tr>
				<td width="10%" height="25">
					<a href="<%=__ROOT_PATH__%>/index/about/about_page.html">关于我们</a>
				</td>
				<td width="9%">
					<a href="<%=__ROOT_PATH__%>/help/help_index.html">帮助中心</a>
				</td>
				<td width="9%">
					<a
						href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=contact">联系我们</a>
				</td>
				<td width="24%" rowspan="2" class="td_1">
					<a href="#">4006-888-923</a>
					<br />
					<span>（客服工作时间：早8:00-晚20:00）</span>
				</td>
				<td width="21%" rowspan="2" class="td_4">
					<span><a target="_blank"
						href="http://wpa.b.qq.com/cgi/wpa.php?ln=1&key=XzkzODA2ODIyN18yMTk2NzVfNDAwNjg4ODkyM18yXw"
						class="srvDj" target="_blank">客服QQ4006888923</a>
									<br />
						<a href="http://jq.qq.com/?_wv=1027&k=a9ixmJ">官方Q群 423919177</a>
					</span>
				</td>
				<td width="13%" colspan="2" rowspan="2" class="td_2">
					<span><a href="#">官方微信</a>
					<br />
						<a href="javascript:void(0);">扫我一下</a> <b><img
								src="<%=__PUBLIC__%>/images/weixin.jpg" /><s><img
									src="<%=__PUBLIC__%>/images/weixin.png" />
						</s>
					</b> </span>
				</td>
				<td  width="14%" colspan="2" rowspan="2" class="td_3">
					<span><a target="_blank" href="http://weibo.com/lfoll">新浪微博</a>
					<br />
						<a target="_blank" href="http://weibo.com/lfoll">立即关注</a>
					</span>
				</td>
			</tr>
			<tr>
				<td height="39">
					<a
						href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=agreement">网站协议</a>
				</td>
				<td>
					<a
						href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=contact">网站地图</a>
				</td>
				<td>
					<a
						href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=feed_back">意见反馈</a>
				</td>
			</tr>
			<tr>
            <td height="17" colspan="10" class="td_5">&nbsp;</td>
         </tr>
		</table>
	</div>
	<div style="margin-top: 2px" class="down">
		CopyRight © 2014 联富金融 All Rights Reserved. Powered
		by&nbsp;&nbsp;|&nbsp;&nbsp;广州天的富网络科技有限公司&nbsp;&nbsp;|&nbsp;&nbsp;粤ICP备14022597号-1
		可信网站
		<span>
		<%-- 
		<script type="text/javascript">
	var cnzz_protocol = (("https:" == document.location.protocol) ? " https://"
			: " http://");
	document
			.write(unescape("%3Cspan id='cnzz_stat_icon_1253898674'%3E%3C/span%3E%3Cscript src='"
					+ cnzz_protocol
					+ "s4.cnzz.com/z_stat.php%3Fid%3D1253898674%26show%3Dpic1' type='text/javascript'%3E%3C/script%3E"));
	</script>
	--%>
		</span>
	</div>
	<div class="down down_2">
		<!-- 可信网站 -->
		<a target="_blank" class="a"
			href="https://search.szfw.org/cert/l/CX20140804008606008701"></a>
		<a target="_blank" class="b"
			href="http://webscan.360.cn/index/checkwebsite/url/www.lfoll.com"
			style="width: 96px; height: 35px;"></a>
		<!-- 360-->
		<!--网站可信度-->
		<a target="_blank" class="c"
			href="http://pinggu.zx110.org/review_url_lfoll.com" target="_blank"
			title="可信评估" style="width: 96px; height: 35px;"></a>
		<!--备案查询-->
		<a target="_blank" class="d"
			href="http://www.beianbeian.com/beianxinxi/ac7274ca-6d76-43a8-83e9-1427eacd2ec3.html"></a>
		<!-- 不良信息举报中心 -->
		<a target="_blank" class="e" href="http://net.china.com.cn/index.htm"></a>
	</div>
</div>