<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">  

<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>

<%
String __ROOT_PATH__THIS = WebApp.getWebRootPath(request);
String __PUBLIC____THIS = WebApp.getPublicPath(request,__ROOT_PATH__THIS);
 %>
<script type="text/javascript" src="<%=__PUBLIC____THIS %>/zclip/js/jquery.zclip.min.js"></script>
<script type="text/javascript">
$(function(){
   
	$('#copy_button').zclip({
		path: '<%=__PUBLIC____THIS %>/zclip/js/ZeroClipboard.swf',
		copy: function(){
			return $('#url').val();
		},
		afterCopy: function(){
		
			$("<span id='msg'/>").insertAfter($('#copy_button')).text('复制成功').fadeOut(2000);
		}
	});
	
	
});
</script>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">平台活动</a>
    </div>
   
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_pthd">
       <div class="up_1">活动一、</div>
       <div class="up"><span>邀请好友一起赚钱：</span><input type="button" style="width:80px;height:30px;  font-size:15px;color:white;cursor:pointer;  background-color: #ed5050;margin-left: 30px;" id="copy_button"  value="复制链接"/><a href="<%=__ROOT_PATH__ %>/index/activity/to_recommend.html">（点击查看详情）</a></div>
       <input type="hidden" id="url" value="${url }"/>
       <div class="table_1">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td class="td_1 td_2" width="35%">邀请成功好友昵称</td>
                <td class="td_1 td_2" width="35%">邀请成功的好友真实姓名</td>
                <td class="td_2" width="30%">获赠金额</td>             
             </tr>
             <c:forEach items="${activity_record_list}" var ="record">
             <tr class="tr_2">
             	<c:if test="${record.r.type == 1}">
             			<td class="td_1" width="35%">${record.r.nickname}</td>
                		<td class="td_1" id="yanse" width="35x%">${record.r.real_name==null?"您的好友还没实名认证":record.r.real_name}</td>
                		<td width="30%">${record.r.reward_amount==null?"0.00":record.r.reward_amount }元</td> 
              	</c:if>       
             </tr>
              </c:forEach> 
             <c:if test="${activity_record_list == null}">
             <tr class="tr_3">
                <td class="td_2" colspan="3" width="36%">您现在还没有邀请好友，赶快去邀请吧！</td>          
             </tr>
             </c:if>
             <tr>
                <td class="td_4" colspan="3" width="35%">获赠总金额：<span><s>${sum_recommend==null?"0.00":sum_recommend }</s>元</span></td>
             </tr>
          </table>
       </div>
       <div class="table_1">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td class="td_1 td_2" width="35%" colspan="3">已邀请注册好友昵称名单</td>          
             </tr>
             <tr class="tr_2">
                <td class="td_1" width="35%">编号</td>
                <td class="td_1" width="35%">好友昵称</td>
                <td width="30%">注册时间</td>             
             </tr>
             <c:forEach items="${user_list}" var="user_l" varStatus="a">
             	<tr class="tr_2">
                <td class="td_1" width="35%">${a.count }</td>
                <td class="td_1" width="35%">${user_l.m.nickname }</td>
                <td width="30%">${user_l.m.add_time }</td>             
             </tr>
             </c:forEach>
             <c:if test="${empty user_list}">
	             <tr class="tr_3">
	                <td class="td_2" colspan="3" width="35%">您现在还没有邀请好友，赶快去邀请吧！</td>          
	             </tr>
             </c:if>
             
          </table>
       </div>
       <div class="con">
          <p>活动说明：</p>  
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp您只需要点击“复制链接”按钮，将复制的推广链接发送给您想邀请的小伙伴，当小伙伴通过这个链接注册并理财或者借款成功时，您就能立即获得现金奖励。获得的现金奖励即可以用于理财投资也可以即刻提现哦！
         <br />
      &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp每邀请到一个好友理财投标或者贷款成功，立即送您20元现金。而且当您邀请的好友理财投资成功之后，您邀请的好友投资额的1%将作为对您的奖励立即送达您的账号。理财投标包括精英散标和联富宝，但是不包括免费的新手标。
      	<br /><span style="color:black;">活动时间：2014年12月12号到2015年12月12号</span>
       </div>
       
       <div class="up_2"></div>
       <div class="up_1">活动二、</div>
       <div class="up"><span>投标立即送现金：</span><a href="<%=__ROOT_PATH__ %>/index/activity/to_account_activity_financial.html">（点击查看详情）</a></div>
       <div class="table_1">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td class="td_1 td_2" width="35%">投资时间</td>
                <td class="td_1 td_2" width="35%">投资总额</td>
                <td class="td_2" width="30%">获赠的金额</td>            
             </tr>
             <c:forEach items="${activity_record_list2}" var="record_financial">
             <tr class="tr_2">
             <c:if test="${record_financial.r.type == 2}">
                <td class="td_1" width="35%">${record_financial.r.add_time }</td>
                <td class="td_1" width="35%">${record_financial.r.amount }元</td>
                <td width="30%">${record_financial.r.reward_amount }元</td>   
             </c:if>        
             </tr>
             </c:forEach> 
             <c:if test="${activity_record_list == null}">
             <tr class="tr_3">
                <td class="td_1" colspan="3" width="35%">您现在还没有投标记录，赶快去投标吧！</td>          
             </tr>
             </c:if>
             <tr>
                <td class="td_4" colspan="3" width="35%">获赠总金额：<span><s>${sum_financial==null?"0.00":sum_financial }</s>元</span></td>
             </tr>
          </table>
       </div>
       <div class="con">
          <p>活动说明：</p> 
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 凡成功参与平台精英散标或者联富宝理财投资（新手标除外）的客户，投资后即刻赠送您投资总额的1%现金。赠送金额在您投资成功后立即到账，赠送金额即可以直接提现也可以再次续投
      <br /><span style="color:black;">活动时间：2014年12月12号到2015年3月12号</span>
       </div>
       
    </div>
           
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>


<script>
</script>


