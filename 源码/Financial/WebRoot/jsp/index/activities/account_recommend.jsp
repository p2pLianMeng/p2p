<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">  

<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>

<%
String __ROOT_PATH__THIS = WebApp.getWebRootPath(request);
String __PUBLIC____THIS = WebApp.getPublicPath(request,__ROOT_PATH__THIS);
 %>
<script type="text/javascript" src="<%=__PUBLIC____THIS %>/zclip/js/jquery.zclip.min.js"></script>
<script type="text/javascript">
$(function(){
   
	$('#copy_button').zclip({
		path: '<%=__PUBLIC____THIS %>/zclip/js/ZeroClipboard.swf',
		copy: function(){
			return $('#url').val();
		},
		afterCopy: function(){
		
			$("<span id='msg'/>").insertAfter($('#copy_button')).text('复制成功').fadeOut(2000);
		}
	});
	
	
});
</script>
<div class="activities">
  
  <div class="activities_con">
     <div class="activities_top">
        <div class="head"></div>        
       <div class="center">成功邀请一个好友送<span><b>20</b>元现金</span><br />
好友参与理财<span>再送</span>理财本金的<span><b>1</b>%</span>
</br> <span style="color:black;font-size:40px;">永久哦！您的好友理财投标总额的1%都归您！</span>
</div> 
        <div class="center_2">
           <input class="button_1" id="huodong2" type="button" />
        </div> 
        <div class="center_3"></div> 
        <dl>
           <dt></dt>
           <dd>2014年<span>12</span>月<span>12</span>日-2015年<span>12</span>月<span>12</span>日</dd>
        </dl>  
        <dl>
           <dt style="background-position:0 -140px"></dt>
           <dd>
              1、推荐人不限注册时间，不限投资条件，所有用户均可参与；<br />
              2、用户每邀请到一个好友理财投标或者贷款成功，即送20元现金；<br />
              3、被推荐人投资理财成功，再奖励推荐人被推荐人投资本金的1%；<br />
              4、被推荐人投资成功后，奖励以现金方式直接发放到账，且奖励可以直接提现；<br />
              5、理财投标包括精英散标、联富宝等，但不包括免费的新手标。
           </dd>
        </dl> 
        <dl>
           <dt style="background-position:0 -210px"></dt>
           <dd>
             1、邀请成功的判断标准为被邀请人贷款成功、或者被邀请人单次投标满100元及以上；<br />
             2、禁止用户通过任何方式作弊，对于各类破坏活动的违规行为，联富金融有权取消任何违规的奖励；<br />
             3、在法律范围内，此活动最终解释权归联富金融所有，如您有任何疑问，欢迎致电4006-888-923。
           </dd>
        </dl> 
        <div class="center_2">
           <input class="button_1" id="huodong"  type="button" />
        </div> 
        <div class="center_2 center_4"><input class="button_2" id="zhuce" type="button" /></div> 
        <div class="down"></div>
     </div>
  </div>
  
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>


<script>
	$("#zhuce").click(function(){
		window.location.href="<%=__ROOT_PATH__ %>/index/registration.html";
	});
	$("#huodong").click(function(){
		window.location.href="<%=__ROOT_PATH__ %>/index/activity/to_activity.html";
	});
	$("#huodong2").click(function(){
		window.location.href="<%=__ROOT_PATH__ %>/index/activity/to_activity.html";
	});
</script>


