<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">   
  </head>
  
  <body>
  <%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="about_left">
      		${list_str}
    </div>
    <div class="about_right">
      <h2>资费说明</h2>
      <h3><s>【注册全免】</s><br />
         所有用户免收注册费、身份验证费、短信息费用、资金托管费等杂费。<br />
         <s>【充值0手续费】</s><br />
         （1）所有用户充值免手续费，7*24全天候即时到账。<br />
         （2）采用在线充值方式，支持国内16家银行的借记卡和信用卡个人网银在线支付。<br />
         <s>【提现0手续费】</s><br />
         （1）所有用户提现免手续费，24小时内到账，节假日顺延。<br />
         （2）用户自充值之日起于15日之内，不投标的提现，将扣除提现金额的0.5%作为手续费。<br />
         （3）提现单笔下限100元，100元以上即可提现。<br />
         <s>【续投奖励】</s><br />
         （1）月标回款后续投的，奖励续投金额的0.2%。<br />
         （2）客户续投后，请及时通知客服添加奖励。同时客服也会在每个工作日下午5：00以后统一添加当日奖励。
      </h3>       
    </div>
  </div>
  
</div>
    </div>
  </div>
  
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>

</body>
</html>

