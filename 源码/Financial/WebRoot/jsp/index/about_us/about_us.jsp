<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		

		<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">   

		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>

	</head>

	<body>

		
<%@ include file="/jsp/index/index_top.jsp"%>
		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="about_left">
					<%-- 
					<ul>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=intro" class="hover">公司简介</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=team" >管理团队</a>
						</li>
						
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=consult">最新资讯</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=notice">网站公告</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=agreement">网站协议</a>
						</li>
					
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=join_us">加入我们</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=contact">联系我们</a>
						</li>
						
					</ul>
					--%>
					${list_str}
				</div>
				<div class="about_right">
					<h2>
						公司简介
					</h2>
					<h3>
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp联富金融（www.lfoll.com）是由银行、担保、IT、电子商务等领域资深专业人士组织成立，现在总部设在广州市白云区。 由广州天的富网络科技有限公司运营,我们致力于为广大缺乏投资渠道的人们提供一个安全、诚信、低风险、回报稳定的理财渠道。<br/>
						 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp在金融产品的设计不断变得让人眼花缭乱、各种理财渠道推销鱼龙混杂的今天，联富金融希望不仅能提供给您更简单轻松的理财体验，还能享受到更好的理财回报和优质服务。 联富金融引入国外先进的信用管理理念，结合中国的社会信用状况，为理财客户和贷款客户提供包括信用咨询、评估、信贷方案制定等多方面专业的全程信用管理和财富管理服务。<br/> 
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp通过联富金融网络借贷平台，使所有客户之间的信贷交易行为变得更加安全、高效、专业、规范。联富金融对理财客户设立本息保障计划，在借款人逾期时由本息保障金垫付投资本息，为理财客户创造了一个绝对安全的理财环境。 我们为了让理财更加轻松安全，让贷款更加便捷高效而努力。
										 
					   <div class="h3_head">资质证明</div> 
				       <div class="h3_head2">我们正规，能够安全、便捷、稳健的完成您投资理财的托付</div> 
				       <div class="h3_img"><a href="http://www.lfoll.com/__PUBLIC__/images/business_licence.jpg" target="_blank"><img src="<%=__PUBLIC__ %>/images/business_licence_1.jpg" /></a><span>营业执照</span></div>
				       <div class="h3_img"><a href="http://www.lfoll.com/__PUBLIC__/images/tax_registration_certificate.jpg" target="_blank"><img src="<%=__PUBLIC__ %>/images/tax_registration_certificate_1.jpg" /></a><span>税务登记证</span></div>
				       <div class="h3_img"><a href="http://www.lfoll.com/__PUBLIC__/images/organazation_code_certificate.jpg" target="_blank"><img src="<%=__PUBLIC__ %>/images/organazation_code_certificate_1.jpg" /></a><span>组织机构代码证</span></div>
				       <div class="h3_img"><a href="http://www.lfoll.com/__PUBLIC__/images/account_open_licenses.jpg" target="_blank"><img src="<%=__PUBLIC__ %>/images/account_open_licenses_1.jpg" /></a><span>开户许可证</span></div>
				       <div class="h3_img"><a href="http://www.lfoll.com/__PUBLIC__/images/top_level_domain_certificate.jpg" target="_blank"><img src="<%=__PUBLIC__ %>/images/top_level_domain_certificate_1.jpg" /></a><span>顶级域名证书</span></div> 
				       <div class="h3_img">
					       	<a href="https://search.szfw.org/cert/l/CX20140804008606008701" target="_blank"><img src="<%=__PUBLIC__ %>/images/trusted_site_certification_1.jpg" /></a>
					       	<span>可信网站认证</span>
				       </div>
      
					</h3>
					<!-- <img src="<%=__PUBLIC__ %>/images/50000000.jpg" style="width:500px;height:700px;padding-left: 140px;" alt="营业执照" /> -->
				</div>
			</div>

		</div>



		<%@ include file="/jsp/index/index_foot.jsp" %>


	</body>
</html>
