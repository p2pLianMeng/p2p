<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>

</head>
<body>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__%>/images/logo.png" /></a></h1>
 <div class="agreement_head"><span>联富宝服务计划预约服务协议</span><a href="<%=__ROOT_PATH__ %>/user/financial/contractdownload/dowon_PDF_by_templateId.html?template_id=2"><img src="<%=__PUBLIC__%>/images/agreement_1.png" />点击下载</a></div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="agreement_con">
      <p class="p_1">联富宝服务计划预约服务协议</p>
      <h3>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <td colspan="4">甲方：____________</td>
             </tr>
             <tr>
                <td colspan="4">证件类型：__________</td>
             </tr>
             <tr>
                <td colspan="4">证件号码：__________</td>
             </tr>
             <tr>
                <td class="td_1" colspan="4">&nbsp;</td>
             </tr>
             <tr>
                <td class="td_5" colspan="4">乙方：广州天的富网络科技有限公司</td>
             </tr>
             <tr>
                <td class="td_5" colspan="4">住所：广州市白云区石井大道银马广场C栋610室</td>
             </tr>
             <tr>
                <td class="td_1" colspan="4">&nbsp;</td>
             </tr>
             <tr>
                <td colspan="4">本协议是《联富宝出借咨询与服务协议》、《联富宝服务计划授权委托书》的补充协议，具有与《联富宝出借咨询与服务协议》、《联富宝服务计划授权委托书》同等法律效力；为了让乙方能够更好的为甲方提供预约服务，现双方达成如下一致意见，以兹信守。</td>
             </tr>
             <tr>
                <td class="td_1" colspan="4">&nbsp;</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">1、</td>
                <td colspan="3">权利与义务：</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（1）甲方预约购买乙方联富宝理财产品，乙方代甲方在联富金融平台上将甲方账户内资金一次性全部（或部分）出借给借款人后，开始为甲方计算投资期间收益；</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（2）甲方在此不可撤销授权乙方在联富金融平台上、在本协议生效期间将甲方账户内资金、回款再出借资金一次性全部（或部分）出借给借款人；</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">（3）甲方不可撤销授权乙方在联富金融网站注册，注册成功后，乙方将成为联富金融网站的注册用户，享受本协议及《联富宝出借咨询与服务协议》项下权利和服务；</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">2、</td>
                <td colspan="3">生效期限：</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">本协议有效期限自生效之日起至甲方本金、收益完全退出联富宝产品（或紧急退出）之日止。</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">3、</td>
                <td colspan="3">各方均对本协议进行操作成功确认时，本协议即成立并生效。</td>
             </tr>
             <tr>
                <td width="8%">&nbsp;</td>
                <td colspan="3">本协议经甲方在联富金融网络平台以线上点击确认的方式、乙方统一在联富金融后台确认的方式订立。甲方委托乙方保管本协议。</td>
             </tr>
             <tr>
                <td class="td_2" width="8%">4、</td>
                <td colspan="3">本协议项下词语之含义以《联富宝出借咨询与服务协议》约定为准。</td>
             </tr>
          </table> 
      </h3>       
    </div>
  </div>
  
</div>



<div class="bottom_2">   
   <div class="content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="64%" class="td_1"><img src="<%=__PUBLIC__%>/images/footer_dh_2.png" />4006-888-923</td>
            <td width="36%" class="td_2">
               粤ICP备14022597号-1 可信网站 CopyRight © 2014<br />
               广州天的富网络科技有限公司版权所有     
            </td>
         </tr>
      </table>
   </div>
</div>

</body>
</html>

