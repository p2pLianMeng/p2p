<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>

</head>
<body>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__%>/images/logo.png" /></a></h1>
 <div class="agreement_head"><span>联富宝服务（3月期）退出确认书</span><a href="<%=__ROOT_PATH__ %>/user/financial/contractdownload/dowon_PDF_by_templateId.html?template_id=9"><img src="<%=__PUBLIC__%>/images/agreement_1.png" />点击下载</a></div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="agreement_con agreement_con2">
      <p class="p_1">联富宝服务（3月期）退出确认书</p>
      <p class="p_3 p_8">本人于_____年_____月_____日在联富金融（网址:www.lfoll.com）加入了服务编号为______的联富宝服务，并签署《联富宝出借咨询及服务协议》，委托联富金融促成本人与网站上的借款人成立借款合同关系，并将本人在联富金融账户的出借资金实际支付给相应的借款人。</p>
      <p>&nbsp;</p>
      <p class="p_3 p_8">本人现因急需资金，向联富金融请求将出借资金退回到本人联富金融账户，并同意向联富金融支付该联富宝服务总资产的1.5%作为紧急退出费（具体金额以网站实际数据的计算为准）。</p>
      <p>&nbsp;</p>
      <p class="p_3 p_8">本确认书自本人签字之日生效。</p>
      <p>&nbsp;</p>
      <p class="p_5">
          姓名：_____________<br />
          身份证号：_____________<br />
          _________年____月____日
      </p>
    </div>
  </div>
  
</div>



<div class="bottom_2">   
   <div class="content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="64%" class="td_1"><img src="<%=__PUBLIC__%>/images/footer_dh_2.png" />4006-888-923</td>
            <td width="36%" class="td_2">
               粤ICP备14022597号-1 可信网站 CopyRight © 2014<br />
               广州天的富网络科技有限公司版权所有     
            </td>
         </tr>
      </table>
   </div>
</div>

</body>
</html>
