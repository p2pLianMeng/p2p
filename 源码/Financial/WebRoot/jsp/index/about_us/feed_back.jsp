<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<title>${applicationScope.title}</title>
		<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">   
		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>

	</head>

	<body>

		
<%@ include file="/jsp/index/index_top.jsp"%>
		<span id="ny_pic"></span>

		<div class="about">
		  <div class="about_con">
		    <div class="about_left">
		     ${list_str}
		    </div>
		    <div class="about_right">
		       <div class="opinion_up">联富金融意见反馈</div>
		       <div class="opinion_up2"><span>*</span>温馨提示:请登录后尝试提交意见反馈,<a href="<%=__ROOT_PATH__ %>/index/to_login.html">立即登录</a></div>
		       <div class="opinion">
		          <dl>
		             <dt>选择类型：</dt>
		             <dd>
		                <select id="type" name="type">
		                   <option value="0">==请选择类型==</option>
		                   <option value="1">建议</option>
		                   <option value="2">投诉</option>
		                   <option value="3">咨询</option>
		                   <option value="4">意见</option>
		                   <option value="5">其他</option>
		                </select>
		             </dd>
		          </dl>
		          <dl>
		             <dt>意见反馈：</dt>
		             <dd>
		                <textarea name="content" id="content"></textarea>
		             </dd>
		          </dl>
		          <dl>
		             <dt>验证码：</dt>
		             <dd>
		                <input class="text" name="verification_code" id="verification_code" type="text" />
		                <a href="javascript:void(0);"> <img id="CheckCodeServlet" width="122"
										height="42" src="<%=__ROOT_PATH__%>/code.jpg"
										onclick="refresh();" style="border: 1px; font-color: white;" />
						</a>
						<a href="javascript:void(0);" onclick="refresh();">换一张</a>
		             </dd>
		          </dl>
		          <p><a id="tijiao">提交意见</a></p>
		       </div>
		    </div>
		  </div>
		  
		</div>
		<%@ include file="/jsp/index/index_foot.jsp" %>
	</body>
</html>
<script>
	function refresh(){
		var CheckCodeServlet=document.getElementById("CheckCodeServlet");
		CheckCodeServlet.src="<%=__ROOT_PATH__%>/code.jpg?id="+ Math.random();
	}
	
	$("#tijiao").click(function(){
		var type = $("#type").val();
		var content = $("#content").val();
		if(content==null||content==""||content.length<10||content.length>500){
			showMessage(["提示","请输入10-500个字"]);
			return;
		}
		var verification_code = $("#verification_code").val();
		if(verification_code==null||verification_code==""){
			showMessage(["提示","请填写验证码"]);
			return;
		}
		$.post(
		'<%=__ROOT_PATH__%>/user/usercenter/save_suggest.html',
		{
			"type":type,
			"content":content,
			"verification_code":verification_code
		},
			function(data){
				if(data == 1){
					showMessage(["提示","请选择反馈类型"]);
				}else if(data == 2){
					showMessage(["提示","请输入10-500个字"]);
				}else if(data == 3){
					showMessage(["提示","请填写验证码"]);
				}else if(data == 4){
					showMessage(["提示","验证码错误"]);
				}else if(data == 5){
					showMessage(["提示","提交反馈失败"]);
				}else if(data == 6){
					showMessage(["提示","提交反馈成功"]);
					$("#content").val("");
					$("#type").val("0");
					$("#verification_code").val("");
					//window.location.href="<%=__ROOT_PATH__%>/user/usercenter/to_user_suggest.html";
				}else if(data == 7){
					window.location.href="<%=__ROOT_PATH__%>/index/to_login.html";
				}
			}
		);
	});
</script>
