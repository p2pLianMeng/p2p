<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">   
		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>

	</head>

	<body>

		
<%@ include file="/jsp/index/index_top.jsp"%>
		<span id="ny_pic"></span>

		<div class="about">
	  <div class="about_con">
	    <div class="about_left">
	      ${list_str}
	    </div>
	    <div class="about_right">
	      <h2>专家顾问</h2>
	       <div class="advisor">联富金融在快速成长的道路上需要启迪与指导。联富金融专家顾问团队，汇聚了风险控制、学术研究、企业管理等各领域的精英和专家。权威铸就信任，经验创造财富，联富金融专家顾问团队将引领联富金融一路前行，乘风破浪。</div>
	       <div class="advisor_1">
	          <img src="<%=__PUBLIC__%>/images/advisor_1.jpg" />
	          <div class="con_up"><span>程宇</span>晨兴资本合伙人</div>
	          <div class="con_down">程宇现任晨兴资本(Morningside Ventures)合伙人。作为具有高度创业精神的早期风险投资机构，晨兴是小米科技、YY、UC优视、迅雷、凤凰新媒体，以及搜狐(NASDAQ:SOHU)，携程(NASDAQ:CTRP)，第九城市(NASDAQ:NCTY)等大批优秀公司的主要投资人。自2012年加入晨兴资本以来，程宇结合他在风险投资、创业和商业拓展的经验，主导投资了大搜车、小猪短租等多家高成长性的创业企业并帮助其快速成长，从早期开始支持了很多优秀的创业者。程宇的风险投资职业生涯开始于德丰杰龙脉基金(DFJ Dragon)，在加入晨兴之前，程宇曾任凯旋创投(Keytone Ventures)副总裁。程宇毕业于东南大学电子工程专业并获得工学学士学位。             
	          </div>
	       </div>
	       <div class="advisor_1">
	          <img src="<%=__PUBLIC__%>/images/advisor_2.jpg" />
	          <div class="con_up"><span>任晋生</span>先声药业集团董事局主席</div>
	          <div class="con_down">任晋生，纽交所上市公司先声药业（NYSE：SCR）的创始人，现任先声药业董事会主席；任晋生1982年毕业于南京中医药大学中药专业；1982~1992年就职于启东盖天力制药公司，历任技术员、副总经理；1992~1995年担任江苏医药工业公司部门经理；1995年3月创立先声药业；2003年获澳洲麦考瑞大学经济学硕士学位；现任中国药科大学客座教授、南京中医药大学客座教授、中国西北大学兼职教授。            
	          </div>
	       </div>
	       <div class="advisor_1">
	          <img src="<%=__PUBLIC__%>/images/advisor_3.jpg" />
	          <div class="con_up"><span>吴岚</span>北京大学金融数学系系主任、博士生导师</div>
	          <div class="con_down">吴岚北京大学金融数学系系主任、博士生导师。1990年至今在北京大学数学科学学院工作，以精算学与金融风险管理为主要研究方向。为中国精算师协会理事、中国保监会委员、教育委员会委员。参与研究国家自然科学基金委精算重点项目《保险信息处理与精算数学理论和方法》；并在2007年7月-2012年12月主持研究了国家科技部国家重点基础研究发展计划（973计划）项目（课题）《金融风险控制中的定量分析与计算》、《银行与保险业中的风险模型与数据分析》等项目（课题）。         
	          </div>
	       </div>
	       <div class="advisor_1">
	          <img src="<%=__PUBLIC__%>/images/advisor_4.jpg" />
	          <div class="con_up"><span>李斌</span>香港宏旺国际有限公司董事长</div>
	          <div class="con_down">李斌香港宏旺国际有限公司董事长，宏旺电子（深圳）有限公司、美耀投资有限公司董事长，株洲市地球村房地产有限公司董事长。 2003年大学毕业后参加工作，2004年开始创业，从最开始设计了二款U盘真皮外壳，被国外公司下了几十万的订单之后迅速购买SMT贴片机从事U盘，MP3产品的生产，主要出口海外。以后规模扩大后又成立了塑胶五金厂，皮具厂，硅胶厂，喷油丝印厂，同时在香港成立了IC芯片的代理公司。最近几年李斌看准了时机，在株洲市荷塘区投资了1741亩的荷塘大道房地产开发项目，这个是至今株洲在开发十大项目中排在首位的项目。没有比人更高的山，没有比脚更长的路，是李斌一直的座右铭。在多年的经商当中，无论在企业经营，还是在商业模式研究，甚至在法律等方面都积累了不少经验。     
	          </div>
	       </div>
	    </div>    
	    </div>
	  </div>
	  
	</div>



		<%@ include file="/jsp/index/index_foot.jsp" %>


	</body>
</html>
