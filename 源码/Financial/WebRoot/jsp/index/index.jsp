<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="pinggu-site-verification" content="522bb1a273f4502407fbc44a47e62995" />

		<title>${applicationScope.title}</title>
		<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">
	</head>

	<body>
		<%--
	<script type="text/javascript">
			if (self != top) {
				window.parent.document.location.href = document.location.href;
			}
	</script>
--%>
		<%@ include file="/jsp/index/index_top.jsp"%>
		<div class="banner">
			<div class="banner_top"></div>
			<link rel="stylesheet" type="text/css" href="css/jquery.jslides.css"
				media="screen" />
			<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
			<script type="text/javascript" src="js/jquery.jslides.js"></script>
			<div id="full-screen-slider">
				<ul id="slides">
					<li
						style="background: url('<%=__PUBLIC__ %>/images/06.jpg') no-repeat center top">
						<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_elite.html"></a>
					</li>
					<li
						style="background: url('<%=__PUBLIC__ %>/images/04.jpg') no-repeat center top">
						<a href="<%=__ROOT_PATH__ %>/index/activity/to_recommend.html"></a>
					</li>
					<li
						style="background: url('<%=__PUBLIC__ %>/images/05.jpg') no-repeat center top">
						<a href="<%=__ROOT_PATH__ %>/index/activity/to_account_activity_financial.html"></a>
					</li>
					<li
						style="background: url('<%=__PUBLIC__ %>/images/01.jpg') no-repeat center top">
						<a href="#"></a>
					</li>
					<li
						style="background: url('<%=__PUBLIC__ %>/images/02.jpg') no-repeat center top">
						<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_creditor_right.html"></a>
					</li>
					<li
						style="background:url('<%=__PUBLIC__ %>/images/03.jpg') no-repeat center top">
						<a href="#"></a>
					</li>
				</ul>
			</div>
		</div>

		<div class="con1">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="tr_1">
					<td width="33%">
						<span id="join_count">${user_num}位</span>
					</td>
					<td width="34%">
						<span id="invest_money">${investment_amount}万</span>
					</td>
					<td width="33%">
						<span id="earnings">${investors_income}万</span>
					</td>
				</tr>
				<tr>
					<td>
						智慧的投资人加入联富金融(元)
					</td>
					<td>
						累积成功投资金额(元)
					</td>
					<td>
						为投资人带来预期收益(元)
					</td>
				</tr>
			</table>
		</div>

		<div class="con1_1">
			<table  width="100%" border="0" cellspacing="0"
				cellpadding="0">
				<tr class="tr_1">
					<td class="td_1">
						安全保障，理财无忧
					</td>
					<td>
						低门槛，高收益
					</td>
					<td>
						多种期限，投资更灵活
					</td>
				</tr>
				<tr class="tr_2">
					<td class="td_1 td_2">
						&bull;&nbsp;
						<span>风险保证金</span>&nbsp;保驾护航
					</td>
					<td class="td_3">
						&bull;&nbsp;投资门槛仅&nbsp;
						<span>50元</span>
					</td>
					<td class="td_4">
						&bull;&nbsp;多种借款期限（
						<span>3个月</span>至
						<span>36个月</span>）
					</td>
				</tr>
				<tr class="tr_2">
					<td class="td_1 td_2">
						&bull;&nbsp;
						<span>6重风险防控 </span>&nbsp;收益安心有保障
					</td>
					<td class="td_3">
						&bull;&nbsp;年化收益高达&nbsp;
						<span>16%-20%</span>
					</td>
					<td class="td_4">
						&bull;&nbsp;多种还款方式及&nbsp;
						<span>债权转让</span>&nbsp;功能
					</td>
				</tr>
				<tr class="tr_2">
					<td class="td_1">
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
				<tr class="tr_3">
					<td class="td_1">
						<img src="<%=__PUBLIC__ %>/images/con1_pic1.jpg" />
					</td>
					<td>
						<img src="<%=__PUBLIC__ %>/images/con1_pic2.jpg" />
					</td>
					<td>
						<img src="<%=__PUBLIC__ %>/images/con1_pic3.jpg" />
					</td>
				</tr>
			</table>
		</div>
		
	<div class="index_flb">
	   <div class="left">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	         <tr class="tr_1">
	            <td colspan="3" id="bid_name"><span>联富宝</span>V121003-0003期</td>
	         </tr>
	         <tr class="tr_2">
	            <td width="35%"><span id="least_money">10000</span>元</td>
	            <td width="35%"><span id="rate">10</span>%</td>
	            <td width="30%"><span id="closed_period">12</span>月</td>
	         </tr>
	         <tr class="tr_3">
	            <td class="td_1">起投金额</td>
	            <td class="td_1">预期年化收益率</td>
	            <td>封闭期</td>
	         </tr>
	      </table>
	   </div>
	   <div class="right">
	      <p id="total_money">总发售金额:800万</p>
	      <p><input type="button" id="buy_fix_bid" value="查看详情&nbsp;&rarr;" /></p>
	   </div>
	   <img id="img"/>
	</div>
	<div class="ps_img"><a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_lfoll_invest.html"><img src="<%=__PUBLIC__ %>/images/lfb1.jpg" /></a></div>
		
		<div class="con2">
			<h2>
				<p>
					投资列表
				</p>
				<span><a
					href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html">查看更多投资理财项目</a>
				</span>
			</h2>
			<h3>
				<table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="29%" height="45" class="con2_kk">借款标题</td>
						<td width="10%" height="45" class="con2_kk">信用等级</td>
						<td width="12%" height="45" class="con2_kk">年利率</td>
						<td width="15%" height="45" class="con2_kk">金额</td>
						<td width="10%" height="45" class="con2_kk">期限</td>
						<td width="11%" height="45" class="con2_kk">进度</td>
						<td width="13%" height="45" class="con2_kk">&nbsp;</td>
					</tr>
					
					<!-- 
					<tr>
						<td height="45" class="con2_kk"><img src="<%=__PUBLIC__ %>/images/icon1.jpg" /><a href="#">购车置业</a></td>
						<td height="45" class="con2_kk"><span class="con2_yuan">A</span></td>
						<td height="45" class="con2_kk">13.00%</td>
						<td height="45" class="con2_kk">18,000元</td>
						<td height="45" class="con2_kk">24个月</td>
						<td height="45" class="con2_kk"><span class="hongse">23.87%</span></td>
						<td height="45" align="center" class="con2_kk"><p class="con2_anniu" style="background: #e85e48;">未满标</p></td>
					</tr>
					 -->
					

				</table>
			</h3>
		</div>

		<div class="con3">
			<h2>
				<p class="hover">
					最新资讯
				</p>
				<p>
					网站公告
				</p>
				<span><a href="<%=__ROOT_PATH__ %>/index/about/about_page.html?flag=consult">查看更多</a>
				</span>
			</h2>
			<ul style="display: block;">
				<c:if test="${not empty least_consult_record_list}">
					<c:forEach items="${least_consult_record_list}" var="record">
						<li>
							<span>${record.r.add_time_str}</span>
							<p></p>
							<a href="<%=__ROOT_PATH__%>/index/about/last_consult_detail.html?id=${record.r.id}">${record.r.content}</a>
						</li>
					</c:forEach>
				</c:if>
				<c:if test="${empty least_consult_record_list}">
						<li>
							<span></span>
							<p></p>
							<a>${tips_1}</a>
						</li>
				</c:if>
			</ul>
			<ul>
				<c:if test="${not empty site_notic_record_list}">
					<c:forEach items="${site_notic_record_list}" var="record">
						<li>
							<span>${record.r.add_time_str}</span>
							<p></p>
							<a href="<%=__ROOT_PATH__%>/index/about/site_notice_detail.html?id=${record.r.id}">${record.r.content}</a>
						</li>
					</c:forEach>
				</c:if>
				<c:if test="${empty site_notic_record_list}">
						<li>
							<span></span>
							<p></p>
							<a>${tips_2}</a>
						</li>
				</c:if>
			</ul>
		</div>

		<%@ include file="/jsp/index/index_foot.jsp"%>


		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/index.js"></script>

	</body>
</html>
<script>
	
</script>

