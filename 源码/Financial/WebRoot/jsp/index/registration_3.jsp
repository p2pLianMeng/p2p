<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=__ROOT_PATH__ %>">
    
    <title>${applicationScope.title}</title>
	   <link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
    <link href="<%=__PUBLIC__ %>/style/jisuanji.css" rel="stylesheet" type="text/css" />
    <link href="<%=__PUBLIC__ %>/style/one.css" rel="stylesheet" type="text/css" />
    <link href="<%=__PUBLIC__ %>/style/prod.css" rel="stylesheet" type="text/css" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<link href="<%=__PUBLIC__ %>/css/easydialog.css" rel="stylesheet"  type="text/css" />
 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/loginDialog.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/pngAlaph.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery-1.8.0.min.js"></script>
    
	<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">
  </head>
  
  <body>
   <div class="head_top">
  			<h1>
				<a href="<%=__ROOT_PATH__ %>/index/index.html"><img src="<%=__PUBLIC__ %>/images/logo.png" /></a>
			</h1>
  <div class="right">注册</div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="registration registration_2">
        <div class="registration_top">
           <div class="up">
              <span style="background:#ed5050; color:#fff;">1</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">2</span>
              <s></s>
              <span>4</span>
           </div>
           <div class="down">
              <span style="color:#cc0000; margin-left:17px;">填写账户信息</span>
              <span style="margin-left:135px;">手机信息验证</span>
              <span style="margin-left:130px;">注册成功</span>
           </div>
        </div>
        <div class="cellphone">
           <div class="cellphone_top">短信已发送至您手机，请输入短信中的验证码，确保您的手机号真实有效。<span></span></div>  
           <div class="con">
              <label class="label_1"><span>*</span>&nbsp;手机号确认：</label>
              <input  type="text" value="${phone }"  name="phone" id="phone" class="label_p"/>
             <!--  <input class="button" value="修改" type="button" name="nickName"> -->
              <p id="1" class="label_p1" style="display:none;color:red;">手机号为空</p>
              <p id="18" class="label_p1" style="display:none;color:red;">请按正常流程注册</p>
              <p id="13" class="label_p1" style="display:none;color:red;">手机号格式有误</p>
              <p id="11" class="label_p1" style="display:none;color:red;">该手机号已经被使用</p>
              <p id="12" class="label_p1" style="display:none;color:red;">修改成功</p>
              <p id="14" class="label_p1" style="display:none;color:red;">修改失败</p>
              <p id="15" class="label_p1" style="display:none;color:red;">操作有误</p>
              <p id="17" class="label_p1" style="display:none;color:red;">手机验证码发送失败</p>
           </div> 
           <div class="con" style="position:relative" >
              <label class="label_1"><span>*</span>&nbsp;验证码：</label>
					<input class="label_p"  id="binding_phone_code" style="margin-bottom: 5px;" type="text">
					<input class="button_2" style="float:left;margin-left: 12px;margin-top: 0px;width:100px;height:42px; background-color:#ed5050;;color:white;" type="button" id="binding_phone_send_message" value="发送验证码" />
					<input class="button_2" style="float:left;display:none;margin:0 0 0 12px;width:100px;height:42px; background-color:#ed5050;;color:white;" type="button" id="binding_phone_send_voice" value="语音验证码" />
					<p id="2" class="label_p1" style="display:none;color:red;position:absolute;top:5px; left:420px;">验证码输入错误</p>
              		<p id="21" class="label_p1" style="display:none;color:red;position:absolute;top:5px; left:420px;">验证码为空</p>
					<span id="bingd_phone" style="display:none;position:absolute;top:45px; left:100px;">没有收到短信验证码 ?<a style="corsur:pointer;color:red;" id="bingd_phone_send">发送语音验证码</a></span><br/>
           </div>   
           <div class="con">
              <label class="label_1"></label>
              <p class="label_p1" id="yanzheng"><a  onclick="email_v();" href="javascript:void(0);">验&nbsp;&nbsp;证</a></p><br/>
           </div> 
           	<div style="margin-top: 280px;margin-left: -10px;background-color: #cbe1e9;width:460px;Border-radius:10px;padding-left: 10px;">如果90秒内没有收到验证码，请检查手机号码是否正确或重新获取验证码</div>
          	 <div class="con">
              <label class="label_1"></label>
           </div>
        </div>
     </div>
  </div>
</div>
<input id="is_submitted" type="hidden" name="is_submitted" value="0" />
<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>
<script type="text/javascript">
	$("#bingd_phone_send").click(function(){
		$("#binding_phone_send_message").hide();
		$("#binding_phone_send_voice").show();
	});
	
	//绑定手机发送验证码
	//倒计时
					var a = 1;
					var b;
					function countdown1(){
						if(a==90){
							a= 0;
							clearTimeout(b);
							$("#binding_phone_send_message").val("发送验证码");
							$("#bingd_phone").show();
							return;		
						}else{
							$("#binding_phone_send_message").val((90-a)+"秒后可重发");
							$("#binding_phone_send_message").css("color","white");
							$("#binding_phone_send_message").attr("disabled",true);
							a++;
							b = setTimeout("countdown1()",1000);
						}
					}
					
			
	$("#binding_phone_send_message").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][34578][0-9]{9}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendmessage/binding_phone_send_message.html',
			{
				"phone":phone
			},
			function(data){
				if(data == 4){
					showMessage(["提示","手机号不能为空"]);
				}else if(data == 1){
					showMessage(["提示","该手机号已经被绑定"]);
				}else if(data == 2){
					showMessage(["提示","请使用语音验证码"]);
					$("#bingd_phone").show();
				}else{
					//showMessage(["提示","短信验证码发送成功"]);
					countdown1();
				}
			},'html','application/x-www-form-urlencoded; charset=utf-8');
	});
	$("#bingd_phone_send").click(function(){
		$("#binding_phone_send_message").hide();
		$("#binding_phone_send_voice").show();
	});
	var a1 = 1;
	var b1;
	function countdown11(){
		if(a1==90){
			a1= 0;
			clearTimeout(b1);
			$("#binding_phone_send_voice").val("语音验证码");
			$("#bingd_phone").show();
			$("#binding_phone_send_voice").attr("disabled",false);
			return;		
		}else{
			$("#binding_phone_send_voice").val((90-a1)+"秒后可重发");
			$("#binding_phone_send_voice").css("color","white");
			$("#binding_phone_send_voice").attr("disabled",true);
			a1++;
			b1 = setTimeout("countdown11()",1000);
		}
	}
	$("#binding_phone_send_voice").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][34578][0-9]{9}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendvoice/binding_phone_send_voice.html',
				{
					"phone":phone
				},
				function(data){
					if(data == 4){
						showMessage(["提示","手机号不能为空"]);
					}else if(data == 1){
						showMessage(["提示","该手机号已经被绑定"]);
					}else if(data == 2){
						showMessage(["提示","验证码发送失败"]);
					}else if(data == 5){
						showMessage(["提示","操作有误"]);
					}else{
						//showMessage(["提示","短信验证码发送成功"]);
						countdown11();
					}
				},'html','application/x-www-form-urlencoded; charset=utf-8');
	});
	
	
	
	
	
	function email_v(){
	var is_submitted = $("#is_submitted").val();
		if(is_submitted == 1){
			showMessage(["提示","系统处理中"]);
			return;
		}
		var phone = $("#phone").val();
		if(!(/^[1][34578][0-9]{9}$/.test(phone))){
			$("#13").show();
		}
		var v_code = $("#binding_phone_code").val();
	try {
	$("#is_submitted").val(1);
		$.post('<%= __ROOT_PATH__%>/index/phone_validate.html',
			{
				"phone":phone,
				"v_code" : v_code
			},
			function(data){
			$("#is_submitted").val(0);
			if(data == 3){
					$("#2").show();
					$("#21").hide();
					$("#1").hide();
				}else if(data == 2){
					$("#1").hide();
					$("#11").hide();
					$("#12").hide();
					$("#14").hide();
					$("#13").hide();
					$("#15").hide();
					$("#16").hide();
					$("#17").hide();
					$("#18").show();
				}else if(data == 4){
					$("#2").hide()
					$("#21").show();
					$("#1").hide();
				}else if(data == 5){
					$("#2").show()
					$("#21").hide();
					$("#1").hide();
				}else if(data == 6){
					$("#1").hide();
					$("#11").show();
					$("#12").hide();
					$("#14").hide();
					$("#13").hide();
					$("#15").hide();
					$("#16").hide();
					$("#17").hide();
					$("#18").hide();
				}else if(data == 0){
					window.location.href="<%= __ROOT_PATH__%>/index/registration3.html";
				}
			
		},"html","application/x-www-form-urlencoded; charset=utf-8");
	} catch (e) {
		$("#is_submitted").val(0);
	}
		
	}
	
	function phone_update(){
		var phone =$("#phone").val();
			if(!(/^[1][34578][0-9]{9}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%= __ROOT_PATH__%>/index/phone_update.html',
			{
				"phone":phone
			},
			function(data){
				if(data == 1){
					$("#1").show();
					$("#11").hide();
					$("#12").hide();
					$("#14").hide();
					$("#13").hide();
					$("#15").hide();
					$("#16").hide();
					$("#17").hide();
				}else if(data == 2){
					$("#1").hide();
					$("#11").show();
					$("#12").hide();
					$("#14").hide();
					$("#13").hide();
					$("#15").hide();
					$("#16").hide();
					$("#17").hide();
				}else if(data == 3){
					$("#1").hide();
					$("#11").hide();
					$("#12").show();
					$("#14").hide();
					$("#13").hide();
					$("#15").hide();
					$("#16").hide();
					$("#17").hide();
				}else if(data == 4){
					$("#1").hide();
					$("#11").hide();
					$("#12").hide();
					$("#14").show();
					$("#13").hide();
					$("#15").hide();
					$("#16").hide();
					$("#17").hide();
				}else if(data == 5){
					$("#1").hide();
					$("#11").hide();
					$("#12").hide();
					$("#14").hide();
					$("#13").show();
					$("#15").hide();
					$("#16").hide();
					$("#17").hide();
				}else if(data == 6){
					$("#1").hide();
					$("#11").hide();
					$("#12").hide();
					$("#14").hide();
					$("#13").hide();
					$("#15").show();
					$("#16").hide();
					$("#17").hide();
				}else if(data == 7){
					$("#1").hide();
					$("#11").hide();
					$("#12").hide();
					$("#14").hide();
					$("#13").hide();
					$("#15").hide();
					$("#16").show();
					$("#17").hide();
				}else if(data == 8){
					$("#1").hide();
					$("#11").hide();
					$("#12").hide();
					$("#14").hide();
					$("#13").hide();
					$("#15").hide();
					$("#16").hide();
					$("#17").show();
				}
		},"html","application/x-www-form-urlencoded; charset=utf-8");
	}
	
	
	function showMessage(message){
				var btnFn = function(){
					  easyDialog.close();
					  };
			   var btnFs = function(){
				   window.location.reload();
			   };
				if(message[2]!=null){
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFs,
							 noFn:false
							}
					});	
				}else{
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFn,
							 noFn:false
							}
					});	
				}
			}
	
</script>