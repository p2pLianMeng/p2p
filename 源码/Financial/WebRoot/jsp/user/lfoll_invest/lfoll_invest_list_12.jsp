<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
<title>${applicationScope.title}</title>
  	<meta http-equiv="keywords" content="${applicationScope.keywords}">
	<meta http-equiv="description" content="${applicationScope.description}">
  </head>
  
  <body>
    <%@ include file="/jsp/index/index_top.jsp" %>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/lfoll_invest.js"></script>
    <span id="ny_pic"></span>

<div class="U_jihua U_jihua_lb">
  <h1><a href="#">我要理财</a> > <a href="#">联富宝</a> > 联富宝<a class="a" href="#">债权转让列表</a><a class="a" href="#">散标投资列表</a></h1>
    
  <div class="U_jihua_head">
  	<a  href="<%=__ROOT_PATH__%>/user/financial/lfollinvest/get_03month_already_released_invests.html">联富宝L03</a>
  	<a  href="<%=__ROOT_PATH__%>/user/financial/lfollinvest/get_06month_already_released_invests.html">联富宝L06</a>
  	<a class="hover" href="<%=__ROOT_PATH__%>/user/financial/lfollinvest/get_12month_already_released_invests.html">联富宝L12</a>
  </div>
  <div style="display:block;height: 270px;margin-bottom:0px;" class="U_jihua_content">
     <table width="100%" cellspacing="0" cellpadding="0">           
        <tr>
           <td width="21%" class="td_1">计划名称</td>
           <td width="21%" class="td_1">计划金额（元）</td>
           <td width="12%" class="td_1">加入人次</td>
           <td width="15%" class="td_1">预期年化收益</td>
           <td width="15%" class="td_1">累计收益（元）</td>
           <td width="16%" class="td_1">状态</td>
        </tr>
        <c:forEach items="${fix_bid_system_order_list}" var="model">
	        <tr>
	           <td class="td_2 td_xm">
	           		<a style="color:#2ea7e0" href="<%=__ROOT_PATH__%>/user/financial/lfollinvest/get_lfoll_invest_info.html?id=${model.m.id}">
	           			${model.m.bid_name}
	           		</a>
	           </td>
	           <td class="td_2">${model.m.total_money}</td>
	           <td class="td_2">${model.m.booking_number}人</td>
	           <td class="td_2">${model.m.annualized_rate}%</td>
	           <td class="td_2">${model.m.repatment_interest}</td>
	           <td class="td_2">
	           	<c:if test="${model.m.public_state_var == 1}">
	           		等待预定
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 2}">
	           		预定中
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 3}">
	           		等待支付
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 4}">
	           		等待开标
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 5}">
	           		开放中
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 6}">
	           		收益中
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 7}">
	           		已结束
	           	</c:if>
	           </td>
	        </tr>
       </c:forEach>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
           <td colspan="6">
	           	<div class="down">
	           		<%--<a style="background:#e5e4e2; border:1px #e5e4e2 solid; color:#fff;" href="#"></a>
	           		<a style="background:#2ea7e0; border:1px #2ea7e0 solid;color:#fff;" href="#">1</a>
	           		<a href="#">2</a>
	           		<a href="#">3</a>
	           		<a href="#">></a>
	           		--%>
	           		${pageDiv}
	           	</div>
          </td>
        </tr>          
     </table>
  </div>
  
          
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>
