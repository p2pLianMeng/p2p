<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>联富金融</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<%--<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
--%></head>
<body>
<%@ include file="/jsp/index/index_top.jsp" %>
<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/fix_bid_newbie_experience.js"></script>
<span id="ny_pic"></span>

<div class="U_jihua">
  <h1><a href="#">我要理财</a> > <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_lfoll_invest.html">联富宝</a> > 联富宝详情</h1>
  <div class="U_jihua_bt U_jihua_bt2">
     <div class="up"><span>联富宝</span><s>（新手体验）</s><a href="<%=__ROOT_PATH__%>/user/financial/financial/to_calculator_lender.html"><img src="<%=__PUBLIC__%>/images/U_jihua_1.png" title="年利率计算器" alt="" /></a><%--<a class="a" href="#">联富宝-A141029期协议（范本）</a>--%></div>
     <div class="left">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">  
           <tr>
              <td height="88" colspan="2">起投金额<br /><span class="U_jihua_text">￥10,000</span></td>
              <td height="88" colspan="2">预期年化收益<br /><span class="U_jihua_text">12.00%</span></td>
              <td width="18%" height="88">锁定期限（天）<br /><span class="U_jihua_text">3</span></td>
           </tr>
           <tr>
              <td width="20%" height="35">投标范围</td>
              <td width="27%" height="35" align="left" class="tbfw"><img src="<%=__PUBLIC__%>/images/icon1.jpg" alt="" /></td>
              <td width="18%" height="35" align="left">退出方式</td>
              <td width="17%" height="35" align="left" >返回收益</td>
              <td rowspan="1"></td>
           </tr>
           <%--
           <tr>
              <td style="color:#688228" height="35">预定进度</td>
              <td colspan="4" height="35" align="left">
                 <div class="td_ydjd"><span></span></div>
                 <div class="td_ydjd2">0%<img src="<%=__PUBLIC__%>/images/loan_b_1.png" /></div>
              </td>
           </tr>
        --%></table>
     </div>
     <div class="right">
        <div class="con" style="text-align: center"><span>代付金额（元）</span></div>
        <div class="con_2" style="text-align: left">10,000元</div>
        <div class="con"><span>预计收益&nbsp;&nbsp;&nbsp;9.84元</span><a href="<%=__ROOT_PATH__ %>/user/usercenter/recharge/into_user_recharge_index.html">充值</a></div>        
        <input class="text_1" type="text" value="10000" readonly="readonly" />
        <div class="con">
           <span id="tishi" style="color:red"></span>
        </div>
        <%--<script src="js/loginDialog.js" type="text/javascript"></script>
        <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
        --%>
       <div class="con_3 U_jihua_B" style="cursor:pointer" onClick="validate_whether_bid_newer();">购&nbsp;&nbsp;&nbsp;&nbsp;买</div>
       <div id="loginDiv1" class="loginDiv1"></div>
       <div id="loginDiv2" class="loginDiv2" style=" z-index:10005; display:none;">
          <div class="U_jihua_B2">
             <input type="button" class="guanbi" onClick="closeme()" value="ｘ" />
             
             <div class="up_b2">确认加入</div>
             <div class="up_b3"><span>计划名称</span>联富宝-新手体验项目</div>
             <div class="up_b3"><span>预期收益</span>12.00%/年</div>
             <div class="up_b3"><span>锁定期限</span>3天</div>
             <div class="up_b3"><span>加入金额</span>10000元</div>
             <div class="up_b3"><span>处理方式</span>返还收益</div>
             <div class="up_b3 up_b4"><span>代付金额</span><s>10000</s>元</div>
             <div>
             	<span style="margin-right:30px; color:#666666;">验证码</span>
		       	<input type="text" id="validate_code" />
		        <img id="CheckCodeServlet" width="122" height="24" src="<%=__ROOT_PATH__%>/code.jpg" onclick="refresh_order_pay();" style="margin-left:10px; font-color: white;" />
    		 </div>
      		 <div class="up_b3" id="error_msg" style="color:red"></div>
             <div class="up_b5">
                <div class="up_b5_up">温馨提示</div>
                <div class="up_b5_up2">
                	1.收益处理方式为到期后返还收益部分。<br />
                	2.联富宝新手体验项目，每个用户只能投资一次，更多收益还在真实投资。
                </div>                
             </div>
             <div class="up_b6">
             	<a href="#" id="submit_new_bid_make_sure">确认</a>
             	<a style="background:#dbdbdb; color:#68696e;" href="javascript:void(0)" onclick="closeme()">取消</a>
             </div>
             <!--
             <div class="up_b7"><img src="<%=__PUBLIC__%>/images/registration_pic7.png" />联富宝加入成功</div>
             <div class="up_b8"><a onClick="closeme()">关闭</a></div>
             
             <div class="up_b7"><img src="<%=__PUBLIC__%>/images/registration_pic8.png" />对不起，您所投金额大于该标所需金额！</div>
             <div class="up_b8"><a onClick="closeme()">关闭</a></div>
             -->
          </div>
       </div>
     </div>
  </div> 
  
   <div class="xin_xq">
     <div class="up">新手体验活动</div>
     <div class="con">
        &bull;&nbsp;本活动中，用户无需支付任何真实本金。奖励金额直接充入帐号且无使用限制。<br />
        &bull;&nbsp;参加活动的用户可以获得等值于投资收益的活动奖励。<br />
        &bull;&nbsp;本活动参与权无任何充值、投资等前置限制。联富金融的任何出借人仅需绑定手机，并成功进行身份验证就可以参与活动。<br />
        &bull;&nbsp;本体验产品仅供注册用户模拟联富金融产品的购买和收益过程，且每个体验产品仅能参与一次。<br />
        &bull;&nbsp;联富金融有权取消涉嫌骗取活动奖励的用户获奖资格。本活动最终解释权归联富金融。<br />
     </div>
  </div>
  
  <div class="xin_xq">
     <div class="up">联富宝理财服务</div>
     <div class="con">
        <h2>服务介绍</h2>
        联富宝是联富金融近期推出的一站式理财咨询服务。出借人通过联富宝将资金出借给联富金融平台上的精英标用户，采用智能投标、循环出借的方式，提高资金的流动率和利用率，从而增加实际收益。联富宝独创一键式服务，能够使出借人在获取丰厚回报的同时，帮助白领人群提前实现梦想，达到双赢。<br />
        <h2>服务优势</h2>
        收益佳：预期年化收益率12-16 % (已扣除相关费用)<br />
        期限灵活： 3/6/12个月<br />
        加入门槛：最小加入金额1千元，以千元为单位递增<br />
        全托管：一站式理财服务，自动投标
     </div>
  </div>
  
  <div class="xin_xq">
     <div class="up">常见问题</div>
     <div class="con">
        <h2>如何在联富金融出借？</h2>
        您需要注册成为联富金融的出借人，之后可以选择自己满意的借款项目进行投标。确认投标并支付后，您就完成了一次出借。在联富金融，您可以自主投标、购买转让债权，也可以参加联富宝服务。<br />
        <h2>出借人的收益如何计算？</h2>
        联富金融平台采用目前流行的等额本息按月还款。出借人每月都有资金收回。由于已收回的本金不再产生收益，利率不变的情况下，利息收入会逐渐降低。为了达到较高的收益，我们建议您循环出借，将收回的资金再借出去。<br />  
        <h2>什么时候能收回借款？</h2>     
        为了增加出借人资金的流动性，联富金融平台目前采用的是“等额本息还款法”，出借人每个月都有资金收回。由借款资金放款成功日（如：11月14日）开始计算，1个自然月后的日期（如：12月14日）为第一个还款日，以后每月的还款时间以此类推。<br /> 
        <h2>联富金融推出的联富宝理财服务是类似银行的理财产品吗？</h2>  
        联富金融的“联富宝”服务与理财产品不是一个概念，其基本原理与理财产品完全不同，这是一种出借人的增值服务，是联富金融平台以现有投标功能为基础，为便于用户投标、提高回款利用率、解决资金流动性问题而推出的一种高级投标工具。用户对于参加服务的资金使用情况可以清晰掌握，公开透明，每一笔出借都有对应的借款合同。在此过程中,联富金融平台不获取使用用户资金。<br />
        <h2>联富金融推出的联富宝理财服务所投标的范围是什么？</h2> 
        联富宝理财服务的投标范围主要为“精英标”，联富金融平台对“精英标”进行严格的信用审核，确保参加联富宝理财服务的用户的资金安全。<br /> 
        <h2>线上加入联富宝理财服务如何支付？</h2>
        线上加入联富宝理财服务，支持网银支付（需注意单日、单笔最大交易金额限制）。联富金融建议您使用如下网银： 中国工商银行、中国建设银行、中国农业银行、中国银行、招商银行、交通银行、邮政储蓄银行、民生银行、中信银行、光大银行、兴业银行、广发银行、深圳发展银行、平安银行。 如在支付过程中遇到问题，请联系联富金融客服：4006-888-923。
     </div>
  </div>
     
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
</body>
</html>
