<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>${applicationScope.title}</title>
	<meta http-equiv="keywords" content="${applicationScope.keywords}">
	<meta http-equiv="description" content="${applicationScope.description}">
  </head>
  
  <body>
    <%@ include file="/jsp/index/index_top.jsp" %>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/lfoll_invest.js"></script>
     <link href="<%=__PUBLIC__ %>/css/count_dowm.css" rel="stylesheet"  type="text/css" />
     <script type="text/javascript" src="<%=__PUBLIC__ %>/js/count_down.js"></script>
     <script type="text/javascript" src="<%=__PUBLIC__%>/js/fix_bid_join.js"></script>
    <span id="ny_pic"></span>

<div class="U_jihua">
  <h1><a href="#">我要理财</a> > <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_lfoll_invest.html">联富宝</a> > ${fix_bid_system_order.m.bid_name}详情</h1>
  <div class="U_jihua_bt">
     <div class="up">
     	<span>${fix_bid_system_order.m.bid_name}</span>
     	<a href="<%=__ROOT_PATH__%>/user/financial/financial/to_calculator_lender.html"><img src="<%=__PUBLIC__%>/images/U_jihua_1.png" title="年利率计算器" alt="" /></a>
     	<a class="a" href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/get_lfoll_fix_bid_contract.html?id=${fix_bid_system_order.m.id}&bid_name=${fix_bid_system_order.m.bid_name}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">${fix_bid_system_order.m.bid_name}协议（范本）</a></div>
     <div class="left">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">  
           <tr>
              <td height="88" colspan="2">计划金额<br /><span class="U_jihua_text">￥${fix_bid_system_order.m.total_money}</span></td>
              <td height="88" colspan="2">预期年化收益<br /><span class="U_jihua_text">${fix_bid_system_order.m.annualized_rate * 100}%</span></td>
              <td width="18%" height="88">锁定期限（月）<br /><span class="U_jihua_text">${fix_bid_system_order.m.closed_period}</span></td>
           </tr>
           <tr>
              <td width="20%" height="35">投标范围</td>
              <td width="27%" height="35" align="left" class="tbfw"><img src="<%=__PUBLIC__%>/images/icon1.jpg" alt="" /><img src="<%=__PUBLIC__%>/images/icon1_2.jpg" alt="" /></td>
              <td width="18%" height="35" align="left">保障方式</td>
              <td width="17%" height="35" align="left" class="bjjlx">本金+利息<%--<img src="<%=__PUBLIC__%>/images/loan_3.png" alt="" /> --%></td>
              <td rowspan="2"></td>
           </tr>
           <tr>
              <td height="35">退出日期</td>
              <td height="35" align="left">${fix_bid_system_order.m.repayment_time}</td>
              <td height="35" align="left"></td>
              <td height="35" align="left"></td>
           </tr>
        </table>
     </div>
     <div class="right">
        <p style="border-bottom:1px solid #009c89;"><span></span><s>支付截止倒计时</s></p>
        <p style="border-top:1px solid #00cdc2;"><span></span><s></s> </p>
         <div class="colockbox colockbox_2" id="demo01">
	           <span class="day"></span>
	           <span class="hour"></span>
	           <span class="minute"></span>
	           <span class="second"></span>
	      </div>
		<input type="hidden" id="count_down" value="${pay_end_time_str3}"/>
     	<input type="hidden" id="fix_bid_sys_order_id" value="${fix_bid_system_order.m.id}"/>
     </div>
  </div>
  <div class="U_jihua_jd">
     <div class="up"><span>联富宝进度</span><a class="a" href="<%=__ROOT_PATH__%>/help/help_lc_lfbjs.html">如何人加入联富宝</a></div>
     <div class="con_1"><span class="a">预告阶段</span><span class="b"></span><span class="c">联富金融发布</span></div>
     <div class="con_1 con_2">
        <span class="a">预定阶段</span>
        <span class="b b_1" style="background:url(<%=__PUBLIC__ %>/images/plan-progress.png) -100px bottom no-repeat;"></span>
        <span class="b b_2" style="background:url(<%=__PUBLIC__ %>/images/plan-progress.png) -240px bottom no-repeat;"></span>
        <span class="b b_3" style="background:url(<%=__PUBLIC__ %>/images/plan-progress.png) -370px center no-repeat;"></span>
        <span class="c">预定开始<br />${fix_bid_system_order.m.reserve_start_time }</span>
        <span class="c">预定结束<br />${fix_bid_system_order.m.reserve_end_time }</span>
        <span class="c">支付截止<br />${fix_bid_system_order.m.pay_end_time }</span>
     </div>
     <div class="con_1">
        <span class="a">开放加入阶段</span>
        <span class="b b_4" style="background:url(<%=__PUBLIC__ %>/images/plan-progress.png) -505px center no-repeat;"></span>
        <span class="c c_1">开放加入<br />${fix_bid_system_order.m.invite_start_time }</span>
     </div>
     <div class="con_1 con_3">
        <span class="a">收益阶段</span>
        <span class="b b_5" style="background:url(<%=__PUBLIC__ %>/images/plan-progress.png) -640px center no-repeat;"></span>
        <span class="b b_6" style="background:url(<%=__PUBLIC__ %>/images/plan-progress.png) -780px center no-repeat;"></span>
        <span class="c c_1">开放加入<br />${fix_bid_system_order.m.invite_end_time }</span>
        <span class="c c_1">开放加入<br />${fix_bid_system_order.m.repayment_time }</span>
     </div>
  </div>
     <div class="U_jihua_head">
     		<a class="hover">联富宝介绍</a>
	     	<a href="javascript:void(0);" onclick="find_fix_bid_order_all_list();">预订记录</a>
	     	<a href="javascript:void(0);" onclick="find_fix_bid_user_hold_all_list();">加入记录</a>
     		<a>常见问题</a>
     </div>
     
     <div class="U_jihua_content" style="display:block">
        <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>名称</span></td>
              <td width="81%" class="td_5"><p>${fix_bid_system_order.m.bid_name}</p></td>
           </tr>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>联富宝介绍</span></td>
              <td width="81%" class="td_5"><p>联富宝是联富金融推出的便捷高效的自动投标工具。联富宝在用户认可的标的范围内，对符合要求的标的进行自动投标，且回款本金在相应期限内自动复投，期限结束后联富宝会通过联富金融债权转让平台进行转让退出。该计划所对应的标的均100%适用于联富金融本金保障计划并由系统实现标的分散投资。出借所获利息收益可选择每月复投或提取，更好的满足用户多样化的理财需求。详情参见<a href="<%=__ROOT_PATH__%>/template/lian_fu_bao_instructions.pdf" target="_blank">《联富金融联富宝说明书》</a></p></td>
           </tr>
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>投标范围</span></td>
              <td width="81%" class="td_5"><p>信用认证标</p></td>
           </tr>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>预期年化收益</span></td>
              <td width="81%" class="td_5">
              	<p>${fix_bid_system_order.m.annualized_rate * 100}%</p>
              </td>
           </tr>
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>锁定期</span></td>
              <td width="81%" class="td_5"><p>${fix_bid_system_order.m.closed_period}个月</p></td>
           </tr>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>退出日期</span></td>
              <td width="81%" class="td_5"><p>${fix_bid_system_order.m.repayment_time }</p></td>
           </tr>
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>加入条件</span></td>
              <td width="81%" class="td_5"><p>加入金额${fix_bid_system_order.m.least_money}元起，且为${fix_bid_system_order.m.least_money}元的整数倍递增。</p></td>
           </tr>
           <%--
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>加入上限</span></td>
              <td width="81%" class="td_5"><p>500,000元</p></td>
           </tr>
           --%>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>定金</span></td>
              <td width="81%" class="td_5"><p>加入金额 x <span>1.0%</span></p></td>
           </tr>
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>预定开始时间</span></td>
              <td width="81%" class="td_5"><p>${fix_bid_system_order.m.reserve_start_time}</p></td>
           </tr>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>支付截止时间</span></td>
              <td width="81%" class="td_5"><p>${fix_bid_system_order.m.pay_end_time }<br /><s>*</s>若逾期支付剩余金额，已支付定金将不予返还。</p></td>
           </tr>
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>开放加入时间</span></td>
              <td width="81%" class="td_5"><p>${fix_bid_system_order.m.invite_start_time }</p></td>
           </tr>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>到期退出方式</span></td>
              <td width="81%" class="td_5"><p>到期本金+利息退还</p></td>
           </tr>
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>提前退出方式</span></td>
              <td width="81%" class="td_5"><p>锁定期内支持提前退出，详情参见<a href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/get_lfoll_fix_bid_contract.html?id=${fix_bid_system_order.m.id}&bid_name=${fix_bid_system_order.m.bid_name}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">《联富宝服务协议》</a>。</p></td>
           </tr>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>费用</span></td>
              <td width="81%" class="td_5"><p>加入费用：<span>0.0%</span><br />管理费用：参见<a href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/get_lfoll_fix_bid_contract.html?id=${fix_bid_system_order.m.id}&bid_name=${fix_bid_system_order.m.bid_name}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">《联富宝服务协议》</a><br />退出费用：<span>0.0%</span><br />提前退出费用：加入金额 x <span>3.0%</span>，详情参见<a href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/get_lfoll_fix_bid_contract.html?id=${fix_bid_system_order.m.id}&bid_name=${fix_bid_system_order.m.bid_name}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">《联富宝服务协议》</a></p></td>
           </tr>
           <tr class="tr_1">
              <td width="19%" class="td_4"><span>保障方式</span></td>
              <td width="81%" class="td_5"><p>100%本金保障计划</p></td>
           </tr>
           <tr class="tr_2">
              <td width="19%" class="td_4"><span>服务协议</span></td>
              <td width="81%" class="td_5"><p><a href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/get_lfoll_fix_bid_contract.html?id=${fix_bid_system_order.m.id}&bid_name=${fix_bid_system_order.m.bid_name}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">【点击查看】</a></p></td>
           </tr>
        </table>
     </div>
     
     <div class="U_jihua_content">
        <table width="100%" cellspacing="0" cellpadding="0">
           <tr>
              <td width="9%" class="td_1">序号</td>
              <td width="22%" class="td_1">理财人</td>
              <td width="17%" class="td_1">加入金额</td>
              <td width="26%" class="td_1">预定时间</td>
              <td width="13%" class="td_1">状态</td>
           </tr>
          <c:if test="${not empty fix_bid_user_order_list}">
	          <c:forEach items="${fix_bid_user_order_list}" var="fix_bid_order" varStatus="n">
	          	 <tr>
	              <td class="td_2">${n.index + 1}</td>
	              <td class="td_2 td_xm">${fix_bid_order.m.nick_name}</td>
	              <td class="td_2">${fix_bid_order.m.bid_money}元</td>
	              <td class="td_2">${fix_bid_order.m.add_time}</td>
	              <td class="td_2">
	              	<c:if test="${fix_bid_order.m.is_pay == 0}">
	              		未支付
	              	</c:if>
	              	<c:if test="${fix_bid_order.m.is_pay == 1}">
	              		已支付
	              	</c:if>
	              </td>
	           </tr>
	          </c:forEach>  
          </c:if>
           
           <c:if test="${empty fix_bid_user_order_list}">
          		<tr>
          			<td colspan="5"><span>${tips_order}</span></td>
           		</tr>
           </c:if>
        </table>
     </div>
     <div class="U_jihua_content">
        <table width="100%" cellspacing="0" cellpadding="0">
           <tr>
              <td class="td_1">序号</td>
              <td class="td_1">理财人</td>
              <td class="td_1">加入金额</td>
              <td class="td_1">加入时间</td>
           </tr>
          <c:if test="${not empty fix_bid_user_hold_list}">
           		<c:forEach items="${fix_bid_user_hold_list}" var="fix_bid_user_hold" varStatus="n">
	           		<tr>
		              <td class="td_2">${n.index + 1}</td>
		              <td class="td_2 td_xm">${fix_bid_user_hold.m.nick_name}</td>
		              <td class="td_2">${fix_bid_user_hold.m.bid_money}元</td>
		              <td class="td_2">${fix_bid_user_hold.m.add_time}</td>
		           </tr>
	           </c:forEach>
           </c:if>
           <c:if test="${empty fix_bid_user_hold_list}">
	          	<tr>
	          		<td colspan="4">${tips_hold}</td>
	          	</tr>
       		</c:if>
        </table>
     </div>
     <div class="U_jihua_content">
        <dl>
           <dt>1.我有10000元，该如何加入联富宝？</dt>
           <dd>
              （1）预定加入<br />
              在预定加入期间，首先预定10000元联富宝，并支付定金10000*1%=100元（当前定金比例为1%），之后在规定的时间内完成剩余金额10000-100=9900元的支付，您即成功加入联富宝。
           </dd>
           <dd>
              （2）开放加入<br />
              若预定加入期间该期优选理财计划未满额，则进入开放加入阶段，您可直接加入10000元。 注：您可以通过关注联富金融官方微信、微博获取联富宝发布信息。 
           </dd>
        </dl>
        <dl>
           <dt>2.联富宝到期后，我能直接拿回本金吗？</dt>
           <dd>
              是的，联富宝到期当日，系统将自动归还本金和当期的收益。 
           </dd>
        </dl>
        <dl>
           <dt>3.定金将如何处理？</dt>
           <dd>
              若您未能按时完成剩余金额的支付, 则视为您主动放弃加入该期联富宝, 定金将不予返还。  
           </dd>
        </dl>
        <dl>
           <dt>4.联富宝安全吗？</dt>
           <dd>
              联富金融以严谨负责的态度对每笔借款进行严格筛选。同时，联富宝所对应借款100%适用联富金融本金保障计划。  
           </dd>
        </dl>
     </div>
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>
