<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
	
	
	<script src="js/loginDialog.js" type="text/javascript"></script>
    <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />

  </head>
  
  <body>
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>



<div class="sanbiao">
  <h1><a href="#">我要理财</a> > 散标投资列表</h1>    
  <div class="sanbiao_head">
  	<a class="hover" href="#">精英标</a>
  	<a href="#">联富宝</a>
  	<a href="#">债权转让</a></div>  
  <div class="sanbiao_list" style="display:block">
  <div class="sanbiao_con">
     <div class="left">工薪精英人群有较高的教育背景、稳定的经济收入及良好的信用意识。宜人贷平台依托多年来积累的信用审核分析技术，针对白领人群的资金使用情况及还款能力进行多方面审核，降低了出借人的风险。同时，宜人贷针对精英标产品还提供了本息保障服务，更好地保障了出借人的收益。</div>
     <div class="right"><a href="#"><img src="<%=__PUBLIC__%>/images/sanbiao.jpg" /></a><a href="#"><img src="<%=__PUBLIC__%>/images/sanbiao_2.jpg" /></a></div>
  </div>  
  <h2>
    <p>精英标列表</p><a href="#">理财计算器</a><span>累计成交总金额<br />44.53亿元</span><span>累计成交总笔数<br />81670笔</span><span>为用户累计赚取<br />29090.86万元</span></h2>
  
  <div class="sanbiao_con2">
     <span>期限：</span>
     <select id="select_1" class="select_1">
        <option value="0">全部</option>
		<option value="3">3个月</option>
		<option value="6">6个月</option>
		<option value="9">9个月</option>
        <option value="12">12个月</option>
        <option value="18">18个月</option>
        <option value="24">24个月</option>
        <option value="36">36个月</option>
     </select>
     <span>年化利率：</span>
     <select id="select_2" class="select_1">
        <option value="0">全部</option>
        <option value="10">10%</option>
        <option value="11">11%</option>
        <option value="12">12%</option>
        <option value="13">13%</option>
		<option value="14">14%</option>
		<option value="15">15%</option>
		<option value="16">16%</option>
		<option value="17">17%</option>
		<option value="18">18%</option>
		<option value="19">19%</option>
		<option value="20">20%</option>
		<option value="21">21%</option>
		<option value="22">22%</option>
		<option value="23">23%</option>
		<option value="24">24%</option>
     </select>
     <span>金额：</span>
     <select id="select_3" class="select_1">
        <option value="0">全部</option>
        <option value="1">1-5万</option>
        <option value="2">5-10万</option>
        <option value="3">10-20万</option>
        <option value="4">20-50万</option>
     </select>
     <span>投标完成度：</span>
     <select id="select_4" class="select_1">
        <option value="0">全部</option>
        <option value="1">低于50%</option>
        <option value="2">50%-79%</option>
        <option value="3">80%-99%</option>
        <option value="4">100%满标</option>
     </select>
     <a href="javascript:void(0);" onclick="asychronized_query(1);">筛选</a>
  </div>
  
  <h3>
  <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="37%" height="45" class="con2_kk">借款标题</td>
	    <td width="11%" height="45" class="con2_kk">信用等级</td>
	    <td width="10%" height="45" class="con2_kk">年利率</td>
	    <td width="12%" height="45" class="con2_kk">金额</td>
	    <td width="10%" height="45" class="con2_kk">期限</td>
	    <td width="9%" height="45" class="con2_kk">进度</td>
	    <td width="11%" height="45" class="con2_kk">&nbsp;</td>
	  </tr>
  	
  </table>
</h3>

<h6 id="asyncPageDiv" >
  	${asyncPageDiv}
</h6>

</div>

<div class="sanbiao_list sanbiao_list2">
  <div class="sanbiao_con">
     <div class="left">宜定盈是宜人贷近期推出的一站式理财咨询服务。出借人通过宜定盈服务将资金出借给宜人贷平台上的精英标用户，采用智能投标、循环出借的方式，提高资金的流动率和利用率，从而增加实际收益。宜定盈独创一键式服务，能够使出借人在获取丰厚回报的同时，帮助白领人群提前实现梦想，达到双赢。</div>
     <div class="right">
        <p><span>&bull;</span>一键加入，自动投标，省心省力</p>
        <p><span>&bull;</span>收益稳定，预期年化收益率10%</p>
        <p><span>&bull;</span>资金安全，担保公司全额本息担保</p>
        <p><span>&bull;</span>多种期限满足不同流动性需求</p>
     </div>
  </div>  
  <h2><p>联富宝列表</p><a href="#">理财计算器</a><span>累计成交总金额<br />44.53亿元</span><span>累计成交总笔数<br />81670笔</span><span>为用户累计赚取<br />29090.86万元</span></h2>  
  
  <div class="U_jihua_head"><a class="hover">U计划A（3个月）</a><a>U计划B（6个月）</a><a>U计划C（12个月）</a></div>
  <div style="display:block;" class="U_jihua_content">
     <table width="100%" cellspacing="0" cellpadding="0">           
        <tr>
           <td width="21%" class="td_1">计划名称</td>
           <td width="21%" class="td_1">计划金额（元）</td>
           <td width="12%" class="td_1">加入人次</td>
           <td width="15%" class="td_1">预期年化收益</td>
           <td width="15%" class="td_1">累计收益（元）</td>
           <td width="16%" class="td_1">状态</td>
        </tr>
        <tr>
           <td class="td_2">联富金融-A141105期</td>
           <td class="td_2 td_xm">10,000,000</td>
           <td class="td_2">0人</td>
           <td class="td_2">7.0%</td>
           <td class="td_2">0.00</td>
           <td class="td_2">等待预定/td>
        </tr>
        <tr>
           <td class="td_3">联富金融-A141103期</td>
           <td class="td_3 td_xm">10,000,000 </td>
           <td class="td_3">296人</td>
           <td class="td_3">7.0%</td>
           <td class="td_3">0.00</td>
           <td class="td_3">预定满额</td>
        </tr>
        <tr>
           <td class="td_2">联富金融-A141029期</td>
           <td class="td_2 td_xm">8,000,000</td>
           <td class="td_2">483人</td>
           <td class="td_2">7.0%</td>
           <td class="td_2">0.00</td>
           <td class="td_2">收益中</td>
        </tr>
        <tr>
           <td class="td_3">联富金融-A141027期</td>
           <td class="td_3 td_xm">2,000,000</td>
           <td class="td_3">155人</td>
           <td class="td_3">7.0%</td>
           <td class="td_3">0.00</td>
           <td class="td_3">收益中</td>
        </tr>  
     </table>
  </div>  
  
  <div class="U_jihua_content">
     <table width="100%" cellspacing="0" cellpadding="0">           
        <tr>
           <td width="21%" class="td_1">计划名称</td>
           <td width="21%" class="td_1">计划金额（元）</td>
           <td width="12%" class="td_1">加入人次</td>
           <td width="15%" class="td_1">预期年化收益</td>
           <td width="15%" class="td_1">累计收益（元）</td>
           <td width="16%" class="td_1">状态</td>
        </tr>
        <tr>
           <td class="td_2">联富金融-B141105期</td>
           <td class="td_2 td_xm">10,000,000</td>
           <td class="td_2">0人</td>
           <td class="td_2">7.0%</td>
           <td class="td_2">0.00</td>
           <td class="td_2">等待预定/td>
        </tr>
        <tr>
           <td class="td_3">联富金融-B141103期</td>
           <td class="td_3 td_xm">10,000,000 </td>
           <td class="td_3">296人</td>
           <td class="td_3">7.0%</td>
           <td class="td_3">0.00</td>
           <td class="td_3">预定满额</td>
        </tr>
        <tr>
           <td class="td_2">联富金融-B141029期</td>
           <td class="td_2 td_xm">8,000,000</td>
           <td class="td_2">483人</td>
           <td class="td_2">7.0%</td>
           <td class="td_2">0.00</td>
           <td class="td_2">收益中</td>
        </tr>
        <tr>
           <td class="td_3">联富金融-B141027期</td>
           <td class="td_3 td_xm">2,000,000</td>
           <td class="td_3">155人</td>
           <td class="td_3">7.0%</td>
           <td class="td_3">0.00</td>
           <td class="td_3">收益中</td>
        </tr>         
     </table>
  </div>
  
  <div class="U_jihua_content">
     <table width="100%" cellspacing="0" cellpadding="0">           
        <tr>
           <td width="21%" class="td_1">计划名称</td>
           <td width="21%" class="td_1">计划金额（元）</td>
           <td width="12%" class="td_1">加入人次</td>
           <td width="15%" class="td_1">预期年化收益</td>
           <td width="15%" class="td_1">累计收益（元）</td>
           <td width="16%" class="td_1">状态</td>
        </tr>
        <tr>
           <td class="td_2">联富金融-C141105期</td>
           <td class="td_2 td_xm">10,000,000</td>
           <td class="td_2">0人</td>
           <td class="td_2">7.0%</td>
           <td class="td_2">0.00</td>
           <td class="td_2">等待预定/td>
        </tr>
        <tr>
           <td class="td_3">联富金融-C141103期</td>
           <td class="td_3 td_xm">10,000,000 </td>
           <td class="td_3">296人</td>
           <td class="td_3">7.0%</td>
           <td class="td_3">0.00</td>
           <td class="td_3">预定满额</td>
        </tr>
        <tr>
           <td class="td_2">联富金融-C141029期</td>
           <td class="td_2 td_xm">8,000,000</td>
           <td class="td_2">483人</td>
           <td class="td_2">7.0%</td>
           <td class="td_2">0.00</td>
           <td class="td_2">收益中</td>
        </tr>
        <tr>
           <td class="td_3">联富金融-C141027期</td>
           <td class="td_3 td_xm">2,000,000</td>
           <td class="td_3">155人</td>
           <td class="td_3">7.0%</td>
           <td class="td_3">0.00</td>
           <td class="td_3">收益中</td>
        </tr>        
     </table>
  </div>
  
  <h6 class="news_list2"><a href="#">〈</a> <a href="#" class="hover">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a> <a href="#">6</a> <a href="#">〉</a></h6>
</div>

<div class="sanbiao_list sanbiao_list3">
  <div class="sanbiao_con">
     <div class="left">平台提供债权转让功能，增加资金流动性。用户可自行转让资产组合中符合条件的债权，也可购买其他用户转让的债权，从而获得折让收益及借款标的后续收益。债权持有90天后即可进行转让，转让次数不限。寻找折价债权，获得更多折让收益，无投资门槛。</div>
     <div class="right">
        <p class="p1">新手引导</p>
        <p><a href="#">什么是债权转让？</a><a href="#">债权的价值是如何计算的？</a><a href="#">债权的转让价格是如何确定的？</a><a href="#">债权转让说明书</a></p>
     </div>
  </div>  
  <h2><p>债权转让列表</p><a href="#">理财计算器</a><span>单笔转让平均耗时<br />7分28秒</span><span>累计转让总笔数<br />81670笔</span><span>累计成交总金额<br />29090.86万元</span></h2>
  
  <div class="sanbiao_con2">
     <span>期限：</span>
     <select class="select_1">
        <option>全部</option>
        <option>3-6个月</option>
        <option>6-12个月</option>
        <option>12-24个月</option>
        <option>24-36个月</option>
     </select>
     <span>剩余本金：</span>
     <select class="select_1">
        <option>全部</option>
        <option>2000一下</option>
        <option>2000-5000元</option>
        <option>5000-10000</option>
        <option>10000以上</option>
     </select>
     <span>年化利率：</span>
     <select class="select_1">
        <option>全部</option>
        <option>10%</option>
        <option>10.5%</option>
        <option>11%</option>
        <option>12%</option>
     </select>
     <a href="#">筛选</a>
  </div>
  
  <h3><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%" height="45" class="con2_kk">借款标题</td>
    <td width="8%" height="45" class="con2_kk">信用等级</td>
    <td width="7%" height="45" class="con2_kk">年利率</td>
    <td width="8%" height="45" class="con2_kk">剩余期限</td>
    <td width="9%" height="45" class="con2_kk">每份价值</td>
    <td width="8%" height="45" class="con2_kk">剩余份额</td>
    <td width="8%" height="45" class="con2_kk">转让系数</td>
    <td width="9%" height="45" class="con2_kk">转出价格</td>
    <td width="9%" height="45" class="con2_kk">&nbsp;</td>
  </tr>
  <tr>
    <td height="55" class="con2_kk"><img src="images/icon3.jpg" /> <a href="#">项目投资，缺少一部分资金</a></td>
    <td height="55" class="con2_kk"><span class="con2_yuan">A</span></td>
    <td height="55" class="con2_kk">13.00%</td>
    <td height="55" class="con2_kk">25个月</td>
    <td height="55" class="con2_kk">36.98元</td>
    <td height="55" class="con2_kk">2份</td>
    <td height="55" class="con2_kk">69.18%</td>
    <td height="55" class="con2_kk">36.98元/份</td>
    <td height="55" align="center" class="con2_kk"><p class="con2_anniu" style="background:#e85e48;">购买</p></td>
  </tr> 
  <tr>
    <td height="55" class="con2_kk"><img src="images/icon3.jpg" /> <a href="#">项目投资，缺少一部分资金</a></td>
    <td height="55" class="con2_kk"><span class="con2_yuan">A</span></td>
    <td height="55" class="con2_kk">13.00%</td>
    <td height="55" class="con2_kk">25个月</td>
    <td height="55" class="con2_kk">36.98元</td>
    <td height="55" class="con2_kk">2份</td>
    <td height="55" class="con2_kk">69.18%</td>
    <td height="55" class="con2_kk">36.98元/份</td>
    <td height="55" align="center" class="con2_kk"><p class="con2_anniu" style="background:#e85e48;">购买</p></td>
  </tr>  
</table></h3>
  <h6 class="news_list2"><a href="#">〈</a> <a href="#" class="hover">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a> <a href="#">6</a> <a href="#">〉</a></h6>
</div>

</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
   	<script type="text/javascript">

		$(function(){
			asychronized_query(1);
		});
	
		function asychronized_query(pn){
			var url = "<%=__ROOT_PATH__%>/user/financial/financial/loan_list_query2.html";
			var borrow_duration = $("#select_1").val();
			var annulized_rate_int = $("#select_2").val();
			var borrow_all_money = $("#select_3").val();
			var gather_progress = $("#select_4").val();
			
			$.ajax({
				url:url,
				type:'post',
				data:{
					"borrow_duration":borrow_duration,
					"annulized_rate_int":annulized_rate_int,
					"borrow_all_money":borrow_all_money,
					"gather_progress":gather_progress,
					"pn" : pn
					
					},
				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table1 tr:not(:first)").empty(); 
					var tr_str ='';
					for(var i=0;i<jsObject.bid_list.length;i++ ){

						var gather_str = '<td height="55" align="center" class="con2_kk"><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.bid_list[i].id+'"><p class="con2_anniu con2_anniu2">投标</p></a></td>';
						var full_str = '<td height="55" align="center" class="con2_kk"><span href="javascript:void(0);"><p class="con2_anniu con2_anniu2">已满标</p></span></td>';
						var out_of_date_str ='<td height="55" align="center" class="con2_kk"><span href="javascript:void(0);"><p class="con2_anniu con2_anniu2">流标</p></span></td>';

						var str ;
						
						if(jsObject.bid_list[i].gather_state==1){
							str = gather_str;
						}else if(jsObject.bid_list[i].gather_state==2){
							str = out_of_date_str;
						}else if(jsObject.bid_list[i].gather_state==3){
							str = full_str;
						}
						
						tr_str += '<tr>'
						+ '<td height="55" class="con2_kk"><img src="<%=__PUBLIC__ %>/images/icon1.jpg" /> <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.bid_list[i].id+'">'+jsObject.bid_list[i].borrow_title+'</a></td>'
						+ '<td height="55" class="con2_kk"><span class="con2_yuan">'+jsObject.bid_list[i].credit_rating+'</span></td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].annulized_rate+'</td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].borrow_all_money+'元</td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].borrow_duration+'个月</td>'
						+ '<td height="55" class="con2_kk"><span class="hongse">'+jsObject.bid_list[i].gather_progress+'%</span></td>'
						+ str
						+ '</tr>';
					}
					$("#table1 tr:eq(0)").after(tr_str);
					$("#asyncPageDiv").html(jsObject.asyncPageDiv);
				}
			},"json");	
		}

		function change_page_of_async(pn){
			asychronized_query(pn);
		}
		
	</script>
</html>