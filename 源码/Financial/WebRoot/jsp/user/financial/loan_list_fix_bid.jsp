<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
	
  </head>
  
  <body>
<%@ include file="/jsp/index/index_top.jsp" %>
	<script type="text/javascript" src="<%=__PUBLIC__%>/js/asyncPage.js"></script>

<span id="ny_pic"></span>
<div class="sanbiao">
  <h1><a href="#">我要理财</a> > 散标投资列表</h1>    
  <div class="sanbiao_head">
  	<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_elite.html">精英散标</a>
  	<a class="hover" href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_lfoll_invest.html">联富宝</a>
  	<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_creditor_right.html">债权转让</a>
  </div>  
	<div class="sanbiao_list sanbiao_list2">
	  <div class="sanbiao_con">
	     <div class="left">宜定盈是宜人贷近期推出的一站式理财咨询服务。出借人通过宜定盈服务将资金出借给宜人贷平台上的精英标用户，采用智能投标、循环出借的方式，提高资金的流动率和利用率，从而增加实际收益。宜定盈独创一键式服务，能够使出借人在获取丰厚回报的同时，帮助白领人群提前实现梦想，达到双赢。</div>
	     <div class="right">
	        <p><span>&bull;</span>一键加入，自动投标，省心省力</p>
	        <p><span>&bull;</span>收益稳定，预期年化收益率10%</p>
	        <p><span>&bull;</span>资金安全，担保公司全额本息担保</p>
	        <p><span>&bull;</span>多种期限满足不同流动性需求</p>
	     </div>
	  </div>  
	  <h2>
	  <p>联富宝列表</p>
	  	<a href="#">理财计算器</a>
	  		<span>累计成交总金额<br />44.53亿元</span>
	  		<span>累计成交总笔数<br />81670笔</span>
	  		<span>为用户累计赚取<br />29090.86万元</span>
	  </h2>  
	  
	  <div class="U_jihua_head">
		  	<a onclick="change_page_of_async(1,3);" class="hover">联富宝L03(3个月)</a>
		  	<a onclick="change_page_of_async(1,6);">联富宝L06(6个月)</a>
		  	<a onclick="change_page_of_async(1,12);">联富宝L12(12个月)</a>
	  </div>
	  <div style="display:block;" class="U_jihua_content">
	     <table width="100%" cellspacing="0" cellpadding="0" id="L03_table">           
	        <tr id="L03_table_first_tr">
	           <td width="21%" class="td_1">联富宝名称</td>
	           <td width="21%" class="td_1">预计金额（元）</td>
	           <td width="12%" class="td_1">加入人次</td>
	           <td width="15%" class="td_1">预期年化收益</td>
	           <td width="15%" class="td_1">累计收益（元）</td>
	           <td width="16%" class="td_1">状态</td>
	        </tr>
	       <c:forEach items="${fix_bid_system_order_list}" var="model">
	        <tr>
	           <td class="td_2 ">
	           	<a style="color:#2ea7e0" href="<%=__ROOT_PATH__%>/user/financial/lfollinvest/get_lfoll_invest_info.html?id=${model.m.id}">
	           		${model.m.bid_name}
	           	</a>
	          </td>
	           <td class="td_2 td_xm">${model.m.total_money}</td>
	           <td class="td_2">${model.m.booking_number}人</td>
	           <td class="td_2">${model.m.annualized_rate}%</td>
	           <td class="td_2">${model.m.all_pay_back_money}</td>
	           <td class="td_2">
	           	<c:if test="${model.m.public_state_var == 1}">
	           		等待预定
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 2}">
	           		预定中
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 3}">
	           		等待支付
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 4}">
	           		等待开标
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 5}">
	           		开放中
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 6}">
	           		收益中
	           	</c:if>
	           	<c:if test="${model.m.public_state_var == 7}">
	           		已结束
	           	</c:if>
	           </td>
	        </tr>
       </c:forEach>
	     </table>
	     <span id="page_L03">${AsyncPageDiv}</span>
	  </div>  
	  
	  <div style="display:none;" class="U_jihua_content">
	     <table width="100%" cellspacing="0" cellpadding="0" id="L06_table">           
	        <tr id="L06_table_first_tr">
	           <td width="21%" class="td_1">计划名称</td>
	           <td width="21%" class="td_1">计划金额（元）</td>
	           <td width="12%" class="td_1">加入人次</td>
	           <td width="15%" class="td_1">预期年化收益</td>
	           <td width="15%" class="td_1">累计收益（元）</td>
	           <td width="16%" class="td_1">状态</td>
	        </tr>
	        <tr>
	           <td class="td_2">联富金融-B141105期</td>
	           <td class="td_2 td_xm">10,000,000</td>
	           <td class="td_2">0人</td>
	           <td class="td_2">7.0%</td>
	           <td class="td_2">0.00</td>
	           <td class="td_2">等待预定/td>
	        </tr>
	        <tr>
	           <td class="td_3">联富金融-B141103期</td>
	           <td class="td_3 td_xm">10,000,000 </td>
	           <td class="td_3">296人</td>
	           <td class="td_3">7.0%</td>
	           <td class="td_3">0.00</td>
	           <td class="td_3">预定满额</td>
	        </tr>
	        <tr>
	           <td class="td_2">联富金融-B141029期</td>
	           <td class="td_2 td_xm">8,000,000</td>
	           <td class="td_2">483人</td>
	           <td class="td_2">7.0%</td>
	           <td class="td_2">0.00</td>
	           <td class="td_2">收益中</td>
	        </tr>
	        <tr>
	           <td class="td_3">联富金融-B141027期</td>
	           <td class="td_3 td_xm">2,000,000</td>
	           <td class="td_3">155人</td>
	           <td class="td_3">7.0%</td>
	           <td class="td_3">0.00</td>
	           <td class="td_3">收益中</td>
	        </tr>         
	     </table>
	     <span id="page_L06">${AsyncPageDiv}</span>
	  </div>
	  
	  <div style="display:none;" class="U_jihua_content">
	     <table width="100%" cellspacing="0" cellpadding="0" id="L12_table">           
	        <tr id="L12_table_first_tr">
	           <td width="21%" class="td_1">计划名称</td>
	           <td width="21%" class="td_1">计划金额（元）</td>
	           <td width="12%" class="td_1">加入人次</td>
	           <td width="15%" class="td_1">预期年化收益</td>
	           <td width="15%" class="td_1">累计收益（元）</td>
	           <td width="16%" class="td_1">状态</td>
	        </tr>
	        <tr>
	           <td class="td_2">联富金融-C141105期</td>
	           <td class="td_2 td_xm">10,000,000</td>
	           <td class="td_2">0人</td>
	           <td class="td_2">7.0%</td>
	           <td class="td_2">0.00</td>
	           <td class="td_2">等待预定/td>
	        </tr>
	        <tr>
	           <td class="td_3">联富金融-C141103期</td>
	           <td class="td_3 td_xm">10,000,000 </td>
	           <td class="td_3">296人</td>
	           <td class="td_3">7.0%</td>
	           <td class="td_3">0.00</td>
	           <td class="td_3">预定满额</td>
	        </tr>
	        <tr>
	           <td class="td_2">联富金融-C141029期</td>
	           <td class="td_2 td_xm">8,000,000</td>
	           <td class="td_2">483人</td>
	           <td class="td_2">7.0%</td>
	           <td class="td_2">0.00</td>
	           <td class="td_2">收益中</td>
	        </tr>
	        <tr>
	           <td class="td_3">联富金融-C141027期</td>
	           <td class="td_3 td_xm">2,000,000</td>
	           <td class="td_3">155人</td>
	           <td class="td_3">7.0%</td>
	           <td class="td_3">0.00</td>
	           <td class="td_3">收益中</td>
	        </tr>        
	     </table>
	       <span id="page_L12">${AsyncPageDiv}</span>
	  </div>
	  <%--
	  <h6 class="news_list2">
	  	<a href="#">〈</a>
	  	<a href="#" class="hover">1</a>
	  	<a href="#">2</a>
	  	<a href="#">3</a>
	  	<a href="#">4</a>
	  	<a href="#">5</a>
	  	<a href="#">6</a>
	  	<a href="#">〉</a>
	  </h6>
	--%>
	</div>
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>