<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>${applicationScope.title}</title>
  </head>
  <body>
  
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>


<div class="sanbiao">
   <div class="calculator_side">
      <h1><a href="<%=__ROOT_PATH__%>/user/financial/financial/to_calculator_borrower.html">我要借款</a> > 借款计算器</h1>
  
      <div class="calculator_lc">
         <div class="up">借款设置</div>
            <div class="content content_2" >
               <div class="con_1">
                  <label class="label_1">借款金额</label>
                  <input id="borrow_money" class="text_1" type="text" onfocus="showP1();" onblur="hideP1();" />
                  <span class="span_1">元</span>
                  <p id="p1" class="p_1"><img src="<%=__PUBLIC__%>/images/loan_1.png" />金额范围3,000-500,000</p>
               </div>
               
               <div class="con_1">
                  <label class="label_1">借款期限</label>
                  <dl class="select">
                     <dt id="borrow_duration">3个月</dt>
                     <dd>
                        <ul>
                           <li><a>3个月</a></li>
                           <li><a>6个月</a></li>
                           <li><a>9个月</a></li>
                           <li><a>12个月</a></li>
                           <li><a>15个月</a></li>
                           <li><a>18个月</a></li>
                           <li><a>24个月</a></li>
						   <li><a>36个月</a></li>
                        </ul>
                     </dd>
                  </dl>
               </div>
			   
			   <div class="con_1">
                  <label class="label_1">年化利率</label>
                  <input id="annulized_rate_int" class="text_1" onfocus="showP3();" onblur="hideP3();" type="text" />
                  <span class="span_1">%</span>
                  <p id="p3" class="p_1"><img src="<%=__PUBLIC__%>/images/loan_1.png" />利率范围<br />5%-24%</p>
               </div>
			   
			   <div class="con_1">
                  <label class="label_1"></label>
                  <span class="span_1"></span>
                  <p class="p_1"></p>
               </div>
               
               <div class="con_1">
                  <label class="label_1">信用等级</label>
                  <p class="p_2"><span>HR</span><a onClick="showsubmenu(1)">选择其它信用等级</a></p>
               </div>   
               
               <div class="con_1">
                  <label class="label_1">月综合费率</label>
                  <p class="p_3">0.88%</p>
               </div>
               
               <div class="con_3" id="submenu1" class="submenu" style="display:none;">
                  <table class="table_4" width="100%" border="0" cellspacing="0" cellpadding="0">
                     <tr class="tr_1">
                        <td class="td_1" width="16%">信用等级</td>
                        <td width="14%"><span style="background:#ffd700;">A+</span></td>
                        <td width="14%"><span style="background:#fded94;">A</span></td>
                        <td width="14%"><span style="background:#7ae805;">B+</span></td>
                        <td width="14%"><span style="background:#d0fba2;">B</span></td>
                        <td width="14%"><span style="background:#908442;">C+</span></td>
                        <td width="14%"><span style="background:#2a2715;">C</span></td>
                     </tr>
                     <tr>
                        <td class="td_1" width="16%">月综合费率</td>
                        <td width="14%">0.55%</td>
                        <td width="14%">0.60%</td>
                        <td width="14%">0.65%</td>
                        <td width="14%">0.70%</td>
                        <td width="14%">0.75%</td>
                        <td width="14%">0.80%</td>
                     </tr>
                     <tr>
                        <td class="td_1" width="16%">期初服务费率</td>
                        <td width="14%">0%</td>
                        <td width="14%">1%</td>
                        <td width="14%">2%</td>
                        <td width="14%">2.5%</td>
                        <td width="14%">3%</td>
                        <td width="14%">4%</td>
                     </tr>
                  </table>
                  <div class="table_on">
                     <table class="table_5" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td width="16%">&nbsp;</td>
                           <td width="14%"><a>&nbsp;</a></td>
                           <td width="14%"><a>&nbsp;</a></td>
                           <td width="14%"><a>&nbsp;</a></td>
                           <td width="14%"><a>&nbsp;</a></td>
                           <td width="14%"><a>&nbsp;</a></td>
                           <td width="14%"><a>&nbsp;</a></td>
                        </tr>
                     </table>
                  </div>
               </div>
              
               <div class="con_2">
                  <input class="button_1" type="button" onclick="begin_calculate();" value="开始计算" />
               </div>            
            </div>
            <div class="up">借款计算</div>
            <div class="content_2">
               <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td>月还款额</td>
                     <td>期初服务费</td>
                     <td>总还款额</td>
                  </tr>
                  <tr class="tr_1">
                     <td id="monthly_principal_and_interest">￥0.00</td>
                     <td>￥0.00</td>
                     <td id="total_money">￥0.00</td>
                  </tr>
               </table>
            </div>
            <div class="up">偿还时间表</div>  
            <div class="content_2">
               <table id="table1" class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr class="tr_1">
                     <td class="td_1" width="20%">月份</td>
                     <td width="20%">月还款额</td>
                     <td width="20%">月还本金</td>
                     <td width="20%">借款管理费</td>
                     <td width="20%">本金余额</td>
                  </tr>
                  <!-- 
                  <tr class="tr_2">
                     <td class="td_1" width="20%">1月</td>
                     <td width="20%">202.60</td>
                     <td width="20%">198.40</td>
                     <td width="20%">4.20</td>
                     <td width="20%">810.00</td>
                  </tr>
                  <tr class="tr_3">
                     <td class="td_1" width="20%">1月</td>
                     <td width="20%">202.60</td>
                     <td width="20%">198.40</td>
                     <td width="20%">4.20</td>
                     <td width="20%">810.00</td>
                  </tr>
                  -->
               </table>
            </div> 
      </div>      
      
   </div>
</div>
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>

	<script type="text/javascript">

	
	function begin_calculate(){
		var borrow_money = $("#borrow_money").val();
		var annulized_rate_int = $("#annulized_rate_int").val();
		var borrow_duration = $("#borrow_duration").html();
		if(borrow_money == null || borrow_money == ""){
			return ;
		}
		if(borrow_money < 3000 || borrow_money > 500000){
			return ;
		}
		if(annulized_rate_int == null || annulized_rate_int == ""){
			return ;
		}
		if(annulized_rate_int < 5 || annulized_rate_int > 24){
			return ;
		}

		var url = "<%=__ROOT_PATH__%>/user/financial/financial/calculate_elite_bid_borrow.html";
		
		$.ajax({
			url:url,
			type:'post',
			data:{
				"borrow_money":borrow_money,
				"annulized_rate_int":annulized_rate_int,
				"borrow_duration":borrow_duration
			},
			cache:false,
			success:function(data)
			{
				var jsObject = JSON.parse(data);

				$("#table1 tr:not(:first)").empty(); 
				
				var tr_str ='';
				for(var i=0;i<jsObject.repayment_plan_list.length;i++ ){

					//条纹样式的控制
					var tr_class;
					if(i%2 == 0){
						tr_class = 'tr_2';
					}else{
						tr_class = 'tr_3';
					}
					tr_str += 

						'<tr class="'+ tr_class +'">'
	                    + '<td class="td_1" width="20%">'+ (i+1) +'月</td>'
	                    + '<td width="20%">'+ jsObject.repayment_plan_list[i].month_pricipal_and_interest.toFixed(2) +'</td>'
	                    + '<td width="20%">'+ jsObject.repayment_plan_list[i].month_pricipal.toFixed(2) +'</td>'
	                    + '<td width="20%">'+ jsObject.manage_fee +'</td>'
	                    + '<td width="20%">'+ jsObject.repayment_plan_list[i].remain_principal.toFixed(2) +'</td>'
	                 + '</tr>';
				}
				$("#table1 tr:eq(0)").after(tr_str);
				
				$("#monthly_principal_and_interest").html("￥"+jsObject.monthly_principal_and_interest);
				$("#total_money").html("￥"+jsObject.total_money);
				//$("#pricipal_interest").html(jsObject.monthly_principal_and_interest+"元");
			}
		},"json");	
		
	}

	$(function(){
		$(".select").each(function(){
			var s=$(this);
			var z=parseInt(s.css("z-index"));
			var dt=$(this).children("dt");
			var dd=$(this).children("dd");
			var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
			var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
			dt.click(function(){dd.is(":hidden")?_show():_hide();});
			dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
			$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
		})
	});

	function showP1(){
		$("#p1").show();
	}
	function hideP1(){
		$("#p1").hide();
	}
	function showP2(){
		$("#p2").show();
	}
	function hideP2(){
		$("#p2").hide();
	}
	function showP3(){
		$("#p3").show();
	}
	function hideP3(){
		$("#p3").hide();
	}
	function showP4(){
		$("#p4").show();
	}
	function hideP4(){
		$("#p4").hide();
	}
  	
	
		function showsubmenu(sid){
		    var whichEl = document.getElementById("submenu" + sid);
		    whichEl.style.display = whichEl.style.display =='none'?'':'none';
		}
</script>

</body>
</html>