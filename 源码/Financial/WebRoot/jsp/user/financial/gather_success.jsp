<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
  </head>
  <%
  //需要检查三个方面
  %>
  <body>
  	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
<%@ include file="/jsp/index/index_top.jsp" %>
<span id="ny_pic"></span>


<div class="about">
  <div class="about_con">
     <div class="loan_aa">
        <div class="loan_aa_top">
           <div class="up">
              <a href="#">我要借款</a>&nbsp;<span>></span>
              <c:choose>
              	<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
              	<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
              </c:choose>
              <span>></span>&nbsp;填写借款申请
           </div>
           <div class="center">
              <span style="background:#ed5050; color:#fff;">1</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">2</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">3</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">4</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">5</span>
           </div>
           <div class="down">
              <span style="color:#cc0000;">填写借款申请</span>
              <span style="margin-left:55px; color:#cc0000;">填写借款信息</span>
              <span style="margin-left:60px; color:#cc0000;">等待审核</span>
              <span style="margin-left:60px;color:#cc0000;">筹集借款</span>
              <span style="margin-left:55px;color:#cc0000;">获得借款</span>
           </div>
        </div>
        <div class="loan_cc"><s>&bull;</s>您已经获得了${borrow_all_money}元的借款，请按时还款保持良好的信用记录。</div>
        <div class="loan_ee_up">
           <p>借款计算</p>
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                 <td width="50%">月还款额</td>
                 <td width="50%">总还款额</td>
              </tr>
              <tr class="tr_1">
                 <td width="50%">￥${monthly_principal_and_interest}</td>
                 <td width="50%">￥${should_repay_total}</td>
              </tr>
              <tr>
                 <td colspan="2">&nbsp;</td>
              </tr>
           </table>
        </div> 
        <div class="loan_ee_up loan_ee_con">
           <p>偿还时间表</p>
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="tr_2">
                 <td width="156">还款时间</td>
                 <td width="156">月还款额</td>
                 <td width="156">月还本金</td>
                 <td width="167">月还利息</td>
                 <td width="169">借款管理费</td>
                 <td width="170" class="td_1">本金余额</td>
              </tr>
              
              <c:forEach items="${repayment_plan_list}" var="repayment_plan">
              	<tr>
                 <td width="156"> ${repayment_plan.r.automatic_repayment_date}</td>
                 <td width="156">${repayment_plan.r.should_repayment_total}</td>
                 <td width="156">${repayment_plan.r.should_repayment_principle}</td>
                 <td width="167">${repayment_plan.r.should_repayment_interest}</td>
                 <td width="169">${repayment_plan.r.manage_fee_by_hand}</td>
                 <td width="170" class="td_1">${repayment_plan.r.pricipal_balance}</td>
              </tr>
              </c:forEach>
           </table>
        </div>                   
              
     </div>
  </div>  
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
	
  </body>
  
  	<script type="text/javascript">	


	</script>
  
</html>
