<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
  </head>
  
  <body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>





<div id="pg-borrow-prod" style=" background:#f2f2f2;overflow:auto; padding-bottom:25px;position:relative; z-index:1">
  <div class="container_12">
            <div class="grid_12">
                <div class="info-list">
                    <h1 class="h3 bg-blue">消费贷<span>(适用非企业消费人群)</span></h1>

                    <div class="info-list-condition border-bt">
                        <h2 class="h4 mb15">申请条件</h2>
                        <ul class="ui-list-disc">
                            <li><i></i>22-55周岁的中国公民</li>
                            <li><i></i>在现单位工作满3个月</li>
                            <li><i></i>月收入2000以上</li>
                        </ul>
                        <div class="review-button">
                            <a onclick="judge_borrow_qualification();"  class="ui-button ui-button-blue ui-button-mid">申请借款</a>
                        </div>
                    </div>
                    <div class="info-list-material">
                        <h2 class="h4 mb15">必要申请资料</h2>
                        <ul class="ui-list-disc">
                            <li>
                                <h5>1. 身份认证<em>（请同时提供下面两项资料）</em></h5>
                                <div class="idcard-div">
                                  <span data="1" class="card-icon icon-id">
                                  	<a href="<%= __PUBLIC__ %>/images/shengfenzheng.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic1.png" /></a>
                                  	</span><em style="left:75px;top:125px;">身份证<br>本人身份证原件的正、反两面照片</em>
                                  <span data="2" class="card-icon icon-id2">
                                  	<a href="<%= __PUBLIC__ %>/images/id_example_girl.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic2.png" /></a>
                                  </span><em style="left:387px;top:125px;">本人手持身份证正面头部照</em>
                                </div>
                            </li>
                            <li>
                                <h5>2. 信用报告<em>（可去当地人民银行打印，部分地区可登陆个人信用信息服务平台）</em></h5>
                                <div class="idcard-div credit-div">
                                    <span data="3" class="card-icon icon-credit"><a href="<%= __PUBLIC__ %>/images/xinyongbaogao.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic3.png" /></a></span>
                                    <em>个人信用报告<br>信用报告有效期为15天，您必须在15天内提供所有必要认证资料<br><a href="http://www.pbccrc.org.cn/zxzx/lxfs/lxfs.shtml" target="_blank">全国各地征信中心联系方式查询>></a><br><a target="_blank" href="https://ipcrs.pbccrc.org.cn/">个人信用信息服务平台&gt;&gt;</a></em>
                                </div>
                            </li>
                            <li>
                                <h5>3. 工作认证<em>（请提供下面任意一项资料）</em></h5>
                                <div class="idcard-div work-div">
                                    <span data="4" class="card-icon icon-con1"><a href="<%= __PUBLIC__ %>/images/laodonghetong.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic4.png" /></a></span><em style="left:77px;top:23px;">劳动合同<br>需加盖单位公章<br>(或劳动合同专用章)</em>
                                    <span data="5" class="card-icon icon-con2"><a href="<%= __PUBLIC__ %>/images/zaizhizhengming.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic5.png" /></a></span><em style="left:295px;top:23px;">在职证明<br>需1个月内开具且加盖单位公章<br>(或人力章、财务章)</em>
                                    <span data="6" class="card-icon icon-con3"><a href="<%= __PUBLIC__ %>/images/gongzuozheng.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic6.png" /></a></span><em style="left:547px;top:23px;">工作证<br>需包括姓名、照片、工作单位名称</em>
                                </div>
                            </li>
                            <li>
                                <h5>4. 收入认证<em>（请提供下面任意一项资料）</em></h5>
 								<div class="idcard-div liushui-div">
                                    <span data="7" class="card-icon icon-liushui"><a href="<%= __PUBLIC__ %>/images/yinhangliushui.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic7.png" /></a></span><em style="left:78px;top:127px;">银行流水单<br>可体现工资项的最近3个月的工资卡流水</em>
                                    <span data="8" class="card-icon icon-liushui2"><a href="<%= __PUBLIC__ %>/images/wangyinjietu.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic8.png" /></a></span><em style="left:387px;top:127px;">网银电脑截屏<br>可体现工资项的最近3个月的网银截屏</em>
                                </div>

                            </li>
                        </ul>
                    </div>
                        <div class="worm-tips" style="margin-left:100px;">
                          <span class="icon"></span>
                          <span class="txt">温馨提示：除进行上述认证外，建议您进行房产、购车认证，有助于提高您的审核通过率与信用额度。</span>
                        </div>
                    </div>


                    <div class="info-list-way borrow-way-div" style="">
                        <h2 class="h4 mb15">借款方式</h2>
                        <ul>
                            <li><span>借&nbsp;款&nbsp;额&nbsp;度&nbsp;</span>3,000-500,000</li>
                            <li><span>借款年利率</span>10%-24% <a class="calc" title="年利率计算器" target="_blank" href="/borrow/calculator.action?prodType=RRGXD"></a></li>
                            <li><span>借&nbsp;款&nbsp;期&nbsp;限</span>3、6、9、12、15、18、24、36个月</li>
                            <li><span>审&nbsp;核&nbsp;时&nbsp;间</span>1-3个工作日</li>
                            <li><span>还&nbsp;款&nbsp;方&nbsp;式</span>等额本息，每月还款</li>
                        </ul>
                    </div>
                    <div class="info-list-fee borrow-way-div borrow-info-div">
                        <h2 class="h4 mb15">费用说明</h2>
                        <ul>
                            <li class="range">
                               <h4>期初服务费</h4>
                                <h5>由联富金融平台收取</h5>
                               <table width="70%" class="table" border=0 cellspacing=0
								cellpadding=0>
								<thead>
									<tr>
										<td width="16%">
											信用等级
										</td>
										<th width="14%">
											<i class="ui-creditlevel ui-creditlevel-s AA snow">A+</i>
										</th>
										<th width="14%">
											<i class="ui-creditlevel ui-creditlevel-s A snow">A</i>
										</th>
										<th width="14%">
											<i class="ui-creditlevel ui-creditlevel-s BB snow">B+</i>
										</th>
										<th width="14%">
											<i class="ui-creditlevel ui-creditlevel-s B snow">B</i>
										</th>
										<th width="14%">
											<i class="ui-creditlevel ui-creditlevel-s CC snow">C+</i>
										</th>
										<th width="14%">
											<i class="ui-creditlevel ui-creditlevel-s C snow">C</i>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											服务费率
										</td>
										<td style="padding-left:7px;" class="text-center-ie">
											0%
										</td>
										<td style="padding-left:7px;" class="text-center-ie">
											1%
										</td>
										<td style="padding-left:7px;" class="text-center-ie">
											2%
										</td>
										<td style="padding-left:7px;" class="text-center-ie">
											3%
										</td>
										<td style="padding-left:7px;" class="text-center-ie">
											4%
										</td>
										<td style="padding-left:7px;" class="text-center-ie">
											5%
										</td>
									</tr>
								</tbody>
							</table>
                            </li>
                            <li class="info">
                                <h4>每月还款额</h4>
                                <ul>
                                    <li><span>本金及利息</span>支付给理财人</li>
                                    <li><span>借款管理费</span>由联富金融平台收取，借款本金的<em>0.3%</em></li>

                                </ul>
                            </li>
                        </ul>
                        <div class="review-button btn-div">
                           
                        </div>
                    </div>
                </div>
            </div>
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
	
	
  </body>
  <script type="text/javascript">
		//进行贷款申请前的判断-这个人是否能够借贷
		function judge_borrow_qualification(){	
			$.post('<%=__ROOT_PATH__%>/user/financial/borrow/judge_borrow_qualification.html?borrow_type=1',   //1工薪贷2生意贷
					{

					},
					function(data)
					{
					 if(data=='1_1'){ /**基本用户信息验证*/
							 showMessage(["提示","请先注册或者登录"]);
					 }else if(data=='1_2'){
							showMessage(["提示","请先进行邮箱认证"]);
					 }else if(data=='1_3'){
							showMessage(["提示","该用户被禁用,请先联系管理员"]);
					 }else if(data=='1_4'){ //修改用户借款认证信息
							showMessage(["提示","该用户还没有实名认证,请先实名认证"]);
					 }else if(data=='1_5'){ /**借贷申请信息验证*/
							showMessage(["提示","该用户还没有绑定手机,请先绑定手机"]);
					 }else if(data=='1_6'){
							showMessage(["提示","非法用户类型:既不是理财也不是借贷"]);
					 }else if(data=='1_7'){
							showMessage(["提示","理财用户不能进行借贷"]);
							//##
							//##
					 }else if(data=='2_1'){ /**借贷种类*/
							showMessage(["提示","非法借贷种类"]);
					 }else if(data=='2_2'){
							showMessage(["提示","您有一笔贷款正在申请或者还款,暂时不能申请其他贷款"]);
					 }else if(data=='2_3'){
							showMessage(["提示","您有一笔贷款正在申请或者还款,暂时不能申请其他贷款"]);
					 }else if(data=='2_4'){
							showMessage(["提示","您有一笔贷款正在申请或者还款,暂时不能申请其他贷款"]);
					 }else if(data=='2_5'){
							showMessage(["提示","提示在没有借贷的类型的情况下面出现了借贷记录未还的情况,请联系管理员"]);
					 }else if(data=='2_6'){//xx=0 and yy=0
						 window.location.href="<%=__ROOT_PATH__%>/user/financial/borrow/loan_to_write_borrow_apply.html?borrow_type=1";
						//##
						//##
					 }else if(data=='3_0'){/**凑集状态*/
						    // 借款单中没有记录
						 showMessage(["提示","系统查找不到申请单或者凑集单的记录"]);
						}else if(data=='3_1'){/**凑集状态*/
							//单子创建完成，跳到填写借款认证信息页面-->填写和修改用户认证信息
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/persional_info/persional_index.html";  //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_2'){//apply_state= 2 3 4 都是那个审核页面
						    // 填写认证信息完成-但是由于非必填的资料没有上传-那么可以在...天内仍然可以修改-->填写和修改用户认证信息 234
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/submit_application/auditing_index.html";  //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_3'){
							//跳到审核页面...只能是审核中的-审核失败的不行
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/submit_application/auditing_index.html";  //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_4'){
							//被审核员驳回,填写和修改用户认证信息
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/submit_application/auditing_index.html"; //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_5'){
							// 审核不通过,可以继续申请借款-但程序是执行不到这
							showMessage(["提示","审核不通过,可以继续申请借款-但程序是执行不到这"]);
						}else if(data=='3_e'){
							showMessage(["提示","bug:非法状态"]);
							//##
							//##
						}else if(data=='4_0'){//凑集单状态
							showMessage(["提示","提示系统出现了问题-就是根据审核成功申请单去凑集单查不到记录-需要您联系管理员"]);
						}else if(data=='4_1'){
							window.location.href="<%=__ROOT_PATH__%>/user/financial/borrow/to_gather_money_page.html?borrow_type=1";
						}else if(data=='4_2'){
							//筹集失败，可以继续申请借款-基本上不会执行到这里-如果执行到这里那么程序就是错的-必须给下次的凑集的机会
						}else if(data=='4_3'){
							//该还回值不存在
						}else if(data=='4_e'){
							showMessage(["提示","bug:非法状态"]);
							//##
							//##
							//还款状态
						} else if (data == '5_1'){
							//该值不存在
						}else if (data == '5_2'){
							//跳转至凑集成功页面
							//showMessage(["提示","跳转至凑集成功页面...add page"]);
                            window.location.href="<%=__ROOT_PATH__%>/user/financial/borrow/to_gather_success_page.html?borrow_type=1";
						}else if (data == '5_3'){
							// 还款完成，可以借款-基本上不会执行到这里-如果执行到这里那么程序就是错的
						}else {
							showMessage(["提示","bug:just else is here..."]);
						}
						
					}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
		}
	</script>
  
</html>
