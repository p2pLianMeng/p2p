<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>

<html xmlns="http://www.w3.org/1999/xhtml">
  <body>
<div class="float_kf">
   <a class="a_1" onclick="changeClass()"><span class="up"></span><span class="down">客<br />服<br />帮<br />助<br />我<br /><img id="mage" src="<%= __PUBLIC__ %>/images/float_2.png" /></span></a>
   <div class="float_con">
      <p>免费电话咨询</p>
      <input id="phone" class="text" type="text" onFocus="clean();" value="您的手机号" />
      <h6 id="ok" style="display:none;color:red">预约成功，我们将尽快给您联系</h6>
      <h6 id="no" style="display:none;color:red">预约失败，请联系在线客服</h6>
      <h6 id="err" style="display:none;color:red">号码有误！</h6>
      <h6 id="wait" style="display:none;color:red">您已经预约，请耐心等待</h6>
      <input class="button" type="button" onclick="submit_appointment();" value="立即预约" />
      <span>注：全程通话，完全免费</span>
   </div>
</div>
	
 </body>
  
  	<script type="text/javascript">	
		function submit_appointment(){
			var phone = $("#phone").val();
			$.ajax({
				url:"<%=__ROOT_PATH__%>/user/financial/borrow/submit_user_appointment.html",
				type:'post',
				cache:false,
				async:false,
				data:{"phone":phone},
				success:function(data){
					if(data == "ok"){
						//showMessage(["提示","预约成功！"]);
						$("#ok").show();
					}else if(data == "no"){
						$("#no").show();
						//showMessage(["提示","预约失败！"]);
						setTimeout('hide()',2000);
					}else if(data == "err"){
						$("#err").show();
						setTimeout('hide()',2000);
						//showMessage(["提示","号码有误！"]);
					}else if(data == "wait"){
						$("#wait").show();
						setTimeout('hide()',2000);
						//showMessage(["提示","已经预约，请耐心等待！"]);
					}else{
						showMessage(["提示","网络错误，请检查网络！"]);
					}
				}
			},"html")
		}
function hide(){
	$("#no").hide();
	$("#err").hide();
	$("#wait").hide();
}
function clean(){
		$("#phone").val("");
		}
function changeClass(){
	$(".float_con").toggle();	
	element=document.getElementById('mage')
	if(element.src.match("2"))
	{
		element.src="<%= __PUBLIC__ %>/images/float_3.png";
	}
	else{
		element.src="<%= __PUBLIC__ %>/images/float_2.png"
	}	
}
	</script>
  
</html>
