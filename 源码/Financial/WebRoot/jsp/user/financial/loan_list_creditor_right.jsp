<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>${applicationScope.title}</title>
	
	<script src="js/loginDialog.js" type="text/javascript"></script>
    <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />

  </head>
  
  <body>
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>



<div class="sanbiao">
  <h1><a href="#">我要理财</a> > 散标投资列表</h1>    
  <div class="sanbiao_head">
  	<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_elite.html">精英散标</a>
  	<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_lfoll_invest.html">联富宝</a>
  	<a class="hover" href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_creditor_right.html">债权转让</a>
  </div>  
  



<div class="sanbiao_list sanbiao_list3">
  <div class="sanbiao_con">
     <div class="left">平台提供债权转让功能，增加资金流动性。用户可自行转让资产组合中符合条件的债权，也可购买其他用户转让的债权，从而获得折让收益及借款标的后续收益。债权持有90天后即可进行转让，转让次数不限。寻找折价债权，获得更多折让收益，无投资门槛。</div>
     <div class="right">
        <p class="p1">新手引导</p>
        <p><a href="#">什么是债权转让？</a><a href="#">债权的价值是如何计算的？</a><a href="#">债权的转让价格是如何确定的？</a><a href="#">债权转让说明书</a></p>
     </div>
  </div>  
  <%--<h2>
  	<p>债权转让列表</p>
  	<a href="#">理财计算器</a>
  	<span>单笔转让平均耗时<br />7分28秒</span>
  	<span>累计转让总笔数<br />81670笔</span>
  	<span>累计成交总金额<br />29090.86万元</span>
 </h2>
  --%>
  <div class="sanbiao_con2">
     <span>剩余期限</span>
     <select class="select_1">
        <option>全部</option>
        <option>3-6个月</option>
        <option>6-12个月</option>
        <option>12-24个月</option>
        <option>24-36个月</option>
     </select>
     <!-- 
     <span>剩余本金：</span>
     <select class="select_1">
        <option>全部</option>
        <option>2000一下</option>
        <option>2000-5000元</option>
        <option>5000-10000</option>
        <option>10000以上</option>
     </select>
     -->
     <span>年化利率：</span>
     <select class="select_1">
        <option>全部</option>
        <option>10%</option>
        <option>10.5%</option>
        <option>11%</option>
        <option>12%</option>
     </select>
     <a href="#">筛选</a>
  </div>
  
  <h3>
  <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%" height="45" class="con2_kk">借款标题</td>
    <td width="8%" height="45" class="con2_kk">信用等级</td>
    <td width="7%" height="45" class="con2_kk">年利率</td>
    <td width="8%" height="45" class="con2_kk">剩余期限</td>
    <td width="9%" height="45" class="con2_kk">每份价值</td>
    <td width="8%" height="45" class="con2_kk">剩余份额</td>
    <td width="8%" height="45" class="con2_kk">转让系数</td>
    <td width="9%" height="45" class="con2_kk">转出价格</td>
    <td width="9%" height="45" class="con2_kk">&nbsp;</td>
  </tr>
 <tr>
 	<td colspan="9" style="text-align: center;">暂无数据!</td>
 </tr>
<tr>
    <td height="55" class="con2_kk"><img src="<%=__PUBLIC__%>/images/icon3.jpg" /> <a href="#">项目投资，缺少一部分资金</a></td>
    <td height="55" class="con2_kk"><span class="con2_yuan">A</span></td>
    <td height="55" class="con2_kk">13.00%</td>
    <td height="55" class="con2_kk">25个月</td>
    <td height="55" class="con2_kk">36.98元</td>
    <td height="55" class="con2_kk">2份</td>
    <td height="55" class="con2_kk">69.18%</td>
    <td height="55" class="con2_kk">36.98元/份</td>
    <td height="55" align="center" class="con2_kk"><p class="con2_anniu" style="background:#e85e48;">购买</p></td>
  </tr> 
  <tr>
    <td height="55" class="con2_kk"><img src="<%=__PUBLIC__%>/images/icon3.jpg" /> <a href="#">项目投资，缺少一部分资金</a></td>
    <td height="55" class="con2_kk"><span class="con2_yuan">A</span></td>
    <td height="55" class="con2_kk">13.00%</td>
    <td height="55" class="con2_kk">25个月</td>
    <td height="55" class="con2_kk">36.98元</td>
    <td height="55" class="con2_kk">2份</td>
    <td height="55" class="con2_kk">69.18%</td>
    <td height="55" class="con2_kk">36.98元/份</td>
    <td height="55" align="center" class="con2_kk"><p class="con2_anniu" style="background:#e85e48;">购买</p></td>
  </tr>  
</table>
</h3>
	<h6 id="asyncPageDiv" >
		 ${asyncPageDiv}
	</h6>
</div>
<div style="margin-left:235px;"><script type="text/javascript">
     document.write('<a style="display:none!important" id="tanx-a-mm_15962270_8698819_29284887"></a>');
     tanx_s = document.createElement("script");
     tanx_s.type = "text/javascript";
     tanx_s.charset = "gbk";
     tanx_s.id = "tanx-s-mm_15962270_8698819_29284887";
     tanx_s.async = true;
     tanx_s.src = "http://p.tanx.com/ex?i=mm_15962270_8698819_29284887";
     tanx_h = document.getElementsByTagName("head")[0];
     if(tanx_h)tanx_h.insertBefore(tanx_s,tanx_h.firstChild);
</script></div>
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>

	<script type="text/javascript">

		$(function(){
			get_debt_transfer_list(1);
		});
	
		//获取债权转让列表   
		function get_debt_transfer_list(pn){

			var url = "<%=__ROOT_PATH__%>/user/financial/transfer/get_debt_transfer_list.html";
			
			$.ajax({
				url:url,
				type:'post',
				data:{
					"pn":pn
				},
				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table1 tr:not(:first)").empty(); 
					var tr_str ='';
					for(var i=0;i<jsObject.creditor_right_transfer_out_list.length;i++ ){

						var rate_style = '';
						
						/*
						A+：#e62129；A：#ff5a08；B+：#ffcc30；B：#7ad84c；C+：#2ec6f5；C：#2a62f7
						*/
						if(jsObject.creditor_right_transfer_out_list[i].credit_rating == 'A+'){
							rate_style = 'style="background:#e62129;"';
						}else if(jsObject.creditor_right_transfer_out_list[i].credit_rating == 'A'){
							rate_style = 'style="background:#ff5a08;"';
						}else if(jsObject.creditor_right_transfer_out_list[i].credit_rating == 'B+'){
							rate_style = 'style="background:#ffcc30;"';
						}else if(jsObject.creditor_right_transfer_out_list[i].credit_rating == 'B'){
							rate_style = 'style="background:#7ad84c;"';
						}else if(jsObject.creditor_right_transfer_out_list[i].credit_rating == 'C+'){
							rate_style = 'style="background:#2ec6f5;"';
						}else if(jsObject.creditor_right_transfer_out_list[i].credit_rating == 'C'){
							rate_style = 'style="background:#2a62f7;"';
						}
						<%--
						var duration_str = '';
						if(jsObject.creditor_right_transfer_out_list[i].borrow_duration != 0){
							duration_str = jsObject.creditor_right_transfer_out_list[i].borrow_duration + '个月';
						}else{
							duration_str = jsObject.creditor_right_transfer_out_list[i].borrow_duration_day + '天';
						}
						--%>

						var icon_str = '';
						if(jsObject.creditor_right_transfer_out_list[i].borrow_type==1){
							icon_str = 'icon1.jpg';
						}else if(jsObject.creditor_right_transfer_out_list[i].borrow_type==2){
							icon_str = 'icon1_2.jpg';
						}else if(jsObject.creditor_right_transfer_out_list[i].borrow_type==3){
							icon_str = 'icon1_3.jpg';
						}

						var state_str = '';
						if(jsObject.creditor_right_transfer_out_list[i].state==1){
							state_str = '<td height="55" align="center" class="con2_kk"><a href="<%=__ROOT_PATH__%>/user/financial/transfer/to_debt_transger_detail.html?transfer_out_order_id='+ jsObject.creditor_right_transfer_out_list[i].id +'"><p class="con2_anniu" style="background:#e85e48;">购买</p></a></td>';
						}else if(jsObject.creditor_right_transfer_out_list[i].state==2){
							state_str = '<td height="55" align="center" class="con2_kk"><p class="con2_anniu"">已完成</p></td>';
						}

						

						<%--
						tr_str += '<tr>'
						+ '<td height="55" class="con2_kk"><img src="<%=__PUBLIC__ %>/images/'+ icon_str +'" /> <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.creditor_right_transfer_out_list[i].id+'">'+jsObject.creditor_right_transfer_out_list[i].borrow_title+'</a></td>'
						+ '<td height="55" class="con2_kk"><span class="con2_yuan" '+ rate_style +'>'+ jsObject.creditor_right_transfer_out_list[i].credit_rating +'</span></td>'
						+ '<td height="55" class="con2_kk">'+jsObject.creditor_right_transfer_out_list[i].annulized_rate_int+'%</td>'
						+ '<td height="55" class="con2_kk">'+jsObject.creditor_right_transfer_out_list[i].borrow_all_money+'元</td>'
						+ '<td height="55" class="con2_kk">'+duration_str+'</td>'
						+ '<td height="55" class="con2_kk"><span class="hongse">'+jsObject.creditor_right_transfer_out_list[i].gather_progress+'%</span></td>'
						+ str
						+ '</tr>';
						--%>

						tr_str +=
							'<tr>'
							    + '<td height="55" class="con2_kk"><img src="<%=__PUBLIC__ %>/images/'+ icon_str +'" /> <a href="<%=__ROOT_PATH__%>/user/financial/transfer/to_debt_transger_detail.html?transfer_out_order_id='+ jsObject.creditor_right_transfer_out_list[i].id +'">'+jsObject.creditor_right_transfer_out_list[i].borrow_title+'</a></td>'
								+ '<td height="55" class="con2_kk"><span class="con2_yuan" '+ rate_style +'>'+ jsObject.creditor_right_transfer_out_list[i].credit_rating +'</span></td>'
								+ '<td height="55" class="con2_kk">'+ jsObject.creditor_right_transfer_out_list[i].annulized_rate_int+'%</td>'
							    + '<td height="55" class="con2_kk">'+ jsObject.creditor_right_transfer_out_list[i].remain_period +'个月</td>'
							    + '<td height="55" class="con2_kk">'+ jsObject.creditor_right_transfer_out_list[i].creditor_right_value +'元</td>'
							    + '<td height="55" class="con2_kk">'+ jsObject.creditor_right_transfer_out_list[i].transfer_share_remainder +'份</td>'
							    + '<td height="55" class="con2_kk">'+ jsObject.creditor_right_transfer_out_list[i].transfer_factor * 100 +'%</td>'
							    + '<td height="55" class="con2_kk">'+ jsObject.creditor_right_transfer_out_list[i].transfer_price +'元/份</td>'
							    + state_str
							+ '</tr>';  
						
					}
					$("#table1 tr:eq(0)").after(tr_str);
					$("#asyncPageDiv").html(jsObject.asyncPageDiv);
				}
			},"json");	
		}
		function change_page_of_async(pn){
			get_debt_transfer_list(pn);
		}
	</script>

  </body>
</html>