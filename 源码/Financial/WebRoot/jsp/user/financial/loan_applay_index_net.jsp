<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
  </head>
  
  <body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>





<div id="pg-borrow-prod" style=" background:#f2f2f2;overflow:auto; padding-bottom:25px;position:relative; z-index:1">
  <div class="container_12">
            <div class="grid_12">
                <div class="info-list">
                    <h1 class="h3 bg-blue">净值贷<span>(适用联富金融所有注册用户)</span></h1>

                    <div class="info-list-condition border-bt">
                        <h2 class="h4 mb15">申请条件</h2>
                        <ul class="ui-list-disc">
                            <li><i></i>22-55周岁的中国公民</li>
                            <li><i></i>净资产90%为1000元及以上</li>
                        </ul>
                        <div class="review-button">
                            <a onclick="judge_borrow_qualification();"  class="ui-button ui-button-blue ui-button-mid">申请借款</a>
                        </div>
                    </div>
                    <div class="info-list-material">
                        <h2 class="h4 mb15">必要申请资料</h2>
                        <ul class="ui-list-disc">
                            <li>
                                <h5>身份认证<em>（请同时提供下面两项资料）</em></h5>
                                <div class="idcard-div">
                                  <span data="1" class="card-icon icon-id">
                                  	<a href="<%= __PUBLIC__ %>/images/shengfenzheng.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic1.png" /></a>
                                  	</span><em style="left:75px;top:125px;">身份证<br>本人身份证原件的正、反两面照片</em>
                                  <span data="2" class="card-icon icon-id2">
                                  	<a href="<%= __PUBLIC__ %>/images/id_example_girl.jpg" rel="clearbox[test1]"><img src="<%=__PUBLIC__%>/images/jiekuan_pic2.png" /></a>
                                  </span><em style="left:387px;top:125px;">本人手持身份证正面头部照</em>
                                </div>
                            </li>
                        </ul>
                    </div>
                    </div>


                    <div class="info-list-way borrow-way-div" style="">
                        <h2 class="h4 mb15">借款方式</h2>
                        <ul>
                            <li><span>借&nbsp;款&nbsp;额&nbsp;度&nbsp;</span>1,000到50万</li>
                            <li><span>借款年利率</span>10%-24% <a class="calc" title="年利率计算器" target="_blank" href="/borrow/calculator.action?prodType=RRGXD"></a></li>
                            <li><span>借&nbsp;款&nbsp;期&nbsp;限</span>5天、 10天、 15天、 20天 、25天、 1个月到12个月</li>
                            <li><span>审&nbsp;核&nbsp;时&nbsp;间</span>24小时内</li>
                            <li><span>还&nbsp;款&nbsp;方&nbsp;式</span>一个月以内一次性还款还息；2个月以上每月还款，等额本息。</li>
                        </ul>
                    </div>
                    <div class="info-list-fee borrow-way-div borrow-info-div">
                        <h2 class="h4 mb15">费用说明</h2>
                        <ul>
                            <li class="range">
                               <h4>期初服务费</h4>
                                <h5><span>服务费率</span>由联富金融平台收取，借款本金1%。</h5>
                            </li>
                            <li class="info">
                                <h4>每月还款额</h4>
                                <ul>
                                    <li><span>本金及利息</span>支付给理财人</li>
                                    <li><span>借款管理费</span>由联富金融平台收取，借款本金的<em>0.3%</em></li>

                                </ul>
                            </li>
                        </ul>
                        <div class="review-button btn-div">
                           
                        </div>
                    </div>
                </div>
            </div>
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
	
	
  </body>
  <script type="text/javascript">
		//进行贷款申请前的判断-这个人是否能够借贷
		function judge_borrow_qualification(){	
			$.post('<%=__ROOT_PATH__%>/user/financial/borrow/judge_borrow_qualification.html?borrow_type=3',   //1工薪贷2生意贷3净值贷
					{

					},
					function(data)
					{
					 if(data=='1_1'){ /**基本用户信息验证*/
							 showMessage(["提示","请先注册或者登录"]);
					 }else if(data=='1_2'){
							showMessage(["提示","请先进行邮箱认证"]);
					 }else if(data=='1_3'){
							showMessage(["提示","该用户被禁用,请先联系管理员"]);
					 }else if(data=='1_4'){ //修改用户借款认证信息
							showMessage(["提示","该用户还没有实名认证,请先实名认证"]);
					 }else if(data=='1_5'){ /**借贷申请信息验证*/
							showMessage(["提示","该用户还没有绑定手机,请先绑定手机"]);
					 }else if(data=='1_6'){
							showMessage(["提示","非法用户类型:既不是理财也不是借贷"]);
					 }else if(data=='1_7'){
							showMessage(["提示","理财用户不能进行借贷"]);
							//##
							//##
					 }else if(data=='2_1'){ /**借贷种类*/
							showMessage(["提示","非法借贷种类"]);
					 }else if(data=='2_2'){
							showMessage(["提示","您有一笔贷款正在申请或者还款,暂时不能申请其他贷款"]);
					 }else if(data=='2_3'){
							showMessage(["提示","您有一笔贷款正在申请或者还款,暂时不能申请其他贷款"]);
					 }else if(data=='2_4'){
							showMessage(["提示","您有一笔贷款正在申请或者还款,暂时不能申请其他贷款"]);
					 }else if(data=='2_5'){
					 		showMessage(["提示","提示在没有借贷的类型的情况下面出现了借贷记录未还的情况,请联系管理员"]);
					 }else if(data=='2_6'){//xx=0 and yy=0
						 window.location.href="<%=__ROOT_PATH__%>/user/financial/borrow/loan_to_write_borrow_apply.html?borrow_type=3";
						 //##
						 //##
					 }else if(data=='3_0'){/**凑集状态*/
						    // 借款单中没有记录
						 showMessage(["提示","系统查找不到申请单或者凑集单的记录"]);
						}else if(data=='3_1'){/**凑集状态*/
							//单子创建完成，跳到填写借款认证信息页面-->填写和修改用户认证信息
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/persional_info/persional_index.html";  //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_2'){//apply_state= 2 3 4 都是那个审核页面
						    // 填写认证信息完成-但是由于非必填的资料没有上传-那么可以在...天内仍然可以修改-->填写和修改用户认证信息 234
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/submit_application/auditing_index.html";  //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_3'){
							//跳到审核页面...只能是审核中的-审核失败的不行
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/submit_application/auditing_index.html";  //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_4'){
							//被审核员驳回,填写和修改用户认证信息
							window.location.href="<%=__ROOT_PATH__%>/user/authentication/submit_application/auditing_index.html"; //需要传递   Applying_Order_Id_In_Session
						}else if(data=='3_5'){
							// 审核不通过,可以继续申请借款-但程序是执行不到这
							showMessage(["提示","审核不通过,可以继续申请借款-但程序是执行不到这"]);
						}else if(data=='3_e'){
							showMessage(["提示","bug:非法状态"]);
							//##
							//##
						}else if(data=='4_0'){//凑集单状态
							showMessage(["提示","提示系统出现了问题-就是根据审核成功申请单去凑集单查不到记录-需要您联系管理员"]);
						}else if(data=='4_1'){
							window.location.href="<%=__ROOT_PATH__%>/user/financial/borrow/to_gather_money_page.html?borrow_type=3";
						}else if(data=='4_2'){
							//筹集失败，可以继续申请借款-基本上不会执行到这里-如果执行到这里那么程序就是错的-必须给下次的凑集的机会
						}else if(data=='4_3'){
							//该还回值不存在
						}else if(data=='4_e'){
							showMessage(["提示","bug:非法状态"]);
							//##
							//##
							//还款状态
						} else if (data == '5_1'){
							//该值不存在
						}else if (data == '5_2'){
							//跳转至凑集成功页面
							//showMessage(["提示","跳转至凑集成功页面...add page"]);
                            window.location.href="<%=__ROOT_PATH__%>/user/financial/borrow/to_gather_success_page.html?borrow_type=3";
						}else if (data == '5_3'){
							// 还款完成，可以借款-基本上不会执行到这里-如果执行到这里那么程序就是错的
						}else {
							showMessage(["提示","bug:just else is here..."]);
						}
						
					}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
		}
	</script>
  
</html>
