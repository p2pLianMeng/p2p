<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
	
	<script src="js/loginDialog.js" type="text/javascript"></script>
    <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />

  </head>
  
  <body>
  
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>



<div class="sanbiao">
  <h1><a href="#">我要理财</a> > 散标投资列表</h1>    
  <div class="sanbiao_head">
  	<a class="hover" href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_elite.html">精英散标</a>
  	<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_lfoll_invest.html">联富宝</a>
  	 <a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_creditor_right.html">债权转让</a>
  </div>  
  <div class="sanbiao_list">
	  <div class="sanbiao_con">
	     <div class="left">精英散标主要针对有稳定经济收入及良好信用意识的人群。联富金融平台依托多年来积累的信用审核分析技术，针对借款人的资金使用情况及还款能力进行多方面审核，降低了出借人的风险。同时，联富金融还提供了本息保障服务，更好地保障了出借人的收益。</div>
	     <div class="right">
	     	 <a href="<%=__ROOT_PATH__ %>/user/usercenter/automatically_bid/to_automatically_bid.html"><img src="<%=__PUBLIC__%>/images/sanbiao.jpg" /></a>
	     	<a href="<%=__ROOT_PATH__ %>/index/newguidance/invest.html"><img src="<%=__PUBLIC__%>/images/sanbiao_2.jpg" /></a>
	     </div>
	  </div>
	   
	  <h2>
	    <p>精英标列表</p>
	    <a href="<%=__ROOT_PATH__%>/user/financial/financial/to_calculator_lender.html">理财计算器</a>
	    <%-- 
	    <span>累计成交总金额<br />44.53亿元</span>
	    <span>累计成交总笔数<br />81670笔</span>
	    <span>为用户累计赚取<br />29090.86万元</span>
	    --%>
	  </h2>
	  
	 <div class="sanbiao_con2">
	     <span>期限：</span>
	     <select id="select_1" class="select_1">
	        <option value="0">全部</option>
			<option value="3">3个月</option>
			<option value="6">6个月</option>
			<option value="9">9个月</option>
	        <option value="12">12个月</option>
	        <option value="18">18个月</option>
	        <option value="24">24个月</option>
	        <option value="36">36个月</option>
	     </select>
	     <span>年化利率：</span>
	     <select id="select_2" class="select_1">
	        <option value="0">全部</option>
	        <option value="10">10%</option>
	        <option value="11">11%</option>
	        <option value="12">12%</option>
	        <option value="13">13%</option>
			<option value="14">14%</option>
			<option value="15">15%</option>
			<option value="16">16%</option>
			<option value="17">17%</option>
			<option value="18">18%</option>
			<option value="19">19%</option>
			<option value="20">20%</option>
			<option value="21">21%</option>
			<option value="22">22%</option>
			<option value="23">23%</option>
			<option value="24">24%</option>
	     </select>
	     <span>金额：</span>
	     <select id="select_3" class="select_1">
	        <option value="0">全部</option>
	        <option value="1">1-5万</option>
	        <option value="2">5-10万</option>
	        <option value="3">10-20万</option>
	        <option value="4">20-50万</option>
	     </select>
	     <span>投标完成度：</span>
	     <select id="select_4" class="select_1">
	        <option value="0">全部</option>
	        <option value="1">低于50%</option>
	        <option value="2">50%-79%</option>
	        <option value="3">80%-99%</option>
	        <option value="4">100%满标</option>
	     </select>
	     <a href="javascript:void(0);" onclick="asychronized_query(1);">筛选</a>
	  </div>
  
		  <h3>
			  <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="37%" height="45" class="con2_kk">借款标题</td>
				    <td width="11%" height="45" class="con2_kk">信用等级</td>
				    <td width="10%" height="45" class="con2_kk">年利率</td>
				    <td width="12%" height="45" class="con2_kk">金额</td>
				    <td width="10%" height="45" class="con2_kk">期限</td>
				    <td width="9%" height="45" class="con2_kk">进度</td>
				    <td width="11%" height="45" class="con2_kk">&nbsp;</td>
				  </tr>
				  <tr>
				    <td height="55" class="con2_kk con2_kk_1"><img src="images/icon2.jpg" /><a href="sanbiao_xx.html"> 新手体验-模拟精英标24月期年利11%</a></td>
				    <td height="55" class="con2_kk"><span class="con2_yuan" style="background:#f9a31a;">A</span></td>
				    <td height="55" class="con2_kk">13.00%</td>
				    <td height="55" class="con2_kk">18,000元</td>
				    <td height="55" class="con2_kk">24个月</td>
				    <td height="55" class="con2_kk"><span class="hongse">23.87%</span></td>
				    <td height="55" align="center" class="con2_kk"><p class="con2_anniu" style="background:#f9a31a;">未满标</p></td>
				  </tr>
				  
			</table>
		</h3>
		<h6 id="asyncPageDiv" >
			 ${asyncPageDiv}
		</h6>
</div>
 
<div style="margin-left:235px;"><script type="text/javascript">
     document.write('<a style="display:none!important" id="tanx-a-mm_15962270_8698819_29284887"></a>');
     tanx_s = document.createElement("script");
     tanx_s.type = "text/javascript";
     tanx_s.charset = "gbk";
     tanx_s.id = "tanx-s-mm_15962270_8698819_29284887";
     tanx_s.async = true;
     tanx_s.src = "http://p.tanx.com/ex?i=mm_15962270_8698819_29284887";
     tanx_h = document.getElementsByTagName("head")[0];
     if(tanx_h)tanx_h.insertBefore(tanx_s,tanx_h.firstChild);
</script></div>
</div>

	
	<input type="hidden" id="newer_gather_id" value="${newer_gather_id}"/>

<%@ include file="/jsp/index/index_foot.jsp" %>
	<script type="text/javascript"> (function(win,doc){ var s = doc.createElement("script"), h = doc.getElementsByTagName("head")[0]; if (!win.alimamatk_show) { s.charset = "gbk"; s.async = true; s.src = "http://a.alimama.cn/tkapi.js"; h.insertBefore(s, h.firstChild); }; var o = { pid: "mm_15962270_8698819_29272871",/*推广单元ID，用于区分不同的推广渠道*/ appkey: "",/*通过TOP平台申请的appkey，设置后引导成交会关联appkey*/ unid: ""/*自定义统计字段*/ }; win.alimamatk_onload = win.alimamatk_onload || []; win.alimamatk_onload.push(o); })(window,document);</script>
  </body>
   	<script type="text/javascript">

		$(function(){
			asychronized_query(1);
		});
	
		function asychronized_query(pn){
			var url = "<%=__ROOT_PATH__%>/user/financial/financial/loan_list_query2.html";
			var borrow_duration = $("#select_1").val();
			var annulized_rate_int = $("#select_2").val();
			var borrow_all_money = $("#select_3").val();
			var gather_progress = $("#select_4").val();
			var newer_gather_id = $("#newer_gather_id").val();

			var xinshoubiao_str = '';

			if(newer_gather_id != 0){
				xinshoubiao_str = 
					'<tr>'
					    + '<td height="55" class="con2_kk con2_kk_1"><a href="<%=__ROOT_PATH__%>/user/financial/financial/to_new_bid.html?newer_gather_id='+ newer_gather_id +'"><img src="<%=__PUBLIC__ %>/images/icon2.jpg" /> 新手体验-模拟精英散标12月期 年利16%</a></td>'
					    + '<td height="55" class="con2_kk"><span class="con2_yuan" style="background:#f9a31a;">A</span></td>'
					    + '<td height="55" class="con2_kk">16%</td>'
					    + '<td height="55" class="con2_kk">500000元</td>'
					    + '<td height="55" class="con2_kk">1天</td>'
					    + '<td height="55" class="con2_kk"><span class="hongse">'+ ${gather_progress} +'%</span></td>'
					    + '<td height="55" align="center" class="con2_kk"><a href="<%=__ROOT_PATH__%>/user/financial/financial/to_new_bid.html?newer_gather_id='+ newer_gather_id +'"><p class="con2_anniu" style="background:#f9a31a;">投标</p></a></td>'
					+ '</tr>';				
			}
			
			$.ajax({
				url:url,
				type:'post',
				data:{
					"borrow_duration":borrow_duration,
					"annulized_rate_int":annulized_rate_int,
					"borrow_all_money":borrow_all_money,
					"gather_progress":gather_progress,
					"pn" : pn
					
					},
				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table1 tr:not(:first)").empty(); 
					var tr_str ='';
					for(var i=0;i<jsObject.bid_list.length;i++ ){

						var gather_str = 		'<td height="55" align="center" class="con2_kk">'
											+		'<a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.bid_list[i].id+'">'
											+			'<p class="con2_anniu" style="background:#e85e48;">'
											+				'投标'
											+			'</p>'
											+		'</a>'
											+	'</td>';

						var full_str = 			'<td height="55" align="center" class="con2_kk"><p class="con2_anniu">已满标</p></td>';
						var out_of_date_str =	'<td height="55" align="center" class="con2_kk"><p class="con2_anniu">流标</p></td>';
						var repaying_str = 		'<td height="55" align="center" class="con2_kk"><p class="con2_anniu">还款中</p></td>';
						var finish_repay_str = 	'<td height="55" align="center" class="con2_kk"><p class="con2_anniu">还款完成</p></td>';

						var str ;
						
						if(jsObject.bid_list[i].gather_state==1){
							str = gather_str;
						}else if(jsObject.bid_list[i].gather_state==2){//流标
							str = out_of_date_str;
						}else if(jsObject.bid_list[i].gather_state==3){//筹款成功
							str = full_str;
							if(jsObject.bid_list[i].payment_state == 2){ //还款中
								str = repaying_str;
							}else if(jsObject.bid_list[i].payment_state == 3){//还款完毕
								str = finish_repay_str;
							}
						}

						var rate_style = '';
						
						/*
						<td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span style="background:#ffd700;">A+</span></td>
	                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span style="background:#fded94;">A</span></td>
	                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span style="background:#7ae805;">B+</span></td>
	                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span style="background:#d0fba2;">B</span></td>
	                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span style="background:#908442;">C+</span></td>
	                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span style="background:#2a2715;">C</span></td>
						*/

						/*
						A+：#e62129；A：#ff5a08；B+：#ffcc30；B：#7ad84c；C+：#2ec6f5；C：#2a62f7
						*/
						if(jsObject.bid_list[i].credit_rating == 'A+'){
							rate_style = 'style="background:#e62129;"';
						}else if(jsObject.bid_list[i].credit_rating == 'A'){
							rate_style = 'style="background:#ff5a08;"';
						}else if(jsObject.bid_list[i].credit_rating == 'B+'){
							rate_style = 'style="background:#ffcc30;"';
						}else if(jsObject.bid_list[i].credit_rating == 'B'){
							rate_style = 'style="background:#7ad84c;"';
						}else if(jsObject.bid_list[i].credit_rating == 'C+'){
							rate_style = 'style="background:#2ec6f5;"';
						}else if(jsObject.bid_list[i].credit_rating == 'C'){
							rate_style = 'style="background:#2a62f7;"';
						}

						var duration_str = '';
						if(jsObject.bid_list[i].borrow_duration != 0){
							duration_str = jsObject.bid_list[i].borrow_duration + '个月';
						}else{
							duration_str = jsObject.bid_list[i].borrow_duration_day + '天';
						}

						var icon_str = '';
						if(jsObject.bid_list[i].borrow_type==1){
							icon_str = 'icon1.jpg';
						}else if(jsObject.bid_list[i].borrow_type==2){
							icon_str = 'icon1_2.jpg';
						}else if(jsObject.bid_list[i].borrow_type==3){
							icon_str = 'icon1_3.jpg';
						}
						
						tr_str += '<tr>'
						+ '<td height="55" class="con2_kk"><img src="<%=__PUBLIC__ %>/images/'+ icon_str +'" /> <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.bid_list[i].id+'">'+jsObject.bid_list[i].borrow_title+'</a></td>'
						+ '<td height="55" class="con2_kk"><span class="con2_yuan" '+ rate_style +'>'+ jsObject.bid_list[i].credit_rating +'</span></td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].annulized_rate_int+'%</td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].borrow_all_money+'元</td>'
						+ '<td height="55" class="con2_kk">'+duration_str+'</td>'
						+ '<td height="55" class="con2_kk"><span class="hongse">'+jsObject.bid_list[i].gather_progress+'%</span></td>'
						+ str
						+ '</tr>';
					}
					$("#table1 tr:eq(0)").after(xinshoubiao_str+tr_str);
					$("#asyncPageDiv").html(jsObject.asyncPageDiv);
				}
			},"json");	
		}

		function change_page_of_async(pn){
			asychronized_query(pn);
		}
		
	</script>
</html>