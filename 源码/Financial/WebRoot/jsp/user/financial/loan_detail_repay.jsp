<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>

  </head>
  
  <body>
  
  <%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<input id="gather_order_id" type="hidden" value="${gather_order.r.id}" />
<div class="sanbiao">
  <h1><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html">我要理财</a> > <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html">散标投资列表</a> > 借款详情</h1>
  <div class="sanbiao_bt" style="height:290px">    
  <div class="up"><span>
  
  		<c:if test="${gather_order.r.borrow_type==1}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1.jpg" /> 
    	</c:if>
    	<c:if test="${gather_order.r.borrow_type==2}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1_2.jpg" /> 
    	</c:if>
    	<c:if test="${gather_order.r.borrow_type==3}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1_3.jpg" /> 
    	</c:if>
  
  ${gather_order.r.borrow_title }</span><a class="a" href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/to_loan_contract.html?gather_order_id=${gather_order.r.id}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">借款协议</a></div>
  
   <h2><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="88" colspan="2" style="font-size:14px;">标的总额 （元）<br /><span class="sanbiao_xx_text">￥${gather_order.r.borrow_all_money }</span></td>
    <td height="88" colspan="2">年利率<br /><span class="sanbiao_xx_text">${gather_order.r.annulized_rate_int }%</span></td>
    <c:if test="${gather_order.r.borrow_duration!=0}">
	    <td width="17%" height="88">还款期限 （月）<br /><span class="sanbiao_xx_text">${gather_order.r.borrow_duration }</span></td>
    </c:if>
    <c:if test="${gather_order.r.borrow_duration==0}">
	    <td width="17%" height="88">还款期限 （天）<br /><span class="sanbiao_xx_text">${gather_order.r.borrow_duration_day }</span></td>
    </c:if>
  </tr>
  <tr>
    <td width="16%" height="35" style="font-size:14px;">保障方式</td>
    <td width="32%" height="35" align="left" style="font-size:14px;" class="td_bj">本金+利息&nbsp;<a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html"><img src="<%=__PUBLIC__ %>/images/loan_3.png" /><span>详情参见<s>本金保障计划</s><img src="<%=__PUBLIC__ %>/images/loan_4.png" /></span></a></td>
    <td width="21%" height="35" align="left" style="font-size:14px;">提前还款费率</td>
    <td width="14%" height="35" align="left" style="font-size:14px;">1.00%</td>
    <td rowspan="2"></td>
  </tr>
  <tr>
    <td height="35" style="font-size:14px;">还款方式</td>
    
   <c:if test="${gather_order.r.borrow_duration>1}">
	    <td height="35" align="left" style="font-size:14px;" class="td_bj td_bx">按月还款/等额本息&nbsp;<a href="#"><img src="<%=__PUBLIC__ %>/images/loan_3.png" /><span>等额本息还款法是在还款期内，每月偿还同等数额的贷款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<img src="<%=__PUBLIC__ %>/images/loan_4.png" /></span></a></td>
    </c:if>
    <c:if test="${gather_order.r.borrow_duration==0 or gather_order.r.borrow_duration==1}">
	    <td height="35" align="left" style="font-size:14px;" class="td_bj td_bx">到期一次性还本付息&nbsp;</td>
     </c:if>
    
    <td height="35" align="left" style="font-size:14px;">月还本息（元）</td>
    <td height="35" align="left" style="font-size:14px;">${monthly_principal_and_interest }</td>
  </tr>

</table>
</h2>

    <h5 class="h5_2">
       <p style="border-top:none"><s>待还本息（元）</s><span>￥${benxi_to_repay}</span></p>
       <p><s>剩余期数（月）</s><span>${remain_period}</span></p>
       <p style="border-bottom:none"><s>下一合约还款日</s><span>${automatic_repayment_date }</span></p>
    </h5>    
  </div>
 <div class="sanbiao_xx_con">
    <h2>
	    <p class="hover">标的详情</p>
	    <p onclick="get_bid_record();">投标记录</p>
	    <p onclick="get_repayment_behavior();">还款表现</p>
	    <p onclick="get_debt_info();" >债权信息</p>
	    <p onclick="get_transfer_record();">转让记录</p>
    </h2>
    <h3 style="display: block">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">用户信息</td>
    <td width="13%" height="45"><span style="float:left"><a href="#" style=" color:#09F">${gather_order.r.nickname }</a></span></td>
    <td height="45" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">用户名</td>
    <td width="17%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.nickname}</td>
    <td width="12%" height="30" class="kk_text">公司行业</td>
    <td width="17%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_trades}</td>
    <td width="13%" height="30" class="kk_text">收入范围</td>
    <td width="14%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.monthly_income}/月</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">年    龄</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.age}</td>
    <td height="30" class="kk_text">公司规模</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_size}</td>
    <td height="30" class="kk_text">房       产</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.housing}</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">学    历</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.highest_education}</td>
    <td height="30" class="kk_text">岗位职位</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.position}</td>
    <td height="30" class="kk_text">房       贷</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.housing_mortgage}</td>
  </tr>
  <tr>
  <td height="30">&nbsp;</td>
	<td height="30" class="kk_text">学         校</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.school}</td>
    <td height="30" class="kk_text">工作城市</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_city}</td>
    <td height="30" class="kk_text">车       产</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.car}</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">婚         姻</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.marriage}</td>
    <td height="30" class="kk_text">工作时间</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.year_limit}</td>
    <td height="30" class="kk_text">车       贷</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.car_mortgage}</td>
  </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="30" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="30" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">信用档案</td>
    <td width="13%" height="30"><span id="rating_color" >${borrower_bulk_standard_order_user_info.m.credit_rating}</span></td>
    <td height="30" colspan="5">&nbsp;</td>
    </tr>
   <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">申请借款（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.loan_application }</td>
    <td height="30" class="kk_text">信用额度（元）</td>
    <td id="line_of_credit" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.line_of_credit }</td>
    <td height="30" class="kk_text">逾期金额（元）</td>
    <td id="overdue_amount" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.overdue_amount }</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">成功借款（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.successful_loan }</td>
    <td height="30" class="kk_text">借款总额（元）</td>
    <td id="total_loan" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.total_loan }</td>
    <td height="30" class="kk_text">逾期次数（次）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.overdue_num }</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">还清笔数（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.pay_off_the_pen_number }</td>
    <td height="30" class="kk_text">待还本息（元）</td>
    <td id="repay" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.repay }</td>
    <td height="30" class="kk_text">严重逾期（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.seriously_overdue }</td>
  </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="25" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">审核状态</td>
    <td width="13%" height="45"></td>
    <td height="45" colspan="5">&nbsp;</td>
   </tr>
  <tr>
    <td height="40" colspan="3" align="center" class="sanbiao_xx_kk3"><span class="kk_text">审核项目</span></td>
    <td height="40" colspan="4" align="center" class="sanbiao_xx_kk3">状态</td>
    </tr>
   <tr id="0_tr">
    <td id="_0" height="45" colspan="3" align="center" ><span class="kk_text">身份认证</span></td>
    <td	id="0" height="45" colspan="4" align="center"  ></td>
    </tr>
  <tr id="1_tr">
    <td id="_1" height="45" colspan="3" align="center" ><span class="kk_text">工作认证</span></td>
    <td id="1" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr id="2_tr">
    <td id="_2" height="45" colspan="3" align="center" ><span class="kk_text">信用报告</span></td>
    <td id="2" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr id="3_tr">
    <td id="_3" height="45" colspan="3" align="center" ><span class="kk_text">收入认证</span></td>
    <td id="3" height="45" colspan="4" align="center" ></td>
   </tr>
     <tr id="4_tr">
    <td id="_4" height="45" colspan="3" align="center" ><span class="kk_text">房产认证</span></td>
    <td id="4" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="5_tr">
    <td id="_5" height="45" colspan="3" align="center" ><span class="kk_text">车产认证</span></td>
    <td id="5" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="6_tr">
    <td id="_6" height="45" colspan="3" align="center" ><span class="kk_text">婚姻认证</span></td>
    <td id="6" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="7_tr">
    <td id="_7" height="45" colspan="3" align="center" ><span class="kk_text">学历认证</span></td>
    <td id="7" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="8_tr">
    <td id="_8" height="45" colspan="3" align="center" ><span class="kk_text">职称认证</span></td>
    <td id="8" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="9_tr">
    <td id="_9" height="45" colspan="3" align="center" ><span class="kk_text">居住认证</span></td>
    <td id="9" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    <td height="45" colspan="6" class="kk_text">&bull;&nbsp;&nbsp;联富金融及其合作机构将始终秉持客观公正的原则，严控风险，最大程度的尽力确保借入者信息的真实性，但不保证审核信息100%无误。<br/>
                                              &bull;&nbsp;&nbsp;借入者若长期逾期，其个人信息将被公布。</td>
    </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="25" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">借款描述</td>
    <td width="13%" height="45"></td>
    <td height="45" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    <td colspan="6" rowspan="2" class="kk_text" style="line-height:30px">${gather_order.r.describe}</td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    </tr>  
  
</table>
</h3>
     <h3 class="h3_2" style="display: none;">
     	
     	<div align="right">
     		<span class="td_1" id="person_time">加入人次0人</span>&nbsp;&nbsp;&nbsp;<span class="td_1" id="sum_invest_money">投标总额0元</span>
     	</div>
        <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">

           <tr class="tr_1">
              <td>序号</td>
              <td>投标人</td>
              <td>投标金额</td>
              <td>投标时间</td>
           </tr>
           
           <%--
           <tr class="tr_2">
              <td>1</td>
              <td class="td_2">Jedzhou（U-A）</td>
              <td>3000.00元</td>
              <td>2014-11-07 13:33</td>
           </tr>
           --%>
           
        </table>
     </h3>
     
     
     <!-- 还款表现 -->
     <h3 class="h3_2">
     	<div align="right">
     		<span class="td_1" id="yihuan_beixi">已还本息0.00元</span>&nbsp;&nbsp;&nbsp;<span class="td_1" id="weihuan_benxi">待还本息24,777.60元</span>
     	</div>
        <table id="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="tr_1">
              <td>合约还款日期</td>
              <td>状态</td>
              <td>应还本息</td>
              <td>应付罚息</td>
              <td>实际还款日期</td>
           </tr>
           
           <tr class="tr_2">
              <td>2014-12-07</td>
              <td>未偿还</td>
              <td>4128.00元</td>
              <td>0.00元</td>
              <td>2014-11-07 13:33</td>
           </tr>
           

        </table>
     </h3>
     
     <!-- 债权信息 -->
     <h3 class="h3_2">
     	<div align="right">
           <span class="td_1" id="hold_count">持有债权人数0人</span>
        </div>
        <table id="table3" width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="tr_1">
              <td>序号</td>
              <td>债权人</td>
              <td>待收本息</td>
              <td>持有份数</td>
           </tr>
           <tr class="tr_2">
              <td>1</td>
              <td class="td_2">呵呵君</td>
              <td>500.00元</td>
              <td>10份</td>
           </tr>

        </table>
     </h3>
     
     <!-- 转让记录 -->
     <h3 class="h3_2">
     	<div align="right">
           <span class="td_1" id="trade_total_money">已交易总额0.00元</span>
         </div>
        <table id="table4" width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="tr_1">
              <td>债权买入者</td>
              <td>债权卖出者</td>
              <td>交易金额</td>
              <td>交易时间</td>
           </tr>
           <tr class="tr_2">
              <td class="td_2">石玉龙</td>
              <td class="td_2">呵呵君</td>
              <td>500.00元</td>
              <td>2014-11-07 13:33</td>
           </tr>
           <tr class="tr_3">
              <td class="td_2">呵呵君</td>
              <td class="td_2">石玉龙</td>
              <td>4128.00元</td>
              <td>2014-11-07 13:33</td>
           </tr>
        </table>
     </h3>
     
  </div>
  
</div>




<%@ include file="/jsp/index/index_foot.jsp" %>
	<script src="<%=__PUBLIC__ %>/js/the_function.js"></script>
	<script src="<%=__PUBLIC__ %>/js/debt_detail_info.js"></script>
	<script type="text/javascript">
var num = 0;
	$(function(){
		get_fund_info(${borrower_bulk_standard_order_user_info.m.line_of_credit },${borrower_bulk_standard_order_user_info.m.overdue_amount },
		${borrower_bulk_standard_order_user_info.m.total_loan },${borrower_bulk_standard_order_user_info.m.repay });
		get_money_can_use();
		get_bid_record();
		change_color("${borrower_bulk_standard_order_user_info.m.credit_rating}","rating_color");
		var upload_info_status = [${borrower_bulk_standard_order_user_info.m.id_authenticate_status},${borrower_bulk_standard_order_user_info.m.work_authenticate_status},${borrower_bulk_standard_order_user_info.m.credit_authenticate_status},${borrower_bulk_standard_order_user_info.m.income_authenticate_status},${borrower_bulk_standard_order_user_info.m.housing_authenticate_status},${borrower_bulk_standard_order_user_info.m.car_authenticate_status},${borrower_bulk_standard_order_user_info.m.marriage_authenticate_status},${borrower_bulk_standard_order_user_info.m.education_authenticate_status},${borrower_bulk_standard_order_user_info.m.title_authenticate_status},${borrower_bulk_standard_order_user_info.m.living_authenticate_status}];
		for(var i = 0 ;i<upload_info_status.length;i++){
			if(upload_info_status[i]==1){
				$("#"+i).append("<img src='<%=__PUBLIC__%>/images/dui_cion.png' />");
				if(num % 2 != 0){
					$("#"+i).addClass("sanbiao_xx_kk1");
					$("#_"+i).addClass("sanbiao_xx_kk1");
				}else{
					$("#"+i).addClass("sanbiao_xx_kk2");
					$("#_"+i).addClass("sanbiao_xx_kk2");
				}
				num += 1;
			}else{
				$("#"+i+"_tr").hide();
			}
		}
	});

	function get_money_can_use(){
		$.get('<%=__ROOT_PATH__%>/user/financial/financial/get_money_can_use.html',
				{
			
				},
				function(data) //回传函数
				{
					$("#money_can_use").html("账户余额  "+data+"元");
				}, "json");//浏览器编码	
	}

	 function closeme_refresh(){
		document.getElementById('loginDiv1').style.display='none';
		document.getElementById('loginDiv2').style.display='none';
		window.location.reload();
		}	 
	</script>

</body>
</html>