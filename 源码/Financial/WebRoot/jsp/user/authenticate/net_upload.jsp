<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<html xmlns="http://www.w3.org/1999/xhtml">

	<body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
		<%@ include file="/jsp/index/index_top.jsp"%>

		<div id="ok" style="display: none;" class="fudong">
			上传成功！
		</div>
		<div id="no" style="display: none;" class="fudong">
			上传失败，格式错误！
		</div>
		<div id="er" style="display: none;" class="fudong">
			上传失败，请检查网络！
		</div>
		<div id="over" style="display: none;" class="fudong">
			上传失败，超过上传限制次数！
		</div>
		<div id="out" style="display: none;" class="fudong">
			上传失败，文件过大，请上传2M以下图片！
		</div>

		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="loan_aa loan_bb loan_bb_5">
					<div class="loan_aa_top">
						<div class="up">
							<a href="#">我要借款</a>&nbsp;
							<span>></span>
							<c:choose>
								<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
								<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
								<c:when test="${borrow_type==3}">&nbsp;净值贷&nbsp;</c:when>
							</c:choose>
							<span>></span>&nbsp;填写借款申请
						</div>
						<div class="center">
							<span style="background: #ed5050; color: #fff;">1</span>
							<s style="background: #ed5050;"></s>
							<span style="background: #ed5050; color: #fff;">2</span>
							<s></s>
							<span>3</span>
							<s></s>
							<span>4</span>
							<s></s>
							<span>5</span>
						</div>
						<div class="down">
							<span style="color: #ed5050;">填写借款申请</span>
							<span style="margin-left: 55px; color: #ed5050;">填写借款信息</span>
							<span style="margin-left: 60px;">审核</span>
							<span style="margin-left: 60px;">筹集借款</span>
							<span style="margin-left: 55px;">获得借款</span>
						</div>
					</div>
					<div class="loan_bb_con">
						<%@ include file="/jsp/user/authenticate/authenticate_left.jsp"%>
						<div class="right">
							<div class="right_top">
								上传资料
							</div>
							<div class="right_top2">
								必要上传资料
							</div>
							<div class="upload" >
								<div style="margin-left:345px " class="upload_con">
									<s >身份认证</s>
									<span id="0" class="span_1 a"></span>
									<a id="0zi" class="a_1" onclick="openme2('0div1','0div2');" >上传资料</a>
									<div id="0div1" class="loginDiv1"></div>
									<div id="0div2" class="loginDiv2"
										style="z-index: 10005; display: none;">
										<div class="area">
											<input type="button" class="guanbi"
												onClick="closeme2('0div1','0div2');" value="ｘ" />
											<div class="area_up">
												<span>身份认证</span>
												<br />
												您上传的身份证照片需和您绑定的身份证一致，否则将无法通过认证。
											</div>
											<p>
												认证说明:
												<br />
												（1）本人身份证原件的正、反两面照片。
												<a href="<%= __PUBLIC__ %>/images/shengfenzheng.jpg" target="_blank">查看示例</a>
												<br />
												（2）本人手持身份证正面头部照，（确保身份证上的信息没有被遮挡，避免证件与头部重叠）。
												<a href="<%= __PUBLIC__ %>/images/id_example_girl.jpg" target="_blank">查看示例</a>
												<br />
												认证有效期：
												<span>永久</span>
												<br />
												上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
												<br />
												<span>警告：</span>
												联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
											</p>
											<input class="button_1" type="button" onclick="doUpload(0);"
												value="上传资料" />
										</div>
									</div>
								</div>

							<input type="hidden" id="apply_order_id"
								value="${apply_order_id}" />
							<div class="upload_down" >
								<a onclick="ok()" href="#">提交申请</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="<%=__PUBLIC__%>/js/select.js"></script>
		<script type="text/javascript">
	function doUpload(type){
		var apply_order_id=$("#apply_order_id").val();
		$.upload({
			// 上传地址				
			url: '<%=__ROOT_PATH__%>/user/authentication/upload_info/upload_info.html', 
			// 文件域名字
			fileName: 'filedata', 
			// 其他表单数据
			params: {name:type,apply_order_id:apply_order_id},
			// 上传完成后, 返回json, text
			dataType: 'json',
			// 上传之前回调,return true表示可继续上传
			onSend: function() {
					return true;
			},
			// 上传之后回调
			onComplate: function(data) {
				if(data == no){
				 	$("#no").show();
					setTimeout('hide()',2000);
				}else if(data == er){
					$("#er").show();
					setTimeout('hide()',2000);
				}else if(data == over){
					$("#over").show();
					setTimeout('hide()',2000);
				}else if(data == out){
					$("#out").show();
					setTimeout('hide()',2000);
				}else if(data == ok){
					window.location.href="<%=__ROOT_PATH__%>/user/authentication/upload_info/upload_info_index.html";
				}else{
					closeme2(data+"div1",data+"div2");
					$("#ok").show();
					$("#"+data+"zi").html("补充上传");
					$("#"+data).append("<img src='<%=__PUBLIC__%>/images/loan_b_11.png' />");
					setTimeout('hide()',2000);
				}
			}
		});
	}

function hide() {
	$("#ok").hide();
	$("#no").hide();	
	$("#er").hide();
	$("#over").hide();
	$("#out").hide();
}

$(function(){
		if(${user_authenticate_upload_info.m.id_authenticate_url!=null} ){
			$("#0zi").html("补充上传");
			$("#0").append("<img src='<%=__PUBLIC__%>/images/loan_b_11.png' />"); 
		}
	if(${borrower_bulk_standard_apply_order.m.borrow_type == 1}){
		$("#wi1").show();
		$("#in1").show();
	}else{
		$("#wi2").show();
		$("#in2").show();
		}
})


</script>

		<%@ include file="/jsp/index/index_foot.jsp"%>



	</body>
</html>

