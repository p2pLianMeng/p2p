<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
			             <script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
			             <link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>
 <%--<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
--%><span id="ny_pic"></span>

<div class="about">
	<div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;我的债权
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jyjl geren_wdzq">
       <div class="left">
          <p>债权已赚金额&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>债权已赚金额 = 债权已赚利息罚息及违约金 + 债权转让盈亏 - 债权转让应计利息支出<br />（由于购买转让的债权时可能需要先支付应计利息，所以此数值可能为负）<img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a>
          </p><span><s>0.00</s>元</span>
          <p class="p_1">利息收益&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>利息收益 = 债权已赚利息罚息及违约金 - 债权转让应计利息支出<br />（由于购买转让的债权时可能需要先支付应计利息，所以此数值可能为负）<img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a>&nbsp;0.00元</p>
          <p class="p_1">债权转让盈亏&nbsp;&nbsp;0.00元</p>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td>债权账户资产</td>
                <td>回收中的债权数量</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td>${financial_account_asset.to_collect_pricipal}元</td>
                <td>${financial_account_asset.hold_count}个</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td colspan="2">没时间亲自投标？试试联富金融自动投标功能吧。</td>
                <td><a href="<%=__ROOT_PATH__ %>/user/usercenter/automatically_bid/to_automatically_bid.html">自动投标</a></td>
             </tr>
          </table>
       </div>    
    </div> 
    <div class="geren_wdzq_head">
    <span>
	    <a class="hover" href="javascipt:void(0);" onclick="get_repaying_debt();">回收中的债权</a>
	    <a href="javascript:void(0);" onclick="get_pay_off_debt();">已结清的债权</a>
	    <a href="javascript:void(0);" onclick="get_bidding_debt();">投标中的债权</a>
	   
    </span>
    <s>
 	   <a href="<%=__ROOT_PATH__%>/user/usercenter/financial_manage/to_recieve_money.html">回账查询</a>
    </s>
    </div>
    <div class="geren_wdzq_con" style="display:block">
       <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>原始投资金额</td>
             <td>年利率</td>
             <td>待收本息</td>
             <td>月收本息</td>
             <td>期数</td>
             <td>下个还款日</td>
             <td>状态</td>
          </tr>
         
         
		<div id="1" class="loginDiv1"></div>
		<div id="2" class="loginDiv2" style=" z-index:10005; display:none;">     
             <div id="confirm_transfer" class="geren_wdzq_div">  
                <input type="button" class="guanbi" onClick="closeme(1,2)" value="ｘ" />
                
                <input type="hidden" id="hidden_creditor_right_hold_id" value="" />
                <input type="hidden" id="hidden_gather_money_order_id" value="" />
                <input type="hidden" id="hidden_creditor_right_value" value="" />
                <input type="hidden" id="hidden_can_transfer_share" value="" />
                
                <div class="up"><span>债权转出</span>ID&nbsp;<span id="debt_id">001</span></div>
                <div class="up_con">
                   <label class="label_1">原始投资额</label>
                   <p class="P1"><span id="invest_money">0.00元</span>（<span id="invest_share">0份</span>）</p>
                </div>
                <div class="up_con">
                   <label class="label_1">已收利息</label>
                   <p class="P2"><span>0.00元</span><span>利率</span><span id="interest_rate">%</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">可转份数</label>
                   <p class="P2"><span id="can_transfer_share">0份</span><span>剩余期数</span><span id="remain_period">24个月</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让份数</label>
                   <input class="text_1" id="transfer_share" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" onblur="count_transfer_price();check_share();" />
                </div>
                <div class="up_con">
                   <label class="label_1">当前债权价值</label>
                   <p class="P2"><span id="debt_value">0.0元/份</span>
                   <span>当前待收本息</span>
                   <span id="daishou_benxi_per_share">0.00元/份</span></p>
                </div>
                <div class="up_con up_con_2">一般为转让时的待回收本金与应计利息之和。<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">转让系数</label>
                   <select id="transfer_factor" class="select_1" onchange="count_transfer_price();">
                      <option value="1">100%</option>
                      <option value="0.9">90%</option>
                      <option value="0.8">80%</option>
                      <option value="0.7">70%</option>
                      <option value="0.6">60%</option>
                      <option value="0.5">50%</option>
                      <option value="0.4">40%</option>
                      <option value="0.3">30%</option>
                      <option value="0.2">20%</option>
                      <option value="0.1">10%</option>
                   </select>
                </div>
                <div class="up_con up_con_2">此转让系数仅作用于债权待收本金部分</div>
                <div class="up_con">
                   <label class="label_1">转让价格</label>
                   <p class="P1" id="transfer_price_per_share">0.00</p>元/份
                </div>
                <div class="up_con">
                   <label class="label_1">转让总价格</label>
                   <p class="P1" id="transfer_price">0.00</p>元
                </div>
                <div class="up_con">
                   <label class="label_1">转让费用</label>
                   <p class="P1" id="transfer_fee">0.00</p>元
                </div>
                <div class="up_con">
                   <label class="label_1">预计收入金额</label>
                   <p class="P1" id="pre_recieve_money">0.00</p>元
                </div>
                <div class="up_con up_con_2">由于债权价值会发生变动，最终收入金额可能发生变动<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">提现密码</label>
                   <input class="text_1" id="money_password" type="password"/>
                   <a class="a_1">忘记密码？</a>
                </div>
                <div class="up_con up_con_3">
                   <input class="checkbox_1" type="checkbox" id="checkbox_1" />
                   <span>我已阅读并同意签署</span>
                   <a href="#">《债权转让及受让协议》</a>
                </div>
                <div class="up_con up_con_4"><a class="a" href="javascript:void(0);" onclick="confirm_debt_transfer();" >转让</a><a href="javascript:void(0);" onclick="closeme(1,2);">取消</a></div>
                
                <div id="tip" class="up_con up_con_5" style="display:none">
                   <img src="<%=__PUBLIC__%>/images/registration_pic8.png" />
                   <span id="error_msg">密码输入错误，您还剩余4次输入机会，请重新输入。</span>
                </div>                
             </div>
             <div id="success" style="display:none" class="geren_wdzq_div" >
			       	<div class="con_3" ><img src="<%=__PUBLIC__ %>/images/registration_pic7.png" />挂出转出单成功</div>
			 		<div class="con_4" ><a onClick="closeme_refresh(1,2)">关闭</a></div>
		        </div> 
          </div>
       </table>
    </div>   
    <div class="geren_wdzq_con">
       <table id="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>投资金额</td>
             <td>年利率</td>
             <td>回收金额</td>
             <td>已赚金额</td>
             <td>结清日期</td>
             <td>结清方式</td>
             <td>&nbsp;</td>
          </tr>
          
          <!-- 
          <tr class="tr_1">
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>200.00(1份)</td>
             <td>10.00%</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2014-11-24</td>
             <td>还款</td>
             <td></td>
          </tr>
           -->
          <tr>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>200.00(1份)</td>
             <td>10.00%</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2014-11-24</td>
             <td>还款</td>
          </tr>

          <tr>
             <td colspan="8" class="td_1">没有已结清的债权</td>
          </tr>
          
       </table>
    </div> 
    <div class="geren_wdzq_con">
        <table id="table3" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>原始投资金额</td>
             <td>年利率</td>
             <td>期限</td>
             <td>信用等级</td>
             <td>剩余时间</td>
             <td>投标进度</td>
          </tr>
          
       </table>
    </div>
    <div class="geren_wdzq_con">
       <table id="table4" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="14%">债权ID</td>
             <td width="10%">成交份数</td>
             <td width="18%">转出时债权总价值</td>
             <td width="17%">转出时总成交金额</td>
             <td width="12%">实际收入</td>
             <td width="11%">交易费用</td>
             <td width="10%">转让盈亏</td>
             <td width="8%">&nbsp;</td>
          </tr>
          
          
          
          <tr class="tr_1">
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>2份</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2.00</td>
             <td>+20.00</td>
             <td><a onClick="showsubmenu(6)">明细</a></td>
          </tr>
          <tr id="submenu6" class="tr_1" style="display:none;">
             <td colspan="8">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td colspan="9" style="font-size:16px">债权转让明细</td>
                  </tr>
                  <tr>
                     <td width="12%">购买人</td>
                     <td width="16%">转出时债权价值</td>
                     <td width="12%">转让价格</td>
                     <td width="10%">成交份数</td>
                     <td width="11%">交易费用</td>
                     <td width="10%">实际收入</td>
                     <td width="9%">盈亏</td>
                     <td width="14%">成交时间</td>
                     <td width="6%">&nbsp;</td>
                  </tr>
                  <tr>
                     <td><span>123456</span></td>
                     <td>32.16/份</td>
                     <td>32.16/份</td>
                     <td>2份</td>
                     <td>0.38</td>
                     <td>78.00</td>
                     <td>-0.38</td>
                     <td>2014-11-12</td>
                     <td><a>合同</a></td>
                  </tr>
               </table>
             </td>
          </tr>
          
          
          <tr>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>2份</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2.00</td>
             <td>+20.00</td>
             <td><a onClick="showsubmenu(7)">明细</a></td>
          </tr>
          <tr id="submenu7" style="display:none;">
             <td colspan="8">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td colspan="9" style="font-size:16px">债权转让明细</td>
                  </tr>
                  <tr>
                     <td width="12%">购买人</td>
                     <td width="16%">转出时债权价值</td>
                     <td width="12%">转让价格</td>
                     <td width="10%">成交份数</td>
                     <td width="11%">交易费用</td>
                     <td width="10%">实际收入</td>
                     <td width="9%">盈亏</td>
                     <td width="14%">成交时间</td>
                     <td width="6%">&nbsp;</td>
                  </tr>
                  <tr>
                     <td><span>123456</span></td>
                     <td>32.16/份</td>
                     <td>32.16/份</td>
                     <td>2份</td>
                     <td>0.38</td>
                     <td>78.00</td>
                     <td>-0.38</td>
                     <td>2014-11-12</td>
                     <td><a>合同</a></td>
                  </tr>
               </table>
             </td>
          </tr>
          
         
          
          <tr>
             <td colspan="8" class="td_1">没有已转出的债权</td>
          </tr>
          
          
       </table>
    </div>
  </div>    
</div>



<div class="bottom"></div>

	<script type="text/javascript">
		$(function(){
			get_repaying_debt();
		});

		//回收中的债权
		function get_repaying_debt(){

			var url = "<%=__ROOT_PATH__%>/user/usercenter/financial_manage/get_repaying_debt.html";
			$.ajax({
				url:url,
				type:'post',

				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table1 tr:not(:first)").empty(); 

					//散标集合字符串
					var tr_str ='';
					//回收中新手标字符串
					var newer_str = '';

					var icon_str = '';

					var div_str = ''
			          	
							;

					if(jsObject.repaying_newer_bid.id != null){
						newer_str = 
							'<tr class="tr_1">'
				             + '<td width="18%"><a href="javascript:void(0);"><img src="<%=__PUBLIC__%>/images/icon2.jpg" /><span>'+  jsObject.repaying_newer_bid.newer_bid_gather_id +'</span></a></td>'
				             + '<td width="15%">20000元(400份)</td>'
				             + '<td width="11%">16%</td>'
				             + '<td width="11%">8.80</td>'
				             + '<td width="11%"></td>'
				             + '<td width="8%">1/1</td>'
				             + '<td width="13%">'+ jsObject.repaying_newer_bid.recieve_date +'</td>'
				             + '<td width="9%">还款中</td>'
				             + '<td width="4%"></td>'
				          + '</tr>';

					}
					
					
					if(jsObject.repaying_debt_list.length == 0 && jsObject.repaying_newer_bid.id == null){
						tr_str = '<tr><td colspan="8" class="td_1">没有回收中的债权</td></tr>';
					}else{
						for(var i=0;i<jsObject.repaying_debt_list.length;i++ ){

							if(jsObject.repaying_debt_list[i].borrow_type==1){
								icon_str = 'icon1.jpg';
							}else if(jsObject.repaying_debt_list[i].borrow_type==2){
								icon_str = 'icon1_2.jpg';
							}else if(jsObject.repaying_debt_list[i].borrow_type==3){
								icon_str = 'icon1_3.jpg';
							}

							var can_transfer_share = (Number(jsObject.repaying_debt_list[i].hold_share) - Number(jsObject.repaying_debt_list[i].transfer_share));
							//alert(jsObject.repaying_debt_list[i].hold_money);
							//alert(jsObject.repaying_debt_list[i].transfer_share);
							
							var stripe_str = '';
							if(i%2==0){
								stripe_str = 'tr_1';
							}

							var transfer_str = '';
							if(jsObject.repaying_debt_list[i].can_transfer){
								transfer_str =
								 '<a onClick="open_debt_transfer(1,2,'
						             + jsObject.repaying_debt_list[i].gather_money_order_id +','
						             + jsObject.repaying_debt_list[i].origin_money +','
						             + jsObject.repaying_debt_list[i].hold_share +','
						             + can_transfer_share +','
						             + jsObject.repaying_debt_list[i].annulized_rate_int +','
						             + jsObject.repaying_debt_list[i].daishou_benxi_per_share +','
						             + jsObject.repaying_debt_list[i].debt_value + ','
						             + jsObject.repaying_debt_list[i].creditor_right_hold_id + ',' //债权持有表id
						             + jsObject.repaying_debt_list[i].remain_period
					             + ')" >转让</a>'
							}else{
								transfer_str = '转让';
							}
							
							tr_str += 
								'<tr class="'+ stripe_str +' tr_2" onClick="showsubmenu('+'\'a'+ i +'\''+')">'
				             + '<td width="18%"><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+ jsObject.repaying_debt_list[i].gather_money_order_id +'"><img src="<%=__PUBLIC__%>/images/'+ icon_str +'" /><span>'+  jsObject.repaying_debt_list[i].gather_money_order_id +'</span></a></td>'
				             + '<td width="15%">'+ jsObject.repaying_debt_list[i].origin_money +'元('+ jsObject.repaying_debt_list[i].hold_share +'份)</td>'
				             + '<td width="11%">'+ jsObject.repaying_debt_list[i].annulized_rate_int +'%</td>'
				             + '<td width="11%">'+ jsObject.repaying_debt_list[i].sum_should_recieve_total.toFixed(2)+ '</td>'
				             + '<td width="11%">'+ jsObject.repaying_debt_list[i].recieve_monthly_principal_and_interest.toFixed(2)+ '</td>'
				             + '<td width="8%">'+ jsObject.repaying_debt_list[i].current_period+ '/'+ jsObject.repaying_debt_list[i].total_periods +'</td>'
				             + '<td width="13%">'+ jsObject.repaying_debt_list[i].automatic_repayment_date +'</td>'
				             + '<td width="9%">还款中</td>'
				             + '<td width="4%">'
				             
				             + transfer_str
				             
				          +'</td>'
				          + '</tr>'

				          + '<tr class="'+ stripe_str +'" id="submenua'+ i +'" style="display:none;">'
				            +'<td>待收本金'+ jsObject.repaying_debt_list[i].sum_should_recieve_principle +'</td>'
				             +'<td>待收利息'+ jsObject.repaying_debt_list[i].sum_should_recieve_interest +'</td>'
				             +'<td><a href="#"></a></td>'
				             +'<td>&nbsp;</td>'
				             +'<td>&nbsp;</td>'
				             +'<td>&nbsp;</td>'
				             +'<td>&nbsp;</td>'
				             +'<td>&nbsp;</td>'
				             +'<td>&nbsp;</td>'
				          +'</tr>'

				          ;
						}
					}
					$("#table1 tr:eq(0)").after(newer_str + tr_str + div_str);
					
				}
			},"json");
		}


		//获取已结清债权
		function get_pay_off_debt(){

			var url = "<%=__ROOT_PATH__%>/user/usercenter/financial_manage/get_pay_off_debt.html";
			$.ajax({
				url:url,
				type:'post',

				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table2 tr:not(:first)").empty(); 

					//散标集合字符串
					var tr_str ='';

					//新手标的字符串
					var newer_str = '';

					//查不到结果字符串
					var no_result_str = '';

					if(jsObject.pay_off_newer_bid.newer_bid_gather_id == null  && jsObject.pay_off_debt_list.length == 0 ){
						no_result_str = '<tr><td colspan="8" class="td_1">没有已结清的债权</td></tr>';
					}
					if(jsObject.pay_off_newer_bid.newer_bid_gather_id != null){
						var bid_money = jsObject.pay_off_newer_bid.bid_money.toFixed(2);
						var bid_share = bid_money / 50;
						newer_str = 
							   '<tr>'
				             + '<td width="18%"><img src="<%=__PUBLIC__%>/images/icon2.jpg" /><span>'+ jsObject.pay_off_newer_bid.newer_bid_gather_id +'</span></td>'
				             + '<td width="15%">'+ bid_money +'('+ bid_share +'份)</td>'
				             + '<td width="11%">16%</td>'
				             + '<td width="13%">'+ jsObject.pay_off_newer_bid.recieve_interest.toFixed(2) +'</td>'
				             + '<td width="13%">'+ jsObject.pay_off_newer_bid.recieve_interest.toFixed(2) +'</td>'
				             + '<td width="15%">'+ jsObject.pay_off_newer_bid.recieve_date +'</td>'
				             + '<td width="11%"></td>'
			     	    	+ '</tr>';
					}
					if(jsObject.pay_off_debt_list.length != 0){
						for(var i = 0; i < jsObject.pay_off_debt_list.length; i++ ){
							if(jsObject.pay_off_debt_list[i].borrow_type==1){
								icon_str = 'icon1.jpg';
							}else if(jsObject.pay_off_debt_list[i].borrow_type==2){
								icon_str = 'icon1_2.jpg';
							}else if(jsObject.pay_off_debt_list[i].borrow_type==3){
								icon_str = 'icon1_3.jpg';
							}
	
							var can_transfer_share = (Number(jsObject.pay_off_debt_list[i].hold_share) - Number(jsObject.pay_off_debt_list[i].transfer_share));
							//alert(jsObject.pay_off_debt_list[i].hold_money);
							//alert(jsObject.pay_off_debt_list[i].transfer_share);
							
							var stripe_str = '';
							if(i%2==0){
								stripe_str = 'tr_1';
							}
							tr_str = 
								'<tr>'
				             + '<td><img src="<%=__PUBLIC__%>/images/'+ icon_str +'" /><span>'+ jsObject.pay_off_debt_list[i].debt_id +'</span></td>'
				             + '<td>'+ jsObject.pay_off_debt_list[i].invest_money +'('+ jsObject.pay_off_debt_list[i].hold_share +'份)</td>'
				             + '<td>'+ jsObject.pay_off_debt_list[i].annulized_rate_int +'%</td>'
				             + '<td>'+ jsObject.pay_off_debt_list[i].sum_principal_interest +'</td>'
				             + '<td>'+ jsObject.pay_off_debt_list[i].sum_interest +'</td>'
				             + '<td>'+ jsObject.pay_off_debt_list[i].pay_off_time_str +'</td>'
				             + '<td>还款</td>'
				          + '</tr>';
						}
					}
					$("#table2 tr:eq(0)").after(newer_str + tr_str + no_result_str);
					
				}
			},"json");
			
		}
		


		//获取投标中的债权
		function get_bidding_debt(){
			var url = "<%=__ROOT_PATH__%>/user/usercenter/financial_manage/get_bidding_debt.html";
			$.ajax({
				url:url,
				type:'post',

				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table3 tr:not(:first)").empty(); 

					//散标集合字符串
					var tr_str ='';

					//新手标的字符串
					var newer_str = '';

					//alert(jsObject.bidding_newer_bid.id == null);
					if(jsObject.bidding_newer_bid.id != null){
						newer_str = 
							'<tr class="tr_1">'
				             + '<td><a href="javascript:void(0);"><img src="<%=__PUBLIC__%>/images/icon2.jpg" /><span>'+  jsObject.bidding_newer_bid.id +'</span></a></td>'
				             + '<td>20000元(400份)</td>'
				             + '<td>16%</td>'
				             + '<td>1天</td>'
				             + '<td>A</td>'
				             + '<td>投满为止</td>'
				             + '<td>'+ jsObject.bidding_newer_bid.gather_progress +'%</td>'
				          + '</tr>';
					}
					
					if(jsObject.bidding_debt_list.length==0 && jsObject.bidding_newer_bid.id == null){
						tr_str = '<tr><td colspan="7" class="td_1">没有投标中的债权</td></tr>';
					}else{
						for(var i=0;i<jsObject.bidding_debt_list.length;i++){
							tr_str += 
								'<tr class="tr_1">'
				             + '<td><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+ jsObject.bidding_debt_list[i].gather_id +'"><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>'+  jsObject.bidding_debt_list[i].gather_id +'</span></a></td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].invest_money +'元('+ jsObject.bidding_debt_list[i].invest_share +'份)</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].annulized_rate_int +'%</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].borrow_duration +'个月</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].credit_rating +'</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].remain_time_str +'</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].gather_progress +'%</td>'
				          + '</tr>';
						}
					}
					$("#table3 tr:eq(0)").after(newer_str + tr_str);
					
				}
			},"json");
		}

		//获取已转出的债权
		function get_transfered_out_debt(){
			
			var url = "<%=__ROOT_PATH__%>/user/usercenter/financial_manage/get_transfered_out_debt.html";
			$.ajax({
				url:url,
				type:'post',

				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table4 tr:not(:first)").empty(); 

					//散标集合字符串
					var tr_str ='';

					
					if(jsObject.bidding_debt_list.length==0){
						tr_str = '<tr><td colspan="7" class="td_1">没有已结清的债权</td></tr>';
					}else{
						for(var i=0;i<jsObject.bidding_debt_list.length;i++){
							tr_str += 
								'<tr class="tr_1">'
				             + '<td><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+ jsObject.bidding_debt_list[i].gather_id +'"><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>'+  jsObject.bidding_debt_list[i].gather_id +'</span></a></td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].invest_money +'元('+ jsObject.bidding_debt_list[i].invest_share +'份)</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].annulized_rate_int +'%</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].borrow_duration +'个月</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].credit_rating +'</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].remain_time_str +'</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].gather_progress +'%</td>'
				          + '</tr>';
						}
					}
					
					$("#table4 tr:eq(0)").after(tr_str);
					
				}
			},"json");
			
		}

		function check_share(){

			 var transfer_share = $("#transfer_share").val();

			 if(transfer_share == 0 || transfer_share == null){
				$("#error_msg").html("请输入正确的份数");
				$("#tip").css({"display":"block"});
			}else{
				$("#error_msg").html("");
				$("#tip").css({"display":"none"});
			}
		}
		
		function count_transfer_price(){
			//转让价格
			var transfer_price_per_share = $("#hidden_creditor_right_value").val() * $("#transfer_factor").val();
			//装让份数
			var transfer_share = $("#transfer_share").val();
			//可转份数
			var can_transfer_share = $("#hidden_can_transfer_share").val();
			
			if(transfer_share == null || transfer_share == ""){
				return ;
			}

			//alert(transfer_share);
			//alert(can_transfer_share);

			//if(transfer_share > can_transfer_share){
			//	$("#transfer_share").val(can_transfer_share);
			//}
			
			//转让总价格 = 转让价格 * 份数
			var transfer_price = transfer_price_per_share * transfer_share;
			//转让费用 = 转让总价格 * 0.005
			var transfer_fee = transfer_price * 0.005;

			//预计收入金额
			var pre_recieve_money = transfer_price - transfer_fee;
			
			$("#transfer_price_per_share").html(transfer_price_per_share.toFixed(2));
			$("#transfer_price").html(transfer_price.toFixed(2));
			$("#transfer_fee").html(transfer_fee.toFixed(2));

			$("#pre_recieve_money").html(pre_recieve_money.toFixed(2));
			
		}

		function confirm_debt_transfer(){

			var url = "<%=__ROOT_PATH__%>/user/financial/transfer/confirm_debt_transfer.html";

			/*
				final long creditor_right_hold_id = getParameterToLong("creditor_right_hold_id");//债权持有表的id
				final long gather_money_order_id = getParameterToLong("gather_money_order_id");//债权id（筹款单id）
				final BigDecimal creditor_right_value = new BigDecimal(getParameterToDouble("creditor_right_value")); // 债权价值
				
				final BigDecimal transfer_factor = new BigDecimal(getParameterToDouble("transfer_factor"));//转让系数
				final BigDecimal transfer_price = new BigDecimal(getParameterToDouble("transfer_price"));//转让价格(每份)
				final int transfer_share = getParameterToInt("transfer_share");  //转让份数
				
				//转让总价格
				//final BigDecimal transfer_total_value = transfer_price.multiply(new BigDecimal(transfer_share));
				final BigDecimal transfer_manage_fee = new BigDecimal(getParameterToDouble("transfer_manage_fee"));
			 */

			 var creditor_right_hold_id = $("#hidden_creditor_right_hold_id").val();
			 var gather_money_order_id = $("#hidden_gather_money_order_id").val();
			 var creditor_right_value = $("#hidden_creditor_right_value").val();

			 var transfer_factor = $("#transfer_factor").val();
			 var transfer_price = $("#transfer_price_per_share").html();
			 var transfer_share = $("#transfer_share").val();

			 var transfer_manage_fee = $("#transfer_fee").html();
			 var money_password = $("#money_password").val();

			 $("#error_msg").html("");
				$("#tip").css({"display":"none"});
			 
			 if(transfer_share == 0 || transfer_share == null){
				 $("#error_msg").html("请输入正确的份数");
					$("#tip").css({"display":"block"});
					return ;
			}
			if( !$("#checkbox_1").is(':checked') ){
				$("#error_msg").html("请先同意转让协议");
				$("#tip").css({"display":"block"});
				return ;
			}
			if(money_password == "" || money_password == null){
				$("#error_msg").html("请输入提现密码");
				$("#tip").css({"display":"block"});
				return ;
			}

			$.ajax({
				url:url,
				type:'post',
				data:{
					"creditor_right_hold_id":creditor_right_hold_id,
					"gather_money_order_id":gather_money_order_id,
					"creditor_right_value":creditor_right_value,

					"transfer_factor":transfer_factor,
					"transfer_price":transfer_price,
					"transfer_share":transfer_share,

					"transfer_manage_fee":transfer_manage_fee,
					"money_password":money_password	
				
				},
				cache:false,
				success:function(data)
				{
					if(data=="1"){
						//alert(data);
						$("#confirm_transfer").hide();
						$("#success").show();
						//showMessage(["提示","挂出转让单成功"]);
						//window.location.reload();
					}else if(data == "2"){
						$("#error_msg").html("转出份数不能大于可转让份数");
						$("#tip").css({"display":"block"});
						//alert("挂出转出单失败");
					}else if(data == "7"){
						$("#error_msg").html("请先设置提现密码");
						$("#tip").css({"display":"block"});
					}else if(data == "8"){
						$("#error_msg").html("提现密码错误");
						$("#tip").css({"display":"block"});
					}
					else{
						alert("else..");
					}
				}
			},"json");
			
		}
		
		
	</script>

</body>
</html>

