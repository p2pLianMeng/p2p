<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
			             <script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
			             <link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>
<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;自动投标工具
    </div>
<%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_zdtb">
       <div class="up">自动投标</div>
       <div class="up up_2">自动投标状态：<span id="disjunctor">  </span></div>
       <div class="up_3">
          <script src="js/loginDialog_2.js" type="text/javascript"></script>
          <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
          <button onClick="openme(1,2)" id="one_key_open" style="display:none;" class="button_1">开启一键式自动投标</button>
          <button onClick="shut();" id="shut" style="display:none;" class="button_1">关闭自动投标</button>
          <div id="1" class="loginDiv1"></div>
          <div id="2" class="loginDiv2" style=" z-index:10005; display:none;">
             <div class="geren_zdtb_con">
                <input onClick="closeme(1,2)" class="button_3" type="button" value="x" />
                <div class="con">
                   <label class="label_1">账户可用余额：</label>
                   <p class="p_1">${cny_can_used } 元</p>
                   <p class="p_2">（大于等于1000元才可开启自动投标工具）</p>
                </div>
                <div class="con">
                   <label class="label_1">每次投标金额：</label>
                   <p class="p_1">200</p>
                </div>
                <div class="con">
                   <label class="label_1">利息范围：</label>
                   <p class="p_1">10.0% － 24.0%</p>
                </div>
                <div class="con">
                   <label class="label_1">借款期限：</label>
                   <p class="p_1">5天 － 36月</p>
                </div>
                <div class="con">
                   <label class="label_1">信用等级范围：</label>
                   <p class="p_1">C － A+</p>
                </div>
                <div class="con">
                   <label class="label_1">账户保留金额：</label>
                   <p class="p_1">200.00元</p>
                </div>
                <div class="con_2"><input class="button_4" type="button" onclick="open_auto(0);" value="开始投标" /></div>
                <div id="tip_0" class="con_2 con_3"></div>
             </div>
          </div>
          <button id="open" onClick="openme(3,4)" style="display:none;" >开启自助式自动投标</button>
          <button id="open_update" onClick="openme(3,4)"  style="display:none;" class="button_1">修改自动投标设置</button>
          <div id="3" class="loginDiv1"></div>
          <div id="4" class="loginDiv2" style=" z-index:10005; display:none;">
             <div class="geren_zdtb_con">
                <input onClick="closeme(3,4)" class="button_3" type="button" value="x" />
                <div class="con">
                   <label class="label_1">账户可用余额：</label>
                   <p class="p_1">${cny_can_used } 元</p>
                   <p class="p_2">（大于等于1000元才可开启自动投标工具）</p>
                </div>
                <div class="con">
                   <label class="label_1">每次投标金额：</label>
                   <p class="p_1">
                   <input id="bid_amount" onblur="change_50_mul('bid_amount')" class="text_1" type="text" value="200"/><span>元</span><a onclick="add_amount();" >+</a><a onclick="sub_amount();">-</a></p>
                   <p class="p_2">（该数值须不小于200元，且为50的倍数）</p>
                </div>
                <div class="con">
                   <label class="label_1">利息范围：</label>
                   <p class="p_1"><input id="annulized_rate_min" class="text_1" type="text" value="10" /><span>%</span><span class="span_2">-</span><input id="annulized_rate_max" class="text_1" type="text" value="24" /><span>%</span></p>
                   <p class="p_2">（10%-24%为有效利率范围）</p>
                </div>
                <div class="con">
                   <label class="label_1">借款期限：</label>
                   <p class="p_1">
                      <select id="borrow_duration_min" class="select_1">
                         <option>5天</option>
                         <option>10天</option>
                         <option>15天</option>
                         <option>20天</option>
                         <option>25天</option>
                         <option>1月</option>
                         <option>6月</option>
                         <option>9月</option>
                         <option>12月</option>
                         <option>18月</option>
                         <option>24月</option>
                         <option>36月</option>
                      </select>
                      <span class="span_2">-</span>
                      <select id="borrow_duration_max" class="select_1">
                         <option>36月</option>
                         <option>24月</option>
                         <option>18月</option>
                         <option>12月</option>
                         <option>9月</option>
                         <option>6月</option>
                         <option>1月</option>
                         <option>25天</option>
                         <option>20天</option>
                         <option>15天</option>
                         <option>10天</option>
                         <option>5天</option>
                      </select>
                   </p>
                </div>
                <div class="con">
                   <label class="label_1">信用等级范围：</label>
                   <p class="p_1">
                      <select id="credit_rate_min" class="select_1">
                         <option>C</option>
                         <option>C+</option>
                         <option>B</option>
                         <option>B+</option>
                         <option>A</option>
                         <option>A+</option>
                      </select>
                      <span class="span_2">-</span>
                      <select id="credit_rate_max" class="select_1" >
                         <option>A+</option>
                         <option>A</option>
                         <option>B+</option>
                         <option>B</option>
                         <option>C+</option>
                         <option>C</option>
                      </select>
                   </p>
                </div>
                <div class="con">
                   <label class="label_1">账户保留金额：</label>
                   <p class="p_1"><input id="reserved_amount" class="text_2" type="text" value="200" />元</p>
                   <p class="p_2">（填写一个不小于100金额，这部分钱不会加入自动投标）</p>
                </div>
                <div class="con_2"><input class="button_4" type="button" value="开始投标" onclick="open_auto(1);"/></div>
                <div id="tip_1" class="con_2 con_3"></div>
             </div>
          </div>
       </div>
	   <div class="geren_zdtb_up">                   
          <h1>自动投标工具说明</h1>
          1.&nbsp;借款进入招标中三十分钟后，才会启动自动投标。<br />
          2.&nbsp;投标进度达到95%时停止自动投标。若投标后投标进度超过95%，则按照投标进度达到95%的金额向下取50的倍数投标。<br />
          3.&nbsp;单笔投标金额若超过该标借款总额的20%，则按照20%比例的金额向下取50的倍数投标。<br />
          4.&nbsp;满足自动投标规则的金额小于设定的每次投标金额，也会进行自动投标。<br />
          5.&nbsp;借款用户在申请借款时会自动关闭自动投标，以避免借款被用作自动投标资金。<br />
          6.&nbsp;投标排序规则如下：<br />
          &nbsp;&nbsp;&nbsp;&nbsp;a）投标序列按照开启自动投标的时间先后进行排序。<br />
          &nbsp;&nbsp;&nbsp;&nbsp;b）每个用户每个标仅自动投标一次，投标后，排到队尾。<br />
          &nbsp;&nbsp;&nbsp;&nbsp;c）轮到用户投标时没有符合用户条件的标，也视为投标一次，重新排队。
       </div>
    </div>
  </div>    
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>
<script type="text/javascript">
$(function(){
	div();
	var disjunctor = ${disjunctor};
	if(disjunctor == 3){
		$("#disjunctor").html("状态异常，请联系客服");
	}else if(disjunctor == 0){
		$("#disjunctor").html("关闭状态");
		$("#open").show();
		$("#one_key_open").show();
	}else if(disjunctor == 1){
		$("#disjunctor").html("已开启");
		$("#shut").show();
		$("#open_update").show();
	}
})

function div(){
	var bid_amount = ${user_automatically_bid.m.bid_amount}.toFixed(0);

	var borrow_duration_min = ${user_automatically_bid.m.borrow_duration_min};
	var borrow_duration_max = ${user_automatically_bid.m.borrow_duration_max};
	var borrow_duration_day_min = ${user_automatically_bid.m.borrow_duration_day_min};
	var borrow_duration_day_max = ${user_automatically_bid.m.borrow_duration_day_max};
	var min = (borrow_duration_min=="0"?borrow_duration_day_min+"天":borrow_duration_min+"月");
	var max = (borrow_duration_max=="0"?borrow_duration_day_max+"天":borrow_duration_max+"月");
	var annulized_rate_min = (${user_automatically_bid.m.annulized_rate_min }*100).toFixed(0);
	var annulized_rate_max = (${user_automatically_bid.m.annulized_rate_max }*100).toFixed(0);
	var reserved_amount = ${user_automatically_bid.m.reserved_amount}.toFixed(0);
	
	$("#bid_amount").val(bid_amount);
	$("#borrow_duration_min").val(min);
	$("#borrow_duration_max").val(max);
	
	$("#annulized_rate_min").val(annulized_rate_min);
	$("#annulized_rate_max").val(annulized_rate_max);
	$("#credit_rate_min").val("${user_automatically_bid.m.credit_rate_min }");
	$("#credit_rate_max").val("${user_automatically_bid.m.credit_rate_max }");
	$("#reserved_amount").val(reserved_amount);
}

function openme(i,j){
	$("#"+i).show();
	$("#"+j).show();
}
function closeme(i,j){
	$("#"+i).hide();
	$("#"+j).hide();
}

function change_50_mul(the_id){
	var amount = $("#"+the_id).val();
	$("#"+the_id).val(amount - amount % 50);
}
function add_amount(){
	var bid_amount = $("#bid_amount").val();
	$("#bid_amount").val(parseInt(bid_amount)+50);
}
function sub_amount(){
	var bid_amount = $("#bid_amount").val();
	bid_amount = parseInt(bid_amount)-50;
	if(bid_amount < 0){
	return;
	}
	$("#bid_amount").val(bid_amount);
}

function duration(){
	var duration_min = $("#duration_min").val();
	//alert(duration_min);
	var min_num = duration_min.replace("天","");
	min_num = duration_min.replace("月","");
	
	//var duration_max_all = $("#duration_max").children();
	//for(var i = 0; i < duration_max_all.length; i++){
   // 	var children_max = duration_max_all[i].html();
    	
  //  }
  //  $("#duration_max").find("select:option").each(function(){
   // 	var duration_max = this.value.replace("天","");
   // 	duration_max = this.value.replace("月","");
  //  })
}
function open_auto(type){//开启自动投标

	var bid_amount;
	var annulized_rate_min;
	var annulized_rate_max;
	var borrow_duration_min;
	var borrow_duration_max;
	var credit_rate_min;
	var credit_rate_max;
	var reserved_amount;
if(type == 1){
	bid_amount = $("#bid_amount").val();
	annulized_rate_min = $("#annulized_rate_min").val();
	annulized_rate_max = $("#annulized_rate_max").val();
	borrow_duration_min = $("#borrow_duration_min").val();
	borrow_duration_max = $("#borrow_duration_max").val();
	credit_rate_min = $("#credit_rate_min").val();
	credit_rate_max = $("#credit_rate_max").val();
	reserved_amount = $("#reserved_amount").val();
}else if(type == 0){
	bid_amount = 200;
	annulized_rate_min = "10";
	annulized_rate_max = "24";
	borrow_duration_min = "5天";
	borrow_duration_max = "36月";
	credit_rate_min = "C";
	credit_rate_max = "A+";
	reserved_amount = 200;
}
	
		$.ajax({
		type:'post',
		url:"<%=__ROOT_PATH__%>/user/usercenter/automatically_bid/open_automatically_bid.html",
		async:false,
		cache:false,
		data:{"bid_amount":bid_amount,"annulized_rate_min":annulized_rate_min,"type":type,
		"annulized_rate_max":annulized_rate_max,"borrow_duration_min":borrow_duration_min,
		"borrow_duration_max":borrow_duration_max,"credit_rate_min":credit_rate_min,
		"credit_rate_max":credit_rate_max,"reserved_amount":reserved_amount},
		success:function(data){
			if(data=="ok"){
				window.location.reload();
			}else if(data!=null){
				$("#tip_"+type).html(data);
				setTimeout(function(){$("#tip_"+type).html("");},3000);
			}
		}
	},"html");
}
function shut(){
	$.post(
		"<%=__ROOT_PATH__%>/user/usercenter/automatically_bid/shut_automatically_bid.html",
		{},
		function(data){
			window.location.reload();
			//showMessage(["提示",data]);
		})
}

</script>
</body>
</html>

