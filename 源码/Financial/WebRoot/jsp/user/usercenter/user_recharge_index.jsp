<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
	</head>
	<body>
		<form method="post" name="form1" id="form1" action="<%=__ROOT_PATH__%>/user/usercenter/recharge/get_user_recharge_order_info.html">
			<table>
				<tr>
					<td>
						选择支付方式:
					</td>
					<td>
						<select name="PayID" id="PayID">
			                            <option value="3001">招商银行(借)</option>
										<option value="3002">工商银行(借)</option>
										<option value="3003">建设银行(借)</option>
										<option value="3004">浦发银行(借)</option>
										<option value="3005">农业银行(借)</option>
										<option value="3006">民生银行(借)</option>
										<option value="3009">兴业银行(借)</option>
										<option value="3020">交通银行(借)</option>
										<option value="3022">光大银行(借)</option>
										<option value="3026">中国银行(借)</option>
										<option value="3032">北京银行(借)</option>		
										<option value="3035">平安银行(借)</option>
										<option value="3036">广发银行(借)</option>
										<option value="3039">中信银行(借)</option>
							
										<option value="4001">招商银行(贷)</option>
										<option value="4002">工商银行(贷)</option>
										<option value="4003">建设银行(贷)</option>
										<option value="4004">浦发银行(贷)</option>
										<option value="4005">农业银行(贷)</option>
										<option value="4006">民生银行(贷)</option>
										<option value="4009">兴业银行(贷)</option>
										<option value="4020">交通银行(贷)</option>
										<option value="4022">光大银行(贷)</option>
										<option value="4026">中国银行(贷)</option>
										<option value="4032">北京银行(贷)</option>		
										<option value="4035">平安银行(贷)</option>
										<option value="4036">广发银行(贷)</option>
										<option value="4039">中信银行(贷)</option>							
						</select>
					</td>
				</tr>
		
				<tr>
					<td>
						订单金额:
					</td>
					<td>
						<input name="OrderMoney" value="1" />
						<span>建议1分钱支付</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" id="btnpost" value="提交" />
					</td>
				</tr>

			</table>
		</form>
	</body>
</html>
