<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;我的债权
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jyjl geren_wdzq">
       <div class="left">
          <p>债权已赚金额&nbsp;<a href="#"><img src="images/loan_3.png" /><span>债权已赚金额 = 债权已赚利息罚息及违约金 + 债权转让盈亏 - 债权转让应计利息支出<br />（由于购买转让的债权时可能需要先支付应计利息，所以此数值可能为负）<img src="images/loan_4.png" /></span></a>
          </p><span><s>0.00</s>元</span>
          <p class="p_1">利息收益&nbsp;<a href="#"><img src="images/loan_3.png" /><span>利息收益 = 债权已赚利息罚息及违约金 - 债权转让应计利息支出<br />（由于购买转让的债权时可能需要先支付应计利息，所以此数值可能为负）<img src="images/loan_4.png" /></span></a>&nbsp;0.00元</p>
          <p class="p_1">债权转让盈亏&nbsp;&nbsp;0.00元</p>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td>债权账户资产</td>
                <td>回收中的债权数量</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td>0.00元</td>
                <td>0个</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td colspan="2">没时间亲自投标？试试联富金融自动投标功能吧。</td>
                <td><a href="#">自动投标</a></td>
             </tr>
          </table>
       </div>    
    </div> 
    <div class="geren_wdzq_head"><span><a class="hover" href="#">回收中的债权</a><a href="#">已结清的债权</a><a href="#">投标中的债权</a><a href="#">已转出的债权</a></span><s><a href="#">回账查询</a></s></div>
    <div class="geren_wdzq_con" style="display:block">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>原始投资金额</td>
             <td>年利率</td>
             <td>待收本息</td>
             <td>月收本息</td>
             <td>期数</td>
             <td>下个还款日</td>
             <td>状态</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="8" class="td_1">没有回收中的债权</td>
          </tr>
       </table>
    </div>   
    <div class="geren_wdzq_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>投资金额</td>
             <td>年利率</td>
             <td>回收金额</td>
             <td>已赚金额</td>
             <td>结清日期</td>
             <td>结清方式</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="7" class="td_1">没有已结清的债权</td>
          </tr>
       </table>
    </div> 
    <div class="geren_wdzq_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>原始投资金额</td>
             <td>年利率</td>
             <td>期限</td>
             <td>信用等级</td>
             <td>剩余时间</td>
             <td>投标进度</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="7" class="td_1">没有投标中的债权</td>
          </tr>
       </table>
    </div>
    <div class="geren_wdzq_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>成交份数</td>
             <td>转出时债权总价值</td>
             <td>转出时总成交金额</td>
             <td>实际收入</td>
             <td>交易费用</td>
             <td>转让盈亏</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="7" class="td_1">没有已转出的债权</td>
          </tr>
       </table>
    </div>
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>

