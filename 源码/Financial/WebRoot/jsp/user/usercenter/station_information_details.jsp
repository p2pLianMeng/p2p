<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">站内消息</a>&nbsp;
    </div>
   <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_znxx_head"><a id="tz" href="<%=__ROOT_PATH__ %>/user/usercenter/to_station_information.html">通知</a><a id="xiangqing" href="<%=__ROOT_PATH__ %>/user/usercenter/station_information_details.html" class="hover">详情</a></div>
    <!-- 显示全部 -->
    <div id="geren_im" class="geren_znxx">
    <c:forEach items="${station_list}" var="record">
       <div class="con">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <td width="10%" rowspan="3" class="td_1"><a href="#"><img src="<%=__PUBLIC__ %>/images/geren_9.png" /></a></td>
                <td width="90%" class="td_2">${record.r.title }</td>
             </tr>
             <tr>
                <td width="90%">${record.r.add_time }</td>
             </tr>
             <tr>
                <td class="td_3">${record.r.content }<span id="chakan"> 点击 <a href="<%=__ROOT_PATH__ %>/user/usercenter/user_credit_files.html">这里</a> 查看您的信用状况。 感谢您对我们的关注和支持!</span></td>
             </tr>             
          </table>
       </div>
    </c:forEach>
       <div class="down" style="margin-left: 600px;margin-bottom: 10px;">
       ${pageDiv }
      </div>
    </div>
    </div>
  </div>    



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>

