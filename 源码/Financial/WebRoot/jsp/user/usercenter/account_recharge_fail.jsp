<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">  

<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">资金管理</a>&nbsp;<span>></span>&nbsp;充值
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_cz geren_cz_cg">
       <div class="geren_cz_cg1"><img src="<%=__PUBLIC__%>/images/zhifu_1.png" />对不起，充值失败！</div>
       <div class="geren_cz_cg2"><b>您的订单号为：</b><span>${TransId }</span></div>
       <div class="geren_cz_cg3">如对此次充值有任何疑问，可凭以上订单号与我们的客服联系，谢谢！</div>
       <div class="geren_cz_cg3">客服联系电话：<span>4006-888-923</span></div>
       <div class="geren_cz_cg3"><a href="#">在线提问==></a></span></div>
       <div class="geren_cz_cg3">您可以在<a href="#">交易记录</a>中查询到您所有的充值记录哦~</div>
       <div class="geren_cz_cg3 geren_cz_cg5"><input type="button" value="继续充值" /></div>
    </div>
        
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>
