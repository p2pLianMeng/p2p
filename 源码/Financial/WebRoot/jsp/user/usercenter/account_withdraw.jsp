<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>${applicationScope.title}</title>

		<link href="style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
	</head>
	<body>

		<div class="head">
			<%@ include file="/jsp/index/index_top.jsp"%>
			<span id="ny_pic"></span>
		<input type="hidden" value="${ok}" name="is_" id="is_"/>
			<div class="about">
				<div class="about_con">
					<div class="geren_up">
						<a href="geren.html">我的联富金融</a>&nbsp;
						<span>></span>&nbsp;
						<a href="#">资金管理</a>&nbsp;
						<span>></span>&nbsp;
						<a href="#">提现</a>
					</div>
					<%@ include file="/jsp/user/usercenter/user_center_left.jsp"%>
					<div class="geren_cz geren_tx">
						<div class="up">
							选择提现银行卡
						</div>
						<input type="hidden" name="card" id="card" value="0" />
						<div class="geren_tx_con">
							<c:if test="${not empty user_bank_info.m.bank_card_num_one}">

								<div class="left" name="left1">
									<p>
										<a href="#" id="suoding"> <img id="img1" src="" /> </a>
									</p>
									<span>${user_bank_info.m.bank_card_num_one }</span>
									<input type="hidden" id="bank_name_one" name="bank_name_one"
										value="${user_bank_info.m.bank_name_one }" />
									<input type="hidden" id="bank_address_one"
										name="bank_address_one"
										value="${user_bank_info.m.bank_address_one }" />
									<input type="hidden" id="bank_card_num_one"
										name="bank_card_num_one"
										value="${user_bank_info.m.bank_card_num_one }" />
								</div>

							</c:if>
							<c:if test="${not empty user_bank_info.m.bank_card_num_two}">
								<div class="left" name="left2">
									<p>
										<a href="#"> <img id="img2" src="" /> </a>
									</p>
									<span>${user_bank_info.m.bank_card_num_two}</span>
									<input type="hidden" id="bank_name_two" name="bank_name_two"
										value="${user_bank_info.m.bank_name_two }" />
									<input type="hidden" id="bank_address_two"
										name="bank_address_two"
										value="${user_bank_info.m.bank_address_two }" />
									<input type="hidden" id="bank_card_num_two"
										name="bank_card_num_two"
										value="${user_bank_info.m.bank_card_num_two }" />
								</div>

							</c:if>
							<c:if test="${not empty user_bank_info.m.bank_card_num_three}">
								<div class="left" name="left3">
									<p>
										<a href="#"><img id="img3" src="" /> </a>
									</p>
									<span>${user_bank_info.m.bank_card_num_three }</span>
									<input type="hidden" id="bank_name_three"
										name="bank_name_three"
										value="${user_bank_info.m.bank_name_three }" />
									<input type="hidden" id="bank_address_three"
										name="bank_address_three"
										value="${user_bank_info.m.bank_address_three }" />
									<input type="hidden" id="bank_card_num_three"
										name="bank_card_num_three"
										value="${user_bank_info.m.bank_card_num_three }" />
								</div>

							</c:if>

							<div id="add_bank" class="left">
								<p>
									<a
										href="<%=__ROOT_PATH__ %>/user/usercenter/bank_info/bank_info_index.html"><img
											src="<%=__PUBLIC__ %>/images/chongzhi_0.jpg" /> </a>
								</p>
								<span><a
									href="<%=__ROOT_PATH__ %>/user/usercenter/bank_info/bank_info_index.html">添加银行卡</a>
								</span>
							</div>

						</div>
						<div class="up">
							填写提现金额
						</div>
						<div class="up_3">
							<label class="label_1">
								可提现资金：
							</label>
							<p class="P1">
							<c:if test="${user_now_money.m.net_amount_int==0}">
								${user_now_money.m.cny_can_used }
							</c:if>
							<c:if test="${user_now_money.m.net_amount_int==1}">
								${user_now_money.m.net_amount-user_now_money.m.total_net_cash }
							</c:if>
								<span>元</span>
							</p>
						</div>
						<div class="up_3 up_4">
							<label class="label_1">
								<span>*</span>&nbsp;提现金额：
							</label>
							<input class="input_1" onkeyup="a();" id="amount" name="amount"
								type="text">
							元
						</div>
						<div id="jine" align="center" style="color: red; display: none;">
							请输入大于100小于50万的正整数
						</div>
						<div class="up_3">
							<label class="label_1">
								提现费用：
							</label>
							<div class="up_3_div">
								<p class="a">
									<span id="fee">0.00</span>元
								</p>
								<div class="b">
									<a href="#"><img src="<%=__PUBLIC__ %>/images/loan_3.png" />
									</a>
									<span class="b_tb"><img
											src="<%=__PUBLIC__ %>/images/loan_4.png" />
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="td_1" colspan="3">
													充值和提现都是0手续费哟！
												</td>
											</tr>
										</table> </span> 提现费用将从您的提现金额中扣除
								</div>
							</div>
						</div>
						<div class="up_3">
							<label class="label_1">
								实际扣除金额：
							</label>
							<p class="P1" id="realfee">
								0.00
								<span>元</span>
							</p>
						</div>
						<div class="up_3">
							<label class="label_1">
								预计到账日期：
							</label>
							<p>
								${d }&nbsp;&nbsp;&nbsp;&nbsp;1-2个工作日（双休日和法定节假日除外）之内到账
							</p>
						</div>
						<div class="up_3 up_4">
							<label class="label_1">
								<span>*</span>&nbsp;提现密码：
							</label>
							<input class="input_1" name="password" id="password"
								type="password">
						</div>
						<div class="up_3 up_5">
							<label class="label_1"></label>
							<p class="p3">
								<a id="tix" href="javascript:void(0);">提&nbsp;现</a>
							</p>
						</div>
						<div class="up_6">
							<p class="p3">
								温馨提示：
								<br />
								1.&nbsp;请确保您输入的提现金额，以及银行帐号信息准确无误；
								<br />
								2.&nbsp;如果您填写的提现信息不正确可能会导致提现失败，由此产生的提现费用将不予返还； 
								<br />
								3.&nbsp;如果客户在平台没有任何投标、借贷行为（新手标不算作投标行为），将收取5%手续费；
								<br />
								4.&nbsp;在双休日和法定节假日期间，用户可以申请提现，联富金融会在下一个工作日进行处理；
								<br />
								5.&nbsp;平台禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确认，将终止该账户的使用。
								<br />
								6.&nbsp;由此造成的不便，请多多谅解！
							</p>
						</div>
						<table class="table_1">
							<tr class="tr_1">
								<td
									style="text-align: center; font-size: 20px; padding-top: 50px;"
									colspan="8">
									提现记录
								</td>
							</tr>
							<tr>
								<td width="12%" align="center">
									开户人
								</td>
								<td width="28%" align="center">
									银行卡号
								</td>
								<td width="12%" align="center">
									提现金额
								</td>
								<td width="12%" align="center">
									提交时间
								</td>
								<td width="12%" align="center">
									是否汇款
								</td>
								<td width="12%" align="center">
									操作
								</td>
							</tr>

							<c:forEach items="${user_withdraw_list}" var="record">
								<tr>
									<td align="center">
										${record.r.account_holder }
									</td>
									<td align="center">
										${record.r.bank_card_num }
									</td>
									<td align="center">
										${record.r.amount }
									</td>
									<td align="center">
										${record.r.add_time }
									</td>
									<td align="center">
										<c:if test="${record.r.is_pay == 1 }">
											 否
										</c:if>
										<c:if test="${record.r.is_pay == 3 }">
											 否
										</c:if>
										<c:if test="${record.r.is_pay == 2}">
											 是
										</c:if>
									</td>
									<td align="center">
										<c:if test="${record.r.is_through == 1 }">
											<a
												href="<%=__ROOT_PATH__ %>/user/usercenter/withdrawal/user_withdraw_revoke.html?id=${record.r.id }"><span
												style="color: blue;">撤单</span> </a>
										</c:if>
										<c:if test="${record.r.is_through == 2 }">
											<span style="color: #FF6347;">已撤单</span>
										</c:if>
										<c:if test="${record.r.is_through == 3 }">
											<span style="color: green;">审核通过</span>
										</c:if>
										<c:if test="${record.r.is_through == 4 }">
											<span style="color: red;">审核未通过</span>
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</table>
						<div style="padding-left: 680px;">
							${pageDiv }
						</div>
					</div>



				</div>

			</div>

			<%@ include file="/jsp/index/index_foot.jsp"%>
	</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
	
});

$(document).ready(function(){
	var arr_name  =  ["中国工商银行","中国农业银行","招商银行","中国银行","中国建设银行",
	                  "广发银行","兴业银行","交通银行","中国邮政储蓄银行","浦发银行",
	                  "中国民生银行","中国光大银行","中信银行","平安银行","华夏银行",
	                  "城市信用社", "恒丰银行","中国农业发展银行","中国商业银行","农村信用社",
	                  "农村合作银行","浙商银行","上海农商银行","中国进出口银行", "渤海银行",
	                  "国家开发银行","徽商银行"];
		for(var i = 0; i< arr_name.length;i++){
			if($("#bank_name_one").val() == arr_name[i]){
			$("#img1").attr("src","<%=__PUBLIC__ %>/images/chongzhi_"+(i+1)+".jpg");
		}
	}
});
$(document).ready(function(){
	var arr_name  =  ["中国工商银行","中国农业银行","招商银行","中国银行","中国建设银行",
	                  "广发银行","兴业银行","交通银行","中国邮政储蓄银行","浦发银行",
	                  "中国民生银行","中国光大银行","中信银行","平安银行","华夏银行",
	                  "城市信用社", "恒丰银行","中国农业发展银行","中国商业银行","农村信用社",
	                  "农村合作银行","浙商银行","上海农商银行","中国进出口银行", "渤海银行",
	                  "国家开发银行","徽商银行"];
		for(var i = 0; i< arr_name.length;i++){
			if($("#bank_name_two").val() == arr_name[i]){
			$("#img2").attr("src","<%=__PUBLIC__ %>/images/chongzhi_"+(i+1)+".jpg");
		}
	}
});
$(document).ready(function(){
	var arr_name  =  ["中国工商银行","中国农业银行","招商银行","中国银行","中国建设银行",
	                  "广发银行","兴业银行","交通银行","中国邮政储蓄银行","浦发银行",
	                  "中国民生银行","中国光大银行","中信银行","平安银行","华夏银行",
	                  "城市信用社", "恒丰银行","中国农业发展银行","中国商业银行","农村信用社",
	                  "农村合作银行","浙商银行","上海农商银行","中国进出口银行", "渤海银行",
	                  "国家开发银行","徽商银行"];
		for(var i = 0; i< arr_name.length;i++){
			if($("#bank_name_three").val() == arr_name[i]){
			$("#img3").attr("src","<%=__PUBLIC__ %>/images/chongzhi_"+(i+1)+".jpg");
		}
	}
});
		function a(){
		//alert(1);
			
			var is_ = $("#is_").val();
			var amount = $("#amount").val();
			var arr = amount.split(".");
			var arr2 = amount.split(" ");
			if(parseInt(amount) <100 || parseInt(amount)>500000||arr.length > 1||arr2.length>1||!(/^[0-9]*$/).test(amount)){
				$("#jine").show();
			}else{
			if(is_== 1){
				var fee = $("#fee").html();
				fee =  (amount * 0.05).toFixed(2);
				$("#fee").html(fee);
			}
			var realfee = $("#realfee").html();
			realfee = parseInt(amount);
			$("#realfee").html(realfee);
			$("#jine").hide();
			}
			if(amount==null||amount==""){
				var realfee = $("#realfee").html("0.00");
			}
		}
		$("#tix").click(function(){
			var card = $("#card").val();
			if(card != 1 && card != 2 && card !=3){
				showMessage(["提示","请选择银行卡"]);
				return;
			}
			var bank_name_one = $("#bank_name_one").val();
			var bank_address_one = $("#bank_address_one").val();
			var bank_card_num_one = $("#bank_card_num_one").val();
			var bank_name_two = $("#bank_name_two").val();
			var bank_address_two = $("#bank_address_two").val();
			var bank_card_num_two = $("#bank_card_num_two").val();
			var bank_name_three = $("#bank_name_three").val();
			var bank_address_three = $("#bank_address_three").val();
			var bank_card_num_three = $("#bank_card_num_three").val();
			var amount = $("#amount").val();
			var fee = $("#fee").html();
			var arr = amount.split(".");
			var arr2 = amount.split(" ");
			if(parseInt(amount) <100 || parseInt(amount)>500000||arr.length > 1||arr2.length>1||!(/^[0-9]*$/).test(amount)){
				showMessage(["提示","请输入正确的金额"]);
				return;
			}
			var fee = $("#fee").html();
			var realfee = $("#realfee").html();
			var password = $("#password").val();
			if(password==null||password==""){
				showMessage(["提示","请填写提现密码"]);
				return;
			}
			//alert(123);
			var i = true;
			if(i){
				i = false;
				$.ajaxSetup({ async: false});
				$.post(
					'<%=__ROOT_PATH__%>/user/usercenter/withdrawal/user_withdraw.html',
				{
					"card":card,
					"bank_name_one":bank_name_one,
					"bank_address_one":bank_address_one,
					"bank_card_num_one":bank_card_num_one,
					"bank_name_two":bank_name_two,
					"bank_address_two":bank_address_two,
					"bank_card_num_two":bank_card_num_two,
					"bank_name_three":bank_name_three,
					"bank_address_three":bank_address_three,
					"bank_card_num_three":bank_card_num_three,
					"fee" : fee,
					"amount":amount,
					"password":password
				},
				function(data){
					//alert(data);
					if(data == 1){
						showMessage(["提示","可用余额小于0,请联系管理员"]);
					}else if(data == 2){
						showMessage(["提示","冻结的资产小于0,请联系管理员"]);
					}else if(data == 3){
						showMessage(["提示","请选择银行卡"]);
					}else if(data == 4){
						showMessage(["提示","请先实名认证"]);
					}else if(data == 5){
						showMessage(["提示","请填写提现金额"]);
					}else if(data == 6){
						showMessage(["提示","提现金额不能大于可用余额"]);
					}else if(data == 8){
						showMessage(["提示","提交提现申请失败"]);
					}else if(data == 9){
						alert("提交提现申请成功");
						window.location.href="<%=__ROOT_PATH__%>/user/usercenter/withdrawal/to_user_withdraw.html";
					}
					else if(data == 10){
						showMessage(["提示","请填写提现密码"]);
					}
					else if(data == 11){
						showMessage(["提示","提现密码错误"]);
					}else if(date == 20){
						showMessage(["提示","提现申请已经成功，但是手机短信消息没有发送成功"]);
					}
					i = true;
				},'html');
			}
			
		});
		$("div[name='left1']").click(function(){
			$("div[name='left1']").css("background","url(<%=__PUBLIC__%>/images/geren_4.png) right bottom no-repeat");
			$("div[name='left1']").css("border","1px #55a500 solid");
			 $("div[name='left2']").css("background","");
			 $("div[name='left3']").css("background","");
			 $("div[name='left2']").css("border","1px #cccccc solid");
			  $("div[name='left3']").css("border","1px #cccccc solid");
			  $("#card").attr("value",1);
		});
		$("div[name='left2']").click(function(){
			$("div[name='left2']").css("background","url(<%=__PUBLIC__%>/images/geren_4.png) right bottom no-repeat");
			$("div[name='left2']").css("border","1px #55a500 solid");
			 $("div[name='left1']").css("background","");
			 $("div[name='left3']").css("background","");
			  $("div[name='left1']").css("border","1px #cccccc solid");
			  $("div[name='left3']").css("border","1px #cccccc solid");
			  $("#card").attr("value",2);
		});
		$("div[name='left3']").click(function(){
			$("div[name='left3']").css("background","url(<%=__PUBLIC__%>/images/geren_4.png) right bottom no-repeat");
			$("div[name='left3']").css("border","1px #55a500 solid");
			 $("div[name='left1']").css("background","");
			 $("div[name='left2']").css("background","");
			  $("div[name='left1']").css("border","1px #cccccc solid");
			   $("div[name='left2']").css("border","1px #cccccc solid");
			   $("#card").attr("value",3);
		});
</script>

