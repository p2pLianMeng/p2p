<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">  

<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="<%=__ROOT_PATH__%>/index/index.html">首页</a>&nbsp;<span>></span>&nbsp;个人信息
    </div>
   
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_right">
      <div class="geren_head"><a href="#">
      <c:if test="${user.m.url == null}"><img src="<%=__PUBLIC__%>/images/geren_2.png" /></c:if>
      <c:if test="${user.m.url != null}"> <img src="${applicationScope.user_image_path}${user.m.url}" width="98px" height="98px"/></c:if>
      </a></div>
      <div class="geren_head_con">
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td class="td_head" colspan="10">${user.m.nickname}</td>
            </tr>
            <tr>
               <td colspan="4"><p class="td_p"><span class="a">安全等级</span><span class="b"><s style="width:${security_progress}%"></s></span></p></td>
               <td colspan="2" class="td_head2">账户余额</td>
               <td colspan="2" class="td_head3"><span>${user_now_money.m.cny_can_used+user_now_money.m.cny_freeze }<s>元</s></span></td>
               <td colspan="1" class="td_a1"><a href="<%=__ROOT_PATH__%>/user/usercenter/recharge/into_user_recharge_index.html">充值</a></td>
               <td colspan="1" class="td_a1"><a style="background:#2ea7e0;" href="<%=__ROOT_PATH__ %>/user/usercenter/withdrawal/to_user_withdraw.html">提现</a></td>
            </tr>
            <tr>
              <c:if test="${is_bind_phone == 1}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_2.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="绑定手机"></a></td>
              </c:if>
               <c:if test="${is_bind_phone == 0}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_2_2.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="未绑定手机"></a></td>
              </c:if>
              <c:if test="${is_real_name_confirmed == 1}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_3.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="实名认证"></a></td>
              </c:if>
              <c:if test="${is_real_name_confirmed == 0}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_3_2.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="未实名认证"></a></td>
              </c:if>
              
              <c:if test="${is_bind_email == 1}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_4.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="绑定邮箱"></a></td>
              </c:if>
              <c:if test="${is_bind_email == 0}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_4_2.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="未绑定邮箱"></a></td>
              </c:if>
              
              <c:if test="${is_set_money_password == 1}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_5.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="提现密码"></a></td>
              </c:if>
              <c:if test="${is_set_money_password == 0}">
              	<td width="47" colspan="1" class="td_a2"><a style="background:url(<%=__PUBLIC__%>/images/U_jihua_5_2.png) center no-repeat #f6fbfd" href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html" title="未设置提现密码"></a></td>
              </c:if>
               <td width="97" class="td_head4">冻结金额</td>
               <td width="137" colspan="2" class="td_head5"><span>${user_now_money.m.cny_freeze }<s>元</s></span></td>
               <td width="97" class="td_head4">可用金额</td>
               <td width="137" colspan="2" class="td_head5"><span>${user_now_money.m.cny_can_used }<s>元</s></span></td>
            </tr>
         </table>
      </div>
    </div><!--
   <c:if test="${security_progress < 100}">
	   		 <div class="geren_head2_con">
	       	   <p class="a">2</p>
		       <p class="b">填写借款信息</p>
		       <p class="c">信息及资料完整度</p>
		       <p class="d"><span><s style="width: ${security_progress}"></s></span></p>
		       <p class="e"><span>${security_progress}%<s></s></span></p>
		       <p class="f"><a href="<%=__ROOT_PATH__%>/user/usercenter/into_safety_information.html">立即完善</a></p>
	    	</div> 
   </c:if>       
          
            -->
            <c:if test="${sessionScope.user.m.borrow_type!=0}">
	    	    <c:if test="${loan_process_information!=null}">
	    	    
	    	       <c:if test="${loan_process_information.index=='0_-1'}">
		    	    	<!-- 没有进行实名认证 -->
		    	    	<div class="geren_head2_con">
				       	   <p class="a"></p>
					       <p class="b"></p>
					       <p class="c"></p>
					       <p class="d"></p>
					       <p class="e"></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/usercenter/into_safety_information.html">请先实名认证</a></p>
				    	</div> 
			    	</c:if>
		    	    <c:if test="${loan_process_information.index=='0_0'}">
		    	    	<!-- 请先选择消费贷或者生意贷0_0 -->
		    	    	<div class="geren_head2_con">
				       	   <p class="a"></p>
					       <p class="b"></p>
					       <p class="c"></p>
					       <p class="d"></p>
					       <p class="e"></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/financial/borrow/loan_applay.html?borrow_type=1">借贷申请</a></p>
				    	</div> 
			    	</c:if>
			    	<c:if test="${loan_process_information.index=='0_1'}">
		    	    	<!--错误的借贷类型0_1 -->bug:错误的借贷类型0_1
			    	</c:if>
			    	
			    	<!-- 1 start -->
				    	<c:if test="${loan_process_information.index=='1_0'}">
			    	    	<!--申请单查询失败 -->bug:申请单查询失败,请联系客服
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='1_1'}">
			    	    	<!--刚刚填写借款申请单 -->
			    	    	 <div class="geren_head2_con">
				       	   <p class="a">2</p>
					       <p class="b">填写借款信息</p>
					       <p class="c">信用资料完整度</p>
					       <p class="d"><span><s style="width: ${loan_process_information.ratio}"></s></span></p>
					       <p class="e"><span>${loan_process_information.part}%<s></s></span></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/usercenter/into_user_borrow_page.html">立即完善</a></p>
				    	</div>
				    	</c:if>
				    	
				    	<c:if test="${loan_process_information.index=='1_2'}">
			    	    	<!--刚刚填写认证信息 -->
			    	    	 <div class="geren_head2_con">
				       	   <p class="a">3</p>
					       <p class="b">补充借款信息</p>
					       <p class="c">信用资料完整度</p>
					       <p class="d"><span><s style="width: ${loan_process_information.ratio}"></s></span></p>
					       <p class="e"><span>${loan_process_information.part}%<s></s></span></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/usercenter/into_user_borrow_page.html">立即完善</a></p>
				    	</div> 
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='1_3'}">
			    	    	<!--正在审核 -->
			    	    	 <div class="geren_head2_con">
				       	   <p class="a">3</p>
					       <p class="b">资料审核中</p>
					       <p class="c"></p>
					       <p class="d">您的借款申请正在审核中，请耐心等待</p>
					       <p class="e"></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/usercenter/into_user_borrow_page.html">正在审核中</a></p>
				    	</div> 
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='1_4'}">
			    	    	<!--驳回修改 -->
			    	    	<div class="geren_head2_con">
				       	   <p class="a">2</p>
					       <p class="b">填写借款信息</p>
					       <p class="c">信用资料完整度</p>
					       <p class="d">您的借款申请已经被驳回，请重新提交资料{补充资料}</p>
					       <p class="e"><span>${loan_process_information.part}%<s></s></span></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/usercenter/into_user_borrow_page.html">立即完善</a></p>
				    	</div> 
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='1_5'}">
			    	    	<!--拒绝 -->
			    	    	 <div class="geren_head2_con">
				       	   <p class="a"></p>
					       <p class="b"></p>
					       <p class="c"></p>
					       <p class="d"></p>
					       <p class="e"></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/financial/borrow/loan_applay.html?borrow_type=1">继续申请</a></p><!-- 默认是消费贷申请 -->
				    	</div> 
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='1_e'}">
			    	    	<!--错误的申请借款类型1_e -->bug:错误的申请借款类型
				    	</c:if>
			    	<!-- 1 end -->
			    	
			    	<!-- 2 start -->
				    	<c:if test="${loan_process_information.index=='2_0'}">
			    	    	<!--借款凑集单查询不到 -->
			    	    	bug:借款凑集单查询不到
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='2_1'}">
			    	    	<!--正在凑集中 -->
			    	    	 <div class="geren_head2_con">
				       	   <p class="a">4</p>
					       <p class="b">凑集中</p>
					       <p class="c"></p>
					       <p class="d"><span><s style="width: ${loan_process_information.ratio}"></s></span></p>
					       <p class="e"><span>${loan_process_information.part}%<s></s></span></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/usercenter/into_user_borrow_page.html">查看筹集详情</a></p>
				    	</div> 
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='2_2'}">
			    	    	<!--凑集失败 -->
			    	    	<div class="geren_head2_con">
				       	   <p class="a"></p>
					       <p class="b">凑集失败</p>
					       <p class="c"></p>
					       <p class="d"></p>
					       <p class="e"></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/financial/borrow/loan_applay.html?borrow_type=1">继续申请</a></p>
				    	</div>
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='2_3'}">
			    	    	<!--该状态-凑集成功关注还款状态-不可能执行到这的-->
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='2_e'}">
			    	    	<!--错误的凑集类型 -->bug:错误的凑集类型
				    	</c:if>
			    	<!-- 2 end -->
			    	<!-- 3 start -->
				    	<c:if test="${loan_process_information.index=='3_1'}">
			    	    	<!--还款不存在的状态 -->bug:还款不存在的状态
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='3_2'}">
			    	    	<!--还款中 -->
			    	    	<div class="geren_head2_con">
				       	   <p class="a"></p>
					       <p class="b">还款中</p>
					       <span>${loan_process_information.msg}</span>
					       <a href="<%=__ROOT_PATH__%>/user/usercenter/into_user_borrow_page.html" style=" padding:5px 20px; background:#ed5050; color:#fff;border-radius:15px; margin-left:10px;">立即还款</a>
				    	</div> 
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='3_3'}">
			    	    	<!--还款成功状态-->
			    	    	<div class="geren_head2_con">
				       	   <p class="a"></p>
					       <p class="b">还款成功</p>
					       <p class="c"></p>
					       <p class="d"></p>
					       <p class="e"></p>
					       <p class="f"><a href="<%=__ROOT_PATH__%>/user/financial/borrow/loan_applay.html?borrow_type=1">你可以继续申请</a></p>
				    	</div> 
				    	</c:if>
				    	<c:if test="${loan_process_information.index=='3_e'}">
			    	    	<!--错误的凑集类型 -->bug:错误的凑集类型
				    	</c:if>
			    	<!-- 3 end -->
	    	  </c:if>
       </c:if>
          
          
          
          
          
    <div class="geren_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="tr_1">
             <td>账户净资产</td>
             <td>&nbsp;</td>
             <td class="td_2">理财资产
                <a href="#">
                   <img src="<%=__PUBLIC__%>/images/loan_3.png" />
                   <span>理财账户内的资产，包括债权投资账户资产和联富宝账户资产，其中债权投资账户资产等于持有债权的待回收本金。<s></s></span>
                </a>                
             </td>
             <td>&nbsp;</td>
             <td>借款负债</td>
             <td>&nbsp;</td>
             <td>账户余额</td>
          </tr>
          <tr class="tr_1 tr_2">
             <td>${net_worth_total}<s>元</s></td>
             <td class="td_1">=</td>
             <td>${financial_total}<s>元</s></td>
             <td class="td_1">+</td>
             <td style="color:#c82e25;">-${borrow_info.weihuan_total}<s>元</s></td>
             <td class="td_1">+</td>
             <td>${user_now_money.m.cny_can_used+user_now_money.m.cny_freeze }<s>元</s></td>
          </tr>
          <tr class="tr_3">
             <td><a href="#">理财账户</a></td>
             <td>已赚总金额</td>
             <td><s>${total_encome}</s>元</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td></td>
          </tr>
          <tr class="tr_4">
             <td>理财方式</td>
             <td>账户资产</td>
             <td>已赚金额
                <a href="#">
                   <img src="<%=__PUBLIC__%>/images/loan_3.png" />
                   <span>已赚金额 = 已赚利息罚息及违约金 + 债权转让盈亏 - 债权转让应计利息支出（由于购买转让的债权时可能需要先支付应计利息，所以此数值可能为负）<s></s></span>
                </a>
             </td>
             <td>加权平均收益率</td>
             <td>&nbsp;</td>
             <td>持有数量</td>
             <td>&nbsp;</td>
          </tr>
          <tr class="tr_5">
             <td>债权投资</td>
             <td>${financial_account_asset.to_collect_pricipal}<s>元</s></td>
             <td>0.00<s>元</s></td>
             <td>${financial_account_asset.average_income_rate}%</td>
             <td>&nbsp;</td>
             <td>${financial_account_asset.hold_count}</td>
             <td><a href="<%=__ROOT_PATH__%>/user/usercenter/financial_manage/to_my_debt.html">查看</a></td>
          </tr>
          <tr class="tr_5">
             <td>联富宝</td>
             <td>${get_lfoll_fix_bid.total_bid_money}<s>元</s></td>
             <td>${get_lfoll_fix_bid.total_earned_incom}<s>元</s></td>
             <td>${get_lfoll_fix_bid.avg_annual_earning * 100 }%</td>
             <td>&nbsp;</td>
             <td>${get_lfoll_fix_bid.hold_amount}</td>
             <td><a href="<%=__ROOT_PATH__%>/user/financial/lfollinvest_user_info/goto_user_fix_bid_manage.html">查看</a></td>
          </tr>
          <tr class="tr_3">
             <td><a href="#">借款账户</a></td>
             <td>待还总金额</td>
             <td><s>${borrow_info.weihuan_total}</s>元</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td><a href="<%=__ROOT_PATH__%>/user/usercenter/loan_manage/to_my_loan_apply.html">申请查询</a></td>
          </tr>
          <tr class="tr_4">
             <td colspan="2">借款标题</td>
             <td>待还本息</td>
             <td colspan="2">管理费</td>
             <td colspan="2">逾期费用</td>
          </tr>
          
          <c:if test="${borrow_info.borrow_title==null}">
          	<tr class="tr_5">
				<td colspan="7">没有借款记录</td>
			</tr>   
          </c:if>
          
          <c:if test="${borrow_info.borrow_title!=null}">
          	<tr class="tr_4">
             <td colspan="2">${borrow_info.borrow_title}</td>
             <td>${borrow_info.weihuan_beixi_total }</td>
             <td colspan="2">${borrow_info.weihuan_normal_manage_total }</td>
             <td colspan="2">${borrow_info.weihuan_over_fee }</td>
          </tr>
          </c:if>
          
       </table>
    </div>
    <%--<div class="geren_con2">
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
           <td class="title" colspan="7">理财推荐</td>
        </tr>
        <tr>
           <td class="td_1"><a href="#">联富宝-A141103期</a></td>
           <td colspan="2">计划金额&nbsp;&nbsp;10,000,000元</td>
           <td colspan="2">剩余金额&nbsp;&nbsp;0元</td>
           <td class="td_2" colspan="2"  rowspan="2"><a href="#">预定满额</a></td>
        </tr>
        <tr>
           <td>加入条件&nbsp;&nbsp;1,000元</td>
           <td colspan="2">预期收益&nbsp;&nbsp;7%/年</td>
           <td colspan="2">加入进度&nbsp;&nbsp;100%</td>
        </tr>
        <tr class="tr_1">
           <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
           <td class="td_1"><a href="#">联富宝-A141103期</a></td>
           <td colspan="2">计划金额&nbsp;&nbsp;10,000,000元</td>
           <td colspan="2">剩余金额&nbsp;&nbsp;0元</td>
           <td class="td_2" colspan="2"  rowspan="2"><a href="#">预定满额</a></td>
        </tr>
        <tr>
           <td>加入条件&nbsp;&nbsp;1,000元</td>
           <td colspan="2">预期收益&nbsp;&nbsp;7%/年</td>
           <td colspan="2">加入进度&nbsp;&nbsp;100%</td>
        </tr>
        <tr class="tr_1">
           <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
           <td class="td_1"><a href="#">联富宝-B141103期</a></td>
           <td colspan="2">计划金额&nbsp;&nbsp;10,000,000元</td>
           <td colspan="2">剩余金额&nbsp;&nbsp;0元</td>
           <td class="td_2" colspan="2"  rowspan="2"><a href="#">预定满额</a></td>
        </tr>
        <tr>
           <td>加入条件&nbsp;&nbsp;1,000元</td>
           <td colspan="2">预期收益&nbsp;&nbsp;7%/年</td>
           <td colspan="2">加入进度&nbsp;&nbsp;100%</td>
        </tr>
        <tr class="tr_1">
           <td colspan="7">&nbsp;</td>
        </tr>
        <tr class="tr_2">
           <td>借款标题</td>
           <td>信用等级</td>
           <td>年利率</td>
           <td>金额</td>
           <td>期限</td>
           <td>进度</td>
           <td>&nbsp;</td>
        </tr>
        <tr class="tr_6">
           <td class="td_3"><a href="#">短期周转</a></td>
           <td class="td_7"><span>HR</span></td>
           <td>10.00%</td>
           <td>12,000元</td>
           <td>3个月</td>
           <td>62%</td>
           <td class="td_4"><a href="#">投标</a></td>
        </tr>
     </table>

    </div>
  --%></div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>
