<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="#">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">资金管理</a>&nbsp;<span>></span>&nbsp;充值
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
     <div class="geren_cz">
    <form action="<%=__ROOT_PATH__ %>/user/usercenter/recharge/get_user_recharge_order_info.html" method="post" onsubmit="return verification();">  
     <div class="up">填写充值金额</div>
       <div class="geren_cz_con">
         <!--  <label class="label_1"><span>*</span>&nbsp;选择充值银行：</label>
         <select name="PayID" style="width:110px;height:30px;" id="PayId">
                <option value="3001"> 招商银行</option>
                  <option value="3002">  工商银行</option>
                  <option value="3003">  建设银行</option>
                  <option value="3004">  浦发银行</option>
                  <option value="3005">  农业银行</option>
                  <option value="3006"> 民生银行</option>
                  <option value="3009"> 兴业银行</option>
                  <option value="3020"> 交通银行</option>
                  <option value="3022"> 光大银行</option>
                <option value="3026"> 中国银行</option>
                  	<option value="3032">  北京银行</option>
                   <option value="3035"> 平安银行</option>
                    <option value="3036"> 广发银行</option>
                  <option value="3039"> 中信银行</option>
                  </select>  -->
       </div>                  
                 
      <div class="up_3">
          <label class="label_1">充值方式：</label>
          <p class="P1">支持储蓄卡和信用卡充值</p>
       </div>
       <div class="up_3">
          <label class="label_1">可用余额：</label>
          <p class="P1">${user_now_money.m.cny_can_used }<span>元</span></p>
       </div>
       <div class="up_3 up_4">
          <label class="label_1"><span>*</span>&nbsp;充值金额：</label>
          <input class="input_1" name="OrderMoney" id="amount" type="text">元
          <p class="P2" id="P2" style="display:none;">请输入100或100以上的正整数</p>
       </div>
       <div class="up_3">
          <label class="label_1">充值费用：</label>
          <p class="P1" ><span id="fee" style="font-size:25px">0.00</span><span>元</span>
          <a class="a_a" href="#">
                <img src="<%=__PUBLIC__ %>/images/loan_3.png" />
                <span>充值和提现都是0手续费哟！<s></s></span>
            </a>
          </p>
          
       </div>
       <div class="up_3">
          <label class="label_1"><span>*</span>&nbsp;实际到账金额：</label>
          <p class="P1" ><span id="realamount" style="font-size:25px;">0.00</span><span>元</span></p>
       </div>
       <div class="up_3 up_5">
          <label class="label_1"></label>
          <p class="p3"><input id="tijiao"  type="submit" style="width:120px;height:40px;background-color: #CD2626;cursor:pointer;"  value="提交"/></p>
       </div>
       </form>
       <div class="up_6">
          <p class="p3">             
             温馨提示：<br />
             1.&nbsp;为了您的账户安全，请在充值前进行身份认证、手机绑定以及提现密码设置。<br />
             2.&nbsp;您的账户资金将通过第三方平台进行充值。<br />
             3.&nbsp;请注意您的银行卡充值限制，以免造成不便。<br />
             4.&nbsp;禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确认，将终止该账户的使用。<br />
             5.&nbsp;如果充值金额没有及时到账，请联系客服，4006-888-923。
          </p>
       </div>
    </div>
        
  </div>    
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>

</body>
</html>







<script language="JavaScript" type="text/javascript">

$("#amount").keyup(function(){
	var arr = $("#amount").val().split(".");
	var arr2 = $("#amount").val().split(" ");
	if(arr.length>1||arr2.length>1||$("#amount").val()<100||!(/^[0-9]*$/).test($("#amount").val())){
		$("#P2").show();
		$("#fee").html("0.00");
		$("#realamount").html("0.00");
	}else{
		$("#P2").hide();
		var fee = $("#fee").html();
		var amount = $("#amount").val();
		var fee2 = parseInt(amount)*0.005;
		fee2 = Math.round(fee2*100)/100;
		$("#fee").html("0.00");
		if($("#fee").html()>100){
		$("#fee").html("100");
		}
		$("#realamount").html($("#amount").val());
	}
});




$(function(){
	$(".select").each(function(){
		var s=$(this);
		var z=parseInt(s.css("z-index"));
		var dt=$(this).children("dt");
		var dd=$(this).children("dd");
		var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
		var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
		dt.click(function(){dd.is(":hidden")?_show():_hide();});
		dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
		$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
	})
})

	function verification(){
		var amount = $("#amount").val();
		if($("#amount").val()==null||$("#amount").val()==""){
			$("#P2").show();
			$("#fee").html("0.00");
			$("#realamount").html("0.00");
			return false;
		}
		var arr = $("#amount").val().split(".");
	var arr2 = $("#amount").val().split(" ");
	if(arr.length>1||arr2.length>1||$("#amount").val()<100||!(/^[0-9]*$/).test($("#amount").val())){
		$("#P2").show();
		$("#fee").html("0.00");
		$("#realamount").html("0.00");
		return false;
	}else{
		$("#P2").hide();
		var fee = $("#fee").html();
		var amount = $("#amount").val();
		var fee2 = parseInt(amount)*0.005;
		fee2 = Math.round(fee2*100)/100;
		$("#fee").html("0.00");
		if($("#fee").html()>100){
		$("#fee").html("100");
		}
		$("#realamount").html($("#amount").val());
	}
	}
</script>
