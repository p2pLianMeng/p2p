<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
    
	<meta http-equiv="keywords" content="${applicationScope.keywords}">
	<meta http-equiv="description" content="${applicationScope.description}">  
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

	<script type="text/javascript">
		function showsubmenu(sid){
		    var whichEl = document.getElementById("submenu" + sid);
		    whichEl.style.display = whichEl.style.display =='none'?'':'none';
		}
	</script>

  </head>
  
  <body>
  <%@ include file="/jsp/index/index_top.jsp" %>
  
  <div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">资金管理</a>&nbsp;<span>></span>&nbsp;交易记录
    </div>
     <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <%--<div class="geren_left">
      <ul>
        <li><a class="a" href="geren.html"><span></span>我的联富金融</a></li>
        <li onClick="showsubmenu(1)">
			<a class="b" href="#" style="background:#ed5050;color:#fff;">
				<span style="background:url(images/geren_1.png) right -19px no-repeat;"></span>
				资金管理
			</a>
		</li>
        <li id="submenu1" class="submenu">
           <a href="geren_jyjl.html" style="background:#f8bbb2;color:#fff;"><span></span>交易记录</a>
           <a href="geren_cz.html"><span></span>充值</a>
           <a href="geren_tx.html"><span></span>提现</a>
        </li>
        <li onClick="showsubmenu(2)"><a class="c" href="#"><span></span>理财管理</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="geren_wdzq.html"><span></span>我的债权</a>
           <a href="geren_ujh.html"><span></span>联富金融</a>
           <a href="geren_zqzr.html"><span></span>债权转让</a>
           <a href="geren_lctj.html"><span></span>理财统计</a>
        </li>
        <li onClick="showsubmenu(3)"><a class="d" href="#"><span></span>借款管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="geren_wdjk.html"><span></span>我的借款</a>
           <a href="geren_jksqcx.html"><span></span>借款申请查询</a>
           <a href="geren_jktj.html"><span></span>借款统计</a>
        </li>
        <li onClick="showsubmenu(4)"><a class="e" href="#"><span></span>账户管理</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="geren_jcxx.html"><span></span>个人基础信息</a>
           <a href="geren_rzxx.html"><span></span>认证信息</a>
           <a href="geren_aqxx.html"><span></span>安全信息</a>
           <a href="geren_yhkxx.html"><span></span>银行卡信息</a>
        </li>
        <li onClick="showsubmenu(5)"><a class="f" href="geren_znxx.html"><span></span>站内消息</a></li>
        <li id="submenu5" class="submenu" style="display:none;"></li>
      </ul>
    </div>
    --%><div class="geren_jyjl">
       <div class="left"><p>账户余额</p><span><s>${user_now_money.m.cny_can_used +user_now_money.m.cny_freeze}</s>元</span></div>
       <div class="up">
          <p><span>可用资金</span><br />${user_now_money.m.cny_can_used }<s>元</s></p>
          <p><span>已充值总额</span><br />${num == null?"0.00":num }<s>元</s></p>
          <p><a href="<%=__ROOT_PATH__ %>/user/usercenter/recharge/into_user_recharge_index.html">充值</a></p>
       </div>
       <div class="up down">
          <p><span>冻结资金</span><br />${user_now_money.m.cny_freeze }<s>元</s></p>
          <p><span>已提现总额</span><br />${withdraw_num == null?"0.00":withdraw_num }<s>元</s></p>
          <p><a href="<%=__ROOT_PATH__ %>/user/usercenter/withdrawal/to_user_withdraw.html">提现</a></p>
       </div>
    </div> 
    
    <div class="geren_jyjl_con" style="height: auto;">
       <form action="<%=__ROOT_PATH__ %>/user/user_center/money_mange/recharge_records/transaction_records_query.html" method="post">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="tr_1">
             <td colspan="2">
                <span>查询类型</span>
                <select name="type"  id="type"> 
                	<c:choose>
                		<c:when test="${query_type==10}"><option value="10" selected="selected">所有</option></c:when>
                		<c:otherwise><option value="10">所有</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==11}"><option value="11" selected="selected">充值</option></c:when>
                		<c:otherwise><option value="11">充值</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==12}"><option value="12" selected="selected">提现</option></c:when>
                		<c:otherwise><option value="12">提现</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==13}"><option value="13" selected="selected">提现撤销</option></c:when>
                		<c:otherwise><option value="13">提现撤销</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==21}"><option value="21" selected="selected">联富宝回款</option></c:when>
                		<c:otherwise><option value="21">联富宝回款</option></c:otherwise>
                	</c:choose>
                	 <c:choose>
                		<c:when test="${query_type==22}"><option value="22" selected="selected">联富宝提前退出</option></c:when>
                		<c:otherwise><option value="22">联富宝提前退出</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==23}"><option value="23" selected="selected">联富宝购买支付</option></c:when>
                		<c:otherwise><option value="23">联富宝购买支付</option></c:otherwise>
                	</c:choose>
                	
                	
                	<c:choose>
                		<c:when test="${query_type==31}"><option value="31" selected="selected">借款服务费</option></c:when>
                		<c:otherwise><option value="31">借款服务费</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==32}"><option value="32" selected="selected">偿还本息</option></c:when>
                		<c:otherwise><option value="32">偿还本息</option></c:otherwise>
                	</c:choose>
                	
                	<c:choose>
                		<c:when test="${query_type==33}"><option value="33" selected="selected">借款管理费</option></c:when>
                		<c:otherwise><option value="33">借款管理费</option></c:otherwise>
                	</c:choose>
                	
                	
                  <c:choose>
                		<c:when test="${query_type==34}"><option value="34" selected="selected">返还服务费</option></c:when>
                		<c:otherwise><option value="34">返还服务费</option></c:otherwise>
                	</c:choose>
                	
                	<c:choose>
                		<c:when test="${query_type==35}"><option value="35" selected="selected">回收本息</option></c:when>
                		<c:otherwise><option value="35">回收本息</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==36}"><option value="36" selected="selected">提前回收</option></c:when>
                		<c:otherwise><option value="36">提前回收</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==37}"><option value="37" selected="selected">提前还款</option></c:when>
                		<c:otherwise><option value="37">提前还款</option></c:otherwise>
                	</c:choose>
                  <c:choose>
                		<c:when test="${query_type==38}"><option value="38" selected="selected">逾期管理费</option></c:when>
                		<c:otherwise><option value="38">逾期管理费</option></c:otherwise>
                	</c:choose>
                	 <c:choose>
                		<c:when test="${query_type==51}"><option value="51" selected="selected">债权转让管理费</option></c:when>
                		<c:otherwise><option value="51">债权转让管理费</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==201}"><option value="201" selected="selected">散标新手标利息</option></c:when>
                		<c:otherwise><option value="201">散标新手标利息</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==202}"><option value="202" selected="selected">联富宝新手标利息</option></c:when>
                		<c:otherwise><option value="202">联富宝新手标利息</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==203}"><option value="203" selected="selected">推荐人奖励</option></c:when>
                		<c:otherwise><option value="203">推荐人奖励</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==204}"><option value="204" selected="selected">理财奖励（活动）</option></c:when>
                		<c:otherwise><option value="204">理财奖励（活动）</option></c:otherwise>
                	</c:choose>
                	<c:choose>
                		<c:when test="${query_type==205}"><option value="205" selected="selected">奖励推荐人（活动）</option></c:when>
                		<c:otherwise><option value="205">奖励推荐人（活动）</option></c:otherwise>
                	</c:choose>
                </select>
             </td>
             <td colspan="2">
                <span>查询时间</span>
                <select id="time" name="time"> 
                <c:choose>
                	<c:when test="${query_time == 1}"><option value="1" selected="selected">三天以内</option></c:when>
                	<c:otherwise ><option value="1" >三天以内</option></c:otherwise>
                </c:choose>
                <c:choose>
                	<c:when test="${query_time == 2}"><option value="2" selected="selected">一周以内</option></c:when>
                	<c:otherwise ><option value="2" >一周以内</option></c:otherwise>
                </c:choose>
                <c:choose>
                	<c:when test="${query_time == 3}"><option value="3" selected="selected">一个月以内</option></c:when>
                	<c:otherwise ><option value="3" >一个月以内</option></c:otherwise>
                </c:choose>
                <c:choose>
                	<c:when test="${query_time == 4}"><option value="4" selected="selected">三个月以内</option></c:when>
                	<c:otherwise ><option value="4" >三个月以内</option></c:otherwise>
                </c:choose>
                <c:choose>
                	<c:when test="${query_time == 5}"><option value="5" selected="selected">一年以内</option></c:when>
                	<c:otherwise ><option value="5" >一年以内</option></c:otherwise>
                </c:choose>
                </select>
             </td>
             <td class="td_1"><input type="submit" style="width:80px;height: 30px;background-color: #ed5050;" value="查询"/></td>
          </tr>
       </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>时间</td>
             <td>类型明细</td>
             <td>收入</td>
             <td>支出</td>
             <td>结余</td>
             <td>备注</td>
          </tr>
          
          <c:forEach items="${transaction_records}" var="record">
            <tr>
             <td>${record.r.add_time}</td>
             <td>
             	<c:if test="${record.r.type == 11}">充值</c:if>
             	<c:if test="${record.r.type == 12}">提现</c:if>
             	<c:if test="${record.r.type == 13}">提现撤销</c:if>
             	<c:if test="${record.r.type == 21}">联富宝回款</c:if>
             	<c:if test="${record.r.type == 22}">联富宝提前退出</c:if>
             	<c:if test="${record.r.type == 23}">联富宝购买支付</c:if>
             	<c:if test="${record.r.type == 31}">借款服务费</c:if>
             	<c:if test="${record.r.type == 32}">偿还本息</c:if>
             	<c:if test="${record.r.type == 33}">借款管理费</c:if>
             	<c:if test="${record.r.type == 34}">返还服务费</c:if>
             	<c:if test="${record.r.type == 35}">回收本息</c:if>
             	<c:if test="${record.r.type == 36}">提前回收</c:if>
             	<c:if test="${record.r.type == 37}">提前还款</c:if>
             	<c:if test="${record.r.type == 38}">逾期管理费</c:if>
             	<c:if test="${record.r.type == 51}">债权转让管理费</c:if>
             	
             	<c:if test="${record.r.type == 201}">散标新手标利息</c:if>
             	<c:if test="${record.r.type == 202}">联富宝新手标利息</c:if>
             	<c:if test="${record.r.type == 203}">推荐人奖励</c:if>
             	<c:if test="${record.r.type == 204}">理财奖励（活动）</c:if>
             	<c:if test="${record.r.type == 205}">理财奖励（活动）</c:if>
             </td>
             <td>${record.r.income }</td>
             <td>${record.r.pay }</td>
             <td>${record.r.end_money }</td>
             <td>${record.r.detail }</td>
             </tr>
          </c:forEach>
          <br/>
          
       </table>
        </form>
        <div style="margin-left: 600px;padding-bottom:10px;">${pageDiv }</div>
    </div>
   
  </div>    
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>

  </body>
</html>
<script type="text/javascript">
	
</script>
