<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		<!-- 进行查询 -->
		<div class="container" style="margin-top: 12px;">
			<legend>
				管理员管理日志
			</legend>
				
					<tbody><!--
			${pageDiv}
			--><table class="table table-bordered table-hover table-condensed"
				id="table">
				<thead>
					<tr>
						<th>
							编号
						</th>
						<th>
							文件名
						</th>
						<th>
							文件路径
						</th>
						<th>
							最后修改时间
						</th>
						<th>
							操作
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${log_file_list}" var="row" varStatus="int">
						<tr>
							<td>
								${int.count }
							</td>
							<td>
								${row.file_name}
							</td>
							<td>
								${row.file_path}
							</td>
							<td>
								${row.last_modify_time}
							</td>
							<td>
								<a href="<%=__ROOT_PATH__%>/log_manager/download.html?path=${row.file_path}">下载</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div style="float:right;">${pageDiv}</div>
		</div>
	</body>
</html>
