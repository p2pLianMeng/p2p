﻿/*
 * 
 */
var https = "http://";
var URL =  https + window.location.host+"/user/authentication/submit_application/submit.html";
var AuditingURL =  https + window.location.host+"/user/authentication/submit_application/auditing_index.html";

/**
 * 下拉框
 * */
$(function(){
	$(".select").each(function(){
		var s=$(this);
		var z=parseInt(s.css("z-index"));
		var dt=$(this).children("dt");
		var dd=$(this).children("dd");
		var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
		var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
		dt.click(function(){dd.is(":hidden")?_show():_hide();});
		dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
		$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
	})
})

function ok(){
var apply_order_id=$("#apply_order_id").val();
$.post(
		URL,
		{'apply_order_id':apply_order_id},
		function(data){
			if(data=='ok'){
				window.location.href=AuditingURL;
			}else if(data=='no'){
				showMessage(["提示","请完善资料再进行提交!"]);
			}else{
				showMessage(["提示",data]);
			}
		},"html","application/x-www-form-urlencoded; charset=utf-8");
}

/**
 * 验证手机号码
 * */
function check_phone(id,num){
	var value = $("#"+id).val();
	if(!(/^1[3|4|5|8|]\d{9}$/.test(value))){
		$("#"+num).show();
	}
}

/**
 * 验证姓名，关系（中文）
 * */
function check(id,num){
	var value = $("#"+id).val();
	if(!(/^([\u4e00-\u9fa5]){2,7}$/.test(value))){
		$("#"+num).show();
	}
}

/**
 * 验证邮编 数字
 * */
function check_num(id,num){
	var value = $("#"+id).val();
	if(!(/^[1-9]\d{5}(?!\d)$/.test(value))){
		$("#"+num).show();
	}
}

/**
* 验证邮箱认证
* */
function check_email(id,num){
	var value = $("#"+id).val();
	if(!( /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.([a-zA-Z0-9_-])+)+$/.test(value))){
		$("#"+num).show();
	}
}

/**
 * 验证居住认证
 * */
function check_addr(id,num){
	var value = $("#"+id).val();
	if(!( /^(?=.*?[\u4E00-\u9FA5])[\dA-Za-z\u4E00-\u9FA5]+$/.test(value))){
		$("#"+num).show();
	}
}

/**
 * 验证 9到12位数字
 * */
function check_long_num(id,num){
	var value = $("#"+id).val();
	if(!(/^\S{9,12}$/.test(value))){
		$("#"+num).show();
	}
}

/**
 * 验证 长度（4-15位）
 * */
function check_long(id,num){
	var value = $("#"+id).val();
	if(!(/^.{4,15}$/.test(value))){
		$("#"+num).show();
	}
}


