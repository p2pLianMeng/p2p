
var path = "http://"+window.location.host;
var validate_url = path+"/user/financial/lfollinvest/validate_user_isnot_fix_bid.html"
/**
 * 校验是否投资新手表
 * @return
 */
function validate_whether_bid_newer(){
	$.ajax({
		url:validate_url,
		type:"post",
		success:function(data){
			if(data == "1"){
			//已经投资新手标
			$("#tishi").html("你已经投资了联富宝新手标!");
			}else if(data == "2"){
			//没有投资新手标
				$("#submit_new_bid_make_sure").attr("onclick","sumbmit_new_order()");
				refresh_order_pay();
				$("#validate_code").val("");
				openme();
			}
			
		}
		
	});
}

/***
*提交新手表订单
***/
function sumbmit_new_order(){
	var validate_code = $("#validate_code").val();
	if(validate_code == null || validate_code == "" || validate_code.length == 0){
		prompt("error_msg","验证码不能为空!");
		return;
	}
	var i = true;
	if(i){
		i = false;
		$.ajax({
			url:path+"/user/financial/lfollinvest/new_users_fix_bid.html",
			type:"post",
			async:false,
			data:{"validate_code":validate_code},
			success:function(data){
				if(data == "非理财身份"){
					prompt("error_msg","非理财账户，请先注册理财账户!");
				}else if(data == "NoLogined"){
	  					window.location.href = login_url;
	  				}else if(data == 1){
					prompt("error_msg","验证码为空!");
				}else if(data == 2){
					prompt("error_msg","Session验证码为空!");
				}else if(data == 3){
					prompt("error_msg","验证码不正确!");
					$("#validate_code").val("");
					refresh_order_pay();
				}else if(data == 4){
					prompt("error_msg","不能重复购买!");
					$("#validate_code").val("");
					refresh_order_pay();
				}else if(data == 5){
					closeme();
					showMessage(["提示","购买成功!"]);
				}else if(data == 6){
					prompt("error_msg","购买失败!");
				}
				i = true;
			}
			
		});
	}
}

/**
	 * 获取验证码：
	 * @return
	 */
	function refresh_order_pay(){
		var CheckCodeServlet=document.getElementById("CheckCodeServlet");
		CheckCodeServlet.src= path+"/code.jpg?id="+ Math.random();
	}
