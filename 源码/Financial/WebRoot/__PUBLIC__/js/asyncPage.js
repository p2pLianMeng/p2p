
var path = "http://" + window.location.host;
var async_page_url_public = path + "/user/financial/financial/loan_list_fix_bid_public.html";
var get_fix_bid_info_by_id_path = path + "/user/financial/lfollinvest/get_lfoll_invest_info.html?id=";


/**
 * 联富宝 异步分页
 * @return
 */
function change_page_of_async(pageNum,type){
	if(!(type == 3 || type == 6 || type == 12)){
		showMessage(["提示","查询联富宝类型出错"]);
		return;
	}
	$.ajax({
		url:async_page_url_public,
		type:"post",
		cache:false,
		async:false,
		data:{"pn":pageNum,"type":type},
		success:function(data){
			var jsonObject = eval("("+data+")");
			
			if(jsonObject != null){
				var ObjectArray = jsonObject.list;
				var AsyncPageDiv = jsonObject.AsyncPageDiv;
				
				var L03_table =  "#L03_table";
				var L03_table_first_tr = "#L03_table_first_tr";
				
				var L06_table =  "#L06_table";
				var L06_table_first_tr = "#L06_table_first_tr";
				
				var L12_table =  "#L12_table";
				var L12_table_first_tr = "#L12_table_first_tr";
				
				if(type == 3){
					$(L03_table + " tr:not(:first)").empty();
				}else if(type == 6){
					$(L06_table + " tr:not(:first)").empty();
				}else if(type == 12){
					$(L12_table + " tr:not(:first)").empty();
				}
				
				
				var str = "";
				for(var i in ObjectArray){
					var id = ObjectArray[i].id;
					var bid_name = ObjectArray[i].bid_name;
					var total_money = ObjectArray[i].total_money;
					var join_number = ObjectArray[i].join_number;
					var annualized_rate = ObjectArray[i].annualized_rate;
					//所有还款的金额
					var all_pay_back_money = ObjectArray[i].all_pay_back_money;
					var public_state_var = ObjectArray[i].public_state_var;
					var state ="";
					if(public_state_var == 1){
						state = "等待预定";
					}else if(public_state_var == 2 ){
						state = "预定中";
					}else if(public_state_var == 3 ){
						state = "等待支付";
					}else if(public_state_var == 4 ){
						state = "等待开标";
					}else if(public_state_var == 5 ){
						state = "开放中";
					}else if(public_state_var == 6 ){
						state = "收益中";
					}else if(public_state_var == 7 ){
						state = "已结束";
					}else{
						return;
					}
					
					str = str + "<tr><td class=\"td_2 \">" + 
								"<a style=\"color:#2ea7e0\" href=\""+get_fix_bid_info_by_id_path +id+"\">" + bid_name + "</a> </td>" +
								"<td class=\"td_2 td_xm\">" + total_money + "</td>" +
								" <td class=\"td_2\">"+ join_number + "人</td>" +
								"<td class=\"td_2\">"+ annualized_rate +"%</td>" +
								"<td class=\"td_2\">"+ all_pay_back_money +"</td>" +
								"<td class=\"td_2\">"+ state +"</td></tr>";
					
				}
				
				if(type == 3){
					$(L03_table_first_tr).after(str);
					$("#page_L03").html(AsyncPageDiv);
				}else if(type == 6){
					$(L06_table_first_tr).after(str);
					$("#page_L06").html(AsyncPageDiv);
				}else if(type == 12){
					$(L12_table_first_tr).after(str);
					$("#page_L12").html(AsyncPageDiv);
				}
				
			}
		}
	});
}
