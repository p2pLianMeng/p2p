  var path = "http://"+window.location.host;
  var fix_bid_order_path = path + "/user/financial/lfollinvest/order_lfoll_inveset.html";//联富宝预定
  var fix_bid_order_ajax_path = path + "/user/financial/lfollinvest_user_info/get_user_fix_bid_order_list.html";//Ajax异步 联富宝预定列表
  var fix_bid_exited_ajax_path = path + "/user/financial/lfollinvest_user_info/get_user_fix_bid_exited_list.html";//Ajax异步查询  当前用户联富宝退出列表
  var login_url = path+"/index/to_login.html";//
  var find_user_fix_bid_orded_list = path+"";//查询联富宝的预定信息
  var fix_bid_order_pay_path = path + "/user/financial/lfollinvest_user_info/user_pay_fix_bid_order_waitpaycny.html";//用户支付预定的联富宝
  var validate_is_login = path + "/user/financial/lfollinvest/validate_user_is_logined.html";//校验是否登陆
  var make_sure_advance_exit_fix_bid_path = path + "/user/financial/lfollinvest_user_info/user_advance_exit_lfoll_fix_bid.html";//确定提前退出联富宝
  	/*function  calculate_deposit(){
		var bid_money =  $("#bid_money").val();
		var deposit = Number(bid_money) * 0.01;
		$("#deposit").html(deposit);

  	}*/
  /**
   * 预订 联富宝输入框，输入内容的校验
   * @return
   */
  function  calculate_deposit(){
	  var can_used_cny = Number($("#can_used").html());//可用的金额
	  var bid_money =  Number($("#bid_money").val());//投资金额
	  var bid_money_var = $("#bid_money").val();
	  var least_money = $("#least_money").val();//最少投资金额
	  var userreg = /^[1-9]\d*$/;//判断正整数
	  	/**
	  	 * 判断是否有空格
	  	 */
	  	var arr = bid_money_var.split(" ");
  		if(arr.length > 1){
  			 $("#bid_money").val("");
			 $("#deposit").html("0.00");
			 prompt("show_message","请输入正确的金额");
			 return;
  		}
  		var arr_2 = bid_money_var.split(".");
  		if(arr_2.length > 1){
  			  $("#bid_money").val("");
	  		  $("#deposit").html("0.00");
			  prompt("show_message","请输入正确的金额");
			  return;
  		}
  		/**
  		 * 判断是否为正整数
  		 */
		  if(userreg.test(bid_money)){
			  //判断可用金额是否足够
			  if(can_used_cny < (bid_money * 0.01)){
				  $("#bid_money").val("");
				  $("#deposit").html("0.00");
				  prompt("show_message","可用金额不足");
				  return;
			  }else{
				  var deposit = Number(bid_money) * 0.01;
				  $("#deposit").html(deposit);
				  return;
			  }
		   }else{
		     $("#bid_money").val("");
		     $("#deposit").html("0.00");
		     return;
		   }   
		
		

  	}

  	/**
  		预定页面 ：提交预定的订单
  	**/
  	
  	function order_Lfoll_bid(validate_code){
  	  	var remaining = Number($("#remaining").html());
  	  	var can_used_cny = Number($("#can_used").html());
  	  	//least_money
  	  	var least_money = $("#least_money").val();
  		//ID
  	  	var order_id = $("#order_id").val();
  	  	//deposit
  	  	var deposit_str = $("#deposit").html();
  	  	var deposit = Number(deposit_str);
  	  	var bid_money = Number($("#bid_money").val());
  	  	if(bid_money == 0 || bid_money == null){
  	  		prompt("error_msg","请输入有效的金额");
	  		return;
  	  	}
  	  	if(bid_money < least_money){
  	  		prompt("error_msg","输入金额小于"+least_money);
  			return;
  	  	}
  	  	if(bid_money % least_money != 0){
  	  		prompt("error_msg","当前金额必须为"+least_money+"的整数倍!");
  	  		return;
  	  	}
  	  	if(validate_code == null || validate_code == ""){
	  		prompt("error_msg","验证码不能为空!");
	  		return;
	  	}
  	  	var i = true;
  	  	if(i){
  	  	  	i = false;
  	  	  	$.ajax({
	  	  	  	  	url :fix_bid_order_path,
	  	  	  		async :false,//同步
	  	  	  	  	type :"post",
	  	  	  		data :{"order_id":order_id,"deposit":deposit,"bid_money":bid_money,"least_money":least_money,"validate_code":validate_code},
	  	  	  	  	success:function(data){
	  	  	  			if(data =="非理财身份"){
	  	  	  				prompt("error_msg","非理财身份!");
	  	  	  			}else if(data == "NoLogined"){
							window.location.href = login_url;
	  	  	  	  	  	}
						if(data == 1){
							prompt("error_msg","非法提交!");
						}else if(data == 2){
							prompt("error_msg","订金为空!");
						}else if(data == 3){
							prompt("error_msg","预定的金额为空!");
						}else if(data == 4){
							prompt("error_msg","验证码不能为空!");
						}else if(data == 5){
							prompt("error_msg","验证码不正确!");
							$("#validate_code").val("");
							refresh_order_pay();
						}else if(data == 6){
							prompt("error_msg","当前金额必须为"+least_money+"的整数倍!");
						}else if(data == 7){
							prompt("error_msg","可用资金不足!");
						}else if(data == 8){
							prompt("error_msg","剩余可预定投资金额不足!");
						}else if(data == 9){
							
							var can_used_new = can_used_cny - deposit;
							$("#can_used").html(can_used_new);

							var remaining_new = remaining - bid_money;
							$("#remaining").html(remaining_new);
							close_order_pay();
							prompt_success("show_message","预定成功!","green");
						}else if(data == 10){
							prompt("error_msg","预定失败!");
						}else if(data == 11){
							prompt("error_msg","请求超时!");
						}else if(data == 12){
							prompt("error_msg","有贷款不能理财!");
						}
						i = true;
						return;
	  	  	  		}
  	  	  	  	});
  	  	}
  	  	
		
  	}
  	
  	/**
  	 * 
  	 * 查询用户预定中的联富宝(查询用户自己的)
  	 * @return
  	 */
  	function find_fix_bid_order_list(){
	  		//alert("预定中...");
	  		$.ajax({
	  			url:fix_bid_order_ajax_path,
	  			async:true,
	  			type:"post",
	  			success:function (data){
		  			var order_table = "#order_table";
	  				var order_table_first_tr = "#order_table_first_tr";
	  				$(order_table + " tr:not(:first)").empty(); 
	  				
		  			if(data!=null && data != ""){
		  				if(data == "NoLogined"){
		  					window.location.href = login_url;
		  	  	  	  	}
		  				var arrays = eval("("+data+")");
		  				var str = "";
		  				for ( var i in arrays) {
		  					
		  					var id = arrays[i].object_1;
		  					var bid_name = arrays[i].object_2;
		  					var bid_moeny = arrays[i].object_3;
		  					var alread_pay_bond = arrays[i].object_4;
		  					var wait_pay_amount = arrays[i].object_5;
		  					var pay_deadline = arrays[i].object_6;
		  					var is_pay = arrays[i].object_7;
		  					var is_pay_str ="";
		  					var nick_name = arrays[i].object_8;
		  					var annualized_rate = arrays[i].object_9;
		  					var closed_period = arrays[i].object_10;
		  					var can_used_cny = arrays[i].object_11;
		  					//alert(nick_name);
		  					if(is_pay == 0){
		  						is_pay_str = "<td><a href=\"###\" onclick=\"dump_order_pay('"+bid_name+"','" + id + "','"+nick_name+"','"+annualized_rate+"','"+closed_period+"','"+wait_pay_amount+"','"+can_used_cny+"')\" style=\"color:green\">未支付</></td> ";
		  					}
		  					if(is_pay == 1){
		  						is_pay_str = "<td>已支付</td> ";
		  					}
		  						
		  					
		  					var str = str + "<tr class=\"tr_2\">" +
			  								"<td>"+bid_name+"</td>" +
			  								"<td>"+bid_moeny+"</td>" +
			  								"<td>"+alread_pay_bond+"</td> " +
					  						"<td>"+wait_pay_amount+"</td>" +
					  						"<td>"+pay_deadline+"</td>" 
					  							+ is_pay_str +
					  						"</tr>";
						}
		  				
		  				$(order_table_first_tr).after(str);
		  				
		  			}else{
		  				var str = "<tr class=\"tr_2\"> <td colspan=\"6\" class=\"td_2\">暂无数据</td>";
		  				$(order_table_first_tr).after(str);
		  			}
	  			}
	  			
	  		});
  	}
  	
  	/**
  	 * ajax 异步查询退出中的联富宝
  	 * @return
  	 */
  	function find_fix_bid_exited(){
  		$.ajax({
  			url:fix_bid_exited_ajax_path,
  			async:true,
  			type:"post",
  			success:function (data){
	  			var exit_table = "#exit_table";
  				var exit_table_first_tr = "#exit_table_first_tr";
  				$(exit_table + " tr:not(:first)").empty(); 
	  			if(data!=null && data != "[]" && data != ""){
	  				if(data == "NoLogined"){
	  					window.location.href = login_url;
	  	  	  	  	}
	  				var arrays = eval("("+data+")");
	  				var str = "";
	  				for ( var i in arrays) {
	  					
	  					var id = arrays[i].object_1;
	  					var bid_name = arrays[i].object_2;
	  					var bid_moeny = arrays[i].object_3;
	  					var earned_incom = arrays[i].object_4;
	  					var annual_earning_real = arrays[i].object_5;
	  					var state = arrays[i].object_6;
	  					var state_var = "";
	  					if(state == 2){
	  						state_var = "<span style=\"color:red\">提前退出</span>";
	  					}else if(state == 1){
	  						state_var = "到期退出";
	  					}
	  					var exit_time_str = arrays[i].object_7;
	  					var str = str + "<tr class=\"tr_2\">" +
		  								"<td>"+ bid_name +"</td>" +
		  								"<td>"+ bid_moeny +"</td>" +
		  								"<td>"+ earned_incom +"</td> " +
				  						"<td>"+ annual_earning_real +"</td>" +
				  						"<td>"+ state_var +"</td>" +
				  						"<td>"+ exit_time_str + "</td>" +
				  						"</tr>";
					}
	  				$(exit_table_first_tr).after(str);
	  				
	  			}else{
	  				var str = "<tr class=\"tr_2\"> <td colspan=\"6\" class=\"td_2\">暂无数据</td>";
	  				$(exit_table_first_tr).after(str);
	  			}
  			}
  			
  		});
  		
  	}
  	
  	/**
  	 * 弹出 预定后 用户支付预定的联富宝 待付金额 的对话框
  	 * @param id
  	 * @param nick_name
  	 * @param annualized_rate
  	 * @param closed_period
  	 * @return
  	 */
  	function dump_order_pay(bid_name,id,nick_name,annualized_rate,closed_period,wait_pay_amount,can_used_cny){
  		$("#bid_name").html(bid_name);
  		$("#nick_name").html(nick_name);
  		$("#rate").html(annualized_rate+"%");
  		$("#close_period").html(closed_period + "个月");
  		$("#wait_pay_amount").html(wait_pay_amount + "元");
  		$("#can_used_cny").html(can_used_cny + "元");
  		$("#order_id").val(id);
  		$("#submit_pay_order").attr("onclick","pay_fix_bid_order_cny()");
  		$("#verificte_code").val("");
  		refresh_order_pay();
  		document.getElementById('loginDiv1').style.display='block';
  		document.getElementById('loginDiv2').style.display='block';
  	}
  	/**
  	 * 关闭div
  	 * @return
  	 */
  	function close_order_pay(){
  		document.getElementById('loginDiv1').style.display='none';
  		document.getElementById('loginDiv2').style.display='none';
  		
  	}
  	/**
  	 * 关闭确认退出联富宝
  	 * @return
  	 */
  	function close_div2_div3(){
  		document.getElementById('loginDiv3').style.display='none';
  		document.getElementById('loginDiv4').style.display='none';
  	}

  	/**
  	 * 获取验证码：
  	 * @return
  	 */
  	function refresh_order_pay(){
		var CheckCodeServlet=document.getElementById("CheckCodeServlet");
			CheckCodeServlet.src= path+"/code.jpg?id="+ Math.random();
	}
  	
  	function refresh_order_pay_div2(){
		var CheckCodeServlet=document.getElementById("CheckCodeServlet_2");
			CheckCodeServlet.src= path+"/code.jpg?id="+ Math.random();
	}
  	/**
  	 * 用户中心--支付预定待支付订单
  	 * @return
  	 */
  	function pay_fix_bid_order_cny(){
  		if(document.getElementById("check_box").checked == false){
  			prompt("error_msg","请先同意协议");
  			return;
  		}
  		//ID
  		var order_id = $("#order_id").val();
  		//联富宝名称
  		var bid_name = $("#bid_name").html();
  		//昵称
  		var nick_name = $("#nick_name").html();
  		//年化利率
  		var rate = $("#rate").html();
  		//投资周期
  		var close_period = $("#close_period").html();
  		//待付金额
  		var wait_pay_amount_var = $("#wait_pay_amount").html();
  		var wait_pay_amount = wait_pay_amount_var.substring(0,wait_pay_amount_var.length-1);
  		//alert("待付金额"+wait_pay_amount);
  		//可用资金
  		var can_used_cny_var = $("#can_used_cny").html();
  		var can_used_cny = can_used_cny_var.substring(0,can_used_cny_var.length - 1);
  		//验证码
  		var verificte_code = $("#verificte_code").val();
  		if(verificte_code == "" || verificte_code == null){
  			prompt("error_msg","请输入验证码");
  			return;
  		}
  		if(Number(can_used_cny) < Number(wait_pay_amount)){
  			prompt("error_msg","可用资金不足,请先充值!");
  			return
  		}
  		var i = true;
  		if(i){
  			i = false;
  			//alert("进入了Ajax...");
	  		$.ajax({
	  			url:fix_bid_order_pay_path,//用户支付预定联富宝的未支付金额
	  		    type:"post",
	  		    async:false,
	  			data:{"order_id":order_id,"bid_name":bid_name,"nick_name":nick_name,"rate":rate,"close_period":close_period,"wait_pay_amount":wait_pay_amount,"can_used_cny":can_used_cny,"verificte_code":verificte_code},
	  			success:function(data){
	  				if(data != null){
	  					if(data == 1){
	  						prompt("error_msg","订单ID为空");
	  					}else if(data == 2){
	  						prompt("error_msg","联富宝名称为空");
	  					}else if(data == 3){
	  						prompt("error_msg","昵称为空");
	  					}else if(data == 4){
	  						prompt("error_msg","年化利率为空");
	  					}else if(data == 5){
	  						prompt("error_msg","联富宝期限为空");
	  					}else if(data == 6){
	  						prompt("error_msg","可用的RMB为空");
	  					}else if(data == 7){
	  						prompt("error_msg","请输入验证码");
	  					}else if(data == 8){
	  						prompt("error_msg","验证码为空");
	  					}else if(data == 9){
	  						prompt("error_msg","验证码输入错误");
	  						$("#verificte_code").val("");
	  						refresh_order_pay();
	  					}else if(data == 10){
	  						prompt("error_msg","您已经支付了该预订单");
	  					}else if(data == 11){
	  						prompt("error_msg","待付金额不正确");
	  					}else if(data == 12){
	  						prompt("error_msg","可用资金不正确");
	  					}else if(data == 13){
	  						prompt("error_msg","可用资金不足");
	  					}else if(data == 14){
	  						prompt_success("error_msg","付款成功","green");
	  						close_order_pay();
	  						find_fix_bid_order_list();
	  					}else if(data == 15){
	  						prompt("error_msg","付款失败");
	  					}else if(data == 16){
	  						prompt("error_msg","联富宝名称有问题");
	  					}
	  					
	  				}
	  				
	  				i =  true;
	  			}
	  		
	  		});
  		}
  	}
  	
  	/**
  	 * 预定页面弹出确认预定 对话框
  	 * @return
  	 */
  	function dump_fix_bid_order(){
  		var bid_money_var = $("#bid_money").val();
  		var bid_money  = bid_money_var.replace(/[ ]/g,"");//去掉空格
  		var least_money = $("#least_money").val();//最小金额
  		//alert(bid_money);
  		if(bid_money == null || bid_money == "" ||bid_money <= 0){
  			prompt("show_message","请输入有效的金额");
	  		return;
  		}
  		if(bid_money == 0 || bid_money == null){
  	  		prompt("show_message","请输入有效的金额");
	  		return;
  	  	}
  	  	if(bid_money < least_money){
  	  		prompt("show_message","输入金额小于"+least_money);
  			return;
  	  	}
  	  	if(bid_money % least_money != 0){
  	  		prompt("show_message","当前金额必须为"+least_money+"的整数倍!");
  	  		return;
  	  	}
  	  	$("#join_money").html(bid_money);
	  	var should_pay_money = Number(bid_money) * 0.01;
		$("#should_pay_money").html(should_pay_money);
  	  	/**
  		 * 判断用户是否登陆
  		 */
  	  	$.ajax({
  	  		url:validate_is_login,
  	  		type:"post",
  	  		async:false,
  	  		success:function(data){
  	  			if(data != null){
  	  				if(data == "非理财身份"){
  	  					prompt("show_message","非理财账户，请先注册理财账户!");
  	  					return;
  	  				}else if(data == "NoLogined"){
  	  					window.location.href = login_url;
  	  					return;
  	  				}else{
  	  					$("#on_sumbit").attr("onclick","order_fix_bid()");//设置点击事件
  	  					refresh_order_pay();//生成验证码
  	  					document.getElementById('loginDiv1').style.display='block';
  	  					document.getElementById('loginDiv2').style.display='block';
  	  				}
  	  			}
  	  		}
  	  		
  	  		
  	  	});
  	}
  	
  	/**
  	 * 
  	 * @return
  	 */
  	function order_fix_bid(){
  		if(document.getElementById("check_box").checked == false){
  			prompt("error_msg","请先同意协议");
  			return;
  		}
  		//验证码
  		var validate_code = $("#validate_code").val();
  		if(validate_code == "" || validate_code == null){
  			prompt("error_msg","请输入验证码");
  			return;
  		}
  		order_Lfoll_bid(validate_code);
  	}
  	
  	//绑定 联富宝 持有中 点击全部事件
  $(document).ready(function(){
	  find_fix_bid_hold(1);
	  $("#lfoll_fix_bid_all").click(function(){
		  find_fix_bid_hold(1);//全部类型的  持有的联富宝
	  });
	  $("#lfoll_fix_bid_l03").click(function(){
		  find_fix_bid_hold(2);//持有的联富宝L03
	  });
	  $("#lfoll_fix_bid_l06").click(function(){
		  find_fix_bid_hold(3);//持有的联富宝L06
	  });
	  $("#lfoll_fix_bid_l12").click(function(){
		  find_fix_bid_hold(4);//持有的联富宝L12
	  });
	});
  
  /**
   * 查询所有的持有的联富宝
   */
  var find_lfoll_fix_bid_path_by_type = path + "/user/financial/lfollinvest_user_info/find_fix_bid_user_hold_by_type.html";
  
  /**
   * 
   * @return
   */
  function find_fix_bid_hold(type){
	  var type = type;
	  $.ajax({
		  url:find_lfoll_fix_bid_path_by_type,
		  type:"post",
		  data:{"type":type},
		  success:function(data){
			  var fix_bid_hold_all = "#fix_bid_hold_all";
			  var fix_bid_hold_all_first_tr = "#fix_bid_hold_all_first_tr";
			  $(fix_bid_hold_all + " tr:not(:first)").empty();
			  if(data != null && data!="[]"){
				if(data == "NoLogined"){
					window.location.href = login_url;
				}
				var arrays = eval("(" + data + ")");
				var str = "";
  				for ( var i in arrays) {
  					var id = arrays[i].id;
  					var bid_name = arrays[i].bid_name;
  					var annual_earning = changeTwoDecimal_f(Number(arrays[i].annual_earning) * Number(100));
  					var bid_money = arrays[i].bid_money;
  					var earned_incom = arrays[i].earned_incom;
  					var bid_num = arrays[i].bid_num;
  					var state = arrays[i].state;
  					var is_locked = arrays[i].is_locked;
  					var back_money = "";
  					var exit_fee = "";
  					var is_exit_state ="";
  					/**
  					 * 如果进入锁定期
  					 */
  					if(is_locked == 1){
  						back_money = arrays[i].back_money;//回收资金
  						exit_fee = arrays[i].exit_fee;//退出费用
  						is_exit_state = "<input type=\"button\" style=\"color:#fff; padding:0 10px;background:#ee5050;border:none\" onclick=\"alert_exit_fix_bid_div('"+bid_name+"','"+bid_money+"','"+back_money+"','"+exit_fee+"','"+id+"');\" value=\"提前退出\"/>"
  					}else if(is_locked == 2){
  						is_exit_state = "<span style=\"color:blue\">不可退出</span>";
  					}else{
  						earned_incom = "0.00";
  						is_exit_state = "<span style=\"color:green\">未进入锁定期</span>";
  					}
  					
  					var state_str = "";
  					if(state == 1){
  						state_str = "持有中"
  					}
  					
  					var str = str + "<tr class=\"tr_2\" id=\"fix_bid_hold_"+ id + "\" >" +
	  								"<td>"+ bid_name +"</td>" +
	  								"<td>"+ annual_earning +"%</td>" +
	  								"<td>"+ bid_money +"</td> " +
			  						"<td>"+ earned_incom +"</td>" +
			  						"<td>"+ bid_num +"</td>" +
			  						"<td>"+ state_str + "</td>" +
			  						"<td>"+is_exit_state+"</td>" +
			  						"</tr>";
				}
  				$(fix_bid_hold_all_first_tr).after(str);
			  }else{
				  var str = "<tr class=\"tr_2\"><td colspan=\"7\">暂无数据</td> </tr>";
	  			  $(fix_bid_hold_all_first_tr).after(str);
			  }
		  }
	  });
  }
  
  /**
   * js保留2位小数（强制）
   * @param floatvar
   * @return
   */
  function changeTwoDecimal_f(floatvar){
		var f_x = parseFloat(floatvar);
		if (isNaN(f_x)){
			alert('function:changeTwoDecimal->parameter error');
			return false;
		}
		var f_x = Math.round(f_x*100)/100;
		var s_x = f_x.toString();
		var pos_decimal = s_x.indexOf('.');
		if (pos_decimal < 0){
			pos_decimal = s_x.length;
			s_x += '.';
		}
		while (s_x.length <= pos_decimal + 2){
			s_x += '0';
		}
		return s_x;
 }
  /**
   * 点击确定 按钮 --- 退出联富宝 操作
   * @return
   */
 
  function make_sure_exit_fix_bid(id){
	  var money_pwd = $("#money_pwd").val();//资金密码
	  var exit_verificte_code = $("#exit_verificte_code").val();//验证码
	  var exie_fee = $("#exie_fee").html();//退出费用
	  if(money_pwd.length == 0){
		  prompt("exit_error_msg","资金密码不能为空!");
		  return;
	  }
	  if(exit_verificte_code == null || exit_verificte_code == "" || exit_verificte_code.length == 0){
		  prompt("exit_error_msg","请输入验证码!");
		  return;
	  }
	  if(id =="" || id <= 0){
		  prompt("exit_error_msg","该单ID异常!");
		  return;
	  }
	  var i = true;
	  if(i){
		  i = false;
		  $.ajax({
			  url:make_sure_advance_exit_fix_bid_path,
			  type:"post",
			  async:false,
			  data:{"money_pwd":money_pwd,"exit_verificte_code":exit_verificte_code,"exie_fee":exie_fee,"id":id},
			  success:function(data){
				  if(data == 1){
					  prompt("exit_error_msg","资金密码为空");
				  }else if(data == 2){
					  prompt("exit_error_msg","没有设置资金密码");
				  }else if(data == 3){
					  prompt("exit_error_msg","资金密码不正确");
				  }else if(data == 4){
					  prompt("exit_error_msg","验证码为空");
				  }else if(data == 5){
					  prompt("exit_error_msg","验证码不正确");
				  }else if(data == 6){
					  prompt("exit_error_msg","退出费用为空");
				  }else if(data == 7){
					  close_div2_div3();
					  showMessage(["提示","退出成功!"]);
					  $("#fix_bid_hold_"+id).remove();
				  }else if(data == 8){
					  prompt("exit_error_msg","退出失败");
				  }
				  i = true;
			  }
		  });
	  }
	  
  }
  
  
  /**
   * 弹出确认退出联富宝的对话框
   * @return
   */
  function alert_exit_fix_bid_div(bid_name,bid_money,back_money,exit_fee,id){
	  $("#exit_bid_name").html(bid_name);
	  $("#exit_bid_money").html(bid_money);
	  $("#exit_back_money").html(back_money);
	  $("#exie_fee").html(exit_fee);
	  
		/**
		 * 判断用户是否登陆
		 */
	  	$.ajax({
	  		url:validate_is_login,
	  		type:"post",
	  		async:false,
	  		success:function(data){
	  			if(data != null){
	  				if(data == "NoLogined"){
	  					window.location.href = login_url;
	  				}else{
	  					$("#submit_exit_fix_bid").attr("onclick","make_sure_exit_fix_bid('"+id+"')");//设置点击事件
	  					refresh_order_pay_div2();//刷新退出 联富宝 时的验证码
	  				  document.getElementById('loginDiv3').style.display='block';
	  				  document.getElementById('loginDiv4').style.display='block';
	  				}
	  			}
	  		}
	  		
	  		
	  	});
  }
