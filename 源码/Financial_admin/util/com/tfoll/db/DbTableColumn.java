package com.tfoll.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 线上数据库和本地数据库表字段对比
 * 
 * @author 曹正辉 CTO
 * 
 */
public class DbTableColumn {
	public static void main(String[] args) throws Exception {
		/**
		 * 指定为本地的数据库 -相比线上修改或者添加
		 */
		Set<String> localTableSet = getTableNameList("jdbc:mysql://192.168.1.9/tfoll_financial", "zjg", "123456", "tfoll_financial");
		/**
		 * 指定为线上的数据库
		 */
		Set<String> onlineTableSet = getTableNameList("jdbc:mysql://192.168.1.9/2.22", "zjg", "123456", "2.22");
		System.out.println(localTableSet);
		System.out.println(onlineTableSet);
		Map<String, Set<String>> local = new HashMap<String, Set<String>>();
		Map<String, Set<String>> online = new HashMap<String, Set<String>>();
		for (String tableName : localTableSet) {
			Set<String> set = getTableInfo("jdbc:mysql://192.168.1.9/tfoll_financial", "zjg", "123456", tableName);
			local.put(tableName, set);
		}
		for (String tableName : onlineTableSet) {
			Set<String> set = getTableInfo("jdbc:mysql://192.168.1.9/2.22", "zjg", "123456", tableName);
			online.put(tableName, set);
		}

		/**
		 * 对比规则
		 */
		// 1本地的少表-线上的表在本地的表集合里面找不到数据
		for (String key2 : onlineTableSet) {
			if (!localTableSet.contains(key2)) {
				System.out.println("本地少表" + key2);
			}

		}

		Set<String> commonTableNameSet = new HashSet<String>();
		// 2本地的多表-本地的表在线上的表集合里面找不到数据
		for (String key1 : localTableSet) {
			if (!onlineTableSet.contains(key1)) {
				System.out.println("本地多表" + key1);
			} else {
				// 共同具有的表
				commonTableNameSet.add(key1);
			}
		}

		/**
		 * 根据共同的表-比较出多的字段和少的字段
		 */
		for (String tableName : commonTableNameSet) {
			System.out.println();
			System.out.println("=================================" + tableName + "===============================");
			Set<String> localTableInfo = local.get(tableName);
			Set<String> onlineTableInfo = online.get(tableName);
			// 1本地的少字段-线上的字段在本地的表集合里面找不到数据
			for (String key2 : onlineTableInfo) {
				if (!localTableInfo.contains(key2)) {
					System.out.println("本地少字段" + key2);
				}

			}
			// 2本地的多字段-本地的字段在线上的表集合里面找不到数据
			for (String key1 : localTableInfo) {
				if (!onlineTableInfo.contains(key1)) {
					System.out.println("本地多字段" + key1);
				}
			}
			System.out.println("=================================" + tableName + "===============================");

		}
	}

	/**
	 * 根据数据库获得数据库表名
	 * 
	 * @throws SQLException
	 */
	public static Set<String> getTableNameList(String url, String username, String password, String dbName) throws SQLException {
		String sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + dbName + "'";
		Connection conn = DriverManager.getConnection(url, username, password);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		Set<String> set = new HashSet<String>();
		while (rs.next()) {
			set.add(rs.getString("TABLE_NAME").trim().toLowerCase());
		}
		conn.close();
		return set;
	}

	/**
	 * 根据数据表名获得数据库表字段
	 */
	public static Set<String> getTableInfo(String url, String username, String password, String tableName) throws SQLException {
		String sql = "select * from " + tableName;
		Connection conn = DriverManager.getConnection(url, username, password);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		Set<String> set = new HashSet<String>();
		ResultSetMetaData rsmd = rs.getMetaData();
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
			set.add(String.valueOf(rsmd.getColumnLabel(i)).trim().toLowerCase());
		}
		conn.close();
		return set;
	}

}
