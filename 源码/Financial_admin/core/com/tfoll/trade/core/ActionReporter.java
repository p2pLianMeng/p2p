package com.tfoll.trade.core;

import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.aop.InterceptorStack;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.upload.CosUpload;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

/**
 * Action信息显示器
 */
class ActionReporter {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	static final void doReport(Controller controller, Action action) {
		// 文件上传判断
		if (isCommonRequestReport(controller, action)) {
			System.out.println("文件上传中");

			printInfoOfFile();
			return;
		}

		if (controller == null || action == null) {
			System.out.println("controller == null || action == null");
			return;
		}
		StringBuilder sb = new StringBuilder("\n--------------------------------天富网 Action 信息报告--------------------------------");
		// 基本信息
		sb.append("\n\n----------信息当前时间:		").append(sdf.format(new Date())).append("    ").append("访问Url:").append(ActionContext.getRequest().getServletPath()).append("    ").append("Action导航Url:" + action.getActionKey());
		Class<? extends Controller> controllerClass = action.getControllerClass();
		sb.append("\n\n----------当前控制器:		").append(controllerClass.getName()).append("	即(").append(controllerClass.getSimpleName()).append(".java文件)");
		sb.append("\n\n----------调用方法:		").append(action.getMethodName());
		// 拦截器信息
		sb.append("\n\n----------该Action拦截器信息显示开始		");
		List<Interceptor> interceptorList = action.getInterceptorList();
		if (interceptorList.size() > 0) {

			for (int i = 0; i < interceptorList.size(); i++) {
				sb.append("\n----------拦截器[" + i + "]:		");
				Interceptor interceptor = interceptorList.get(i);
				if (interceptor != null) {
					Class<? extends Interceptor> interceptorClass = interceptor.getClass();
					if (interceptor instanceof InterceptorStack) {
						sb.append("拦截器栈:");
					}
					sb.append(interceptorClass.getName()).append("	即(").append(interceptorClass.getSimpleName()).append(".java)");
				}

			}
		} else {
			sb.append("\n----------该Action拦截器信息无,如果你在该Action中已经添加拦截器信息，建议你检查拦截器配置是否正确");
		}
		sb.append("\n----------该Action拦截器信息显示结束		");

		{
			// 参数信息
			sb.append("\n\n----------该Action参数信息显示开始		");
			HttpServletRequest request = ActionContext.getRequest();
			@SuppressWarnings("unchecked")
			Enumeration<String> e = request.getParameterNames();
			if (!e.hasMoreElements()) {
				sb.append("\n----------该Action没有提交任何参数		");
			} else {
				while (e.hasMoreElements()) {
					String name = e.nextElement();
					String[] values = request.getParameterValues(name);
					if (values.length == 1) {
						sb.append("\n----------只有一个参数:" + name).append("=").append(values[0]);
					} else {
						{// 一行显示
							sb.append("\n----------参数信息[一行]显示:" + name).append("[]={");
							for (int i = 0; i < values.length; i++) {
								if (i > 0) {
									sb.append("    ,");
								}
								sb.append(values[i]);
							}
							sb.append("}");
						}
						{// 多行显示
							sb.append("\n----------参数信息[多行]显示如下:" + name).append("[]");
							for (int i = 0; i < values.length; i++) {
								sb.append("\n----------" + name + "[" + i + "]		").append(values[i]);
							}
						}
					}
				}
			}

			sb.append("\n----------该Action参数信息显示结束		");
		}

		sb.append("\n--------------------------------天富网 Action 信息报告--------------------------------");
		System.out.println(sb.toString());
	}

	/**
	 * 判断是否是普通http请求
	 */
	static final boolean isCommonRequestReport(Controller controller, Action action) {
		String ContentType = ActionContext.getRequest().getContentType();
		if (ContentType != null && ContentType.toLowerCase().indexOf("multipart") != -1) {
			return true;
		} else {
			return false;
		}

	}

	static void printInfoOfFile() {
		CosUpload cosUpload = new CosUpload(ActionContext.getRequest(), Constants.characterEncoding, null, null);
		Map<String, String[]> map = cosUpload.getFieldsMap();
		Set<String> keys = map.keySet();
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			System.out.println("文件上传参数key:" + key + "value:" + (map.get(key))[0]);// 必须有值

		}
	}

}
