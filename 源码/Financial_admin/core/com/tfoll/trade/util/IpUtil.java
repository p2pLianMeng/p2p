package com.tfoll.trade.util;

import javax.servlet.http.HttpServletRequest;

public class IpUtil {

	public static String getRemoteAddrIP(HttpServletRequest request) {
		if (request.getHeader("x-forwarded-for") == null) {
			return request.getRemoteAddr();
		} else {
			return request.getHeader("x-forwarded-for");
		}
	}
}
