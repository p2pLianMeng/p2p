package com.tfoll.trade.activerecord.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ModelBuilder {

	/**
	 * 核心方法--需要控制返回数据的时候里面需要修改的字段个数为0
	 */
	@SuppressWarnings( { "unchecked" })
	public static final <T> List<T> build(ResultSet rs, Class<? extends Model> modelClass) throws SQLException {
		List<T> list = new ArrayList<T>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		String[] columnLabels = new String[columnCount + 1];
		int[] types = new int[columnCount + 1];
		buildColumnLabelsAndTypes(rsmd, columnLabels, types);
		while (rs.next()) {
			Model<?> me = null;
			try {
				me = modelClass.newInstance();
				me.clearModifiedSet();
			} catch (InstantiationException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			Map<String, Object> map = me.getM();
			for (int i = 1; i <= columnCount; i++) {
				Object value;
				// 下面是基本数据处理
				/**
				 * <pre>
				 * 约定
				 * Types.INTEGER-->int
				 * Types.BIGINT-->long
				 * Types.FLOAT-->float
				 * Types.VARCHAR-->string
				 * Types.BOOLEAN-->smalint
				 * </pre>
				 */
				if (types[i] == Types.INTEGER) {
					value = rs.getInt(i);
				} else if (types[i] == Types.BIGINT) {
					value = rs.getLong(i);
				} else if (types[i] == Types.FLOAT) {
					value = rs.getFloat(i);
				} else if (types[i] == Types.DOUBLE) {
					value = rs.getDouble(i);
				} else if (types[i] == Types.VARCHAR) {
					value = rs.getString(i);
				} else if (types[i] == Types.BOOLEAN) {
					value = rs.getInt(i) == 1 ? true : false;
					// 下面是三大数据处理
				} else if (types[i] < Types.BLOB) {
					value = rs.getObject(i);
				} else if (types[i] == Types.CLOB) {
					value = handleClob(rs.getClob(i));
				} else if (types[i] == Types.NCLOB) {
					value = handleClob(rs.getNClob(i));
				} else if (types[i] == Types.BLOB) {
					value = handleBlob(rs.getBlob(i));
				} else {
					value = rs.getObject(i);
				}

				map.put(columnLabels[i], value);
			}
			me.setMap(map);
			me.clearModifiedSet();
			list.add((T) me);
		}
		return list;
	}

	/**
	 * 使用引用设值方式进行注入值
	 */
	private static final void buildColumnLabelsAndTypes(ResultSetMetaData rsmd, String[] columnLabels, int[] types) throws SQLException {
		int length = columnLabels.length;
		for (int i = 1; i < length; i++) {
			columnLabels[i] = rsmd.getColumnLabel(i);
			types[i] = rsmd.getColumnType(i);
		}
	}

	public static byte[] handleBlob(Blob blob) throws SQLException {
		if (blob == null) {
			return null;
		}

		InputStream is = null;
		try {
			is = blob.getBinaryStream();
			byte[] data = new byte[(int) blob.length()];
			is.read(data);
			is.close();
			return data;
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}
	}

	public static String handleClob(Clob clob) throws SQLException {
		if (clob == null) {
			return null;
		}

		Reader reader = null;
		try {
			reader = clob.getCharacterStream();
			char[] data = new char[(int) clob.length()];
			reader.read(data);
			return new String(data);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
					reader = null;
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
