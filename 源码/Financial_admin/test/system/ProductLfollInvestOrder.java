package system;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.web.model.FixBidSystemOrderM;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class ProductLfollInvestOrder {

	/**
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws ParseException {
		// fix_bid_system_order
		FixBidSystemOrderM fix_bid_system_orderM = new FixBidSystemOrderM();

		Date date = new Date();
		String date_str = Model.Time.format(date);
		// System.out.println("------------date_str----------"+date_str);
		// Calendar calendar = Calendar.getInstance();
		String fix_bid_notice_start_time = "2014-10-29 09:00:00";// 预告开始时间
		int from_notice_to_reservation_hours = 48;// 从通告开始到到预定开始的小时数 48 h
		int from_reservation_start_to_reservation_end = 31;// 预定开始到预定结束的小时数；31 h
		int from_reservation_end_to_paystop = 21;// 从预定结束到支付截止时间 21 h
		int from_paystop_to_begain_order = 1;// 从支付截止时间到开放定投时间为1h
		int from_begain_order_to_lock_order = 67;// 从开放定投时间到进入锁定期的时间67h
		int from_lock_time_to_exit_time = 12;// 从锁定时间到退出时间开发的月数 12 M;

		Date date_start = Model.Time.parse(fix_bid_notice_start_time);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date_start);
		cal.add(Calendar.HOUR, 21);
		Date one_hour_later_date = cal.getTime();
		System.out.println("------1小时以后------" + Model.Time.format(one_hour_later_date));

		/*
		 * `bid_name` varchar(50) DEFAULT NULL COMMENT '联富定投名称', `total_money`
		 * int(11) DEFAULT NULL COMMENT '总发售金额', `sold_money` int(11) DEFAULT
		 * NULL COMMENT '已售出金额', `least_money` int(11) DEFAULT NULL COMMENT
		 * '起投金额', `annualized_rate` decimal(10,2) DEFAULT NULL COMMENT '年化利率',
		 * `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP, `add_time_long`
		 * bigint(20) DEFAULT NULL, `is_show` int(1) DEFAULT '0' COMMENT
		 * '当管理员在后台添加这个单子的时候是否这个单子在前台进行显示:0如果是0则不用挂出去，可能我们需要进行修改。[定投第一步-管理员完善单子的信息]0表示不显示，1表示对外公开给理财的用户,管理员手动设置该定投是否可以对外公开,管理员不能出现状态值由0变成1再变成0的情况,基本上如果管理员要发布一个定投则需要提前设置',
		 * `reserve_start_time` timestamp NULL DEFAULT NULL COMMENT '预定开始时间',
		 * `reserve_start_time_long` bigint(20) DEFAULT NULL COMMENT
		 * '预定开始时间的毫秒数', `reserve_end_time` timestamp NULL DEFAULT NULL COMMENT
		 * '预定结束时间', `reserve_end_time_long` bigint(20) DEFAULT NULL COMMENT
		 * '预定结束时间的毫秒数', `pay_end_time` timestamp NULL DEFAULT NULL COMMENT
		 * '支付截止时间', `pay_end_time_long` bigint(20) DEFAULT NULL,
		 * `invite_start_time` timestamp NULL DEFAULT NULL COMMENT '投标开始时间',
		 * `invite_start_time_long` bigint(20) DEFAULT NULL, `invite_end_time`
		 * timestamp NULL DEFAULT NULL COMMENT '投标结束时间', `invite_end_time_long`
		 * bigint(20) DEFAULT NULL, `closed_period` int(11) DEFAULT NULL COMMENT
		 * '封闭期（月）', `repayment_time` timestamp NULL DEFAULT NULL COMMENT
		 * '还款时间（退出定投时间）', `repayment_time_long` bigint(20) DEFAULT NULL,
		 * `repatment_interest` decimal(10,0) DEFAULT NULL COMMENT '还款利息',
		 */

	}

}
