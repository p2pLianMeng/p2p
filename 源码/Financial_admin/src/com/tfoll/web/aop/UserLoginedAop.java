package com.tfoll.web.aop;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.RootRender;

import javax.servlet.http.HttpSession;

/**
 * 判读普通用户是否登录
 * 
 */
public class UserLoginedAop implements Interceptor {

	public void doIt(ActionExecutor ae) {
		HttpSession session = ActionContext.getRequest().getSession();
		if (session != null && session.getAttribute("user") != null) {
			ae.invoke();
		} else {
			ActionContext.getRequest().setAttribute("loginLink", "1");
			ActionContext.setRender(new RootRender("/index/indexPage.jsp"));

		}

	}

}
