package com.tfoll.web.aop;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.RootRender;

import javax.servlet.http.HttpSession;

public class AdminAop implements Interceptor {

	public void doIt(ActionExecutor actionInvocation) {
		HttpSession session = ActionContext.getRequest().getSession();
		if (session != null && session.getAttribute("admin") != null) {
			actionInvocation.invoke();
		} else {

			ActionContext.setRender(new RootRender("/index/bak_login.jsp"));
		}
	}

}
