package com.tfoll.web.action.contract;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;

@ActionKey("/contract")
public class ContractManageAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入合同管理首页", last_update_author = "hx")
	public void contract_manage_index() {

		renderJsp("/contract/contract_manage.jsp");
		return;
	}
}
