package com.tfoll.web.action.admin;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.SysErrorInfoWaitingForDealingM;
import com.tfoll.web.util.page.PageDiv;

import java.util.List;

@ActionKey("/admin/admin")
public class SysErrorInfoWaitingForDealingAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "跳转查询等待处理系统错误信息页面", last_update_author = "zjb")
	@Before( { AdminAop.class })
	public void sys_error_info_waiting_for_dealing_index() {
		int pn = getParameterToInt("pn", 1);
		String count_sql = "select count(1) from sys_error_info_waiting_for_dealing where deal = 0 ";
		String sql = "select * from sys_error_info_waiting_for_dealing where deal = 0 ";

		long totalRow = Db.queryLong(count_sql);
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<SysErrorInfoWaitingForDealingM> sys_error_info_waiting_for_dealing_list = SysErrorInfoWaitingForDealingM.dao.find(page.creatLimitSql(sql));
			setAttribute("sys_error_info_waiting_for_dealing_list", sys_error_info_waiting_for_dealing_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/admin/admin/sys_error_info_waiting_for_dealing_index");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());

			System.out.println("so cool!!!" + pageDiv);
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/admin/admin/sys_error_info_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "处理等待处理系统错误信息", last_update_author = "zjb")
	@Before( { AdminAop.class })
	public void deal_sys_error_info_waiting_for_dealing() {
		int id = getParameterToInt("id", 0);
		if (id == 0) {
			renderText("err");
			return;
		}
		SysErrorInfoWaitingForDealingM sys_error_info_waiting_for_dealing = SysErrorInfoWaitingForDealingM.dao.findById(id);
		int deal = sys_error_info_waiting_for_dealing.get("deal");
		boolean deal_ok = false;
		if (deal == 0) {
			deal_ok = sys_error_info_waiting_for_dealing.set("deal", 1).update();
		} else {
			renderText("err");
			return;
		}
		if (deal_ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}
	}
}
