package com.tfoll.web.action.admin;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.util.Utils;

@ActionKey("/admin/question")
public class AdminReplyUser extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "添加管理员反馈信息", last_update_author = "")
	public void question_relply_index() {
		renderJsp("/admin/question/tiwen.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "添加管理员反馈信息", last_update_author = "")
	public void question_relply() {
		String tw_id = getParameter("tw_id"); // 问题ID
		String msg_type = getParameter("msg_type"); // 问题类型
		String msg_content = getParameter("msg_content"); // 问题描述
		if (Utils.isNullOrEmptyString(msg_content)) {
			setAttribute("tips", "问题回复不能为空");
			renderJsp("/admin/question/tiwen.jsp");
			return;
		} else {

		}
	}

}
