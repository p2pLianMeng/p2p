package com.tfoll.web.action.user.about_us;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.AboutInfoM;
import com.tfoll.web.model.AboutTypeM;
import com.tfoll.web.util.Utils;

import java.util.ArrayList;
import java.util.List;

@ActionKey("/user/about_us")
public class AboutUsAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "关于我们管理", last_update_author = "hx")
	public void about_us_manage() {
		renderJsp("/user/about_us/about_us_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "跳转到关于我们页面", last_update_author = "hx")
	public void add_about_us_page() {
		String sql = "SELECT MAX(sort) FROM about_type";
		int max_sort = Db.queryInt(sql);
		setAttribute("sort", max_sort + 1);
		renderJsp("/user/about_us/add_about_type.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "保存类型", last_update_author = "hx")
	public void save_type() {
		final int id = getParameterToInt("id", 0);
		final String type_name = getParameter("type_name");
		final String url = getParameter("url");
		final String tag = getParameter("tag");
		final int is_effect = getParameterToInt("status");
		final int sort = getParameterToInt("sort");

		if (Utils.isNullOrEmptyString(type_name)) {
			renderText("1");
			return;
		}
		if (Utils.isNullOrEmptyString(url)) {
			renderText("2");
			return;
		}
		boolean is_ok = false;
		if (id > 0) {
			is_ok = Db.tx(new IAtomic() {
				public boolean transactionProcessing() throws Exception {
					AboutTypeM aboutTypeM = AboutTypeM.dao.findById(id);
					aboutTypeM.set("type_name", type_name);
					aboutTypeM.set("url", url);
					aboutTypeM.set("tag", tag);
					aboutTypeM.set("is_effect", is_effect);
					aboutTypeM.set("sort", sort);
					boolean bool1 = aboutTypeM.update();
					if (bool1) {
						return true;
					} else
						return false;
				}
			});
		} else {
			is_ok = Db.tx(new IAtomic() {
				public boolean transactionProcessing() throws Exception {
					AboutTypeM aboutTypeM = new AboutTypeM();
					aboutTypeM.set("type_name", type_name);
					aboutTypeM.set("url", url);
					aboutTypeM.set("tag", tag);
					aboutTypeM.set("is_effect", is_effect);
					aboutTypeM.set("sort", sort);
					boolean bool1 = aboutTypeM.save();
					if (bool1) {
						return true;
					} else
						return false;

				}
			});
		}

		if (is_ok) {
			renderText("3");
			return;
		} else {
			renderText("4");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询关于类型", last_update_author = "hx")
	public void find_all_about_type() {
		String sql = "select * from about_type order by id desc";
		List<AboutTypeM> aboutTypeList = AboutTypeM.dao.find(sql);
		if (Utils.isHasData(aboutTypeList)) {
			setAttribute("aboutTypeList", aboutTypeList);
		} else {
			setAttribute("tips", "暂无数据");
		}
		renderJsp("/user/about_us/about_us_manage.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "跳转到添加新闻", last_update_author = "hx")
	public void goto_add_article_page() {
		String sql = "select id ,type_name from about_type order by id desc";
		List<AboutTypeM> aboutTypeList = AboutTypeM.dao.find(sql);
		if (Utils.isHasData(aboutTypeList)) {
			setAttribute("aboutTypeList", aboutTypeList);
		} else {
			setAttribute("tips", "暂无数据");
		}
		renderJsp("/user/about_us/article_add_index.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "添加新闻", last_update_author = "hx")
	public void save_article_info() {
		final int id = getParameterToInt("id", 0);

		final Integer type_id = getParameterToInt("type_id");// 类型ID
		final String info_title = getParameter("info_title");// 信息标题

		final String author = getParameter("author");// 发布作者
		final String key_word = getParameter("key_word");// 信息的关键字

		final String description = getParameter("description");// 信息描述
		final String content = getParameter("content");// 信息内容
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				boolean bool = false;
				if (id == 0) {
					AboutInfoM about_info = new AboutInfoM();
					bool = about_info.set("type_id", type_id).set("info_title", info_title).set("author", author).set("description", description).set("content", content).set("key_word", key_word).save();
				} else {
					AboutInfoM about_info = AboutInfoM.dao.findById(id);
					about_info.set("type_id", type_id);
					about_info.set("info_title", info_title);
					about_info.set("author", author);
					about_info.set("key_word", key_word);
					about_info.set("description", description);
					about_info.set("content", content);
					bool = about_info.update();
				}

				if (bool) {
					return true;
				} else {
					return false;
				}

			}
		});
		if (is_ok) {
			String sql = "select id ,type_name from about_type order by id desc";
			List<AboutTypeM> aboutTypeList = AboutTypeM.dao.find(sql);
			if (Utils.isHasData(aboutTypeList)) {
				setAttribute("aboutTypeList", aboutTypeList);
				setAttribute("tips", "添加成功!");
			}
		} else {
			setAttribute("tips", "添加失败!");
		}
		renderJsp("/user/about_us/article_add_index.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "关于详情", last_update_author = "hx")
	public void get_about_detail() {
		Integer id = getParameterToInt("id");
		AboutTypeM aboutType = AboutTypeM.dao.findById(id);
		if (aboutType != null) {
			setAttribute("aboutType", aboutType);
		}
		renderJsp("/user/about_us/about_info_detail.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "新闻管理", last_update_author = "hx")
	public void go_to_new_manage_page() {
		String sql = "select id,type_name from about_type order by sort asc";
		List<AboutTypeM> about_type_list = AboutTypeM.dao.find(sql);
		setAttribute("about_type_list", about_type_list);
		renderJsp("/user/about_us/news_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询信息列表", last_update_author = "hx")
	public void get_all_about_info_list() {
		int type_id = getParameterToInt("type_id", 0);

		String list_sql = "select * from about_info where 1 = 1";
		List<Object> params = new ArrayList<Object>();
		if (type_id != 0) {
			list_sql = list_sql + " and type_id = ?";
			params.add(type_id);
		}
		List<AboutInfoM> about_info_list = AboutInfoM.dao.find(list_sql, params.toArray());
		setAttribute("about_info_list", about_info_list);

		String sql = "select id,type_name from about_type order by sort asc";
		List<AboutTypeM> about_type_list = AboutTypeM.dao.find(sql);
		setAttribute("about_type_list", about_type_list);
		renderJsp("/user/about_us/news_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询信息列表", last_update_author = "hx")
	public void get_about_info_detail() {
		int id = getParameterToInt("id");
		AboutInfoM about_info = AboutInfoM.dao.findById(id);
		setAttribute("about_info", about_info);

		String sql = "select id,type_name from about_type order by sort asc";
		List<AboutTypeM> aboutTypeList = AboutTypeM.dao.find(sql);
		setAttribute("aboutTypeList", aboutTypeList);
		renderJsp("/user/about_us/article_editor.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "删除选择的网站公告或最新资讯", last_update_author = "hx")
	public void delete_about_info_by_id() {
		int id = getParameterToInt("id");
		// AboutInfoM about_info = AboutInfoM.dao.findById(id);

		boolean bool = AboutInfoM.dao.deleteById(id);
		if (bool) {
			renderText("1");
			return;
		} else {
			renderText("2");
			return;
		}

	}
}
