package com.tfoll.web.action.user.loan_order;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ActionKey("/user/loan_order")
public class GatherAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入正在从凑集列表", last_update_author = "曹正辉")
	public void loan_gather_order_index() {
		setSessionAttribute("real_name", null);
		setSessionAttribute("borrow_duration", -1);
		setSessionAttribute("annulized_rate_int", -1);
		setSessionAttribute("borrow_all_money_level", -1);
		setSessionAttribute("gather_progress_level", -1);
		setSessionAttribute("gather_state", -1);

		setSessionAttribute("pass_start_time", null);
		setSessionAttribute("pass_end_time", null);

		setAttribute("real_name", null);
		setAttribute("borrow_duration", -1);
		setAttribute("borrow_duration_day", -1);
		setAttribute("annulized_rate_int", -1);
		setAttribute("borrow_all_money_level", -1);
		setAttribute("gather_progress_level", -1);
		setAttribute("gather_state", -1);

		setAttribute("pass_start_time", null);
		setAttribute("pass_end_time", null);

		renderJsp("/user/loan_order/gather_order/gather_order_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入正在从凑集列表", last_update_author = "曹正辉")
	public void loan_gather_order_query() {
		String real_name = getParameter("real_name");
		int borrow_type = getParameterToInt("borrow_type", -1);
		int borrow_duration = getParameterToInt("borrow_duration", -1);
		int borrow_duration_day = getParameterToInt("borrow_duration_day", -1);
		int annulized_rate_int = getParameterToInt("annulized_rate_int", -1);
		int borrow_all_money_level = getParameterToInt("borrow_all_money_level", -1);
		int gather_progress_level = getParameterToInt("gather_progress_level", -1);
		int gather_state = getParameterToInt("gather_state", -1);

		String pass_start_time = getParameter("pass_start_time", null);
		String pass_end_time = getParameter("pass_end_time", null);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("borrow_type", borrow_type);
		setSessionAttribute("borrow_duration", borrow_duration);
		setSessionAttribute("borrow_duration_day", borrow_duration_day);
		setSessionAttribute("annulized_rate_int", annulized_rate_int);
		setSessionAttribute("borrow_all_money_level", borrow_all_money_level);
		setSessionAttribute("gather_progress_level", gather_progress_level);
		setSessionAttribute("gather_state", gather_state);

		setSessionAttribute("pass_start_time", pass_start_time);
		setSessionAttribute("pass_end_time", pass_end_time);

		setAttribute("real_name", real_name);
		setAttribute("borrow_type", borrow_type);
		setAttribute("borrow_duration", borrow_duration);
		setAttribute("borrow_duration_day", borrow_duration_day);
		setAttribute("annulized_rate_int", annulized_rate_int);
		setAttribute("borrow_all_money_level", borrow_all_money_level);
		setAttribute("gather_progress_level", gather_progress_level);
		setAttribute("gather_state", gather_state);

		setAttribute("pass_start_time", pass_start_time);
		setAttribute("pass_end_time", pass_end_time);

		StringBuilder sql_select = new StringBuilder();
		StringBuilder sql_count = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql_select.append("SELECT u.real_name,gmo.* FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		sql_count.append("SELECT count(1) FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_select.append(" and u.real_name=? ");
			sql_count.append(" and u.real_name=? ");
			params.add(real_name);
		}
		if (borrow_type != -1) {
			sql_select.append(" and gmo.borrow_type=? ");
			sql_count.append(" and gmo.borrow_type=? ");
			params.add(borrow_type);
		}
		if (borrow_duration != -1) {
			sql_select.append(" and gmo.borrow_duration=? ");
			sql_count.append(" and gmo.borrow_duration=? ");
			params.add(borrow_duration);
		}
		if (borrow_duration_day != -1) {
			sql_select.append(" and gmo.borrow_duration_day=? ");
			sql_count.append(" and gmo.borrow_duration_day=? ");
			params.add(borrow_duration_day);
		}
		if (annulized_rate_int != -1) {
			sql_select.append(" and gmo.annulized_rate_int=? ");
			sql_count.append(" and gmo.annulized_rate_int=? ");
			params.add(annulized_rate_int);
		}
		if (borrow_all_money_level != -1) {
			sql_select.append(" and gmo.borrow_all_money_level=? ");
			sql_count.append(" and gmo.borrow_all_money_level=? ");
			params.add(borrow_all_money_level);
		}
		if (gather_progress_level != -1) {
			sql_select.append(" and gmo.gather_progress_level=? ");
			sql_count.append(" and gmo.gather_progress_level=? ");
			params.add(gather_progress_level);
		}
		if (gather_state != -1) {
			sql_select.append(" and gmo.gather_state = ? ");
			sql_count.append(" and gmo.gather_state = ? ");
			params.add(gather_state);
		}
		if (Utils.isNotNullAndNotEmptyString(pass_start_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(pass_start_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and gmo.add_time > ? ");
			sql_count.append(" and gmo.add_time > ? ");
			params.add(date);
		}
		if (Utils.isNotNullAndNotEmptyString(pass_end_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(pass_end_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and gmo.add_time < ? ");
			sql_count.append(" and gmo.add_time < ? ");
			params.add(date);
		}
		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, 1);

			List<Record> gather_money_order_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(gather_money_order_list)) {
				setAttribute("gather_money_order_list", gather_money_order_list);
			}

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/loan_gather_order_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/gather_order/gather_order_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入正在从凑集列表", last_update_author = "曹正辉")
	public void loan_gather_order_page() {
		int pn = getParameterToInt("pn", 1);
		String real_name = getParameter("real_name");
		int borrow_duration = getParameterToInt("borrow_duration", 0);
		int annulized_rate_int = getParameterToInt("annulized_rate_int", 0);
		int borrow_all_money_level = getParameterToInt("borrow_all_money_level", 0);
		int gather_progress_level = getParameterToInt("gather_progress_level", 0);
		int gather_state = getParameterToInt("gather_state", 0);

		String pass_start_time = getParameter("pass_start_time", "");
		String pass_end_time = getParameter("pass_end_time", "");

		if ("".equals(real_name) && pn > 1) {
			real_name = getSessionAttribute("real_name");
		}
		if (borrow_duration == 0 && pn > 1) {
			borrow_duration = getSessionAttribute("borrow_duration");
		}
		if (annulized_rate_int == 0 && pn > 1) {
			annulized_rate_int = getSessionAttribute("annulized_rate_int");
		}
		if (borrow_all_money_level == 0 && pn > 1) {
			borrow_all_money_level = getSessionAttribute("borrow_all_money_level");
		}
		if (gather_progress_level == 0 && pn > 1) {
			gather_progress_level = getSessionAttribute("gather_progress_level");
		}
		if (gather_state == 0 && pn > 1) {
			gather_state = getSessionAttribute("gather_state");
		}

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("borrow_duration", borrow_duration);
		setSessionAttribute("annulized_rate_int", annulized_rate_int);
		setSessionAttribute("borrow_all_money_level", borrow_all_money_level);
		setSessionAttribute("gather_progress_level", gather_progress_level);
		setSessionAttribute("gather_state", gather_state);

		setSessionAttribute("pass_start_time", pass_start_time);
		setSessionAttribute("pass_end_time", pass_end_time);

		setAttribute("real_name", real_name);
		setAttribute("borrow_duration", borrow_duration);
		setAttribute("annulized_rate_int", annulized_rate_int);
		setAttribute("borrow_all_money_level", borrow_all_money_level);
		setAttribute("gather_progress_level", gather_progress_level);
		setAttribute("gather_state", gather_state);

		setAttribute("pass_start_time", pass_start_time);
		setAttribute("pass_end_time", pass_end_time);

		StringBuilder sql_select = new StringBuilder();
		StringBuilder sql_count = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql_select.append("SELECT u.real_name,gmo.* FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		sql_count.append("SELECT count(1) FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_select.append(" and u.real_name=? ");
			sql_count.append(" and u.real_name=? ");
			params.add(real_name);
		}
		if (borrow_duration != -1) {
			sql_select.append(" and gmo.borrow_duration=? ");
			sql_count.append(" and gmo.borrow_duration=? ");
			params.add(borrow_duration);
		}
		if (annulized_rate_int != -1) {
			sql_select.append(" and gmo.annulized_rate_int=? ");
			sql_count.append(" and gmo.annulized_rate_int=? ");
			params.add(annulized_rate_int);
		}
		if (borrow_all_money_level != -1) {
			sql_select.append(" and gmo.borrow_all_money_level=? ");
			sql_count.append(" and gmo.borrow_all_money_level=? ");
			params.add(borrow_all_money_level);
		}
		if (gather_progress_level != -1) {
			sql_select.append(" and gmo.gather_progress_level=? ");
			sql_count.append(" and gmo.gather_progress_level=? ");
			params.add(gather_progress_level);
		}
		if (gather_state != -1) {
			sql_select.append(" and gmo.gather_state = ? ");
			sql_count.append(" and gmo.gather_state = ? ");
			params.add(gather_state);
		}
		if (Utils.isNotNullAndNotEmptyString(pass_start_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(pass_start_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and gmo.add_time > ? ");
			sql_count.append(" and gmo.add_time > ? ");
			params.add(date);
		}
		if (Utils.isNotNullAndNotEmptyString(pass_end_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(pass_end_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and gmo.add_time < ? ");
			sql_count.append(" and gmo.add_time < ? ");
			params.add(date);
		}
		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> gather_money_order_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(gather_money_order_list)) {
				setAttribute("gather_money_order_list", gather_money_order_list);
			}

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/loan_gather_order_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/gather_order/gather_order_list.jsp");
		return;
	}
}
