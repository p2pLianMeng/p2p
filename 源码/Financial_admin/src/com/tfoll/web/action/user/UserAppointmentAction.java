package com.tfoll.web.action.user;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.UserAppointmentM;
import com.tfoll.web.util.page.PageDiv;

import java.util.List;

@ActionKey("/user/user_appointment")
public class UserAppointmentAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(AdminAop.class)
	@Function(for_people = "登录", function_description = "用户预约", last_update_author = "zjb")
	public void query_appointment() {
		long pn = getParameterToInt("pn", 1);
		String count_sql = "select count(1) from user_appointment where is_deal = 0 ";
		String sql = "select * from user_appointment where is_deal = 0 ";

		long totalRow = Db.queryLong(count_sql);
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<UserAppointmentM> user_appointment_list = UserAppointmentM.dao.find(page.creatLimitSql(sql));
			setAttribute("user_appointment_list", user_appointment_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/user_appointment/query_appointment");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			String tips = "暂无人预约";
			setAttribute("tips", tips);
		}

		renderJsp("/user/user_appointment_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "处理用户预约", last_update_author = "zjb")
	@Before( { AdminAop.class })
	public void deal_appointment() {
		int id = getParameterToInt("id", 0);
		System.out.println("-------------------------------------------------------" + id);
		if (id == 0) {
			renderText("err");
			return;
		}
		System.out.println(id);
		UserAppointmentM user_appointment = UserAppointmentM.dao.findById(id);
		int deal = user_appointment.get("is_deal");
		boolean deal_ok = false;
		if (deal == 0) {
			deal_ok = user_appointment.set("is_deal", 1).update();
		} else {
			renderText("err");
			return;
		}
		if (deal_ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}
	}
}
