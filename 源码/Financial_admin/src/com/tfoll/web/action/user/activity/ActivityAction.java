package com.tfoll.web.action.user.activity;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.BootStraoDiv;

import java.util.ArrayList;
import java.util.List;

@ActionKey("/user/activity")
public class ActivityAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "跳转到活动查询页面", last_update_author = "zjb")
	public void to_activity_info() {

		renderJsp("/user/activity/activity_info.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "活动查询", last_update_author = "zjb")
	public void activity_query() {
		int pn = getParameterToInt("pn", 1);

		String real_name = getParameter("real_name");
		String phone = getParameter("phone");
		String type = getParameter("type");

		System.out.println("type+   ------" + type);

		StringBuilder count_sql = new StringBuilder();
		StringBuilder sql = new StringBuilder();

		count_sql.append("select count(1) FROM user_info u,_____activity___reward_record a WHERE u.id = a.user_id ");
		sql.append("SELECT u.id as user_id,u.real_name,u.phone,a.*  FROM user_info u,_____activity___reward_record a WHERE u.id = a.user_id ");
		List<Object> params = new ArrayList<Object>();

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			count_sql.append(" and u.real_name = ? ");
			sql.append(" and u.real_name = ?  ");
			params.add(real_name);
		}

		if (Utils.isNotNullAndNotEmptyString(phone)) {
			count_sql.append(" and u.phone = ? ");
			sql.append(" and u.phone = ?  ");
			params.add(phone);
		}

		if (Utils.isNotNullAndNotEmptyString(type)) {
			count_sql.append(" and a.type = ? ");
			sql.append(" and a.type = ?  ");
			params.add(type);
		}

		Long totalRow = Db.queryLong(count_sql.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> activity_list = Db.find(page.creatLimitSql(sql.toString()), params.toArray());

			if (Utils.isHasData(activity_list)) {
				setAttribute("activity_list", activity_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/activity/activity_query");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());

			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/activity/activity_info.jsp");
		return;
	}

}
