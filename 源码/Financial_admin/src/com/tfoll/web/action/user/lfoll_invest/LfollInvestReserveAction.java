package com.tfoll.web.action.user.lfoll_invest;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.FixBidUserExitM;
import com.tfoll.web.model.FixBidUserHoldM;
import com.tfoll.web.model.FixBidUserOrderM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.util.ArrayList;
import java.util.List;

/**
 * 联富宝预约管理
 * 
 * @author tf
 * 
 */
@ActionKey("/user/lfoll_invest/lfoll_invest_reserve")
public class LfollInvestReserveAction extends Controller {

	/**
	 * 查询预约联富宝的列表
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "联富宝预约列表", last_update_author = "hx")
	public void get_lfoll_invest_reserve_list() {
		int pn = getParameterToInt("pn", 1);

		String real_name = getParameter("real_name");
		String bid_name = getParameter("bid_name");
		// Integer transaction_state = getParameterToInt("transaction_state");//
		// 1为默认为未全额付款
		Integer is_pay = getParameterToInt("is_pay", 0);// 是否付款
		String order_start_time = getParameter("order_start_time");
		String order_end_time = getParameter("order_end_time");

		if (real_name == null) {
			real_name = getSessionAttribute("real_name");
		} else if (bid_name == null) {
			bid_name = getSessionAttribute("bid_name");
		} else if (order_start_time == null) {
			order_start_time = getSessionAttribute("order_start_time");
		} else if (order_end_time == null) {
			order_end_time = getSessionAttribute("order_end_time");
		} else if (is_pay == null) {
			is_pay = getSessionAttribute("is_pay");
		}
		setSessionAttribute("real_name", real_name);
		setSessionAttribute("bid_name", bid_name);
		setSessionAttribute("is_pay", is_pay);
		setSessionAttribute("order_start_time", order_start_time);
		setSessionAttribute("order_end_time", order_end_time);

		setAttribute("real_name", real_name);
		setAttribute("bid_name", bid_name);
		setAttribute("is_pay", is_pay);
		setAttribute("order_start_time", order_start_time);
		setAttribute("order_end_time", order_end_time);

		String reserve_count = "SELECT count(1) FROM fix_bid_user_order t1 where 1=1 ";

		String reserve_list = "SELECT * FROM fix_bid_user_order t1 where 1=1 ";
		StringBuffer buffer_reserve_count = new StringBuffer(reserve_count);
		StringBuffer buffer_reserve_list = new StringBuffer(reserve_list);
		List<Object> params = new ArrayList<Object>();

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			buffer_reserve_count.append(" and t1.real_name like ? ");
			buffer_reserve_list.append(" and t1.real_name like ? ");
			params.add("%" + real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(bid_name)) {
			buffer_reserve_count.append(" and t1.bid_name like ? ");
			buffer_reserve_list.append(" and t1.bid_name like ? ");
			params.add("%" + bid_name + "%");
		}
		if (is_pay != null && is_pay != -1) {
			buffer_reserve_count.append(" and t1.is_pay = ?");
			buffer_reserve_list.append(" and t1.is_pay = ?");
			params.add(is_pay);
		}
		if (Utils.isNotNullAndNotEmptyString(order_start_time)) {
			buffer_reserve_count.append(" and t1.add_time >= ? ");
			buffer_reserve_list.append(" and t1.add_time >= ?");
			params.add(order_start_time);
		}
		if (Utils.isNotNullAndNotEmptyString(order_end_time)) {
			buffer_reserve_count.append(" and t1.add_time <= ? ");
			buffer_reserve_list.append(" and t1.add_time <= ?");
			params.add(order_end_time);
		}
		// if (transaction_state > 0) {
		// buffer_reserve_count.append(" and t2.transaction_state = ?");
		// buffer_reserve_list.append(" and t2.transaction_state = ?");
		// params.add(transaction_state);
		// }
		buffer_reserve_count.append(" ORDER BY id DESC");
		buffer_reserve_list.append(" ORDER BY id DESC");

		Long totalRows = Db.queryLong(buffer_reserve_count.toString(), params.toArray());

		if (totalRows != null && totalRows > 0) {
			Page page = new Page(totalRows, pn);
			List<FixBidUserOrderM> lfoll_invest_reserve_list = FixBidUserOrderM.dao.find(page.creatLimitSql(buffer_reserve_list.toString()), params.toArray());
			setAttribute("lfoll_invest_reserve_list", lfoll_invest_reserve_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/lfoll_invest/lfoll_invest_reserve/get_lfoll_invest_reserve_list");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}

		renderJsp("/user/lfoll_invest/lfoll_invest_reseve_manage.jsp");
		return;

	}

	/**
	 * 查询加入联富宝的记录
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "联富宝加入列表", last_update_author = "hx")
	public void get_lfoll_invest_join_list() {
		int pn = getParameterToInt("pn", 1);

		String real_name = getParameter("real_name");
		String bid_name = getParameter("bid_name");
		Integer state = getParameterToInt("state", 1);// 0 ： 持有后退出 (要给违约金）；1
		// ：持有中

		String join_start_time = getParameter("join_start_time");// 加入开始时间
		String join_end_time = getParameter("join_end_time");// 加入结束时间

		if (real_name == null) {
			real_name = getSessionAttribute("real_name");
		} else if (bid_name == null) {
			bid_name = getSessionAttribute("bid_name");
		} else if (join_start_time == null) {
			join_start_time = getSessionAttribute("join_start_time");
		} else if (join_end_time == null) {
			join_end_time = getSessionAttribute("join_end_time");
		} else if (state == null) {
			state = getSessionAttribute("state");
		}
		setSessionAttribute("real_name", real_name);
		setSessionAttribute("bid_name", bid_name);
		setSessionAttribute("state", state);
		setSessionAttribute("join_start_time", join_start_time);
		setSessionAttribute("join_end_time", join_end_time);

		setAttribute("real_name", real_name);
		setAttribute("bid_name", bid_name);
		setAttribute("state", state);
		setAttribute("join_start_time", join_start_time);
		setAttribute("join_end_time", join_end_time);

		String reserve_count = "SELECT count(1) FROM fix_bid_user_hold t1 where 1=1 ";

		String reserve_list = "SELECT * FROM fix_bid_user_hold t1 where 1=1 ";
		StringBuffer buffer_reserve_count = new StringBuffer(reserve_count);
		StringBuffer buffer_reserve_list = new StringBuffer(reserve_list);
		List<Object> params = new ArrayList<Object>();

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			buffer_reserve_count.append(" and t1.real_name like ? ");
			buffer_reserve_list.append(" and t1.real_name like ? ");
			params.add("%" + real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(bid_name)) {
			buffer_reserve_count.append(" and t1.bid_name like ? ");
			buffer_reserve_list.append(" and t1.bid_name like ? ");
			params.add("%" + bid_name + "%");
		}
		if (state != null && state != -1) {
			buffer_reserve_count.append(" and t1.state = ?");
			buffer_reserve_list.append(" and t1.state = ?");
			params.add(state);
		}
		if (Utils.isNotNullAndNotEmptyString(join_start_time)) {
			buffer_reserve_count.append(" and t1.add_time >= ? ");
			buffer_reserve_list.append(" and t1.add_time >= ?");
			params.add(join_start_time);
		}
		if (Utils.isNotNullAndNotEmptyString(join_end_time)) {
			buffer_reserve_count.append(" and t1.add_time <= ? ");
			buffer_reserve_list.append(" and t1.add_time <= ?");
			params.add(join_end_time);
		}
		// if (transaction_state > 0) {
		// buffer_reserve_count.append(" and t2.transaction_state = ?");
		// buffer_reserve_list.append(" and t2.transaction_state = ?");
		// params.add(transaction_state);
		// }
		buffer_reserve_count.append(" ORDER BY id DESC");
		buffer_reserve_list.append(" ORDER BY id DESC");

		Long totalRows = Db.queryLong(buffer_reserve_count.toString(), params.toArray());

		if (totalRows != null && totalRows > 0) {
			Page page = new Page(totalRows, pn);
			List<FixBidUserHoldM> lfoll_invest_hold_list = FixBidUserHoldM.dao.find(page.creatLimitSql(buffer_reserve_list.toString()), params.toArray());
			setAttribute("lfoll_invest_hold_list", lfoll_invest_hold_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/lfoll_invest/lfoll_invest_reserve/get_lfoll_invest_join_list");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}

		renderJsp("/user/lfoll_invest/lfoll_invest_join_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "联富宝退出管理页面", last_update_author = "hx")
	public void advance_exit_manage() {

		renderJsp("/user/lfoll_invest/lfoll_invest_exit_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询联富宝退出的信息记录", last_update_author = "hx")
	public void find_fix_bid_exited() {
		int pn = getParameterToInt("pn", 1);
		String user_name = getParameter("user_name");
		String bid_name = getParameter("bid_name");
		String exit_start_time = getParameter("exit_start_time");
		String exit_end_time = getParameter("exit_end_time");

		String find_fix_bid_exited_count_sql = "select count(1) from fix_bid_user_exit tt where 1=1";
		String find_fix_bid_exited_sql = "SELECT * FROM fix_bid_user_exit tt where 1=1 ";
		StringBuffer buffer_count = new StringBuffer(find_fix_bid_exited_count_sql);
		StringBuffer buffer_list = new StringBuffer(find_fix_bid_exited_sql);

		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(user_name)) {
			buffer_count.append(" and tt.real_name like ? ");
			buffer_list.append(" and tt.real_name like ? ");
			params.add(user_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(bid_name)) {
			buffer_count.append(" and tt.bid_name like ? ");
			buffer_list.append(" and tt.bid_name like ? ");
			params.add(bid_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(exit_start_time)) {
			buffer_count.append(" and tt.user_exit_time >= ? ");
			buffer_list.append(" and tt.user_exit_time >= ? ");
			params.add(exit_start_time);
		}
		if (Utils.isNotNullAndNotEmptyString(exit_end_time)) {
			buffer_count.append(" and tt.user_exit_time <= ? ");
			buffer_list.append(" and tt.user_exit_time <= ? ");
			params.add(exit_end_time);
		}

		Long totalRow = Db.queryLong(buffer_count.toString(), params.toArray());
		if (totalRow != null && totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<FixBidUserExitM> fix_bid_user_exit_list = FixBidUserExitM.dao.find(page.creatLimitSql(buffer_list.toString()), params.toArray());
			if (Utils.isHasData(fix_bid_user_exit_list)) {
				String URL = PageDiv.createUrl(getRequest(), "/user/lfoll_invest/lfoll_invest_reserve/find_fix_bid_exited");
				String pageDiv = PageDiv.getDiv(URL, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
				setAttribute("fix_bid_user_exit_list", fix_bid_user_exit_list);
				setAttribute("pageDiv", pageDiv);
			}

		} else {
			setAttribute("tips", "暂无数据");
		}
		renderJsp("/user/lfoll_invest/lfoll_invest_exit_manage.jsp");
		return;

	}
}
