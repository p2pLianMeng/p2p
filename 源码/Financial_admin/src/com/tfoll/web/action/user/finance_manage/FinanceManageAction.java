package com.tfoll.web.action.user.finance_manage;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.AdminM;
import com.tfoll.web.model.SysNotificationM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.model.UserWithdrawCashLogM;
import com.tfoll.web.util.DateTimeUtil;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ActionKey("/user/finance_manage")
public class FinanceManageAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "客服处理用户提现申请导航", last_update_author = "lh")
	public void user_cash_withdraw_index() {

		setSessionAttribute("real_name", null);
		setSessionAttribute("user_identity", null);
		setSessionAttribute("add_time_start", null);
		setSessionAttribute("add_time_end", null);
		setSessionAttribute("is_through", null);
		setAttribute("is_through", -1);

		renderJsp("/user/finance_manage/withdraw_apply_list.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "客服处理用户提现申请查询", last_update_author = "lh")
	public void user_cash_withdraw_query() {

		String real_name = getParameter("real_name");
		String user_identity = getParameter("user_identity");
		String add_time_start = getParameter("add_time_start");
		String add_time_end = getParameter("add_time_end");
		int is_through = getParameterToInt("is_through", -1);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("user_identity", user_identity);
		setSessionAttribute("add_time_start", add_time_start);
		setSessionAttribute("add_time_end", add_time_end);
		setSessionAttribute("is_through", is_through);

		setAttribute("real_name", real_name);
		setAttribute("user_identity", user_identity);
		setAttribute("add_time_start", add_time_start);
		setAttribute("add_time_end", add_time_end);
		setAttribute("is_through", is_through);

		Date $add_time_start = null;
		Date $add_time_end = null;
		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			$add_time_start = DateTimeUtil.my_get_morning_time(add_time_start);
		}
		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			$add_time_end = DateTimeUtil.my_get_night_time(add_time_end);
		}
		// 拼接字符串

		StringBuilder sql_count = new StringBuilder(100);
		StringBuilder sql_select = new StringBuilder(100);

		sql_count.append("select count(1) from user_withdraw_cash_log a , user_info b  where 1=1 and  a.user_id = b.id");
		sql_select.append("select a.id, a.user_id,  b.real_name,b.user_identity,a.account_holder,a.bank_name,a.bank_card_num,a.amount,a.add_time,a.is_through from user_withdraw_cash_log a , user_info b  where 1=1 and  a.user_id = b.id ");

		List<Object> params = new ArrayList<Object>();

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_count.append(" and b.real_name = ? ");
			sql_select.append(" and b.real_name = ? ");
			params.add(real_name);
		}

		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			sql_count.append(" and b.user_identity = ? ");// 这个是存在基本条件
			sql_select.append(" and b.user_identity = ? ");
			params.add(user_identity);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			sql_count.append(" and a.add_time>=? ");
			sql_select.append(" and a.add_time>=? ");
			params.add($add_time_start);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			sql_count.append(" and a.add_time<=? ");
			sql_select.append(" and a.add_time<=? ");
			params.add($add_time_end);
		}

		if (is_through != -1) {// 查询所有的
			sql_count.append(" and a.is_through=? ");
			sql_select.append(" and a.is_through=? ");
			params.add(is_through);
		}
		sql_select.append(" order by a.add_time desc ");// 按照时间排序

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, 1);

			List<Record> withdraw_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());
			for (Record record : withdraw_list) {
				BigDecimal amount = record.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("amount", amount);
			}
			if (Utils.isHasData(withdraw_list)) {
				setAttribute("withdraw_list", withdraw_list);
			}

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/user_cash_withdraw_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());

			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/finance_manage/withdraw_apply_list.jsp");// 不同页面进行组合
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "客服处理用户提现申请页面", last_update_author = "lh")
	public void user_cash_withdraw_page() {

		int pn = getParameterToInt("pn", 1);

		String real_name = getParameter("real_name");
		String user_identity = getParameter("user_identity");
		String add_time_start = getParameter("add_time_start");
		String add_time_end = getParameter("add_time_end");
		int is_through = getParameterToInt("is_through", 0);

		if ("".equals(real_name) && pn > 1) {
			real_name = getSessionAttribute("real_name");
		}
		if ("".equals(user_identity) && pn > 1) {
			user_identity = getSessionAttribute("user_identity");
		}
		if ("".equals(add_time_start) && pn > 1) {
			add_time_start = getSessionAttribute("add_time_start");
		}
		if ("".equals(add_time_end) && pn > 1) {
			add_time_end = getSessionAttribute("add_time_end");
		}
		if (is_through == 0 && pn > 1) {
			is_through = getSessionAttribute("is_through");
		}
		setSessionAttribute("real_name", real_name);
		setSessionAttribute("user_identity", user_identity);
		setSessionAttribute("add_time_start", add_time_start);
		setSessionAttribute("add_time_end", add_time_end);
		setSessionAttribute("is_through", is_through);

		setAttribute("real_name", real_name);
		setAttribute("user_identity", user_identity);
		setAttribute("add_time_start", add_time_start);
		setAttribute("add_time_end", add_time_end);
		setAttribute("is_through", is_through);

		Date $add_time_start = null;
		Date $add_time_end = null;
		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			$add_time_start = DateTimeUtil.my_get_morning_time(add_time_start);
		}
		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			$add_time_end = DateTimeUtil.my_get_night_time(add_time_end);
		}
		// 拼接字符串

		StringBuilder sql_count = new StringBuilder(100);
		StringBuilder sql_select = new StringBuilder(100);

		sql_count.append("select count(1) from user_withdraw_cash_log a , user_info b  where 1=1 and  a.user_id = b.id");
		sql_select.append("select a.id,a.user_id,b.real_name,b.user_identity,a.account_holder,a.bank_name,a.bank_card_num,a.amount,a.add_time,a.is_through from user_withdraw_cash_log a , user_info b  where 1=1 and  a.user_id = b.id ");

		List<Object> params = new ArrayList<Object>();

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_count.append(" and b.real_name = ? ");
			sql_select.append(" and b.real_name = ? ");
			params.add(real_name);
		}

		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			sql_count.append(" and b.user_identity = ? ");
			sql_select.append(" and b.user_identity = ? ");
			params.add(user_identity);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			sql_count.append(" and a.add_time>=? ");
			sql_select.append(" and a.add_time>=? ");
			params.add($add_time_start);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			sql_count.append(" and a.add_time<=? ");
			sql_select.append(" and a.add_time<=? ");
			params.add($add_time_end);
		}

		if (is_through != -1) {// 查询所有的
			sql_count.append(" and a.is_through=? ");
			sql_select.append(" and a.is_through=? ");
			params.add(is_through);
		}
		sql_select.append(" order by a.add_time desc ");// 按照时间排序

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> withdraw_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());
			for (Record record : withdraw_list) {
				BigDecimal amount = record.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("amount", amount);
			}
			if (Utils.isHasData(withdraw_list)) {
				setAttribute("withdraw_list", withdraw_list);
			}

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/user_cash_withdraw_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());

			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/finance_manage/withdraw_apply_list.jsp");// 不同页面进行组合
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "客服同意用户提现申请", last_update_author = "lh")
	public void approve_withdraw_apply_first() {
		int withdraw_id = getParameterToInt("withdraw_id");
		UserWithdrawCashLogM withdraw = UserWithdrawCashLogM.dao.findById(withdraw_id);
		String through_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		boolean ok = withdraw.set("is_through", 3).set("through_time", through_time).update();
		if (ok) {
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "客服拒绝用户提现申请", last_update_author = "lh")
	public void decline_withdraw_apply_first() {
		final int withdraw_id = getParameterToInt("withdraw_id");
		final UserWithdrawCashLogM user_withdraw_cash_log = UserWithdrawCashLogM.dao.findById(withdraw_id);
		final int user_id = user_withdraw_cash_log.getInt("user_id");
		AdminM admin = getSessionAttribute("admin");
		final int admin_id = admin.getInt("id");

		UserM user = UserM.dao.findById(user_id);
		final String phone = user.getString("phone");
		final String email = user.getString("email");

		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				BigDecimal amount = user_withdraw_cash_log.getBigDecimal("amount");
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
				BigDecimal total_net_cash = user_now_money.getBigDecimal("total_net_cash");

				boolean is_ok_update_user_now_money = user_now_money.set("total_net_cash", total_net_cash.subtract(amount)).set("cny_can_used", cny_can_used.add(amount)).update();
				String through_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
				boolean is_Ok_update_user_withdraw_cash_log = user_withdraw_cash_log.set("is_through", 4).set("through_time", through_time).update();

				boolean is_ok_add_user_money_change_record = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_15, cny_can_used.add(cny_freeze), new BigDecimal("0"), amount, cny_can_used.add(cny_freeze).add(amount), "提现撤销#提现id" + withdraw_id);
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_13, cny_can_used.add(cny_freeze), new BigDecimal("0"), amount, cny_can_used.add(cny_freeze).add(amount), "提现撤销");
				if (!is_ok_add_user_money_change_record) {
					return false;
				}
				if (!is_ok_add_user_transaction_records) {
					return false;
				}

				return is_ok_update_user_now_money && is_Ok_update_user_withdraw_cash_log;
			}
		});

		if (is_ok) {
			SysNotificationM.send_sys_notification(admin_id, user_id, "提现拒绝", "你的提现申请被拒绝，具体原因请联系客服", phone, true, 3, email == null ? "@" : email);
			// , true, 7
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	// 以下是财务处理部分
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "财务处理用户提现申请导航", last_update_author = "lh")
	public void handle_user_cash_withdraw_index() {

		setSessionAttribute("user_name", null);
		setSessionAttribute("id_no", null);
		setSessionAttribute("add_time_start", null);
		setSessionAttribute("add_time_end", null);
		setSessionAttribute("is_pay", null);
		setAttribute("is_pay", -1);

		renderJsp("/user/finance_manage/handle_withdraw_list.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "财务处理用户提现申请查询", last_update_author = "lh")
	public void handle_user_cash_withdraw_query() {

		String real_name = getParameter("real_name");
		String id_no = getParameter("id_no");
		String add_time_start = getParameter("add_time_start");
		String add_time_end = getParameter("add_time_end");
		int is_pay = getParameterToInt("is_pay", -1);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("id_no", id_no);
		setSessionAttribute("add_time_start", add_time_start);
		setSessionAttribute("add_time_end", add_time_end);
		setSessionAttribute("is_pay", is_pay);

		setAttribute("real_name", real_name);
		setAttribute("id_no", id_no);
		setAttribute("add_time_start", add_time_start);
		setAttribute("add_time_end", add_time_end);
		setAttribute("is_pay", is_pay);

		Date $add_time_start = null;
		Date $add_time_end = null;
		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			$add_time_start = DateTimeUtil.my_get_morning_time(add_time_start);
		}
		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			$add_time_end = DateTimeUtil.my_get_night_time(add_time_end);
		}
		// 拼接字符串

		StringBuilder sql_count = new StringBuilder(100);
		StringBuilder sql_select = new StringBuilder(100);

		sql_count.append("select count(1) from user_withdraw_cash_log a left join user_info b on a.user_id = b.id where a.is_through=3 and 1=1 ");
		sql_select.append("select a.id,a.user_id,b.real_name,b.user_identity,a.account_holder,a.bank_name,a.bank_card_num,a.amount,a.fee,a.add_time,a.is_pay from user_withdraw_cash_log a left join user_info b on a.user_id = b.id where a.is_through=3 and 1=1 ");

		List<Object> params = new ArrayList<Object>();

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_count.append(" and b.real_name = ? ");
			sql_select.append(" and b.real_name = ? ");
			params.add(real_name);
		}

		if (Utils.isNotNullAndNotEmptyString(id_no)) {
			sql_count.append(" and b.user_identity = ? ");
			sql_select.append(" and b.user_identity = ? ");
			params.add(id_no);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			sql_count.append(" and a.add_time>=? ");
			sql_select.append(" and a.add_time>=? ");
			params.add($add_time_start);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			sql_count.append(" and a.add_time<=? ");
			sql_select.append(" and a.add_time<=? ");
			params.add($add_time_end);
		}

		if (is_pay != -1) {// 查询所有的
			sql_count.append(" and a.is_pay=? ");
			sql_select.append(" and a.is_pay=? ");
			params.add(is_pay);
		}
		sql_select.append(" order by a.add_time desc ");// 按照时间排序

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, 1);

			List<Record> withdraw_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			for (Record withdraw : withdraw_list) {
				BigDecimal amount = withdraw.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN);
				BigDecimal fee = withdraw.getBigDecimal("fee").setScale(2, BigDecimal.ROUND_DOWN);
				BigDecimal transfer_money = amount.subtract(fee).setScale(2, BigDecimal.ROUND_DOWN);
				withdraw.add("amount", amount);
				withdraw.add("fee", fee);
				withdraw.add("transfer_money", transfer_money);

			}

			if (Utils.isHasData(withdraw_list)) {
				setAttribute("withdraw_list", withdraw_list);
			}

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/handle_user_cash_withdraw_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());

			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/finance_manage/handle_withdraw_list.jsp");// 不同页面进行组合
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "财务处理用户提现申请页面", last_update_author = "lh")
	public void handle_user_cash_withdraw_page() {

		int pn = getParameterToInt("pn", 1);

		String real_name = getParameter("real_name");
		String id_no = getParameter("id_no");
		String add_time_start = getParameter("add_time_start");
		String add_time_end = getParameter("add_time_end");
		int is_pay = getParameterToInt("is_pay", 0);

		if ("".equals(real_name) && pn > 1) {
			real_name = getSessionAttribute("real_name");
		}
		if ("".equals(id_no) && pn > 1) {
			id_no = getSessionAttribute("id_no");
		}
		if ("".equals(add_time_start) && pn > 1) {
			add_time_start = getSessionAttribute("add_time_start");
		}
		if ("".equals(add_time_end) && pn > 1) {
			add_time_end = getSessionAttribute("add_time_end");
		}
		if (is_pay == 0 && pn > 1) {
			is_pay = getSessionAttribute("is_pay");
		}

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("id_no", id_no);
		setSessionAttribute("add_time_start", add_time_start);
		setSessionAttribute("add_time_end", add_time_end);
		setSessionAttribute("is_pay", is_pay);

		setAttribute("real_name", real_name);
		setAttribute("id_no", id_no);
		setAttribute("add_time_start", add_time_start);
		setAttribute("add_time_end", add_time_end);
		setAttribute("is_pay", is_pay);

		Date $add_time_start = null;
		Date $add_time_end = null;
		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			$add_time_start = DateTimeUtil.my_get_morning_time(add_time_start);
		}
		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			$add_time_end = DateTimeUtil.my_get_night_time(add_time_end);
		}

		StringBuilder sql_count = new StringBuilder(100);
		StringBuilder sql_select = new StringBuilder(100);

		sql_count.append("select count(1) from user_withdraw_cash_log a left join user_info b on a.user_id = b.id where a.is_through=3 and 1=1 ");
		sql_select.append("select a.id,a.user_id,b.real_name,b.user_identity,a.account_holder,a.bank_name,a.bank_card_num,a.amount,a.fee,a.add_time,a.is_pay from user_withdraw_cash_log a left join user_info b on a.user_id = b.id where a.is_through=3 and 1=1 ");

		List<Object> params = new ArrayList<Object>();

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_count.append(" and b.real_name = ? ");
			sql_select.append(" and b.real_name = ? ");
			params.add(real_name);
		}

		if (Utils.isNotNullAndNotEmptyString(id_no)) {
			sql_count.append(" and b.user_identity = ? ");// 这个是存在基本条件
			sql_select.append(" and b.user_identity = ? ");
			params.add(id_no);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			sql_count.append(" and a.add_time>=? ");
			sql_select.append(" and a.add_time>=? ");
			params.add($add_time_start);
		}

		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			sql_count.append(" and a.add_time<=? ");
			sql_select.append(" and a.add_time<=? ");
			params.add($add_time_end);
		}

		if (is_pay != -1) {// 查询所有的
			sql_count.append(" and a.is_pay=? ");
			sql_select.append(" and a.is_pay=? ");
			params.add(is_pay);
		}
		sql_select.append(" order by a.add_time desc ");// 按照时间排序

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> withdraw_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());
			for (Record withdraw : withdraw_list) {
				BigDecimal amount = withdraw.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN);
				BigDecimal fee = withdraw.getBigDecimal("fee").setScale(2, BigDecimal.ROUND_DOWN);
				BigDecimal transfer_money = amount.subtract(fee).setScale(2, BigDecimal.ROUND_DOWN);
				withdraw.add("amount", amount);
				withdraw.add("fee", fee);
				withdraw.add("transfer_money", transfer_money);
			}

			if (Utils.isHasData(withdraw_list)) {
				setAttribute("withdraw_list", withdraw_list);
			}

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/handle_user_cash_withdraw_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());

			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/finance_manage/handle_withdraw_list.jsp");// 不同页面进行组合
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "财务同意用户提现申请", last_update_author = "lh")
	public void approve_withdraw_apply_second() {

		AdminM admin = getSessionAttribute("admin");
		int admin_id = admin.getInt("id");

		int withdraw_id = getParameterToInt("withdraw_id");
		UserWithdrawCashLogM withdraw = UserWithdrawCashLogM.dao.findById(withdraw_id);

		int user_id = withdraw.getInt("user_id");
		UserM user = UserM.dao.findById(user_id);
		String phone = user.getString("phone");
		String email = user.getString("email");

		String pay_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		boolean ok = withdraw.set("is_pay", 2).set("pay_time", pay_time).update();
		if (ok) {
			// 同意不发短信
			// SysNotificationM.send_sys_notification(admin_id, user_id, "提现成功",
			// "你的提现申请已经审核通过，等待银行处理中", true, 9, phone, true, 7, email == null ?
			// "" : email);
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "财务拒绝用户提现申请", last_update_author = "lh")
	public void decline_withdraw_apply_second() {
		final int withdraw_id = getParameterToInt("withdraw_id");
		final UserWithdrawCashLogM user_withdraw_cash_log = UserWithdrawCashLogM.dao.findById(withdraw_id);

		final BigDecimal amount = user_withdraw_cash_log.getBigDecimal("amount");
		final int user_id = user_withdraw_cash_log.getInt("user_id");
		AdminM admin = getSessionAttribute("admin");
		int admin_id = admin.getInt("id");

		UserM user = UserM.dao.findById(user_id);
		String phone = user.getString("phone");
		String email = user.getString("email");

		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
				BigDecimal total_net_cash = user_now_money.getBigDecimal("total_net_cash");
				boolean is_ok_update_user_now_money = user_now_money.set("total_net_cash", total_net_cash.subtract(amount)).set("cny_can_used", cny_can_used.add(amount)).update();
				String pay_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
				boolean is_ok_update_user_withdraw_cash_log = user_withdraw_cash_log.set("is_pay", 3).set("pay_time", pay_time).update();

				boolean is_ok_add_user_money_change_record = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_15, cny_can_used.add(cny_freeze), new BigDecimal("0"), amount, cny_can_used.add(cny_freeze).add(amount), "提现撤销#提现id:" + withdraw_id);
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_13, cny_can_used.add(cny_freeze), new BigDecimal("0"), amount, cny_can_used.add(cny_freeze).add(amount), "提现撤销");
				if (!is_ok_add_user_money_change_record) {
					return false;
				}
				if (!is_ok_add_user_transaction_records) {
					return false;
				}

				return is_ok_update_user_withdraw_cash_log && is_ok_update_user_now_money;
			}
		});
		if (is_ok) {
			SysNotificationM.send_sys_notification(admin_id, user_id, "提现拒绝", "你的提现申请被拒绝，具体原因请联系客服", phone, true, 3, email == null ? "@" : email);
			// , true, 7
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询用户充值记录管理页面", last_update_author = "hx")
	public void find_user_recharge_record_page() {
		setAttribute("real_name", "");
		setAttribute("user_identity", "");
		setAttribute("start_time", "");
		setAttribute("end_time", "");
		renderJsp("/user/finance_manage/rmb_recharge_record_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询用户充值记录", last_update_author = "hx")
	public void get_recharge_record() {
		int pn = getParameterToInt("pn", 1);
		String real_name = getParameter("real_name", "");
		String user_identity = getParameter("user_identity", "");
		String start_time = getParameter("start_time", "");
		String end_time = getParameter("end_time", "");

		if ("".equals(real_name) && pn > 1) {
			real_name = getSessionAttribute("real_name");
		}
		if ("".equals(user_identity) && pn > 1) {
			user_identity = getSessionAttribute("user_identity");
		}
		if ("".equals(start_time) && pn > 1) {
			start_time = getSessionAttribute("start_time");
		}
		if ("".equals(end_time) && pn > 1) {
			end_time = getSessionAttribute("end_time");
		}
		setSessionAttribute("real_name", real_name);
		setSessionAttribute("user_identity", user_identity);
		setSessionAttribute("start_time", start_time);
		setSessionAttribute("end_time", end_time);

		setAttribute("real_name", real_name);
		setAttribute("user_identity", user_identity);
		setAttribute("start_time", start_time);
		setAttribute("end_time", end_time);

		String count_sql = "SELECT count(1) FROM user_recharge_log t1, user_info t2 WHERE t1.user_id = t2.id";
		String list_sql = "SELECT t2.real_name AS real_name, t2.user_identity AS user_identity, t1.amount AS amount, t1.actual_amount AS actual_amount, t1.add_time AS add_time, t1.type AS type FROM user_recharge_log t1, user_info t2 WHERE t1.user_id = t2.id";
		StringBuffer buffer_count = new StringBuffer(count_sql);
		StringBuffer buffer_list = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			buffer_count.append(" AND t2.real_name like ? ");
			buffer_list.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			buffer_count.append(" AND t2.user_identity like ? ");
			buffer_list.append(" AND t2.user_identity like ? ");
			params.add(user_identity + "%");

		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			buffer_count.append(" AND t1.add_time >= ? ");
			buffer_list.append(" AND t1.add_time >= ? ");
			params.add(start_time);

		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			buffer_count.append(" AND t1.add_time <= ? ");
			buffer_list.append(" AND t1.add_time <= ? ");
			params.add(end_time);
		}
		buffer_count.append(" ORDER BY t1.add_time DESC");
		buffer_list.append(" ORDER BY t1.add_time DESC");

		Long total_rows = Db.queryLong(buffer_count.toString(), params.toArray());
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<Record> user_recharge_log_list = Db.find(page.creatLimitSql(buffer_list.toString()), params.toArray());
			for (Record record : user_recharge_log_list) {
				BigDecimal amount = record.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN);
				BigDecimal actual_amount = record.getBigDecimal("actual_amount").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("amount", amount);
				record.add("actual_amount", actual_amount);
			}
			setAttribute("user_recharge_log_list", user_recharge_log_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/get_recharge_record");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}
		renderJsp("/user/finance_manage/rmb_recharge_record_manage.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询用户提现记录管理页面", last_update_author = "hx")
	public void find_user_withdraw_record_page() {
		setAttribute("real_name", "");
		setAttribute("user_identity", "");
		setAttribute("start_time", "");
		setAttribute("end_time", "");
		renderJsp("/user/finance_manage/rmb_withdraw_record_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询用户提现记录", last_update_author = "hx")
	public void get_withdraw_record() {
		int pn = getParameterToInt("pn", 1);
		String real_name = getParameter("real_name", "");
		String user_identity = getParameter("user_identity", "");
		String start_time = getParameter("start_time", "");
		String end_time = getParameter("end_time", "");

		if ("".equals(real_name) && pn > 1) {
			real_name = getSessionAttribute("real_name");
		}
		if ("".equals(user_identity) && pn > 1) {
			user_identity = getSessionAttribute("user_identity");
		}
		if ("".equals(start_time) && pn > 1) {
			start_time = getSessionAttribute("start_time");
		}
		if ("".equals(end_time) && pn > 1) {
			end_time = getSessionAttribute("end_time");
		}
		setSessionAttribute("real_name", real_name);
		setSessionAttribute("user_identity", user_identity);
		setSessionAttribute("start_time", start_time);
		setSessionAttribute("end_time", end_time);

		setAttribute("real_name", real_name);
		setAttribute("user_identity", user_identity);
		setAttribute("start_time", start_time);
		setAttribute("end_time", end_time);

		String count_sql = "SELECT count(1) FROM user_withdraw_cash_log t1, user_info t2 WHERE t1.user_id = t2.id";
		String list_sql = "SELECT t2.real_name AS real_name, t2.user_identity AS user_identity, t1.account_holder AS account_holder, t1.bank_name AS bank_name, t1.bank_address AS bank_address, t1.bank_card_num AS bank_card_num, t1.amount AS amount, t1.fee AS fee,t1.add_time as add_time, t1.pay_time AS pay_time FROM user_withdraw_cash_log t1, user_info t2 WHERE t1.user_id = t2.id AND t1.is_through = 3 AND t1.is_pay = 2";
		StringBuffer buffer_count = new StringBuffer(count_sql);
		StringBuffer buffer_list = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			buffer_count.append(" AND t2.real_name like ? ");
			buffer_list.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			buffer_count.append(" AND t2.user_identity like ? ");
			buffer_list.append(" AND t2.user_identity like ? ");
			params.add(user_identity + "%");

		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			buffer_count.append(" AND t1.pay_time >= ? ");
			buffer_list.append(" AND t1.pay_time >= ? ");
			params.add(start_time);

		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			buffer_count.append(" AND t1.pay_time <= ? ");
			buffer_list.append(" AND t1.pay_time <= ? ");
			params.add(end_time);
		}
		buffer_count.append(" ORDER BY t1.add_time DESC");
		buffer_list.append(" ORDER BY t1.add_time DESC");

		Long total_rows = Db.queryLong(buffer_count.toString(), params.toArray());
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<Record> user_withdraw_cash_log_list = Db.find(page.creatLimitSql(buffer_list.toString()), params.toArray());
			for (Record record : user_withdraw_cash_log_list) {
				BigDecimal amount = record.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN);
				BigDecimal fee = record.getBigDecimal("fee").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("amount", amount);
				record.add("fee", fee);
			}
			setAttribute("user_withdraw_cash_log_list", user_withdraw_cash_log_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/get_withdraw_record");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}
		renderJsp("/user/finance_manage/rmb_withdraw_record_manage.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询财务收入记录页面", last_update_author = "hx")
	public void finance_income_page() {
		setAttribute("start_time", "");
		setAttribute("end_time", "");
		setAttribute("real_name", "");
		renderJsp("/user/finance_manage/finance_income_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询财务收入记录", last_update_author = "hx")
	public void find_financial_income_records() {
		int pn = getParameterToInt("pn", 1);
		String start_time = getParameter("start_time", "");
		String end_time = getParameter("end_time", "");
		String real_name = getParameter("real_name", "");

		if ("".equals(start_time) && pn > 1) {
			start_time = getSessionAttribute("start_time");
		}
		if ("".equals(end_time) && pn > 1) {
			end_time = getSessionAttribute("end_time");
		}
		if ("".equals(real_name) && pn > 1) {
			real_name = getSessionAttribute("real_name");
		}

		setSessionAttribute("start_time", start_time);
		setSessionAttribute("end_time", end_time);
		setSessionAttribute("real_name", real_name);

		setAttribute("start_time", start_time);
		setAttribute("end_time", end_time);
		setAttribute("real_name", real_name);

		String count_sql = "SELECT COUNT(1) FROM sys_income_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		String list_sql = "SELECT t2.real_name as real_name ,t1.money as money,t1.detail as detail,t1.add_time as add_time FROM sys_income_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		StringBuffer count_buffer = new StringBuffer(count_sql);
		StringBuffer list_buffer = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			count_buffer.append(" AND t2.real_name like ? ");
			list_buffer.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			count_buffer.append(" AND t1.add_time >= ? ");
			list_buffer.append(" AND t1.add_time >= ? ");
			params.add(start_time);
		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			count_buffer.append(" AND t1.add_time < ? ");
			list_buffer.append(" AND t1.add_time < ? ");
			params.add(end_time);
		}
		count_buffer.append(" ORDER BY t1.id DESC");
		list_buffer.append(" ORDER BY t1.id DESC");

		Long total_rows = Db.queryLong(count_buffer.toString(), params.toArray());

		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<Record> sys_income_record_list = Db.find(page.creatLimitSql(list_buffer.toString()), params.toArray());
			for (Record record : sys_income_record_list) {
				BigDecimal decimal_money = record.getBigDecimal("money").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("money", decimal_money);
			}
			setAttribute("sys_income_record_list", sys_income_record_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/find_financial_income_records");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}
		renderJsp("/user/finance_manage/finance_income_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询财务支出记录页面", last_update_author = "hx")
	public void finance_expenses_page() {
		setAttribute("start_time", "");
		setAttribute("end_time", "");
		setAttribute("real_name", "");
		renderJsp("/user/finance_manage/finance_expenses_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询财务支出记录", last_update_author = "hx")
	public void find_financial_expenses_records() {
		int pn = getParameterToInt("pn", 1);
		String start_time = getParameter("start_time", "");
		String end_time = getParameter("end_time", "");
		String real_name = getParameter("real_name", "");

		if ("".equals(start_time) && pn > 1) {
			start_time = getSessionAttribute("start_time");
		}
		if ("".equals(end_time) && pn > 1) {
			end_time = getSessionAttribute("end_time");
		}
		if ("".equals(real_name) && pn > 1) {
			real_name = getSessionAttribute("real_name");
		}

		setSessionAttribute("start_time", start_time);
		setSessionAttribute("end_time", end_time);
		setSessionAttribute("real_name", real_name);

		setAttribute("start_time", start_time);
		setAttribute("end_time", end_time);
		setAttribute("real_name", real_name);

		String count_sql = "SELECT COUNT(1) FROM sys_expenses_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		String list_sql = "SELECT t2.real_name as real_name ,t1.money as money,t1.detail as detail,t1.add_time as add_time FROM sys_expenses_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		StringBuffer count_buffer = new StringBuffer(count_sql);
		StringBuffer list_buffer = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			count_buffer.append(" AND t2.real_name like ? ");
			list_buffer.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			count_buffer.append(" AND t1.add_time >= ? ");
			list_buffer.append(" AND t1.add_time >= ? ");
			params.add(start_time);
		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			count_buffer.append(" AND t1.add_time < ? ");
			list_buffer.append(" AND t1.add_time < ? ");
			params.add(end_time);
		}
		count_buffer.append(" ORDER BY t1.id DESC");
		list_buffer.append(" ORDER BY t1.id DESC");

		Long total_rows = Db.queryLong(count_buffer.toString(), params.toArray());

		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<Record> sys_income_record_list = Db.find(page.creatLimitSql(list_buffer.toString()), params.toArray());
			for (Record record : sys_income_record_list) {
				BigDecimal decimal_money = record.getBigDecimal("money").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("money", decimal_money);
			}
			setAttribute("sys_income_record_list", sys_income_record_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/finance_manage/find_financial_expenses_records");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}
		renderJsp("/user/finance_manage/finance_expenses_manage.jsp");
		return;
	}
}
