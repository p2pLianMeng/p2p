package com.tfoll.web.action.user.lfoll_invest;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.util.DateTimeUtil;
import com.tfoll.web.util.LfollInvestConfigUtil;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

@ActionKey("/user/lfoll_invest")
public class LfollInvestAction extends Controller {

	/**
	 * 联富宝 管理Action
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入联富定投设置页面", last_update_author = "hx")
	public void forward_lfoll_invest_set_page() {
		setSessionAttribute("bid_name", null);// 联富定投名称：
		setSessionAttribute("advance_notice_start_time", null);// 预告开始时间
		setSessionAttribute("invite_end_time", null);// 开放结束时间

		setAttribute("tips", "暂无数据!");
		renderJsp("/user/lfoll_invest/lfoll_invest_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询联富宝", last_update_author = "hx")
	public void find_lfoll_invest_query() {
		String bid_name = getParameter("bid_name");
		String advance_notice_start_time = getParameter("advance_notice_start_time");
		String invite_end_time = getParameter("invite_end_time");

		setSessionAttribute("bid_name", bid_name);// 联富定投名称：
		setSessionAttribute("advance_notice_start_time", advance_notice_start_time);// 预告开始时间
		setSessionAttribute("invite_end_time", invite_end_time);// 开放结束时间

		setAttribute("bid_name", bid_name);// 联富定投名称：
		setAttribute("advance_notice_start_time", advance_notice_start_time);// 预告开始时间
		setAttribute("invite_end_time", invite_end_time);// 开放结束时间

		Date advance_notice_start_time_date = null;
		Date invite_end_time_date = null;
		if (Utils.isNotNullAndNotEmptyString(advance_notice_start_time)) {
			advance_notice_start_time_date = DateTimeUtil.get_morning_time(advance_notice_start_time + " 00:00:00");
		}
		if (Utils.isNotNullAndNotEmptyString(invite_end_time)) {
			invite_end_time_date = DateTimeUtil.get_night_time(invite_end_time + " 23:59:59");
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder count_sql = new StringBuilder("select count(1) from fix_bid_system_order tt where 1=1 ");// );
		StringBuilder query_sql = new StringBuilder("select * from fix_bid_system_order tt where 1=1 ");
		if (Utils.isNotNullAndNotEmptyString(bid_name)) {
			count_sql.append("and	 tt.bid_name like ? ");
			query_sql.append("and	 tt.bid_name like ? ");
			params.add("%" + bid_name + "%");

		}

		if (Utils.isNotNullAndNotEmptyString(advance_notice_start_time)) {
			count_sql.append("and	 tt.advance_notice_start_time >= ? ");
			query_sql.append("and	 tt.advance_notice_start_time >= ? ");
			params.add(advance_notice_start_time_date);
		}
		if (Utils.isNotNullAndNotEmptyString(invite_end_time)) {
			count_sql.append("and	 tt.invite_end_time <= ? ");
			query_sql.append("and	 tt.invite_end_time <= ? ");
			params.add(invite_end_time_date);

		}

		Long total_rows = Db.queryLong(count_sql.toString(), params.toArray());
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, 1);
			List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(page.creatLimitSql(query_sql.toString()), params.toArray());
			setAttribute("fix_bid_system_order_list", fix_bid_system_order_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/lfoll_invest/find_lfoll_invest_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无记录");
		}
		renderJsp("/user/lfoll_invest/lfoll_invest_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询联富宝", last_update_author = "hx")
	public void find_lfoll_invest_page() {
		int pn = getParameterToInt("pn");

		String bid_name = getSessionAttribute("bid_name");// 联富定投名称：
		String advance_notice_start_time = getSessionAttribute("advance_notice_start_time");
		String invite_end_time = getSessionAttribute("invite_end_time");

		setAttribute("bid_name", bid_name);// 联富定投名称：
		setAttribute("advance_notice_start_time", advance_notice_start_time);// 预告开始时间
		setAttribute("invite_end_time", invite_end_time);// 开放结束时间

		Date advance_notice_start_time_date = null;
		Date invite_end_time_date = null;
		if (Utils.isNotNullAndNotEmptyString(advance_notice_start_time)) {
			advance_notice_start_time_date = DateTimeUtil.get_morning_time(advance_notice_start_time + " 00:00:00");
		}
		if (Utils.isNotNullAndNotEmptyString(invite_end_time)) {
			invite_end_time_date = DateTimeUtil.get_night_time(invite_end_time + " 23:59:59");
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder count_sql = new StringBuilder("select count(1) from fix_bid_system_order tt where 1=1 ");// );
		StringBuilder query_sql = new StringBuilder("select * from fix_bid_system_order tt where 1=1 ");
		if (Utils.isNotNullAndNotEmptyString(bid_name)) {
			count_sql.append("and	 tt.bid_name like ? ");
			query_sql.append("and	 tt.bid_name like ? ");
			params.add("%" + bid_name + "%");

		}

		if (Utils.isNotNullAndNotEmptyString(advance_notice_start_time)) {
			count_sql.append("and	 tt.advance_notice_start_time >= ? ");
			query_sql.append("and	 tt.advance_notice_start_time >= ? ");
			params.add(advance_notice_start_time_date);
		}
		if (Utils.isNotNullAndNotEmptyString(invite_end_time)) {
			count_sql.append("and	 tt.invite_end_time <= ? ");
			query_sql.append("and	 tt.invite_end_time <= ? ");
			params.add(invite_end_time_date);

		}

		Long total_rows = Db.queryLong(count_sql.toString(), params.toArray());
		if (total_rows != null && total_rows > 0) {
			Page page = new Page(total_rows, pn);
			List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find(page.creatLimitSql(query_sql.toString()), params.toArray());
			setAttribute("fix_bid_system_order_list", fix_bid_system_order_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/lfoll_invest/find_lfoll_invest_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无记录");
		}
		renderJsp("/user/lfoll_invest/lfoll_invest_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入查询单笔联富定投页面", last_update_author = "hx")
	public void get_lfoll_invest_info() {
		Long id = getParameterToLong("id", 0);
		if (id == 0) {
			renderText("访问出错!");
			return;
		}
		FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.dao.findById(id);
		if (fix_bid_system_order == null) {
			fix_bid_system_order = new FixBidSystemOrderM();// 基本上不会出现这个问题
		} else {
			// 时间需要格式化-去掉最后的一个0
			fix_bid_system_order.set("advance_notice_start_time", Time.format(fix_bid_system_order.getDate("advance_notice_start_time")));
			fix_bid_system_order.set("reserve_start_time", Time.format(fix_bid_system_order.getDate("reserve_start_time")));
			fix_bid_system_order.set("reserve_end_time", Time.format(fix_bid_system_order.getDate("reserve_end_time")));
			fix_bid_system_order.set("pay_end_time", Time.format(fix_bid_system_order.getDate("pay_end_time")));
			fix_bid_system_order.set("invite_start_time", Time.format(fix_bid_system_order.getDate("invite_start_time")));
			fix_bid_system_order.set("invite_end_time", Time.format(fix_bid_system_order.getDate("invite_end_time")));
			fix_bid_system_order.set("repayment_time", Time.format(fix_bid_system_order.getDate("repayment_time")));

		}
		setAttribute("fix_bid_system_order", fix_bid_system_order);
		renderJsp("/user/lfoll_invest/lfoll_invest_detail.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "更新联富定投单笔信息", last_update_author = "hx")
	public void update_lfoll_invest_info() throws Exception {

		Long id = getParameterToLong("id");
		String bid_name = getParameter("bid_name");
		Integer total_money = getParameterToInt("total_money");
		Integer sold_money = getParameterToInt("sold_money");
		Integer least_money = getParameterToInt("least_money");
		String annualized_rate_string = getParameter("annualized_rate");
		BigDecimal annualized_rate = new BigDecimal(annualized_rate_string);
		Integer is_show = getParameterToInt("is_show");

		String advance_notice_start_time_string = getParameter("advance_notice_start_time");
		String reserve_start_time_string = getParameter("reserve_start_time");
		String reserve_end_time_string = getParameter("reserve_end_time");
		String pay_end_time_string = getParameter("pay_end_time");
		String invite_start_time_string = getParameter("invite_start_time");
		String invite_end_time_string = getParameter("invite_end_time");
		Integer closed_period = getParameterToInt("closed_period");
		String repayment_time_string = getParameter("repayment_time");

		Date advance_notice_start_time = Time.parse(advance_notice_start_time_string);
		Date reserve_start_time = Time.parse(reserve_start_time_string);
		Date reserve_end_time = Time.parse(reserve_end_time_string);
		Date pay_end_time = Time.parse(pay_end_time_string);
		Date invite_start_time = Time.parse(invite_start_time_string);
		Date invite_end_time = Time.parse(invite_end_time_string);
		Date repayment_time = Time.parse(repayment_time_string);

		FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.dao.findById(id);
		// 联富定投名称
		if (Utils.isNotNullAndNotEmptyString(bid_name)) {
			fix_bid_system_order.set("bid_name", bid_name);
		}
		// 总发售金额
		if (total_money != null && total_money != 0) {
			fix_bid_system_order.set("total_money", total_money);
		}
		// 已售出金额
		if (sold_money != null && sold_money != 0) {
			fix_bid_system_order.set("sold_money", sold_money);
		}

		// 起投金额
		if (least_money != null && least_money != 0) {
			fix_bid_system_order.set("least_money", least_money);
		}
		// 年化利率
		if (Utils.isNotNullAndNotEmptyString(annualized_rate_string)) {
			fix_bid_system_order.set("annualized_rate", annualized_rate);
		}
		// 是否挂出定投单
		if (is_show != null && is_show != 0) {
			fix_bid_system_order.set("is_show", is_show);
		}
		// 预告开始时间
		if (advance_notice_start_time != null) {
			fix_bid_system_order.set("advance_notice_start_time", advance_notice_start_time);
			fix_bid_system_order.set("advance_notice_start_time_long", advance_notice_start_time.getTime());
		}
		// 预定开始时间
		if (reserve_start_time != null) {
			fix_bid_system_order.set("reserve_start_time", reserve_start_time);
			fix_bid_system_order.set("reserve_start_time_long", reserve_start_time.getTime());
		}

		// 预定结束时间
		if (reserve_end_time != null) {
			fix_bid_system_order.set("reserve_end_time", reserve_end_time);
			fix_bid_system_order.set("reserve_end_time_long", reserve_end_time.getTime());
		}
		// 支付截止时间
		if (pay_end_time != null) {
			fix_bid_system_order.set("pay_end_time", pay_end_time);
			fix_bid_system_order.set("pay_end_time_long", pay_end_time.getTime());
		}
		// 投标开始时间
		if (invite_start_time != null) {
			fix_bid_system_order.set("invite_start_time", invite_start_time);
			fix_bid_system_order.set("invite_start_time_long", invite_start_time.getTime());
		}
		// 投标结束时间
		if (invite_end_time != null) {
			fix_bid_system_order.set("invite_end_time", invite_end_time);
			fix_bid_system_order.set("invite_end_time_long", invite_end_time.getTime());
		}
		// 封闭期（月）
		if (closed_period != null && closed_period != 0) {
			fix_bid_system_order.set("closed_period", closed_period);
		}
		// 退出定投时间
		if (repayment_time != null) {
			fix_bid_system_order.set("repayment_time", repayment_time);
			fix_bid_system_order.set("repayment_time_long", repayment_time.getTime());
		}
		fix_bid_system_order.update();

		renderText("13");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入联富定投配置页面", last_update_author = "hx")
	public void goto_lfoll_invest_config() {
		renderJsp("/user/lfoll_invest/lfoll_invest_config.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "保存联富宝配置信息", last_update_author = "hx")
	public void create_all_lfoll_invest_order() throws Exception {

		// 定投发售总额金额
		Integer total_money = getParameterToInt("total_money");
		String annualized_rate_string = getParameter("annualized_rate");// 传入为小数
		// 年化利率
		BigDecimal annualized_rate = new BigDecimal(annualized_rate_string);
		// 起投金额
		Integer least_money = getParameterToInt("least_money");

		// 配置系统开始时间
		String system_start_time = getParameter("system_start_time");
		// 配置系统结束时间
		String system_end_time = getParameter("system_end_time");

		// 定投之间间隔天数
		Integer index_day = getParameterToInt("index_day", 0);
		// 从通告开始 到预订开始时间的小时数
		Integer from_notice_to_reservation_hours = getParameterToInt("from_notice_to_reservation_hours", 0);
		// 从预定开始到预定结束的小时数
		Integer from_reservation_start_to_reservation_end = getParameterToInt("from_reservation_start_to_reservation_end", 0);
		// 从预定结束到支付截止的小时数
		Integer from_reservation_end_to_paystop = getParameterToInt("from_reservation_end_to_paystop", 0);
		// 从支付结束到开放加入的小时数
		Integer from_paystop_to_begain_order = getParameterToInt("from_paystop_to_begain_order", 0);
		// 从开放定投到锁定开始的小时数
		Integer from_begain_order_to_lock_order = getParameterToInt("from_begain_order_to_lock_order", 0);
		// 从锁定开始到退出定投的 月数(默认为12个月)
		Integer from_lock_time_to_exit_time = getParameterToInt("from_lock_time_to_exit_time", 0);

		/******
		 * 校验
		 *****/
		if (total_money == null || total_money == 0) {
			renderText("1");// 发售总额金额 不能为空
			return;
		}

		if (!Utils.isNotNullAndNotEmptyString(annualized_rate_string)) {
			renderText("2");// 设置年利率不能为空
			return;
		}

		if (!(least_money % 1000 == 0)) {
			renderText("3");// 起投金额 必须是50的倍数
			return;
		}

		if (!Utils.isNotNullAndNotEmptyString(system_start_time)) {
			renderText("4");// 系统开始时间不能不空
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(system_end_time)) {
			renderText("5");// 系统结束时间不能为空
			return;
		}
		if (index_day == null || index_day == 0) {
			renderText("6");// 定投之间间隔天数不能为空或不能为0
			return;
		}
		if (from_notice_to_reservation_hours == null || from_notice_to_reservation_hours == 0) {
			renderText("7");// 通告开始 到预订开始时间的小时数不能为空或不能为0
			return;
		}
		if (from_reservation_start_to_reservation_end == null || from_reservation_start_to_reservation_end == 0) {
			renderText("8");// 预定开始到预定结束的小时数不能为空或不能为0
			return;
		}
		if (from_reservation_end_to_paystop == null || from_reservation_end_to_paystop == 0) {
			renderText("9");// 从预定结束到支付截止的小时数
			return;
		}

		if (from_paystop_to_begain_order == null || from_paystop_to_begain_order == 0) {
			renderText("10");// 从支付结束到开放加入的小时数
			return;
		}

		if (from_begain_order_to_lock_order == null || from_begain_order_to_lock_order == 0) {
			renderText("11");// 从开放定投到锁定开始的小时数
			return;
		}

		if (from_lock_time_to_exit_time == null || from_lock_time_to_exit_time == 0) {
			renderText("12");// 从锁定开始到退出定投的 月数(默认为12个月)
			return;
		}

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date_sys_start_time = null;// 系统开始时间(Date类型)
		Date date_sys_end_time = null;// 系统结束时间(Date类型)

		date_sys_start_time = format.parse(system_start_time);
		date_sys_end_time = format.parse(system_end_time);

		while (date_sys_start_time.getTime() <= date_sys_end_time.getTime()) {
			// 保存联富定投12个月单子
			boolean bool_12 = save_lfoll_invest_12(total_money, annualized_rate, least_money, from_notice_to_reservation_hours, from_reservation_start_to_reservation_end, from_reservation_end_to_paystop, from_paystop_to_begain_order, from_begain_order_to_lock_order, from_lock_time_to_exit_time, format, date_sys_start_time);
			// 保存联富定投6个月单子
			boolean bool_06 = save_lfoll_invest_06(total_money, annualized_rate, least_money, from_notice_to_reservation_hours, from_reservation_start_to_reservation_end, from_reservation_end_to_paystop, from_paystop_to_begain_order, from_begain_order_to_lock_order, from_lock_time_to_exit_time, format, date_sys_start_time);
			// 保存联富定投3个月单子
			boolean bool_03 = save_lfoll_invest_03(total_money, annualized_rate, least_money, from_notice_to_reservation_hours, from_reservation_start_to_reservation_end, from_reservation_end_to_paystop, from_paystop_to_begain_order, from_begain_order_to_lock_order, from_lock_time_to_exit_time, format, date_sys_start_time);
			if (bool_12 && bool_06 && bool_03) {
				date_sys_start_time = LfollInvestConfigUtil.get_next_days_Date(date_sys_start_time, index_day);
			}

		}
		renderText("13");// 设置成功!
		return;
	}

	/**
	 * 保存联富宝（定投）12个月期限的配置
	 * 
	 * @param total_money
	 * @param annualized_rate
	 * @param least_money
	 * @param from_notice_to_reservation_hours
	 * @param from_reservation_start_to_reservation_end
	 * @param from_reservation_end_to_paystop
	 * @param from_paystop_to_begain_order
	 * @param from_begain_order_to_lock_order
	 * @param from_lock_time_to_exit_time
	 * @param format
	 * @param date_sys_start_time
	 * @return
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "保存联富宝配置信息", last_update_author = "hx")
	private boolean save_lfoll_invest_12(Integer total_money, BigDecimal annualized_rate, Integer least_money, Integer from_notice_to_reservation_hours, Integer from_reservation_start_to_reservation_end, Integer from_reservation_end_to_paystop, Integer from_paystop_to_begain_order, Integer from_begain_order_to_lock_order, Integer from_lock_time_to_exit_time, SimpleDateFormat format,
			Date date_sys_start_time) {
		String format_start_time;
		String bid_name = "联富宝L12-";
		// 获取预定开始时间
		Date booking_start_date = LfollInvestConfigUtil.get_next_hours_Date(date_sys_start_time, from_notice_to_reservation_hours);
		format_start_time = format.format(booking_start_date);
		format_start_time = format_start_time.replaceAll("-", "");
		bid_name = bid_name + format_start_time.substring(2, 8);
		// 预定结束时间
		Date booking_end_date = LfollInvestConfigUtil.get_next_hours_Date(booking_start_date, from_reservation_start_to_reservation_end);
		// 支付截止时间
		Date payment_end_date = LfollInvestConfigUtil.get_next_hours_Date(booking_end_date, from_reservation_end_to_paystop);
		// 投标开始时间
		Date invite_start_date = LfollInvestConfigUtil.get_next_hours_Date(payment_end_date, from_paystop_to_begain_order);
		// 投标结束时间
		Date invite_end_date = LfollInvestConfigUtil.get_next_hours_Date(invite_start_date, from_begain_order_to_lock_order);
		// 获取到期退出时间
		Date repayment_date = LfollInvestConfigUtil.get_next_month_Date(invite_end_date, 12);

		FixBidSystemOrderM fix_bid_system_orderM = new FixBidSystemOrderM();
		fix_bid_system_orderM.set("bid_name", bid_name);// 联富定投名称
		fix_bid_system_orderM.set("total_money", total_money);// 总发售金额
		fix_bid_system_orderM.set("sold_money", 0);// 已售出金额
		fix_bid_system_orderM.set("least_money", least_money);// 起投金额
		fix_bid_system_orderM.set("annualized_rate", annualized_rate);// 年化利率
		fix_bid_system_orderM.set("add_time", new Date());// 添加时间
		fix_bid_system_orderM.set("add_time_long", System.currentTimeMillis());// 毫秒时间戳
		fix_bid_system_orderM.set("is_show", 0);// 默认不挂出定投
		fix_bid_system_orderM.set("advance_notice_start_time", date_sys_start_time);// 预告开始时间
		fix_bid_system_orderM.set("advance_notice_start_time_long", date_sys_start_time.getTime());// 预告开始时间时间戳
		fix_bid_system_orderM.set("reserve_start_time", booking_start_date);// 预定开始时间
		fix_bid_system_orderM.set("reserve_start_time_long", booking_start_date.getTime());// 预定开始时间的毫秒数
		fix_bid_system_orderM.set("reserve_end_time", booking_end_date);// 预定结束时间
		fix_bid_system_orderM.set("reserve_end_time_long", booking_end_date.getTime());// 预定结束时间的毫秒数
		fix_bid_system_orderM.set("pay_end_time", payment_end_date);// 支付截止时间
		fix_bid_system_orderM.set("pay_end_time_long", payment_end_date.getTime());// 支付截止毫秒数
		fix_bid_system_orderM.set("invite_start_time", invite_start_date);// 投标开始时间
		fix_bid_system_orderM.set("invite_start_time_long", invite_start_date.getTime());// 投标开始时间的毫秒数
		fix_bid_system_orderM.set("invite_end_time", invite_end_date);// 投标结束时间
		fix_bid_system_orderM.set("invite_end_time_long", invite_end_date.getTime());
		fix_bid_system_orderM.set("closed_period", 12);// 封闭期（月）
		fix_bid_system_orderM.set("repayment_time", repayment_date);// 还款时间（退出定投时间）',
		fix_bid_system_orderM.set("repayment_time_long", repayment_date.getTime());// 退出定投时间毫秒数
		// 暂时确定为0
		fix_bid_system_orderM.set("repatment_interest", new BigDecimal("0"));// 还款利息
		boolean bool = fix_bid_system_orderM.save();
		return bool;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "保存联富宝配置信息", last_update_author = "hx")
	private boolean save_lfoll_invest_06(Integer total_money, BigDecimal annualized_rate, Integer least_money, Integer from_notice_to_reservation_hours, Integer from_reservation_start_to_reservation_end, Integer from_reservation_end_to_paystop, Integer from_paystop_to_begain_order, Integer from_begain_order_to_lock_order, Integer from_lock_time_to_exit_time, SimpleDateFormat format,
			Date date_sys_start_time) {
		String format_start_time;
		String bid_name = "联富宝L06-";
		// 获取预定开始时间
		Date booking_start_date = LfollInvestConfigUtil.get_next_hours_Date(date_sys_start_time, from_notice_to_reservation_hours);
		format_start_time = format.format(booking_start_date);
		format_start_time = format_start_time.replaceAll("-", "");
		bid_name = bid_name + format_start_time.substring(2, 8);
		// 预定结束时间
		Date booking_end_date = LfollInvestConfigUtil.get_next_hours_Date(booking_start_date, from_reservation_start_to_reservation_end);
		// 支付截止时间
		Date payment_end_date = LfollInvestConfigUtil.get_next_hours_Date(booking_end_date, from_reservation_end_to_paystop);
		// 投标开始时间
		Date invite_start_date = LfollInvestConfigUtil.get_next_hours_Date(payment_end_date, from_paystop_to_begain_order);
		// 投标结束时间
		Date invite_end_date = LfollInvestConfigUtil.get_next_hours_Date(invite_start_date, from_begain_order_to_lock_order);
		// 获取到期退出时间
		Date repayment_date = LfollInvestConfigUtil.get_next_month_Date(invite_end_date, 6);

		FixBidSystemOrderM fix_bid_system_orderM = new FixBidSystemOrderM();
		fix_bid_system_orderM.set("bid_name", bid_name);// 联富定投名称
		fix_bid_system_orderM.set("total_money", total_money);// 总发售金额
		fix_bid_system_orderM.set("sold_money", 0);// 已售出金额
		fix_bid_system_orderM.set("least_money", least_money);// 起投金额
		fix_bid_system_orderM.set("annualized_rate", annualized_rate);// 年化利率
		fix_bid_system_orderM.set("add_time", new Date());// 添加时间
		fix_bid_system_orderM.set("add_time_long", System.currentTimeMillis());// 毫秒时间戳
		fix_bid_system_orderM.set("is_show", 0);// 默认不挂出定投
		fix_bid_system_orderM.set("advance_notice_start_time", date_sys_start_time);// 预告开始时间
		fix_bid_system_orderM.set("advance_notice_start_time_long", date_sys_start_time.getTime());// 预告开始时间时间戳
		fix_bid_system_orderM.set("reserve_start_time", booking_start_date);// 预定开始时间
		fix_bid_system_orderM.set("reserve_start_time_long", booking_start_date.getTime());// 预定开始时间的毫秒数
		fix_bid_system_orderM.set("reserve_end_time", booking_end_date);// 预定结束时间
		fix_bid_system_orderM.set("reserve_end_time_long", booking_end_date.getTime());// 预定结束时间的毫秒数
		fix_bid_system_orderM.set("pay_end_time", payment_end_date);// 支付截止时间
		fix_bid_system_orderM.set("pay_end_time_long", payment_end_date.getTime());// 支付截止毫秒数
		fix_bid_system_orderM.set("invite_start_time", invite_start_date);// 投标开始时间
		fix_bid_system_orderM.set("invite_start_time_long", invite_start_date.getTime());// 投标开始时间的毫秒数
		fix_bid_system_orderM.set("invite_end_time", invite_end_date);// 投标结束时间
		fix_bid_system_orderM.set("invite_end_time_long", invite_end_date.getTime());
		fix_bid_system_orderM.set("closed_period", 6);// 封闭期（月）
		fix_bid_system_orderM.set("repayment_time", repayment_date);// 还款时间（退出定投时间）',
		fix_bid_system_orderM.set("repayment_time_long", repayment_date.getTime());// 退出定投时间毫秒数
		// 暂时确定为0
		fix_bid_system_orderM.set("repatment_interest", new BigDecimal("0"));// 还款利息
		boolean bool = fix_bid_system_orderM.save();
		return bool;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "保存联富宝配置信息", last_update_author = "hx")
	private boolean save_lfoll_invest_03(Integer total_money, BigDecimal annualized_rate, Integer least_money, Integer from_notice_to_reservation_hours, Integer from_reservation_start_to_reservation_end, Integer from_reservation_end_to_paystop, Integer from_paystop_to_begain_order, Integer from_begain_order_to_lock_order, Integer from_lock_time_to_exit_time, SimpleDateFormat format,
			Date date_sys_start_time) {
		String format_start_time;
		String bid_name = "联富宝L03-";
		// 获取预定开始时间
		Date booking_start_date = LfollInvestConfigUtil.get_next_hours_Date(date_sys_start_time, from_notice_to_reservation_hours);
		format_start_time = format.format(booking_start_date);
		format_start_time = format_start_time.replaceAll("-", "");
		bid_name = bid_name + format_start_time.substring(2, 8);
		// 预定结束时间
		Date booking_end_date = LfollInvestConfigUtil.get_next_hours_Date(booking_start_date, from_reservation_start_to_reservation_end);
		// 支付截止时间
		Date payment_end_date = LfollInvestConfigUtil.get_next_hours_Date(booking_end_date, from_reservation_end_to_paystop);
		// 投标开始时间
		Date invite_start_date = LfollInvestConfigUtil.get_next_hours_Date(payment_end_date, from_paystop_to_begain_order);
		// 投标结束时间
		Date invite_end_date = LfollInvestConfigUtil.get_next_hours_Date(invite_start_date, from_begain_order_to_lock_order);
		// 获取到期退出时间
		Date repayment_date = LfollInvestConfigUtil.get_next_month_Date(invite_end_date, 3);

		FixBidSystemOrderM fix_bid_system_orderM = new FixBidSystemOrderM();
		fix_bid_system_orderM.set("bid_name", bid_name);// 联富定投名称
		fix_bid_system_orderM.set("total_money", total_money);// 总发售金额
		fix_bid_system_orderM.set("sold_money", 0);// 已售出金额
		fix_bid_system_orderM.set("least_money", least_money);// 起投金额
		fix_bid_system_orderM.set("annualized_rate", annualized_rate);// 年化利率
		fix_bid_system_orderM.set("add_time", new Date());// 添加时间
		fix_bid_system_orderM.set("add_time_long", System.currentTimeMillis());// 毫秒时间戳
		fix_bid_system_orderM.set("is_show", 0);// 默认不挂出定投
		fix_bid_system_orderM.set("advance_notice_start_time", date_sys_start_time);// 预告开始时间
		fix_bid_system_orderM.set("advance_notice_start_time_long", date_sys_start_time.getTime());// 预告开始时间时间戳
		fix_bid_system_orderM.set("reserve_start_time", booking_start_date);// 预定开始时间
		fix_bid_system_orderM.set("reserve_start_time_long", booking_start_date.getTime());// 预定开始时间的毫秒数
		fix_bid_system_orderM.set("reserve_end_time", booking_end_date);// 预定结束时间
		fix_bid_system_orderM.set("reserve_end_time_long", booking_end_date.getTime());// 预定结束时间的毫秒数
		fix_bid_system_orderM.set("pay_end_time", payment_end_date);// 支付截止时间
		fix_bid_system_orderM.set("pay_end_time_long", payment_end_date.getTime());// 支付截止毫秒数
		fix_bid_system_orderM.set("invite_start_time", invite_start_date);// 投标开始时间
		fix_bid_system_orderM.set("invite_start_time_long", invite_start_date.getTime());// 投标开始时间的毫秒数
		fix_bid_system_orderM.set("invite_end_time", invite_end_date);// 投标结束时间
		fix_bid_system_orderM.set("invite_end_time_long", invite_end_date.getTime());
		fix_bid_system_orderM.set("closed_period", 3);// 封闭期（月）
		fix_bid_system_orderM.set("repayment_time", repayment_date);// 还款时间（退出定投时间）',
		fix_bid_system_orderM.set("repayment_time_long", repayment_date.getTime());// 退出定投时间毫秒数
		// 暂时确定为0
		fix_bid_system_orderM.set("repatment_interest", new BigDecimal("0"));// 还款利息
		boolean bool = fix_bid_system_orderM.save();
		return bool;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("--------------测试开始------------------");
		LfollInvestAction lfollInvestAction = new LfollInvestAction();
		HttpServletRequest request = ActionContext.getRequest();
		System.out.println("------request---------" + request);
		request.setAttribute("total_money", 1000000);
		request.setAttribute("annualized_rate", 0.09);
		request.setAttribute("least_money", 1000);
		request.setAttribute("system_start_time", "2014-10-29 10:00:00");
		request.setAttribute("system_end_time", "2015-10-29 10:00:00");

		request.setAttribute("index_day", 7);// 定投之间的间隔时间为7天
		request.setAttribute("from_notice_to_reservation_hours", 72);// 从通告开始
		// 到预订开始时间的小时数
		request.setAttribute("from_reservation_start_to_reservation_end", 31);

		request.setAttribute("from_reservation_end_to_paystop", 21);

		request.setAttribute("from_paystop_to_begain_order", 1);

		request.setAttribute("from_begain_order_to_lock_order", 67);

		request.setAttribute("from_lock_time_to_exit_time", 12);

		lfollInvestAction.create_all_lfoll_invest_order();
		System.out.println("----------------测试完成----------------");

		/*
		 * String str = "20141028";
		 * System.out.println("---------------"+str.substring(2, str.length()));
		 */
	}

}
