package com.tfoll.web.action.user.loan_order;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.BootStraoDiv;

import java.util.ArrayList;
import java.util.List;

@ActionKey("/user/loan_order/repaying")
public class RepayingAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入还款列表", last_update_author = "lh")
	public void repaying_list_index() {

		setSessionAttribute("real_name", null);
		setSessionAttribute("borrow_duration", -1);
		setSessionAttribute("annulized_rate_int", -1);
		setSessionAttribute("borrow_all_money_level", -1);
		setSessionAttribute("payment_state", 2);

		setAttribute("real_name", null);
		setAttribute("borrow_duration", -1);
		setAttribute("annulized_rate_int", -1);
		setAttribute("borrow_all_money_level", -1);
		setAttribute("payment_state", 2);

		renderJsp("/user/loan_order/repaying/repaying_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "还款列表查询", last_update_author = "lh")
	public void repaying_list_query() {

		String real_name = getParameter("real_name");
		int borrow_duration = getParameterToInt("borrow_duration", -1);
		int annulized_rate_int = getParameterToInt("annulized_rate_int", -1);
		int borrow_all_money_level = getParameterToInt("borrow_all_money_level", -1);
		int payment_state = getParameterToInt("payment_state", 2);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("borrow_duration", borrow_duration);
		setSessionAttribute("annulized_rate_int", annulized_rate_int);
		setSessionAttribute("borrow_all_money_level", borrow_all_money_level);
		setSessionAttribute("payment_state", payment_state);

		setAttribute("real_name", real_name);
		setAttribute("borrow_duration", borrow_duration);
		setAttribute("annulized_rate_int", annulized_rate_int);
		setAttribute("borrow_all_money_level", borrow_all_money_level);
		setAttribute("payment_state", payment_state);

		StringBuilder sql_select = new StringBuilder();
		StringBuilder sql_count = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql_select.append("SELECT u.real_name,gmo.* FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		sql_count.append("SELECT count(1) FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_select.append(" and u.real_name=? ");
			sql_count.append(" and u.real_name=? ");
			params.add(real_name);
		}
		if (borrow_duration != -1) {
			sql_select.append(" and gmo.borrow_duration=? ");
			sql_count.append(" and gmo.borrow_duration=? ");
			params.add(borrow_duration);
		}
		if (annulized_rate_int != -1) {
			sql_select.append(" and gmo.annulized_rate_int=? ");
			sql_count.append(" and gmo.annulized_rate_int=? ");
			params.add(annulized_rate_int);
		}
		if (borrow_all_money_level != -1) {
			sql_select.append(" and gmo.borrow_all_money_level=? ");
			sql_count.append(" and gmo.borrow_all_money_level=? ");
			params.add(borrow_all_money_level);
		}
		if (1 == 1) {
			sql_select.append(" and gmo.payment_state = ? ");
			sql_count.append(" and gmo.payment_state = ? ");
			params.add(payment_state);
		}
		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, 1);

			List<Record> gather_money_order_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(gather_money_order_list)) {
				setAttribute("gather_money_order_list", gather_money_order_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/repaying/repaying_list_page");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/repaying/repaying_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "还款列表查询", last_update_author = "lh")
	public void repaying_list_page() {

		int pn = getParameterToInt("pn", 1);
		String real_name = getParameter("real_name");
		int borrow_duration = getParameterToInt("borrow_duration", -1);
		int annulized_rate_int = getParameterToInt("annulized_rate_int", -1);
		int borrow_all_money_level = getParameterToInt("borrow_all_money_level", -1);
		int payment_state = getParameterToInt("payment_state", 2);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("borrow_duration", borrow_duration);
		setSessionAttribute("annulized_rate_int", annulized_rate_int);
		setSessionAttribute("borrow_all_money_level", borrow_all_money_level);
		setSessionAttribute("payment_state", payment_state);

		setAttribute("real_name", real_name);
		setAttribute("borrow_duration", borrow_duration);
		setAttribute("annulized_rate_int", annulized_rate_int);
		setAttribute("borrow_all_money_level", borrow_all_money_level);
		setAttribute("payment_state", payment_state);

		StringBuilder sql_select = new StringBuilder();
		StringBuilder sql_count = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql_select.append("SELECT u.real_name,gmo.* FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		sql_count.append("SELECT count(1) FROM user_info u, borrower_bulk_standard_gather_money_order gmo WHERE 1 = 1 AND gmo.user_id = u.id ");
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_select.append(" and u.real_name=? ");
			sql_count.append(" and u.real_name=? ");
			params.add(real_name);
		}
		if (borrow_duration != -1) {
			sql_select.append(" and gmo.borrow_duration=? ");
			sql_count.append(" and gmo.borrow_duration=? ");
			params.add(borrow_duration);
		}
		if (annulized_rate_int != -1) {
			sql_select.append(" and gmo.annulized_rate_int=? ");
			sql_count.append(" and gmo.annulized_rate_int=? ");
			params.add(annulized_rate_int);
		}
		if (borrow_all_money_level != -1) {
			sql_select.append(" and gmo.borrow_all_money_level=? ");
			sql_count.append(" and gmo.borrow_all_money_level=? ");
			params.add(borrow_all_money_level);
		}
		if (1 == 1) {
			sql_select.append(" and gmo.payment_state = ? ");
			sql_count.append(" and gmo.payment_state = ? ");
			params.add(payment_state);
		}
		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> gather_money_order_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(gather_money_order_list)) {
				setAttribute("gather_money_order_list", gather_money_order_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/repaying/repaying_list_page");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/repaying/repaying_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "还款详细", last_update_author = "lh")
	public void repayment_detail() {
		int gather_order_id = getParameterToInt("gather_order_id");

		String sql = "select * from `borrower_bulk_standard_repayment_plan` where gather_money_order_id = ? ";
		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find(sql, new Object[] { gather_order_id });
		setAttribute("repayment_plan_list", borrower_bulk_standard_repayment_plan_list);

		renderJsp("/user/loan_order/repaying/repaying_detail.jsp");
		return;
	}

}
