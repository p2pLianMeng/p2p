package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_gather_money_order_waiting", primaryKey = "id")
public class BorrowerBulkStandardGatherMoneyOrderWaitingM extends Model<BorrowerBulkStandardGatherMoneyOrderWaitingM> {
	public static BorrowerBulkStandardGatherMoneyOrderWaitingM dao = new BorrowerBulkStandardGatherMoneyOrderWaitingM();
}
