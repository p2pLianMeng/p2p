package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "admin_role_ralation_module", primaryKey = "id")
public class AdminRoleRalationModuleM extends Model<AdminRoleRalationModuleM> {
	public static AdminRoleRalationModuleM dao = new AdminRoleRalationModuleM();
}
