package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_appointment", primaryKey = "id")
public class UserAppointmentM extends Model<UserAppointmentM> {
	public static UserAppointmentM dao = new UserAppointmentM();
}
