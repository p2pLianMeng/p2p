package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.util.Calendar;
import java.util.Date;

@TableBind(tableName = "user_info", primaryKey = "id")
public class UserM extends Model<UserM> {
	public static UserM dao = new UserM();

	public static UserM getUserM(int user_id) {
		return UserM.dao.findFirst("select * from user_info WHERE id=?  for UPDATE", new Object[] { user_id });
	}

	// 根据用户id获得性别
	public static String getSex(int user_id) {
		UserM user = UserM.dao.findById(user_id);
		String user_identity = user.getString("user_identity");

		int sex_num = user_identity.charAt(16);
		String sex = null;
		if (sex_num % 2 == 0) {
			sex = "女";
		} else {
			sex = "男";
		}
		return sex;
	}

	// 根据用户id获得年龄
	public static String getAge(int user_id) throws Exception {
		UserM user = UserM.dao.findById(user_id);
		String user_identity = user.getString("user_identity");
		String birthday = user_identity.substring(6, 14);

		Date date = new Date();
		date = Model.$Date.parse(birthday);

		Calendar calendar = Calendar.getInstance();
		// 获取当前时间毫秒值
		long now = (new Date()).getTime();
		long birthdate = date.getTime();
		long time = now - birthdate;
		int count = 0;
		// 时间换算
		long days = time / 1000 / 60 / 60 / 24;
		// 判断闰年
		int birth_year = Integer.parseInt((birthday.substring(0, 4)));

		for (int i = calendar.get(Calendar.YEAR); i >= birth_year; i--) {
			if ((i % 4 == 0 && !(i % 100 == 0)) || (i % 400 == 0)) {
				count++;
			}
		}
		// 加入闰年因素进行整理换算
		String age = String.valueOf((days - count) / 365);
		return age;
	}

	// 根据用户id获得生日
	public static String getBirthday(int user_id) throws Exception {
		UserM user = UserM.dao.findById(user_id);
		String user_identity = user.getString("user_identity");
		String birthday = user_identity.substring(6, 14);

		String year = birthday.substring(0, 4);
		String month = birthday.substring(4, 6);
		String day = birthday.substring(6, 8);
		String birthday2 = year + "-" + month + "-" + day;

		return birthday2;
	}
}
