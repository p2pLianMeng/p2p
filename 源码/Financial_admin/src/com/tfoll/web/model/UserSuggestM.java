package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_suggestion", primaryKey = "user_id")
public class UserSuggestM extends Model<UserSuggestM> {
	public static UserSuggestM dao = new UserSuggestM();
}
