package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_now_money", primaryKey = "user_id")
public class UserNowMoneyM extends Model<UserNowMoneyM> {
	public static UserNowMoneyM dao = new UserNowMoneyM();

	/**
	 * 获取用户的资金信息 （不锁表）
	 * 
	 * @param user_id
	 * @return
	 */
	public static UserNowMoneyM get_user_current_money(Integer user_id) {
		String sql = "select * from user_now_money where user_id = ?";
		return UserNowMoneyM.dao.findFirst(sql, new Object[] { user_id });

	}

	/**
	 * 获取用户的资金信息 （锁表）
	 * 
	 * @param user_id
	 * @return
	 */
	public static UserNowMoneyM get_user_current_money_for_update(Integer user_id) {
		String sql = "select * from user_now_money where user_id = ? for update";
		return UserNowMoneyM.dao.findFirst(sql, new Object[] { user_id });

	}
}
