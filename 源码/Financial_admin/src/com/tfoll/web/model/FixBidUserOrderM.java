package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "fix_bid_user_order", primaryKey = "id")
public class FixBidUserOrderM extends Model<FixBidUserOrderM> implements Serializable {

	private static final long serialVersionUID = -3639570065600081234L;

	public static FixBidUserOrderM dao = new FixBidUserOrderM();
}
