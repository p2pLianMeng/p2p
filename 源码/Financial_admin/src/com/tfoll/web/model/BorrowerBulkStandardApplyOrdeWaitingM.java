package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_apply_order_waiting", primaryKey = "id")
public class BorrowerBulkStandardApplyOrdeWaitingM extends Model<BorrowerBulkStandardApplyOrdeWaitingM> {
	public static BorrowerBulkStandardApplyOrdeWaitingM dao = new BorrowerBulkStandardApplyOrdeWaitingM();
}
