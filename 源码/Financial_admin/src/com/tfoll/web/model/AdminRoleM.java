package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "admin_role", primaryKey = "id")
public class AdminRoleM extends Model<AdminRoleM> {
	public static AdminRoleM dao = new AdminRoleM();
}
