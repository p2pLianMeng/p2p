package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "admin_dept", primaryKey = "dept_id")
public class AdminDeptM extends Model<AdminDeptM> implements Serializable {
	private static final long serialVersionUID = -4503054112919855075L;
	public static AdminDeptM dao = new AdminDeptM();
}
