package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "fix_bid_user_hold", primaryKey = "id")
public class FixBidUserHoldM extends Model<FixBidUserHoldM> implements Serializable {

	private static final long serialVersionUID = -4581396011470744251L;
	public static FixBidUserHoldM dao = new FixBidUserHoldM();
}
