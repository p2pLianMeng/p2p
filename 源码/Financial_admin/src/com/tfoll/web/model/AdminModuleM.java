package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "admin_module", primaryKey = "id")
public class AdminModuleM extends Model<AdminModuleM> {
	public static AdminModuleM dao = new AdminModuleM();
}
