package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "sys_income_records", primaryKey = "id")
public class SysIncomeRecordsM extends Model<SysIncomeRecordsM> {
	public static SysIncomeRecordsM dao = new SysIncomeRecordsM();
}
