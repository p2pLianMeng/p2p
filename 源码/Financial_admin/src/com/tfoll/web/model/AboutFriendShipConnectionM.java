package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "about_friendship_connetion", primaryKey = "id")
public class AboutFriendShipConnectionM extends Model<AboutFriendShipConnectionM> implements Serializable {

	private static final long serialVersionUID = -2687308498214444144L;
	public static AboutFriendShipConnectionM dao = new AboutFriendShipConnectionM();
}
