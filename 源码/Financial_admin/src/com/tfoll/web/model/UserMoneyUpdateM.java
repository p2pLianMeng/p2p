package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "user_money_update", primaryKey = "id")
public class UserMoneyUpdateM extends Model<UserMoneyUpdateM> implements Serializable {

	private static final long serialVersionUID = 1L;
	public static UserMoneyUpdateM dao = new UserMoneyUpdateM();
}