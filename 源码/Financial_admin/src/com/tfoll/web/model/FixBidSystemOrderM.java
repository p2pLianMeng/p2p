package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

/**
 * 联富定投表
 * 
 * @author HangSun
 * 
 */
@TableBind(tableName = "fix_bid_system_order", primaryKey = "id")
public class FixBidSystemOrderM extends Model<FixBidSystemOrderM> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1773601101021450220L;

	public static FixBidSystemOrderM dao = new FixBidSystemOrderM();
}
