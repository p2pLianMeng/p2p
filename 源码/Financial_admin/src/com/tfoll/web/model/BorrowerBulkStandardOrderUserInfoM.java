package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_order_user_info", primaryKey = "id")
public class BorrowerBulkStandardOrderUserInfoM extends Model<BorrowerBulkStandardOrderUserInfoM> {
	public static BorrowerBulkStandardOrderUserInfoM dao = new BorrowerBulkStandardOrderUserInfoM();
}
