package com.tfoll.web.util.page;

import com.tfoll.trade.config.Constants;
import com.tfoll.trade.util.page.IPageDiv;

import javax.servlet.http.HttpServletRequest;

public class BsPageDiv implements IPageDiv {
	/**
	 * currentPageNum代表是在数据库经过计算的真实的页数
	 */
	public static String getDiv(String url, long totalPageNum, long currentPageNum, long pageSize) {

		/*
		 * 10122 条记录 1/507 页 <a href='#'>下一页</a> <span
		 * class='current'>1</span><a href='#'>2</a><a
		 * href='/chinapost/index.php?m=Label&a=index&p=3'>3</a><a
		 * href='#'>4</a><a href='#'>5</a> <a href='#'>下5页</a> <a
		 * href='#'>最后一页</a>
		 */

		/*
		 * // 上三页-下三页-最小页数和最大页数 // 当前页左右分别判断 // 前面到当前页小于3可能为0,1,2三种情况 if
		 * (totalPageNum == 1 || totalPageNum == 0) { return ""; // 只有一页 } else
		 * { StringBuilder div = new StringBuilder(240); div.append("<ul>"); //
		 * currentPageNum一定大于等于1小于totalPageNum同时会出现currentPageNum永远不会出现在分页的链接两边的情况
		 * if (currentPageNum - 8 > 1) { div.append("<a href='" + url +
		 * "?pn=1'>" + "首页" + "</a>"); } for (long i = currentPageNum - 8; i <=
		 * currentPageNum + 8; i++) { // 可能存在三种情况:第一个,中间一个,最后一个
		 * 
		 * if (i <= 0 || i > totalPageNum) { // 则不进行分页 } else { //
		 * 判断是不是[当前页,第一页,最后一页] // i>=1 if (i == currentPageNum) {
		 * div.append("<a style='color:black;'>" + i + "</a>"); } else if (i ==
		 * 1) { div.append("<a href='" + url + "?pn=1'>" + "首页" + "</a>"); }
		 * else if (i == totalPageNum) { div.append("<a href='" + url + "?pn=" +
		 * totalPageNum + "'>" + "末页" + "</a>"); } else { div.append("<a href='"
		 * + url + "?pn=" + i + "'>" + i + "</a>"); } } } if (currentPageNum + 8
		 * < totalPageNum) { div.append("<a href='" + url + "?pn=" +
		 * totalPageNum + "'>" + "末页" + "</a>"); } div.append("</ul>"); return
		 * div.toString();
		 */
		return "";

	}

	/**
	 * 生成http协议的url
	 */
	public static String createUrl(HttpServletRequest request, String actionUrl) {
		String path = request.getContextPath();
		String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
		String url = __ROOT_PATH__ + actionUrl;
		if (Constants.actionExtension != null && !"".equals(Constants.actionExtension)) {
			url = url + Constants.actionExtension;
		}
		return url;
	}

	public void ok() {
		// TODO Auto-generated method stub

	}

}