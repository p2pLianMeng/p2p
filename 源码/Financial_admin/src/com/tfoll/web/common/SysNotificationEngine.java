package com.tfoll.web.common;

/**
 * 系统通知引擎
 * 
 */
public class SysNotificationEngine {

	/**
	 * 邮件-短信-系统
	 * 
	 * <pre>
	 * 系统显示的-只需要把这部分的消息让用户查询即可
	 * 邮件发送的-只需要把这部分的消息同时写入到邮件发送引擎里面
	 * 短信发送的-只需要把这部分的消息同时写入到短信发送引擎里面
	 * </pre>
	 */

}
