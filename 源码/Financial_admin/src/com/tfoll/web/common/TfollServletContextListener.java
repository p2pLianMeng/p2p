package com.tfoll.web.common;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 
 * 通过在一个static类里面注册一个ServletContextListener实例，在定时器里面，
 * 对static类的操作会自动更新到ServletContext中
 * 
 * @author 330937205@qq.com
 * 
 */
public class TfollServletContextListener implements ServletContextListener {

	public static ServletContext servletContext = null;

	public void contextDestroyed(ServletContextEvent servletContextEvent) {

	}

	public void contextInitialized(ServletContextEvent arg0) {

	}

}
