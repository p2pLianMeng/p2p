﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.action.index.IndexAction"%>
<%@page import="com.tfoll.web.util.Utils"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="天富网，比特币交易，比特币交易网，莱特币交易，莱特币交易网，凯特币交易，凯特币交易网，买比特币，卖比特币，挖比特币，bitcoin，litecoin，btc，ltc，ctc，交易，交易所" />
		<meta name="description" content="Tfoll天富网是国内最大最专业的比特币、莱特币、凯特币交易平台。采用ssl、冷存储、gslb、分布式服务器等先进技术，确保每一笔交易都安全可靠，而且高效便捷。" />				
		<title>天富网交易平台</title>

		<link href="<%=__PUBLIC__%>/style/main.css" type="text/css" rel="stylesheet" />
		<link href="<%=__PUBLIC__%>/css/lrtk.css" type="text/css" rel="stylesheet" />
		<link href="<%=__PUBLIC__%>/css/css.css" type="text/css" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css"rel="stylesheet" />
		<link href="<%=__PUBLIC__%>/css/jquery.jslides.css" type="text/css" rel="stylesheet" />
		<link href="<%=__PUBLIC__%>/css/css.css" rel="stylesheet">
		
	</head>
	<body style="font-family: '微软雅黑'">
		<!-- 顶部通用模板 Start -->
		<jsp:include page="/jsp/index/user_top.jsp" />
		<!-- 顶部通用模板 END -->

</head>
<body style="font-family:'微软雅黑'"> 
	

		<%
			boolean isSow = false;

			Integer isOk=(Integer)request.getAttribute("isOk");
			String e=(String)request.getAttribute("e");
			String u=(String)request.getAttribute("u");
			if(isOk!=null){
			isSow=true;
			}
			
			if (isSow) {
				request.setAttribute("isShow", 1);
			}
		%>
		
		<script type="text/javascript">
		function user_password_update_2_email(){
			//alert("进人了修改密码");
			var email=$("#e").val();
			var u=$("#u").val();
			
			var password=$("#pwd").val();
			var password2=$("#pwd2").val();
			var code=$("#_code").val();

			if(password==null||password==''){
			    $("#pwd_msg").html("密码不能为空");
			    return;
			}
			if(password.length < 6){
				 $("#pwd_msg").html("密码位数小于6位");
				 return;
			}
			if(password2==null||password2==''){
			    $("#pwd2_error").html("确认密码不能为空");
			    return;
			}
			
			if(password2!=password){
			    $("#pwd2_error").html("输入的两次密码不一致");
			    return;
			}
			if(code==null||code==''){
			    $("#_code_error").html("验证码不能为空");
			    return;
			}
		
	
			_user_password_update(email,u,password,code);
		}

	
	function _user_password_update(_email,_u,_password,_code) {
								//需要进行日志记录，记录在什么时候谁进行充值的
								$.get(
												'<%=__ROOT_PATH__%>/index/user_password_update.html',
												{
													e : _email,
													u : _u,
													password : _password,
													code:_code
												},
												function(data) //回传函数
												{
													//alert(data);

													$result = $("#pwd_msg");
					
												if (data == -2) {
													$("#_code_error").html("输入验证码错误");
													//$result.html("输入验证码不成功");
												}else if (data == -1) {
													$result.html("系统不存在此帐号");
												} else if (data == 0) {
													$result.html("修改失败");
												} else if (data == 1) {
													$result.html("修改成功");
													setTimeout("window.location.href('<%=__ROOT_PATH__%>/index/index.html')",2000);
												} else {
													$result.html("修改失败");
												}
				
												}, "html");

							}
</script>


	
		<!--------------------------------------------------------------- 头部结束 -->

		

		<div class="geren">

			<c:choose>
				<c:when test="${isShow!=null}">
					<div class="mima">
						<h2>
							找回登录密码
						</h2>
						<h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="33%" height="60" align="right">
										<span class="hongse">*</span> 密码：
									</td>
									<td width="20%" height="60">
										<input name="" type="password" class="tixian_an" id="pwd" />				
										<input type="hidden"  id="e" value="<%=e%>" />
										<input type="hidden"  id="u" value="<%=u%>" />
									</td>
									<td width="47%" height="60"><span id="pwd_msg" style="color: red"> </span></td>
								</tr>
								<tr>
									<td width="33%" height="60" align="right">
										<span class="hongse">*</span> 确认密码：
									</td>
									<td width="20%" height="60">
										<input name="" type="password" class="tixian_an" id="pwd2" />

									</td>
									<td width="47%" height="60"><span id="pwd2_error" style="color: red"> </span></td>
								</tr>


								<tr>
									<td width="33%" height="60" align="right">
										<span class="hongse">*</span> 验证码：
									</td>
									<td width="20%" height="60">
										<input name="" type="text" class="tixian_an" id="_code" />

									</td>
									<td width="47%" height="60"><span id="_code_error" style="color: red"> </span></td>
								</tr>
								<tr>
									<td width="33%" height="60" align="right">
										<span class="hongse">*</span> 验证码：
									</td>
									<td width="67%" height="60" colspan="2">
										<img id="CheckCodeServlet" src="<%=__ROOT_PATH__%>/code.jpg"
											title="看不清楚，点击获得验证码" onclick="refresh();"
											style="border: #000000 1px dotted; padding-bottom: 0;" />
										<script>
									function refresh(){
									var CheckCodeServlet=document.getElementById("CheckCodeServlet");
									CheckCodeServlet.src="<%=__ROOT_PATH__%>/code.jpg?id="+ Math.random();}
									</script>


										<a href="#" class="lanse" onclick="refresh();">看不清楚换一张？</a>
									</td>
								</tr>
								<tr>
									<td height="60">
										&nbsp;
									</td>
									<td height="60" colspan="2">
										<input type="button" class="dl_an" value="修改密码"
											onclick="user_password_update_2_email();" />
									</td>
								</tr>
							</table>
						</h3>
					</div>
				</c:when>
				<c:otherwise>
					<div class="mima">
						<h2>
							找回登录密码
						</h2>
						<h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="60" align="center" colspan="2">
										密码修改申请无效
									</td>
								</tr>
							</table>
						</h3>
					</div>

				</c:otherwise>

			</c:choose>





		</div>


		<!--------------------------------------------------------------- 内容结束 -->
<!-- 底部通用模板 Start -->
<jsp:include page="/jsp/index/user_foot.jsp" />
<!-- 底部通用模板 END -->

	</body>
</html>
