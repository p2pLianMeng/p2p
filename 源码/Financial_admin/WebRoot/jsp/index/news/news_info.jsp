﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="天富网，比特币交易，比特币交易网，莱特币交易，莱特币交易网，凯特币交易，凯特币交易网，买比特币，卖比特币，挖比特币，bitcoin，litecoin，btc，ltc，ctc，交易，交易所" />
		<meta name="description" content="Tfoll天富网是国内最大最专业的比特币、莱特币、凯特币交易平台。采用ssl、冷存储、gslb、分布式服务器等先进技术，确保每一笔交易都安全可靠，而且高效便捷。" />				
		<title>新闻资讯-天富(TFoll)网-国内最大最专业的虚拟币交易平台</title>

		<link href="<%=__PUBLIC__%>/css/css.css" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__%>/css/lrtk.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css"
			rel="stylesheet" />

	</head>
	<body style="font-family: '微软雅黑'">
		<!-- 顶部通用模板 Start -->
		<jsp:include page="../../index/user_top.jsp" />
		<!-- 顶部通用模板 END -->
		<div class="xinwen">
			<div class="xinwen_con">
				<div class="xinwen_con_left">
					<h2>
						<p id="title">
							<strong>${article.m.name}</strong>
						</p>
						<div id="add_time">
							<strong>${article.m.add_time}</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关键词：${article.m.key}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: red">热度:&nbsp;${article.m.point}℃</span>
						</div>
					
					</h2>
					<h3>
						<!--<strong><span id="author">作者：${article.m.author}</span> </strong>
						<br />
						<br />
						<br />-->

						<div id="content"></div>
							${article.m.content}
						<div>
						<ul style="text-align: right;">
							<c:choose>
								<c:when test="${articleBefore_id==0}"></c:when>
								<c:otherwise>
							    <a href="<%=__ROOT_PATH__%>/index/news/news_detail.html?id=${articleBefore_id}">上一篇</a>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${articleAfter_id==0}"></c:when>
								<c:otherwise><a href="<%=__ROOT_PATH__%>/index/news/news_detail.html?id=${articleAfter_id}">下一篇</a></c:otherwise>
							</c:choose>
						
						</ul>
						</div>
					</h3>
					<h4>
						分享到：
						<a class="shareto_button" href="http://shareto.com.cn/share.html"><img
								src="http://s.shareto.com.cn/btn/lg-share-cn.gif" width="135"
								height="21" alt="分享道" style="border: 0" /> </a>
						<script type="text/javascript"
							src="http://s.shareto.com.cn/js/shareto_button.js"
							charset="utf-8"></script>
						<span><a href="#"><img
									src="<%=__PUBLIC__%>/images/sina_weibo_2.jpg" /> </a> <a href="#"><img
									src="<%=__PUBLIC__%>/images/tencent_weibo_2.jpg" /> </a> </span>
					</h4>
				</div>
				<div class="xinwen_con_right">
					<!--<h2>
						每日分析
					</h2>
					<ul>
						<c:forEach items="${tfoll_article_list}" var="article">
							<li>
								<a
									href="<%=__ROOT_PATH__%>/index/news/news_detail.html?id=${article.m.article_id}">${article.m.name}</a>
							</li>

						</c:forEach>
					</ul>
					--><h2>
						最新相关信息
					</h2>
					<ul>
						<c:forEach items="${last_article_list}" var="article">
							<li>
								<a
									href="<%=__ROOT_PATH__%>/index/news/news_detail.html?id=${article.m.article_id}">${article.m.name}</a>
							</li>
						</c:forEach>
					</ul>
					<h2>
						以往热门信息
					</h2>
					<ul>
						<c:forEach items="${hot_article_list}" var="article">
							<li>
								<a
									href="<%=__ROOT_PATH__%>/index/news/news_detail.html?id=${article.m.article_id}">${article.m.name}</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
		<!-- 底部通用模板 Start -->
		<jsp:include page="../../index/user_foot.jsp" />
		<!-- 底部通用模板 END -->
	</body>
</html>
