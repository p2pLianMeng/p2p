<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	//String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
	String __PUBLIC__ ="https://tfoll-cdn.oss.aliyuncs.com"; 
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://com.tfoll.web/lable/menu_show_tag" prefix="tfoll-menu" %>
<!-- 底部 Start -->
<div class="con4">
	<ul>
		<li>
			<img src="<%=__PUBLIC__%>/images/bottom_icon1.jpg"
				alt="Phone service" />
			<span>7*24小时热线<br /> 4006-888-923 </span>
		</li>
		<li>
			<img src="<%=__PUBLIC__%>/images/bottom_icon5.jpg" alt="QQ service" />
			<span>7*24QQ客服<br /> 4006888923 </span>
		</li>
		<li style="width: 180px">
			<img src="<%=__PUBLIC__%>/images/bottom_icon9.jpg"
				alt="Complaints Suggestions" />
			<span>投诉与建议<br /> support@tfoll.com </span>
		</li>
		<li style="float: right">
			<img src="<%=__PUBLIC__%>/images/bottom_icon3.jpg" />
			<span>天富网<br /> 新浪微博 </span>
		</li>
	</ul>
</div>
<!-- 底部 END -->

<!-- 关于我们 Start -->
<div class="con5">
	<ul>
		<li>
			<h2>
				关于我们
			</h2>
			<a href="<%=__ROOT_PATH__%>/index/about_service/tfoll_index.html">关于天富网</a><a href="<%=__ROOT_PATH__%>/index/about_service/team_index.html">天富网团队</a>
			<a href="<%=__ROOT_PATH__%>/index/about_service/web_ideas_index.html">理念和服务</a>
			<%--<a href="#">加入我们</a>
		--%></li>
		<li>
			<h2>
				服务条款
			</h2>
			<a href="<%=__ROOT_PATH__%>/index/about_service/agreement_index.html">用户协议</a><a href="<%=__ROOT_PATH__%>/index/about_service/law_index.html">法律声明</a><%--<a href="#">费率标准</a>
		--%></li>
		<li>
			<h2>
				新手入门
			</h2>
			<!--<a href="<%=__ROOT_PATH__%>/user/trade_manage/btc_trade/btc_buy_index.html">注册天富网</a>
			--><a href="<%=__ROOT_PATH__%>/user/acount/rmb_code/rmb_code_add_money_index.html">充值提现</a>
			<a href="<%=__ROOT_PATH__%>/index/about_service/question_index.html">疑问解答</a>
			<%--<a href="#">投诉建议</a>
		--%></li>
	</ul>
	<h1>
		<p>
			官方交流群1：
			<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=05d9dcecab7d5ca26a569a9c40daa8cfd6ff4c6a8f4675ac2b874cb1341e9f46"><img border="0" src="<%=__PUBLIC__%>/images/group.png" alt="天富网投资交流群1" title="天富网投资交流群1"></a>
			&nbsp;官方交流群2：
			<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=34001e5af0f04285d64824f3eb1ea9ecbead7b4711e57d6335fa5fff30b73bdd"><img border="0" src="<%=__PUBLIC__%>/images/group.png" alt="天富网投资交流群2" title="天富网投资交流群2"></a>
			<br />
			官方交流群3：
			<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=262ffa193873c7a334d1d9664a4628f8e459ee3e383da8a34ddec5f2bb276fec"><img border="0" src="<%=__PUBLIC__%>/images/group.png" alt="天富网投资交流群3" title="天富网投资交流群3"></a>
			&nbsp;官方交流群4：
			<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=14f88680376a60a5b4172591f45766ef44c0aa4531708e8afc66cd635183450d"><img border="0" src="<%=__PUBLIC__%>/images/group.png" alt="天富网投资交流群4" title="天富网投资交流群4"></a>
			<br />
			官方交流群6：
			<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=31e0ef52c10f58a4a3c012de8099cb9415e97215baa576628d78c5406c9f16c9"><img border="0" src="<%=__PUBLIC__%>/images/group.png" alt="天富网投资交流群6" title="天富网投资交流群6"></a>
		</p>
	</h1>
</div>
<!-- 关于我们 END -->

<!-- 风险提示 Start -->
<div class="tishi">
	<span class="hongse"><strong>风险提示：</strong> </span>比特币、莱特币、凯特币的交易都存在风险，作为全球的虚拟数字货币，它们都是全天24小时交易，没有涨跌限制，价格容易因为庄家、全球政府的政策影响而大幅波动，我们强烈建议您在自身能承受的风险范围内，参与虚拟货币交易。
</div>
<!-- 风险提示 END -->

<!-- 版权所有 Start -->
<div class="bottom">
	Copyright  2014 All Rights Reserved 广州天的富网络科技有限公司版权所有&nbsp;&nbsp;
	<a href="http://www.miitbeian.gov.cn">ICP证：粤ICP备14022597号</a>&nbsp;&nbsp;<script src="https://s4.cnzz.com/z_stat.php?id=1000472148&web_id=1000472148" language="JavaScript"></script>
</div>

    <DIV id=floatTools class=float0831>
  <DIV class=floatL>
  <A id=aFloatTools_Show class=btnOpen 
title=查看在线客服 onclick="javascript:$('#divFloatToolsView').animate({width: 'show', opacity: 'show'}, 'normal',function(){ $('#divFloatToolsView').show();kf_setCookie('RightFloatShown', 0, '', '/', '#'); });$('#aFloatTools_Show').attr('style','display:none');$('#aFloatTools_Hide').attr('style','display:block');" 
href="javascript:void(0);">展开</A>
<A id=aFloatTools_Hide class=btnCtn   style="DISPLAY:none"
title=关闭在线客服 onclick="javascript:$('#divFloatToolsView').animate({width: 'hide', opacity: 'hide'}, 'normal',function(){ $('#divFloatToolsView').hide();kf_setCookie('RightFloatShown', 1, '', '/', '#'); });$('#aFloatTools_Show').attr('style','display:block');$('#aFloatTools_Hide').attr('style','display:none');" 
href="javascript:void(0);">收缩</A>
  </DIV>
  <DIV id=divFloatToolsView class=floatR>
    <DIV class=tp></DIV>
    <DIV class=cn>
      <UL>
        <LI class=top>
          <H3 class=titZx>QQ咨询</H3>
        </LI>
        <LI><SPAN class=icoZx><a href="http://wpa.qq.com/msgrd?v=3&uin=4006888923&site=qq&menu=yes" target="_blank">在线咨询</a></SPAN> </LI>
      </UL>
      <UL class=webZx>
        <LI class=webZx-in><A href="#" target="_blank" style="FLOAT: left"><IMG src="<%=__PUBLIC__%>/images/right_float_web.png" border="0px"></A> </LI>
      </UL>
      <UL>
        <LI>
          <H3 class=titDh>电话咨询</H3>
        </LI>
        <LI><SPAN class=icoTl>4006-888-923</SPAN> </LI>
        <LI class=bot>
          <H3 class=titDc><A href="#" target="_blank">意见反馈</A></H3>
        </LI>
      </UL>
    </DIV>
  </DIV>
</DIV>


<!-- 版权所有 END -->
<script type="text/javascript" src="<%=__PUBLIC__%>/js/taobao.js" ></script>
<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>
<script type="text/javascript" src="<%=__PUBLIC__%>/js/loginDialog.js"></script>
<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.jslides.js"></script>
<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
<script type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
<script type=text/javascript src="<%=__PUBLIC__%>/js/jquery33.js"></script>
<script type=text/javascript src="<%=__PUBLIC__%>/js/kefu.js"></script>
<link type="text/css" rel="stylesheet" href="<%=__PUBLIC__%>/css/easydialog.css"  />
<link type=text/css rel="stylesheet" href="<%=__PUBLIC__%>/css/common2.css">
<script type="text/javascript">
    var coinPriceUrl="<%=__ROOT_PATH__%>/api/t/last_bargain_info.html";
	var coinOnlieUrl="<%=__ROOT_PATH__%>/api/t/get_time_info.html";
	var coinOrderBuyUrl="<%=__ROOT_PATH__%>/user/trade_info/public_trade_info/dummy_buy.html";
	var coinOrderSellUrl="<%=__ROOT_PATH__%>/user/trade_info/public_trade_info/dummy_sell.html";
	var userRegisterUrl="<%=__ROOT_PATH__%>/index/user_register.html";
	var userLoginUrl="<%=__ROOT_PATH__%>/index/login.html";
	var userAcountUrl ="<%=__ROOT_PATH__%>/user/user/user_financial_information.html";
	var userLogoutUrl="<%=__ROOT_PATH__%>/index/quit.html";
	var checkLoginUrl ="<%=__ROOT_PATH__%>/index/checkLogin.html";
	
	
	$("#divFloatToolsView").hide();
	/*
	* method:none
	* param:none
	* explain:初始化数据
	*/
	getCoinOnlineData();
	getUserLogin(true);
	getCoinMaxPrice();
	window.dhx_globalImgPath="<%=__PUBLIC__%>/codebase/imgs/"; 
	

	/*
	* method:getCoinMaxPrice
	* param:none
	* explain:获取虚拟币最新成交价格
	*/
	//var nextCoinPrice = [0,0,0];
	function getCoinMaxPrice()
	{
		$.ajax({url:coinPriceUrl,type:'post',
			success:function(data)
			{
			 var obj = eval("(" + data + ")");
			  //判断服务器是否正确返回信息
			  if(obj!=null)
			  {
				  if(obj.result==200)
				  {
				      if(obj.btcBeforePrice<=obj.btcPrice)
				      {
				    	  $('#btc_max_price').removeClass("head_top_lvse");
					      $('#btc_max_price').removeClass("head_top_lanse");
				          $('#btc_max_price').addClass("head_top_hongse");
				      }
				      else if(obj.btcBeforePrice>obj.btcPrice)
				      {
				    	  $('#btc_max_price').removeClass("head_top_lanse");
					      $('#btc_max_price').removeClass("head_top_hongse");
				          $('#btc_max_price').addClass("head_top_lvse");
				      }
				      if(obj.ltcBeforePrice<=obj.ltcPrice)
				      {
				    	  $('#ltc_max_price').removeClass("head_top_lvse");
					      $('#ltc_max_price').removeClass("head_top_lanse");
				          $('#ltc_max_price').addClass("head_top_hongse");
				      }
				      else if(obj.ltcBeforePrice>obj.ltcPrice)
				      {
				    	  $('#ltc_max_price').removeClass("head_top_lanse");
					      $('#ltc_max_price').removeClass("head_top_hongse");
				          $('#ltc_max_price').addClass("head_top_lvse");
				      }
				      
				      if(obj.ctcBeforePrice<=obj.ctcPrice)
				      {
				    	  $('#ctc_max_price').removeClass("head_top_lvse");
					      $('#ctc_max_price').removeClass("head_top_lanse");
				          $('#ctc_max_price').addClass("head_top_hongse");
				      }
				      else if(obj.ctcBeforePrice>obj.ctcPrice)
				      {
				    	  $('#ctc_max_price').removeClass("head_top_lanse");
					      $('#ctc_max_price').removeClass("head_top_hongse");
				          $('#ctc_max_price').addClass("head_top_lvse");
				      }
					  $('#btc_max_price').val(obj.btcPrice);
					  $('#ltc_max_price').val(obj.ltcPrice);
					  $('#ctc_max_price').val(obj.ctcPrice);
					 // nextCoinPrice[0] = obj.btcPrice;
					 // nextCoinPrice[1] = obj.ltcPrice;
					 // nextCoinPrice[2] = obj.ctcPrice;
				  }
			  }
			}
		});
		setTimeout('getCoinMaxPrice()',3000);
	}
	
	
	
	/*
	* method:getCoinOnlineData
	* param:coin
	* explain:虚拟币即时交易信息
	*/
	function getCoinOnlineData()
	{
		$.ajax({
			url:coinOnlieUrl,
			type:'get',
			success:function(data)
			{
				//判断是否正常返回数据
				var obj = eval( "("+data+")" );
				if(obj!=null)
				{
						 //更新币种成交信息
						 //注：默认取小数点后2位
						var coin = "#btc";
						//比特币即时交易数据
						// var a=obj.btc.nowPrice;
						$(coin+"_now_price").html("￥"+obj.btc.nowPrice);
						$(coin+"_now_buy_price").html("￥"+obj.btc.buyPrice);
						$(coin+"_now_sell_price").html("￥"+obj.btc.sellPrice);
						$(coin+"_trade_max_price").html("￥"+obj.btc.maxPrice);
						$(coin+"_trade_last_price").html("￥"+obj.btc.lastPrice);
						$(coin+"_trade_count").html(obj.btc.count+"฿");
						
						//莱特币即时交易数据
						coin = "#ltc";
						$(coin+"_now_price").html("￥"+obj.ltc.nowPrice);
						$(coin+"_now_buy_price").html("￥"+obj.ltc.buyPrice);
						$(coin+"_now_sell_price").html("￥"+obj.ltc.sellPrice);
						$(coin+"_trade_max_price").html("￥"+obj.ltc.maxPrice);
						$(coin+"_trade_last_price").html("￥"+obj.ltc.lastPrice);
						$(coin+"_trade_count").html(obj.ltc.count+"Ł");
						//凯特币即时交易数据
						coin = "#ctc";
						$(coin+"_now_price").html("￥"+obj.ctc.nowPrice);
						$(coin+"_now_buy_price").html("￥"+obj.ctc.buyPrice);
						$(coin+"_now_sell_price").html("￥"+obj.ctc.sellPrice);
						$(coin+"_trade_max_price").html("￥"+obj.ctc.maxPrice);
						$(coin+"_trade_last_price").html("￥"+obj.ctc.lastPrice);
						$(coin+"_trade_count").html(obj.ctc.count+"₡");
					 }
			}
		});
		setTimeout('getCoinOnlineData()',3000);
	}
	
	/*
	* method:checkUserMmail
	* param:username,id,color
	* explain:验证用户账号输入
	*/
	function checkUserEmail(id, color) {
	  $("#checkText" + id).html("");
	  $("#checkText" + id).css("display","none");
	  var u = $("#m_u" + id).val();
	  if (u == null || u == '') {
	    $("#checkText" + id).css("display","block");
	    $("#checkText" + id).html("请输入登录账号！");
	    return;
	  }
	  if (u.split('@').length<0) {
			$("#checkText" + id).css("display","block");
		    $("#checkText" + id).html("邮箱格式不正确！");
		    return;
	  }
	}

	/*
	* method:checkUserPass
	* param:userpass,id,color
	* explain:验证用户密码输入
	*/
	function checkUserPass(id, color) {
	  $("#checkText" + id).html("");
	  $("#checkText" + id).css("display","none");
	  var p = $("#m_p" + id).val();
	  if (p == null || p == '') {
	    $("#checkText" + id).css("display","block");
	    $("#checkText" + id).html("请输入登录密码！");
	  }
	}

	/*
	* method:doLogin
	* param:username,password
	* explain:登录系统
	*/
	function doLogin(id) {
	  $("#checkText" + id).html("");
	  $("#checkText" + id).css("display","none");
	  
	  var u = $("#m_u" + id).val();
	  if (u == null || u == '') {
		$("#checkText" + id).css("display","block");
	    $("#checkText" + id).html("请输入登录账号！");
	    return;
	  }
	   if (u.split('@').length<0) {
			$("#checkText" + id).css("display","block");
		   $("#checkText" + id).html("邮箱格式不正确！");
		    return;
		  }
	  
	  var p = $("#m_p" + id).val();
	  if (p == null || p == '') {
		$("#checkText" + id).css("display","block");
	    $("#checkText" + id).html("请输入登录密码！");
	    return;
	  }
	  if(p.length<6)
	{
		  $("#checkText" + id).css("display","block");
		  $("#checkText" + id).html("密码长度最低为6！");
		  return;
	  }
	  
	  //AJAX验证登录
	  $.ajax({
	    url:userLoginUrl,
	    type: 'post',
	    data: {
	      "username": u,
	      "password": p
	    },
	    success: function(data) {
	    	
			 $("#doLoginTFoll").val("登录中...");
			 $("#doLoginTFoll").attr('disabled',true);
		   	 $("#doLoginTFollTop").val("登录中...");
			 $("#doLoginTFollTop").attr('disabled',true);
		   	
	      if (data != null) {
	        if (data == 0) {
	          $("#m_p" + id).val("") 
	          $("#checkText" + id).css("display","block");
	          $("#checkText" + id).html("帐号或密码错误，请重新输入！");
	          
			  $("#doLoginTFoll").attr('disabled',false);
		   	  $("#doLoginTFoll").val("立即登录");
			  $("#doLoginTFollTop").attr('disabled',false);
			   $("#doLoginTFollTop").val("立即登录");
	        } else if (data == 1) {
	        	 location.replace(location.href);
	        	 $("#doLoginTFoll").attr('disabled',false);
			   	  $("#doLoginTFoll").val("立即登录");
				  $("#doLoginTFollTop").attr('disabled',false);
				  $("#doLoginTFollTop").val("立即登录");
	        	 location.reload(false);
	        } else if (data == 2) {
	          $("#checkText" + id).css("display","block");
	          $("#checkText" + id).html("登陆账号已连续输错五次，请联系客服处理！");	 
	          $("#doLoginTFoll").attr('disabled',false);
		   	  $("#doLoginTFoll").val("立即登录");
			  $("#doLoginTFollTop").attr('disabled',false);
			  $("#doLoginTFollTop").val("立即登录");       
	        }
	      }
	    }
	  },"html");
	}

	/*
	* method:getUserLogin
	* param:none   
	* explain:获取登录状态
	
	*/
	function getUserLogin(isLogin) {
		if (!isLogin) {
			$("#tradeSellDiv").css("display","none");
            $("#tradeBuyDiv").css("display","none");
			$("#indexLoginDiv").css("display", "none");
            $("#indexNotLoginDiv").css("display", "block");
            $("#userLoginDiv").css("display", "none");
            $("#userNoLoginDiv").css("display", "block");
            $("#site-nav").css("display", "none");
            $("#tradeBuyDivH6").css("display","none");
            $("#tradeBuyDivH5").css("display","none");
		} else {
			//验证用户登陆状态
		    $.ajax({
		        url: checkLoginUrl,
		        type: 'get',
		        success: function(data) {
			        //data为1即登陆，反之未登录
		            if (data == 1) {
			            $("#tradeSellDiv").css("display","block");
			            $("#tradeBuyDiv").css("display","block");
			            $("#tradeBuyDivH6").css("display","block");
            			$("#tradeBuyDivH5").css("display","block");
		                $("#indexLoginDiv").css("display", "block");
		                $("#indexNotLoginDiv").css("display", "none");
		                $("#userLoginDiv").css("display", "block");
		                $("#userNoLoginDiv").css("display", "none");
		                $("#site-nav").css("display", "block");
		                getUserAcount();
		            } else {
		            	$("#tradeSellDiv").css("display","none");
			            $("#tradeBuyDiv").css("display","none");
			            $("#tradeBuyDivH6").css("display","none");
            			$("#tradeBuyDivH5").css("display","none");
		                $("#indexLoginDiv").css("display", "none");
		                $("#indexNotLoginDiv").css("display", "block");
		                $("#userLoginDiv").css("display", "none");
		                $("#userNoLoginDiv").css("display", "block");
		                $("#site-nav").css("display", "none");
		            }
		        }
		    },"json");
		}
	}

	/*
	* method:getUserAcount
	* param:none
	* explain:获取用户账户信息
	*/
	function getUserAcount() {
	  $.ajax({
	    url: userAcountUrl,
	    type: 'get',
	    success: function(data) {
		    if(data!=null)
		    {
		    	$.ajax({url:coinPriceUrl,type:'get',
					success:function(priceData)
					{
					 var priceObj = eval("("+priceData+")");
					  //判断服务器是否正确返回信息
					  if(priceObj!=null)
					  {
						  if(priceObj.result==200)
						  {
							    var obj = eval("("+data+")");
						    	//首页用户账户面板显示
						    	var btcM = (obj.has_BTC+obj.use_BTC)*(priceObj.btcPrice==0?1:priceObj.btcPrice);
						    	var ltcM = (obj.has_LTC+obj.use_LTC)*(priceObj.ltcPrice==0?1:priceObj.ltcPrice);
						    	var ctcM = (obj.has_CTC+obj.use_CTC)*(priceObj.ctcPrice==0?1:priceObj.ctcPrice);
						    	$("#userCanUseMoney").html("￥"+(obj.has_CNY+obj.use_CNY+btcM+ltcM+ctcM).toFixed(2));
						    	$("#userAllMoney").html("￥"+(obj.has_CNY+obj.use_CNY+btcM+ltcM+ctcM).toFixed(2));
						    	$("#indexUserMoney").html("￥&nbsp"+obj.has_CNY.toFixed(2));
						    	$("#indexUserBtc").html("฿&nbsp"+obj.has_BTC.toFixed(4));
						    	$("#indexUserLtc").html("Ł&nbsp"+obj.has_LTC.toFixed(4));
						    	$("#indexUserCtc").html("₡&nbsp"+obj.has_CTC.toFixed(4));
						    	//顶部用户账户面板显示
						    	$("#user_money").html("￥"+(obj.has_CNY+obj.use_CNY+btcM+ltcM+ctcM).toFixed(2));
							    $("#has_CNY").html("￥"+obj.has_CNY.toFixed(2));
							    $("#has_BTC").html("฿"+obj.has_BTC.toFixed(4));
							    $("#has_LTC").html("Ł"+obj.has_LTC.toFixed(4));
							    $("#has_CTC").html("₡"+obj.has_CTC.toFixed(4));
							    $("#use_CNY").html("￥"+obj.use_CNY.toFixed(2));
							    $("#use_BTC").html("฿"+obj.use_BTC.toFixed(4));
							    $("#use_LTC").html("Ł"+obj.use_LTC.toFixed(4));
							    $("#use_CTC").html("₡"+obj.use_CTC.toFixed(4));
						  }
					  }
					}
				});
			}
	    }
	  },
	  "json");
	}

	/*
	* method:checkInputUserEmail
	* param:none
	* explain:验证用户输入的Email格式
	*/
	function checkInputUserEmail(s) {
		clearError();
	    var u = $("#username").val();
	    if (u == null || u == "") {
	        $("#r_msg_e").html("注册邮箱为空！");
	        return;
	    }
	    if(s)
	    {
	    	var email = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
		    if (!email.test(u)) {
		        $("#r_msg_e").html("请输入有效的Email！");
		        return;
		    }
	    }
	}

	//清理错误信息
	function clearError()
	{
		$("#r_msg_e").html("");
	    $("#r_msg_p").html("");
	    $("#r_msg_p2").html("");
	}

	/*
	* method:checkInputUserPass
	* param:none
	* explain:验证用户密码输入
	*/
	function checkInputUserPass() {
	  $("#r_msg_e").html("");
	  $("#r_msg_p").html("");
	  $("#r_msg_p2").html("");
	  var p = $("#password").val();
	  if (p == null || p == "") {
	    $("#r_msg_p").html("登录密码为空！");
	    return;
	  }
	   if (p.length < 6){
		 $("#r_msg_p").html("登录密码长度不能低于6位！");
		 return;
	  }
	}

	/*
	* method:checkInputUserPass2
	* param:none
	* explain:验证用户密码二次输入
	*/
	function checkInputUserPass2() {
	  $("#r_msg_e").html("");
	  $("#r_msg_p").html("");
	  $("#r_msg_p2").html("");

	  var p = $("#password").val();
	  var p2 = $("#password2").val();
	  if (p2 == null || p2 == "") {
	    $("#r_msg_p2").html("二次密码为空！");
	    return;
	  }
	  if(p.length<=p2.length)
	  {
	     if (p2 != p) {
	        $("#r_msg_p2").html("两次输入的密码不一致！");
	        return;
	     }
	  }
	}

	/*
	* method:doRegisterUser
	* param:none
	* explain:注册用户信息
	*/
	function doRegisterUser() {
	  $("#r_msg_e").html("");
	  $("#r_msg_p").html("");
	  $("#r_msg_p2").html("");
	
	
	  var u = $("#username").val();
	  if (u == null || u == "") {
	    $("#r_msg_e").html("注册邮箱为空！");
	    return false;
	  }
	  var email = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	  if (!email.test(u)) {
	    $("#r_msg_e").html("请输入有效的Email！");
	    return false;
	  }

	  var p = $("#password").val();
	  if (p == null || p == "") {
	    $("#r_msg_p").html("密码为空！");
	    return false;
	  }
	  
	   if(p.length < 6){
		 $("#r_msg_p").html("密码长度不能低于6位！");
		 return false;
	  }

	  var p2 = $("#password2").val();
	  if (p2 == null || p2 == "") {
	    $("#r_msg_p2").html("确认密码为空！");
	    return false;
	  }
	  if (p2 != p) {
	    $("#r_msg_p2").html("两次输入的密码不一致！");
	    return false;
	  }
	  //ajax邮箱验证，添加用户
	  $.ajax({
		  url:userRegisterUrl,
		  type:'post',
		  data:{"username":u,"password":p},
		  success:function(data)
		  {
		  		
			  $("#reg_btn").val("注册中...");
			  $('#reg_btn').attr('disabled',true);
			  if (data != null) {
					$("#r_msg_e").html("");
					$("#r_msg_p").html("");
					$("#r_msg_p2").html("");
					if (data == 2) {
						$("#username").val("");
						$("#r_msg_e").html("该Email已经被使用！");
						
			  			$("#reg_btn").attr('disabled',false);
			  			$("#reg_btn").val("确认注册");
					} else if (data == 0) {
						$("#username").val("");
						$("#r_msg_e").html("不支持该邮箱，请使用QQ或163邮箱！");
						$("#reg_btn").attr('disabled',false);
			  			$("#reg_btn").val("确认注册");
					}else if (data == 3) {
						$("#username").val("");
						$("#r_msg_e").html("该身份证号已被使用！");
						$("#reg_btn").attr('disabled',false);
			  			$("#reg_btn").val("确认注册");
					}else if (data == 1) {
						$("#username").val("");
						$("#password").val("");
						$("#password2").val("");
						$("#con_sever_2").css("display","none");
						$("#show_success").css("display","block");
						var str = "恭喜您注册成功，我们已经给您的邮箱：<br />";
						str+=u+"<br />";
						str+="发送了一份激活邮件，您可以选择激活，也可以直接进入个人中心。";
						$("#show_success_message").html(str);
						$("#reg_btn").attr('disabled',false);
			  			$("#reg_btn").val("确认注册");
						$.ajax({
						    url: userLoginUrl,
						    type: 'post',
						    data: {
						      "username": u,
						      "password": p
						    },
						    success: function(data) {
						    	getUserLogin(true);
						    }
						  },
						  "html");
					}else {
						$("#username").val("");
						$("#r_msg_e").html("非法访问！");
						$("#reg_btn").attr('disabled',false);
			  			$("#reg_btn").val("确认注册");
					}
				}
		  }
	 });
	}

	/*
	* method:doSubmitOrder
    * param:type 1:btc,2:ltc,3:ctc   Ok
	* explain:提交买单请求   
	*/
	function doSubmitBuyOrder(coin)
	{
	
		var price = $("#buy_price").val();
		var count= $("#buy_quantity").val();
		var pwd = $("#buy_pwd_money").val();
		var type=coin;//$("#dummy_type").val();
		
		
	    if(price>0&&count>0.001)
		{
			$.ajaxSetup ({
				cache: false //close AJAX cache设置ajax不缓存，下面的位置清不行，不清缓存的话，ie下$.load只加载一次
			});
		
			$.ajax({
				url:coinOrderBuyUrl,
				type:'post',
				data:{"buy_price":price,"buy_quantity":count,"pwd_money":pwd,"dummy_type":coin},
				cache:false,
				success:function(data)
				{
					if(data!=null)
					{
						if (data == 0) {
							showMssage(["提示","购买数量为空"]);
						} else if (data == 1) {
							showMssage(["提示","购买价为空"]);
						}else if (data == 2) {
							showMssage(["提示","资金密码为空"]);
						} else if (data == 3) {
							showMssage(["提示","输入的资金密码不匹配"]);
						} else if (data == 4) {
							showMssage(["提示","系统不存在此资金号"]);
						} else if (data == 5) {
							showMssage(["提示","购买成功"]);
							//window.location.reload();
							//如果成功则加载用户最近挂单表
							getUserAcount();
							var cny_str=$("#CNY_remainder").html()-(($("#buy_quantity").val()*$("#buy_price").val()).toFixed(2));
							$("#CNY_remainder").html(cny_str);
							if(coin == 1){
								var btc_num=(cny_str/${btc_price<=0?1:btc_price}).toFixed(4);
								$("#buy_num").html(btc_num);
								$("#table_info").empty();
								$("#table_info").load('<%=__ROOT_PATH__%>/user/trade_info/btc_trade/user_info.html');
								$("#table").empty();
								$("#table").load('<%=__ROOT_PATH__%>/user/trade_manage/btc_trade/user_info.html');
								$("#tips").hide();
							}else if(coin == 2){
								var ltc_num=(cny_str/${(ltc_price<=0?1:ltc_price)}).toFixed(4);
								$("#buy_num").html(ltc_num);
								$("#table_info").empty();
								$("#table_info").load('<%=__ROOT_PATH__%>/user/trade_info/ltc_trade/user_info.html');
								$("#table").empty();
								$("#table").load('<%=__ROOT_PATH__%>/user/trade_manage/ltc_trade/user_info.html');
								$("#tips").hide();
							}else if(coin == 3){
								var ctc_num=(cny_str/${(ctc_price<=0?1:ctc_price)}).toFixed(4);
								$("#buy_num").html(ctc_num);
							
								$("#table_info").empty();
								$("#table_info").load('<%=__ROOT_PATH__%>/user/trade_info/ctc_trade/user_info.html');
								$("#table").empty();
								$("#table").load('<%=__ROOT_PATH__%>/user/trade_manage/ctc_trade/user_info.html');
								$("#tips").hide();
							}
							
							
						} 
						 else if (data == 6) {
							showMssage(["提示","购买失败"]);
						} 
						 else if (data == 7) {
							showMssage(["提示","账户余额不足，请及时充值"]);
						} 
						 else if (data == 8) {
							showMssage(["提示","当前的价格不能低于系统价格的15%"]);
						} else if (data == 9) {
							showMssage(["提示","请设置资金密码后再进行交易"]);
						} 
					 }
					else
					{
						showMssage(["警告","下单失败,请检查网络连接是否通畅,或服务器处于正常状态！"]);
					 }
				}},"html");
		}
		
	    if(price<=0)
		{
	    	showMssage(["提示","下单金额不得低于0，请重新输入！"]);
		}
		
		if(count<=0.001||count<=0)
		{
			showMssage(["提示","下单数量过小，最低不能小于0.001！"]);
		}
		
	    if($("#pwd_money").css('display')=="inline-block")
	    {
		    if(pwd==''||pwd==null)
		    {
		    	showMssage(["提示","请输入交易密码！"]);
			}
		    else if(pwd.length<4)
		    {
		    	showMssage(["提示","交易密码不得低于4位！"]);
			}
		}
	}
	/*
	* method:doSubmitSellOrder
    * param:type 1:btc,2:ltc,3:ctc
	* explain:提交卖单请求
	*/
	function doSubmitSellOrder(coin)
	{
		var price = $("#sell_price").val();
		var count= $("#sell_quantity").val();
		var pwd=$("#sell_pwd_money").val();
		var type=coin;//$("#dummy_type").val();

		if(price>0&&count>0.001)
		{
			$.ajaxSetup ({
				cache: false //close AJAX cache设置ajax不缓存，下面的位置清不行，不清缓存的话，ie下$.load只加载一次
			});		
			$.ajax({
				url:coinOrderSellUrl,
				type:'post',
				data:{"sell_price":price,"sell_quantity":count,"pwd_money":pwd,"dummy_type":coin},
				cache:false,
				success:function(data)
				{
					if(data!=null)
					{
						if (data == 0) {
							showMssage(["提示","出售数量为空"]);
						} else if (data == 1) {
							showMssage(["提示","出售价为空"]);
						}else if (data == 2) {
							showMssage(["提示","资金密码为空"]);
						} else if (data == 3) {
							showMssage(["提示","输入的资金密码不匹配"]);
						} else if (data == 4) {
							showMssage(["提示","系统不存在此资金号"]);
						} else if (data == 5) {
							showMssage(["提示","出售成功"]);
							//window.location.reload();
							//如果成功则加载用户最近挂单表
							getUserAcount();
							if(coin == 1){
								var dmy_str=$("#BTC_remainder").html()-$("#sell_quantity").val();
								$("#BTC_remainder").html(dmy_str.toFixed(4));
								var btc_num=(dmy_str*${btc_price<=0?1:btc_price}).toFixed(2);
								$("#sell_num").html(btc_num);
								
								$("#table_info").empty();
								$("#table_info").load('<%=__ROOT_PATH__%>/user/trade_info/btc_trade/user_info.html');
								$("#table").empty();
								$("#table").load('<%=__ROOT_PATH__%>/user/trade_manage/btc_trade/user_info.html');
								$("#tips").hide();
							}else if(coin == 2){
								var dmy_str=$("#LTC_remainder").html()-$("#sell_quantity").val();
								$("#LTC_remainder").html(dmy_str.toFixed(4));
								var ltc_num=(dmy_str*${ltc_price<=0?1:ltc_price}).toFixed(2);
								$("#sell_num").html(ltc_num);
								
								$("#table_info").empty();
								$("#table_info").load('<%=__ROOT_PATH__%>/user/trade_info/ltc_trade/user_info.html');
								$("#table").empty();
								$("#table").load('<%=__ROOT_PATH__%>/user/trade_manage/ltc_trade/user_info.html');
								$("#tips").hide();
							}else if(coin == 3){
								var dmy_str=$("#CTC_remainder").html()-$("#sell_quantity").val();
								$("#CTC_remainder").html(dmy_str.toFixed(4));
								var ctc_num=(dmy_str*${ctc_price<=0?1:ctc_price}).toFixed(2);
								$("#sell_num").html(ctc_num);
							
								$("#table_info").empty();
								$("#table_info").load('<%=__ROOT_PATH__%>/user/trade_info/ctc_trade/user_info.html');
								$("#table").empty();
								$("#table").load('<%=__ROOT_PATH__%>/user/trade_manage/ctc_trade/user_info.html');
								$("#tips").hide();
							}
						} 
						 else if (data == 6) {
							showMssage(["提示","出售失败"]);
						} 
						 else if (data == 7) {
							showMssage(["提示","账户余额不足，请及时充值"]);
						} else if (data == 9) {
							showMssage(["提示","请设置资金密码后再进行交易"]);
						} 
						
					}
					else
					{
						showMssage(["警告","下单失败,请检查网络连接是否通畅,或服务器处于正常状态！"]);
					}
				}},"json");
		}
		 if(price<=0)
			{
			 showMssage(["提示","下单金额不得低于0，请重新输入！"]);
			}
			if(count<=0.001||count<=0)
			{
				showMssage(["提示","下单数量过小，最低不能小于0.001！"]);
			}
			
		    if($("#pwd_money").css('display')=="inline-block")
		    {
			    if(pwd==''||pwd==null)
			    {
			    	showMssage(["提示","请输入交易密码！"]);
				}
			    else if(pwd.length<4)
			    {
			    	showMssage(["提示","交易密码不得低于4位！"]);
				}
			}		
	}

	/*
	* method:null
    * param:null 
	* explain:绑定登陆按钮
	*/
	$(document).keydown(function(event){
	    if(event.keyCode==13){
		    if($('#loginDiv2').css('display')=='block')
		    {
		    	doLogin('1');
			 }
		    else
		    {
			    doLogin('2');
			}
	    }});
      
	/*
	* method:showMssage
    * param:message 
	* explain:显示对话信息
	*/
	function showMssage(message)
	{
		var btnFn = function(){
			  easyDialog.close();
			  };
	   var btnFs = function(){
		   window.location.reload();
	   };
	   /**
	   var btnTz = function(){
		   
		   	easyDialog.close();
		  
		   	window.location.href(message[3]);
	   }**/
		if(message[2]!=null){
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFs,
					 noFn:false
					}
			});	
			//实名认证成功以后 跳向安全设置页面
		}else{
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFn,
					 noFn:false
					}
			});	
		}
	}

	/*
	* method:logout
    * param:none
	* explain:退出系统
	*/
	function doLogout() {
		$.ajax({
		    url: userLogoutUrl,
		    type: "get",
		    success: function(data) {
		    	if(data==1)
		    	{
		    		doLogout();
			    }
		    	else
		    	{
		    		getUserLogin(false);
		    		location.replace(location.href);
			    }
		    }
		},
		"json");
	}
</script>

			<script type="text/javascript">
		/*
		* method:下单比例
		* param:none
		* explain:设置下单比例
		*/
		//买单算法
		
		$('#buy_price').bind('keyup', function () {
			var pattern=/^-?\d+[\.\d]?\d{0,4}$/;if(!pattern.test (this.value)) this.value = this.value.substring(0,this.value.length - 1);
			$("#buy_sum").val(($("#buy_quantity").val()*$("#buy_price").val()).toFixed(4));
			});
		$('#buy_quantity').bind('keyup', function () {
			var pattern=/^-?\d+[\.\d]?\d{0,4}$/;if(!pattern.test (this.value)) this.value = this.value.substring(0,this.value.length - 1);
			$("#buy_sum").val(($("#buy_quantity").val()*$("#buy_price").val()).toFixed(4));
			});
		$('#buy_sum').bind('keyup', function () {
			var pattern=/^-?\d+[\.\d]?\d{0,4}$/;if(!pattern.test (this.value)) this.value = this.value.substring(0,this.value.length - 1);
			$("#buy_quantity").val(($("#buy_sum").val()/$("#buy_price").val()).toFixed(2));
			});
		
		$("#buy_half").click(function(){
		 	$("#buy_sum").val((${CNY_remainder==null?0:CNY_remainder}/2=='NaN'?0:${CNY_remainder==null?0:CNY_remainder}/2).toFixed(4));
			 $("#buy_quantity").val(($("#buy_sum").val()/$("#buy_price").val()).toFixed(2));
		});
		$("#buy_third").click(function(){
			$("#buy_sum").val((${CNY_remainder==null?0:CNY_remainder}/3=='NaN'?0:${CNY_remainder==null?0:CNY_remainder}/3).toFixed(4));
			 $("#buy_quantity").val(($("#buy_sum").val()*$("#buy_price").val()).toFixed(2));
		});
		$("#buy_quarter").click(function(){
			$("#buy_sum").val((${CNY_remainder==null?0:CNY_remainder}/4=='NaN'?0:${CNY_remainder==null?0:CNY_remainder}/4).toFixed(4));
			$("#buy_quantity").val(($("#buy_sum").val()*$("#buy_price").val()).toFixed(2));
		});
			
			
			//卖单算法
		$('#sell_price').bind('keyup', function () {
			var pattern=/^-?\d+[\.\d]?\d{0,4}$/;if(!pattern.test (this.value)) this.value = this.value.substring(0,this.value.length - 1);
			$("#sell_sum").val(($("#sell_quantity").val()*$("#sell_price").val()).toFixed(4));
			});
		$('#sell_quantity').bind('keyup', function () {
			var pattern=/^-?\d+[\.\d]?\d{0,4}$/;if(!pattern.test (this.value)) this.value = this.value.substring(0,this.value.length - 1);
			$("#sell_sum").val(($("#sell_quantity").val()*$("#sell_price").val()).toFixed(4));
			});
		$('#sell_sum').bind('keyup', function () {
			var pattern=/^-?\d+[\.\d]?\d{0,4}$/;if(!pattern.test (this.value)) this.value = this.value.substring(0,this.value.length - 1);
			$("#sell_quantity").val(($("#sell_sum").val()/$("#sell_price").val()).toFixed(2));
			});
		
		$("#sell_half").click(function(){
		    $("#sell_quantity").val((${BTC_remainder==null?0:BTC_remainder}/2).toFixed(4));
			$("#sell_sum").val(($("#sell_quantity").val()*$("#sell_price").val()).toFixed(2));
		});
		$("#sell_third").click(function(){
			$("#sell_quantity").val((${BTC_remainder==null?0:BTC_remainder}/3).toFixed(4));
			$("#sell_sum").val(($("#sell_quantity").val()*$("#sell_price").val()).toFixed(2));
		});
		$("#sell_quarter").click(function(){
			 $("#sell_quantity").val((${BTC_remainder==null?0:BTC_remainder}/4).toFixed(4));
			 $("#sell_sum").val(($("#sell_quantity").val()*$("#sell_price").val()).toFixed(2));
		});		
			
		</script>
		
			</script>
		<%
		String loginLink=(String)request.getAttribute("loginLink");
		if(loginLink==null){
		loginLink="0";
		}
		 %>
		<!-- 未登录要求登录 -->
		<script type="text/javascript">
		<tfoll-menu:if need_url="1" url="<%=loginLink%>">
		openme('2');
		</tfoll-menu:if>
		</script>
		<script>
		//设置导航条当前位置的样式
		$(document).ready(function(){
			$(".head_nav a").each(function(i){
				if(window.location.href == $(this).attr("href") ){
					$(this).addClass("hover");
				}else{
					$(this).removeClass("hover");
				}
			})
			$("#user_center a").each(function(i){
				if(window.location.href == $(this).attr("href") ){
					$("#user-nav").addClass("hover");
				}
			})
			
			
			if(window.location.href.indexOf("index/index.html") != -1 ){

				$("#index-nav").addClass("hover");
			}
			if(window.location.href.indexOf("index/index.html") != -1 ){
				
				$("#user-nav").removeClass("hover");
				$("#money-nav").removeClass("hover");
				$("#news-nav").removeClass("hover");
				$("#trade-nav").removeClass("hover");
				$("#index-nav").addClass("hover");
			}	
			if(window.location.href.indexOf("index/news") != -1 ){
				$("#index-nav").removeClass("hover");
				$("#trade-nav").removeClass("hover");
				$("#money-nav").removeClass("hover");
				$("#user-nav").removeClass("hover");
				$("#news-nav").addClass("hover");

			}
			
			if(window.location.href.indexOf("/user/trade_info") != -1 ){
				$("#index-nav").removeClass("hover");
				$("#money-nav").removeClass("hover");
				$("#user-nav").removeClass("hover");
				$("#news-nav").removeClass("hover");
				$("#trade-nav").addClass("hover");
			}
			
			if(window.location.href.indexOf("user/acount") != -1 ){
				$("#index-nav").removeClass("hover");
				$("#user-nav").removeClass("hover");
				$("#news-nav").removeClass("hover");
				$("#trade-nav").removeClass("hover");
				$("#money-nav").addClass("hover");
			}
			if(window.location.href.indexOf("/user/user/user_bank") != -1 ){
				$("#index-nav").removeClass("hover");
				$("#user-nav").removeClass("hover");
				$("#news-nav").removeClass("hover");
				$("#trade-nav").removeClass("hover");
				$("#money-nav").addClass("hover");
			}
			if(window.location.href.indexOf("entrust/trade_manage") != -1 ){
				$("#index-nav").removeClass("hover");
				$("#money-nav").removeClass("hover");
				$("#news-nav").removeClass("hover");
				$("#trade-nav").removeClass("hover");
				$("#user-nav").addClass("hover");
			}
			if(window.location.href.indexOf("user/trade_manage") != -1 ){
				$("#index-nav").removeClass("hover");
				$("#money-nav").removeClass("hover");
				$("#news-nav").removeClass("hover");
				$("#trade-nav").removeClass("hover");
				$("#user-nav").addClass("hover");
			}
			if(window.location.href.indexOf("/user/user/user") != -1 ){
				$("#index-nav").removeClass("hover");
				$("#money-nav").removeClass("hover");
				$("#news-nav").removeClass("hover");
				$("#trade-nav").removeClass("hover");
				$("#user-nav").addClass("hover");
			}	
			
	
		})
		</script>
		<script>
			var count = 0;  
			$(document).ready(
					function funTitle() {  
						
						
						//${btc_price}
						//${ltc_price}
						//${ctc_price}
						var btc = "฿:"+${btc_price}+" -天富网(TFOLL)-国内最大最专业的虚拟币交易平台";
						var ltc = "Ł:"+${ltc_price}+" -天富网(TFOLL)-国内最大最专业的虚拟币交易平台";
						var ctc = "₡:"+${ctc_price}+" -天富网(TFOLL)-国内最大最专业的虚拟币交易平台";
					
						switch(count){
							case 0:
								
								document.title = btc;
								count ++;
								break;
							case 1:
								
								document.title = ltc;
								count ++;
								break;
							case 2:
								
								document.title = ctc;
								count = 0;
								break;
								
						};
						
						setTimeout(funTitle,1500);
					});
					
		</script>
