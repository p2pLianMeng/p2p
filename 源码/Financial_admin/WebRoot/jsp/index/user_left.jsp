<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://com.tfoll.web/lable/menu_show_tag" prefix="tfoll-menu" %>
	<h2><img src="<%=__PUBLIC__%>/images/left_icon10.png" /> <p>用户中心</p></h2>
	<ul id="user_center">
	<%
	String url=request.getRequestURI();
	 %>
		<li>
		<a href="<%=__ROOT_PATH__%>/user/trade_manage/btc_trade/btc_buy_index.html"  
		<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/index/index.jsp#/jsp/user/trade_manage/btc_trade/btc_jy2.jsp">class="hover"</tfoll-menu:if>
		>BTC交易</a>
		</li>
		
		<li><a href="<%=__ROOT_PATH__%>/user/trade_manage/ltc_trade/ltc_buy_index.html"
		<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/trade_manage/ltc_trade/ltc_jy.jsp#/jsp/user/trade_manage/ltc_trade/ltc_jy2.jsp">class="hover"</tfoll-menu:if>
		>LTC交易</a>
		</li>
	
		<li><a
			href="<%=__ROOT_PATH__%>/user/trade_manage/ctc_trade/ctc_buy_index.html"
			<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/trade_manage/ctc_trade/ctc_jy.jsp#/jsp/user/trade_manage/ctc_trade/ctc_jy2.jsp">class="hover"</tfoll-menu:if>
			>CTC交易</a>
		</li>
		<!-- 
		<li><a href="#" url="geren.html">计划委托</a>
		</li>
		-->
		<li><a href="<%=__ROOT_PATH__%>/user/entrust/trade_manage/entrust_index.html" 
		<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/trade_manage/entrust.jsp#/jsp/user/trade_manage/entrust/btc_entrust_list_history.jsp#/jsp/user/trade_manage/entrust/ltc_entrust_list_amonth.jsp#/jsp/user/trade_manage/entrust/ltc_entrust_list_history.jsp#/jsp/user/trade_manage/entrust/ctc_entrust_list_amonth.jsp#/jsp/user/trade_manage/entrust/ctc_entrust_list_history.jsp">class="hover"</tfoll-menu:if>
		>委托管理</a>
		</li>
		 <li><a href="<%=__ROOT_PATH__%>/user/trade_manage/integral/integral_index.html" url="jifen.html"
		 <tfoll-menu:if url="<%=url%>" need_url="/jsp/user/trade_manage/integral.jsp">class="hover"</tfoll-menu:if>
		>积分等级</a> 
		</li>
		<li><a href="<%=__ROOT_PATH__%>/user/user/user_info_safety_set_index.html"
		<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/user/user_info_safety_set_index.jsp#/jsp/user/user/user_pwd_show_4_update.jsp">class="hover"</tfoll-menu:if>
		>安全设置</a>
		</li>
		<li><a href="<%=__ROOT_PATH__%>/user/user/user_info_show.html"
		<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/user/user_info_show.jsp#/jsp/user/user/user_bank_info_index.jsp">class="hover"</tfoll-menu:if>
		>账户信息</a>
		</li>
		<!--<li><a href="<%=__ROOT_PATH__%>/user/share/share_index.html"
		<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/user/go_shares.jsp">class="hover"</tfoll-menu:if>
		>推广分红</a>
		</li>
	--></ul>
	<h2><img src="<%=__PUBLIC__%>/images/left_icon16.png" /> <p>财务中心</p></h2>
	<ul id="financial_center">
		<li><a
			href="<%=__ROOT_PATH__%>/user/acount/coins/coins_add_list_index.html"
			<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/acount/coins/coins_add_list.jsp#/jsp/user/acount/coins/coins_add_list_log_query.jsp">class="hover"</tfoll-menu:if>
			>BTC/LTC/CTC充值</a>
		</li>
				<li><a
			href="<%=__ROOT_PATH__%>/user/acount/coins/get_btc_coins_list_index.html"
			<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/acount/coins/get_btc_coins_list_index.jsp#/jsp/user/acount/coins/get_ltc_coins_list_index.jsp#/jsp/user/acount/coins/get_ctc_coins_list_index.jsp#/jsp/user/acount/coins/coins_put_list_log_query.jsp">class="hover"</tfoll-menu:if>
			>BTC/LTC/CTC提现</a>
		</li>
		<li><a
			href="<%=__ROOT_PATH__%>/user/acount/rmb_code/rmb_code_add_money_index.html"
			<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/acount/rmb_code/rmb_code_add_money.jsp#/jsp/user/acount/rmb_code/agent_rmb_code_add_money.jsp#/jsp/user/acount/rmb_code/rmb_code_more_record.jsp">class="hover"</tfoll-menu:if>

			>人民币充值</a>
		</li>
		<li><a
			href="<%=__ROOT_PATH__%>/user/acount/rmb_withdrawal/rmb_withdrawal_index.html"
			<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/acount/rmb_withdrawal/rmb_withdrawal_index.jsp#/jsp/user/user/user_bank_info_add_index.jsp#/jsp/user/acount/rmb_withdrawal/rmb_withdrawal_record.jsp">class="hover"</tfoll-menu:if>
			>人民币提现</a>
		</li>

		<li><a href="<%=__ROOT_PATH__%>/user/acount/financial_record/income/financial_record_income_index.html"
		<tfoll-menu:if url="<%=url%>" need_url="/jsp/user/acount/financial_record/income/financial_record_income_rmb.jsp#/jsp/user/acount/financial_record/income/financial_record_income_dummy.jsp#/jsp/user/acount/financial_record/business/financial_record_business.jsp#/jsp/user/acount/financial_record/pay/financial_record_pay_rmb.jsp#/jsp/user/acount/financial_record/pay/financial_record_pay_dummy.jsp">class="hover"</tfoll-menu:if>
		>财务记录</a>
		</li>
	</ul>

	<!-- 
	<h2><img src="<%=__PUBLIC__%>/images/left_icon19.png" /> <p>融资融币</p></h2>
	<ul>
		<li><a href="#" url="jiedai.html">我要借款</a>
		</li>
		<li><a href="#" url="jiedai2.html">我要放款</a>
		</li>
		<li><a href="#" url="jiedai3.html">融资融币说明</a>
		</li>
	</ul>
	 -->
	
	


