<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.filter.AdminLoginFilter"%>
<%@page import="com.tfoll.web.util.Utils"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>天富网后台登录界面</title>
		
		<link href="<%=__PUBLIC__%>/css/login.css" rel="stylesheet" />
		<script src="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			type="text/javascript"></script>
		<script src="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			type="text/javascript"></script>
		<script src="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			type="text/javascript"></script>
	</head>
	<script type="text/javascript">
			if (self != top) {
				window.parent.document.location.href = document.location.href;
			}
	</script>

	<%
		String isLogined = (String) session.getAttribute(AdminLoginFilter.isLogined);
		if (Utils.isNotNullAndNotEmptyString(isLogined) && "1".equals(isLogined)) {
			//filterChain.doFilter(servletRequest, servletResponse);
		} else {
			return;
		}
	%>
	<body style="font-family: '微软雅黑'">
		<div class="houtai">
			<div class="houtai_con">
				<div class="houtai_con_pic">
					<h1>
					<form action="<%=__ROOT_PATH__%>/index/$my_admin_login.html" method="post">
							<p>
								<input name="username" type="text" class="houtai_kk" />
							</p>
							<p style="padding-top: 25px;">
								<input name="password" type="password" class="houtai_kk" />
							</p>
							<p style="padding-top: 25px;">
								<input name="code" type="text" class="houtai_kk" style="width: 45px;" />
								<input type="button" style="width: 68px; height: 26px; background: #d4d4d4; border: 0; cursor: pointer;; margin-left: 8px;" value="3158" />
								<input type="submit" style="width: 68px; height: 26px; background: url(/images/houtai_dl_03.jpg); border: 0; font-weight: bold; color: #FFF; cursor: pointer; margin-left: 12px;" value=" " />
							</p>
							<p><span id="tishi" style="color: red">${tips}</span></p>
						</form>
					</h1>
				</div>
			</div>
		</div>
	</body>
</html>