<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>联富宝预约列表</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">联富宝持有列表</span></h3>
     <hr/>
     <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/lfoll_invest/lfoll_invest_reserve/get_lfoll_invest_join_list.html" method="post">
     	姓名：
     	<input type="text" name="real_name" id="bid_name" value="${real_name}" class="abc input-default"  style="width: 100px"/>
     	期数：
     	<input type="text" name="bid_name" id="bid_name" value="${bid_name}" class="abc input-default"  style="width: 100px"/>
     	预定开始时间：
     	<input type="text" name="order_start_time" id="system_start_time" style="width: 150px" value="${order_start_time }"  onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/>
     	预定结束时间：
     	<input type="text" name="order_end_time" id="system_end_time" style="width: 150px" value="${order_end_time }" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/>
     	支付情况：
     	<select name="state" style="width:135px">
     		<c:if test="${state == 0}">
     			<option value="-1">--全部--</option>
     			<option value="0" selected="selected">已退出</option>
	     		<option value="1">持有中</option>
     		</c:if>
     		<c:if test="${state == 1}">
     			<option value="-1">--全部--</option>
     			<option value="0">已退出</option>
	     		<option value="1"  selected="selected">持有中</option>
     		</c:if>
     		<c:if test="${state == -1}">
     			<option value="-1" selected="selected">--全部--</option>
     			<option value="0">已退出</option>
	     		<option value="1" >持有中</option>
     		</c:if>
     	</select>
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     </form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>序号</th>
	     		<th>联富宝名称</th>
	     		<th>真实姓名</th>
	     		<th>昵称</th>
	     		<th>加入金额</th>
	     		<th>年化利率</th>
	     		<th>已获收益</th>
	     		<th>投标次数</th>
	     		<th>加入时间</th>
	     		<th>持有状态</th>
	     	</tr>
     	</thead>
     	<c:forEach items="${lfoll_invest_hold_list}" var="fix_bid_user_hold" varStatus="n">
     		<tr>
     			<td>${n.index + 1 }</td>
     			<td>${fix_bid_user_hold.m.bid_name}</td>
     			<td>${fix_bid_user_hold.m.real_name}</td>
     			<td>${fix_bid_user_hold.m.nick_name}</td>
     			<td>${fix_bid_user_hold.m.bid_money}</td>
     			<td>${fix_bid_user_hold.m.annual_earning}</td>
     			<td>${fix_bid_user_hold.m.earned_incom} </td>
     			<td>${fix_bid_user_hold.m.bid_num} </td>
     			<td>${fix_bid_user_hold.m.add_time} </td>
     			
     			<td>
     				<c:if test="${fix_bid_user_hold.m.state == 0}">
     					<span style="color:red">已退出</span>
     				</c:if>
     				<c:if test="${fix_bid_user_hold.m.state == 1}">
     					<span style="color:green">持有中</span>
     				</c:if>
     			</td>
     		</tr>
     	</c:forEach>
     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv }
		</div>
  </body>
  <script type="text/javascript">
  	$(function (){
  	  $('#add_new_fix_bid').click(
  		 function(){
  			window.location.href="<%=__ROOT_PATH__%>/user/lfoll_invest/goto_lfoll_invest_config.html";
	 	 }
  		);
  	});
  </script>
</html>
