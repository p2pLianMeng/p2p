<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>添加关于类型</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">添加类型</span></h3>
    <hr/>
    <div style="text-align: center">
    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/" method="post">
    <table class="table table-bordered table-hover ">
    	<tr>
    		<td class="tableleft">类型名称：</td>
	    	<td><input type="text" name="type_name" id="type_name" /></td>
    	</tr>
    	<tr>
    		<td class="tableleft">URL路径：</td>
	    	<td><input type="text" name="url" id="url"/></td>
    	</tr>
    	<tr>
    		<td class="tableleft">标记：</td>
   	 		<td><input type="text" name="tag" id="tag"/></td>
    	</tr>
    	<tr>
    		<td class="tableleft">排序：</td>
    		<td><input type="text" name="sort" value="${sort}" readonly="readonly" id="sort"/></td>
    	</tr>
  		<tr>
    		<td class="tableleft">是否有效：</td>
   	 		<td>
   	 			<input type="radio" name="status" value="1" /> 启用
				<input type="radio" name="status" value="0" checked="checked"/> 禁用
   	 		</td>
    	</tr>
    	<tr>
    		<td class="tableleft"></td>
    		<td>
    			<button type="button" class="btn btn-success" id="add_about_us_type" onclick="save_type();">保存</button>
   				<button type="button" class="btn btn-success" id="back" >返回</button>
   			</td>
    	</tr>
    	</table>
    </form>
    </div>
  </body>
</html>
<script type="text/javascript">

	$(function(){
		$("#back").click(function(){
			window.location.href="<%=__ROOT_PATH__%>/user/about_us/about_us_manage.html";
		});
	});
	 function save_type(){
		var type_name = $("#type_name").val();
		var url = $("#url").val();
		var tag = $("#tag").val();
		var status = $("input:radio:checked").val();
		var  sort = $("#sort").val();
		if(type_name == null || type_name ==""){
			showMessage(["提示","类型名称不能为空"]);
			return;
		}
		if(url == null || url ==""){
			showMessage(["提示","URL不能为空"]);
			return;
		}
		$.ajax({
			url:"<%=__ROOT_PATH__%>/user/about_us/save_type.html",
			type:"post",
			data:{"type_name":type_name,"url":url,"tag":tag,"status":status,"sort":sort},
			success:function(data){
				if(data == 1){
					showMessage(["提示","类型名称不能为空"]);
					return;
				}else if(data == 2){
					showMessage(["提示","URL不能为空"]);
					return;
				}else if(data == 3){
					showMessage(["提示","添加成功"]);
					return;
				}else if(data ==4){
					showMessage(["提示","添加失败"]);
					return;
				}
			}
		});
	}
	
</script>