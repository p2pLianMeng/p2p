<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/bootstrap-responsive.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/style.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/datepicker.css" />
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/bootstrap.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/ckform.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/common.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/bootstrap-datepicker.js"></script>

		<style type="text/css">
			body {
				padding-bottom: 40px;
			}
			
			.sidebar-nav {
				padding: 9px 0;
			}
			
			@media ( max-width : 980px) { /* Enable use of floated navbar text */
				.navbar-text.pull-right {
					float: none;
					padding-left: 5px;
					padding-right: 5px;
				}
			}
		</style>
	</head>

	<body>
		<table class="table table-bordered table-hover definewidth m10;border-bottom:none;">
			<thead>
				<tr>
					<th  align="center">
						编号
					</th>
					<th  align="center">
						借款方真实姓名
					</th>
					<th align="center">
						电话号码
					</th>
					<th align="center"> 
						身份证
					</th>
					<th align="center">
						还款总期数
					</th>
					<th align="center">
						当期期数
					</th>
					<th align="center">
						实际还款时间
					</th>
					<th align="center">
						应还本息
					</th>
					<th align="center">
						应还本金
					</th>
					<th align="center">
						应还利息
					</th>
					<th align="center">
						正常管理费
					</th>
					<th align="center">
						逾期管理费
					</th>
					<th align="center">
						逾期罚息
					</th>
					<th align="center">
						实际还款金额
					</th>
					<th align="center">
						还款方式
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${borrower_bulk_standard_overdue_repayment_record_list}" var="record" varStatus="row">
				<tr>
					<td>${row.count}</td>
					<td>${record.r.real_name}</td>
					<td>${record.r.phone}</td>
					<td>${record.r.user_identity}</td>
					<td>${record.r.total_periods}</td>
					<td>${record.r.current_period}</td>
					<td><fmt:formatDate value="${record.r.repay_actual_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td>${record.r.should_repayment_total}</td>
					<td>${record.r.should_repayment_principle}</td>
					<td>${record.r.should_repayment_interest}</td>
					<td>${record.r.normal_manage_fee}</td>
					<td>${record.r.over_manage_fee}</td>
					<td>${record.r.over_punish_interest}</td>
					<td>${record.r.actual_repayment}</td>
					<td>${record.r.is_one_off == 0 ? "分期":"一次性"}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
		<div style="padding-left:26px;"><div style="width:1232px;border:1px solid #e5e5e5;border-top:none;text-align: center"><span style="text-align: center">${tips}</span></div>
		</div>
		<div style="float:right;">
			${pageDiv }
		</div>
		
	</body>
</html>
<script type="text/javascript">
	var yest = new Date();
	jQuery('#start_time').datepicker({format:'yyyy-mm-dd',endDate:yest});
	jQuery('#end_time').datepicker({format:'yyyy-mm-dd',endDate:yest});
</script>
