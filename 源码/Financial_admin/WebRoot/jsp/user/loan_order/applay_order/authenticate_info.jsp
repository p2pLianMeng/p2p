<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
	String url = "http://www.lfoll.com/admin_visit_file";        //这里需要修改成web端地址
	String __DOWN_URL__ = url+"/download.html?password=459b58fdea4bc9564b4bb19c0709190a&path=";
	String __ZIP_DOWN_URL__ = url+"/download_zip.html?password=459b58fdea4bc9564b4bb19c0709190a&apply_order_id=";
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<title>审核用户认证信息</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/style.css" />
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
	</head>

	<body>
		<h5 >
			姓名：${real_name} &nbsp;&nbsp;
			用户身份证：${user_identity }&nbsp;&nbsp;
			订单id：${loan_order_id}&nbsp;&nbsp;
			
			<!--信用分数：<span style="color: red">${user_credit_files.m.credit_scores }</span>-->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a  href="<%= __ROOT_PATH__ %>/user/loan_order/loan_order_list_index.html">返回</a>
		</h5>
		<h4 align="center">
			操作：（
			<span>
			<c:if test="${borrow_type == 1}">消费贷</c:if>
			<c:if test="${borrow_type == 2}">生意贷</c:if>
			<c:if test="${borrow_type == 3}">净值贷</c:if>
			</span>）
			<a  style="color:red" href="javascript:void(0);" onclick="decline(${loan_order_id },'${real_name}');">拒绝</a>&nbsp;&nbsp;
	     	<a  style="color:red" href="javascript:void(0);" onclick="approve(${loan_order_id },'${real_name}');">批准</a>&nbsp;&nbsp;
			<a  id="reject" style="color:red;display:none" href="javascript:void(0);" onclick="rejected_modify(${loan_order_id },'${real_name}');">驳回修改
			</a>
			<input type="text" id="reason" style="display:none"/>

		</h4>
		<table class="table table-bordered table-hover definewidth m10">
		<tr><th>借款描述 </th> </tr>
			<tr><td>${ describe}<td></tr>
		</table>
		 
		<h5 align="center">
			<span style="color: blue">资产信息列表</span>
		</h5>
		<table class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						房产
					</th>
					<th>
						房贷
					</th>
					<th>
						车产
					</th>
					<th>
						车贷
					</th>

				</tr>
			</thead>
			<tr>
				<td>
					${user_authenticate_assets_info.m.housing}
				</td>
				<td>
					${user_authenticate_assets_info.m.housing_mortgage}
				</td>
				<td>
					${user_authenticate_assets_info.m.car}
				</td>
				<td>
					${user_authenticate_assets_info.m.car_mortgage}
				</td>
			</tr>
		</table>
		<h5 align="center">
			<span style="color: blue">个人基本信息列表</span>
		</h5>
		<table class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						最高学历
					</th>
					<th>
						入学年份
					</th>
					<th>
						毕业学校
					</th>
					<th>
						籍贯
					</th>
					<th>
						户口所在地
					</th>
					<th>
						住址
					</th>
					<th>
						邮编
					</th>
					<th>
						居住电话
					</th>
				</tr>
			</thead>
			<tr>
				<td>
					${user_authenticate_persional_info.m.highest_education}
				</td>
				<td>
					${user_authenticate_persional_info.m.year_of_admission}
				</td>
				<td>
					${user_authenticate_persional_info.m.school}
				</td>
				<td>
					${user_authenticate_persional_info.m.native_place_province}
					${user_authenticate_persional_info.m.native_place_city}
				</td>
				<td>
					${user_authenticate_persional_info.m.domicile_place_province}
					${user_authenticate_persional_info.m.domicile_place_city}
				</td>
				<td>
					${user_authenticate_persional_info.m.address}
				</td>
				<td>
					${user_authenticate_persional_info.m.zip_code}
				</td>
				<td>
					${user_authenticate_persional_info.m.landline}
				</td>
			</tr>
		</table>
		<h5 align="center">
			<span style="color: blue">家庭信息列表</span>
		</h5>
		<table class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						婚姻状况
					</th>
					<th>
						有无子女
					</th>
					<th>
						直属亲属名字
					</th>
					<th>
						关系
					</th>
					<th>
						直属亲属电话
					</th>
					<th>
						其他联系人一名字
					</th>
					<th>
						关系
					</th>
					<th>
						其他联系人一电话
					</th>
					<th>
						其他联系人二名字
					</th>
					<th>
						关系
					</th>
					<th>
						其他联系人二电话
					</th>

				</tr>
			</thead>
			<tr>
				<td>
					${user_authenticate_family_info.m.marriage}
				</td>
				<td>
					${user_authenticate_family_info.m.children}
				</td>
				<td>
					${user_authenticate_family_info.m.lineal_relatives_name}
				</td>
				<td>
					${user_authenticate_family_info.m.lineal_relatives_relation}
				</td>
				<td>
					${user_authenticate_family_info.m.lineal_relatives_phone}
				</td>
				<td>
					${user_authenticate_family_info.m.other_contacts_name}
				</td>
				<td>
					${user_authenticate_family_info.m.other_contacts_relation}
				</td>
				<td>
					${user_authenticate_family_info.m.other_contacts_phone}
				</td>
				<td>
					${user_authenticate_family_info.m.other_contacts_name_two}
				</td>
				<td>
					${user_authenticate_family_info.m.other_contacts_relation_two}
				</td>
				<td>
					${user_authenticate_family_info.m.other_contacts_phone_two}
				</td>
			</tr>
		</table>

		<h5 align="center">
			<span style="color: blue">工作信息列表</span>
		</h5>
		<table class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						公司名字
					</th>
					<th>
						职位
					</th>
					<th>
						月收入
					</th>
					<th>
						电子邮箱
					</th>
					<th>
						工作省市
					</th>
					<th>
						公司地址
					</th>
					<th>
						公司类型
					</th>
					<th>
						公司行业
					</th>
					<th>
						公司规模
					</th>
					<th>
						现单位工作年限
					</th>
					<th>
						公司电话
					</th>

				</tr>
			</thead>
			<tr>
				<td>
					${user_authenticate_work_info.m.company_name}
				</td>
				<td>
					${user_authenticate_work_info.m.position}
				</td>
				<td>
					${user_authenticate_work_info.m.monthly_income}
				</td>
				<td>
					${user_authenticate_work_info.m.work_email}
				</td>
				<td>
					${user_authenticate_work_info.m.work_province}
					${user_authenticate_work_info.m.work_city}
				</td>
				<td>
					${user_authenticate_work_info.m.company_address}
				</td>
				<td>
					${user_authenticate_work_info.m.company_type}
				</td>
				<td>
					${user_authenticate_work_info.m.company_trades}
				</td>
				<td>
					${user_authenticate_work_info.m.company_size}
				</td>
				<td>
					${user_authenticate_work_info.m.year_limit}
				</td>
				<td>
					${user_authenticate_work_info.m.company_phone}
				</td>
			</tr>
		</table>
		<h5 id="common_basic"  style="display:none" align="center">
			<span >个人基本信息认证评分：</span>
			<input id="100" type="text" value="${user_credit_files.m.basic_info_credit_scores}">
			<input id="" type="button" onclick="score(100,0);" value="评分">
		</h5>
		
		<h5 align="center">
			<span style="color: blue">上传认证信息列表（<a   href="<%=__ZIP_DOWN_URL__ %>${loan_order_id}">打包下载</a>）</span>
		</h5>
		
		<table id="net_upload" align="center" style="display:none" class="table table-bordered table-hover definewidth m10">
			<thead >
				<tr>
					<th >
						身份认证
					</th>
				</tr>
			</thead>
			<tr>
				<td >
				<table>
				<tr>
				<c:forEach items="${upload_info[0]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>
				<input style="display:none" id="101" type="text" value="1">
				<input id="1_0" type="button" onclick="score(101,1);" value="通过" />
				</td>
			</tr>
			</table>
		
		<table id="common_1" style="display:none" class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						身份认证
					</th>
					<th>
						工作认证
					</th>
					<th>
						信用认证
					</th>
					<th>
						收入认证
					</th>
				</tr>
			</thead>
			<tr>
				<td >
				<table>
				<tr>
				<c:forEach items="${upload_info[0]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[1]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[2]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[3]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
			</tr>
				<td>
				<input id="0" type="text" value="${user_authenticate_upload_info.m.id_authenticate_scores}">
				<input id="1_0" type="button" onclick="score(0,1);" value="通过" />
				<input id="1_0" type="button" onclick="score(0,2);" value="不通过" />
				</td>
				<td>
				<input id="1" type="text" value="${user_authenticate_upload_info.m.work_authenticate_scores}">
				<input id="1_1" type="button" onclick="score(1,1);" value="通过" />
				<input id="1_1" type="button" onclick="score(1,2);" value="不通过" />
				</td>				
				<td>
				<input id="2" type="text" value="${user_authenticate_upload_info.m.credit_authenticate_scores}">
				<input id="1_2" type="button" onclick="score(2,1);" value="通过" />
				<input id="1_2" type="button" onclick="score(2,2);" value="不通过" />
				</td>				
				<td>
				<input id="3" type="text" value="${user_authenticate_upload_info.m.income_authenticate_scores}">
				<input id="1_3" type="button" onclick="score(3,1);" value="通过" />
				<input id="1_3" type="button" onclick="score(3,2);" value="不通过" />
				</td>
			<tr>
			
			</tr>
		</table>
			<table id="common_2" style="display:none" class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						房产认证
					</th>
					<th>
						车产认证
					</th>
					<th>
						婚姻认证
					</th>
					<th>
						教育认证
					</th>
					<th>
						职称认证
					</th>
					<th>
						居住认证
					</th>
				</tr>
			</thead>
			<tr>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[4]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[5]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[6]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[7]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[8]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
				<td>
				<table>
				<tr>
				<c:forEach items="${upload_info[9]}" var="i" >
				<a style="cursor:pointer;">
				<c:forEach items="${i}" var="url" >
				<a  href="<%= __DOWN_URL__%>${url}"> 下载</a>
				</c:forEach>
				</a>
				</c:forEach>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>
				<input id="4" type="text" value="${user_authenticate_upload_info.m.housing_authenticate_scores}">
				<input id="1_4" type="button" onclick="score(4,1);" value="通过" />
				<input id="1_4" type="button" onclick="score(4,2);" value="不通过" />
				</td>
				<td>
				<input id="5" type="text" value="${user_authenticate_upload_info.m.car_authenticate_scores}">
				<input id="1_5" type="button" onclick="score(5,1);" value="通过" />
				<input id="1_5" type="button" onclick="score(5,2);" value="不通过" />
				</td>
				<td>
				<input id="6" type="text" value="${user_authenticate_upload_info.m.marriage_authenticate_scores}">
				<input id="1_6" type="button" onclick="score(6,1);" value="通过" />
				<input id="1_6" type="button" onclick="score(6,2);" value="不通过" />
				</td>
				<td>
				<input id="7" type="text" value="${user_authenticate_upload_info.m.education_authenticate_scores}">
				<input id="1_7" type="button" onclick="score(7,1);" value="通过" />
				<input id="1_7" type="button" onclick="score(7,2);" value="不通过" />
				</td>
				<td>
				<input id="8" type="text" value="${user_authenticate_upload_info.m.title_authenticate_scores}">
				<input id="1_8" type="button" onclick="score(8,1);" value="通过" />
				<input id="1_8" type="button" onclick="score(8,2);" value="不通过" />
				</td>
				<td>
				<input id="9" type="text" value="${user_authenticate_upload_info.m.living_authenticate_scores}">
				<input id="1_9" type="button" onclick="score(9,1);" value="通过" />
				<input id="1_9" type="button" onclick="score(9,2);" value="不通过" />
				</td>
			</tr>
		</table>
		</hr>
	</body>
	<script type="text/javascript">
var user_id = ${user_id};
$(function (){
	if(${borrow_type == 3}){
		$("#net_upload").show();
	}else{
		$("#reject").show();
		$("#reason").show();
		$("#common_basic").show();
		$("#common_1").show();
		$("#common_2").show();
	}
	 var obj = [${user_authenticate_assets_info.m.status},${user_authenticate_family_info.m.status},${user_authenticate_persional_info.m.status},${user_authenticate_work_info.m.status}];
	 for(var i = 5;i<3;i++){
	 	if(obj[i]==1){
	 		$("#"+i).val("不通过");
	 	}
	 }
})

function score(type,okorno){
	var scores = $("#"+type).val();
	$.ajax(
			{url:'<%=__ROOT_PATH__%>/user/loan_order/authentication_info_scores.html',
			type:'post',
			cache:false,
			async:false,
			data:{"user_id":user_id,"scores":scores,"type":type,"okorno":okorno},
			success:function(data) //回传函数
				{
				if(data=="no"){
					alert("评分失败");
				}else if(data=="err"){
					alert("分数有误");
				}else{
					alert("评分成功");
					$("#"+type).val(data);
				}
			}
		},"html","application/x-www-form-urlencoded; charset=utf-8")
 }
 
 function upload_scores(type,scores,status){
	$.ajax(
			{url:'<%=__ROOT_PATH__%>/user/loan_order/upload_scores.html',
			type:'post',
			cache:false,
			async:false,
			data:{"user_id":user_id,"scores":scores,"type":type,"status":status},
			success:function(data) //回传函数
				{
				if(data=="no"){
					alert("操作失败");
				}if(data=="ok"){
					alert("操作成功");
				}
			}
		},"html","application/x-www-form-urlencoded; charset=utf-8")
 }

 function rejected_modify(order_id,real_name){//4
		var confirm_str = '确定要驳回修改"'+real_name+'"的申请吗?';
	  	var decline = confirm(confirm_str);
	  	var reason = $("#reason").val();
	  	if(decline){
	  	$.post('<%=__ROOT_PATH__%>/user/loan_order/rejected_modify_loan_apply.html',
				{
					"loan_apply_order_id" : order_id,"reason":reason
				},
				function(data) //回传函数
				{
					if(data=='1'){	//操作成功
						alert('操作成功');
						window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
					}
					if(data=='0'){ //操作失败
						alert('操作失败');
						window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
					}
					if(data=="no"){ //
						alert('请填写原因！');
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
	  	 }
}
function decline(order_id,real_name){//5
		var confirm_str = '确定要拒绝"'+real_name+'"的申请吗?';
	  	var decline = confirm(confirm_str);
	  	if(decline){
	  	$.post('<%=__ROOT_PATH__%>/user/loan_order/decline_loan_apply.html',
				{
					"loan_apply_order_id" : order_id,
				},
				function(data) //回传函数
				{
					if(data=='1'){	//操作成功
						alert('操作成功');
						window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
					}
					if(data=='0'){ //操作失败
						alert('操作失败');
						window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
	  	 }
}
function approve(order_id,real_name){//6
	//var user_id = $("#user_id").html();
	var confirm_str = '确定要批准"'+real_name+'"的申请吗?';
 	var approved = confirm(confirm_str);
 	if(approved){

 	$.post('<%=__ROOT_PATH__%>/user/loan_order/approve_loan_apply.html',
			{
				"loan_apply_order_id" : order_id,
			},
			function(data) //回传函数
			{
				if(data=='1'){	//操作成功
					alert('操作成功');
					window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
				}
				if(data=='0'){ //操作失败
					alert('操作失败');
					window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
				}
			}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
 	 }
}
 
  </script>
</html>
