<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>审核用户借款申请</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
  	<%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">借款申请列表</span></h3>
     <hr/>
     

	<form class="form-inline definewidth m20" action="<%=__ROOT_PATH__ %>/user/loan_order/loan_order_list_query.html" method="post">
     	用户ID：
		<input type="text" name="user_id" id="id" value="${user_id}" class="abc input-default"  style="width: 50px"/>
     	&nbsp;&nbsp;手机：
		<input type="text" name="phone" id="phone" value="${phone}" class="abc input-default"  style="width: 100px"/>
     	&nbsp;&nbsp;姓名：
     	<input type="text" name="real_name" id="real_name" value="${real_name}" class="abc input-default"  style="width: 70px"/>

     	     
		&nbsp;&nbsp;借款期限（月）：
		<select id="borrow_duration" name="borrow_duration" style="width:135px">
		<c:choose>
			<c:when test="${borrow_duration==-1}">
				<option value="-1" selected="selected">--选择月--</option>
			</c:when>
			<c:otherwise>
				<option value="-1">--选择月--</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==1}">
				<option value="1" selected="selected">1个月</option>
			</c:when>
			<c:otherwise>
				<option value="1">1个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==2}">
				<option value="2" selected="selected">2个月</option>
			</c:when>
			<c:otherwise>
				<option value="2">2个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==3}">
				<option value="3" selected="selected">3个月</option>
			</c:when>
			<c:otherwise>
				<option value="3">3个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==4}">
				<option value="4" selected="selected">4个月</option>
			</c:when>
			<c:otherwise>
				<option value="4">4个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==5}">
				<option value="5" selected="selected">5个月</option>
			</c:when>
			<c:otherwise>
				<option value="5">5个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==6}">
				<option value="6" selected="selected">6个月</option>
			</c:when>
			<c:otherwise>
				<option value="6">6个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==7}">
				<option value="7" selected="selected">7个月</option>
			</c:when>
			<c:otherwise>
				<option value="7">7个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==8}">
				<option value="8" selected="selected">8个月</option>
			</c:when>
			<c:otherwise>
				<option value="8">8个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==9}">
				<option value="9" selected="selected">9个月</option>
			</c:when>
			<c:otherwise>
				<option value="9">9个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==10}">
				<option value="10" selected="selected">10个月</option>
			</c:when>
			<c:otherwise>
				<option value="10">10个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==11}">
				<option value="11" selected="selected">11个月</option>
			</c:when>
			<c:otherwise>
				<option value="11">11个月</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration==12}">
				<option value="12" selected="selected">12个月</option>
			</c:when>
			<c:otherwise>
				<option value="12">12个月</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${borrow_duration==15}">
				<option value="15" selected="selected">15个月</option>
			</c:when>
			<c:otherwise>
				<option value="15">15个月</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${borrow_duration==18}">
				<option value="18" selected="selected">18个月</option>
			</c:when>
			<c:otherwise>
				<option value="18">18个月</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${borrow_duration==24}">
				<option value="24" selected="selected">24个月</option>
			</c:when>
			<c:otherwise>
				<option value="24">24个月</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${borrow_duration==36}">
				<option value="36" selected="selected">36个月</option>
			</c:when>
			<c:otherwise>
				<option value="36">36个月</option>
			</c:otherwise>
		</c:choose>
		
	 </select>
	&nbsp;&nbsp; 借款期限（天）：
		<select id="borrow_duration_day" name="borrow_duration_day" style="width:135px">
			<c:choose>
			<c:when test="${borrow_duration_day==-1}">
				<option value="-1" selected="selected">--选择天--</option>
			</c:when>
			<c:otherwise>
				<option value="-1">--选择天--</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration_day == 5}">
				<option value="5" selected="selected">5天</option>
			</c:when>
			<c:otherwise>
				<option value="5">5天</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration_day == 10}">
				<option value="10" selected="selected">10天</option>
			</c:when>
			<c:otherwise>
				<option value="10">10天</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration_day == 15}">
				<option value="15" selected="selected">15天</option>
			</c:when>
			<c:otherwise>
				<option value="15">15天</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration_day == 20}">
				<option value="20" selected="selected">20天</option>
			</c:when>
			<c:otherwise>
				<option value="20">20天</option>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${borrow_duration_day == 25}">
				<option value="25" selected="selected">25天</option>
			</c:when>
			<c:otherwise>
				<option value="25">25天</option>
			</c:otherwise>
		</c:choose>
		</select>
	 &nbsp;&nbsp;申请状态
     	<select id="state" name="state" style="width:135px">
     	
     	
		<c:choose>
			<c:when test="${state==3}">
				<option value="3" selected="selected">等待审核中</option>
			</c:when>
			<c:otherwise>
				<option value="3">等待审核中</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${state==4}">
				<option value="4" selected="selected">驳回修改中</option>
			</c:when>
			<c:otherwise>
				<option value="4">驳回修改中</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${state==5}">
				<option value="5" selected="selected">审核不通过</option>
			</c:when>
			<c:otherwise>
				<option value="5">审核不通过</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${state==6}">
				<option value="6" selected="selected">审核通过</option>
			</c:when>
			<c:otherwise>
				<option value="6">审核通过</option>
			</c:otherwise>
		</c:choose>

				
     	</select>
	 <br/>
	 申请单提交时间 ：从
     	<input type="text" name="commit_start_time" id="commit_start_time" style="width: 150px" value="${commit_start_time}"  onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/>
     	到：
     	<input type="text" name="commit_end_time" id="commit_end_time" style="width: 150px" value="${commit_end_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/>
     	&nbsp;&nbsp; &nbsp;&nbsp; 
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
     </form>



     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>用户id</th>
	     		<th>真实姓名</th>
	     		<th>手机号码</th>
	     		<th>贷款类型</th>
	     		<th>借款标题</th>
	     		<th>借款用途</th>
	     		<th>借款金额</th>
	     		<th>借款期限</th>
	     		<th>年化利率</th>
	     		<th>描述</th>
	     		<th>保存时间</th>
	     		<th>提交时间</th>
	     		<th>查看借款信息</th>
	     		<th>状态</th>
	     		
	     	</tr>
     	</thead>
     	<c:forEach items="${apply_order_list}" var="order">
     		<tr>
     			<td id="user_id">${order.r.user_id }</td>
     			<td >${order.r.real_name }</td>
     			<td>${order.r.phone }</td>
     			<td>
     				<c:if test="${order.r.borrow_type==1}">
     					工薪贷
     				</c:if>
     				<c:if test="${order.r.borrow_type==2}">
     					生意贷
     				</c:if>
     				<c:if test="${order.r.borrow_type==3}">
     					净值贷
     				</c:if>
     			</td>
     			<td>${order.r.borrow_title }</td>
     			<td>${order.r.borrow_purpose }</td>
     			<td>${order.r.borrow_all_money}
     				<c:if test="${order.r.state == 3}">
     					<c:if test="${order.r.borrow_type==1}">
	     					<a href="<%=__ROOT_PATH__%>/user/loan_order/to_modify_borrow_money.html?borrow_order_id=${order.r.id}">修改</a>
	     				</c:if>
	     				<c:if test="${order.r.borrow_type==2}">
	     					<a href="<%=__ROOT_PATH__%>/user/loan_order/to_modify_borrow_money.html?borrow_order_id=${order.r.id}">修改</a>
	     				</c:if>
     				</c:if>
     			</td>
     			<td>${order.r.borrow_duration }</td>
     			<td>${order.r.annulized_rate }</td>
     			<td>${order.r.describe }</td>
     			<td>${order.r.save_time }</td>
     			<td>${order.r.commit_time }</td>
     			<td><a href="<%=__ROOT_PATH__%>/user/loan_order/authentication_info_index.html?loan_order_id=${order.r.id}&real_name=${order.r.real_name}&user_id=${order.r.user_id}&user_identity=${order.r.user_identity}&describe=${order.r.describe }">查看</a></td>
     			
     			<c:if test="${order.r.state == 3}">
	     			<td>等待审核</td>
     			</c:if>
     			<c:if test="${order.r.state == 4}">
     				<td>驳回修改中</td>
     			</c:if>
     			<c:if test="${order.r.state == 5}">
     				<td>已拒绝申请</td>
     			</c:if>
     			<c:if test="${order.r.state == 6}">
     				<td>已经审核通过</td>
     			</c:if>
     		
     				
     		</tr>
     	</c:forEach>
     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv }
	</div>
  </body>
  <script type="text/javascript">
  	$(function (){
  	  $('#add_new_fix_bid').click(
  		 function(){
  			window.location.href="<%=__ROOT_PATH__%>/user/lfoll_invest/goto_lfoll_invest_config.html";
	 	 }
  		);
  	});

  </script>
</html>
