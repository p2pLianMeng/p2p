<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
    
    
    	

     <hr/>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     	
	     		<th>筹款单id</th>
	     		<th>期数</th>
	     		<th>总期数</th>
	     		<th>还款日</th>
	     		<th>还款截止时间</th>
	     		<th>应还本息</th>
	     		<th>实际还款时间</th>
	     		<th>正常管理费</th>
	     		<th>逾期管理费</th>
	     		<th>逾期罚息</th>
	     		<th>是否还款</th>
	     		
	     		<th>持有份额</th>
	     		<th>应收金额</th>

	     	</tr>
     	</thead>
     	
     	<c:forEach items="${debt_detail}" var="debt">
     		<tr>
     			<td>${debt.r.gather_money_order_id}</td>
     			<td>${debt.r.current_period}</td>
     			<td>${debt.r.total_periods}</td>
     			<td>${debt.r.automatic_repayment_date}</td>
     			<td>${debt.r.repay_end_time}</td>
     			<td>${debt.r.should_repayment_total}</td>
     			<td>${debt.r.repay_actual_time}</td>
     			<td>${debt.r.normal_manage_fee}</td>
     			<td>${debt.r.over_manage_fee}</td>
     			<td>${debt.r.over_punish_interest}</td>
     			<c:if test="${debt.r.is_repay==0}">
     				<td>否</td>
     			</c:if>
     			<c:if test="${debt.r.is_repay==1}">
     				<td>是</td>
     			</c:if>
     			
     			<td>${debt.r.hold_share}</td>
     			<td>${debt.r.recieve_money}</td>
     			
     		</tr>
     	</c:forEach>

     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv}
	</div>
  </body>
</html>
