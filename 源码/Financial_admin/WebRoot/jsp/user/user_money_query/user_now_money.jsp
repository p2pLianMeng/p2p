<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/style.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/datepicker.css" />
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/js/bootstrap-datepicker.js"></script>

		<style type="text/css">
body {
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}

@media ( max-width : 980px) { /* Enable use of floated navbar text */
	.navbar-text.pull-right {
		float: none;
		padding-left: 5px;
		padding-right: 5px;
	}
}
</style>
	</head>

	<body>
		<form class="form-inline definewidth m20" width="100%"
			action="<%=__ROOT_PATH__%>/user/user_money_query/user_money_query_query.html"
			method="post">
			
			用户id：
			<input type="text" name="id" id="id"
				class="abc input-default" placeholder="" style="width: 30px"
				value="${id}">
			真实姓名：
			<input type="text" name="real_name" id="real_name"
				class="abc input-default" placeholder="" style="width: 50px"
				value="${real_name}">
			昵称：
			<input type="text" name="nickname" id="nickname"
				class="abc input-default" placeholder="" style="width: 50px"
				value="${nick_name}">
			手机号：
			<input type="text" name="phone" id="phone"
				class="abc input-default" placeholder="" style="width: 90px"
				value="${phone}">

			邮箱：
			<input type="text" name="email" id="email" class="abc input-default"
				placeholder="" style="width: 100px" value="${email}">
			身份证号：
			<input type="text" name="user_identity" id="user_identity"
				class="abc input-default" placeholder="" style="width: 130px"
				value="${user_identity}">

			已禁用登录：
			<c:choose>
				<c:when test="${is_active=='-1'}">
								全部<input type="radio" checked="true" value="-1" id="check3"
						name="is_active" />
				</c:when>
				<c:otherwise>
								全部<input type="radio" id="check3" value="-1" name="is_active" />
				</c:otherwise>
			</c:choose>

			<c:choose>
				<c:when test="${is_active=='0'}">
								是<input type="radio" checked="true" value="0" id="check1"
						name="is_active" />
				</c:when>
				<c:otherwise>
								是<input type="radio" value="0" id="check1" name="is_active" />
				</c:otherwise>
			</c:choose>



			<c:choose>
				<c:when test="${is_active=='1'}">
								否<input type="radio" checked="true" value="1" id="check1"
						name="is_active" />
				</c:when>
				<c:otherwise>
								否<input type="radio" value="1" id="check1" name="is_active" />
				</c:otherwise>
			</c:choose>

			<input type="submit" class="btn btn-primary" value="查询" />
		</form>
		<table
			class="table table-bordered table-hover definewidth m10;border-bottom:none;">
			<thead>
				<tr>
					<th align="center" style="width:30px;">
						用户id
					</th>
					<th align="center">
						姓名
					</th>
					<th align="center">
						昵称
					</th>
					<th align="center">
						手机号码
					</th>
					<th align="center">
						邮箱
					</th>
					<th align="center">
						身份证号
					</th>
					<th align="center">
						电话号码
					</th>
					<th align="center">
						可用余额
					</th>
					<th align="center">
						冻结人民币
					</th>
					<th align="center">
						总资产
					</th>
					<th align="center" >
						操作
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${user_info_list}" var="record">
					<tr>
						<td style="width:30px;">
							${record.r.id}
						</td>
						<td>
							${record.r.real_name}
						</td>
						<td>
							${record.r.nickname}
						</td>
						<td>
							${record.r.phone}
						</td>
						<td>
							${record.r.email}
						</td>
						<td>
							${record.r.user_identity}
						</td>
						<td>
							${record.r.phone}
						</td>
						<td>
							<c:if test="${record.r.cny_can_used == '0E-8'}">0</c:if>
							<c:if test="${record.r.cny_can_used != '0E-8'}">${record.r.cny_can_used}</c:if>
						</td>
						<td>
							<c:if test="${record.r.cny_freeze == '0E-8'}">0</c:if>
							<c:if test="${record.r.cny_freeze != '0E-8'}">${record.r.cny_freeze}</c:if>
						</td>
						<td>
							<c:if test="${record.r.cny_freeze+record.r.cny_can_used == '0E-8'}">0</c:if>
							<c:if test="${record.r.cny_freeze+record.r.cny_can_used != '0E-8'}">${record.r.cny_freeze+record.r.cny_can_used}</c:if>
						</td>
						<td>
							<c:if test="${record.r.status == 0}">
								<a style="text-decoration: none;"
									href="<%=__ROOT_PATH__%>/user/user_money_query/frozen_account.html?user_id=${record.r.user_id }">冻结资产</a>
							</c:if>
							<c:if test="${record.r.status == 1}">
								<a style="text-decoration: none;"
									href="<%=__ROOT_PATH__%>/user/user_money_query/frozen_account.html?user_id=${record.r.user_id }">解冻资产</a>
							</c:if>
							<c:if test="${record.r.is_active == 1}">
								<a style="text-decoration: none;"
									href="<%=__ROOT_PATH__%>/user/user_money_query/ban.html?id=${record.r.id }">禁止登陆</a>
							</c:if>
							<c:if test="${record.r.is_active == 0}">
								<a style="text-decoration: none;"
									href="<%=__ROOT_PATH__%>/user/user_money_query/ban.html?id=${record.r.id }">开放登录</a>
							</c:if>
							<a style="text-decoration: none;"
								href="<%=__ROOT_PATH__%>/user/user_money_query/to_update_money_page.html?id=${record.r.id}">修改资产信息</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div style="float: right;">
			${pageDiv }
		</div>

	</body>
</html>
