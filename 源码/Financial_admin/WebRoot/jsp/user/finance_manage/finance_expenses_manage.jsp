<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>联富金融财务收入查询</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <%@include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">联富金融财务支出查询</span></h3>
     <hr/>
    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/finance_manage/find_financial_expenses_records.html" method="post">
     	用户姓名：
     	<input type="text" name="real_name" id="real_name" class="abc input-default" style="width: 100px" value="${real_name}" />
     	开始时间
     	<input type="text" name="start_time" id="start_time" style="width: 150px" value="${start_time}"  onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate" />
     	结束时间
     	<input type="text" name="end_time" id="end_time" style="width: 150px" value="${end_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate" />
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
		<a href="javascript:void(0);" onclick="export_financial_expenses_to_excel();"><button type="button" class="btn btn-success" id="add_new_fix_bid">导出Excel</button></a>
     </form>
     
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>用户姓名</th>
	     		<th>支出金额</th>
	     		<th>明细</th>
	     		<th>时间</th>
	     	</tr>
     	</thead>
     	<c:if test="${not empty sys_income_record_list}">
     		<c:forEach items="${sys_income_record_list}" var="record">
     			<tr>
     				<td>${record.r.real_name}</td>
     				<td>${record.r.money}</td>
     				<td>${record.r.detail}</td>
     				<td>${record.r.add_time}</td>
     			</tr>
     		</c:forEach>
     	</c:if>
     </table>
    <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
	<div style="float:right;">
		${pageDiv }
	</div>
  </body>
  <script type="text/javascript">
  		function export_financial_expenses_to_excel(){
			var real_name = $("#real_name").val();
			var start_time = $("#start_time").val();
			var end_time = $("#end_time").val();
			window.location.href = "<%=__ROOT_PATH__%>/exportExcel/export_financial_expenses_to_excel.html?real_name=" + real_name +"&start_time=" + start_time + "&end_time=" + end_time;
  		}
  </script>
</html>
