<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>客户提现申请列表</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
  	<%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">客户提现申请列表</span></h3>
     <hr/>
     <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/finance_manage/handle_user_cash_withdraw_query.html" method="post">
     	姓名：
     	<input type="text" name="real_name" id="real_name" value="${real_name}" class="abc input-default"  style="width: 100px"/>
     	身份证号：
     	<input type="text" name="id_no" id="id_no" value="${id_no}" class="abc input-default"  style="width: 100px"/>
     	申请日期：
			
		<input type="date" name="add_time_start" id="add_time_start" value="${add_time_start}" />
		
		<input type="date" name="add_time_end" id="add_time_end" value="${add_time_end}" />
			
     	审核状态：
     	<select id="is_pay" name="is_pay" style="width:135px">
     	
     			<option value="-1">全部</option>
     			<option value="1">未处理</option>
	     		<option value="2">已汇款</option>
	     		<option value="3">已拒绝汇款</option>
     	</select>
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
     </form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>真实姓名</th>
	     		<th>身份证</th>
	     		<th>开户名</th>
	     		<th>开户行</th>
	     		<th>账号</th>
	     		<th>申请金额</th>
	     		<th>提现费用</th>
	     		<th>转账金额</th>
	     		<th>申请时间</th>
	     		<th>操作</th>
	     	</tr>
     	</thead>

     	<c:forEach items="${withdraw_list}" var="withdraw_apply">
     	
     		<tr>
     			<td>${withdraw_apply.r.real_name }</td>
     			<td>${withdraw_apply.r.user_identity }</td>
     			<td>${withdraw_apply.r.account_holder }</td>
     			<td>${withdraw_apply.r.bank_name }</td>
     			<td>${withdraw_apply.r.bank_card_num }</td>
     			<td>${withdraw_apply.r.amount }</td>
     			<td>${withdraw_apply.r.fee }</td>
     			<td>${withdraw_apply.r.transfer_money }</td>
     			<td>${withdraw_apply.r.add_time }</td>
     			<td>
					<c:if test="${withdraw_apply.r.is_pay==1}">
						<a href="#" onclick="approve(${withdraw_apply.r.id },'${withdraw_apply.r.real_name }')">汇款</a>&nbsp;&nbsp;
						<a href="#" onclick="decline(${withdraw_apply.r.id },'${withdraw_apply.r.real_name }')">拒绝汇款</a>
					</c:if>
					<c:if test="${withdraw_apply.r.is_pay==2}">已汇款</c:if>
					<c:if test="${withdraw_apply.r.is_pay==3}">已拒绝汇款</c:if>
				</td>
     			
     		</tr>
     	</c:forEach>

     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv }
	</div>
  </body>
  <script type="text/javascript">
  	$(function (){
	  	var is_pay = ${is_pay};
		$("#is_pay option").each(function(){
			if($(this).val() == is_pay){
				$(this).attr("selected",true); 
			}
		});
  	});

  	function approve(withdraw_id,real_name){
		//var user_id = $("#user_id").html();
		var confirm_str = '确定要批准"'+real_name+'"的申请吗？';
  	  	var approved = confirm(confirm_str);
  	  	if(approved){

  	  	$.post('<%=__ROOT_PATH__%>/user/finance_manage/approve_withdraw_apply_second.html',
				{
					"withdraw_id" : withdraw_id,
				},
				function(data) //回传函数
				{
					if(data=='1'){	//操作成功
						alert('操作成功');
						window.location.href="<%=__ROOT_PATH__%>/user/finance_manage/handle_user_cash_withdraw_query.html";
					}
					if(data=='0'){ //操作失败
						alert('操作失败');
						window.location.href="<%=__ROOT_PATH__%>/user/finance_manage/handle_user_cash_withdraw_query.html";
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
  	  	 }
  	 }
 	 function decline(withdraw_id,real_name){
 		var confirm_str = '确定要拒绝"'+real_name+'"的申请吗？';
  	  	var decline = confirm(confirm_str);
  	  	if(decline){
  	  	$.post('<%=__ROOT_PATH__%>/user/finance_manage/decline_withdraw_apply_second.html',
				{
					"withdraw_id" : withdraw_id,
				},
				function(data) //回传函数
				{
					if(data=='1'){	//操作成功
						alert('操作成功');
						window.location.href="<%=__ROOT_PATH__%>/user/finance_manage/handle_user_cash_withdraw_query.html";
					}
					if(data=='0'){ //操作失败
						alert('操作失败');
						window.location.href="<%=__ROOT_PATH__%>/user/finance_manage/handle_user_cash_withdraw_query.html";
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
  	  	 }
 	 }
  	
  </script>
</html>
