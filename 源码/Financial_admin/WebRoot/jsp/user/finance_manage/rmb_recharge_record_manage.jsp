<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>人民币充值记录查询管理页面</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<%@include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">充值记录管理页面</span></h3>
     <hr/>
    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/finance_manage/get_recharge_record.html" method="post">
     	用户姓名：
     	<input type="text" name="real_name" id="real_name" class="abc input-default" style="width: 100px" value="${real_name}" />
     	身份证号：
     	<input type="text" name="user_identity" id="user_identity" class="abc input-default" style="width: 200px" value="${user_identity}" />
     	开始时间
     	<input type="text" name="start_time" id="start_time" style="width: 150px" value="${start_time}"  onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate" />
     	结束时间
     	<input type="text" name="end_time" id="end_time" style="width: 150px" value="${end_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate" />
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
		<a href="javascript:void(0);" onclick="export_rmb_recharge_record();"><button type="button" class="btn btn-success" id="add_new_fix_bid">导出Excel</button></a>
     </form>
     
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>用户姓名</th>
	     		<th>身份证号</th>
	     		<th>充值金额</th>
	     		<th>实际充值金额</th>
	     		<th>充值时间</th>
	     		<th>充值类型</th>
	     	</tr>
     	</thead>
     	<c:if test="${not empty user_recharge_log_list}">
     		<c:forEach items="${user_recharge_log_list}" var="record">
     			<tr>
     				<td>${record.r.real_name}</td>
     				<td>${record.r.user_identity}</td>
     				<td>${record.r.amount}</td>
     				<td>${record.r.actual_amount}</td>
     				<td>${record.r.add_time}</td>
     				<td>
     					<c:if test="${record.r.type == 1}">
     						<span style="color:green">正常充值</span>
     					</c:if>
     					<c:if test="${record.r.type == 2}">
     						<span style="color:red">其他奖励</span>
     					</c:if>
     				</td>
     			</tr>
     		</c:forEach>
     	</c:if>
     </table>
    <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
	<div style="float:right;">
		${pageDiv}
	</div>
  </body>
  <script type="text/javascript">
  		function export_rmb_recharge_record(){
  	  		var real_name = $("#real_name").val();
  	  		var user_identity = $("#user_identity").val();
  	  		var start_time = $("#start_time").val();
  	  		var end_time = $("#end_time").val();
  	  		if(real_name == null){
  	  			real_name = "";
  	  		}
  	  		if(user_identity == null){
  	  			user_identity = "";
	  		}
  	 		 if(start_time == null){
  	 			start_time = "";
  			}
  			if(end_time == null){
  				end_time = "";
			}
			window.location.href = "<%=__ROOT_PATH__%>/exportExcel/export_charge_record_excel.html?real_name=" + real_name + "&user_identity=" + user_identity +"&start_time=" + start_time +"&end_time=" + end_time;
  		}
  </script>
</html>
