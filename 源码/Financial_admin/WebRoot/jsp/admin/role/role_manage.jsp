<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		 <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
	</head>

	<body>
		<form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/admin/admin/query_role_info.html" method="post">  
		    角色名称：
		    <input type="text" name="role_name" id="role_name"class="abc input-default" placeholder="" value="">&nbsp;&nbsp;  
		    <button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
		    <button type="button" class="btn btn-success" id="addnewrole">新增角色</button>
		</form>
		<table class="table table-bordered table-hover definewidth m10" >
		   	 <thead>
			    <tr>
			        <th>角色id</th>
			        <th>角色名称</th>
			        <th>状态</th>
			        <th>操作</th>
			    </tr>
		    </thead>
	    	<c:forEach items="${admin_role_list}" var="role">
	    		 <tr>
		            <td>${role.m.id}</td>
		            <td>${role.m.role_name}</td>
		            <td>
		            	<c:if test="${role.m.is_effective == 0}"><span style="color: red">无效</span></c:if>
		            	<c:if test="${role.m.is_effective == 1}"><span style="color: green">有效</span></c:if>
		            </td>
		            <td>
		                  <a href="<%=__ROOT_PATH__%>/admin/admin/edit_role.html?role_id=${role.m.id}">编辑</a>
		                  
		            </td>
	        	</tr>
	    	</c:forEach>
		    
	   </table>
	   <div style="float:right;">
			${pageDiv}
			${tips}
		</div>
	</body>
</html>
<script>
$(function () {
	$('#addnewrole').click(function(){
			window.location.href="<%=__ROOT_PATH__%>/admin/admin/goto_add_role_detail_page.html";
	 });
});

function del(id){
	if(confirm("确定要删除吗？")){
		var url = "index.html";	
		window.location.href=url;		
	}
}
</script>