<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		 <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
	    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
	    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script> 

	    <style type="text/css">
	        body {
	            padding-bottom: 40px;
	        }
	        .sidebar-nav {
	            padding: 9px 0;
	        }
	
	        @media (max-width: 980px) {
	            /* Enable use of floated navbar text */
	            .navbar-text.pull-right {
	                float: none;
	                padding-left: 5px;
	                padding-right: 5px;
	            }
	        }
	
	
	    </style>
	</head>

	<body>
		<form action="<%=__ROOT_PATH__ %>/admin/admin/employee_detail_save_or_update.html" method="post" class="definewidth m20" onsubmit="return checkInfo();">
			<input type="hidden" name="id" value="${adminM.m.id}"/>
			<table class="table table-bordered table-hover ">
				<tr>
			        <td class="tableleft">真实姓名：</td>
			        <td ><input type="text" name="real_name" id="real_name" value="${adminM.m.real_name}" class="form-control" required="required"/></td>
			    </tr> 
				<tr>
			        <td width="10%" class="tableleft">员工编号：</td>
			        <td><input type="text" name="emplyee_no" id="emplyee_no" value="${adminM.m.emplyee_no}" class="form-control" required="required"/></td>
			    </tr>
			    <tr>
			        <td width="10%" class="tableleft">员工登陆账号：</td>
			        <td><input type="text" name="login_account" id="login_account" value="${adminM.m.login_account}" class="form-control" required="required"/></td>
			    </tr>
			    <tr>
			        <td class="tableleft">登陆密码：</td>
			        <td ><input type="password" name="login_pwd" id="login_pwd" value="" class="form-control" required="required"/></td>
			    </tr> 
			     <tr>
			        <td class="tableleft">操作密码：</td>
			        <td ><input type="password" name="operation_code" id="operation_code" value="" class="form-control" required="required"/></td>
			    </tr> 
			    <tr>
			        <td class="tableleft">邮箱：</td>
			        <td ><input type="text" name="email" id="email" value="${adminM.m.email}"/></td>
			    </tr> 
			    <tr>
			        <td class="tableleft">所属部门</td>
			        <td >
			        	<select name="dept_id" id="dept_id" required="required">
			        			<option value="">--请选择部门--</option>
			        		<c:forEach items="${adminDeptMList}" var="dept">
			        			<c:if test="${adminM.m.dept_id == dept.m.dept_id}">
			        				<option value="${dept.m.dept_id}" selected="selected">${dept.m.dept_name}</option>
			        			</c:if>
			        			<c:if test="${adminM.m.dept_id != dept.m.dept_id}">
			        				<option value="${dept.m.dept_id}" >${dept.m.dept_name}</option>
			        			</c:if>
			        		</c:forEach>
			        		
			        	</select>
			        </td>
			    </tr> 
			    <tr>
			        <td class="tableleft">所属角色</td>
			        <td >
			        	<select name="role_id" id="role_id" required="required">
			        			<option value="">--请选择角色--</option>
			        		<c:forEach items="${admin_role_list}" var="role">
				        		<c:if test="${adminM.m.role_id == role.m.id}">
				        			<option value="${role.m.id}" selected="selected">${role.m.role_name}</option>
				        		</c:if>
				        		<c:if test="${adminM.m.role_id != role.m.id}">
				        			<option value="${role.m.id}">${role.m.role_name}</option>
				        		</c:if>
			        		</c:forEach>
			        		
			        	</select>
			        </td>
			    </tr>   
			    <tr>
			        <td class="tableleft">状态</td>
			        <td >
			        		<c:if test="${adminM.m.is_effective == 0}">
			        			<input type="radio" name="status" value="1" /> 启用
			           			<input type="radio" name="status" value="0" checked="checked"/> 禁用
			        		</c:if>
			        		<c:if test="${adminM.m.is_effective == 1}">
			        			<input type="radio" name="status" value="1" checked="checked"/> 启用
			           			<input type="radio" name="status" value="0" /> 禁用
			        		</c:if>
			        		
			        </td>
			    </tr>
			    <tr>
			        <td class="tableleft"></td>
			        <td>
			            <button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			        </td>
			    </tr>
			</table>
		</form>
	</body>
</html>
<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="<%=__ROOT_PATH__%>/admin/admin/employee_list_index.html";
		 });

    });

     function checkInfo(){
        // alert(1);
         var dept_no = $("#dept_no").val;
         var dept_name = $("#dept_name").val;
         var parent_dept_id = $("#parent_dept_id").val;
    	 var status = $("input[@type=radio][@checked]").val();  
    	 alert(dept_no);
    	 alert(dept_name);
    	 alert(parent_dept_id);
    	 alert(status);
    	 return false;
    	 /**
		 if(dept_no == "" || dept_no == null){
			alert("部门编号为空!");
			return false;
		 }else  if(dept_name == "" || dept_name == null){
			alert("部门名称为空!");
			return false;
		 }else  if(parent_dept_id == 0 || parent_dept_id == null){
			alert("请选择父级部门!");
			return false;
		 }else{
			 alert(5);
			return true;
		 }
    	 **/
     }
</script>