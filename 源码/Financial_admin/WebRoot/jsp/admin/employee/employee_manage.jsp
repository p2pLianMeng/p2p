<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/bootstrap-responsive.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/style.css" />
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/bootstrap.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/ckform.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/common.js"></script>

		<style type="text/css">
			body {
				padding-bottom: 40px;
			}
			
			.sidebar-nav {
				padding: 9px 0;
			}
			
			@media ( max-width : 980px) { /* Enable use of floated navbar text */
				.navbar-text.pull-right {
					float: none;
					padding-left: 5px;
					padding-right: 5px;
				}
			}
		</style>
	</head>

	<body>
		<form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/admin/admin/query_employee_info.html"
			method="post">
			员工姓名：
			<input type="text" name="real_name" id="real_name" class="abc input-default" placeholder="" value="">
			员工编号：
			<input type="text" name="emplyee_no" id="emplyee_no" class="abc input-default" placeholder="" value="">
				&nbsp;&nbsp;
				<button type="submit" class="btn btn-primary">
					查询
				</button>
				&nbsp;&nbsp;
				<button type="button" class="btn btn-success" id="addnew" >
					新增员工
				</button>
		</form>
		<table class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						员工编号
					</th>
					<th>
						真实姓名
					</th>
					<th>
						邮箱
					</th>
					<th>
						所属部门
					</th>
					<th>
						所属角色
					</th>
					<th>
						状态
					</th>
					<th>
						管理操作
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${admin_record_list}" var="record">
					<tr>
						<td>
							${record.r.emplyee_no}
						</td>
						<td>
							${record.r.real_name}
						</td>
						<td>
							${record.r.email}
						</td>
						<td>
							${record.r.dept_name}
						</td>
						<td>
							${record.r.role_name}
						</td>
						<td>
							<c:if test="${record.r.is_effective == 0}"><span style="color: red">无效</span></c:if>
							<c:if test="${record.r.is_effective == 1}"><span style="color: green">有效</span></c:if>
						</td>
						<td>
							<a href="<%=__ROOT_PATH__%>/admin/admin/employee_detail_info.html?admin_id=${record.r.id}">编辑</a>
		
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="inline pull-right page">
			10122 条记录 1/507 页
			<a href='#'>下一页</a>
			<span class='current'>1</span><a href='#'>2</a><a
				href='/chinapost/index.php?m=Label&a=index&p=3'>3</a><a href='#'>4</a><a
				href='#'>5</a>
			<a href='#'>下5页</a>
			<a href='#'>最后一页</a>
		</div>
	</body>
</html>
<script>
    $(function () {
		$('#addnew').click(function(){
				window.location.href="<%=__ROOT_PATH__%>/admin/admin/goto_add_employee_detail_page.html";
		 });
    });

	function del(id){
		if(confirm("确定要删除吗？")){
			var url = "index.html";
			window.location.href=url;	
		}
	}
	
	
</script>