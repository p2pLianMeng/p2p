<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>
	 	
		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
			<!--
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
			 -->
		<!-- 
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		 
		<script type="text/javascript" src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
		
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery-1.6.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/SimpleTree.js"></script>-->
		
		<script type="text/javascript">
		/**
		$(function(){
				$(".st_tree").SimpleTree({
					click:function(a){
					//	if(!$(a).attr("hasChild"))
							//alert($(a).attr("ref"));
					}
				});
			});
		**/
			//更改选中权限
			
			function changeSelected(id){
				//alert(id);
				var array = getClass(id);
				//alert(array.length);
				for(var i = 0 ;i<array.length;i++){
					//alert(array[i].checked);
					if(array[i].checked){
						array[i].checked = "";
					}else{
						array[i].checked = "checked";
					}
				}
			}

			function getClass(attr){  
				 var aArray=[];//定义一个新的空数组   
				 var i=0;  
				 var aAll = document.getElementsByTagName('input');//获取obj对象下面所有的节点   
				  for(i=0;i<aAll.length;i++){  
				     if(aAll[i].className == attr){//判断当前对象的class名称是不是符合传进来的参数   
				         aArray.push(aAll[i]);//如果符合则push到到aArray数组里边   
				      }  
				   }  
				  return aArray;//最后return一下   
			}  
		</script>
		
	</head>
  <body>
    	<div class="container" style="margin-top: 12px;">
    		<form action="<%=__ROOT_PATH__%>/admin/admin/saveOrUpdatePower.html?admin_id=${administrator_id}" method="post">
    			<div class="st_tree">
    				<ul>
			    		<c:forEach items="${AllAdminPowerMlist}" var="adminPower">
			    			<c:if test="${adminPower.m.power_grade == 1}">
			    				<li>
			    					<a href="#" >
			    						<input type="checkbox" onclick="changeSelected(this.id);"  name="power_name" id="${adminPower.m.power_id}" value="${adminPower.m.power_id}"  />${adminPower.m.power_name} <br/>
			    					</a>
			    				</li>
			    			</c:if>
			    			
				    			<c:if test="${adminPower.m.power_grade == 2}">
				    				<ul show="true">
				    					<li>
				    						<a href="#" >
				    							<input type="checkbox" name="power_name" id="${adminPower.m.power_id}" class="${adminPower.m.power_parent_id}" value="${adminPower.m.power_id}"  />${adminPower.m.power_name} <br/>
				    						</a>
				    					</li>
				    				</ul>
				    			</c:if>
			    			
			    			
			    		</c:forEach>
			    		<input type="submit" value="保存"/>
		    		</ul>
		    	</div>
    		</form>
    	</div>
  </body>
  <script >
  //alert(1);
  
  	var arrs = document.getElementsByName("power_name");

  	var arrays = ${adminIdList};
  	for(var i = 0;i<arrs.length;i++){
			//alert(arrs[i].value);
			var checkboxvalue = arrs[i].value;
			for(var j = 0 ;j<arrays.length;j++){
				if(checkboxvalue == arrays[j]){
					arrs[i].checked = "checked";
				}
			}
  	  }
  </script>
</html>
