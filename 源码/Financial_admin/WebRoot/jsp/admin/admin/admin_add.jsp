<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>

		<link type="text/css" href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link type="text/css" href="<%=__PUBLIC__%>/css/easydialog.css" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
		
	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
		<form action="<%=__ROOT_PATH__ %>/admin/admin/admin_add.html" method="post" name="form1">
			<table class="table table-bordered table-hover table-condensed"
				id="table">
				<legend>
					添加管理员
				</legend>
		
				<tr>
					<td>
						用&nbsp;&nbsp;户&nbsp;&nbsp;名:
					</td>
					<td>
						<input type="text" name="admin_name" id="admin_name" class="form-control" required="required"/>
					</td>
				</tr>
				<tr>
					<td>
						密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码:
					</td>
					<td>
						<input type="password" name="pwd" id="pwd" class="form-control" required="required" />
						<span id="pwd_start" style="color:red"></span>
					</td>
				</tr>
				<tr>
					<td>
						确认密码:
					</td>
					<td>
						<input type="password" name="pwd_confirm" id="pwd_confirm" class="form-control" required="required" />
					</td>
					<span id="pwd_info" style="color:red"></span>
				</tr>
				<tr>
					<td>
						邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱:
					</td>
					<td>
						<input type="text" name="email" id="email" class="form-control"  required="required"/>
					</td>
				</tr>
				<tr>
					<td>
						Q&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q:
					</td>
					<td id="QQ">
						<input type="text" name="QQ" id="QQ" class="form-control" />
					</td>
				</tr>
				<tr>
						<td>
							操作密码:
						</td>
						<td>
							<input type="password" name="operation_code" class="form-control" id="operation_code" required="required" />
						</td>
					</tr>
				<tr>
					<td>
						权&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;限:
					</td>
					<td id="agency_id" >
					是否超级管理员：
					<select name="agency_id">
						<c:choose>
							<c:when test="${agency_id=='0'}">
								<option value="0" selected="selected">
									是
								</option>
								</c:when>
								<c:otherwise>
									<option value="0">
										是
									</option>
								</c:otherwise>
									</c:choose>

									<c:choose>
								<c:when test="${agency_id=='1'}">
									<option value="1" selected="selected">
										不是
									</option>
								</c:when>
									<c:otherwise>
										<option value="1">
											不是
										</option>
									</c:otherwise>
								</c:choose>
							</select>
					</td>
				</tr>
				<!-- <tr>
					<td>
						金额
					</td>
					<td id="">
						<input type="number" name="money" id="money" min="1"
							max="1000000000000" class="form-control" required="required" />
					</td>
				</tr> -->
				<tr>
					<td>
						
					</td>
					<td>
						<input type="submit" id="add" class="btn btn-default" value="增加" style="margin-right: 480px;" onclick="return doAddInfo()" />
						<input type="button" value="刷新页面" id="msg_show"
							class="btn btn-default" onclick="f5();" />
					</td>
				</tr>
				</tbody>
			</table>
			</form>
		</div>
	</body>
	<script type="text/javascript">
	/*
	* method:showMssage
    * param:message 
	* explain:显示对话信息
	*/
	function showMssage(message)
	{
		var btnFn = function(){
			  easyDialog.close();
			  };
	   var btnFs = function(){
		   window.location.reload();
	   };
	   /**
	   var btnTz = function(){
		   
		   	easyDialog.close();
		  
		   	window.location.href(message[3]);
	   }**/
		if(message[2]!=null){
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFs,
					 noFn:false
					}
			});	
			//实名认证成功以后 跳向安全设置页面
		}else{
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFn,
					 noFn:false
					}
			});	
		}
	}
			
	
		$(function(){
			$("#pwd_confirm").blur(function(){
				$("#pwd_info").text("");
				if($("#pwd").val() != $("#pwd_confirm").val()){
					$("#pwd_info").text("两次输入的密码不一致");
					return false;
				}
			});
		});
		
		function doAddInfo(){
			//alert($("#admin_name").val());
			if($("#admin_name").val() ==null || $("#admin_name").val() == ""){
				//alert("请设置用户名，用户名为登录名！");
				showMssage(["提示","请设置用户名，用户名为登录名！"]);
				return false;
			}
			if($("#pwd").val() ==null || $("#pwd").val() == ""){
					//$("#pwd_start").text("请设置新密码！");
					//alert("请设置新密码！");
					showMssage(["提示","请设置新密码！"]);
					return false;
			}

			if($("#pwd").val() != $("#pwd_confirm").val()){
				//$("#pwd_info").text("两次输入的密码不一致");
				showMssage(["提示","两次输入的密码不一致！"]);
				return false;
			}
			
			if($("#email").val() ==null || $("#email").val() == ""){
				//alert("请设置邮箱，邮箱是唯一的！");
				showMssage(["提示","请设置邮箱，邮箱是唯一的！"]);
				return false;
			}
			
			if($("#operation_code").val() ==null || $("#operation_code").val() == ""){
				//$("#pwd_start").text("请设置新密码！");
				//alert("请设置新密码！");
				showMssage(["提示","请设置授权密码！"]);
				return false;
		}
			
		}
	</script>
</html>
