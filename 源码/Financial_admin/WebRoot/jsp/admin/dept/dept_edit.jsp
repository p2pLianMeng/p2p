<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		 <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
	    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
	    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
	    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script> 

	    <style type="text/css">
	        body {
	            padding-bottom: 40px;
	        }
	        .sidebar-nav {
	            padding: 9px 0;
	        }
	
	        @media (max-width: 980px) {
	            /* Enable use of floated navbar text */
	            .navbar-text.pull-right {
	                float: none;
	                padding-left: 5px;
	                padding-right: 5px;
	            }
	        }
	
	
	    </style>
	</head>

	<body>
		<form action="<%=__ROOT_PATH__ %>/admin/admin/dept_detail_save_or_update.html" method="post" class="definewidth m20">
			<input type="hidden" name="dept_id" value="${admin_dept.m.dept_id}" />
			<table class="table table-bordered table-hover ">
			    <tr>
			        <td width="10%" class="tableleft">部门号</td>
			        <td><input type="text" name="dept_no" value="${admin_dept.m.dept_no}" required="required"/></td>
			    </tr>
			    <tr>
			        <td class="tableleft">部门名称</td>
			        <td ><input type="text" name="dept_name" value="${admin_dept.m.dept_name}" required="required"/></td>
			    </tr> 
			    <tr>
			        <td class="tableleft">父级部门</td>
			        <td >
			        	<select name="parent_dept_id" id="parent_dept_id" required="required">
			        		<option value="">---选择部门---</option>
			        		<c:forEach items="${all_parent_dept_list}" var="parentdept">
			        			<c:if test="${admin_dept.m.parent_dept_id == parentdept.m.dept_id}">
			        				<option value="${parentdept.m.dept_id}" selected="selected">${parentdept.m.dept_name}</option>
			        			</c:if>
			        			<c:if test="${admin_dept.m.parent_dept_id != parentdept.m.dept_id}">
			        				<option value="${parentdept.m.dept_id}">${parentdept.m.dept_name}</option>
			        			</c:if>
			        		</c:forEach>
			        		
			        	</select>
			        </td>
			    </tr> 
			    <tr>
			        <td class="tableleft">部门描述</td>
			        <td ><input type="text" name="dept_desc" value="${admin_dept.m.dept_desc}"/></td>
			    </tr>   
			    <tr>
			        <td class="tableleft">状态</td>
			        <td >
			        	<c:if test="${admin_dept.m.is_effective == 0}">
			        		<input type="radio" name="status" value="1" /> 启用
			           		<input type="radio" name="status" value="0" checked="checked"/> 禁用
			        	</c:if>
			            <c:if test="${admin_dept.m.is_effective == 1}">
			        		<input type="radio" name="status" value="1" checked="checked"/> 启用
			           		<input type="radio" name="status" value="0" /> 禁用
			        	</c:if>
			        </td>
			    </tr>
			    <tr>
			        <td class="tableleft"></td>
			        <td>
			            <button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			        </td>
			    </tr>
			</table>
		</form>
	</body>
</html>
<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="<%=__ROOT_PATH__%>/admin/admin/dept_list_index.html";
		 });

    });
</script>