<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>人民币代码生成界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>

		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		
		<div class="container" style="margin-top: 12px;">
			<a href="<%=__ROOT_PATH__%>/admin/sys_friendship_link/add_index.html">添加友情链接</a>
			<legend>友情链接</legend>${tips}
			<table class="table table-bordered table-hover table-condensed"
				id="table">
				<thead>
					<tr>
					    <th>
							#
						</th>
						<th>
							Name
						</th>
						<th>
							URL
						</th>
						<th>
						         修改
						</th>
						<th>
						         删除
						</th>
						
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${sys_friendship_link_list}" var="sys_friendship_link">
						<tr>
							 <td>${sys_friendship_link.m.id}</td>
							 <td>${sys_friendship_link.m.name}</td>
							 <td>${sys_friendship_link.m.url}</td>
							 <td><a href="<%=__ROOT_PATH__%>/admin/sys_friendship_link/update_index.html?id=${sys_friendship_link.m.id}">修改</a></td>
							 <td><a href="<%=__ROOT_PATH__%>/admin/sys_friendship_link/delete.html?id=${sys_friendship_link.m.id}">删除</a></td>
						</tr>

					</c:forEach>
				</tbody>
			</table>
		</div>	
	</body>
</htrl>
