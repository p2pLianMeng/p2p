<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员——修改文章界面</title>
		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>

	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
			<form action="<%=__ROOT_PATH__%>/admin/article/article_update.html" method="post" name="form1">
				<table>
					<tr>
						<td style="text-align:right;">文章标题:</td>
						<td><input type="text" name="title" id="title" value="${title}" class="form-control" required="required" style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">分类:</td>
						<td><input type="text" name="cat_id" id="cat_id" value="${cat_id}" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">文章作者:</td>
						<td><input type="text" name="author" id="author" value="${author}" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">文章作者的 email：</td>
						<td><input type="text" name="author_email" id="author_email" value="${author_email}" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">关键字：</td>
						<td><input type="text" name="keywords" id="keywords" value="${keywords}" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">文章类型:</td>
						<td><input type="text" name="article_type" id="article_type" value="${article_type}" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">文章描述:</td>
						<td><input type="text" name="description" id="description" value="${description}" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
				</table>
				文章内容:<input type="text" name="content" id="content" value="${content}" class="form-control" required="required" style=" width:800px; height:500px; margin-bottom:10px;"  /><br/>
				<input type="submit" id="update" class="btn btn-default" value="提交" style="margin-right: 30px;" />
			</form>
		</div>
	</body>
</html>
