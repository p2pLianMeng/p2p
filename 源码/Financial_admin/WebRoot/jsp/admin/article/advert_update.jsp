<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>人民币代码生成界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<link type="text/css" href="<%=__PUBLIC__%>/css/easydialog.css" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/Calendar2.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
	</head>
	
	<script>
	/*
	* method:showMssage
    * param:message 
	* explain:显示对话信息
	*/
	function showMssage(message)
	{
		var btnFn = function(){
			  easyDialog.close();
			  };
	   var btnFs = function(){
		   window.location.reload();
	   };
	   /**
	   var btnTz = function(){
		   
		   	easyDialog.close();
		  
		   	window.location.href(message[3]);
	   }**/
		if(message[2]!=null){
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFs,
					 noFn:false
					}
			});	
			//实名认证成功以后 跳向安全设置页面
		}else{
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFn,
					 noFn:false
					}
			});	
		}
	}
	
		function checksubmit(){
			var name = $("#name").val();//广告商名称
			var img_url = $("#img_url").val();//网络图片地址
			var advertiser_url = $("#advertiser_url").val();//广告商连接
			var advert_id = $("#advert_id").val();//广告位ID影藏的
			if(name == null || name == ""){
				showMssage(["提示","广告商名称不为空！"]);
				return false;
			}
			if(img_url == null || img_url == ""){
				showMssage(["提示","网络图片地址不为空！"]);
				return false;
			}
			
			
			//return true;
			//alert("进入保存数据");
			$.post(
					'<%=__ROOT_PATH__%>/admin/news/article/save_advert.html',
					{
						name : name,
						img_url : img_url,
						advertiser_url : advertiser_url,
						advert_id :advert_id
					},
					function(data) //回传函数
					{
						if (data == 0) {
							
							showMssage(["提示","添加广告位失败！"]);
						}else if (data == 1) {
							
							showMssage(["提示","添加广告位成功！"]);
							//window.location.href('<%=__ROOT_PATH__%>/admin/user_integral/gotoAddUserIntegralPage.html');
						}else if (data == 2) {
							
							showMssage(["提示","广告位修改成功！"]);
						}else if (data == 3) {
							
							showMssage(["提示","广告位修改失败！"]);
						}else{
							
							showMssage(["提示","系统错误！"]);
						}
					}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
	
			
		}
	</script>
	

	<body>
		<div class="container" style="margin-top: 12px;">
		<form action="" method="post">
		<table class="table table-bordered table-hover table-condensed"
			id="table">
			<legend>添加广告位</legend>
			<tr>
				<td>广告商名称：</td>
				<td>
					<input type="text" name="name" id="name" class="form-control" value="${advertM.m.name}" required="required"/>
				</td>
			</tr>
			<tr>
				<td>网络图片地址：</td>
				<td>
					<input type="text" name="img_url" id="img_url" class="form-control" value="${advertM.m.img_url}" required="required"/>
				</td>
			</tr>
			<tr>
				<td>广告商连接：</td>
				<td>
					<input type="text" name="advertiser_url" id="advertiser_url" class="form-control" value="${advertM.m.advertiser_url}" required="required"/>
				</td>
			</tr>
			<tr>
				<td><input type="hidden" name="advert_id" id="advert_id" value="${advertM.m.id}" /></td>
				<td id=""><input type="button" value="确定" onclick="checksubmit();" /></td>
			</tr>



		</table>
		
		</form>
	</div>







	</body>
</html>
