<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
			<legend>增加广告</legend>&nbsp;&nbsp;
			<a href="<%=__ROOT_PATH__%>/admin/news/article/goto_add_advert.html">添加广告位</a>
		</div>
		<br /><br /><!-- 进行查询 -->
		<div class="container" style="margin-top: 12px;">
			<legend>
				广告位列表
			</legend>
			<p>${tips}</p>
			<!-- 
			<form method="post"
				action="<%=__ROOT_PATH__%>/admin/news/article/article_list_query.html"
				class="form-inline">
				<table class="table table-bordered table-hover table-condensed"
					id="table">
					<tbody>
						<tr>
							<td>
								广告商名称：
								<input type="text" id="" name="name" class="form-control" value="${name}"/>
							</td>
							<td>
								网络图片地址：
								<input type="text" id="" name="key" class="form-control" value="${key}" />
							</td>
							<td>
								<input type="submit" value="查询" class="form-control" />
							</td>
						</tr>
					</tbody>
				</table>
			</form> --><!--
			${pageDiv}
			--><table class="table table-bordered table-hover table-condensed"
				id="table">
				<thead>
					<tr>
						<th>
							广告商名称
						</th>
						<th>
							网络图片地址
						</th>
						<th>
							广告连接
						</th>
						<th>
							添加时间
						</th>
						<th>
							操作
						</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${adveretMList}" var="advert">
						<tr>
							<td>
								${advert.m.name}
							</td>
							<td>
								${advert.m.img_url}
							</td>
							<td>
								${advert.m.advertiser_url}
							</td>
							<td>
								${advert.m.add_time}
							</td>
							<td>
								<a href="<%=__ROOT_PATH__%>/admin/news/article/goto_update_advert.html?advert_id=${advert.m.id}" style="margin-right: 10px;">修改</a>
								<a href="<%=__ROOT_PATH__%>/admin/news/article/delete_advert.html?advert_id=${advert.m.id}" onclick="return validate();">删除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<!--<span style="font-size:14px; display:block;text-align:center">${tips}</span>
			--><div style="float:right;">${pageDiv}</div>
			
		</div>
		<script type="text/javascript">
			function validate(){
				if(confirm("你确定要删除广告位!")){
					return true;
				}else{
					return false;
				}
			}
		</script>
	</body>
	
</html>
