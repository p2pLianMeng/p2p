<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员——文章分类列表界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>

	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
			<form action="<%=__ROOT_PATH__ %>/admin/news/type/article_type_add.html"
				method="post" name="form1">
				<table class="table table-bordered table-hover table-condensed"
					id="table">
					<legend>
						添加文章分类 
					</legend>
					 ${tips}
					<form action="<%=__ROOT_PATH__%>/admin/news/type/article_type_index.html" method="post">
					<tr>
						<td>
							分类名:
						</td>
						<td id="cat_name">
							<input type="text" name="name" id="cat_name" class="form-control" required="required" />
						</td>
						<td>
							<input type="submit" value="新建新闻类别" />
						</td>
					</tr>
					
					</form>
					
					</tbody>
				</table>
			</form>
			<table class="table table-bordered table-hover table-condensed">
				<tr>
					<td>名称</td>
					<td>类型</td>
					<td>操作</td>
				</tr>
				<c:forEach items="${article_typeList}" var="article_type">
				<form method="post" action="<%=__ROOT_PATH__%>/admin/news/type/article_type_update.html" >
				<tr>
					
					<td><input type="text" required="required" name="name" id="name" value="${article_type.m.name}" class="form-control"/></td>
					<td >
						<input type="hidden"  name="id" value="${article_type.m.id}"/>
						<input type="submit" value="修改" />
					</td>
					<td >	
						<a href="<%=__ROOT_PATH__ %>/admin/news/type/article_type_delete.html?id=${article_type.m.id}" style=" margin-right:30px;" >删除</a><!-- 删除下面的所有的文章 -->
					</td>
				</tr>
				
				</form>
				
				</c:forEach>
			</table>
		</div>
	</body>
</html>
