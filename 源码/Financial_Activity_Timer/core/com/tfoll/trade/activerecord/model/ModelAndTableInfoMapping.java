package com.tfoll.trade.activerecord.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 保存模型和表之间的映射关系
 */
public class ModelAndTableInfoMapping {

	private static ModelAndTableInfoMapping instance = new ModelAndTableInfoMapping();

	public static final Map<Class<? extends Model<?>>, TableInfo> modelAndTableInfoMap = new HashMap<Class<? extends Model<?>>, TableInfo>();

	public static ModelAndTableInfoMapping getInstance() {
		return instance;
	}

	private ModelAndTableInfoMapping() {
	}

	public void addModelAndTableInfoMapping(Class<? extends Model<?>> modelClass, TableInfo tableInfo) {
		if (modelAndTableInfoMap.containsKey(modelClass)) {
			throw new IllegalArgumentException("modelClass:" + modelClass + "重复");
		}
		modelAndTableInfoMap.put(modelClass, tableInfo);
	}

	@SuppressWarnings("unchecked")
	public TableInfo getTableInfo(Class<? extends Model> modelClass) {
		if (modelAndTableInfoMap == null) {
			throw new NullPointerException("modelAndTableInfoMap为空");
		}
		TableInfo result = modelAndTableInfoMap.get(modelClass);
		if (result == null) {
			throw new NullPointerException("ModelAndTableInfoMap 中不存在 模型:" + modelClass.getName() + " 与之对应的关系映射");
		}
		return result;
	}

}
