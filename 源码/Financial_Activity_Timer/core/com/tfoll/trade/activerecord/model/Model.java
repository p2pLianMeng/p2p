package com.tfoll.trade.activerecord.model;

import com.tfoll.trade.activerecord.ActiveRecordException;
import com.tfoll.trade.activerecord.DataSourceKit;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.config.Constants;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

/**
 * Model和TableInfo组合,建议使用注解配置 <br/>
 * 单表不存在事物
 */
@SuppressWarnings("unchecked")
public abstract class Model<M extends Model<?>> {
	public static final SimpleDateFormat Date = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat Time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	public static final SimpleDateFormat $Date = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat $Time = new SimpleDateFormat("yyyyMMddhhmmss");
	public static final SimpleDateFormat _Date = new SimpleDateFormat("yyyy年MM月dd日");
	public static final SimpleDateFormat _Time = new SimpleDateFormat("yyyy年MM月dd日hh时mm分ss秒");

	public static final long Second = 1000;
	public static final long Minute = Second * 60;
	public static final long Hour = Minute * 60;
	public static final long Day = Hour * 24;

	static Logger logger = Logger.getLogger(Model.class);
	private static final ModelAndTableInfoMapping modelAndTableInfoMapping = ModelAndTableInfoMapping.getInstance();
	private static Object[] o = new Object[0];
	/**
	 * 实例变量Map--通过get方法被页面访问
	 */
	private Map<String, Object> M = new HashMap<String, Object>();// 修改底层的时候不能进行修改-否则导致外面去数据的时候有问题

	/**
	 * 修改过的实例变量的Set
	 */
	private Set<String> modifiedSet = new HashSet<String>();

	/**
	 * 只能同包访问这个方法-防止使用的人调用此方法造成更新数据的时候会出现问题
	 */
	void clearModifiedSet() {
		modifiedSet.clear();
	}

	/**
	 * 检查sql是否有Model与之对应的表名
	 */
	private void checkTableName(Class<? extends Model> modelClass, String sql) {
		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(modelClass);
		if (!sql.toLowerCase().contains(tableInfo.getTableName().toLowerCase())) {
			throw new ActiveRecordException("sql[" + sql + "]语句中不包含表名:" + tableInfo.getTableName());
		}
	}

	public boolean delete() {
		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(getClass());
		String primaryKey = tableInfo.getPrimaryKey();
		Object id = M.get(primaryKey);
		if (id == null) {
			throw new ActiveRecordException("主键不存在");
		}
		return deleteById(tableInfo, id);
	}

	// 删除Model.属性
	public Model<M> clear() {
		M.clear();
		getModifiedAttributeFlagSet().clear();
		return this;
	}

	public boolean deleteById(Object id) {
		if (id == null) {
			throw new NullPointerException("主键不存在");
		}
		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(getClass());
		return deleteById(tableInfo, id);
	}

	private boolean deleteById(TableInfo tableInfo, Object id) {
		String sql = DataSourceKit.getDialect().deleteByIdForModel(tableInfo);
		return Db.update(sql, id) >= 1;
	}

	// 查询
	private List<M> find(Connection conn, String sql, Object... params) throws Exception {
		if (Constants.devMode) {
			logger.debug("sql:" + sql);
		}
		Class<? extends Model> modelClass = getClass();
		if (Constants.devMode) {

			// 有可能在运行阶段的SQL语句不是操作这张表
			checkTableName(modelClass, sql);
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		int length = params.length;
		for (int i = 0; i < length; i++) {
			ps.setObject(i + 1, params[i]);
		}

		/**
		 * 结果集永远不为空:底层方法实现List<T> result = new ArrayList<T>();
		 */
		ResultSet rs = ps.executeQuery();
		List<M> result = ModelBuilder.build(rs, modelClass);
		DataSourceKit.close(rs, ps);

		return result;
	}

	public List<M> find(String sql) {
		return find(sql, o);
	}

	public List<M> find(String sql, Object... params) {
		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();
			return find(conn, sql, params);
		} catch (Exception e) {
			throw new ActiveRecordException(e);
		} finally {
			DataSourceKit.close(conn);
		}
	}

	public M findById(Object id) {
		return findById(id, "*");
	}

	// 根据主键查询
	/**
	 * <pre>
	 * 根据ID主键查询出columns中字段的数据信息 Example: User user = User.dao.findById(15, "name, age");
	 * 
	 * @param id  主键
	 * @param columns  内部用","分割
	 * </pre>
	 */
	public M findById(Object id, String columns) {
		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(getClass());
		String sql = DataSourceKit.getDialect().findByIdForModel(tableInfo, columns);
		List<M> list = find(sql, id);
		return list.size() > 0 ? list.get(0) : null;
	}

	public M findFirst(String sql) {
		List<M> result = find(sql, o);
		if (result == null) {
			return null;
		} else {
			return result.size() > 0 ? result.get(0) : null;
		}
	}

	/**
	 * 得到结果集第一条。建议在SQL语句中添加 "limit 1".
	 */
	public M findFirst(String sql, Object... params) {
		List<M> result = find(sql, params);
		return result.size() > 0 ? result.get(0) : null;
	}

	public <T> T get(String key) {
		return (T) M.get(key);
	}

	public <T> T get(String key, Object defaultValue) {
		Object result = M.get(key);
		return (T) (result != null ? result : defaultValue);
	}

	/**
	 * getMap映射-可以扩容-Keys-但是不能更新数据
	 */
	// 页面el表达式读取属性
	// 注意: 只能读取不能修改
	public Map<String, Object> getM() {
		return new HashMap<String, Object>(M);
	}

	public Set<Entry<String, Object>> getAttrsEntrySet() {
		return M.entrySet();
	}

	/**
	 * 处理tinyint(1)
	 */
	public Boolean getBoolean(String key) {
		return (Boolean) M.get(key);
	}

	/**
	 * 处理BLob
	 */
	public byte[] getBytes(String key) {
		return (byte[]) M.get(key);
	}

	/**
	 * @param filePath
	 *            文件路径 根据byte数组，生成文件
	 */
	public static void bytesToFile(byte[] bytes, String filePath) {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;

		File file = new File(filePath);
		try {
			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			bos.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					throw new RuntimeException(e1);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					throw new RuntimeException(e1);
				}
			}
		}
	}

	public java.util.Date getDate(String key) {
		return (java.util.Date) M.get(key);
	}

	/**
	 * 专门处理Timestamp的秒数转为Date的处理方法
	 */
	public java.util.Date getSecondsIntToDate(String key) {
		int seconds = (Integer) M.get(key);
		return new java.util.Date(seconds * 1000);
	}

	public java.util.Date getMillisecondsIntToDate(String key) {
		int milliseconds = (Integer) M.get(key);
		return new java.util.Date(milliseconds);
	}

	/**
	 * 不建议使用这个方法-数据不精确
	 */

	@SuppressWarnings("unused")
	private Double getDouble(String key) {
		return (Double) M.get(key);
	}

	@SuppressWarnings("unused")
	private Float getFloat(String key) {
		return (Float) M.get(key);
	}

	public java.math.BigDecimal getBigDecimal(String key) {
		return (java.math.BigDecimal) M.get(key);
	}

	public Integer getInt(String key) {
		return (Integer) M.get(key);
	}

	public Long getLong(String key) {
		return (Long) M.get(key);
	}

	public BigInteger getBigInteger(String key) {
		return (BigInteger) M.get(key);
	}

	public String getString(String key) {
		return (String) M.get(key);
	}

	public java.sql.Time getTime(String key) {
		return (java.sql.Time) M.get(key);
	}

	public java.sql.Timestamp getTimestamp(String key) {
		return (java.sql.Timestamp) M.get(key);
	}

	/**
	 * 该方法主要针对不必加入修改过的实例变量的Set的属性进行设值
	 */
	public Model<M> put(String key, Object value) {
		M.put(key, value);
		return this;
	}

	/**
	 * 保存Model信息:注意保存信息后原来的信息马上清空
	 */
	public boolean save() {
		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(getClass());

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		DataSourceKit.getDialect().saveForModel(tableInfo, M, sql, params);
		if (Constants.devMode) {
			logger.debug("sql:" + sql);
		}
		Connection conn = null;
		PreparedStatement ps = null;
		int result = 0;
		try {
			conn = DataSourceKit.getConnection();
			boolean isSupportAutoIncrementKey = DataSourceKit.getDialect().isSupportAutoIncrementKey();

			if (isSupportAutoIncrementKey) {
				ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			} else {
				ps = conn.prepareStatement(sql.toString());
			}

			int size = params.size();
			for (int i = 0; i < size; i++) {
				ps.setObject(i + 1, params.get(i));
			}
			result = ps.executeUpdate();
			if (isSupportAutoIncrementKey) {
				getGeneratedKey(ps, tableInfo);
			}

			getModifiedAttributeFlagSet().clear();
			return result >= 1;
		} catch (Exception e) {
			throw new ActiveRecordException(e);
		} finally {
			DataSourceKit.close(ps, conn);
		}
	}

	private Set<String> getModifiedAttributeFlagSet() {
		return modifiedSet;
	}

	void getModifiedAttributeFlagSetAddKey(String key) {
		if (key != null && "".equals(key.trim())) {
			modifiedSet.add(key);
		}

	}

	private void getGeneratedKey(PreparedStatement ps, TableInfo tableInfo) throws SQLException {
		String primaryKey = tableInfo.getPrimaryKey();
		if (get(primaryKey) == null) {
			// 这句话会自动获取主键
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				Class colType = tableInfo.getColumnType(primaryKey);
				if (colType == Integer.class || colType == int.class) {
					set(primaryKey, rs.getInt(1));
				} else if (colType == Long.class || colType == long.class) {
					set(primaryKey, rs.getLong(1));
				} else {
					set(primaryKey, rs.getObject(1));
				}
				rs.close();
			}
		}
	}

	public Model<M> setAttributeMap(M model) {
		return setMap(model.getM());
	}

	public Model<M> setMap(Map<String, Object> attributeMap) {
		for (Entry<String, Object> entry : attributeMap.entrySet()) {
			set(entry.getKey(), entry.getValue());
		}
		return this;
	}

	/**
	 * if (modelAndTableInfoMapping.getTableInfo(getClass()).containsColumnName(
	 * key)) {} <br/>
	 * 理论上的方法名是setAttribute
	 * 
	 * @see #put(String, Object)
	 */
	// 常用的方法
	public Model<M> set(String key, Object value) {
		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(getClass());

		if (tableInfo.getColumnTypeMap().containsKey(key)) {
			M.put(key, value);
			if (!tableInfo.getPrimaryKey().equals(key)) {
				modifiedSet.add(key);
			}

		} else {
			throw new ActiveRecordException("参数key:" + key + "不存在");
		}
		return this;
	}

	// 更新Model.
	public boolean update() {
		if (getModifiedAttributeFlagSet().isEmpty()) {
			logger.debug("没有什么可以用于更新的字段");
			return true;
		}

		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(getClass());
		String primaryKey = tableInfo.getPrimaryKey();
		Object id = M.get(primaryKey);
		if (id == null) {
			throw new ActiveRecordException("执行更新操作时缺少主键值");
		}
		// 设置主键
		// set(primaryKey, id);//Ok
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		DataSourceKit.getDialect().updateForModel(tableInfo, M, getModifiedAttributeFlagSet(), primaryKey, id, sql, params);
		if (Constants.devMode) {
			logger.debug("sql:" + sql);
		}
		if (params.size() == 0) {
			logger.debug("没有什么可以用于更新的字段");
			return true;
		}

		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();

			int result = Db.update(conn, sql.toString(), params.toArray());
			getModifiedAttributeFlagSet().clear();
			if (result >= 1) {
				clearModifiedSet();
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			throw new ActiveRecordException(e);
		} finally {

			DataSourceKit.close(conn);
		}
	}

	/**
	 * 打印该表字段类型
	 */

	public void getAllColumnMapInfo() {
		TableInfo tableInfo = modelAndTableInfoMapping.getTableInfo(getClass());
		tableInfo.getAllColumnMapInfo();
	}

	/**
	 * <pre>
	 * 特殊的几个方法:该方法主要处理不是该table_info里面记录的字段存储
	 * </pre>
	 */
	/**
	 * 获取底层的map
	 */
	public Map<String, Object> getReferenceMap() {
		return M;
	}

	/**
	 * 修改底层的map
	 */
	public Model<M> updateReferenceMap(Map<String, Object> attributeMap) {
		this.M.putAll(attributeMap);
		return this;
	}

	/**
	 * 添加不是本Model中对应的table_info字段的key_value对
	 * 
	 * @param key
	 *            不是本Model中对应的table_info字段
	 * @param value
	 *            Object
	 */
	public Model<M> addOtherKey(String key, Object value) {
		M.put(key, value);
		return this;
	}

}
