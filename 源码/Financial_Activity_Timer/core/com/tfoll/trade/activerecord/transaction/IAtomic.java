package com.tfoll.trade.activerecord.transaction;

/**
 * Database transaction atomic:数据库事务原子
 */
public interface IAtomic {

	/**
	 * 返回真则提交事务,为假或者出现异常则回滚
	 */
	boolean transactionProcessing() throws Exception;
}