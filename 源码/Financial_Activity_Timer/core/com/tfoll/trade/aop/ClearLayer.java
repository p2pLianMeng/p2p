package com.tfoll.trade.aop;

/**
 * <pre>
 * ClearLayer即[添加了拦截器]对象清除施加在其身的拦截器层次:upper和all . JFly 拦截器针对的对象分为3种:Global,Controller and Action.
 * </pre>
 */
public enum ClearLayer {

	/**
	 * 清除施加在该对象前面所有的拦截器对象-自身定义的拦截器不会被清理掉
	 */
	Before,

	/**
	 * 清除施加在该对象所有的拦截器对象-自身定义的拦截器同样会被清理掉
	 */
	All;
}
