package com.tfoll.trade.plugin;

import com.googlecode.asyn4j.core.handler.CacheAsynWorkHandler;
import com.googlecode.asyn4j.core.handler.DefaultErrorAsynWorkHandler;
import com.googlecode.asyn4j.service.AsynService;
import com.googlecode.asyn4j.service.AsynServiceImpl;

import org.apache.log4j.Logger;

/**
 * 异步服务插件-现在只支持Windows文件系统
 * 
 */
public class AsynServicePlugin implements IPlugin {
	public static AsynService asynService = null;

	public AsynServicePlugin() {
	}

	private static Logger logger = Logger.getLogger(AsynServicePlugin.class);

	public boolean start() {
		/**
		 * 这个文件存在则删除c:\asynwork.data
		 */
		String path = "c:\\asynwork.data";
		// 初始化异步工作服务
		asynService = AsynServiceImpl.getService(300, 3000L, 100, 100, 1000);
		// 异步工作缓冲处理器
		asynService.setWorkQueueFullHandler(new CacheAsynWorkHandler(100));
		// 服务启动和关闭处理器
		// asynService.setServiceHandler(new FileAsynServiceHandler());
		// 异步工作执行异常处理器
		asynService.setErrorAsynWorkHandler(new DefaultErrorAsynWorkHandler());
		// 启动服务
		try {
			asynService.init();
		} catch (Exception e) {
			logger.error(e);
			logger.error("异步框架文件不存在" + path + "请先配置然后重启项目");
		}
		logger.info("异步服务加载成功");
		return true;
	}

	public boolean stop() {
		asynService.close();
		return true;
	}

}
