<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <link href="<%=__PUBLIC__%>/assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
   <link href="<%=__PUBLIC__%>/assets/css/bui-min.css" rel="stylesheet" type="text/css" />
   <link href="<%=__PUBLIC__%>/assets/css/main-min.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="header">
    
      <div class="dl-title">
       <!--<img src="/chinapost/Public/assets/img/top.png">-->
      </div>

    <div class="dl-log">欢迎您，<span class="dl-log-user">${sessionScope.admin.m.real_name}</span><a href="#" title="退出系统" class="dl-log-quit">[退出]</a>
    </div>
  </div>
   <div class="content">
    <div class="dl-main-nav">
      <div class="dl-inform"><div class="dl-inform-title"><s class="dl-inform-icon dl-up"></s></div></div>
      <ul id="J_Nav"  class="nav-list ks-clear">
      			<c:forEach items="${parentModuleList}" var="module">
      				<li class="nav-item dl-selected"><div class="nav-item-inner nav-home">${module.r.module_name}</div></li>
      			</c:forEach>
        		
        		    

      </ul>
    </div>
    <ul id="J_NavContent" class="dl-tab-conten">

    </ul>
   </div>
  <script type="text/javascript" src="<%=__PUBLIC__%>/assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="<%=__PUBLIC__%>/assets/js/bui-min.js"></script>
  <script type="text/javascript" src="<%=__PUBLIC__%>/assets/js/common/main-min.js"></script>
  <script type="text/javascript" src="<%=__PUBLIC__%>/assets/js/config-min.js"></script>
  <script>
    BUI.use('common/main',function(){
      var config = ${str};
      new PageUtil.MainPage({
        modulesConfig : config
      });
    });
  </script> 
</body>
</html>