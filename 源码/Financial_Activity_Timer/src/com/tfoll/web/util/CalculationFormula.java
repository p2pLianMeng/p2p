package com.tfoll.web.util;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.web.domain.RepaymentPlan;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 理财和借贷和债权转让公式
 * 
 */
public class CalculationFormula {

	public static final int Each_Share_Price = 50; // 初始每份（债权）价格

	/*
	 * 计算等额本息-构建还款计划
	 */
	/**
	 * 等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、
	 * 利息比重逐月递减。
	 * 
	 * @param loan_principal
	 *            借款本金
	 * @param month_rate
	 *            每月利率
	 * @param periods_number
	 *            还款总期数
	 * @return 每月还款额(月还本金 + 利息)
	 */
	public static BigDecimal get_principal_and_interest(BigDecimal loan_principal, BigDecimal month_rate, int periods_number) {
		BigDecimal top = loan_principal.multiply(month_rate).multiply((new BigDecimal(1).add(month_rate)).pow(periods_number));
		BigDecimal bottom = ((new BigDecimal(1).add(month_rate)).pow(periods_number)).subtract(new BigDecimal(1));
		return top.divide(bottom, 4, BigDecimal.ROUND_DOWN);
	}

	/**
	 * 等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、
	 * 利息比重逐月递减。 这个方法比上面那个方法的好处是针对年利率-不用针对月利率
	 * 
	 * @param loan_principal
	 *            借款本金
	 * @param annual_interest_rate
	 *            年化利率
	 * @param periods_number
	 *            还款总期数
	 * @return 每月还款额(月还本金 + 利息)
	 */
	public static BigDecimal get_principal_and_interest_by_year_rate(BigDecimal loan_principal, BigDecimal annual_interest_rate, int periods_number) {
		BigDecimal month_rate = annual_interest_rate.divide(new BigDecimal(12), 10, BigDecimal.ROUND_DOWN);

		BigDecimal top = loan_principal.multiply(month_rate).multiply((new BigDecimal(1).add(month_rate)).pow(periods_number));
		BigDecimal bottom = ((new BigDecimal(1).add(month_rate)).pow(periods_number)).subtract(new BigDecimal(1));
		return top.divide(bottom, 4, BigDecimal.ROUND_DOWN);
	}

	/**
	 * 
	 * 还款计划-对外提供的接口方法
	 * 
	 * @param borrow_amount
	 *            借款金额
	 * @param annual_interest_rate
	 *            年化利率
	 * @param total_period
	 *            总期数
	 */
	public static List<RepaymentPlan> get_repayment_plan(BigDecimal borrow_amount, BigDecimal annual_interest_rate, int total_period) {
		List<RepaymentPlan> repayment_plan_list = new ArrayList<RepaymentPlan>();

		BigDecimal month_rates = annual_interest_rate.divide(new BigDecimal(12), 8, BigDecimal.ROUND_DOWN);// 月利率
		BigDecimal principal_and_interest = get_principal_and_interest(borrow_amount, month_rates, total_period);// 等额本息
		/**
		 * cuurent_period=total_period
		 */
		get_interest_month(month_rates, principal_and_interest, repayment_plan_list, total_period, total_period, borrow_amount);

		Date now = new Date();

		Date last_month_date = now;// 上一次的还款时间
		String last_month_date_string = Model.Date.format(last_month_date);

		if (Utils.isHasData(repayment_plan_list)) {
			int size = repayment_plan_list.size();
			for (int i = 0; i < size; i++) {
				RepaymentPlan repayment_plan = repayment_plan_list.get(i);
				int month_count = i + 1;
				String next_payment_date_day = create_the_repayment_date(now, month_count);
				repayment_plan.setAuto_repayment_date(next_payment_date_day);
				/**
				 * 需要计算出上次还款的那天结束时间和这个还款的结束时间和自动还款日期
				 */
				// 上个月还款那天的晚上+1毫秒，第一个月是创建当时.
				repayment_plan.setRepayment_start_time(get_date_just_other_day(last_month_date_string));
				last_month_date_string = next_payment_date_day;// 将这个月的自动还款的那天作为下个月的开始的第一天的前一天
				repayment_plan.setRepayment_end_time(get_date_just_last(next_payment_date_day));
			}
		}

		return repayment_plan_list;
	}

	/**
	 * 
	 * @param month_rate
	 *            月利率
	 * @param principal_and_interest
	 *            等额本息
	 * @param repayment_plan_list
	 *            还款计划列表
	 * @param total_period
	 *            总期数
	 * @param cuurent_period
	 *            当前第几期
	 * @param remain_pay_amount
	 *            还需要还的本金
	 */
	private static BigDecimal get_interest_month(BigDecimal month_rate, BigDecimal principal_and_interest, List<RepaymentPlan> repayment_plan_list, int total_period, int cuurent_period, BigDecimal remain_pay_amount) {

		RepaymentPlan repayment_plan = new RepaymentPlan();
		repayment_plan.setMonth_pricipal_and_interest(principal_and_interest);

		// 月还利息
		BigDecimal month_interest = remain_pay_amount.multiply(month_rate);
		repayment_plan.setMonth_interest(month_interest);

		// 月还本金
		BigDecimal month_pricipal = principal_and_interest.subtract(month_interest);
		repayment_plan.setMonth_pricipal(month_pricipal);

		// 剩余本金
		remain_pay_amount = remain_pay_amount.subtract(month_pricipal);
		repayment_plan.setRemain_principal(remain_pay_amount);// 最后未还本金可能为0.01这个是可以进行忽略的

		repayment_plan_list.add(repayment_plan);

		cuurent_period = cuurent_period - 1;
		if (cuurent_period == 0) {
			return month_interest;
		} else {
			return get_interest_month(month_rate, principal_and_interest, repayment_plan_list, total_period, cuurent_period, remain_pay_amount);
		}
	}

	/**
	 *创建每个月份的还款日期
	 * 
	 * @param start_time
	 *            开始日期
	 * @param count
	 *            第几次还款
	 */
	public static String create_the_repayment_date(Date start_time, int count) {
		String date = Model.Date.format(start_time);
		Date day_date = null;
		try {
			day_date = Model.Date.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		Calendar calendar = Calendar.getInstance(); // 创建一个日历对象
		calendar.setTime(day_date);

		@SuppressWarnings("unused")
		int year = calendar.get(Calendar.YEAR);// 年份
		@SuppressWarnings("unused")
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		calendar.add(Calendar.MONTH, count);// 添加月份
		int year_next = calendar.get(Calendar.YEAR);
		int month_next = calendar.get(Calendar.MONTH) + 1;

		/**
		 * <pre>
		 * 1 3 5 7 8 10 12 -31
		 *   4 6   9 11    -30
		 *   2 28/29
		 * </pre>
		 */
		if (month_next == 1 || month_next == 3 || month_next == 5 || month_next == 7 || month_next == 8 || month_next == 10 || month_next == 12) {// 31天满的

		}
		if (month_next == 4 || month_next == 6 || month_next == 9 || month_next == 11) {// 30天-如果以前是31天则需要改为30
			if (day == 31) {
				calendar.set(Calendar.DAY_OF_MONTH, 30);
			}
		}
		if (month_next == 2) {
			if (1 <= day && day <= 28) {

			}
			if (day == 29 || day == 30 || day == 31) {
				boolean is_leap_year = is_leap_year(year_next);
				if (is_leap_year) {
					calendar.set(Calendar.DAY_OF_MONTH, 29);
				} else {
					calendar.set(Calendar.DAY_OF_MONTH, 28);
				}
			}
		}
		return Model.Date.format(calendar.getTime());

	}

	/**
	 * 获取某个日期的早上的日期
	 */
	public static Date get_date_just_moning(String date_string) {
		Date date = null;
		try {
			date = Model.Date.parse(date_string);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return date;

	}

	/**
	 * 获取某个日期的晚上最后一刻个日期
	 */
	public static Date get_date_just_last(String date_string) {
		Date date = null;
		try {
			date = Model.Date.parse(date_string);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return new Date(date.getTime() + Model.Day - 1);

	}

	/**
	 * 当头的第二天开始那个日期
	 */
	public static Date get_date_just_other_day(String date_string) {
		Date date = null;
		try {
			date = Model.Date.parse(date_string);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return new Date(date.getTime() + Model.Day);

	}

	/**
	 * 判断当前年份是不是闰年
	 */
	private static boolean is_leap_year(int year) {
		if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
			return true;
		} else {
			return false;
		}

	}

	/*
	 * 计算罚息
	 */
	/**
	 * 如果逾期还款，您要承担罚息与逾期后的管理费用
	 * 
	 * 罚息总额计算公式
	 * 
	 * @param principal_and_interest
	 *            逾期本息
	 * 
	 * @param deadline
	 *            还款截止时间
	 * 
	 * @return 罚息总额
	 * 
	 */
	public static BigDecimal get_punish_principal_and_interest(BigDecimal principal_and_interest, Date deadline) {

		Date now = new Date();
		/**
		 * Math.ceil取上整数
		 */
		int overdue_days = (int) Math.ceil(new Long(now.getTime() - deadline.getTime()).doubleValue() / new Long(24 * 60 * 60 * 1000).doubleValue()); // 逾期天数
		if (overdue_days <= 0) {
			return new BigDecimal(0.00);

		} else if (overdue_days <= 30) {
			return principal_and_interest.multiply(new BigDecimal(overdue_days).multiply(new BigDecimal(0.0005)));
		} else {
			BigDecimal $30_day_before = principal_and_interest.multiply(new BigDecimal(30)).multiply(new BigDecimal(0.0005));
			BigDecimal $30_day_after = principal_and_interest.multiply(new BigDecimal(overdue_days - 30)).multiply(new BigDecimal(0.001));
			return $30_day_before.add($30_day_after);
		}

	}

	/**
	 * 
	 * 计算还款所处的时期
	 * 
	 */
	public static int get_repayment_period(Date repay_end_time) {

		Date now = new Date();
		int overdue_days = (int) Math.ceil(new Long(now.getTime() - repay_end_time.getTime()).doubleValue() / new Long(24 * 60 * 60 * 1000).doubleValue()); // 逾期天数
		if (overdue_days < 0) { // 还款日之前
			return 1;
		} else if (overdue_days == 0) { // 处于还款日当天，自动还款
			return 2;
		} else if (0 < overdue_days && overdue_days <= 30) { // 普通逾期
			return 3;
		} else { // 严重逾期
			return 4;

		}

	}

	/**
	 * 如果逾期还款，您要承担罚息与逾期后的管理费用
	 * 
	 * 获取逾期管理费
	 * 
	 * @param principal_and_interest
	 *            逾期本息
	 * 
	 * @param deadline
	 *            还款截止时间
	 * 
	 * @return 逾期管理费
	 * 
	 */
	public static BigDecimal get_punish_manage_fee(BigDecimal principal_and_interest, Date deadline) {

		Date now = new Date();
		int overdue_days = (int) Math.ceil(new Long(now.getTime() - deadline.getTime()).doubleValue() / new Long(24 * 60 * 60 * 1000).doubleValue()); // 逾期天数
		if (overdue_days <= 0) {
			return new BigDecimal(0.00);
		} else if (overdue_days <= 30) {
			return principal_and_interest.multiply(new BigDecimal(overdue_days)).multiply(new BigDecimal(0.001));
		} else {
			BigDecimal $30_day_before = principal_and_interest.multiply(new BigDecimal(30)).multiply(new BigDecimal(0.001));
			BigDecimal $30_day_after = principal_and_interest.multiply(new BigDecimal(overdue_days - 30)).multiply(new BigDecimal(0.005));
			return $30_day_before.add($30_day_after);
		}

	}

	/**
	 * 债权转让
	 */

	/**
	 * 转让时，当期还款处于未还款状态 的债权价值
	 * 
	 * @param left_not_pay_back_principal
	 *            剩余未还本金
	 * @param deal_date
	 *            债权转让成交日期
	 * @param last_month_pay_date
	 *            上期还款对应的应还款日期
	 * @param annulized_rate
	 *            年化利率
	 * @return debt_value 债权价值
	 */
	public static BigDecimal get_debt_value1(BigDecimal left_not_pay_back_principal, Date deal_date, Date last_month_pay_date, BigDecimal annulized_rate) {
		BigDecimal month_rate = annulized_rate.divide(new BigDecimal(12), 4, BigDecimal.ROUND_DOWN);
		// 应计利息天数
		int should_count_day = (int) ((deal_date.getTime() - last_month_pay_date.getTime()) / Model.Day);
		should_count_day = should_count_day < 30 ? should_count_day : 30;
		return left_not_pay_back_principal.add(left_not_pay_back_principal.multiply(month_rate).multiply(new BigDecimal(new Double(should_count_day) / 30)));
	}

	/**
	 * 转让时，当期还款处于已还款状态，但下一期处于未还款状态
	 * 
	 * @param left_not_pay_back_principal
	 *            剩余未还本金
	 * @param deal_date
	 *            成交日期
	 * @param pay_date
	 *            成交日期所在期的应还款日期
	 * @param annulized_rate
	 *            年化利率
	 * @return debt_value 债权价值
	 */
	public static BigDecimal get_debt_value2(BigDecimal left_not_pay_back_principal, Date deal_date, Date pay_date, BigDecimal annulized_rate) {

		BigDecimal month_rate = annulized_rate.divide(new BigDecimal(12), 4, BigDecimal.ROUND_DOWN);
		// 天数差
		int should_count_day = (int) ((pay_date.getTime() - deal_date.getTime()) / (1000 * 60 * 60 * 24));
		should_count_day = should_count_day < 30 ? should_count_day : 30;

		return left_not_pay_back_principal.subtract(left_not_pay_back_principal.multiply(month_rate).multiply(new BigDecimal(new Double(should_count_day) / 30)));

	}

	/**
	 * 转让时，当期还款处于已还款状态，且下面的N期处于已还款状态，但还未完全还清
	 * 
	 * @param left_not_pay_back_principal
	 *            剩余未还本金
	 * @param deal_date
	 *            成交日期
	 * @param pay_date
	 *            成交日期所在期的应还款日期
	 * @param annulized_rate
	 *            年化利率
	 * @param N
	 *            从下期开始算，已经还款的期数
	 * @return debt_value 债权价值
	 */
	public static BigDecimal get_debt_value3(BigDecimal left_not_pay_back_principal, Date deal_date, Date pay_date, BigDecimal annulized_rate, int N) {

		BigDecimal month_rate = annulized_rate.divide(new BigDecimal(12), 4, BigDecimal.ROUND_DOWN);

		// 最后还款所在期数与成交日期所在期数之差
		int period_different = N;

		// 天数差
		int should_count_day = (int) ((pay_date.getTime() - deal_date.getTime()) / (1000 * 60 * 60 * 24));
		should_count_day = should_count_day < 30 ? should_count_day : 30;

		BigDecimal debt_value_left = left_not_pay_back_principal.divide((new BigDecimal(1).add(month_rate)).pow(period_different), 10, BigDecimal.ROUND_DOWN);

		BigDecimal debt_value_right = new BigDecimal(1).subtract(new BigDecimal(new Double(should_count_day) / 30).multiply(month_rate));

		return debt_value_left.multiply(debt_value_right);

	}

	/*
	 * 联富宝投资
	 */
	/**
	 * 获取 联富宝投资天数的 内的收益 利息
	 * 
	 * @param start_date
	 *            锁定时间
	 * @param end_date
	 *            退出时间
	 * @param annulized_rate
	 *            年化利率
	 * @param bid_money
	 *            投资金额
	 * @return
	 * @throws ParseException
	 */
	public static BigDecimal get_fix_bid_back_money(String start_date, String end_date, BigDecimal annulized_rate, Integer bid_money) throws ParseException {
		int days = DateTimeUtil.daysBetween(start_date, end_date);// 当天到锁定的天数
		// 不到一天 要退出按一天计算
		if (days == 0) {
			days = 1;
		}
		BigDecimal days_decimal = new BigDecimal(days);
		BigDecimal bid_money_decimal = new BigDecimal(bid_money);// 投资金额

		/**
		 * 判断当年有多少天 如果是闰年 366
		 */
		Calendar calendar = Calendar.getInstance(); // 创建一个日历对象
		calendar.setTime(new Date());
		int year = calendar.get(Calendar.YEAR);
		int days_of_year = new GregorianCalendar().isLeapYear(year) ? 366 : 365;
		// （投资金额 * 年化利率 * 投资的天数 ）/ 365
		BigDecimal one_years_days = new BigDecimal(days_of_year);
		/**
		 * 利息收入
		 */
		return bid_money_decimal.multiply(annulized_rate).multiply(days_decimal).divide(one_years_days, 4, BigDecimal.ROUND_DOWN).setScale(2, BigDecimal.ROUND_DOWN);
	}

}
