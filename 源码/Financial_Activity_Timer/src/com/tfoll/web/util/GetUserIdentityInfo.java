package com.tfoll.web.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

public class GetUserIdentityInfo {
	public static final String Idcard_Info_Url = "http://apis.juhe.cn/idcard/index?key=670a622c50f1c13e64c75ba51e5e7026&cardno=";
	public static Logger logger = Logger.getLogger(GetUserIdentityInfo.class);

	@SuppressWarnings("unchecked")
	public static UserIdentityInfo getUserIdentityInfo(String idcard) {

		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(Idcard_Info_Url + idcard);
		List<Header> headers = new ArrayList<Header>();
		headers.add(new Header("Content-Type", "application/json-rpc"));
		client.getHostConfiguration().getParams().setParameter("http.default-headers", headers);
		try {
			int code = client.executeMethod(post);
			if (code != HttpStatus.SC_OK) {
				return null;
			} else {
				try {
					String msg = post.getResponseBodyAsString();
					/**
					 * <pre>
					 * {"resultcode":"200","reason":"成功的返回","result":{"area":"湖北省潜江市","sex":"男","birthday":"1993年08月15日","verify":""},"error_code":0}
					 * </pre>
					 */
					Gson gson = new Gson();
					Map<String, Object> map = gson.fromJson(msg, new TypeToken<Map<String, Object>>() {
					}.getType());
					String resultcode = (String) map.get("resultcode");
					if (!"200".equals(resultcode)) {
						return null;
					} else {
						Map<String, String> result = (Map<String, String>) map.get("result");
						String area = result.get("area");
						String sex = result.get("sex");
						String birthday = result.get("birthday");
						String verify = result.get("verify");
						UserIdentityInfo userIdentityInfo = new UserIdentityInfo();
						userIdentityInfo.setArea(area);
						userIdentityInfo.setSex(sex);
						userIdentityInfo.setBirthday(birthday);
						userIdentityInfo.setVerify(verify);

						return userIdentityInfo;
					}
				} catch (Exception e) {
					return null;
				}
			}
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException(e.getMessage());
		} finally {
			if (post != null) {
				post.releaseConnection();// 释放连接
			}
		}
	}

	public static void main(String[] args) {
		System.out.println(GetUserIdentityInfo.getUserIdentityInfo("429005199308150152"));

	}

	public static class UserIdentityInfo {
		@Override
		public String toString() {
			return "UserIdentityInfo [area=" + area + ", birthday=" + birthday + ", sex=" + sex + ", verify=" + verify + "]";
		}

		/**
		 * 地址
		 * 
		 */
		private String area;
		/**
		 * 性别
		 */
		private String sex;
		/**
		 * 生日
		 */
		private String birthday;
		/**
		 * 。。。
		 */
		private String verify;

		public String getArea() {
			return area;
		}

		public void setArea(String area) {
			this.area = area;
		}

		public String getSex() {
			return sex;
		}

		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getBirthday() {
			return birthday;
		}

		public void setBirthday(String birthday) {
			this.birthday = birthday;
		}

		public String getVerify() {
			return verify;
		}

		public void setVerify(String verify) {
			this.verify = verify;
		}

	}

}