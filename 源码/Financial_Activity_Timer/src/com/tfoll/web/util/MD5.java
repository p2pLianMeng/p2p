package com.tfoll.web.util;

import java.security.MessageDigest;

public class MD5 {
	public static String md5(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");

		}
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.reset();
			md5.update(string.getBytes());
			byte[] array = md5.digest();
			StringBuffer sb = new StringBuffer(32);
			for (int j = 0; j < array.length; ++j) {
				int bit = array[j] & 0xFF;
				if (bit < 16)
					sb.append('0');

				sb.append(Integer.toHexString(bit));
			}
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public MessageDigest getMD5Algorithm() {
		try {
			return MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public byte[] getMessageDigest(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");

		}
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			md5.reset();
			md5.update(string.getBytes());
			return md5.digest();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		System.out.println(md5("1"));
	}
}