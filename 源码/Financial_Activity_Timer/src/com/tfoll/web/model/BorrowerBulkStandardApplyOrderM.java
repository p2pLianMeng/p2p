package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_apply_order", primaryKey = "id")
public class BorrowerBulkStandardApplyOrderM extends Model<BorrowerBulkStandardApplyOrderM> {
	public static BorrowerBulkStandardApplyOrderM dao = new BorrowerBulkStandardApplyOrderM();
}
