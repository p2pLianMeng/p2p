package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;
import java.util.Map;

@TableBind(tableName = "_____activity___standard_order_waiting_of_lender_copy", primaryKey = "id")
public class _____Activity___Standard_Order_Waiting_Of_Lender_CopyM extends Model<_____Activity___Standard_Order_Waiting_Of_Lender_CopyM> implements Serializable {

	private static final long serialVersionUID = -7124140993536632669L;
	public static _____Activity___Standard_Order_Waiting_Of_Lender_CopyM dao = new _____Activity___Standard_Order_Waiting_Of_Lender_CopyM();

	public static boolean add______activity___standard_order_waiting_of_lender_copy(_____Activity___Standard_Order_Waiting_Of_LenderM _____activity___standard_order_waiting_of_lender) {
		_____Activity___Standard_Order_Waiting_Of_Lender_CopyM _____activity___standard_order_waiting_of_lender_copy = new _____Activity___Standard_Order_Waiting_Of_Lender_CopyM();
		/**
		 * 里面的表结构必须一致，也就是必须是同一条的数据才行
		 */
		Map<String, Object> map = _____activity___standard_order_waiting_of_lender.getM();
		map.remove("id");
		_____activity___standard_order_waiting_of_lender_copy.setMap(map);
		return _____activity___standard_order_waiting_of_lender_copy.save();
	}
}
