package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "_____activity___standard_order_waiting_of_borrower", primaryKey = "id")
public class _____Activity___Standard_Order_Waiting_Of_BorrowerM extends Model<_____Activity___Standard_Order_Waiting_Of_BorrowerM> implements Serializable {

	private static final long serialVersionUID = -7124140993536632669L;
	public static _____Activity___Standard_Order_Waiting_Of_BorrowerM dao = new _____Activity___Standard_Order_Waiting_Of_BorrowerM();
}
