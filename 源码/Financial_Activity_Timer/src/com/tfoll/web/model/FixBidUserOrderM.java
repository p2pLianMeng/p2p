package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

/**
 * 
 * @author HangSun
 * 
 */
@TableBind(tableName = "fix_bid_user_order", primaryKey = "id")
public class FixBidUserOrderM extends Model<FixBidUserOrderM> implements Serializable {
	private static final long serialVersionUID = 1341508986121443764L;
	public static FixBidUserOrderM dao = new FixBidUserOrderM();

}
