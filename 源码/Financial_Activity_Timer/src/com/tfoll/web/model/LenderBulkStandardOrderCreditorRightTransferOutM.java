package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_order_creditor_right_transfer_out", primaryKey = "id")
public class LenderBulkStandardOrderCreditorRightTransferOutM extends Model<LenderBulkStandardOrderCreditorRightTransferOutM> {
	public static LenderBulkStandardOrderCreditorRightTransferOutM dao = new LenderBulkStandardOrderCreditorRightTransferOutM();

}
