package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@TableBind(tableName = "borrower_bulk_standard_gather_money_order", primaryKey = "id")
public class BorrowerBulkStandardGatherMoneyOrderM extends Model<BorrowerBulkStandardGatherMoneyOrderM> {

	private static final long serialVersionUID = 8714753820330201896L;
	public static BorrowerBulkStandardGatherMoneyOrderM dao = new BorrowerBulkStandardGatherMoneyOrderM();

	/**
	 * 
	 * 根据id加锁 获取 筹款单的信息
	 * 
	 */
	private static BorrowerBulkStandardGatherMoneyOrderM get_borrower_bulk_standard_gather_money_order_by_id_for_update(Long id) {
		String sql = "select * from borrower_bulk_standard_gather_money_order where id = ? for update";
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst(sql, new Object[] { id });
		return borrower_bulk_standard_gather_money_order;
	}

	/**
	 * 多个用户并发访问同一条筹款单时，
	 * 
	 */
	public static class GetGatherOrderByIdTask implements Callable<BorrowerBulkStandardGatherMoneyOrderM> {
		private long order_id;

		public GetGatherOrderByIdTask(long orderId) {
			super();
			this.order_id = orderId;
		}

		public BorrowerBulkStandardGatherMoneyOrderM call() throws Exception {
			return BorrowerBulkStandardGatherMoneyOrderM.get_borrower_bulk_standard_gather_money_order_by_id_for_update(this.order_id);
		}
	}

	/**
	 * <pre>
	 * 1 该方法可能由于数据库锁等待超时，会返回为null
	 * 2 该方法只能在原子事务里面调用-否则会死锁
	 * </pre>
	 */
	public static BorrowerBulkStandardGatherMoneyOrderM get_borrower_bulk_standard_gather_money_order_short_wait(final long order_id) throws Exception {

		BorrowerBulkStandardGatherMoneyOrderM.GetGatherOrderByIdTask get_fix_bid_task = new BorrowerBulkStandardGatherMoneyOrderM.GetGatherOrderByIdTask(order_id);
		ExecutorService get_gather_order_task_service = Executors.newFixedThreadPool(1);
		// 对task对象进行各种set操作以初始化任务
		Future<BorrowerBulkStandardGatherMoneyOrderM> future = get_gather_order_task_service.submit(get_fix_bid_task);
		try {
			return future.get(3, TimeUnit.SECONDS);
		} catch (Exception e) {
			return null;
		} finally {
			if (future.isCancelled()) {
				future.cancel(true);
			}
			get_gather_order_task_service.shutdownNow();
		}

	}

}