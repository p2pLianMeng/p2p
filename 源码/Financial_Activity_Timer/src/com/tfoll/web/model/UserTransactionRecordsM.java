package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.trade.core.Controller;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 活动这边不需要写注释
 * 
 */
@TableBind(tableName = "user_transaction_records", primaryKey = "id")
public class UserTransactionRecordsM extends Model<UserTransactionRecordsM> {
	public static UserTransactionRecordsM dao = new UserTransactionRecordsM();

	/**
	 * 定义参数类型
	 * 
	 * <pre>
	 * Key规定  Value可以少算不能多算
	 * --充值体现
	 * 11充值操作-前期为0  后期可能为0.2%
	 * 12 提现操作-前期为0  后期可能为几块钱
	 * --联富宝
	 * 21 联富宝回款-投资本金及利息返还-没有手续费
	 * 22联富宝提前退出-2%
	 * --借款
	 * 31-借款服务费-借完后马上扣掉按照信用等级
	 * 
	 * 32-偿还本息-根据借款金额而定（按照人人贷公司核算出来的）
	 * 33-借款管理费-借款本金的0.3%-每个月多收
	 * 34-返还服务费-特殊客户特定时期返还，这个后期再说-借款服务费 每个月都收固定的0.3%
	 * 上面三个是每个月必须要收的
	 * 
	 * 35-回收本息：逾期1-30天内，只要借款者账户有钱就要扣除应还的本息（和偿还本息一样的公司算出来的）+管理费+需要加上罚息[1-30天的]+逾期管理费
	 * 36-提前回收：当用户逾期到达31天就需要提前回收所有本金，并且按照罚息公式计算其应付的利息
	 * 37-提前还款：需要支付给理财人借款剩余本金的1%作为违约金，不用再支付后续的利息及管理费用。
	 * 38-逾期管理费：和人人贷平台一致
	 * 
	 * 
	 * 
	 * --理财
	 * 51债权转让管理费：债权转让的费用为转让管理费。平台向转出人收取，不向购买人收取任何费用。
	 * 转让管理费金额为成交金额*转让管理费率，转让管理费率为0.5%，具体金额以债权转让页面显示为准。债权转让管理费在 成交后直接从成交金额中扣除，不成交平台不向用户收取转让管理费。
	 * </pre>
	 */
	public static final int Type_11 = 11;// 充值操作
	public static final int Type_12 = 12;// 提现操作

	public static final int Type_21 = 21;// 联富宝回款
	public static final int Type_22 = 22;// 联富宝提前退出
	public static final int Type_23 = 23;// 联富宝购买支付

	public static final int Type_31 = 31;// 借款服务费
	public static final int Type_32 = 32;// 偿还本息
	public static final int Type_33 = 33;// 借款管理费
	public static final int Type_34 = 34;// 返还服务费
	public static final int Type_35 = 35;// 回收本息
	public static final int Type_36 = 36;// 提前回收
	public static final int Type_37 = 37;// 提前还款
	public static final int Type_38 = 38;// 逾期管理费

	public static final int Type_51 = 51;// 债权转让管理费

	public static final int Type_201 = 201;// 散标新手标利息
	public static final int Type_202 = 202;// 联富宝新手标利息
	public static final int Type_203 = 203;// 推荐人奖励一直是30元
	public static final int Type_204 = 204;// 指定期间-12月12号都2月12号理财奖励（本金*1%）
	public static final int Type_205 = 205;// 推荐人奖励（本金*1%）-推荐人奖励

	/**
	 *按照类型进行添加财务记录
	 * 
	 * @param user_id
	 * @param type
	 * @param start_money
	 * @param pay
	 * @param income
	 * @param end_money
	 * @return
	 */
	public static boolean add_user_transaction_records_by_type(int user_id, int type, BigDecimal start_money, BigDecimal pay, BigDecimal income, BigDecimal end_money) {
		Date now = new Date();
		String add_time_date = Controller.Date.format(now);
		UserTransactionRecordsM user_transaction_records = new UserTransactionRecordsM();
		return user_transaction_records.//
				set("user_id", user_id).//
				set("type", type).//
				set("start_money", start_money).//
				set("pay", pay).//
				set("income", income).//
				set("end_money", end_money).//
				set("add_time_long", now.getTime()).//
				set("add_time_date", add_time_date).//
				save();

	}

}
