package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.math.BigDecimal;
import java.util.Date;

@TableBind(tableName = "sys_expenses_records", primaryKey = "id")
public class SysExpensesRecordsM extends Model<SysExpensesRecordsM> {
	public static SysExpensesRecordsM dao = new SysExpensesRecordsM();

	public static boolean add_sys_expenses_records(int user_id, int type, BigDecimal money, String detail) {
		SysExpensesRecordsM sys_income_records = new SysExpensesRecordsM();
		Date now = new Date();
		return sys_income_records.//
				set("user_id", user_id).//
				set("type", type).//
				set("money", money).//
				set("detail", detail).//
				set("add_time_long", now.getTime()).//
				set("add_time_date", Date.format(now)).//
				save();

	}
}
