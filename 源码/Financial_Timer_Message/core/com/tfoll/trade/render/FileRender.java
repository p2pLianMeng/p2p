package com.tfoll.trade.render;

import com.tfoll.trade.activerecord.model.Model;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class FileRender extends Render {
	static Logger logger = Logger.getLogger(FileRender.class);

	private File file;

	/**
	 * 下载文件的时候要确保文件存在
	 */
	public FileRender(File file) {
		this.file = file;
	}

	@Override
	public void render() {
		// 下载文件的大小由应用程序控制
		if (file == null || !file.isFile() || file.length() > Long.MAX_VALUE) {
			return;
		}
		String[] dots = file.getName().split("\\.");
		if (dots.length < 1) {
			return;
		}
		HttpServletResponse response = getResponse();

		response.reset();
		response.setContentType("application/octet-stream");
		String date = (Model.Date.format(new Date()));
		response.addHeader("Content-disposition", "attachment; filename=" + date + "." + dots[dots.length - 1]);
		response.setContentLength((int) file.length());
		response.setHeader("Connection", "close");

		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = new BufferedInputStream(new FileInputStream(file));
			outputStream = response.getOutputStream();
			byte[] buffer = new byte[1024];
			int length = -1;
			while ((length = (inputStream.read(buffer))) != -1) {
				outputStream.write(buffer, 0, length);
			}
			outputStream.flush();
		} catch (Exception e) {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e1) {
					logger.error(e1);
				} finally {
					// 调用垃圾收集器回收没有引用指向的流对象
					outputStream = null;
				}
			}
			if (inputStream != null) {
				try {
					// 调用垃圾收集器回收没有引用指向的流对象
					inputStream.close();
				} catch (IOException e2) {
					logger.error(e2);
				} finally {
					inputStream = null;
				}
			}
			try {// 调用垃圾收集器回收没有引用指向的流对象
				System.gc();
			} catch (Exception e2) {
			}
			throw new RenderException(e);
		} finally {

			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					logger.error(e);
				} finally {
					// 调用垃圾收集器回收没有引用指向的流对象
					outputStream = null;
				}
			}
			if (inputStream != null) {
				try {
					// 调用垃圾收集器回收没有引用指向的流对象
					inputStream.close();
				} catch (IOException e) {
					logger.error(e);
				} finally {
					inputStream = null;
				}
			}

		}
	}
}
