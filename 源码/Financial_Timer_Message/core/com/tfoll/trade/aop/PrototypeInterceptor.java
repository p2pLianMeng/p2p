package com.tfoll.trade.aop;

/**
 * 继承这个类可以保证Interceptor线程安全
 */
public abstract class PrototypeInterceptor implements Interceptor {

	public final void doIt(ActionExecutor actionExecutor) {
		try {
			getClass().newInstance().doPrototypeIntercept(actionExecutor);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public abstract void doPrototypeIntercept(ActionExecutor actionExecutor);
}
