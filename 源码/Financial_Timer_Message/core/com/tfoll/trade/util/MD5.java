package com.tfoll.trade.util;

import java.security.MessageDigest;

/**
 * 公司内部MD5
 * 
 */
public class MD5 {
	/**
	 * 
	 * @param string
	 *            a-z
	 * @return
	 */
	public static String md5(String string) {
		return _md5(_md5(_md5(string)));
	}

	private static String _md5(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");

		}
		string = "a" + string + "z";// 公司内部加密算法
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.reset();
			md5.update(string.getBytes());
			byte[] array = md5.digest();
			StringBuffer sb = new StringBuffer(32);
			for (int j = 0; j < array.length; ++j) {
				int bit = array[j] & 0xFF;
				if (bit < 16)
					sb.append('0');

				sb.append(Integer.toHexString(bit));
			}
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public MessageDigest getMD5Algorithm() {
		try {
			return MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public byte[] getMessageDigest(String string) {
		if (string == null) {
			throw new NullPointerException("string is null");

		}
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			md5.reset();
			md5.update(string.getBytes());
			return md5.digest();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		System.out.println(md5("123456"));// dea235f9fe9778222b90232c8a8a9164
	}
}