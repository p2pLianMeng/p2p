package com.tfoll.trade.activerecord.dialect;

import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.TableInfo;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class OracleDialect extends Dialect {
	@Override
	public String buildSqlForTableInfo(String tableName) {

		return "select * from " + tableName;
	}

	@Override
	public String deleteByIdForDb(String tableName, String primaryKey) {

		StringBuilder sql = new StringBuilder("delete from ");
		sql.append(tableName.trim());
		sql.append(" where ").append(primaryKey).append(" = ?");
		return sql.toString();
	}

	@Override
	public String deleteByIdForModel(TableInfo tableInfo) {
		String pKey = tableInfo.getPrimaryKey();
		StringBuilder sql = new StringBuilder(45);
		sql.append("delete from ");
		sql.append(tableInfo.getTableName());
		sql.append(" where ").append(pKey).append(" = ?");
		return sql.toString();
	}

	@Override
	public String findByIdForDb(String tableName, String primaryKey, String columns) {
		StringBuilder sql = new StringBuilder("select ");
		if (columns.trim().equals("*")) {
			sql.append(columns);
		} else {
			String[] columnsArray = columns.split(",");
			for (int i = 0; i < columnsArray.length; i++) {
				if (i > 0) {
					sql.append(", ");
				}
				sql.append(columnsArray[i].trim());
			}
		}
		sql.append(" from ");
		sql.append(tableName.trim());
		sql.append(" where ").append(primaryKey).append(" = ?");
		return sql.toString();
	}

	@Override
	public String findByIdForModel(TableInfo tableInfo, String columns) {
		StringBuilder sql = new StringBuilder("select ");
		if (columns.trim().equals("*")) {
			sql.append(columns);
		} else {
			String[] columnsArray = columns.split(",");
			for (int i = 0; i < columnsArray.length; i++) {
				if (i > 0) {
					sql.append(", ");
				}
				sql.append(columnsArray[i].trim());
			}
		}
		sql.append(" from ");
		sql.append(tableInfo.getTableName());
		sql.append(" where ").append(tableInfo.getPrimaryKey()).append(" = ?");
		return sql.toString();
	}

	@Override
	public boolean isSupportAutoIncrementKey() {
		return false;
	}

	@Override
	public void saveForDb(String tableName, Record record, StringBuilder sql, List<Object> params) {
		sql.append("insert into ");
		sql.append(tableName.trim()).append("(");
		StringBuilder temp = new StringBuilder();
		// insert into tableName(name,password)values("name","password")
		temp.append(") values(");

		for (Entry<String, Object> entry : record.getColumnMap().entrySet()) {
			if (params.size() > 0) {
				sql.append(", ");
				temp.append(", ");
			}
			sql.append(entry.getKey());
			temp.append("?");
			params.add(entry.getValue());
		}
		sql.append(temp.toString()).append(")");
	}

	@Override
	public void saveForModel(TableInfo tableInfo, Map<String, Object> attributeMap, StringBuilder sql, List<Object> params) {
		sql.append("insert into ").append(tableInfo.getTableName()).append("(");
		StringBuilder temp = new StringBuilder(") values(");
		for (Entry<String, Object> entry : attributeMap.entrySet()) {
			String columnName = entry.getKey();
			if (tableInfo.containsColumnName(columnName)) {
				if (params.size() > 0) {
					sql.append(", ");
					temp.append(", ");
				}
				sql.append(columnName);
				temp.append("?");
				params.add(entry.getValue());
			}
		}
		sql.append(temp.toString()).append(")");
	}

	@Override
	public void updateForDb(String tableName, String primaryKey, Object id, Record record, StringBuilder sql, List<Object> params) {
		sql.append("update ").append(tableName.trim()).append(" set ");
		for (Entry<String, Object> entry : record.getColumnMap().entrySet()) {
			String columnName = entry.getKey();
			if (!primaryKey.equalsIgnoreCase(columnName)) {
				if (params.size() > 0) {
					sql.append(", ");
				}
				sql.append(columnName).append(" = ? ");
				params.add(entry.getValue());
			}
		}
		sql.append(" where ").append(primaryKey).append(" = ?");
		params.add(id);
	}

	@Override
	public void updateForModel(TableInfo tableInfo, Map<String, Object> attributeMap, Set<String> modifyFlag, String primaryKey, Object id, StringBuilder sql, List<Object> params) {
		sql.append("update ").append(tableInfo.getTableName()).append(" set ");
		for (Entry<String, Object> entry : attributeMap.entrySet()) {
			String columnName = entry.getKey();
			if (!primaryKey.equalsIgnoreCase(columnName) && modifyFlag.contains(columnName) && tableInfo.containsColumnName(columnName)) {
				if (params.size() > 0) {
					sql.append(", ");
				}
				sql.append(columnName).append(" = ? ");
				params.add(entry.getValue());
			}
		}
		sql.append(" where ").append(primaryKey).append(" = ?");
		params.add(id);
	}

}
