package com.tfoll.trade.activerecord;

import com.tfoll.trade.activerecord.dialect.Dialect;
import com.tfoll.trade.activerecord.dialect.MysqlDialect;
import com.tfoll.trade.activerecord.transaction.Transaction;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * 该类需要在框架启动的时候设置 [数据源_必须设置,数据库事务级别,数据库方言]
 * 本数据库连接管理默认不支持事务-需要在获得连接前将ThreadLocalContent中isSurpportTransaction变量设置为true
 */
public final class DataSourceKit {
	/**
	 * 本框架只支持一个数据源实例
	 */
	public static DataSource dataSource;

	public static Dialect dialect = new MysqlDialect();

	public static Logger logger = Logger.getLogger(DataSourceKit.class);
	public static final Object[] NULL_PARA_ARRAY = new Object[0];

	/**
	 * 支持事务的连接获取方法
	 */
	public static final Connection getConnection() {
		Connection conn = Transaction.getConnection();

		try {
			if (conn != null && !conn.isClosed()) {
				return conn;
			}
		} catch (SQLException e) {
			logger.error(e);
			throw new ActiveRecordException(e);
		}

		if (dataSource == null) {
			logger.error("请初始化数据源");
			throw new ActiveRecordException("请初始化数据源");
		}
		try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ActiveRecordException(e);
		}
		if (conn == null) {
			logger.error("不能从数据源取得连接");
			throw new ActiveRecordException("不能从数据源取得连接");
		} else {
			Transaction.set(conn);
			// 需要和事务保持一致
			boolean isSurpportTransaction = Transaction.isSurpportTransaction();
			if (!isSurpportTransaction) {// 包括第一次添加连接和连接处于非事务环境。如果本来就是事务环境则保持事务环境不变
				Transaction.set(false);
			}

			return conn;
		}

	}

	public static final void close(Statement stmt, Connection conn) {
		close(stmt);
		close(conn);
	}

	public static final void close(ResultSet rs, Statement stmt) {
		close(rs);
		close(stmt);
	}

	public static final void close(ResultSet rs, Statement stmt, Connection conn) {
		close(rs);
		close(stmt);
		close(conn);
	}

	public static final void close(Connection conn) {
		boolean isSurpportTransaction = Transaction.isSurpportTransaction();
		try {
			if (isSurpportTransaction) {// 判断是不是处于事务环境下面
				if (conn != null && !conn.isClosed()) {
					Transaction.set(conn);
				} else {
					throw new ActiveRecordException("connection is null or isClosed");
				}
			} else {
				// 真正的需要关闭-如果是支持事务的连接需要更新数据-则需要将事务环境设置为false才能设置成功
				if (conn != null && !conn.isClosed()) {// 非事务的环境
					try {
						// conn.setAutoCommit(true);// 恢复自动提交
						conn.close();

					} catch (Exception e) {
						logger.error(e);
						System.gc();
					} finally {
						conn = null;
						Transaction.removeConnection();// 真正的关闭连接和事务标志
						Transaction.removeTransaction();
					}
				} else {
					conn = null;

				}
			}
		} catch (SQLException e) {
			logger.error(e);
			throw new ActiveRecordException(e.getMessage());
		}

	}

	public static final void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				logger.error("Statement实例不能关闭" + e.getMessage());
			} finally {
				stmt = null;
			}
		}
	}

	public static final void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				logger.error("ResultSet实例不能关闭" + e.getMessage());
			} finally {
				rs = null;
			}
		}

	}

	// ==========================================================================
	public static final Dialect getDialect() {
		return dialect;
	}

	public static final void commit(Connection conn) {
		try {
			if (conn != null && !conn.isClosed()) {
				conn.commit();
			} else {
				throw new SQLException("connection is null or isClosed");
			}
		} catch (SQLException e) {
			logger.error(e);
			throw new ActiveRecordException(e.getMessage());
		}
	}

	public static final void rollback(Connection conn) {
		try {
			if (conn != null && !conn.isClosed()) {
				conn.rollback();
			} else {
				throw new SQLException("connection is null or isClosed");
			}
		} catch (SQLException e) {
			logger.error(e);
			throw new ActiveRecordException(e);
		}
	}

	// 事务处理
	public static final void setAutoCommit(Connection conn, Boolean vlaue) {
		try {
			if (conn != null && !conn.isClosed()) {
				conn.setAutoCommit(vlaue);
			} else {
				throw new SQLException("connection is null or isClosed");
			}
		} catch (SQLException e) {
			logger.error(e);
			throw new ActiveRecordException(e);
		}
	}

	// 必须设置
	public static final void setDataSource(DataSource dataSource) {
		DataSourceKit.dataSource = dataSource;
	}

	// 自定义设置
	public static final void setDialect(Dialect dialect) {
		DataSourceKit.dialect = dialect;
	}

}
