package com.tfoll.trade.core;

import com.tfoll.trade.aop.Interceptor;

import java.lang.reflect.Method;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Action可以定位到Controller+Method
 */
public class Action {
	public static Logger logger = Logger.getLogger(Action.class);
	private final String controllerClassKey;
	private final Class<? extends Controller> controllerClass;
	private final Controller controller;
	// 访问URL
	private final String actionKey;
	private final Method method;
	private final String methodName;
	private final List<Interceptor> interceptorList;

	public Action(String actionKey, String controllerKey, Class<? extends Controller> controllerClass, Controller controller, String methodName, Method method, List<Interceptor> interceptorList) {
		this.actionKey = actionKey;

		this.controllerClassKey = controllerKey;
		this.controllerClass = controllerClass;
		this.controller = controller;

		this.methodName = methodName;
		this.method = method;
		this.interceptorList = interceptorList;

	}

	public String getActionKey() {
		return actionKey;
	}

	public String getControllerKey() {
		return controllerClassKey;
	}

	public Class<? extends Controller> getControllerClass() {
		return controllerClass;
	}

	public Controller getController() {
		return controller;
	}

	public List<Interceptor> getInterceptorList() {
		return interceptorList;
	}

	public String getMethodName() {
		return methodName;
	}

	public Method getMethod() {
		return method;
	}

}
