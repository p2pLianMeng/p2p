package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_withdraw_cash_log", primaryKey = "id")
public class UserWithdrawCashLogM extends Model<UserWithdrawCashLogM> {
	public static UserWithdrawCashLogM dao = new UserWithdrawCashLogM();
}
