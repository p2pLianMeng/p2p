package com.tfoll.web.timer;

import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.UserMessageRequestWaitingM;
import com.tfoll.web.timer.message_engine.MessageEngine;
import com.tfoll.web.util.Utils;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 快速短信处理引擎-提取数据-处理数据
 * 
 */
public class Timer_auto_send_message extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_auto_send_message.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_auto_send_message.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_auto_send_message.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_auto_send_message.class.getName() + "开始");
				{// 真正的业务控制-不用while就可以实现循环检测功能
					doTask();
				}
				logger.info("定时任务:" + Timer_auto_send_message.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_auto_send_message.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	/**
	 * 此处只是读取数据-邮箱
	 */
	public static void doTask() throws Exception {
		while (true) {
			List<UserMessageRequestWaitingM> user_message_request_waiting_list = UserMessageRequestWaitingM.dao.find("SELECT * from user_message_request_waiting ORDER BY id asc");
			if (Utils.isHasData(user_message_request_waiting_list)) {

				for (UserMessageRequestWaitingM user_message_request_waiting : user_message_request_waiting_list) {
					if (user_message_request_waiting != null) {
						try {
							MessageEngine.doSubTaskForUserMessageRequestWaitingKey(user_message_request_waiting);
						} catch (Exception e) {
							logger.error(e.getMessage());
							if (Constants.devMode) {
								e.printStackTrace();
							}
							

						}
					}
				}
			}
		}

	}

}