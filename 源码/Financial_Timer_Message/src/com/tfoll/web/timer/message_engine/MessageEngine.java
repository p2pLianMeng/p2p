package com.tfoll.web.timer.message_engine;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.config.Constants;
import com.tfoll.web.model.UserMessageRequestErrorM;
import com.tfoll.web.model.UserMessageRequestSuccessM;
import com.tfoll.web.model.UserMessageRequestWaitingM;
import com.tfoll.web.model.UserWithdrawCashLogM;

import java.util.Date;

import org.apache.log4j.Logger;

public class MessageEngine {
	private static Logger logger = Logger.getLogger(MessageEngine.class);

	public static void doSubTaskForUserMessageRequestWaitingKey(UserMessageRequestWaitingM user_email_request_waiting) throws Exception {
		if (user_email_request_waiting == null) {
			throw new NullPointerException("user_email_request_waiting is null");
		}
		int request_type = user_email_request_waiting.getInt("request_type");
		if (request_type == 1) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType1(user_email_request_waiting);
		}
		if (request_type == 2) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType2(user_email_request_waiting);
		}
		if (request_type == 3) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType3(user_email_request_waiting);
		}
		if (request_type == 4) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType4(user_email_request_waiting);
		}
		if (request_type == 5) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType5(user_email_request_waiting);
		}
		if (request_type == 6) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType6(user_email_request_waiting);
		}
		if (request_type == 7) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType7(user_email_request_waiting);
		}
		if (request_type == 8) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType8(user_email_request_waiting);
		}
		if (request_type == 9) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType9(user_email_request_waiting);
		}
		if (request_type == 10) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType10(user_email_request_waiting);
		}
		if (request_type == 11) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType11(user_email_request_waiting);
		}
		if (request_type == 12) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType12(user_email_request_waiting);
		}
		if (request_type == 13) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType13(user_email_request_waiting);
		}
		if (request_type == 14) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType14(user_email_request_waiting);
		}
		if (request_type == 15) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType15(user_email_request_waiting);
		}
		if (request_type == 16) {
			doSubTaskForUserMessageRequestWaitingKeyByRequestType16(user_email_request_waiting);
		}

	}

	/**
	 * 1 贷款驳回
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType1(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {
		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");
		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 1) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_loans_rejected_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 2 绑定手机
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType2(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 2) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_binding_phone_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 3 修改手机号码
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType3(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 3) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_update_phone_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 4 提现申请发送验证码
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType4(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int len = content.length();
		String code = content.substring(0, 6);
		String cny = content.substring(7, len);
		String[] a = new String[2];
		a[0] = cny;
		a[1] = code;
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 4) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_recharge_message(phone, a);// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 5 设置提现密码发送验证码
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType5(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 5) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_binding_withdraw_password_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 6 找回提现密码发送验证码
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType6(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 6) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_back_withdraw_password_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 * 7 提现被驳回
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType7(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");
		// String content = user_message_request_waiting.getString("content");
		UserWithdrawCashLogM user_withdraw_cashlog = UserWithdrawCashLogM.dao.findFirst("select * from user_withdraw_cash_log where is_throuth = 4 and user_id =? order by add_time desc", new Object[] { user_id });
		Date date = user_withdraw_cashlog.getDate("add_time");

		String content = Model._Date.format(date);
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 7) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_withdraw_back_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 *8修改提现密码发送验证信息
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType8(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 8) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_update_withdraw_password_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}

	}

	/**
	 *9人民币提现发送提示消息
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType9(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 9) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_rmb_withdraw_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}

	/**
	 *10修改提现密码发送验证信息
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType10(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 10) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_borrow_success_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}

	/**
	 *11 贷款资料驳回
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType11(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");

		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 11) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_borrow_fail_message(phone);// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}

	/**
	 *12 找回登录密码
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType12(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");
		String content = user_message_request_waiting.getString("content");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 12) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_back_password_message(phone, new String[] { content });// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}

	/**
	 *13 收到还款
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType13(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");
		String content = user_message_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 13) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_receive_repayment_message(phone, b);// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}

	/**
	 *14 还款成功
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType14(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");
		String content = user_message_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 14) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_reimbursement_success_message(phone, b);// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}

	/**
	 *15 提前还款成功
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType15(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");
		String content = user_message_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 15) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_early_repayment_success_message(phone, b);// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}

	/**
	 *16 严重逾期通知
	 */
	private static void doSubTaskForUserMessageRequestWaitingKeyByRequestType16(UserMessageRequestWaitingM user_message_request_waiting) throws Exception {

		long id = user_message_request_waiting.getLong("id");
		int user_id = user_message_request_waiting.getInt("user_id");
		String ip = user_message_request_waiting.getString("ip");
		String phone = user_message_request_waiting.getString("phone");
		String content = user_message_request_waiting.getString("content");
		String[] b = content.split("#");
		int request_type = user_message_request_waiting.getInt("request_type");
		int retry_times = user_message_request_waiting.getInt("retry_times");
		// Date add_time=user_email_request_waiting.getDate("add_time");

		if (request_type == 16) {
			boolean send_ok = false;
			String failure_cause = "";
			try {
				send_ok = MessageEngineTemplate.send_seriously_delinquent_message(phone, b);// 关注这个就可以了
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				failure_cause = e.getMessage() + "--" + e.getCause().getMessage();
				logger.error(failure_cause);

			}
			if (send_ok) {
				UserMessageRequestSuccessM user_message_request_success = new UserMessageRequestSuccessM();
				user_message_request_success.//
						set("waiting_id", id).//
						set("user_id", user_id).//
						set("ip", ip).//
						set("phone", phone).//
						set("content", content).//
						set("request_type", request_type).//
						// set("retry_times", retry_times).//
						// set("add_time", add_time).//
						save();
				user_message_request_waiting.delete();

			} else {

				/**
				 * 判断当前的重试次数是否达到三次则写入失败表-包括失败的原因-同时删除原来的记录-关键是原来的记录可能不存在
				 */
				if (retry_times >= 3) {
					UserMessageRequestErrorM user_message_request_error = new UserMessageRequestErrorM();
					user_message_request_error.//
							set("waiting_id", id).//
							set("user_id", user_id).//
							set("ip", ip).//
							set("phone", phone).//
							set("content", content).//
							set("request_type", request_type).//
							set("retry_times", retry_times).//
							// set("add_time", add_time).//
							set("failure_cause", failure_cause).//
							save();
					user_message_request_waiting.delete();
				} else {
					user_message_request_waiting.set("retry_times", retry_times + 1).update();
				}
			}

		}
	}
}