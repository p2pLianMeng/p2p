package com.tfoll.web.common;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 
 * 通过在一个static类里面注册一个ServletContextListener实例-将ServletContext分享出来
 * 
 */
public class TfollServletContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent servletContextEvent) {

	}

	public void contextInitialized(ServletContextEvent servletContextEvent) {

		ServletContext context = servletContextEvent.getServletContext();
		TfollServletContextMapExchanger.registerServletContext(context);

	}

}
