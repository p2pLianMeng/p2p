package com.tfoll.web.util;

/**
 * 
 * 
 * @author hx
 * 
 */
public class JsonMsgObject {
	private Object object_1;
	private Object object_2;
	private Object object_3;
	private Object object_4;
	private Object object_5;
	private Object object_6;
	private Object object_7;
	private Object object_8;
	private Object object_9;
	private Object object_10;
	private Object object_11;
	private Object object_12;
	private Object object_13;

	public JsonMsgObject(Object object_1, Object object_2, Object object_3, Object object_4, Object object_5, Object object_6, Object object_7, Object object_8, Object object_9, Object object_10, Object object_11, Object object_12, Object object_13) {
		this.object_1 = object_1;
		this.object_2 = object_2;
		this.object_3 = object_3;
		this.object_4 = object_4;
		this.object_5 = object_5;
		this.object_6 = object_6;
		this.object_7 = object_7;
		this.object_8 = object_8;
		this.object_9 = object_9;
		this.object_10 = object_10;
		this.object_11 = object_11;
		this.object_12 = object_12;
		this.object_13 = object_13;
	}

	public JsonMsgObject() {
	}

	public Object getObject_1() {
		return object_1;
	}

	public void setObject_1(Object object_1) {
		this.object_1 = object_1;
	}

	public Object getObject_2() {
		return object_2;
	}

	public void setObject_2(Object object_2) {
		this.object_2 = object_2;
	}

	public Object getObject_3() {
		return object_3;
	}

	public void setObject_3(Object object_3) {
		this.object_3 = object_3;
	}

	public Object getObject_4() {
		return object_4;
	}

	public void setObject_4(Object object_4) {
		this.object_4 = object_4;
	}

	public Object getObject_5() {
		return object_5;
	}

	public void setObject_5(Object object_5) {
		this.object_5 = object_5;
	}

	public Object getObject_6() {
		return object_6;
	}

	public void setObject_6(Object object_6) {
		this.object_6 = object_6;
	}

	public Object getObject_7() {
		return object_7;
	}

	public void setObject_7(Object object_7) {
		this.object_7 = object_7;
	}

	public Object getObject_8() {
		return object_8;
	}

	public void setObject_8(Object object_8) {
		this.object_8 = object_8;
	}

	public Object getObject_9() {
		return object_9;
	}

	public void setObject_9(Object object_9) {
		this.object_9 = object_9;
	}

	public Object getObject_10() {
		return object_10;
	}

	public void setObject_10(Object object_10) {
		this.object_10 = object_10;
	}

	public Object getObject_11() {
		return object_11;
	}

	public void setObject_11(Object object_11) {
		this.object_11 = object_11;
	}

	public Object getObject_12() {
		return object_12;
	}

	public void setObject_12(Object object_12) {
		this.object_12 = object_12;
	}

	public Object getObject_13() {
		return object_13;
	}

	public void setObject_13(Object object_13) {
		this.object_13 = object_13;
	}

}
